import { Component, OnInit } from '@angular/core';
import { AbstractControl, FormControl, FormGroup, ValidatorFn, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { AlertController } from '@ionic/angular';
import { AuthService } from '@services/auth/auth.service';
import { LocalstorageService } from '@services/shared/localstroage/localstorage.service';
import { UserdataService } from '@services/userdata.service';

@Component({
  selector: 'app-auth',
  templateUrl: './auth.page.html',
  styleUrls: ['./auth.page.scss'],
})
export class AuthPage implements OnInit {
  segmentModel = "login";
  userRole;
  loginForm;
  registrationForm;
  constructor(
    private router: Router,
    private userData: UserdataService,
    private authService: AuthService,
    private alertCtrl: AlertController,
    private localStorage: LocalstorageService
  ) { }

  ngOnInit() {
    this.loginFormInitialization();
    this.registrationFormInitialization();
    this.userRole = this.localStorage.getItem('registrationRole');
  }

  loginFormInitialization() {
    this.loginForm = new FormGroup({
      email: new FormControl('', [Validators.required, Validators.email]),
      password: new FormControl('', Validators.required)
    });
  }

  async goToScanArrival() {

    if (this.loginForm.valid) {
      let newData = {
        "email": this.loginForm.value.email,
        "password": this.loginForm.value.password
      }
      let res = await this.authService.login(newData);
      if (res) {
        if (res['error']) {
          this.showAlert('Error', JSON.stringify(res['error'].message));
        } else {
          if (res['user']) {
            this.localStorage.setItem('user', JSON.stringify(res['user']));
          }
          this.localStorage.setItem('userLoginDetails', JSON.stringify(res));
          if (this.userRole != undefined && this.userRole != null) {
            if (this.userRole == 'coach') {
              this.router.navigate(['/body-scan-arrival-coach'])
            } else {
              this.router.navigate(['/body-scan-arrival'])
            }
          }
        }
      }
    } else {

    }
  }

  registrationFormInitialization() {
    this.registrationForm = new FormGroup({
      email: new FormControl('', [Validators.required, Validators.email]),
      password: new FormControl('', Validators.required),
      confirm_password: new FormControl('', [Validators.required, this.equalto('password')]),
      first_name: new FormControl('', Validators.required),
      last_name: new FormControl('', Validators.required)
    });
  }

  async registerAndRedirect() {
    if (this.registrationForm.valid) {

      let newData = {
        "email": this.registrationForm.value.email,
        "password": this.registrationForm.value.password,
        "role": (this.userRole != undefined && this.userRole != null) ? this.userRole : '',
        "name": this.registrationForm.value.last_name + ' ' + this.registrationForm.value.first_name
      }
      let res = await this.authService.register(newData);
      if (res) {
        if (res['error']) {
          this.showAlert('Error', res['error'].message);
        } else {
          this.showAlert('Success', JSON.stringify(res['message']));
          this.localStorage.removeItem('registrationRole');
          this.router.navigate(['/body-scan-arrival'])
        }
      }
    } else {

    }
  }

  equalto(field_name): ValidatorFn {
    return (control: AbstractControl): { [key: string]: any } => {
      let input = control.value;
      let isValid = control.root.value[field_name] == input;
      if (!isValid)
        return { 'equalTo': { isValid } };
      else
        return null;
    };
  }


  async showAlert(status = '', message = '') {
    const alert = this.alertCtrl.create({
      header: status,
      message: message,
      buttons: ['Dismiss']
    });
    (await alert).present();
  }

  segmentChanged(event) {
    console.log(this.segmentModel);

    console.log(event);
  }

  goBack() {
    this.router.navigate(['/select-roles'])
  }


}
