import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { SelectRolesPage } from './select-roles.page';

const routes: Routes = [
  {
    path: '',
    component: SelectRolesPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class SelectRolesPageRoutingModule {}
