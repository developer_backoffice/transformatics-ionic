import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { LocalstorageService } from '@services/shared/localstroage/localstorage.service';
import { UserdataService } from '@services/userdata.service';

@Component({
  selector: 'app-select-roles',
  templateUrl: './select-roles.page.html',
  styleUrls: ['./select-roles.page.scss'],
})
export class SelectRolesPage implements OnInit {

  constructor(
    private router: Router,
    private userData: UserdataService,
    private localStorage: LocalstorageService
  ) { }

  ngOnInit() {
  }

  async goToAuth(role) {
    this.localStorage.setItem('registrationRole', role ? 'coach' : 'athlete');
    if (role) {
      this.router.navigate(['/auth'])
      return this.userData.isCoach = true;
    }


    this.router.navigate(['/auth'])

    return this.userData.isCoach = false;




  }
}
