import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { SelectRolesPageRoutingModule } from './select-roles-routing.module';

import { SelectRolesPage } from './select-roles.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    SelectRolesPageRoutingModule
  ],
  declarations: [SelectRolesPage]
})
export class SelectRolesPageModule {}
