import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { SelectRolesPage } from './select-roles.page';

describe('SelectRolesPage', () => {
  let component: SelectRolesPage;
  let fixture: ComponentFixture<SelectRolesPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SelectRolesPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(SelectRolesPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
