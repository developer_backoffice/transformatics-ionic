import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { BodyScanArrivalCoachPageRoutingModule } from './body-scan-arrival-coach-routing.module';

import { BodyScanArrivalCoachPage } from './body-scan-arrival-coach.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    BodyScanArrivalCoachPageRoutingModule
  ],
  declarations: [BodyScanArrivalCoachPage]
})
export class BodyScanArrivalCoachPageModule {}
