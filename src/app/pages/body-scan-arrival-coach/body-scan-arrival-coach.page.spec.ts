import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { BodyScanArrivalCoachPage } from './body-scan-arrival-coach.page';

describe('BodyScanArrivalCoachPage', () => {
  let component: BodyScanArrivalCoachPage;
  let fixture: ComponentFixture<BodyScanArrivalCoachPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BodyScanArrivalCoachPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(BodyScanArrivalCoachPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
