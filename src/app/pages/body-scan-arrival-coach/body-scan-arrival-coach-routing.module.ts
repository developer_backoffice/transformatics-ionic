import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { BodyScanArrivalCoachPage } from './body-scan-arrival-coach.page';

const routes: Routes = [
  {
    path: '',
    component: BodyScanArrivalCoachPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class BodyScanArrivalCoachPageRoutingModule {}
