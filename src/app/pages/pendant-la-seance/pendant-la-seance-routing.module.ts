import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { PendantLaSeancePage } from './pendant-la-seance.page';

const routes: Routes = [
  {
    path: '',
    component: PendantLaSeancePage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class PendantLaSeancePageRoutingModule {}
