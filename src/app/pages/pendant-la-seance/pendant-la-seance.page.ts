import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-pendant-la-seance',
  templateUrl: './pendant-la-seance.page.html',
  styleUrls: ['./pendant-la-seance.page.scss'],
})
export class PendantLaSeancePage implements OnInit {

  constructor() { }

  ngOnInit() {
  }
  categories = {
    slidesPerView: 1,
    effect: 'flip',
    slidesPerColumn: 1,
    slidesPerGroup: 1,
    watchSlidesProgress: true,
  };
}
