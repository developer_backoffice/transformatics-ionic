import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { PendantLaSeancePageRoutingModule } from './pendant-la-seance-routing.module';

import { PendantLaSeancePage } from './pendant-la-seance.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    PendantLaSeancePageRoutingModule
  ],
  declarations: [PendantLaSeancePage]
})
export class PendantLaSeancePageModule {}
