import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { PendantLaSeancePage } from './pendant-la-seance.page';

describe('PendantLaSeancePage', () => {
  let component: PendantLaSeancePage;
  let fixture: ComponentFixture<PendantLaSeancePage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PendantLaSeancePage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(PendantLaSeancePage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
