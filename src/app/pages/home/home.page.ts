import { Component, OnInit } from '@angular/core';
import { HttpService } from '@services/http.service'

@Component({
  selector: 'app-home',
  templateUrl: './home.page.html',
  styleUrls: ['./home.page.scss'],
})
export class HomePage implements OnInit {

  constructor(private http: HttpService) { }

  ngOnInit() {
  	console.log(this.http.callApi("", {}, ""))
  	
  }

}
