import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { MusculationAutoSeancesProgrammsPageRoutingModule } from './musculation-auto-seances-programms-routing.module';

import { MusculationAutoSeancesProgrammsPage } from './musculation-auto-seances-programms.page';
import { SharedModule } from '@components/shared.module';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    MusculationAutoSeancesProgrammsPageRoutingModule,
    SharedModule
  ],
  declarations: [MusculationAutoSeancesProgrammsPage]
})
export class MusculationAutoSeancesProgrammsPageModule {}
