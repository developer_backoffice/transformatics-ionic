import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { MusculationAutoSeancesProgrammsPage } from './musculation-auto-seances-programms.page';

const routes: Routes = [
  {
    path: '',
    component: MusculationAutoSeancesProgrammsPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class MusculationAutoSeancesProgrammsPageRoutingModule {}
