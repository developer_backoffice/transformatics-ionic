import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { MusculationAutoSeancesProgrammsPage } from './musculation-auto-seances-programms.page';

describe('MusculationAutoSeancesProgrammsPage', () => {
  let component: MusculationAutoSeancesProgrammsPage;
  let fixture: ComponentFixture<MusculationAutoSeancesProgrammsPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MusculationAutoSeancesProgrammsPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(MusculationAutoSeancesProgrammsPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
