import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { MusculationAutoProgrammsSessionPage } from './musculation-auto-programms-session.page';

describe('MusculationAutoProgrammsSessionPage', () => {
  let component: MusculationAutoProgrammsSessionPage;
  let fixture: ComponentFixture<MusculationAutoProgrammsSessionPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MusculationAutoProgrammsSessionPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(MusculationAutoProgrammsSessionPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
