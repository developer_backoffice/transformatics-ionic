import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { MusculationAutoProgrammsSessionPage } from './musculation-auto-programms-session.page';

const routes: Routes = [
  {
    path: '',
    component: MusculationAutoProgrammsSessionPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class MusculationAutoProgrammsSessionPageRoutingModule {}
