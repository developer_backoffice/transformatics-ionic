import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-musculation-auto-programms-session',
  templateUrl: './musculation-auto-programms-session.page.html',
  styleUrls: ['./musculation-auto-programms-session.page.scss'],
})
export class MusculationAutoProgrammsSessionPage implements OnInit {

  constructor() { }

  ngOnInit() {
  }
  public onItemReorder({ detail }) {
    detail.complete(true);
  }
}
