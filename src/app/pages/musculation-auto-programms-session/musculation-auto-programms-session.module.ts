import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { MusculationAutoProgrammsSessionPageRoutingModule } from './musculation-auto-programms-session-routing.module';

import { MusculationAutoProgrammsSessionPage } from './musculation-auto-programms-session.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    MusculationAutoProgrammsSessionPageRoutingModule
  ],
  declarations: [MusculationAutoProgrammsSessionPage]
})
export class MusculationAutoProgrammsSessionPageModule {}
