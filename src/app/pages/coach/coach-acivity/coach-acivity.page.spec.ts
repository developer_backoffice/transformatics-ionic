import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { CoachAcivityPage } from './coach-acivity.page';

describe('CoachAcivityPage', () => {
  let component: CoachAcivityPage;
  let fixture: ComponentFixture<CoachAcivityPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CoachAcivityPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(CoachAcivityPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
