import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { CoachAcivityPageRoutingModule } from './coach-acivity-routing.module';

import { CoachAcivityPage } from './coach-acivity.page';
import { SharedModule } from '@components/shared.module';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    CoachAcivityPageRoutingModule,
    SharedModule
  ],
  declarations: [CoachAcivityPage]
})
export class CoachAcivityPageModule {}
