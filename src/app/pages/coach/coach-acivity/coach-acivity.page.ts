import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-coach-acivity',
  templateUrl: './coach-acivity.page.html',
  styleUrls: ['./coach-acivity.page.scss'],
})
export class CoachAcivityPage implements OnInit {

  parentSegmentModel = "paiements";
  parentSegmentModelChild = "mon-activite";
  parentSegmentModelChildTwo = "nombretotaldeclients";
  parentSegmentModelChildFacturation = "twojor";
  parentSegmentModelChildPaiements = "visibilite";

  constructor() { }

  ngOnInit() {
  }
  segmentChanged(event){
    console.log(event);
  }
  segmentChangedChild(event){
    console.log(event);
    this.parentSegmentModelChild = event.detail.value
  }
  segmentChangedChildTwo(event){
    console.log(event);
    this.parentSegmentModelChildTwo = event.detail.value
  }
  
  segmentChangedChildPaiements(event){
    console.log(event);
    this.parentSegmentModelChildTwo = event.detail.value
  }
  segmentChangedChildFacturation(event){
    console.log(event);
    this.parentSegmentModelChildFacturation = event.detail.value
  }
}
