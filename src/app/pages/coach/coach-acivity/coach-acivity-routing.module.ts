import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { CoachAcivityPage } from './coach-acivity.page';

const routes: Routes = [
  {
    path: '',
    component: CoachAcivityPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class CoachAcivityPageRoutingModule {}
