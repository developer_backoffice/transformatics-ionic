import { Component, OnInit, ViewChild } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { PaymentModalComponent } from '@components/modals/payment-modal/payment-modal.component';
import { ModalController } from '@ionic/angular';

@Component({
  selector: 'app-select-plan-coach',
  templateUrl: './select-plan-coach.page.html',
  styleUrls: ['./select-plan-coach.page.scss'],
})
export class SelectPlanCoachPage implements OnInit {

  planSlideOptions = {
    centeredSlides: true,
    slidesPerView: 1.1,
    spaceBetween: 10,
  }

  activepage = 0;

  @ViewChild('slides') slider;
  
  constructor(
    public router: Router,
    public route: ActivatedRoute,
    public modal: ModalController
  ) { }


  
  ngOnInit() {
  }

  async getIndex(event) {
    console.log(event);
    this.activepage = await this.slider.getActiveIndex();


    // console.log("currentIndex", currentIndex);

  }

  goToBodyScan() {
    console.log("cdsc");
    this.router.navigate(['/body-scan-step-coach']);
  }

  async openPaymentModal() {
    const modal = await this.modal.create({
      component: PaymentModalComponent,
      cssClass: 'check-up-modal',
      // showBackdrop: true,
    });
    return await modal.present();
  }

}
