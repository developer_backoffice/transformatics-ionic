import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { SelectPlanCoachPage } from './select-plan-coach.page';

const routes: Routes = [
  {
    path: '',
    component: SelectPlanCoachPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class SelectPlanCoachPageRoutingModule {}
