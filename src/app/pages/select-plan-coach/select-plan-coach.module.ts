import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { SelectPlanCoachPageRoutingModule } from './select-plan-coach-routing.module';

import { SelectPlanCoachPage } from './select-plan-coach.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    SelectPlanCoachPageRoutingModule
  ],
  declarations: [SelectPlanCoachPage]
})
export class SelectPlanCoachPageModule {}
