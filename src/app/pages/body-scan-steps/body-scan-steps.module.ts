import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { BodyScanStepsPageRoutingModule } from './body-scan-steps-routing.module';

import { BodyScanStepsPage } from './body-scan-steps.page';
import { SharedModule } from '@components/shared.module';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    BodyScanStepsPageRoutingModule,
    SharedModule
  ],
  declarations: [BodyScanStepsPage]
})
export class BodyScanStepsPageModule { }
