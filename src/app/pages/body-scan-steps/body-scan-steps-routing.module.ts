import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { BodyScanStepsPage } from './body-scan-steps.page';

const routes: Routes = [
  {
    path: '',
    component: BodyScanStepsPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class BodyScanStepsPageRoutingModule {}
