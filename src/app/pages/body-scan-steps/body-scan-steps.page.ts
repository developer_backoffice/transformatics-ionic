import { Component, Input, OnInit } from '@angular/core';
import { exit } from 'process';
import { Router, ActivatedRoute } from '@angular/router';
import { BodyScanStepOneComponent } from '../../components/body-scan-step-one/body-scan-step-one.component';
import { UserdataService } from '@services/userdata.service';
import { LocalstorageService } from '@services/shared/localstroage/localstorage.service';
@Component({
  selector: 'app-body-scan-steps',
  templateUrl: './body-scan-steps.page.html',
  styleUrls: ['./body-scan-steps.page.scss'],
})
export class BodyScanStepsPage implements OnInit {
  @Input() text;
  activepage: number = 1;

  bodyScanStepsDetails;
  bodyScanStep1Details;
  bodyScanStep2Details;
  bodyScanStep3Details;
  bodyScanStep4Details;
  bodyScanStep5Details;

  constructor(
    public router: Router,
    public route: ActivatedRoute,
    public bodyScanStepOne: BodyScanStepOneComponent,
    public userDataService: UserdataService,
    private localStorage: LocalstorageService
  ) { }


  ngOnInit() {
    console.log(this.activepage)
    // console.log(this.bodyScanStepOne.gender);
    // this.bodyScanStepOne.gender = "female";
    // console.log("here:" + this.bodyScanStepOne.gender);
  }

  nextPage() {
    if (this.activepage >= 5) {
      this.saveBodyScan();
      // this.router.navigate(['/tabs/home'])
      this.activepage = 1;
      return
    }
    this.activepage += 1;

  }

  previousPage() {
    if (this.activepage <= 1) {
      this.activepage = 1;
      this.router.navigate(['body-scan-arrival']);
      return
    }
    this.activepage -= 1;
    console.log("next", this.activepage)
  }

  submitted(event) {
    console.log(event);
    if (this.activepage == 1) {
      this.bodyScanStep1Details = event;
    } else if (this.activepage == 2) {
      this.bodyScanStep2Details = event;
    } else if (this.activepage == 3) {
      this.bodyScanStep3Details = event;
    } else if (this.activepage == 4) {
      this.bodyScanStep4Details = event;
    } else if (this.activepage == 5) {
      this.bodyScanStep5Details = event;
    }
  }

  saveBodyScan() {
    let user = this.localStorage.getItem('user');
    if (user) {
      user = JSON.parse(user);
    }
    console.log(user);
    this.bodyScanStepsDetails = { 'user_id': user['id'], ...this.bodyScanStep1Details, ...this.bodyScanStep2Details, ...this.bodyScanStep3Details, ...this.bodyScanStep4Details, ...this.bodyScanStep5Details, ...this.bodyScanStep1Details };
    console.log(this.bodyScanStepsDetails);
    let res = this.userDataService.bodyScanDetailsStore(this.bodyScanStepsDetails);
  }

}
