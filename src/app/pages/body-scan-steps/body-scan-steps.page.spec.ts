import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { BodyScanStepsPage } from './body-scan-steps.page';

describe('BodyScanStepsPage', () => {
  let component: BodyScanStepsPage;
  let fixture: ComponentFixture<BodyScanStepsPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BodyScanStepsPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(BodyScanStepsPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
