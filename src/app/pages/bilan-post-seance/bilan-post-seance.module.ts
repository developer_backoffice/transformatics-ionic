import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { BilanPostSeancePageRoutingModule } from './bilan-post-seance-routing.module';

import { BilanPostSeancePage } from './bilan-post-seance.page';
import { NgCircleProgressModule } from 'ng-circle-progress';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
      // Specify ng-circle-progress as an import
    NgCircleProgressModule.forRoot({
      // set defaults here
      "percent": 77.3,
      "backgroundPadding": 15,
      "radius": 0,
      "space": -10,
      "outerStrokeWidth": 10,
      "innerStrokeColor": "#e7e8ea",
      "innerStrokeWidth": 10,
      "outerStrokeGradient":true,
      "outerStrokeColor":"#f4b183",
      "outerStrokeGradientStopColor":"#926444",
      "subtitle": 'MOYEN',
      "title":[
        "RESUME DE LA SEANCE",
        "",
        "77.3%",
        ""
      ] ,
      "titleFontSize": "12",
      "subtitleFontSize": "10",
      "animateTitle": false,
      "animationDuration": 1000,
      "showUnits": false,
      "clockwise": true,
      "showSubtitle": true,
      "showTitle": true,

    }),

    BilanPostSeancePageRoutingModule
  ],
  declarations: [BilanPostSeancePage]
})
export class BilanPostSeancePageModule {}
