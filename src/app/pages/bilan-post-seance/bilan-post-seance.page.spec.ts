import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { BilanPostSeancePage } from './bilan-post-seance.page';

describe('BilanPostSeancePage', () => {
  let component: BilanPostSeancePage;
  let fixture: ComponentFixture<BilanPostSeancePage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BilanPostSeancePage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(BilanPostSeancePage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
