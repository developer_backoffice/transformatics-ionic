import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { BilanPostSeancePage } from './bilan-post-seance.page';

const routes: Routes = [
  {
    path: '',
    component: BilanPostSeancePage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class BilanPostSeancePageRoutingModule {}
