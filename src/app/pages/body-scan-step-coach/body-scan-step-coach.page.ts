import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'app-body-scan-step-coach',
  templateUrl: './body-scan-step-coach.page.html',
  styleUrls: ['./body-scan-step-coach.page.scss'],
})
export class BodyScanStepCoachPage implements OnInit {

  activepage: number = 1;
  constructor(
    public router: Router,
    public route: ActivatedRoute,
  ) { }

    
  ngOnInit() {
    console.log(this.activepage)
  }

  nextPage() {
    if(this.activepage >= 3) {
      this.router.navigate(['/coach-acivity'])
      this.activepage = 1;
      return
    }
    this.activepage += 1;
    console.log("next", this.activepage)
    
  }

  previousPage() {
    if(this.activepage <= 1) {
      this.activepage = 1;
      this.router.navigate(['body-scan-arrival']);
      return
    }
    this.activepage -= 1;
    console.log("next", this.activepage)
  }

}
