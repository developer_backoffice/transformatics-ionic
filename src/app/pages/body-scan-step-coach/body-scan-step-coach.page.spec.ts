import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { BodyScanStepCoachPage } from './body-scan-step-coach.page';

describe('BodyScanStepCoachPage', () => {
  let component: BodyScanStepCoachPage;
  let fixture: ComponentFixture<BodyScanStepCoachPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BodyScanStepCoachPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(BodyScanStepCoachPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
