import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { BodyScanStepCoachPage } from './body-scan-step-coach.page';

const routes: Routes = [
  {
    path: '',
    component: BodyScanStepCoachPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class BodyScanStepCoachPageRoutingModule {}
