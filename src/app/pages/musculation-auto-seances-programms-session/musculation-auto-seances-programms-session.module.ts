import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { MusculationAutoSeancesProgrammsSessionPageRoutingModule } from './musculation-auto-seances-programms-session-routing.module';

import { MusculationAutoSeancesProgrammsSessionPage } from './musculation-auto-seances-programms-session.page';
import { SharedModule } from '@components/shared.module';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    MusculationAutoSeancesProgrammsSessionPageRoutingModule,
    SharedModule
  ],
  declarations: [MusculationAutoSeancesProgrammsSessionPage]
})
export class MusculationAutoSeancesProgrammsSessionPageModule {}
