import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { MusculationAutoSeancesProgrammsSessionPage } from './musculation-auto-seances-programms-session.page';

describe('MusculationAutoSeancesProgrammsSessionPage', () => {
  let component: MusculationAutoSeancesProgrammsSessionPage;
  let fixture: ComponentFixture<MusculationAutoSeancesProgrammsSessionPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MusculationAutoSeancesProgrammsSessionPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(MusculationAutoSeancesProgrammsSessionPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
