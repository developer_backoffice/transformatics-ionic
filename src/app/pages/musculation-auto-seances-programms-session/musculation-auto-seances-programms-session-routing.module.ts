import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { MusculationAutoSeancesProgrammsSessionPage } from './musculation-auto-seances-programms-session.page';

const routes: Routes = [
  {
    path: '',
    component: MusculationAutoSeancesProgrammsSessionPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class MusculationAutoSeancesProgrammsSessionPageRoutingModule {}
