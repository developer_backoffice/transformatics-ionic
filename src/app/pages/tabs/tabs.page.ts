import { AfterViewInit, Component, OnInit } from '@angular/core';
import { Routes, RouterModule,Router,NavigationEnd } from '@angular/router';
import { BeforeAlimentsComponent } from '@components/modals/before-aliments/before-aliments.component';
import { ModalController } from '@ionic/angular';
import { UserdataService } from '@services/userdata.service';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-tabs',
  templateUrl: './tabs.page.html',
  styleUrls: ['./tabs.page.scss'],
})
export class TabsPage {
  active: boolean = false;
  showBackdrop = false;
 
  constructor(private router:Router,private userData: UserdataService, private modal: ModalController) {
    
  }
  
  ngAfterViewChecked() {
  
    
    
  }
  ngAfterViewInit() {
    
  }
  myMethod1() {
    this.userData.changeColor = true;
    // this.active = true;
  }
  myMethod2() {
    this.userData.changeColor = false;
    // this.active = false;
  }


  async openManualModal() {
    const modal = await this.modal.create({
      component: BeforeAlimentsComponent,
      cssClass: 'check-up-modal',
      showBackdrop: false,
      // mode: "ios",
    });
    return await modal.present();
  }

}
