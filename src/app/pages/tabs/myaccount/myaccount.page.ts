import { Component, OnInit, ViewChild,NgZone } from '@angular/core';
import { Subscription } from 'rxjs';
import { NavigationEnd, Router } from '@angular/router';
import { UserdataService } from '@services/userdata.service';
import {IonContent} from '@ionic/angular';

@Component({
  selector: 'app-myaccount',
  templateUrl: './myaccount.page.html',
  styleUrls: ['./myaccount.page.scss'],
})
export class MyaccountPage implements OnInit {
  
  @ViewChild("contentRef") contentHandle;
  segmentModel = "performance";
  parentSegmentModel = "profile";
  routerSubscription: Subscription;
  parameterSegmentModel = "autorisations"
  private lastY = 0;


  constructor(private router: Router,private userData: UserdataService, private zone: NgZone) {
    this.routerWatch();
   }

  ngOnInit() {
  }

  ionViewDidEnter() {
   
  }


  segmentChanged(event){
    console.log(this.segmentModel);
    
    console.log(event);
  }


  parameterSegmentChanged(event){
    this.parameterSegmentModel = event.detail.value
    console.log(event);
  }
  routerWatch() {
    this.routerSubscription = this.router.events.subscribe(
      (event: NavigationEnd) => {
        if (event instanceof NavigationEnd) {
          if (event.url == '/tabs/account') {
            console.log(111)
            this.userData.changeColor = true;
          }
        }
      }
    );
  }

  ionPageWillLeave() {
    this.routerSubscription.unsubscribe();
  }
  scrollHandler(event) {
    this.zone.run(() => {
       
       
            this.lastY = 5;
            if (event.detail.scrollTop > this.lastY) {
                let elem = document.querySelector('ion-tab-bar');
                elem.style.setProperty('margin-bottom',"-100px");
                elem.style.setProperty('transition',"0.5s");
              
            } else {
              let elem = document.querySelector('ion-tab-bar');
              elem.style.setProperty('margin-bottom',"10px");
              elem.style.setProperty('transition',"0.5s");
            }
            this.lastY = event.detail.scrollTop;

    });
     
}

}
