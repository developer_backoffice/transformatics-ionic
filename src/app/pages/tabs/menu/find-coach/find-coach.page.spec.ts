import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { FindCoachPage } from './find-coach.page';

describe('FindCoachPage', () => {
  let component: FindCoachPage;
  let fixture: ComponentFixture<FindCoachPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FindCoachPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(FindCoachPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
