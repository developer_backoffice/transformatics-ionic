import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { FindCoachPageRoutingModule } from './find-coach-routing.module';

import { FindCoachPage } from './find-coach.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    FindCoachPageRoutingModule
  ],
  declarations: [FindCoachPage]
})
export class FindCoachPageModule {}
