import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-find-coach',
  templateUrl: './find-coach.page.html',
  styleUrls: ['./find-coach.page.scss'],
})
export class FindCoachPage implements OnInit {

  constructor() { }

  ngOnInit() {
  }
  categories = {
    initialSlide: 1,
    loop:true,
    centeredSlides: true,
    slidesPerView: 1.7,
    slidesPerGroup: 1,
    spaceBetween: 5,
    speed: 500,
    fadeEffect:
    {
        crossFade: true
    }
  
  };
}
