import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { FindCoachPage } from './find-coach.page';

const routes: Routes = [
  {
    path: '',
    component: FindCoachPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class FindCoachPageRoutingModule {}
