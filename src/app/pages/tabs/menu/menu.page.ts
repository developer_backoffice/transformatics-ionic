import { Component, OnInit } from '@angular/core';
import { Routes, RouterModule,Router } from '@angular/router';
import { CheckUpModalsComponent } from '@components/modals/check-up-modals/check-up-modals.component';
import { CoursesModalComponent } from '@components/modals/courses-modal/courses-modal.component';
import { ManualRegistrationComponent } from '@components/modals/manual-registration/manual-registration.component';
import { MyDevicesModalComponent } from '@components/modals/my-devices-modal/my-devices-modal.component';
import { MyDietModalComponent } from '@components/modals/my-diet-modal/my-diet-modal.component';
import { NotebookModalComponent } from '@components/modals/notebook-modal/notebook-modal.component';
import { SponsershipModalComponent } from '@components/modals/sponsership-modal/sponsership-modal.component';
import { ModalController } from '@ionic/angular';
import { UserdataService } from '@services/userdata.service';
@Component({
  selector: 'app-menu',
  templateUrl: './menu.page.html',
  styleUrls: ['./menu.page.scss'],
})
export class MenuPage implements OnInit {
  constructor(
    private modal: ModalController,
    private router:Router,
    private userData: UserdataService
  ) { }





  ngOnInit() {
   
  }

  segmentChanged(event) {

  }

 

  async openCheckupModal() {
    const modal = await this.modal.create({
      component: CheckUpModalsComponent,
      cssClass: 'check-up-modal',
      showBackdrop: false,
      // mode: "ios",
    });
    return await modal.present();
  }

  async openPerformance() {
    this.userData.changeColor = !this.userData.changeColor;
    this.router.navigate(['/tabs/myaccount']);
  }

  async openCourseModal() {
    const modal = await this.modal.create({
      component: CoursesModalComponent,
      cssClass: 'check-up-modal',
      showBackdrop: false,
      // mode: "ios",
    });
    return await modal.present();
  }


  async openDietModal() {
    const modal = await this.modal.create({
      component: MyDietModalComponent,
      cssClass: 'check-up-modal',
      showBackdrop: false,
      // mode: "ios",
    });
    return await modal.present();
  }




  async openManualModal() {
    const modal = await this.modal.create({
      component: ManualRegistrationComponent,
      cssClass: 'check-up-modal',
      showBackdrop: false,
      // mode: "ios",
    });
    return await modal.present();
  }



  async openNotebookModal() {
    console.log("vfvfvf");
    const modal = await this.modal.create({
      component: NotebookModalComponent,
      cssClass: 'check-up-modal',
      showBackdrop: false,
      // mode: "ios",
      backdropDismiss: true,
      swipeToClose: true,

    });
    return await modal.present();
  }

  async openCart() {
    this.userData.changeColor = !this.userData.changeColor;
    this.router.navigate(['/tabs/cart']);
  } 

  async openPhysiologyModal() {
    this.userData.changeColor = !this.userData.changeColor;
    this.router.navigate(['/tabs/myaccount']);
    // console.log("vfvfvf");
    // const modal = await this.modal.create({
    //   component: Phys,
    //   cssClass: 'check-up-modal',
    //   showBackdrop: false,
    //   // mode: "ios",
    //   backdropDismiss: true
    // });
    // return await modal.present();
  }




  async openDeviceModal() {
    const modal = await this.modal.create({
      component: MyDevicesModalComponent,
      cssClass: 'check-up-modal',
      showBackdrop: false,
      // mode: "ios",
      backdropDismiss: true
    });
    return await modal.present();
  }



  async openSponserShipModal() {
    const modal = await this.modal.create({
      component: SponsershipModalComponent,
      cssClass: 'check-up-modal',
      showBackdrop: false,
      // mode: "ios",
      backdropDismiss: true
    });
    return await modal.present();
  }



}
