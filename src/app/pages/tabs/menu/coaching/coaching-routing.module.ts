import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { CoachingPage } from './coaching.page';

const routes: Routes = [
  {
    path: '',
    component: CoachingPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class CoachingPageRoutingModule {}
