import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { CoachingPageRoutingModule } from './coaching-routing.module';

import { CoachingPage } from './coaching.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    CoachingPageRoutingModule
  ],
  declarations: [CoachingPage]
})
export class CoachingPageModule {}
