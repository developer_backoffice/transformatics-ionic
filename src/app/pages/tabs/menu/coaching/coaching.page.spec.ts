import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { CoachingPage } from './coaching.page';

describe('CoachingPage', () => {
  let component: CoachingPage;
  let fixture: ComponentFixture<CoachingPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CoachingPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(CoachingPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
