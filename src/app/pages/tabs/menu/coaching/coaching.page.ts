import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-coaching',
  templateUrl: './coaching.page.html',
  styleUrls: ['./coaching.page.scss'],
})
export class CoachingPage implements OnInit {

  constructor() { }

  ngOnInit() {
  }
  categories = {
    slidesPerView: 1.8,
    effect: 'flip',
    slidesPerColumn: 1,
    slidesPerGroup: 1,
    watchSlidesProgress: true,
  };
}
