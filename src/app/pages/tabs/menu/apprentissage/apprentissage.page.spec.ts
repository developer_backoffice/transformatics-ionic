import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { ApprentissagePage } from './apprentissage.page';

describe('ApprentissagePage', () => {
  let component: ApprentissagePage;
  let fixture: ComponentFixture<ApprentissagePage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ApprentissagePage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(ApprentissagePage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
