import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-apprentissage',
  templateUrl: './apprentissage.page.html',
  styleUrls: ['./apprentissage.page.scss'],
})
export class ApprentissagePage implements OnInit {

  constructor() { }

  ngOnInit() {
  }

  categories = {
    slidesPerView: 1.8,
    effect: 'flip',
    slidesPerColumn: 1,
    slidesPerGroup: 1,
    watchSlidesProgress: true,
  };
}
