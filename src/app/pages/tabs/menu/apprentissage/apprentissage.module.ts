import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { ApprentissagePageRoutingModule } from './apprentissage-routing.module';

import { ApprentissagePage } from './apprentissage.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ApprentissagePageRoutingModule
  ],
  declarations: [ApprentissagePage]
})
export class ApprentissagePageModule {}
