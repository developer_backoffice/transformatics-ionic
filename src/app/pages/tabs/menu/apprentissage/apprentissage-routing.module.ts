import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ApprentissagePage } from './apprentissage.page';

const routes: Routes = [
  {
    path: '',
    component: ApprentissagePage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ApprentissagePageRoutingModule {}
