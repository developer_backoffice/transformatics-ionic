import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { PreparationMentalPage } from './preparation-mental.page';

describe('PreparationMentalPage', () => {
  let component: PreparationMentalPage;
  let fixture: ComponentFixture<PreparationMentalPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PreparationMentalPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(PreparationMentalPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
