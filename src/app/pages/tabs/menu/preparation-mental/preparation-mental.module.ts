import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { PreparationMentalPageRoutingModule } from './preparation-mental-routing.module';

import { PreparationMentalPage } from './preparation-mental.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    PreparationMentalPageRoutingModule
  ],
  declarations: [PreparationMentalPage]
})
export class PreparationMentalPageModule {}
