import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { PreparationMentalPage } from './preparation-mental.page';

const routes: Routes = [
  {
    path: '',
    component: PreparationMentalPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class PreparationMentalPageRoutingModule {}
