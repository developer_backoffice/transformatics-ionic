import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { EbookDetailsPageRoutingModule } from './ebook-details-routing.module';

import { EbookDetailsPage } from './ebook-details.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    EbookDetailsPageRoutingModule
  ],
  declarations: [EbookDetailsPage]
})
export class EbookDetailsPageModule {}
