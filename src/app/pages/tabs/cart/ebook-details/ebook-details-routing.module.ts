import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { EbookDetailsPage } from './ebook-details.page';

const routes: Routes = [
  {
    path: '',
    component: EbookDetailsPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class EbookDetailsPageRoutingModule {}
