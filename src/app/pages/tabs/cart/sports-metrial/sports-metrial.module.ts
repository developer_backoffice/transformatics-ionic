import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { SportsMetrialPageRoutingModule } from './sports-metrial-routing.module';

import { SportsMetrialPage } from './sports-metrial.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    SportsMetrialPageRoutingModule
  ],
  declarations: [SportsMetrialPage]
})
export class SportsMetrialPageModule {}
