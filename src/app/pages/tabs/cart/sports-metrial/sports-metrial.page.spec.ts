import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { SportsMetrialPage } from './sports-metrial.page';

describe('SportsMetrialPage', () => {
  let component: SportsMetrialPage;
  let fixture: ComponentFixture<SportsMetrialPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SportsMetrialPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(SportsMetrialPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
