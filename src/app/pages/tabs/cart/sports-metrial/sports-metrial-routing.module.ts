import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { SportsMetrialPage } from './sports-metrial.page';

const routes: Routes = [
  {
    path: '',
    component: SportsMetrialPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class SportsMetrialPageRoutingModule {}
