import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { ClothAccessoiresPageRoutingModule } from './cloth-accessoires-routing.module';

import { ClothAccessoiresPage } from './cloth-accessoires.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ClothAccessoiresPageRoutingModule
  ],
  declarations: [ClothAccessoiresPage]
})
export class ClothAccessoiresPageModule {}
