import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ClothAccessoiresPage } from './cloth-accessoires.page';

const routes: Routes = [
  {
    path: '',
    component: ClothAccessoiresPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ClothAccessoiresPageRoutingModule {}
