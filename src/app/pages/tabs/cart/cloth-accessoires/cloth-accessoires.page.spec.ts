import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { ClothAccessoiresPage } from './cloth-accessoires.page';

describe('ClothAccessoiresPage', () => {
  let component: ClothAccessoiresPage;
  let fixture: ComponentFixture<ClothAccessoiresPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ClothAccessoiresPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(ClothAccessoiresPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
