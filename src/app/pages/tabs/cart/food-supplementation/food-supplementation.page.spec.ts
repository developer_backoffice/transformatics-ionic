import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { FoodSupplementationPage } from './food-supplementation.page';

describe('FoodSupplementationPage', () => {
  let component: FoodSupplementationPage;
  let fixture: ComponentFixture<FoodSupplementationPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FoodSupplementationPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(FoodSupplementationPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
