import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { FoodSupplementationPage } from './food-supplementation.page';

const routes: Routes = [
  {
    path: '',
    component: FoodSupplementationPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class FoodSupplementationPageRoutingModule {}
