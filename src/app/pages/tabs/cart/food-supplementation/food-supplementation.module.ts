import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { FoodSupplementationPageRoutingModule } from './food-supplementation-routing.module';

import { FoodSupplementationPage } from './food-supplementation.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    FoodSupplementationPageRoutingModule
  ],
  declarations: [FoodSupplementationPage]
})
export class FoodSupplementationPageModule {}
