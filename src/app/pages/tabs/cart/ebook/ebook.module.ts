import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { EbookPageRoutingModule } from './ebook-routing.module';

import { EbookPage } from './ebook.page';
import { SharedModule } from '@components/shared.module';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    EbookPageRoutingModule,
    SharedModule
  ],
  declarations: [EbookPage]
})
export class EbookPageModule {}
