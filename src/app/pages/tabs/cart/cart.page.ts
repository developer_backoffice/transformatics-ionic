import { Component, OnInit, ViewChild,NgZone } from '@angular/core';
import { Subscription } from 'rxjs';
import { NavigationEnd, Router } from '@angular/router';
import { UserdataService } from '@services/userdata.service';

@Component({
  selector: 'app-cart',
  templateUrl: './cart.page.html',
  styleUrls: ['./cart.page.scss'],
})
export class CartPage implements OnInit {
  @ViewChild("contentRef") contentHandle;
  routerSubscription: Subscription;
  private lastY = 0;
  


  constructor(private router: Router,private userData: UserdataService,private zone: NgZone) { }

  ngOnInit() {
  }
  routerWatch() {
    this.routerSubscription = this.router.events.subscribe(
      (event: NavigationEnd) => {
        if (event instanceof NavigationEnd) {
          if (event.url == '/tabs/cart') {
            console.log(111)
            this.userData.changeColor = true;
          }
        }
      }
    );
  }

  ionPageWillLeave() {
    this.routerSubscription.unsubscribe();
  }
  scrollHandler(event) {
    this.zone.run(() => {
       
       
            this.lastY = 0;
            if (event.detail.scrollTop > this.lastY) {
                let elem = document.querySelector('ion-tab-bar');
                // let elem1 = document.querySelector('ion-fab');
                // elem1.style.setProperty('bottom',"-5.5%");
                elem.style.setProperty('margin-bottom',"-100px");
                elem.style.setProperty('transition',"0.5s");
                // elem1.style.setProperty('transition',"0.6s");
              
            } else {
              let elem = document.querySelector('ion-tab-bar');
              let elem1 = document.querySelector('ion-fab');
              // elem1.style.setProperty('bottom',"2.5%");
              elem.style.setProperty('margin-bottom',"10px");
              elem.style.setProperty('transition',"0.5s");
              // elem1.style.setProperty('transition',"0.6s");
            }
            this.lastY = event.detail.scrollTop;

    });
     
}
}
