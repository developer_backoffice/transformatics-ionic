import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { EditPhysicalActivityPageRoutingModule } from './edit-physical-activity-routing.module';

import { EditPhysicalActivityPage } from './edit-physical-activity.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    EditPhysicalActivityPageRoutingModule
  ],
  declarations: [EditPhysicalActivityPage]
})
export class EditPhysicalActivityPageModule {}
