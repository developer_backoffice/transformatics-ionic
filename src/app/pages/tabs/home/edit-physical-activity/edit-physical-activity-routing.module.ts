import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { EditPhysicalActivityPage } from './edit-physical-activity.page';

const routes: Routes = [
  {
    path: '',
    component: EditPhysicalActivityPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class EditPhysicalActivityPageRoutingModule {}
