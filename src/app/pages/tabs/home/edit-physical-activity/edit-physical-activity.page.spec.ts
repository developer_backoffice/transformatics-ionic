import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { EditPhysicalActivityPage } from './edit-physical-activity.page';

describe('EditPhysicalActivityPage', () => {
  let component: EditPhysicalActivityPage;
  let fixture: ComponentFixture<EditPhysicalActivityPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EditPhysicalActivityPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(EditPhysicalActivityPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
