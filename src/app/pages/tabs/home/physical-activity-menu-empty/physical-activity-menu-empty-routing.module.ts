import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { PhysicalActivityMenuEmptyPage } from './physical-activity-menu-empty.page';

const routes: Routes = [
  {
    path: '',
    component: PhysicalActivityMenuEmptyPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class PhysicalActivityMenuEmptyPageRoutingModule {}
