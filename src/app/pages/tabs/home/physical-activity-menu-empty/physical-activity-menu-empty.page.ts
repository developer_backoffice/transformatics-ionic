import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AddExcerciseWoodModalComponent } from '@components/modals/add-excercise-wood-modal/add-excercise-wood-modal.component';
import { ModalController } from '@ionic/angular';

@Component({
  selector: 'app-physical-activity-menu-empty',
  templateUrl: './physical-activity-menu-empty.page.html',
  styleUrls: ['./physical-activity-menu-empty.page.scss'],
})
export class PhysicalActivityMenuEmptyPage implements OnInit {

  creationMode = "manual"
  plansBtn = "program";
  firstPage = true;
  plansBtn2="program"
  programmeSelect = ""
  constructor(
    private modal: ModalController,
    private router: Router
  ) { }

  ngOnInit() {
  }


  async openModal() {
    const modal = await this.modal.create({
      component: AddExcerciseWoodModalComponent,
      cssClass: 'centerd-modal-wood-excercise',
      showBackdrop: false,
      // mode: "ios",
    });
    return await modal.present();
  }


  // redirectPageProgram() {
  //   console.log("programSelect", this.programmeSelect)
  //   console.log("creationMode", this.creationMode)
  //   if(this.creationMode == 'manual' && this.programmeSelect == 'masculation') {
  //     this.router.navigate(['/hypertrophy-strengthening-program-creation'])
  //   } else if(this.creationMode == 'automatic' && this.programmeSelect == 'masculation') {
  //     this.router.navigate(['/musculation-auto-seances-programms'])
  //   }else if(this.creationMode == 'crossfit' && this.programmeSelect == 'manual') {
  //     this.router.navigate(['/musculation-auto-seances-programms'])
  //   }
  // }
}
