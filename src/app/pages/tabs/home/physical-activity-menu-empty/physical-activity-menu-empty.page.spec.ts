import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { PhysicalActivityMenuEmptyPage } from './physical-activity-menu-empty.page';

describe('PhysicalActivityMenuEmptyPage', () => {
  let component: PhysicalActivityMenuEmptyPage;
  let fixture: ComponentFixture<PhysicalActivityMenuEmptyPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PhysicalActivityMenuEmptyPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(PhysicalActivityMenuEmptyPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
