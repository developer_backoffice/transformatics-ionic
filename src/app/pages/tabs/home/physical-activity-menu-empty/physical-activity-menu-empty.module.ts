import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { PhysicalActivityMenuEmptyPageRoutingModule } from './physical-activity-menu-empty-routing.module';

import { PhysicalActivityMenuEmptyPage } from './physical-activity-menu-empty.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    PhysicalActivityMenuEmptyPageRoutingModule
  ],
  declarations: [PhysicalActivityMenuEmptyPage]
})
export class PhysicalActivityMenuEmptyPageModule {}
