import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { HypertrophyStrengtheningProgramCreationPage } from './hypertrophy-strengthening-program-creation.page';

describe('HypertrophyStrengtheningProgramCreationPage', () => {
  let component: HypertrophyStrengtheningProgramCreationPage;
  let fixture: ComponentFixture<HypertrophyStrengtheningProgramCreationPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ HypertrophyStrengtheningProgramCreationPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(HypertrophyStrengtheningProgramCreationPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
