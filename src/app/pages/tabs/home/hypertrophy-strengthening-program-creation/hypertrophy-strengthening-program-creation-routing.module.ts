import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { HypertrophyStrengtheningProgramCreationPage } from './hypertrophy-strengthening-program-creation.page';

const routes: Routes = [
  {
    path: '',
    component: HypertrophyStrengtheningProgramCreationPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class HypertrophyStrengtheningProgramCreationPageRoutingModule {}
