import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { HypertrophyStrengtheningProgramCreationPageRoutingModule } from './hypertrophy-strengthening-program-creation-routing.module';

import { HypertrophyStrengtheningProgramCreationPage } from './hypertrophy-strengthening-program-creation.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    HypertrophyStrengtheningProgramCreationPageRoutingModule
  ],
  declarations: [HypertrophyStrengtheningProgramCreationPage]
})
export class HypertrophyStrengtheningProgramCreationPageModule {}
