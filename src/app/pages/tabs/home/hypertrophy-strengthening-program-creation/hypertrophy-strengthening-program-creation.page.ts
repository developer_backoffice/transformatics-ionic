import { Component, OnInit } from "@angular/core";

@Component({
  selector: "app-hypertrophy-strengthening-program-creation",
  templateUrl: "./hypertrophy-strengthening-program-creation.page.html",
  styleUrls: ["./hypertrophy-strengthening-program-creation.page.scss"],
})
export class HypertrophyStrengtheningProgramCreationPage implements OnInit {
  showListItem = false;

  slideOptions = {
    // centeredSlides: true,
    slidesPerView: 2.5,
    spaceBetween: 10,
  };

  btnSlideOptions = {
    // centeredSlides: true,
    slidesPerView: 3,
    spaceBetween: 5,
  };

  objectif: any = "Hypertrophie";
  format_dentrainement: any = "SPLIT";
  training_days: any = "3";
  progression_method = "Aucune";
  start_the_program = "Aujourd’hui";

  btnArray = [
    {
      text: 1,
      active: true
    },
    {
      text: 2,
      active: false
    },
    {
      text: 3,
      active: false

    },
  ];

  filteredData = {};

  constructor() {}

  ngOnInit() {
    this.btnArray.map((doc) => {
      if(doc.active) {
        this.filteredData = doc;
      }
    })

    console.log(this.filteredData)
  }

  toggleChange(event) {
    console.log(event);

    this.showListItem = event.detail.checked;
  }

  addBtn(slides) {
    if(!(this.btnArray.length >= 7)) {
      let text = 2;
      let arr = this.btnArray[this.btnArray.length - 1];
      let obj = { text: arr.text + 1, active: false };
      this.btnArray.push(obj);
      slides.update();
      slides.slideTo(this.btnArray.length + 1);
      setTimeout(() => {
        slides.slideTo(this.btnArray.length + 2);
      });
    }
  }

  public onItemReorder({ detail }) {
    detail.complete(true);
  }


  makeActive(item) {
    console.log(item)
    item.active = true;
    let filteredData = [];


  this.btnArray.forEach((doc) => {
     if(item.text != doc.text) {
        doc.active = false;
     }
     filteredData.push(doc)
     
    })


    this.btnArray = filteredData;

    this.btnArray.map((doc) => {
      if(doc.active) {
        this.filteredData = doc;
      }
    })

  }
}
