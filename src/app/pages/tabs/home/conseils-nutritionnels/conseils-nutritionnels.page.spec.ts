import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { ConseilsNutritionnelsPage } from './conseils-nutritionnels.page';

describe('ConseilsNutritionnelsPage', () => {
  let component: ConseilsNutritionnelsPage;
  let fixture: ComponentFixture<ConseilsNutritionnelsPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ConseilsNutritionnelsPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(ConseilsNutritionnelsPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
