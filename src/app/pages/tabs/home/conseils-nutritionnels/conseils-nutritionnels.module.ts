import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { ConseilsNutritionnelsPageRoutingModule } from './conseils-nutritionnels-routing.module';

import { ConseilsNutritionnelsPage } from './conseils-nutritionnels.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ConseilsNutritionnelsPageRoutingModule
  ],
  declarations: [ConseilsNutritionnelsPage]
})
export class ConseilsNutritionnelsPageModule {}
