import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { SelectBodyGroupsPageRoutingModule } from './select-body-groups-routing.module';

import { SelectBodyGroupsPage } from './select-body-groups.page';
import { SharedModule } from '@components/shared.module';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    SelectBodyGroupsPageRoutingModule,
    SharedModule
  ],
  declarations: [SelectBodyGroupsPage]
})
export class SelectBodyGroupsPageModule {}
