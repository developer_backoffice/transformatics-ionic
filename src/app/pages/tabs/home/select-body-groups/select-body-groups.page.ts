import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-select-body-groups',
  templateUrl: './select-body-groups.page.html',
  styleUrls: ['./select-body-groups.page.scss'],
})
export class SelectBodyGroupsPage implements OnInit {

  face: "front" | "back" = "front";
  constructor() { }

  ngOnInit() {
  }

  flipImage() {
    console.log("clicked")
    if (this.face === "front") {
      this.face = "back";
    } else {
      this.face = "front";
    }
  }

}
