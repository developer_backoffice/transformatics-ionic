import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { SelectBodyGroupsPage } from './select-body-groups.page';

const routes: Routes = [
  {
    path: '',
    component: SelectBodyGroupsPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class SelectBodyGroupsPageRoutingModule {}
