import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { SelectBodyGroupsPage } from './select-body-groups.page';

describe('SelectBodyGroupsPage', () => {
  let component: SelectBodyGroupsPage;
  let fixture: ComponentFixture<SelectBodyGroupsPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SelectBodyGroupsPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(SelectBodyGroupsPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
