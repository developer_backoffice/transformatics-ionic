import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { AlimentsDetailsPageRoutingModule } from './aliments-details-routing.module';

import { AlimentsDetailsPage } from './aliments-details.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    AlimentsDetailsPageRoutingModule
  ],
  declarations: [AlimentsDetailsPage]
})
export class AlimentsDetailsPageModule {}
