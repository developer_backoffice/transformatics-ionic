import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { AlimentsDetailsPage } from './aliments-details.page';

const routes: Routes = [
  {
    path: '',
    component: AlimentsDetailsPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class AlimentsDetailsPageRoutingModule {}
