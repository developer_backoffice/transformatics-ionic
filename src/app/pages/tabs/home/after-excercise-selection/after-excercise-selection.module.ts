import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { AfterExcerciseSelectionPageRoutingModule } from './after-excercise-selection-routing.module';

import { AfterExcerciseSelectionPage } from './after-excercise-selection.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    AfterExcerciseSelectionPageRoutingModule
  ],
  declarations: [AfterExcerciseSelectionPage]
})
export class AfterExcerciseSelectionPageModule {}
