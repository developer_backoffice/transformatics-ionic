import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { AfterExcerciseSelectionPage } from './after-excercise-selection.page';

describe('AfterExcerciseSelectionPage', () => {
  let component: AfterExcerciseSelectionPage;
  let fixture: ComponentFixture<AfterExcerciseSelectionPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AfterExcerciseSelectionPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(AfterExcerciseSelectionPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
