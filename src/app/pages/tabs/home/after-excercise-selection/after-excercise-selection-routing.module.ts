import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { AfterExcerciseSelectionPage } from './after-excercise-selection.page';

const routes: Routes = [
  {
    path: '',
    component: AfterExcerciseSelectionPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class AfterExcerciseSelectionPageRoutingModule {}
