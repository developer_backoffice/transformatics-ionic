import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { AddExcerciseForMenuPageRoutingModule } from './add-excercise-for-menu-routing.module';

import { AddExcerciseForMenuPage } from './add-excercise-for-menu.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    AddExcerciseForMenuPageRoutingModule
  ],
  declarations: [AddExcerciseForMenuPage]
})
export class AddExcerciseForMenuPageModule {}
