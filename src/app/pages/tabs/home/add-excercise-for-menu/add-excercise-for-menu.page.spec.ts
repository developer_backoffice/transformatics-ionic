import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { AddExcerciseForMenuPage } from './add-excercise-for-menu.page';

describe('AddExcerciseForMenuPage', () => {
  let component: AddExcerciseForMenuPage;
  let fixture: ComponentFixture<AddExcerciseForMenuPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AddExcerciseForMenuPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(AddExcerciseForMenuPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
