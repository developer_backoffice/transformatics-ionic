import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { AddExcerciseForMenuPage } from './add-excercise-for-menu.page';

const routes: Routes = [
  {
    path: '',
    component: AddExcerciseForMenuPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class AddExcerciseForMenuPageRoutingModule {}
