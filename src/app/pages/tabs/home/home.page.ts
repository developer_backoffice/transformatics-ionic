import { ViewChild,NgZone } from '@angular/core';
import { Component, OnInit, AfterViewInit } from '@angular/core';
import { NavigationEnd, Router } from '@angular/router';
import { CheckUpModalsComponent } from '@components/modals/check-up-modals/check-up-modals.component';
import { CoursesModalComponent } from '@components/modals/courses-modal/courses-modal.component';
import { ManualRegistrationComponent } from '@components/modals/manual-registration/manual-registration.component';
import { NotebookModalComponent } from '@components/modals/notebook-modal/notebook-modal.component';
import { MyDietHomeModalComponent } from '@components/modals/my-diet-home-modal/my-diet-home-modal.component';
import { ModalController } from '@ionic/angular';
import { Subscription } from 'rxjs';
import { UserdataService } from '../../../services/userdata.service';
import { CalendarModal } from './../../../components/modals/calendar-modal/calendar-modal';
import {IonContent} from '@ionic/angular';

@Component({
  selector: 'app-home',
  templateUrl: './home.page.html',
  styleUrls: ['./home.page.scss'],
})
export class HomePage implements OnInit, AfterViewInit {

  creationMode = "deux"
  plansBtn = "Gain muscle";
  private lastY = 0;
  private hidden: boolean = false;
  private triggerDistance: number = 10;
  segmentModel = "training";
  show: boolean;
  detail: boolean = false;
  startDate: Date;
  days: any = [];
  @ViewChild('myslide') slider;
  eauMinRange = 0;
  eauChangeRange = 20;
  sommeilMinRange = 0;
  sommeilChangeRange = 15;
  routerSubscription: Subscription
  options = {
    centeredSlides: true,
    slidesPerView: 1,
    spaceBetween: 0,
    initialSlide: 5,
    on: {
      beforeInit() {
        const swiper = this;
        swiper.classNames.push(`${swiper.params.containerModifierClass}fade`);
        const overwriteParams = {
          slidesPerView: 1,
          slidesPerColumn: 1,
          slidesPerGroup: 1,
          watchSlidesProgress: true,
          spaceBetween: 0,
          virtualTranslate: true,
        };
        swiper.params = Object.assign(swiper.params, overwriteParams);
        swiper.params = Object.assign(swiper.originalParams, overwriteParams);
      },
      setTranslate() {
        const swiper = this;
        const { slides } = swiper;
        for (let i = 0; i < slides.length; i += 1) {
          const $slideEl = swiper.slides.eq(i);
          const offset$$1 = $slideEl[0].swiperSlideOffset;
          let tx = -offset$$1;
          if (!swiper.params.virtualTranslate) tx -= swiper.translate;
          let ty = 0;
          if (!swiper.isHorizontal()) {
            ty = tx;
            tx = 0;
          }
          const slideOpacity = swiper.params.fadeEffect.crossFade
            ? Math.max(1 - Math.abs($slideEl[0].progress), 0)
            : 1 + Math.min(Math.max($slideEl[0].progress, -1), 0);
          $slideEl
            .css({
              opacity: slideOpacity,
            })
            .transform(`translate3d(${tx}px, ${ty}px, 0px)`);
        }
      },
      setTransition(duration) {
        const swiper = this;
        const { slides, $wrapperEl } = swiper;
        slides.transition(duration);
        if (swiper.params.virtualTranslate && duration !== 0) {
          let eventTriggered = false;
          slides.transitionEnd(() => {
            if (eventTriggered) return;
            if (!swiper || swiper.destroyed) return;
            eventTriggered = true;
            swiper.animating = false;
            const triggerEvents = ['webkitTransitionEnd', 'transitionend'];
            for (let i = 0; i < triggerEvents.length; i += 1) {
              $wrapperEl.trigger(triggerEvents[i]);
            }
          });
        }
      },
    }
  };


  constructor(public userData: UserdataService, public modalCtrl: ModalController, private router: Router,private zone: NgZone) { 
    this.routerWatch();
  }

  ngOnInit() {
    const date = new Date();
    this.days = this.getDaysInMonth(date.getMonth(), date.getFullYear());
    console.log(this.slider);
  }

  ionViewDidEnter() {
    console.log("cc", this.userData.showActivity);
    this.show = this.userData.showActivity;
  }


  ionViewWillEnter() {
    console.log("cc", this.userData.showActivity);
    this.show = this.userData.showActivity;
  }


  async openCheckupModal() {
    const modal = await this.modalCtrl.create({
      component: CheckUpModalsComponent,
      cssClass: 'check-up-modal',
      showBackdrop: false,
      // mode: "ios",
    });
    return await modal.present();
  }



  
  async openNotebookModal() {
    const modal = await this.modalCtrl.create({
      component: NotebookModalComponent,
      cssClass: 'check-up-modal',
      showBackdrop: false,
      backdropDismiss: true,
      // mode: "ios",
    });
    return await modal.present();
  }



  async openCourseModal() {
    const modal = await this.modalCtrl.create({
      component: CoursesModalComponent,
      cssClass: 'check-up-modal',
      showBackdrop: false,
      // mode: "ios",
    });
    return await modal.present();
  }



  async openMyDietHomeModal() {
    const modal = await this.modalCtrl.create({
      component: MyDietHomeModalComponent,
      cssClass: 'check-up-modal',
      showBackdrop: false,
      // mode: "ios",
    });
    return await modal.present();
  }

  eauChange(event) {
    this.eauChangeRange = event.detail.value
  }
  sommeilChange(event) {
    this.sommeilChangeRange = event.detail.value
  }

  ngAfterViewInit() {
    this.goToSlide();
  }
  segmentChanged(event) {
    console.log(this.segmentModel);

    console.log(event);
  }

  showDetail() {
    this.detail = !this.detail;
  }
  async openCalendar() {
    const modal = await this.modalCtrl.create({
      component: CalendarModal,
      cssClass: 'calendar-style',
      backdropDismiss: true,
      showBackdrop: false,
      componentProps: {
        'startDate': new Date(),
      }
    });
    return await modal.present();
  }
  /**
   * @param {number} The month number, 0 based
   * @param {number} The year, not zero based, required to account for leap years
   * @return {Date[]} List with date objects for each day of the month
   */
  getDaysInMonth(month: number, year: number) {
    const date = new Date(year, month, 1);
    const days = [];
    const today = new Date();
    while (date.getMonth() === month) {
      days.push({ date: new Date(date) });
      date.setDate(date.getDate() + 1);
    }

    days.forEach((day, index) => {
      const date = new Date(day.date);
      // days[index]["monthName"] = date.toLocaleString("default", {
      //   month: "short",
      // });
      days[index]["dayName"] = date.toLocaleString("default", {
        weekday: "short",
      });
      days[index]["day"] = date.toLocaleString("default", { day: "2-digit" });
      // days[index]["data"] = this.getRandomInt();
      // days[index]["color"] = "#ff9900";
      if (date.getDate() === today.getDate()) {
        days[index]["isToday"] = true;
        days[index]["dayName"] = 'Today';
      } else {
        days[index]["isToday"] = false;
      }
    });
    console.log("days", days);
    return days;
  }
  async goToSlide() {

    let currentIndex = await this.slider.getActiveIndex();

    let index = this.days.findIndex(x => x.isToday === true);

    this.slider.slideTo(index, 1000);
  }
  async goBack() {
    let currentIndex = await this.slider.getActiveIndex();
    this.slider.slideTo(currentIndex - 1, 1000);
  }
  async goAhead() {
    let currentIndex = await this.slider.getActiveIndex();
    this.slider.slideTo(currentIndex + 1, 1000);
  }

  routerWatch() {
    this.routerSubscription = this.router.events.subscribe(
      (event: NavigationEnd) => {
        if (event instanceof NavigationEnd) {
          if (event.url == '/tabs/home') {
            console.log(111)
            this.show = this.userData.showActivity;
          }
        }
      }
    );
  }

  ionPageWillLeave() {
    this.routerSubscription.unsubscribe();
  }

  scrollHandler(event) {
    this.zone.run(() => {

      console.log("event",event)
      let a = 0;
      let elem1 = document.getElementById('chart');
      let elem = document.querySelector('ion-tab-bar');
      // elem.style.setProperty('transition',"0.5s");
      if (event.detail.scrollTop > a) {
          // this.hide();
         
          // elem1.style.setProperty('display',"none");
          elem.style.setProperty('margin-bottom',"-100px");
        // event.detail.scrollTop = event.detail.scrollTop
        } else {
        // this.show1();
        // elem1.style.setProperty('display',"block");
        elem.style.setProperty('margin-bottom',"10px");
        }
        a = event.detail.scrollTop;
           
    });
  }

}
