import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { PhysicalActivityCreationMenuPageRoutingModule } from './physical-activity-creation-menu-routing.module';

import { PhysicalActivityCreationMenuPage } from './physical-activity-creation-menu.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    PhysicalActivityCreationMenuPageRoutingModule
  ],
  declarations: [PhysicalActivityCreationMenuPage]
})
export class PhysicalActivityCreationMenuPageModule {}
