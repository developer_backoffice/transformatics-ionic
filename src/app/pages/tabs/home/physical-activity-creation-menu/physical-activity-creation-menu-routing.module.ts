import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { PhysicalActivityCreationMenuPage } from './physical-activity-creation-menu.page';

const routes: Routes = [
  {
    path: '',
    component: PhysicalActivityCreationMenuPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class PhysicalActivityCreationMenuPageRoutingModule {}
