import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AddExcerciseWoodModalComponent } from '@components/modals/add-excercise-wood-modal/add-excercise-wood-modal.component';
import { ModalController } from '@ionic/angular';

@Component({
  selector: 'app-physical-activity-creation-menu',
  templateUrl: './physical-activity-creation-menu.page.html',
  styleUrls: ['./physical-activity-creation-menu.page.scss'],
})
export class PhysicalActivityCreationMenuPage implements OnInit {
  creationMode = "manual"
  plansBtn = "program";
  firstPage = true;
  plansBtn2 = "program"
  programmeSelect = ""
  constructor(
    private modal: ModalController,
    private router: Router
  ) { }

  ngOnInit() {
  }


  async openModal() {
    const modal = await this.modal.create({
      component: AddExcerciseWoodModalComponent,
      cssClass: 'centerd-modal-wood-excercise',
      showBackdrop: false,
      // mode: "ios",
    });
    return await modal.present();
  }


  redirectPageProgram() {
    console.log("programSelect", this.programmeSelect)
    console.log("creationMode", this.creationMode)
    if (this.creationMode == 'manual' && this.programmeSelect == 'masculation') {

      this.router.navigate(['/hypertrophy-strengthening-program-creation']);
    } else if (this.creationMode == 'automatic' && this.programmeSelect == 'masculation') {

      this.router.navigate(['/musculation-auto-seances-programms', 'physical-activity-creation-menu'])
    } else if (this.creationMode == 'manual' && this.programmeSelect == 'crossfit') {

      this.router.navigate(['/physical-activity-menu-empty'])
    } else if (this.creationMode == 'automatic' && this.programmeSelect == 'crossfit') {
      
      this.router.navigate(['/musculation-auto-seances-programms', 'physical-activity-creation-menu'])
    }
  }
}
