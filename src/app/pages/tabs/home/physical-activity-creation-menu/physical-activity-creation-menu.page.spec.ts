import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { PhysicalActivityCreationMenuPage } from './physical-activity-creation-menu.page';

describe('PhysicalActivityCreationMenuPage', () => {
  let component: PhysicalActivityCreationMenuPage;
  let fixture: ComponentFixture<PhysicalActivityCreationMenuPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PhysicalActivityCreationMenuPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(PhysicalActivityCreationMenuPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
