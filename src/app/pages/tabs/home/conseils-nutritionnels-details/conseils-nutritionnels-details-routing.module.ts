import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ConseilsNutritionnelsDetailsPage } from './conseils-nutritionnels-details.page';

const routes: Routes = [
  {
    path: '',
    component: ConseilsNutritionnelsDetailsPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ConseilsNutritionnelsDetailsPageRoutingModule {}
