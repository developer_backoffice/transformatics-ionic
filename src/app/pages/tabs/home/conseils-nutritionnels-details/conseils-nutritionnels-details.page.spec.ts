import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { ConseilsNutritionnelsDetailsPage } from './conseils-nutritionnels-details.page';

describe('ConseilsNutritionnelsDetailsPage', () => {
  let component: ConseilsNutritionnelsDetailsPage;
  let fixture: ComponentFixture<ConseilsNutritionnelsDetailsPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ConseilsNutritionnelsDetailsPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(ConseilsNutritionnelsDetailsPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
