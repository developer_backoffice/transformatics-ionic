import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { ConseilsNutritionnelsDetailsPageRoutingModule } from './conseils-nutritionnels-details-routing.module';

import { ConseilsNutritionnelsDetailsPage } from './conseils-nutritionnels-details.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ConseilsNutritionnelsDetailsPageRoutingModule
  ],
  declarations: [ConseilsNutritionnelsDetailsPage]
})
export class ConseilsNutritionnelsDetailsPageModule {}
