import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { AlimentsPage } from './aliments.page';

const routes: Routes = [
  {
    path: '',
    component: AlimentsPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class AlimentsPageRoutingModule {}
