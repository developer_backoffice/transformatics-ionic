import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { AlimentsPage } from './aliments.page';

describe('AlimentsPage', () => {
  let component: AlimentsPage;
  let fixture: ComponentFixture<AlimentsPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AlimentsPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(AlimentsPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
