import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { AlimentsPageRoutingModule } from './aliments-routing.module';

import { AlimentsPage } from './aliments.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    AlimentsPageRoutingModule
  ],
  declarations: [AlimentsPage]
})
export class AlimentsPageModule {}
