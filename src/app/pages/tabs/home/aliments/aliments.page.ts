import { Component, OnInit } from '@angular/core';
import { ModalController } from '@ionic/angular';
import {RecipesFiltersComponent} from '@components/modals/recipes-filters/recipes-filters.component';
@Component({
  selector: 'app-aliments',
  templateUrl: './aliments.page.html',
  styleUrls: ['./aliments.page.scss'],
})
export class AlimentsPage implements OnInit {
  segmentModel = "aliments";
  constructor(private modalCtrl: ModalController) { }

  showMore = false;
  ngOnInit() {
  }
  segmentChanged(event){
    console.log(this.segmentModel);
    
    console.log(event);
  }




  async openModal() {
    const modal = await this.modalCtrl.create({
      component: RecipesFiltersComponent,
      cssClass: 'aliments-modal',
      showBackdrop: false,
      // mode: "ios",
    });
    return await modal.present();
  }
}
