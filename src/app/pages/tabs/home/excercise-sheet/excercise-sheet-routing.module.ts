import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ExcerciseSheetPage } from './excercise-sheet.page';

const routes: Routes = [
  {
    path: '',
    component: ExcerciseSheetPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ExcerciseSheetPageRoutingModule {}
