import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { CreateASupersetComponent } from '@components/create-a-superset/create-a-superset.component';
import { ModalController, NavController } from '@ionic/angular';
import { UserdataService } from '../../../../services/userdata.service';

@Component({
  selector: 'app-excercise-sheet',
  templateUrl: './excercise-sheet.page.html',
  styleUrls: ['./excercise-sheet.page.scss'],
})
export class ExcerciseSheetPage implements OnInit {
  segmentModel = "training";

  constructor(
    private modal: ModalController,
    public userData: UserdataService,
    private route: Router,
    private nav: NavController
  ) { }

  ngOnInit() {
  }

  segmentChanged(event) {

  }

  updateService() {

    this.userData.showActivity = true;
    console.log(this.userData.showActivity)
    this.nav.pop();
    this.nav.navigateRoot("tabs/home")
    this.nav.navigateForward("tabs/home")
    this.route.navigate(['/tabs/home'], {replaceUrl:true})
    .then(() => this.route.navigate(['/tabs/home']));
  }

  async openCreateSupersetModal() {
    const modal = await this.modal.create({
      component: CreateASupersetComponent,
      cssClass: 'create-a-superuser-modal'
    });
    return await modal.present();
  }

}
