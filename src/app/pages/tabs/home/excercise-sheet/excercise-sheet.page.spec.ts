import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { ExcerciseSheetPage } from './excercise-sheet.page';

describe('ExcerciseSheetPage', () => {
  let component: ExcerciseSheetPage;
  let fixture: ComponentFixture<ExcerciseSheetPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ExcerciseSheetPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(ExcerciseSheetPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
