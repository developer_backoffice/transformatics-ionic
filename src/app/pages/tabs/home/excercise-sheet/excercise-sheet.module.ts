import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { ExcerciseSheetPageRoutingModule } from './excercise-sheet-routing.module';

import { ExcerciseSheetPage } from './excercise-sheet.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ExcerciseSheetPageRoutingModule
  ],
  declarations: [ExcerciseSheetPage]
})
export class ExcerciseSheetPageModule {}
