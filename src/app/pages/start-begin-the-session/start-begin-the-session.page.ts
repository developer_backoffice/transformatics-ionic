import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-start-begin-the-session',
  templateUrl: './start-begin-the-session.page.html',
  styleUrls: ['./start-begin-the-session.page.scss'],
})
export class StartBeginTheSessionPage implements OnInit {

  constructor() { }

  ngOnInit() {
  }
  categories = {
    slidesPerView: 3.5,
    effect: 'flip',
    slidesPerColumn: 1,
    slidesPerGroup: 1,
    watchSlidesProgress: false,
    spaceBetween: 10,
  };
}
