import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { StartBeginTheSessionPage } from './start-begin-the-session.page';

describe('StartBeginTheSessionPage', () => {
  let component: StartBeginTheSessionPage;
  let fixture: ComponentFixture<StartBeginTheSessionPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ StartBeginTheSessionPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(StartBeginTheSessionPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
