import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { StartBeginTheSessionPage } from './start-begin-the-session.page';

const routes: Routes = [
  {
    path: '',
    component: StartBeginTheSessionPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class StartBeginTheSessionPageRoutingModule {}
