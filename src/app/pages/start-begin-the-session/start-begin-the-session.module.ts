import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { StartBeginTheSessionPageRoutingModule } from './start-begin-the-session-routing.module';

import { StartBeginTheSessionPage } from './start-begin-the-session.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    StartBeginTheSessionPageRoutingModule
  ],
  declarations: [StartBeginTheSessionPage]
})
export class StartBeginTheSessionPageModule {}
