import { ModalController } from '@ionic/angular';
import { PaymentModalComponent } from '../../components/modals/payment-modal/payment-modal.component';
import { Component, OnInit } from '@angular/core';
import { Router,ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-select-plan',
  templateUrl: './select-plan.page.html',
  styleUrls: ['./select-plan.page.scss'],
})
export class SelectPlanPage implements OnInit {
  planSlideOptions = {
    centeredSlides: true,
    slidesPerView: 1.2,
    spaceBetween: 10,
  }
  constructor(
    public router: Router,
    public route: ActivatedRoute,
    public modal: ModalController
  ) { }

  ngOnInit() {
  }

  goToBodyScan() {
    console.log("cdsc");
    this.router.navigate(['body-scan-steps']);
  }

  async openPaymentModal() {
    const modal = await this.modal.create({
      component: PaymentModalComponent,
      cssClass: 'check-up-modal',
      showBackdrop: false,
    });
    return await modal.present();
  }


}
