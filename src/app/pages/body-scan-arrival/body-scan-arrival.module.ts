import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { BodyScanArrivalPageRoutingModule } from './body-scan-arrival-routing.module';

import { BodyScanArrivalPage } from './body-scan-arrival.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    BodyScanArrivalPageRoutingModule
  ],
  declarations: [BodyScanArrivalPage]
})
export class BodyScanArrivalPageModule {}
