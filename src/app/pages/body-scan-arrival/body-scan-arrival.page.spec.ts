import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { BodyScanArrivalPage } from './body-scan-arrival.page';

describe('BodyScanArrivalPage', () => {
  let component: BodyScanArrivalPage;
  let fixture: ComponentFixture<BodyScanArrivalPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BodyScanArrivalPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(BodyScanArrivalPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
