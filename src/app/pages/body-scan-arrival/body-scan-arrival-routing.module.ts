import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { BodyScanArrivalPage } from './body-scan-arrival.page';

const routes: Routes = [
  {
    path: '',
    component: BodyScanArrivalPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class BodyScanArrivalPageRoutingModule {}
