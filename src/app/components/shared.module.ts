import { PaymentModalComponent } from './modals/payment-modal/payment-modal.component';
import { RouterModule } from '@angular/router';
import { BrowserModule, HAMMER_GESTURE_CONFIG, HammerGestureConfig, HammerModule } from '@angular/platform-browser';
import { CheckUpsProfileComponent } from './check-ups-profile/check-ups-profile.component';
import { PhysiologyProfileComponent } from './physiology-profile/physiology-profile.component';
import { ChartBarComponent } from './chart-bar/chart-bar.component';
import { BodyScanStepFiveComponent } from './body-scan-step-five/body-scan-step-five.component';
import { BodyScanStepFourComponent } from './body-scan-step-four/body-scan-step-four.component';
import { BodyScanStepThreeComponent } from './body-scan-step-three/body-scan-step-three.component';
import { BodyScanStepTwoComponent } from './body-scan-step-two/body-scan-step-two.component';
import { BodyScanStepOneComponent } from './body-scan-step-one/body-scan-step-one.component';
import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HeaderComponent } from './header/header.component';
import { TranslateModule } from '@ngx-translate/core';
import { PerformanceProfileComponent } from './performance-profile/performance-profile.component';
import { RecordsProfileComponent } from './records-profile/records-profile.component';
import { ChartComponent } from './chart/chart.component';
import { ChartsCheckupComponent } from './charts-checkup/charts-checkup.component';
import { BookComponent } from './book/book.component';
import { CheckUpModalsComponent } from './modals/check-up-modals/check-up-modals.component';
import { CoursesModalComponent } from './modals/courses-modal/courses-modal.component';
import { MyDietModalComponent } from './modals/my-diet-modal/my-diet-modal.component';
import { ManualRegistrationComponent } from './modals/manual-registration/manual-registration.component';
import { CalendarModal } from './modals/calendar-modal/calendar-modal';
import { NgCalendarModule } from 'ionic2-calendar';
import { CalendarModule } from 'ion2-calendar';
import { BodysvgComponent } from './bodysvgfront/bodysvg.component';
import { BodysvgbackComponent } from './bodysvgback/bodysvgback.component';
import { NotebookModalComponent } from './modals/notebook-modal/notebook-modal.component';
import { MyDevicesModalComponent } from './modals/my-devices-modal/my-devices-modal.component';
import { SponsershipModalComponent } from './modals/sponsership-modal/sponsership-modal.component';
import { SwiperCardComponent } from './swiper-card/swiper-card.component';
import 'hammerjs';
import { MyDietHomeModalComponent } from './modals/my-diet-home-modal/my-diet-home-modal.component';

import { CoachPaymentVisibilityComponent } from './coach-payment-visibility/coach-payment-visibility.component';
import { CoachChatModalComponent } from '@components/modals/coach-chat-modal/coach-chat-modal.component';
import { CoachCheckupModalComponent } from '@components/modals/coach-checkup-modal/coach-checkup-modal.component';
import { CoachNewRequestComponent } from '@components/modals/coach-new-request/coach-new-request.component';
import { CoachBodyScanStepOneComponent } from './coach/coach-body-scan-step-one/coach-body-scan-step-one.component';
import { CoachBodyScanStepTwoComponent } from './coach/coach-body-scan-step-two/coach-body-scan-step-two.component';
import { CoachBodyScanStepThreeComponent } from './coach/coach-body-scan-step-three/coach-body-scan-step-three.component';
import { RecipesFiltersComponent } from './modals/recipes-filters/recipes-filters.component';
import { MusculationAutoSeancesComponent } from './musculation-auto-seances/musculation-auto-seances.component';
import { MusculationAutoSeancesSessionComponent } from './musculation-auto-seances-session/musculation-auto-seances-session.component';

import { ChartOneComponent } from './coach/chart-one/chart-one.component';
import { ChartTwoComponent } from './coach/chart-two/chart-two.component';
import { AddExcerciseWoodModalComponent } from './modals/add-excercise-wood-modal/add-excercise-wood-modal.component';
import { ClickAddExcerciseModalComponent } from './modals/click-add-excercise-modal/click-add-excercise-modal.component';
import { BeforeAlimentsComponent } from './modals/before-aliments/before-aliments.component';




@NgModule({
  schemas: [CUSTOM_ELEMENTS_SCHEMA],
  declarations: [
    HeaderComponent,
    BodyScanStepOneComponent,
    BodyScanStepTwoComponent,
    BodyScanStepThreeComponent,
    BodyScanStepFourComponent,
    BodyScanStepFiveComponent,
    ChartBarComponent,
    PerformanceProfileComponent,
    PhysiologyProfileComponent,
    RecordsProfileComponent,
    CheckUpsProfileComponent,
    ChartComponent,
    ChartsCheckupComponent,
    BookComponent,
    CalendarModal,
    CheckUpModalsComponent,
    CoursesModalComponent,
    MyDietModalComponent,
    ManualRegistrationComponent,
    BodysvgComponent,
    BodysvgbackComponent,
    NotebookModalComponent,
    MyDevicesModalComponent,
    SponsershipModalComponent,
    SwiperCardComponent,
    MyDietHomeModalComponent,
    MyDietHomeModalComponent,
    PaymentModalComponent,

    CoachPaymentVisibilityComponent,
    CoachChatModalComponent,
    CoachCheckupModalComponent,
    CoachNewRequestComponent,

    CoachBodyScanStepOneComponent,
    CoachBodyScanStepTwoComponent,
    CoachBodyScanStepThreeComponent,
    RecipesFiltersComponent,
    MusculationAutoSeancesComponent,
    MusculationAutoSeancesSessionComponent,
    ChartOneComponent,
    ChartTwoComponent,
    AddExcerciseWoodModalComponent,
    ClickAddExcerciseModalComponent,
    BeforeAlimentsComponent
  ],
  imports: [
    CommonModule,
    TranslateModule,
    NgCalendarModule,
    CalendarModule,

    RouterModule
  ],
  exports: [
    HeaderComponent,
    BodyScanStepOneComponent,
    BodyScanStepTwoComponent,
    BodyScanStepThreeComponent,
    BodyScanStepFourComponent,
    BodyScanStepFiveComponent,
    ChartBarComponent,
    PerformanceProfileComponent,
    PhysiologyProfileComponent,
    RecordsProfileComponent,
    CheckUpsProfileComponent,
    ChartComponent,
    ChartsCheckupComponent,
    BookComponent,
    CheckUpModalsComponent,
    CoursesModalComponent,
    MyDietModalComponent,
    ManualRegistrationComponent,
    CalendarModal,
    BodysvgComponent,
    BodysvgbackComponent,
    NotebookModalComponent,
    MyDevicesModalComponent,
    SponsershipModalComponent,
    SwiperCardComponent,
    MyDietHomeModalComponent,

    PaymentModalComponent,
    CoachPaymentVisibilityComponent,
    CoachChatModalComponent,
    CoachCheckupModalComponent,
    CoachNewRequestComponent,

    PaymentModalComponent,
    CoachBodyScanStepOneComponent,
    CoachBodyScanStepTwoComponent,
    CoachBodyScanStepThreeComponent,
    PaymentModalComponent,
    RecipesFiltersComponent,
    MusculationAutoSeancesComponent,
    MusculationAutoSeancesSessionComponent,
    ChartOneComponent,
    ChartTwoComponent,
    AddExcerciseWoodModalComponent,
    ClickAddExcerciseModalComponent,
    BeforeAlimentsComponent

  ],
  providers: [
    BodyScanStepOneComponent
  ]
})
export class SharedModule { }
