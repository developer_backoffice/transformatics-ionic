import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { BodyScanStepOneComponent } from './body-scan-step-one.component';

describe('BodyScanStepOneComponent', () => {
  let component: BodyScanStepOneComponent;
  let fixture: ComponentFixture<BodyScanStepOneComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [BodyScanStepOneComponent],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(BodyScanStepOneComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
