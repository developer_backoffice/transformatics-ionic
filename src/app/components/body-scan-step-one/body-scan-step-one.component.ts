import { Component, Input, OnInit, Output, EventEmitter, OnChanges } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { CommondataService } from '@services/commondata.service';
import { ToasterService } from '@services/ionic/toast.service';

@Component({
  selector: 'app-body-scan-step-one',
  templateUrl: './body-scan-step-one.component.html',
  styleUrls: ['./body-scan-step-one.component.scss'],
})
export class BodyScanStepOneComponent implements OnInit {
  segmentModel = "favorites";

  @Output()
  stepOneSelected = new EventEmitter();

  @Input() stepOneForm: FormGroup;

  sportsData;

  gender = 'male';

  training_days = [
    {
      id: 1,
      name: 'Sommeil, position allongée',
      quantity: 0
    },
    {
      id: 2,
      name: 'Position assise',
      quantity: 0
    },
    {
      id: 3,
      name: 'Position debout, activité de faible intensité',
      quantity: 0
    },
    {
      id: 4,
      name: 'Activité d’intensité moyenne (marche)',
      quantity: 0
    },
    {
      id: 5,
      name: 'Activité à haute intensité (marche rapide, sport)',
      quantity: 0
    },
    {
      id: 6,
      name: 'Sport, activité intense',
      quantity: 0
    }
  ]

  days_off = [
    {
      id: 1,
      name: 'Sommeil, position allongée',
      quantity: 0
    },
    {
      id: 2,
      name: 'Position assise',
      quantity: 0
    },
    {
      id: 3,
      name: 'Position debout, activité de faible intensité',
      quantity: 0
    },
    {
      id: 4,
      name: 'Activité d’intensité moyenne (marche)',
      quantity: 0
    },
    {
      id: 5,
      name: 'Activité à haute intensité (marche rapide, sport)',
      quantity: 0
    },
    {
      id: 6,
      name: 'Sport, activité intense',
      quantity: 0
    }
  ]


  // select_sports = [
  //   {
  //     id: 1,
  //     icon: 'assets/icon/sports/Football.png',
  //     name: 'Football',
  //     isActive: false
  //   },
  //   {
  //     id: 2,
  //     icon: 'assets/icon/sports/Badminton.png',
  //     name: 'Badminton',
  //     isActive: false
  //   },
  //   {
  //     id: 3,
  //     icon: 'assets/icon/sports/Basketball.png',
  //     name: 'Basketball',
  //     isActive: false
  //   },
  //   {
  //     id: 4,
  //     icon: 'assets/icon/sports/Volleyball.png',
  //     name: 'Volleyball',
  //     isActive: false
  //   },
  //   {
  //     id: 5,
  //     icon: 'assets/icon/sports/Tennis.png',
  //     name: 'Tennis',
  //     isActive: false
  //   },
  //   {
  //     id: 6,
  //     icon: 'assets/icon/sports/Aquabike.png',
  //     name: 'Aquabike',
  //     isActive: false
  //   },
  //   {
  //     id: 7,
  //     icon: 'assets/icon/sports/Aquagym.png',
  //     name: 'Aquagym',
  //     isActive: false
  //   },
  //   {
  //     id: 8,
  //     icon: 'assets/icon/sports/Arts martiaux.png',
  //     name: 'Arts martiaux',
  //     isActive: false
  //   },
  //   {
  //     id: 9,
  //     icon: 'assets/icon/sports/Aviron.png',
  //     name: 'Aviron',
  //     isActive: false
  //   },
  //   {
  //     id: 10,
  //     icon: 'assets/icon/sports/Boxe.png',
  //     name: 'Boxe',
  //     isActive: false
  //   },
  //   {
  //     id: 12,
  //     icon: 'assets/icon/sports/Cours collectifs.png',
  //     name: 'Cours collectifs',
  //     isActive: false
  //   },
  //   {
  //     id: 13,
  //     icon: 'assets/icon/sports/Haltérophilie.png',
  //     name: 'Haltérophilie',
  //     isActive: false
  //   },
  //   {
  //     id: 14,
  //     icon: 'assets/icon/sports/Handball.png',
  //     name: 'Handball',
  //     isActive: false
  //   },
  //   {
  //     id: 15,
  //     icon: 'assets/icon/sports/Natation.png',
  //     name: 'Natation',
  //     isActive: false
  //   },
  //   {
  //     id: 16,
  //     icon: 'assets/icon/sports/Planche à voile.png',
  //     name: 'Planche à voile',
  //     isActive: false
  //   },
  //   {
  //     id: 17,
  //     icon: 'assets/icon/sports/Plongée.png',
  //     name: 'Plongée',
  //     isActive: false
  //   },
  //   {
  //     id: 18,
  //     icon: 'assets/icon/sports/Rugby.png',
  //     name: 'Rugby',
  //     isActive: false
  //   },
  //   {
  //     id: 19,
  //     icon: 'assets/icon/sports/Surf.png',
  //     name: 'Surf',
  //     isActive: false
  //   },
  //   {
  //     id: 20,
  //     icon: 'assets/icon/sports/Water polo.png',
  //     name: 'Water polo',
  //     isActive: false
  //   },

  //   {
  //     id: 21,
  //     icon: 'assets/icon/sports/Tennis de table.png',
  //     name: 'Tennis de table',
  //     isActive: false
  //   },

  //   {
  //     id: 22,
  //     icon: 'assets/icon/sports/Yoga.png',
  //     name: 'Tennis de table',
  //     isActive: false
  //   },

  // ]

  pressedBtnArray = []

  total_trainings_quantity: number;
  total_days_off_quantity: number;


  weightMinRange = 0;
  weightChangeRange = 200;

  heightMinRange = 0;
  heightChangeRange = 250;

  totalTrainingHours = 0;
  totalDaysHours = 0;

  selectedDate = "1994-12-15T13:47:20.789";

  constructor(
    private toast: ToasterService,
    private commonDataService: CommondataService
  ) {
    this.getSports();
  }

  async getSports() {
    let res = await this.commonDataService.getSportsData();
    if (res) {
      if (res['status']) {
        this.sportsData = res['data'];
        this.sportsData.forEach(element => {
          element['isActive'] = false;
        });
      }
    }

  }

  ngOnInit() {

    this.initializeForm();

    this.total_trainings_quantity = this.training_days.reduce((a, b) => {
      // return a.quantity + b;
      return a + b.quantity;
    }, 0);


    this.total_days_off_quantity = this.days_off.reduce((a, b) => {
      // return a.quantity + b;
      return a + b.quantity;
    }, 0);



    // console.log("sum", sum)



    //reduce training
    this.checkTotalTrainingHours()
    this.checkDaysHours()

  }

  initializeForm() {
    this.stepOneForm = new FormGroup({
      gender: new FormControl('male'),
      dob: new FormControl(this.selectedDate),
      width: new FormControl(''),
      height: new FormControl(''),
      favorites: new FormControl(),
      profile: new FormControl(),
      sports: new FormControl()
    });
    this.onChanges();
  }

  changeGender(value) {
    this.gender = value;
    this.stepOneForm.get('gender').setValue(value);
    // this.stepOneSelected.emit(value);
  }

  checkTotalTrainingHours() {
    this.totalTrainingHours = this.training_days.reduce(function (acc, obj) { return acc + obj.quantity; }, 0); // 7
    this.emitChanges();
    console.log("aaa", this.totalTrainingHours)
  }

  checkDaysHours() {
    this.totalDaysHours = this.days_off.reduce(function (acc, obj) { return acc + obj.quantity; }, 0); // 7
    this.emitChanges();
    console.log("aaa", this.totalTrainingHours)
  }


  heightChange(event) {
    this.heightChangeRange = event.detail.value
    this.stepOneForm.get('height').setValue(event.detail.value);
  }

  weightChange(event) {
    this.weightChangeRange = event.detail.value;
    this.stepOneForm.get('weight').setValue(event.detail.value);
  }

  segmentChanged(event) {
    console.log(this.segmentModel);

    console.log(event.detail.value);


    this.segmentModel = event.detail.value;
  }

  increase(id) {
    let filteredData = this.training_days.filter((item) => {
      if (item.id === id) {
        if (this.totalTrainingHours != 24) {

          if (item.quantity >= 24) {
            return item
          } else {
            item.quantity += 1;
          }
        } else {
          this.toast.present("Le nombre total d'heures ne doit pas dépasser 24")
          return item;
        }
      }

      return item;
    })

    console.log(filteredData)

    this.total_trainings_quantity = filteredData.reduce((a, b) => {
      // return a.quantity + b;
      return a + b.quantity;
    }, 0);


    this.training_days = filteredData;

    this.checkTotalTrainingHours()
  }

  decrease(id) {
    let filteredData = this.training_days.filter((item) => {
      if (item.id === id) {
        if (item.quantity <= 1) {
          item.quantity = 1;

          // return;
        } else {
          item.quantity -= 1;

        }

      }

      return item;
    })

    this.checkTotalTrainingHours()
    // this.total_trainings_quantity = filteredData.reduce((a, b) => {
    //   // return a.quantity + b;
    //   return a + b.quantity; 
    // }, 0);


    this.training_days = filteredData;
  }




  increaseDaysOff(id) {
    let filteredData = this.days_off.filter((item) => {
      if (item.id === id) {
        if (this.totalDaysHours != 24) {

          if (item.quantity >= 24) {
            return item
          } else {
            item.quantity += 1;
          }
        } else {
          this.toast.present("Le nombre total d'heures ne doit pas dépasser 24")
          return item;
        }
      }


      return item;
    })

    console.log(filteredData)
    // this.total_days_off_quantity = filteredData.reduce((a, b) => {
    //   // return a.quantity + b;
    //   return a + b.quantity; 
    // }, 0);



    this.days_off = filteredData;

    this.checkDaysHours()
  }

  decreaseDaysOff(id) {
    let filteredData = this.days_off.filter((item) => {
      if (item.id === id) {
        if (item.quantity <= 1) {
          item.quantity = 1;

          // return;
        } else {
          item.quantity -= 1;

        }

      }

      this.checkDaysHours()
      return item;
    })

    this.total_days_off_quantity = filteredData.reduce((a, b) => {
      // return a.quantity + b;
      return a + b.quantity;
    }, 0);


    this.days_off = filteredData;
  }


  clickSportButton(btn) {
    btn.isActive = !btn.isActive;
    // this.pressedBtnArray.push(btn)

    let newString = JSON.stringify(this.sportsData.filter((item) => { return item.isActive }));
    newString = newString.replace('"', '\"');
    this.stepOneForm.get('sports').setValue(newString);
    // this.stepOneForm.get('sports').setValue(JSON.stringify(this.select_sports.filter((item) => { return item.isActive })));
    this.emitChanges();
    // if(btn.isActive == 1) {
    //   btn.isActive = 0;
    // }

    // let filteredData = this.select_sports.filter((item) => {
    //   if (item.id == id) {
    //     item.isActive = 1;

    //   } else {
    //     item.isActive = 0;
    //   }

    //   return item;
    // })

    // this.select_sports = filteredData;

    // console.log(this.select_sports)

  }

  onChanges(): void {
    this.stepOneForm.valueChanges.subscribe(val => {
      // this.stepOneSelected.emit(JSON.stringify(val));
      this.emitChanges();
    });
  }

  emitChanges() {
    // console.log();
    let favorites = JSON.stringify(this.stepOneForm.value.favorites);
    let profile = JSON.stringify(this.stepOneForm.value.profile);
    this.stepOneForm.get('favorites').setValue(this.stepOneForm.value.favorites);
    this.stepOneForm.get('profile').setValue(this.stepOneForm.value.profile);
    this.stepOneSelected.emit(this.stepOneForm.value);
  }

  changeDOB(event) {
    this.stepOneForm.get('dob').setValue(event.detail.value);
  }

}
