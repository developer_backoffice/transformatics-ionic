import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-physiology-profile',
  templateUrl: './physiology-profile.component.html',
  styleUrls: ['./physiology-profile.component.scss'],
})
export class PhysiologyProfileComponent implements OnInit {
  physiologySegmentModel = "phote-de-face";

  constructor() { }

  ngOnInit() {}


  physiologySegmentChanged(event){
    console.log(this.physiologySegmentModel);
    console.log(this.physiologySegmentModel)
    console.log(event);
    this.physiologySegmentModel = event.detail.value;
  }


}
