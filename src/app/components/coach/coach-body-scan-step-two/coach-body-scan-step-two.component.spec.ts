import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { CoachBodyScanStepTwoComponent } from './coach-body-scan-step-two.component';

describe('CoachBodyScanStepTwoComponent', () => {
  let component: CoachBodyScanStepTwoComponent;
  let fixture: ComponentFixture<CoachBodyScanStepTwoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CoachBodyScanStepTwoComponent ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(CoachBodyScanStepTwoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
