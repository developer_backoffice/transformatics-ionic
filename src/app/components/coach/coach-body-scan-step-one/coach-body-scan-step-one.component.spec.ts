import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { CoachBodyScanStepOneComponent } from './coach-body-scan-step-one.component';

describe('CoachBodyScanStepOneComponent', () => {
  let component: CoachBodyScanStepOneComponent;
  let fixture: ComponentFixture<CoachBodyScanStepOneComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CoachBodyScanStepOneComponent ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(CoachBodyScanStepOneComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
