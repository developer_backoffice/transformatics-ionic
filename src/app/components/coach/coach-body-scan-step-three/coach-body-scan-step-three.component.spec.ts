import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { CoachBodyScanStepThreeComponent } from './coach-body-scan-step-three.component';

describe('CoachBodyScanStepThreeComponent', () => {
  let component: CoachBodyScanStepThreeComponent;
  let fixture: ComponentFixture<CoachBodyScanStepThreeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CoachBodyScanStepThreeComponent ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(CoachBodyScanStepThreeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
