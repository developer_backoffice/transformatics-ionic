import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-records-profile',
  templateUrl: './records-profile.component.html',
  styleUrls: ['./records-profile.component.scss'],
})
export class RecordsProfileComponent implements OnInit {
  segmentRMModel = "1rm";
  
  constructor() { }

  ngOnInit() {
  }
  segmentChanged(event){
    this.segmentRMModel = event.detail.value
    console.log("segmentRMModel", event);
  }
}