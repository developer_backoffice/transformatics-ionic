import { Component, OnInit } from '@angular/core';
import { ModalController } from '@ionic/angular';

@Component({
  selector: 'app-create-a-superset',
  templateUrl: './create-a-superset.component.html',
  styleUrls: ['./create-a-superset.component.scss'],
})
export class CreateASupersetComponent implements OnInit {

  constructor(
    private modal: ModalController
  ) { }

  typeOfSupersetBtnArray = ""
  yesNoBtnClick = ""
  ngOnInit() {}

  yesClick() {
    this.yesNoBtnClick ='yes'
    this.modal.dismiss()
  }

  noClick() {
    this.yesNoBtnClick ='no'
    this.modal.dismiss()

  }
}
