import { EventEmitter, Input,NgZone } from '@angular/core';
import { Output } from '@angular/core';
import { Component,OnInit,ElementRef,QueryList,ViewChildren,AfterViewInit} from "@angular/core";
import { GestureController, Platform } from "@ionic/angular";
// import { GestureCtrlService } from '@services/ionic/gesture-control.service';
import { IonCard } from "@ionic/angular";
import 'hammerjs';
@Component({
  selector: 'app-swiper-card',
  templateUrl: './swiper-card.component.html',
  styleUrls: ['./swiper-card.component.scss'],
})
export class SwiperCardComponent implements OnInit {


  @Input('cards') cards: Array<{
    img: string,
    title: string,
    description: string
  }>;

  @Output() choiceMade = new EventEmitter();
  
  @ViewChildren('swiperCard') swipercards: QueryList<ElementRef>;
  
  
  minutesMinRange = 0;
  minutesChangeRange = 60;
  swiperCardsArray: Array<ElementRef>;

  moveOutWidth: number; // value in pixels that a card needs to travel to dissapear from screen
  shiftRequired: boolean; // state variable that indicates we need to remove the top card of the stack
  transitionInProgress: boolean; // state variable that indicates currently there is transition on-going
  heartVisible: boolean;
  crossVisible: boolean;

  constructor(  private gestureCtrl: GestureController,  private platform: Platform) { }

  ngOnInit() {}

 
  minuteChange(event) {
    this.minutesChangeRange = event.detail.value
  }


  userClickedButton(event, heart) {
    event.preventDefault();
    if (!this.cards.length) return false;
    if (heart) {
      this.swiperCardsArray[0].nativeElement.style.transform = 'translate(' + this.moveOutWidth + 'px, -100px) rotate(-30deg)';
      
    } else {
      this.swiperCardsArray[0].nativeElement.style.transform = 'translate(-' + this.moveOutWidth + 'px, -100px) rotate(30deg)';
      
    };
    this.shiftRequired = true;
    this.transitionInProgress = true;
  };


  handleShift() {
   
    this.transitionInProgress = false;
    if (this.shiftRequired) {
      this.shiftRequired = false;
      this.cards.shift();
    };
  };

  ngAfterViewInit() {
    const cardArray = this.swipercards.toArray();
    this.useSwiperGesture(cardArray);
    this.moveOutWidth = document.documentElement.clientWidth * 1.5;
    this.swiperCardsArray = this.swipercards.toArray();
    this.swipercards.changes.subscribe(()=>{
      this.swiperCardsArray = this.swipercards.toArray();
    })
  };

  useSwiperGesture(cardArray) {
  
    for (let i = 0; i < cardArray.length; i++) {
      const card = cardArray[i];
      // console.log("card", card);

      const gesture = this.gestureCtrl.create({
        el: card.nativeElement,
        threshold: 15,
        gestureName: "swipte",
        onStart: (ev) => {},
        onMove: (ev) => {
          // console.log("ev : ", ev);
          card.nativeElement.style.transform = `translateX(${
            ev.deltaX
          }px) rotate(${ev.deltaX / 10}deg)`;

          //TO SET COLOR ON SWIPE
          // this.setCardColor(ev.deltaX, card.nativeElement);
        },
        onEnd: (ev) => {
          card.nativeElement.style.transition = ".5s ease-out";
        
          //Right side Move
          if (ev.deltaX > 150) {
            card.nativeElement.style.transform = `translateX(${
              +this.platform.width() * 2
            }px) rotate(${ev.deltaX / 2}deg)`;
          }
          // Left Side Move
          else if (ev.deltaX < -150) {
            card.nativeElement.style.transform = `translateX(-${
              +this.platform.width() * 2
            }px) rotate(${ev.deltaX / 2}deg)`;
          }
          // When No move or if small move back to original
          else {
            card.nativeElement.style.transform = "";
          }
          this.shiftRequired = true;
          this.transitionInProgress = true;
        },
      });
      gesture.enable(true);
    }
  }

  // STYLE OF CARD WHEN GESTURE START
  setCardColor(x, element) {
    let color = "";
    const abs = Math.abs(x);
    const min = Math.trunc(Math.min(16 * 16 - abs, 16 * 16));
    const hexCode = this.decimalToHex(min, 2);

    if (x < 0) {
      color = "#FF" + hexCode + "FF" + hexCode;
    } else {
      color = "#" + hexCode + "FF" + hexCode;
    }
    element.style.background = color;
  }

  decimalToHex(d, padding) {
    let hex = Number(d).toString(16);
    padding =
      typeof padding === "undefined" || padding === null
        ? (padding = 2)
        : padding;

    while (hex.length < padding) {
      hex = "0" + hex;
    }
    return hex;
  }

}
