import { ModalController } from '@ionic/angular';
import { Directive, ElementRef, Output, OnInit, Component,OnDestroy,Renderer2, EventEmitter, AfterViewInit, ViewChild} from '@angular/core';
import { Gesture,GestureConfig,createGesture } from "@ionic/core";
import { GestureController } from "@ionic/angular";
import 'hammerjs';

@Component({
  selector: 'app-check-up-modals',
  templateUrl: './check-up-modals.component.html',
  styleUrls: ['./check-up-modals.component.scss'],
})
export class CheckUpModalsComponent implements AfterViewInit {
  dom:any;
  categories = {
    slidesPerView: 3,
    initialSlide:0,
    effect: 'flip',
    slidesPerColumn: 1,
    slidesPerGroup: 1,
    watchSlidesProgress: true,
    loop: true
  };
 

  morphotypeArray = [
    { id: 1, src: "assets/icon/check-up-slide1.png", name: "First Slide", activeClass: false },
    { id: 2, src: "assets/icon/check-up-slide2.png", name: "Second Slide", activeClass: false },
    { id: 3, src: "assets/icon/check-up-slide3.png", name: "Third Slide", activeClass: false }
  ]



  constructor(
    private modalCtrl: ModalController,
    private gestureCtrl: GestureController, 
    private element: ElementRef,
    private renderer: Renderer2,
  ) { }

  ngOnInit() {}

  closeModal() {
    console.log("aa")
    this.modalCtrl.dismiss()
  }


  donsomething(event, src: any) {
    this.morphotypeArray.forEach((obj) => {
      if (obj.id == src.id) {
        obj.activeClass = true;
      } else {
        obj.activeClass = false;
      }
    })
  }
  async ngAfterViewInit() {
   let d = document.querySelector('ion-modal');
  
    const options: GestureConfig = {
      el: d,
      direction: "y",
      gestureName: 'swipe',
      onStart : () => {
       this.renderer.setStyle(d,'transition','none')
      },
      onMove: (ev) => {
        
       if(ev.deltaY > 0){
         
         this.renderer.setStyle(d,'transform',`translateY(${ev.deltaY}px)`)
       }
       
      },
      onEnd: (ev) => {

       this.renderer.setStyle(d,'transition','0.5s ease-out')
     
       if(ev.deltaY > -200){
       
         this.renderer.setStyle(d,'transform','translateY(500px)')
         this.modalCtrl.dismiss();
       }else{
        
         this.renderer.setStyle(d,'transform','translateY(0px)')
       }
      

      }
    };
    const gesture: Gesture = await this.gestureCtrl.create(options);
    gesture.enable();
  }


}
