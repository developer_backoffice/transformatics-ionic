import { Component, ElementRef, OnInit, Renderer2 } from '@angular/core';
import { Router } from '@angular/router';
import { GestureController, ModalController } from '@ionic/angular';
import { Gesture,GestureConfig,createGesture } from "@ionic/core";
@Component({
  selector: 'app-before-aliments',
  templateUrl: './before-aliments.component.html',
  styleUrls: ['./before-aliments.component.scss'],
})
export class BeforeAlimentsComponent implements OnInit {

  constructor(
    private modalCtrl: ModalController,
    private gestureCtrl: GestureController, 
    private element: ElementRef,
    private renderer: Renderer2,
    private router: Router
  ) { }

  ngOnInit() {}


  async ngAfterViewInit() {
    let d = document.querySelector('ion-modal');
   
     const options: GestureConfig = {
       el: d,
       direction: "y",
       gestureName: 'swipe',
       onStart : () => {
        this.renderer.setStyle(d,'transition','none')
       },
       onMove: (ev) => {
         
        if(ev.deltaY > 0){
          
          this.renderer.setStyle(d,'transform',`translateY(${ev.deltaY}px)`)
        }
        
       },
       onEnd: (ev) => {
 
        this.renderer.setStyle(d,'transition','0.5s ease-out')
      
        if(ev.deltaY > -200){
        
          this.renderer.setStyle(d,'transform','translateY(500px)')
          this.modalCtrl.dismiss();
        }else{
         
          this.renderer.setStyle(d,'transform','translateY(0px)')
        }
       
 
       }
     };
     const gesture: Gesture = await this.gestureCtrl.create(options);
     gesture.enable();
   }


   gotoAliments() {
     this.router.navigate(['/aliments'])
     this.modalCtrl.dismiss()
   }


   closeModal() {
     this.modalCtrl.dismiss()
   }
}
