import { ModalController } from '@ionic/angular';
import { Directive, ElementRef, Output, OnInit, Component,OnDestroy,Renderer2, EventEmitter, AfterViewInit, ViewChild} from '@angular/core';
import { Gesture,GestureConfig,createGesture } from "@ionic/core";
import { GestureController } from "@ionic/angular";
import 'hammerjs';
import  { CoachCheckupModalComponent} from '@components/modals/coach-checkup-modal/coach-checkup-modal.component';

@Component({
  selector: 'app-coach-chat-modal',
  templateUrl: './coach-chat-modal.component.html',
  styleUrls: ['./coach-chat-modal.component.scss'],
})
export class CoachChatModalComponent implements AfterViewInit {

  constructor(
    private modalCtrl: ModalController,
    private gestureCtrl: GestureController, 
    private element: ElementRef,
    private renderer: Renderer2,
    ) { }

  ngOnInit() {}

  closeModal() {
  
    this.modalCtrl.dismiss()
  }

  async openCheckUp() {
    const modal = await this.modalCtrl.create({
      component:  CoachCheckupModalComponent,
      cssClass: 'coach-checkup-modal',
      showBackdrop: false,
      // mode: "ios",
    });
    return await modal.present()

  }
  async ngAfterViewInit() {
    let d = document.querySelector('ion-modal');
   
     const options: GestureConfig = {
       el: d,
       direction: "y",
       gestureName: 'swipe',
       onStart : () => {
        this.renderer.setStyle(d,'transition','none')
       },
       onMove: (ev) => {
         
        if(ev.deltaY > 0){
          
          this.renderer.setStyle(d,'transform',`translateY(${ev.deltaY}px)`)
        }
        
       },
       onEnd: (ev) => {
 
        this.renderer.setStyle(d,'transition','0.5s ease-out')
      
        if(ev.deltaY > -200){
        
          this.renderer.setStyle(d,'transform','translateY(500px)')
          this.modalCtrl.dismiss();
        }else{
         
          this.renderer.setStyle(d,'transform','translateY(0px)')
        }
       
 
       }
     };
     const gesture: Gesture = await this.gestureCtrl.create(options);
     gesture.enable();
   }
}
