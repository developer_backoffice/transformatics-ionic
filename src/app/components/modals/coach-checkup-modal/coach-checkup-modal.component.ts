import { ModalController } from '@ionic/angular';
import {CalendarModal } from '@components/modals/calendar-modal/calendar-modal';
import { Directive, ElementRef, Output, OnInit, Component,OnDestroy,Renderer2, EventEmitter, AfterViewInit, ViewChild} from '@angular/core';
import { Gesture,GestureConfig,createGesture } from "@ionic/core";
import { GestureController } from "@ionic/angular";
import 'hammerjs';

@Component({
  selector: 'app-coach-checkup-modal',
  templateUrl: './coach-checkup-modal.component.html',
  styleUrls: ['./coach-checkup-modal.component.scss'],
})
export class CoachCheckupModalComponent implements AfterViewInit {

  startDate: Date;
  days: any = [];
  @ViewChild('myslide') slider;
  categories = {
    slidesPerView: 2.5,
    initialSlide:0,
    effect: 'flip',
    slidesPerColumn: 1,
    slidesPerGroup: 1,
    watchSlidesProgress: true,
    loop: true
  };

  options = {
    centeredSlides: true,
    slidesPerView: 1,
    spaceBetween: 0,
    initialSlide: 5,
    on: {
      beforeInit() {
        const swiper = this;
        swiper.classNames.push(`${swiper.params.containerModifierClass}fade`);
        const overwriteParams = {
          slidesPerView: 1,
          slidesPerColumn: 1,
          slidesPerGroup: 1,
          watchSlidesProgress: true,
          spaceBetween: 0,
          virtualTranslate: true,
        };
        swiper.params = Object.assign(swiper.params, overwriteParams);
        swiper.params = Object.assign(swiper.originalParams, overwriteParams);
      },
      setTranslate() {
        const swiper = this;
        const { slides } = swiper;
        for (let i = 0; i < slides.length; i += 1) {
          const $slideEl = swiper.slides.eq(i);
          const offset$$1 = $slideEl[0].swiperSlideOffset;
          let tx = -offset$$1;
          if (!swiper.params.virtualTranslate) tx -= swiper.translate;
          let ty = 0;
          if (!swiper.isHorizontal()) {
            ty = tx;
            tx = 0;
          }
          const slideOpacity = swiper.params.fadeEffect.crossFade
            ? Math.max(1 - Math.abs($slideEl[0].progress), 0)
            : 1 + Math.min(Math.max($slideEl[0].progress, -1), 0);
          $slideEl
            .css({
              opacity: slideOpacity,
            })
            .transform(`translate3d(${tx}px, ${ty}px, 0px)`);
        }
      },
      setTransition(duration) {
        const swiper = this;
        const { slides, $wrapperEl } = swiper;
        slides.transition(duration);
        if (swiper.params.virtualTranslate && duration !== 0) {
          let eventTriggered = false;
          slides.transitionEnd(() => {
            if (eventTriggered) return;
            if (!swiper || swiper.destroyed) return;
            eventTriggered = true;
            swiper.animating = false;
            const triggerEvents = ['webkitTransitionEnd', 'transitionend'];
            for (let i = 0; i < triggerEvents.length; i += 1) {
              $wrapperEl.trigger(triggerEvents[i]);
            }
          });
        }
      },
    }
  };
  morphotypeArray = [
    { id: 1, src: "assets/icon/check-up-slide1.png", name: "First Slide", activeClass: false },
    { id: 2, src: "assets/icon/check-up-slide2.png", name: "Second Slide", activeClass: false },
    { id: 3, src: "assets/icon/check-up-slide3.png", name: "Third Slide", activeClass: false }
  ]


  constructor(
    private modalCtrl: ModalController,
    private gestureCtrl: GestureController, 
    private element: ElementRef,
    private renderer: Renderer2,
    
    ) { }

  ngOnInit() {

    const date = new Date();
    this.days = this.getDaysInMonth(date.getMonth(), date.getFullYear());
    console.log(this.slider);
  }
  async ngAfterViewInit() {
    this.goToSlide();
    let d = document.querySelector('ion-modal');
   
     const options: GestureConfig = {
       el: d,
       direction: "y",
       gestureName: 'swipe',
       onStart : () => {
        this.renderer.setStyle(d,'transition','none')
       },
       onMove: (ev) => {
         
        if(ev.deltaY > 0){
          
          this.renderer.setStyle(d,'transform',`translateY(${ev.deltaY}px)`)
        }
        
       },
       onEnd: (ev) => {
 
        this.renderer.setStyle(d,'transition','0.5s ease-out')
      
        if(ev.deltaY > -200){
        
          this.renderer.setStyle(d,'transform','translateY(500px)')
          this.modalCtrl.dismiss();
        }else{
         
          this.renderer.setStyle(d,'transform','translateY(0px)')
        }
       
 
       }
     };
     const gesture: Gesture = await this.gestureCtrl.create(options);
     gesture.enable();
  }
  closeModal() {
  
    this.modalCtrl.dismiss()
  }
  donsomething(event, src: any) {
    this.morphotypeArray.forEach((obj) => {
      if (obj.id == src.id) {
        obj.activeClass = true;
      } else {
        obj.activeClass = false;
      }
    })
  }
  async openCalendar() {
    const modal = await this.modalCtrl.create({
      component: CalendarModal,
      cssClass: 'calendar-style',
      backdropDismiss: true,
      showBackdrop: false,
      componentProps: {
        'startDate': new Date(),
      }
    });
    return await modal.present();
  }
  /**
   * @param {number} The month number, 0 based
   * @param {number} The year, not zero based, required to account for leap years
   * @return {Date[]} List with date objects for each day of the month
   */
  getDaysInMonth(month: number, year: number) {
    const date = new Date(year, month, 1);
    const days = [];
    const today = new Date();
    while (date.getMonth() === month) {
      days.push({ date: new Date(date) });
      date.setDate(date.getDate() + 1);
    }

    days.forEach((day, index) => {
      const date = new Date(day.date);
      // days[index]["monthName"] = date.toLocaleString("default", {
      //   month: "short",
      // });
      days[index]["dayName"] = date.toLocaleString("default", {
        weekday: "short",
      });
      days[index]["day"] = date.toLocaleString("default", { day: "2-digit" });
      // days[index]["data"] = this.getRandomInt();
      // days[index]["color"] = "#ff9900";
      if (date.getDate() === today.getDate()) {
        days[index]["isToday"] = true;
        days[index]["dayName"] = 'Today';
      } else {
        days[index]["isToday"] = false;
      }
    });
    console.log("days", days);
    return days;
  }
  async goToSlide() {

    let currentIndex = await this.slider.nativeElement.getActiveIndex();

    let index = this.days.findIndex(x => x.isToday === true);

    this.slider.nativeElement.slideTo(index, 1000);
  }
  async goBack() {
    let currentIndex = await this.slider.nativeElement.getActiveIndex();
    this.slider.nativeElement.slideTo(currentIndex - 1, 1000);
  }
  async goAhead() {
    let currentIndex = await this.slider.nativeElement.getActiveIndex();
    this.slider.nativeElement.slideTo(currentIndex + 1, 1000);
  }
  

}
