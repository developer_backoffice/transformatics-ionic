import { ModalController } from '@ionic/angular';
import { Directive, ElementRef, Output, OnInit, Component,OnDestroy,Renderer2, EventEmitter, AfterViewInit, ViewChild} from '@angular/core';
import { Gesture,GestureConfig,createGesture } from "@ionic/core";
import { GestureController } from "@ionic/angular";
import 'hammerjs';

@Component({
  selector: 'app-sponsership-modal',
  templateUrl: './sponsership-modal.component.html',
  styleUrls: ['./sponsership-modal.component.scss'],
})
export class SponsershipModalComponent implements AfterViewInit {


  segmentModel = "principle";


  constructor(
    private modalCtrl: ModalController,
    private gestureCtrl: GestureController, 
    private element: ElementRef,
    private renderer: Renderer2,
  ) { }

  showRow = "first"


  segmentChanged(event){
    console.log(this.segmentModel);
    
    console.log(event);
    this.segmentModel = event.detail.value
  }


  ngOnInit() {}

  closeModal() {
    // this.modalCtrl.dismiss()

    if(this.showRow == 'third') {
    this.modalCtrl.dismiss()

    } else {
      this.showRow = "second"

    }
  }
  async ngAfterViewInit() {
    let d = document.querySelector('ion-modal');
   
     const options: GestureConfig = {
       el: d,
       direction: "y",
       gestureName: 'swipe',
       onStart : () => {
        this.renderer.setStyle(d,'transition','none')
       },
       onMove: (ev) => {
         
        if(ev.deltaY > 0){
          
          this.renderer.setStyle(d,'transform',`translateY(${ev.deltaY}px)`)
        }
        
       },
       onEnd: (ev) => {
 
        this.renderer.setStyle(d,'transition','0.5s ease-out')
      
        if(ev.deltaY > -200){
        
          this.renderer.setStyle(d,'transform','translateY(500px)')
          this.modalCtrl.dismiss();
        }else{
         
          this.renderer.setStyle(d,'transform','translateY(0px)')
        }
       
 
       }
     };
     const gesture: Gesture = await this.gestureCtrl.create(options);
     gesture.enable();
   }

}
