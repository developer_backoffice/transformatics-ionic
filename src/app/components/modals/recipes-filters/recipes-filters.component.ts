import { Router } from '@angular/router';
import { ModalController } from '@ionic/angular';
import { Component, OnInit } from '@angular/core';
@Component({
  selector: 'app-recipes-filters',
  templateUrl: './recipes-filters.component.html',
  styleUrls: ['./recipes-filters.component.scss'],
})
export class RecipesFiltersComponent implements OnInit {

  constructor(
    private modal: ModalController,
    private rouer: Router
  ) { }

  ngOnInit() {}

  closeModal() {
    this.modal.dismiss()
  }


}
