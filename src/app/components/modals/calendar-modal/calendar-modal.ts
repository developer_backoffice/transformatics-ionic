import { Input } from '@angular/core';
import { Component, OnInit } from '@angular/core';
import { ModalController } from '@ionic/angular';
import { CalendarComponentOptions } from 'ion2-calendar';

@Component({
  selector: 'app-calendar-modal',
  templateUrl: './calendar-modal.html',
  styleUrls: ['./calendar-modal.scss'],
})
export class CalendarModal implements OnInit {
    days = new Array(40);
    showDetails: boolean = false;
      options: CalendarComponentOptions = {
        from: new Date(2000, 0, 1),
        pickMode: 'single',
        weekdays: ['Sun','Mon','Tue','Wed','Thu','Fri','Sat'],
        monthPickerFormat: ['Janvier','Février','Mars','Avril','Mai','Juin','Juillet','Août','Septembre','Octobre','Novembre','Décembre'],
        monthFormat: 'MMMM',
      };
	
 
  	constructor(
    	private modalCtrl: ModalController
  	) { }

  	ngOnInit() {
     
 	 }
    onChange(event) {
        console.log(event);
    }
    showDetail(event) {
        this.showDetails = !this.showDetails;
    }
  	closeModal() {
    	console.log("aa")
     	this.modalCtrl.dismiss({
      
      	componentProps: {
    
      	}
    	});
  	}
	 
}
