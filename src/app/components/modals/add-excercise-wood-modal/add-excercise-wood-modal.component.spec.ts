import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { AddExcerciseWoodModalComponent } from './add-excercise-wood-modal.component';

describe('AddExcerciseWoodModalComponent', () => {
  let component: AddExcerciseWoodModalComponent;
  let fixture: ComponentFixture<AddExcerciseWoodModalComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AddExcerciseWoodModalComponent ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(AddExcerciseWoodModalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
