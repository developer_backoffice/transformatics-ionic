import { ModalController } from '@ionic/angular';
import { Directive, ElementRef, Output, OnInit, Component,OnDestroy,Renderer2, EventEmitter, AfterViewInit, ViewChild} from '@angular/core';
import { Gesture,GestureConfig,createGesture } from "@ionic/core";
import { GestureController } from "@ionic/angular";
import 'hammerjs';

@Component({
  selector: 'app-coach-new-request',
  templateUrl: './coach-new-request.component.html',
  styleUrls: ['./coach-new-request.component.scss'],
})
export class CoachNewRequestComponent implements AfterViewInit {

  constructor(
    private modalCtrl: ModalController,
    private gestureCtrl: GestureController, 
    private element: ElementRef,
    private renderer: Renderer2,
    
    ) { }

  ngOnInit() {}
  closeModal() {
  
    this.modalCtrl.dismiss()
  }
  async ngAfterViewInit() {
    let d = document.querySelector('ion-modal');
   
     const options: GestureConfig = {
       el: d,
       direction: "y",
       gestureName: 'swipe',
       onStart : () => {
        this.renderer.setStyle(d,'transition','none')
       },
       onMove: (ev) => {
         
        if(ev.deltaY > 0){
          
          this.renderer.setStyle(d,'transform',`translateY(${ev.deltaY}px)`)
        }
        
       },
       onEnd: (ev) => {
 
        this.renderer.setStyle(d,'transition','0.5s ease-out')
      
        if(ev.deltaY > -200){
        
          this.renderer.setStyle(d,'transform','translateY(500px)')
          this.modalCtrl.dismiss();
        }else{
         
          this.renderer.setStyle(d,'transform','translateY(0px)')
        }
       
 
       }
     };
     const gesture: Gesture = await this.gestureCtrl.create(options);
     gesture.enable();
   }
}
