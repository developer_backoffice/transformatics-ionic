import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ModalController, NavController } from '@ionic/angular';
import { UserdataService } from '@services/userdata.service';

@Component({
  selector: 'app-click-add-excercise-modal',
  templateUrl: './click-add-excercise-modal.component.html',
  styleUrls: ['./click-add-excercise-modal.component.scss'],
})
export class ClickAddExcerciseModalComponent implements OnInit {
  constructor(
    private modal: ModalController,
    private router: Router
  ) { }

  typeOfSupersetBtnArray = "excercise_de_force"
  yesNoBtnClick = ""
  ngOnInit() {}

  yesClick() {
    this.yesNoBtnClick ='yes'
    this.modal.dismiss()
  }

  noClick() {
    this.yesNoBtnClick ='no'
    this.modal.dismiss()

  }


  goToNext() {
    this.router.navigate(['/add-exercise'])
    this.modal.dismiss()
  }

}
