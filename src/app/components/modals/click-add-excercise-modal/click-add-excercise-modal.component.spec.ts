import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { ClickAddExcerciseModalComponent } from './click-add-excercise-modal.component';

describe('ClickAddExcerciseModalComponent', () => {
  let component: ClickAddExcerciseModalComponent;
  let fixture: ComponentFixture<ClickAddExcerciseModalComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ClickAddExcerciseModalComponent ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(ClickAddExcerciseModalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
