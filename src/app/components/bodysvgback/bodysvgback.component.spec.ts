import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { BodysvgbackComponent } from './bodysvgback.component';

describe('BodysvgbackComponent', () => {
  let component: BodysvgbackComponent;
  let fixture: ComponentFixture<BodysvgbackComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BodysvgbackComponent ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(BodysvgbackComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
