import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-bodysvgback',
  templateUrl: './bodysvgback.component.html',
  styleUrls: ['./bodysvgback.component.scss'],
})
export class BodysvgbackComponent implements OnInit {

  constructor() { }

  ngOnInit() {}
  onClickPathBack(event){
    if (event) {
      if(event.target.classList && event.target.classList){
        const cls  = event.target.classList;
        if(cls && cls.length > 0){
          const className = "Pathselected";
          const res  = cls.value.includes(className);
          console.log("res===",res)
          if(!res){
            cls.add(className);
          }else{
            cls.remove(className);
          }
          console.log("cls",cls)
        }
      }
    }
  }
}
