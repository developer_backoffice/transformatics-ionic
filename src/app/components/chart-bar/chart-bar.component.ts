import { AfterViewInit, Component, Input, OnInit, ViewChild } from "@angular/core";

@Component({
  selector: "app-chart-bar",
  templateUrl: "./chart-bar.component.html",
  styleUrls: ["./chart-bar.component.scss"],
})
export class ChartBarComponent implements OnInit ,AfterViewInit{
  Charts = [];
  slideOpts = {
    initialSlide:0,
    speed: 500,
    slidesPerView:8,
    spaceBetween: 5
  };

  @ViewChild('mySlider') slider;
  @Input('label') label : string;
  constructor() {
  }


  ngOnInit() {
    console.log(this.label);
    const date = new Date();
    this.Charts = this.getDaysInMonth(date.getMonth(), date.getFullYear());
  }

   ngAfterViewInit(){
    this.goToSlide();
  }

  /**
   * @param {number} The month number, 0 based
   * @param {number} The year, not zero based, required to account for leap years
   * @return {Date[]} List with date objects for each day of the month
   */
  getDaysInMonth(month: number, year: number) {
    const date = new Date(year, month, 1);
    const days = [];
    const today = new Date();
    while (date.getMonth() === month) {
      days.push({ date: new Date(date) });
      date.setDate(date.getDate() + 1);
    }

    days.forEach((day, index) => {
      const date = new Date(day.date);
      days[index]["monthName"] = date.toLocaleString("default", {
        month: "short",
      });
      days[index]["dayName"] = date.toLocaleString("default", {
        weekday: "short",
      });
      days[index]["day"] = date.toLocaleString("default", { day: "2-digit" });
      days[index]["data"] = this.getRandomInt();
      days[index]["color"] = "#ff9900";
      if (date.getDate() === today.getDate()) {
        days[index]["isToday"] = true;
      } else {
        days[index]["isToday"] = false;
      }
    });
    console.log("day", days);
    return days;
  }

  getRandomInt(min = 0, max = 100) {
    min = Math.ceil(min);
    max = Math.floor(max);
    return Math.floor(Math.random() * (max - min + 1)) + min;
  }

  async goToSlide() {
    let currentIndex = await this.slider.nativeElement.getActiveIndex();
    let index = this.Charts.findIndex(x=> x.isToday === true);
    this.slider.nativeElement.slideTo(index - 2, 1000);
  }
}
