import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-musculation-auto-seances-session',
  templateUrl: './musculation-auto-seances-session.component.html',
  styleUrls: ['./musculation-auto-seances-session.component.scss'],
})
export class MusculationAutoSeancesSessionComponent implements OnInit {

  constructor() { }

  ngOnInit() {}
  categories = {
    slidesPerView: 3.5,
    effect: 'flip',
    slidesPerColumn: 1,
    slidesPerGroup: 1,
    watchSlidesProgress: false,
    spaceBetween: 10,
  };
}
