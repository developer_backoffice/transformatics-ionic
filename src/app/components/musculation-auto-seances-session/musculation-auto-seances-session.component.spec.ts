import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { MusculationAutoSeancesSessionComponent } from './musculation-auto-seances-session.component';

describe('MusculationAutoSeancesSessionComponent', () => {
  let component: MusculationAutoSeancesSessionComponent;
  let fixture: ComponentFixture<MusculationAutoSeancesSessionComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MusculationAutoSeancesSessionComponent ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(MusculationAutoSeancesSessionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
