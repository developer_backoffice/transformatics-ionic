import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { BodysvgComponent } from './bodysvg.component';

describe('BodysvgComponent', () => {
  let component: BodysvgComponent;
  let fixture: ComponentFixture<BodysvgComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BodysvgComponent ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(BodysvgComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
