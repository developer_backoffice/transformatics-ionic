import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'app-bodysvg',
  templateUrl: './bodysvg.component.html',
  styleUrls: ['./bodysvg.component.scss'],
})
export class BodysvgComponent implements OnInit {

  constructor() { }
  ngOnInit() {}

  onClickPath(event) {
    console.log("click", event)
    if (event) {
      if(event.target.classList && event.target.classList){
        const cls  = event.target.classList;
        if(cls && cls.length > 0){
          const className = "Pathselected";
          const res  = cls.value.includes(className);
          console.log("res===",res)
          if(!res){
            cls.add(className);
          }else{
            cls.remove(className);
          }
          console.log("cls",cls)
        }
      }
    }
  }
}
