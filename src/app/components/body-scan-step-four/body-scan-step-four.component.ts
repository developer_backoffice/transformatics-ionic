import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';

@Component({
	selector: 'app-body-scan-step-four',
	templateUrl: './body-scan-step-four.component.html',
	styleUrls: ['./body-scan-step-four.component.scss'],
})
export class BodyScanStepFourComponent implements OnInit {

	@Output()
	stepFourSelected = new EventEmitter();

	@Input() stepFourForm: FormGroup;

	showMeds: boolean = false;
	showIntolerance: boolean = false;
	constructor() { }

	ngOnInit() {
		this.initializeForm();
	}

	initializeForm() {
		this.stepFourForm = new FormGroup({
			field1: new FormControl(),
			field2: new FormControl(),
			field3: new FormControl(),
			field4: new FormControl(),
			field5: new FormControl(),
			field6: new FormControl()
		});
		this.onChanges();
	}

	showMedicines(event) {
		console.log(event);
		if (event.detail.value == 'yes' && event.detail.checked == true) {
			this.showMeds = true;
		} else {
			this.showMeds = false;
		}
	}

	hideMedicines(event) {

		console.log(event);
		if (event.detail.value == 'no' && event.detail.checked == true) {
			this.showMeds = false;
		}
	}
	showIntolenrances(event) {
		console.log(event);
		if (event.detail.value == 'yes' && event.detail.checked == true) {
			this.showIntolerance = true;
		} else {
			this.showIntolerance = false;
		}
	}

	hideIntolenrances(event) {

		console.log(event);
		if (event.detail.value == 'no' && event.detail.checked == true) {
			this.showIntolerance = false;
		}
	}

	onChanges(): void {
		this.stepFourForm.valueChanges.subscribe(val => {
			this.emitChanges();
		});
	}

	emitChanges() {
		this.stepFourSelected.emit(this.stepFourForm.value);
	}

	onMedicineSelection(event) {
		this.stepFourForm.get('field2').setValue(event.detail.value);
	}

	onIntolerenceSelection(event) {
		this.stepFourForm.get('field3').setValue(event.detail.value);
	}
}
