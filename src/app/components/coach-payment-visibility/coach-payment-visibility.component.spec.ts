import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { CoachPaymentVisibilityComponent } from './coach-payment-visibility.component';

describe('CoachPaymentVisibilityComponent', () => {
  let component: CoachPaymentVisibilityComponent;
  let fixture: ComponentFixture<CoachPaymentVisibilityComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CoachPaymentVisibilityComponent ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(CoachPaymentVisibilityComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
