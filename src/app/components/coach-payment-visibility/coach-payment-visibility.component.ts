import { Component, OnInit } from '@angular/core';
import { ModalController } from '@ionic/angular';
import { CoachChatModalComponent } from '@components/modals/coach-chat-modal/coach-chat-modal.component';
import  { CoachNewRequestComponent} from '@components/modals/coach-new-request/coach-new-request.component';
@Component({
  selector: 'app-coach-payment-visibility',
  templateUrl: './coach-payment-visibility.component.html',
  styleUrls: ['./coach-payment-visibility.component.scss'],
})
export class CoachPaymentVisibilityComponent implements OnInit {

  constructor(private modal: ModalController) { }

  ngOnInit() {}

  async openChat() {
    const modal = await this.modal.create({
      component:  CoachChatModalComponent,
      cssClass: 'coach-chat-modal',
      showBackdrop: false,
      // mode: "ios",
    });
    return await modal.present();
  }

  async openNewRequest() {
    const modal = await this.modal.create({
      component:  CoachNewRequestComponent,
      cssClass: 'coach-new-request-modal',
      showBackdrop: false,
      // mode: "ios",
    });
    return await modal.present()
  }
}
