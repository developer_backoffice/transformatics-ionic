import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';

@Component({
  selector: 'app-body-scan-step-two',
  templateUrl: './body-scan-step-two.component.html',
  styleUrls: ['./body-scan-step-two.component.scss'],
})
export class BodyScanStepTwoComponent implements OnInit {

  @Output()
  stepTwoSelected = new EventEmitter();

  @Input() stepTwoForm: FormGroup;

  constructor() { }
  foodGoalsSelection = "Gain muscle"
  paceOfMuscleGain = "Soft"
  ngOnInit() {
    this.initializeForm();
  }

  initializeForm() {
    this.stepTwoForm = new FormGroup({
      food_pref: new FormControl(this.foodGoalsSelection),
      food_intolerance: new FormControl(this.paceOfMuscleGain),
    });
    this.onChanges();
  }

  onChanges(): void {
    this.stepTwoForm.valueChanges.subscribe(val => {
      this.emitChanges();
    });
  }

  emitChanges() {
    this.stepTwoSelected.emit(this.stepTwoForm.value);
  }

  changeFoodGoalsSelection(value) {
    this.foodGoalsSelection = value;
    this.stepTwoForm.get('food_pref').setValue(value);
  }

  changePaceOfMuscleGain(value) {
    this.paceOfMuscleGain = value;
    this.stepTwoForm.get('food_intolerance').setValue(value);
  }

}
