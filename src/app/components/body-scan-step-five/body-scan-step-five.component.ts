import { Component, EventEmitter, Input, OnInit, Output, ViewChild } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';
import { IonSlides } from '@ionic/angular';

@Component({
  selector: 'app-body-scan-step-five',
  templateUrl: './body-scan-step-five.component.html',
  styleUrls: ['./body-scan-step-five.component.scss'],
})
export class BodyScanStepFiveComponent implements OnInit {

  @Output()
  stepFiveSelected = new EventEmitter();

  @Input() stepFiveForm: FormGroup;

  @ViewChild(IonSlides) slides: IonSlides;

  morphotypeArray = [
    { id: 1, src: "assets/images/Morphotype men-one.png", name: "First Slide", activeClass: false },
    { id: 2, src: "assets/images/Morphotype men-two.png", name: "Second Slide", activeClass: false },
    { id: 3, src: "assets/images/Morphotype men-three.png", name: "Third Slide", activeClass: false }
  ]

  fatMassRateArray = [
    {
      id: 1,
      src: "assets/images/bodyfat-35.png",
      activeClass: false
    },
    {
      id: 2,
      src: "assets/images/bodyfat-30.png",
      activeClass: false
    },
    {
      id: 3,
      src: "assets/images/bodyfat-25.png",
      activeClass: false
    },
    {
      id: 4,
      src: "assets/images/bodyfat-35.png",
      activeClass: false
    },
    {
      id: 5,
      src: "assets/images/bodyfat-20.png",
      activeClass: false
    },
    {
      id: 6,
      src: "assets/images/bodyfat-15.png",
      activeClass: false
    },
    {
      id: 7,
      src: "assets/images/bodyfat-12.png",
      activeClass: false
    },
    {
      id: 8,
      src: "assets/images/bodyfat-8.png",
      activeClass: false
    },

  ]

  options = {
    centeredSlides: true,
    effect: 'flip',
    spaceBetween: 0,
  };

  categories = {
    slidesPerView: 3,
    effect: 'flip',
    slidesPerColumn: 1,
    slidesPerGroup: 1,
    watchSlidesProgress: true,
  };

  constructor() { }

  ngOnInit() {
    this.initializeForm();
  }

  initializeForm() {
    this.stepFiveForm = new FormGroup({
      field1: new FormControl(),
      field2: new FormControl(),
      field3: new FormControl(),
      field4: new FormControl(),
      field5: new FormControl(),
      field6: new FormControl()
    });
    this.onChanges();
  }

  donsomething(event, src: any) {
    this.morphotypeArray.forEach((obj) => {
      if (obj.id == src.id) {
        this.stepFiveForm.get('field1').setValue(obj);
        obj.activeClass = true;
      } else {
        obj.activeClass = false;
      }
    })
  }



  fatMassRateArraySelect(obj) {
    this.fatMassRateArray.forEach((doc) => {
      if (doc.id == obj.id) {
        this.stepFiveForm.get('field2').setValue(obj);
        doc.activeClass = true;
      } else {
        doc.activeClass = false;
      }
    })
  }

  onChanges(): void {
    this.stepFiveForm.valueChanges.subscribe(val => {
      this.emitChanges();
    });
  }

  emitChanges() {
    this.stepFiveSelected.emit(this.stepFiveForm.value);
  }
}
