import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'app-musculation-auto-seances',
  templateUrl: './musculation-auto-seances.component.html',
  styleUrls: ['./musculation-auto-seances.component.scss'],
})
export class MusculationAutoSeancesComponent implements OnInit {

  backpage = "";

  constructor(
    private route: ActivatedRoute
  ) {
   this.backpage =  "/" + this.route.snapshot.paramMap.get('page')
   console.log(this.backpage)
   }

  ngOnInit() {}
  categories = {
    slidesPerView: 1.8,
    effect: 'flip',
    slidesPerColumn: 1,
    slidesPerGroup: 1,
    watchSlidesProgress: false,
    spaceBetween: 10,
  };
}
