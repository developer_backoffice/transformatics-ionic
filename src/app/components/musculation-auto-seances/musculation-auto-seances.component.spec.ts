import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { MusculationAutoSeancesComponent } from './musculation-auto-seances.component';

describe('MusculationAutoSeancesComponent', () => {
  let component: MusculationAutoSeancesComponent;
  let fixture: ComponentFixture<MusculationAutoSeancesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MusculationAutoSeancesComponent ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(MusculationAutoSeancesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
