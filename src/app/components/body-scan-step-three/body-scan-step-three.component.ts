import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';

@Component({
  selector: 'app-body-scan-step-three',
  templateUrl: './body-scan-step-three.component.html',
  styleUrls: ['./body-scan-step-three.component.scss'],
})
export class BodyScanStepThreeComponent implements OnInit {

  showNutrition: boolean = false;
  @Output()
  stepThreeSelected = new EventEmitter();

  @Input() stepThreeForm: FormGroup;

  nutritionArr = [];

  calorieCycling = "Automatic management"

  vagetables = [
    {
      id: 1,
      text: 'Aubergine',
      icon: 'assets/icon/legumes/Aubergine.jpg',
      active: false
    },
    {
      id: 2,
      text: 'Brocoli',
      icon: 'assets/icon/legumes/Brocoli.jpg',
      active: false
    },
    {
      id: 3,
      text: 'Carotte',
      icon: 'assets/icon/legumes/Carotte.jpg',
      active: false
    },
    {
      id: 4,
      text: 'Chou',
      icon: 'assets/icon/legumes/Chou.jpg',
      active: false
    },
    {
      id: 5,
      text: 'Courgette',
      icon: 'assets/icon/legumes/Courgette.jpg',
      active: false
    },
    {
      id: 6,
      text: 'Epinard',
      icon: 'assets/icon/legumes/Epinard.jpg',
      active: false
    },
    {
      id: 7,
      text: 'Haricot',
      icon: 'assets/icon/legumes/Haricot.jpg',
      active: false
    },
    {
      id: 8,
      text: 'Laitue',
      icon: 'assets/icon/legumes/Laitue.jpg',
      active: false
    },

    {
      id: 9,
      text: 'Oignon',
      icon: 'assets/icon/legumes/Oignon.jpg',
      active: false
    },
    {
      id: 10,
      text: 'Poireau',
      icon: 'assets/icon/legumes/Poireau.jpg',
      active: false
    },
    {
      id: 11,
      text: 'Pomme de terre',
      icon: 'assets/icon/legumes/Pomme de terre.jpg',
      active: false
    },

    {
      id: 12,
      text: 'Radis',
      icon: 'assets/icon/legumes/Radis.jpg',
      active: false
    },


  ]

  fruits = [
    {
      id: 1,
      text: 'Abricot',
      icon: 'assets/icon/fruits/Abricot.jpg',
      active: false
    },
    {
      id: 2,
      text: 'Ananas',
      icon: 'assets/icon/fruits/Ananas.jpg',
      active: false
    },
    {
      id: 2,
      text: 'Banane',
      icon: 'assets/icon/fruits/Banane.jpg',
      active: false
    },
    {
      id: 2,
      text: 'Fraise',
      icon: 'assets/icon/fruits/Fraise.jpg',
      active: false
    },
    {
      id: 2,
      text: 'Framboise',
      icon: 'assets/icon/fruits/Framboise.jpg',
      active: false
    },
    {
      id: 2,
      text: 'Pastèque',
      icon: 'assets/icon/fruits/Pastèque.jpg',
      active: false
    },
    {
      id: 2,
      text: 'Poire',
      icon: 'assets/icon/fruits/Poire.jpg',
      active: false
    },
    {
      id: 2,
      text: 'Pomme',
      icon: 'assets/icon/fruits/Pomme.jpg',
      active: false
    },


  ]

  starchy = [
    {
      id: 1,
      text: 'Haricots verts',
      icon: 'assets/icon/feculents/Haricots verts.jpg',
      active: false
    },
    {
      id: 2,
      text: 'Lentilles',
      icon: 'assets/icon/feculents/Lentilles.jpg',
      active: false
    },
    {
      id: 3,
      text: 'Patate douce',
      icon: 'assets/icon/feculents/Patate douce.jpg',
      active: false
    },
    {
      id: 4,
      text: 'Pâtes',
      icon: 'assets/icon/feculents/Pâtes.jpg',
      active: false
    },
    {
      id: 5,
      text: 'Pomme de terre',
      icon: 'assets/icon/feculents/Pomme de terre.jpg',
      active: false
    },
    {
      id: 6,
      text: 'Quinoa',
      icon: 'assets/icon/feculents/Quinoa.jpg',
      active: false
    },
    {
      id: 7,
      text: 'Riz',
      icon: 'assets/icon/feculents/Riz.jpg',
      active: false
    },
    {
      id: 8,
      text: 'Semoule',
      icon: 'assets/icon/feculents/Semoule.jpg',
      active: false
    },

  ]

  constructor() { }

  ngOnInit() {
    this.initializeForm();
  }

  initializeForm() {
    this.stepThreeForm = new FormGroup({
      field1: new FormControl(),
      field2: new FormControl(),
      field3: new FormControl(this.calorieCycling),
      field4: new FormControl(),
      field5: new FormControl(),
      field6: new FormControl()
    });
    this.onChanges();
  }



  selectStarchy(starchy) {
    starchy.active = !starchy.active;
    this.stepThreeForm.get('field6').setValue(JSON.stringify(this.starchy.filter((item) => { return item.active })));
    this.emitChanges();
  }



  selectFruits(fruit) {
    fruit.active = !fruit.active;
    this.stepThreeForm.get('field5').setValue(JSON.stringify(this.fruits.filter((item) => { return item.active })));
    this.emitChanges();
  }


  selectVeg(veg) {
    veg.active = !veg.active;
    this.stepThreeForm.get('field4').setValue(JSON.stringify(this.vagetables.filter((item) => { return item.active })));
    this.emitChanges();
  }

  checkValue1(event) {
    console.log(event);
    if (event.detail.value == 'yes' && event.detail.checked == true) {
      this.showNutrition = true;
    } else {
      this.showNutrition = false;
    }
    this.stepThreeForm.get('field1').setValue(this.showNutrition);
  }
  checkValue2(event) {
    console.log(event);
    if (event.detail.value == 'no' && event.detail.checked == true) {
      this.showNutrition = false;
    }
    this.stepThreeForm.get('field1').setValue(this.showNutrition);
  }

  onChanges(): void {
    this.stepThreeForm.valueChanges.subscribe(val => {
      this.emitChanges();
    });
  }

  emitChanges() {
    this.stepThreeSelected.emit(this.stepThreeForm.value);
  }

  addRemoveNutrition(event) {
    if (event.target.checked) {
      this.nutritionArr.push(event.target.value);
    } else {
      let index = this.nutritionArr.indexOf(event.target.value);
      this.nutritionArr.splice(index, 1);
    }
    if (this.nutritionArr.length > 0) {
      this.stepThreeForm.get('field2').setValue(JSON.stringify(this.nutritionArr));
    } else {
      this.stepThreeForm.get('field2').setValue('');
    }
    this.onChanges();
  }

  calorieCyclingSelection(value) {
    this.calorieCycling = value;
    this.stepThreeForm.get('field3').setValue(value);
    this.onChanges();
  }
}
