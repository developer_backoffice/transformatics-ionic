import { NgModule } from '@angular/core';
import { PreloadAllModules, RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  { path: '', redirectTo: 'landing-page', pathMatch: 'full' },
  { path: 'home', loadChildren: './pages/home/home.module#HomePageModule' },
  {
    path: 'landing-page',
    loadChildren: () => import('./pages/landing-page/landing-page.module').then(m => m.LandingPagePageModule)
  },
  {
    path: 'select-roles',
    loadChildren: () => import('./pages/select-roles/select-roles.module').then(m => m.SelectRolesPageModule)
  },
  {
    path: 'body-scan-steps',
    loadChildren: () => import('./pages/body-scan-steps/body-scan-steps.module').then(m => m.BodyScanStepsPageModule)
  },
  {
    path: 'tabs',
    loadChildren: () => import('./pages/tabs/tabs.module').then(m => m.TabsPageModule)
  },
  {
    path: 'auth',
    loadChildren: () => import('./pages/auth/auth.module').then(m => m.AuthPageModule)
  },
  {
    path: 'body-scan-arrival',
    loadChildren: () => import('./pages/body-scan-arrival/body-scan-arrival.module').then(m => m.BodyScanArrivalPageModule)
  },
  {
    path: 'select-plan',
    loadChildren: () => import('./pages/select-plan/select-plan.module').then(m => m.SelectPlanPageModule)
  },
  {
    path: 'hypertrophy-strengthening-program-creation',
    loadChildren: () => import('./pages/tabs/home/hypertrophy-strengthening-program-creation/hypertrophy-strengthening-program-creation.module').then(m => m.HypertrophyStrengtheningProgramCreationPageModule)

  },
  {
    path: 'add-exercise',
    loadChildren: () => import('./pages/tabs/home/add-exercise/add-exercise.module').then(m => m.AddExercisePageModule)

  },

  {
    path: 'select-body-groups',
    loadChildren: () => import('./pages/tabs/home/select-body-groups/select-body-groups.module').then(m => m.SelectBodyGroupsPageModule)
  },
  {
    path: 'excercise-sheet',
    loadChildren: () => import('./pages/tabs/home/excercise-sheet/excercise-sheet.module').then(m => m.ExcerciseSheetPageModule)
  },

  {
    path: 'edit-physical-activity',
    loadChildren: () => import('./pages/tabs/home/edit-physical-activity/edit-physical-activity.module').then(m => m.EditPhysicalActivityPageModule)
  },
  {
    path: 'ebook',
    loadChildren: () => import('./pages/tabs/cart/ebook/ebook.module').then(m => m.EbookPageModule)
  },
  {
    path: 'cloth-accessoires',
    loadChildren: () => import('./pages/tabs/cart/cloth-accessoires/cloth-accessoires.module').then(m => m.ClothAccessoiresPageModule)
  },
  {
    path: 'food-supplementation',
    loadChildren: () => import('./pages/tabs/cart/food-supplementation/food-supplementation.module').then(m => m.FoodSupplementationPageModule)
  },
  {
    path: 'sports-metrial',
    loadChildren: () => import('./pages/tabs/cart/sports-metrial/sports-metrial.module').then(m => m.SportsMetrialPageModule)
  },
  {
    path: 'ebook-details',
    loadChildren: () => import('./pages/tabs/cart/ebook-details/ebook-details.module').then(m => m.EbookDetailsPageModule)
  },
  {
    path: 'coaching',
    loadChildren: () => import('./pages/tabs/menu/coaching/coaching.module').then(m => m.CoachingPageModule)
  }
  ,
  {
    path: 'preparation-mental',
    loadChildren: () => import('./pages/tabs/menu/preparation-mental/preparation-mental.module').then(m => m.PreparationMentalPageModule)
  }
  ,
  {
    path: 'apprentissage',
    loadChildren: () => import('./pages/tabs/menu/apprentissage/apprentissage.module').then(m => m.ApprentissagePageModule)
  },
  {
    path: 'chat',
    loadChildren: () => import('./pages/tabs/menu/chat/chat.module').then(m => m.ChatPageModule)
  },
  {
    path: 'physical-activity-creation-menu',
    loadChildren: () => import('./pages/tabs/home/physical-activity-creation-menu/physical-activity-creation-menu.module').then(m => m.PhysicalActivityCreationMenuPageModule)
  },

  {
    path: 'find-coach',
    loadChildren: () => import('./pages/tabs/menu/find-coach/find-coach.module').then(m => m.FindCoachPageModule)
  },
  {
    path: 'aliments',
    loadChildren: () => import('./pages/tabs/home/aliments/aliments.module').then(m => m.AlimentsPageModule)
  },
  {
    path: 'aliments-details',
    loadChildren: () => import('./pages/tabs/home/aliments-details/aliments-details.module').then(m => m.AlimentsDetailsPageModule)
  },
  {
    path: 'conseils-nutritionnels',
    loadChildren: () => import('./pages/tabs/home/conseils-nutritionnels/conseils-nutritionnels.module').then(m => m.ConseilsNutritionnelsPageModule)
  },
  {
    path: 'conseils-nutritionnels-details',
    loadChildren: () => import('./pages/tabs/home/conseils-nutritionnels-details/conseils-nutritionnels-details.module').then(m => m.ConseilsNutritionnelsDetailsPageModule)
  },
  {
    path: 'body-scan-arrival-coach',
    loadChildren: () => import('./pages/body-scan-arrival-coach/body-scan-arrival-coach.module').then(m => m.BodyScanArrivalCoachPageModule)
  },
  {
    path: 'body-scan-step-coach',
    loadChildren: () => import('./pages/body-scan-step-coach/body-scan-step-coach.module').then(m => m.BodyScanStepCoachPageModule)
  },
  {
    path: 'select-plan-coach',
    loadChildren: () => import('./pages/select-plan-coach/select-plan-coach.module').then(m => m.SelectPlanCoachPageModule)
  },
  {
    path: 'recipe-details',
    loadChildren: () => import('./pages/tabs/home/recipe-details/recipe-details.module').then(m => m.RecipeDetailsPageModule)
  },
  {
    path: 'coach-acivity',
    loadChildren: () => import('./pages/coach/coach-acivity/coach-acivity.module').then(m => m.CoachAcivityPageModule)
  },
  {
    path: 'musculation-auto-seances-programms/:page',
    loadChildren: () => import('./pages/musculation-auto-seances-programms/musculation-auto-seances-programms.module').then(m => m.MusculationAutoSeancesProgrammsPageModule)
  },
  {
    path: 'musculation-auto-seances-programms-session',
    loadChildren: () => import('./pages/musculation-auto-seances-programms-session/musculation-auto-seances-programms-session.module').then(m => m.MusculationAutoSeancesProgrammsSessionPageModule)
  },
  {
    path: 'musculation-auto-programms-session',
    loadChildren: () => import('./pages/musculation-auto-programms-session/musculation-auto-programms-session.module').then(m => m.MusculationAutoProgrammsSessionPageModule)
  },
  {
    path: 'start-begin-the-session',
    loadChildren: () => import('./pages/start-begin-the-session/start-begin-the-session.module').then(m => m.StartBeginTheSessionPageModule)
  },
  {
    path: 'after-excercise-selection',
    loadChildren: () => import('./pages/tabs/home/after-excercise-selection/after-excercise-selection.module').then(m => m.AfterExcerciseSelectionPageModule)
  },
  {
    path: 'physical-activity-menu-empty',
    loadChildren: () => import('./pages/tabs/home/physical-activity-menu-empty/physical-activity-menu-empty.module').then(m => m.PhysicalActivityMenuEmptyPageModule)
  },

  {
    path: 'pendant-la-seance',
    loadChildren: () => import('./pages/pendant-la-seance/pendant-la-seance.module').then(m => m.PendantLaSeancePageModule)
  },
  {
    path: 'bilan-post-seance',
    loadChildren: () => import('./pages/bilan-post-seance/bilan-post-seance.module').then(m => m.BilanPostSeancePageModule)
  },
  {
    path: 'add-excercise-for-menu',
    loadChildren: () => import('./pages/tabs/home/add-excercise-for-menu/add-excercise-for-menu.module').then(m => m.AddExcerciseForMenuPageModule)
  }

];

@NgModule({
  imports: [
    RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules })
  ],
  exports: [RouterModule]
})
export class AppRoutingModule { }
