import { Injectable } from '@angular/core';
import { HttpService } from '@services/http.service';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  constructor(public httpService: HttpService) { }

  async login(details) {
    return await this.httpService.callApi('POST', details, 'api/login');
  }

  async register(details) {
    return await this.httpService.callApi('POST', details, 'api/register');
  }
}
