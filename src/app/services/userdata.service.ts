import { Injectable } from '@angular/core';
import { HttpService } from './http.service';

@Injectable({
  providedIn: 'root'
})
export class UserdataService {
  public showActivity: boolean = false;
  public changeColor: boolean = false;
  public isCoach: boolean = false;
  constructor(public httpService: HttpService) { }

  async bodyScanDetailsStore(details) {
    return await this.httpService.callApi('POST', details, 'api/bodyscan/store');
  }

}
