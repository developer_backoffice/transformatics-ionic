import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { environment } from '@env';

@Injectable({
	providedIn: 'root'
})
export class HttpService {

	private headers = new Headers({
		'Content-Type': 'application/json'
	});

	constructor(private http: HttpClient) { }


	// This callApi method has 3 mindatorty param and one optional param
	// Optional param will carry the headers info if any required
	// Body is mindatory but initialized with empty object, so while passing keep in mind to pass valida json

	callApi(methodType: string, body: any = {}, path: string = "", options?: any) {
		let protocol = (environment.port == 443) ? 'https://' : 'http://';
		let url = protocol + environment.server_url.replace(/\/$/, "");
		if (path != "") {
			url += '/' + path;
		}
		let requestMehod: string = (methodType) ? methodType.toLowerCase() : "get";
		let headers: HttpHeaders = new HttpHeaders();
		headers = headers.append("Content-Type", "application/json");
		if (typeof (options) == "object") {
			for (let key in options) {
				headers.append(key, options[key]);
			}
		}
		return new Promise(async (resolve, reject) => {
			switch (requestMehod) {
				case "get":
				case "delete":
					try {
						let callData = await this.http.get(url, { headers: headers }).toPromise();
						resolve(callData);
					} catch (error) {
						resolve(error);
					}
					break;
				case "post":
				case "put":
					console.log(headers);
					this.http.post(url, JSON.stringify(body), { headers: headers })
						.subscribe(callData => {
							resolve(callData);
						}, error => {
							resolve(error);
						});
					break;
			}
		});
	}


}
