import { Injectable } from '@angular/core';
import { HttpService } from './http.service';

@Injectable({
  providedIn: 'root'
})
export class CommondataService {

  constructor(public httpService: HttpService) { }

  async getSportsData() {
    return await this.httpService.callApi('GET', '', 'api/sports');
  }
}
