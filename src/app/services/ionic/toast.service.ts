import { Injectable } from '@angular/core';

import { ToastController } from '@ionic/angular';

@Injectable({
    providedIn: 'root'
})
export class ToasterService {

    toaster;

    constructor(
        private toastCtrl: ToastController,
    ) { }

    async present(message: string, duration = 2000, color = 'primary') {
        this.toaster = await this.toastCtrl.create({ message, duration, color });
        await this.toaster.present();
    }
}
