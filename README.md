# Mobile Code followup 

Please follow the imports with alias name

# Import page
`
	import { PageName} from  '@pages/pageFolder/page.component';
`

# Import page

`
	import {PageName} from '@pages/pageFolder/page.module';
`

# Updating Routing modules to route new page

`
	path: 'home', loadChildren: './pages/home/home.module#HomePageModule'
`

# Importing Components

`
	import {componentName} from '@components/componentName.component'
`

# Importing Services

`
	import { ServiceClassName} from '@services/servicename.service';
`

# Importing Guard

`
	import { guardNameClass} from '@guards/guardName.guard';
`

# Importing Environment

`
	import {environment} from '@env';
`

Must put all the environment related things here for further use.

# Generate Page

`
	ionic generate page pages/pagename
`

# Generating Comoponent
`
	ionic generate component components/componentName
`

# Generating Guard
`
	ionic generate guard guards/guardName;
`

# Generating Service

`
	ionic generate service services/serviceName;
`

# Must follow 4 space indent all over using any editor. Or must set your editor to have 4 indent spaces
# Follow the following namings for the Class and inside methods

* Class Name

It should start with the Caps, and if it's long must have the words first char in caps formate

Good Ways

`
	HelloClassName
	Hello
	HelloClass
`

Wrong Way
`
	hello
	helloClassName
	helloClass
	helloclass
`


* Method Names

Good ways


`
	thisMethodDoValidation();
	validateEmail()
	validatePassword()
	parseData()
	callApi()
`


Bad Ways


`
	this_Method_Do_Validation()
	thismethoddovalidation()
	validate()
	parse()
	call()
	??
`



# The above give an idea to write valid names of the methods and class, where devs can relate the meaning with actual existance of the method with real world.

# Using valid names for the variable is also a key point to have good application all time

* Use proper namings of variables with proper type check

`
	loadingData:boolean;
	moreDataToLoad:boolean;
	productDisplayList:array;
`

# Running and performing builds

* Run app in local machine

`
	ionic serve --port 8080
	http://localhost:8080
`


* Adding Platform
`
	ionic cordova platform add android/ios
`

* Adding Plugins

`
	ionic cordova plugin add pluginName
`

* Generating resource

`
	ionic cordova resources android/ios
`

* Building debug for android or ios

`
	ionic cordova build android/ios
`

* Running on device or emulator

`
	ionic cordova run android/ios --device
`

* Building for Production

`
	ionic cordova build android/ios --prod --release --source-map
`

# Hope it's helpful

