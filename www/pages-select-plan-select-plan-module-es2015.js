(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["pages-select-plan-select-plan-module"],{

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/pages/select-plan/select-plan.page.html":
/*!***********************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/pages/select-plan/select-plan.page.html ***!
  \***********************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<ion-header class=\"ion-no-border\">\n    <div class=\"header-img\">\n        <img src=\"assets/icon/header.png\" />\n    </div>\n</ion-header>\n\n<ion-content>\n    <div class=\"main_container\">\n        <div class=\"description_section\">\n            <div class=\"header\">\n                <p>\n                    Commencez dès aujourd’hui à reprendre un rythme de vie sain. Sélectionnez ci-dessous l’une des formules d’abonnement suivantes pour accéder au contenu illimité de l’application Transformatics.\n                </p>\n            </div>\n        </div>\n\n        <div class=\"slider\">\n            <ion-slides pager=\"false\" class=\"plan-slider\" [options]=\"planSlideOptions\" #slides class=\"select-plan\">\n                <ion-slide>\n                    <ion-card>\n                        <img src=\"assets/images/payment_slide_1.png\" class=\"slide_image\" />\n                        <div class=\"price_section\">\n                            <p class=\"p1\">24.90€ /mois</p>\n                            <p class=\"p2\">Ou 0.83 /jour</p>\n                        </div>\n                        <ion-card-header>\n                            <ion-card-title>\n                                Abonnement avec engagement de 12 mois\n                            </ion-card-title>\n                        </ion-card-header>\n                        <ion-card-content>\n                            <p>\n                                Notre formule la plus adéquate. Vous accèderez à l’intégralité fonctionnelle du système Transformatics, ses spécificités, ses check-ups et son coaching personnalisé. Prenez enfin le contrôle de votre rythme de vie et changez votre façon de voir les choses\n                                !\n                            </p>\n                            <ion-button class=\"select-plan\" (click)=\"openPaymentModal()\">\n                                Sélectionner la formule\n                            </ion-button>\n                            <ion-button (click)=\"goToBodyScan()\" class=\"skip-btn\" fill=\"clear\">\n                                Skip\n                            </ion-button>\n                        </ion-card-content>\n                    </ion-card>\n                </ion-slide>\n\n                <ion-slide>\n                    <ion-card>\n                        <img src=\"assets/images/payment_slide_2.png\" class=\"slide_image\" />\n                        <div class=\"price_section\">\n                            <p class=\"p1\">24.90€ /mois</p>\n                            <p class=\"p2\">Ou 0.83 /jour</p>\n                        </div>\n                        <ion-card-header>\n                            <ion-card-title> Abonnement sans engagement </ion-card-title>\n                        </ion-card-header>\n                        <ion-card-content>\n                            <p>\n                                Si vous ne souhaitez pas vous lancer dans un engagement à l’année, cette formule est idéale. Vous payez mensuellement et pouvez résilier votre abonnement quand bon vous semble. Certaines fonctionnalités vous seront cependant inaccessibles.\n                            </p>\n                            <ion-button class=\"select-plan\">\n                                Sélectionner la formule\n                            </ion-button>\n                            <ion-button (click)=\"goToBodyScan()\" class=\"skip-btn\" fill=\"clear\">\n                                Skip\n                            </ion-button>\n                        </ion-card-content>\n                    </ion-card>\n                </ion-slide>\n\n                <ion-slide>\n                    <ion-card>\n                        <img src=\"assets/images/payment_slide_3.png\" class=\"slide_image\" />\n                        <div class=\"price_section\">\n                            <p class=\"p1\">24.90€ /mois</p>\n                            <p class=\"p2\">Ou 0.83 /jour</p>\n                        </div>\n                        <ion-card-header>\n                            <ion-card-title>\n                                Abonnement limité – Training only\n                            </ion-card-title>\n                        </ion-card-header>\n                        <ion-card-content>\n                            <p>\n                                Pour moins de 5€/mois, vous avez la possibilité de créer vos programmes d’entraînement et de les télécharger directement sur votre smartphone. Un outil indispensable si vous jugez que l’application dans son ensemble ne vous sera pas utile.\n                            </p>\n                            <ion-button class=\"select-plan\" routerLink=\"/body-scan-steps\">\n                                Sélectionner la formule\n                            </ion-button>\n                            <ion-button (click)=\"goToBodyScan()\" class=\"skip-btn\" fill=\"clear\">\n                                Skip\n                            </ion-button>\n                        </ion-card-content>\n                    </ion-card>\n                </ion-slide>\n            </ion-slides>\n        </div>\n\n        <div class=\"footer\">\n            <p>\n                En sélectionnant une formule d’abonnement, vous acceptez les Conditions et Termes de Vente et la Politique de Confidentialité. La souscription sera automatiquement renouvelée 24 heures avant la date anniversaire de cette dernière.\n            </p>\n        </div>\n    </div>\n</ion-content>");

/***/ }),

/***/ "./src/app/pages/select-plan/select-plan-routing.module.ts":
/*!*****************************************************************!*\
  !*** ./src/app/pages/select-plan/select-plan-routing.module.ts ***!
  \*****************************************************************/
/*! exports provided: SelectPlanPageRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SelectPlanPageRoutingModule", function() { return SelectPlanPageRoutingModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");
/* harmony import */ var _select_plan_page__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./select-plan.page */ "./src/app/pages/select-plan/select-plan.page.ts");




const routes = [
    {
        path: '',
        component: _select_plan_page__WEBPACK_IMPORTED_MODULE_3__["SelectPlanPage"]
    }
];
let SelectPlanPageRoutingModule = class SelectPlanPageRoutingModule {
};
SelectPlanPageRoutingModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]],
    })
], SelectPlanPageRoutingModule);



/***/ }),

/***/ "./src/app/pages/select-plan/select-plan.module.ts":
/*!*********************************************************!*\
  !*** ./src/app/pages/select-plan/select-plan.module.ts ***!
  \*********************************************************/
/*! exports provided: SelectPlanPageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SelectPlanPageModule", function() { return SelectPlanPageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/__ivy_ngcc__/fesm2015/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/__ivy_ngcc__/fesm2015/forms.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/__ivy_ngcc__/fesm2015/ionic-angular.js");
/* harmony import */ var _select_plan_routing_module__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./select-plan-routing.module */ "./src/app/pages/select-plan/select-plan-routing.module.ts");
/* harmony import */ var _select_plan_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./select-plan.page */ "./src/app/pages/select-plan/select-plan.page.ts");
/* harmony import */ var _components_shared_module__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! @components/shared.module */ "./src/app/components/shared.module.ts");








let SelectPlanPageModule = class SelectPlanPageModule {
};
SelectPlanPageModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [
            _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonicModule"],
            _select_plan_routing_module__WEBPACK_IMPORTED_MODULE_5__["SelectPlanPageRoutingModule"],
            _components_shared_module__WEBPACK_IMPORTED_MODULE_7__["SharedModule"]
        ],
        declarations: [_select_plan_page__WEBPACK_IMPORTED_MODULE_6__["SelectPlanPage"]]
    })
], SelectPlanPageModule);



/***/ }),

/***/ "./src/app/pages/select-plan/select-plan.page.scss":
/*!*********************************************************!*\
  !*** ./src/app/pages/select-plan/select-plan.page.scss ***!
  \*********************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("ion-header {\n  background: #000;\n}\n\nion-content {\n  --background: url('role.png') 0 0/100% no-repeat;\n  background-size: cover;\n  --overflow: hidden;\n}\n\nion-title {\n  color: #fff;\n  font-family: Oswald, sans-serif;\n  font-size: 17px;\n  letter-spacing: 4px;\n}\n\n.header-img {\n  display: flex;\n  justify-content: center;\n}\n\n.header-img img {\n  height: 50px;\n}\n\n.steps {\n  height: 5px;\n  background: #4e4e4e;\n  border-radius: 10px;\n}\n\n.steps.active {\n  background: #c1c1c1;\n}\n\n.body {\n  color: #fff;\n}\n\n.body .p1,\n.body .p2 {\n  font-family: Oswald, serif;\n  text-align: justify;\n  font-size: 0.9rem;\n  letter-spacing: 1px;\n  margin-top: 5%;\n  line-height: 1rem;\n}\n\n.body .p3 {\n  font-family: Oswald, serif;\n  text-align: justify;\n  font-style: italic;\n  margin-top: 5%;\n  line-height: 0.9rem;\n  font-size: 0.8rem;\n}\n\n.main_container {\n  /* height: 100%; */\n  /* padding-bottom: 10%; */\n  background-color: rgba(0, 0, 0, 0.7);\n  padding-left: 5%;\n  padding-right: 5%;\n  height: 100%;\n  display: flex;\n  flex-direction: column;\n  justify-content: center;\n}\n\n.header {\n  margin-top: 6%;\n}\n\n.header p {\n  font-family: Oswald, serif;\n  text-align: center;\n  font-size: 0.75rem;\n  /* letter-spacing: 1px; */\n  line-height: 0.9rem;\n  color: #fff;\n  margin: 0;\n}\n\n.slider {\n  margin-top: 7%;\n}\n\n.footer {\n  margin-top: 7%;\n  margin-bottom: 7%;\n}\n\n.footer p {\n  font-family: Oswald, serif;\n  text-align: center;\n  font-size: 0.75rem;\n  /* letter-spacing: 1px; */\n  color: #fff;\n}\n\nion-card-header {\n  padding-right: 5%;\n  padding-left: 5%;\n  height: 38px;\n}\n\nion-card-title {\n  font-family: Oswald, serif;\n  font-size: 0.8rem;\n  font-weight: bold;\n  text-align: left;\n}\n\n.select-plan {\n  --background: var(--ion-btn-custom-color);\n}\n\nion-card {\n  margin: 0;\n}\n\nion-card-content {\n  padding-right: 5%;\n  padding-left: 5%;\n  padding-bottom: 2%;\n}\n\nion-card-content p {\n  font-family: Oswald, serif;\n  font-size: 0.7rem;\n  text-align: justify;\n  color: #000;\n  padding-top: 0.2rem;\n  padding-bottom: 0.2rem;\n}\n\nion-card-content ion-button {\n  width: 100%;\n}\n\n.slide_image {\n  height: 185px;\n  -o-object-fit: cover;\n     object-fit: cover;\n  width: 100%;\n  -o-object-position: center;\n     object-position: center;\n}\n\n.skip-btn {\n  margin-top: 5px;\n  --background: transparent;\n  --color: var(--ion-color-primary);\n}\n\n.price_section {\n  position: absolute;\n  top: 4%;\n  left: 4%;\n  text-align: left;\n}\n\n.price_section .p1 {\n  font-family: Oswald, serif;\n  font-weight: bold;\n  color: #fff;\n  font-size: 16px;\n  padding-bottom: 0 !important;\n  margin-bottom: 0 !important;\n}\n\n.price_section .p2 {\n  font-family: Oswald, serif;\n  color: #d2d1d1;\n  font-size: 10px;\n  padding-top: 0 !important;\n  margin-top: 0 !important;\n}\n\n.text-bold {\n  font-weight: bold;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvcGFnZXMvc2VsZWN0LXBsYW4vc2VsZWN0LXBsYW4ucGFnZS5zY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBO0VBQ0ksZ0JBQUE7QUFDSjs7QUFFQTtFQUNJLGdEQUFBO0VBR0Esc0JBQUE7RUFDQSxrQkFBQTtBQUNKOztBQUdBO0VBQ0ksV0FBQTtFQUNBLCtCQUFBO0VBQ0EsZUFBQTtFQUNBLG1CQUFBO0FBQUo7O0FBSUE7RUFDSSxhQUFBO0VBQ0EsdUJBQUE7QUFESjs7QUFHSTtFQUNJLFlBQUE7QUFEUjs7QUFPQTtFQUNJLFdBQUE7RUFDQSxtQkFBQTtFQUNBLG1CQUFBO0FBSko7O0FBT0E7RUFDSSxtQkFBQTtBQUpKOztBQVVBO0VBQ0ksV0FBQTtBQVBKOztBQVNJOztFQUVJLDBCQUFBO0VBQ0EsbUJBQUE7RUFDQSxpQkFBQTtFQUNBLG1CQUFBO0VBQ0EsY0FBQTtFQUNBLGlCQUFBO0FBUFI7O0FBVUk7RUFDSSwwQkFBQTtFQUNBLG1CQUFBO0VBQ0Esa0JBQUE7RUFDQSxjQUFBO0VBQ0EsbUJBQUE7RUFDQSxpQkFBQTtBQVJSOztBQWFBO0VBQ0ssa0JBQUE7RUFDRCx5QkFBQTtFQUNBLG9DQUFBO0VBQ0EsZ0JBQUE7RUFDQSxpQkFBQTtFQUVBLFlBQUE7RUFDQSxhQUFBO0VBQ0Esc0JBQUE7RUFDQSx1QkFBQTtBQVhKOztBQWNBO0VBQ0ksY0FBQTtBQVhKOztBQVlJO0VBQ0ksMEJBQUE7RUFDQSxrQkFBQTtFQUNBLGtCQUFBO0VBRUEseUJBQUE7RUFFQSxtQkFBQTtFQUNBLFdBQUE7RUFFQSxTQUFBO0FBYlI7O0FBb0JBO0VBRUksY0FBQTtBQWxCSjs7QUF5QkE7RUFDSSxjQUFBO0VBQ0EsaUJBQUE7QUF0Qko7O0FBd0JJO0VBQ0ksMEJBQUE7RUFDQSxrQkFBQTtFQUNBLGtCQUFBO0VBQ0EseUJBQUE7RUFDQSxXQUFBO0FBdEJSOztBQTJCQTtFQUNJLGlCQUFBO0VBQ0EsZ0JBQUE7RUFDQSxZQUFBO0FBeEJKOztBQTJCQTtFQUNJLDBCQUFBO0VBQ0EsaUJBQUE7RUFDQSxpQkFBQTtFQUNBLGdCQUFBO0FBeEJKOztBQTZCQTtFQUNJLHlDQUFBO0FBMUJKOztBQTZCQTtFQUNJLFNBQUE7QUExQko7O0FBNEJBO0VBQ0ksaUJBQUE7RUFDQSxnQkFBQTtFQUNBLGtCQUFBO0FBekJKOztBQThCSTtFQUNJLDBCQUFBO0VBQ0EsaUJBQUE7RUFFQSxtQkFBQTtFQUNBLFdBQUE7RUFDQSxtQkFBQTtFQUNBLHNCQUFBO0FBN0JSOztBQWdDSTtFQUNJLFdBQUE7QUE5QlI7O0FBbUNBO0VBQ0ksYUFBQTtFQUNBLG9CQUFBO0tBQUEsaUJBQUE7RUFDQSxXQUFBO0VBQ0EsMEJBQUE7S0FBQSx1QkFBQTtBQWhDSjs7QUFvQ0E7RUFDSSxlQUFBO0VBQ0EseUJBQUE7RUFDQSxpQ0FBQTtBQWpDSjs7QUFxQ0E7RUFDSSxrQkFBQTtFQUNBLE9BQUE7RUFDQSxRQUFBO0VBQ0EsZ0JBQUE7QUFsQ0o7O0FBb0NJO0VBQ0ksMEJBQUE7RUFDQSxpQkFBQTtFQUNBLFdBQUE7RUFDQSxlQUFBO0VBQ0EsNEJBQUE7RUFDQSwyQkFBQTtBQWxDUjs7QUFxQ0k7RUFDSSwwQkFBQTtFQUNBLGNBQUE7RUFDQSxlQUFBO0VBQ0EseUJBQUE7RUFDQSx3QkFBQTtBQW5DUjs7QUFnREE7RUFDSSxpQkFBQTtBQTdDSiIsImZpbGUiOiJzcmMvYXBwL3BhZ2VzL3NlbGVjdC1wbGFuL3NlbGVjdC1wbGFuLnBhZ2Uuc2NzcyIsInNvdXJjZXNDb250ZW50IjpbImlvbi1oZWFkZXIge1xuICAgIGJhY2tncm91bmQ6ICMwMDA7XG59XG5cbmlvbi1jb250ZW50IHtcbiAgICAtLWJhY2tncm91bmQ6IHVybCguLi8uLi8uLi9hc3NldHMvaW1hZ2VzL3JvbGUucG5nKSAwIDAvMTAwJSBuby1yZXBlYXQ7XG4gICAgLXdlYmtpdC1iYWNrZ3JvdW5kLXNpemU6IGNvdmVyO1xuICAgIC1tb3otYmFja2dyb3VuZC1zaXplOiBjb3ZlcjtcbiAgICBiYWNrZ3JvdW5kLXNpemU6IGNvdmVyO1xuICAgIC0tb3ZlcmZsb3c6IGhpZGRlbjtcbn1cblxuXG5pb24tdGl0bGUge1xuICAgIGNvbG9yOiAjZmZmO1xuICAgIGZvbnQtZmFtaWx5OiBPc3dhbGQsIHNhbnMtc2VyaWY7XG4gICAgZm9udC1zaXplOiAxN3B4O1xuICAgIGxldHRlci1zcGFjaW5nOiA0cHg7XG5cbn1cblxuLmhlYWRlci1pbWcge1xuICAgIGRpc3BsYXk6IGZsZXg7XG4gICAganVzdGlmeS1jb250ZW50OiBjZW50ZXI7XG5cbiAgICBpbWcge1xuICAgICAgICBoZWlnaHQ6IDUwcHg7XG5cbiAgICB9XG59XG5cblxuLnN0ZXBzIHtcbiAgICBoZWlnaHQ6IDVweDtcbiAgICBiYWNrZ3JvdW5kOiByZ2IoNzgsIDc4LCA3OCk7XG4gICAgYm9yZGVyLXJhZGl1czogMTBweDtcbn1cblxuLnN0ZXBzLmFjdGl2ZSB7XG4gICAgYmFja2dyb3VuZDogI2MxYzFjMTtcbn1cblxuXG5cblxuLmJvZHkge1xuICAgIGNvbG9yOiAjZmZmO1xuXG4gICAgLnAxLFxuICAgIC5wMiB7XG4gICAgICAgIGZvbnQtZmFtaWx5OiBPc3dhbGQsIHNlcmlmO1xuICAgICAgICB0ZXh0LWFsaWduOiBqdXN0aWZ5O1xuICAgICAgICBmb250LXNpemU6IDAuOXJlbTtcbiAgICAgICAgbGV0dGVyLXNwYWNpbmc6IDFweDtcbiAgICAgICAgbWFyZ2luLXRvcDogNSU7XG4gICAgICAgIGxpbmUtaGVpZ2h0OiAxcmVtO1xuICAgIH1cblxuICAgIC5wMyB7XG4gICAgICAgIGZvbnQtZmFtaWx5OiBPc3dhbGQsIHNlcmlmO1xuICAgICAgICB0ZXh0LWFsaWduOiBqdXN0aWZ5O1xuICAgICAgICBmb250LXN0eWxlOiBpdGFsaWM7XG4gICAgICAgIG1hcmdpbi10b3A6IDUlO1xuICAgICAgICBsaW5lLWhlaWdodDogMC45cmVtO1xuICAgICAgICBmb250LXNpemU6IDAuOHJlbTtcbiAgICB9XG5cbn1cblxuLm1haW5fY29udGFpbmVyIHtcbiAgICAgLyogaGVpZ2h0OiAxMDAlOyAqL1xuICAgIC8qIHBhZGRpbmctYm90dG9tOiAxMCU7ICovXG4gICAgYmFja2dyb3VuZC1jb2xvcjogcmdiYSgwLCAwLCAwLCAwLjcpO1xuICAgIHBhZGRpbmctbGVmdDogNSU7XG4gICAgcGFkZGluZy1yaWdodDogNSU7XG5cbiAgICBoZWlnaHQ6IDEwMCU7XG4gICAgZGlzcGxheTogZmxleDtcbiAgICBmbGV4LWRpcmVjdGlvbjogY29sdW1uO1xuICAgIGp1c3RpZnktY29udGVudDogY2VudGVyO1xufVxuXG4uaGVhZGVyIHtcbiAgICBtYXJnaW4tdG9wOiA2JTtcbiAgICBwIHtcbiAgICAgICAgZm9udC1mYW1pbHk6IE9zd2FsZCwgc2VyaWY7XG4gICAgICAgIHRleHQtYWxpZ246IGNlbnRlcjtcbiAgICAgICAgZm9udC1zaXplOiAwLjc1cmVtO1xuXG4gICAgICAgIC8qIGxldHRlci1zcGFjaW5nOiAxcHg7ICovXG4gICAgICAgIC8vIG1hcmdpbi10b3A6IDIlO1xuICAgICAgICBsaW5lLWhlaWdodDogMC45cmVtO1xuICAgICAgICBjb2xvcjogI2ZmZjtcblxuICAgICAgICBtYXJnaW46IDA7XG5cbiAgICB9XG59XG5cblxuXG4uc2xpZGVyIHtcbiAgICAvLyBoZWlnaHQ6IDYwJTtcbiAgICBtYXJnaW4tdG9wOiA3JTtcbn1cblxuaW9uLXNsaWRlIHtcbiAgICAvLyBwYWRkaW5nLWJvdHRvbTogMTVweDtcbn1cblxuLmZvb3RlciB7XG4gICAgbWFyZ2luLXRvcDogNyU7XG4gICAgbWFyZ2luLWJvdHRvbTogNyU7XG5cbiAgICBwIHtcbiAgICAgICAgZm9udC1mYW1pbHk6IE9zd2FsZCwgc2VyaWY7XG4gICAgICAgIHRleHQtYWxpZ246IGNlbnRlcjtcbiAgICAgICAgZm9udC1zaXplOiAwLjc1cmVtO1xuICAgICAgICAvKiBsZXR0ZXItc3BhY2luZzogMXB4OyAqL1xuICAgICAgICBjb2xvcjogI2ZmZjtcblxuICAgIH1cbn1cblxuaW9uLWNhcmQtaGVhZGVyIHtcbiAgICBwYWRkaW5nLXJpZ2h0OiA1JTtcbiAgICBwYWRkaW5nLWxlZnQ6IDUlO1xuICAgIGhlaWdodDogMzhweDtcbn1cblxuaW9uLWNhcmQtdGl0bGUge1xuICAgIGZvbnQtZmFtaWx5OiBPc3dhbGQsIHNlcmlmO1xuICAgIGZvbnQtc2l6ZTogMC44cmVtO1xuICAgIGZvbnQtd2VpZ2h0OiBib2xkO1xuICAgIHRleHQtYWxpZ246IGxlZnQ7XG5cblxufVxuXG4uc2VsZWN0LXBsYW4ge1xuICAgIC0tYmFja2dyb3VuZDogdmFyKC0taW9uLWJ0bi1jdXN0b20tY29sb3IpO1xufVxuXG5pb24tY2FyZCB7XG4gICAgbWFyZ2luOiAwO1xufVxuaW9uLWNhcmQtY29udGVudCB7XG4gICAgcGFkZGluZy1yaWdodDogNSU7XG4gICAgcGFkZGluZy1sZWZ0OiA1JTtcbiAgICBwYWRkaW5nLWJvdHRvbTogMiU7XG5cbiAgICAvLyBtYXJnaW4tdG9wOiA3JTtcbiAgICAvLyBtYXJnaW4tYm90dG9tOiA3JTtcblxuICAgIHAge1xuICAgICAgICBmb250LWZhbWlseTogT3N3YWxkLCBzZXJpZjtcbiAgICAgICAgZm9udC1zaXplOiAwLjdyZW07XG5cbiAgICAgICAgdGV4dC1hbGlnbjoganVzdGlmeTtcbiAgICAgICAgY29sb3I6ICMwMDA7XG4gICAgICAgIHBhZGRpbmctdG9wOiAwLjJyZW07XG4gICAgICAgIHBhZGRpbmctYm90dG9tOiAwLjJyZW07XG4gICAgfVxuXG4gICAgaW9uLWJ1dHRvbiB7XG4gICAgICAgIHdpZHRoOiAxMDAlO1xuICAgIH1cbn1cblxuXG4uc2xpZGVfaW1hZ2Uge1xuICAgIGhlaWdodDogMTg1cHg7XG4gICAgb2JqZWN0LWZpdDogY292ZXI7XG4gICAgd2lkdGg6IDEwMCU7XG4gICAgb2JqZWN0LXBvc2l0aW9uOiBjZW50ZXI7XG59XG5cblxuLnNraXAtYnRuIHtcbiAgICBtYXJnaW4tdG9wOiA1cHg7XG4gICAgLS1iYWNrZ3JvdW5kOiB0cmFuc3BhcmVudDtcbiAgICAtLWNvbG9yOiB2YXIoLS1pb24tY29sb3ItcHJpbWFyeSk7XG5cbn1cblxuLnByaWNlX3NlY3Rpb24ge1xuICAgIHBvc2l0aW9uOiBhYnNvbHV0ZTtcbiAgICB0b3A6IDQlO1xuICAgIGxlZnQ6IDQlO1xuICAgIHRleHQtYWxpZ246IGxlZnQ7XG5cbiAgICAucDEge1xuICAgICAgICBmb250LWZhbWlseTogT3N3YWxkLCBzZXJpZjtcbiAgICAgICAgZm9udC13ZWlnaHQ6IGJvbGQ7XG4gICAgICAgIGNvbG9yOiAjZmZmO1xuICAgICAgICBmb250LXNpemU6IDE2cHg7XG4gICAgICAgIHBhZGRpbmctYm90dG9tOiAwICFpbXBvcnRhbnQ7XG4gICAgICAgIG1hcmdpbi1ib3R0b206IDAgIWltcG9ydGFudDtcbiAgICB9XG5cbiAgICAucDIge1xuICAgICAgICBmb250LWZhbWlseTogT3N3YWxkLCBzZXJpZjtcbiAgICAgICAgY29sb3I6ICNkMmQxZDE7XG4gICAgICAgIGZvbnQtc2l6ZTogMTBweDtcbiAgICAgICAgcGFkZGluZy10b3A6IDAgIWltcG9ydGFudDtcbiAgICAgICAgbWFyZ2luLXRvcDogMCAhaW1wb3J0YW50O1xuICAgIH1cblxuXG5cbn1cblxuXG5cblxuXG5cblxuLnRleHQtYm9sZCB7XG4gICAgZm9udC13ZWlnaHQ6IGJvbGQ7XG59Il19 */");

/***/ }),

/***/ "./src/app/pages/select-plan/select-plan.page.ts":
/*!*******************************************************!*\
  !*** ./src/app/pages/select-plan/select-plan.page.ts ***!
  \*******************************************************/
/*! exports provided: SelectPlanPage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SelectPlanPage", function() { return SelectPlanPage; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/__ivy_ngcc__/fesm2015/ionic-angular.js");
/* harmony import */ var _components_modals_payment_modal_payment_modal_component__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../components/modals/payment-modal/payment-modal.component */ "./src/app/components/modals/payment-modal/payment-modal.component.ts");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");





let SelectPlanPage = class SelectPlanPage {
    constructor(router, route, modal) {
        this.router = router;
        this.route = route;
        this.modal = modal;
        this.planSlideOptions = {
            centeredSlides: true,
            slidesPerView: 1.2,
            spaceBetween: 10,
        };
    }
    ngOnInit() {
    }
    goToBodyScan() {
        console.log("cdsc");
        this.router.navigate(['body-scan-steps']);
    }
    openPaymentModal() {
        return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, function* () {
            const modal = yield this.modal.create({
                component: _components_modals_payment_modal_payment_modal_component__WEBPACK_IMPORTED_MODULE_2__["PaymentModalComponent"],
                cssClass: 'check-up-modal',
                showBackdrop: false,
            });
            return yield modal.present();
        });
    }
};
SelectPlanPage.ctorParameters = () => [
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_4__["Router"] },
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_4__["ActivatedRoute"] },
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_1__["ModalController"] }
];
SelectPlanPage = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_3__["Component"])({
        selector: 'app-select-plan',
        template: Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(/*! raw-loader!./select-plan.page.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/pages/select-plan/select-plan.page.html")).default,
        styles: [Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(/*! ./select-plan.page.scss */ "./src/app/pages/select-plan/select-plan.page.scss")).default]
    })
], SelectPlanPage);



/***/ })

}]);
//# sourceMappingURL=pages-select-plan-select-plan-module-es2015.js.map