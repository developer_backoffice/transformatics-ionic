(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["menu-menu-module"],{

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/pages/tabs/menu/menu.page.html":
/*!**************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/pages/tabs/menu/menu.page.html ***!
  \**************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<ion-content>\n    <ion-grid>\n        <ion-row>\n\n        </ion-row>\n        <ion-row>\n\n        </ion-row>\n        <ion-row>\n\n        </ion-row>\n        <ion-row>\n            <ion-col size=\"12\">\n                <div class=\"ion-text-center menu-heading\">\n                    <h3>Menu</h3>\n                </div>\n            </ion-col>\n            <ion-col size=\"3\">\n                <div class=\"menu\" routerLink=\"/coaching\">\n                    <div class=\"menu-box coaching-menu-box\">\n                        <img src=\"assets/icon/menu/coaching.png\" />\n                    </div>\n                    <div class=\"menu-title\">Coaching</div>\n                </div>\n            </ion-col>\n            <ion-col size=\"3\">\n                <div class=\"menu\" (click)=\"openCourseModal()\">\n                    <div class=\"menu-box courses-menu-box\">\n                        <img src=\"assets/icon/menu/listdecourses.png\" />\n                    </div>\n                    <div class=\"menu-title\">Liste de courses</div>\n                </div>\n            </ion-col>\n            <ion-col size=\"3\">\n                <div class=\"menu\" (click)=\"openDietModal()\">\n                    <div class=\"menu-box diate-menu-box\">\n                        <img src=\"assets/icon/menu/madiets.png\" />\n                    </div>\n                    <div class=\"menu-title\">Ma diète</div>\n                </div>\n            </ion-col>\n            <ion-col size=\"3\">\n                <div class=\"menu\" (click)=\"openNotebookModal()\">\n                    <div class=\"menu-box carnet-menu-box\">\n                        <img src=\"assets/icon/menu/moncarnet.png\" />\n                    </div>\n                    <div class=\"menu-title\">Mon carnet</div>\n                </div>\n            </ion-col>\n            <ion-col size=\"3\">\n                <div class=\"menu\" (click)=\"openPerformance()\">\n                    <div class=\"menu-box performances-menu-box\">\n                        <img src=\"assets/icon/menu/performances.png\" />\n                    </div>\n                    <div class=\"menu-title\">Performances</div>\n                </div>\n            </ion-col>\n            <ion-col size=\"3\">\n                <div class=\"menu\">\n                    <div class=\"menu-box record-menu-box\" (click)=\"openPerformance()\">\n                        <img src=\"assets/icon/menu/records.png\" />\n                    </div>\n                    <div class=\"menu-title\">Records</div>\n                </div>\n            </ion-col>\n            <ion-col size=\"3\">\n                <div class=\"menu\" (click)=\"openPhysiologyModal()\">\n                    <div class=\"menu-box physiologie-menu-box \">\n                        <img src=\"assets/icon/menu/physiology.png\" />\n                    </div>\n                    <div class=\"menu-title\">Physiologie</div>\n                </div>\n            </ion-col>\n            <ion-col size=\"3\">\n                <div class=\"menu\" (click)=\"openManualModal()\"  >\n                    <div class=\"menu-box\">\n                        <img src=\"assets/icon/menu/mesplan.png\" />\n                    </div>\n                    <div class=\"menu-title\">Mes plans</div>\n                </div>\n            </ion-col>\n            <ion-col size=\"3\">\n                <div class=\"menu\" routerLink=\"/apprentissage\">\n                    <div class=\"menu-box apprentice-menu-box\">\n                        <img src=\"assets/icon/menu/apprentissage.png\" />\n                    </div>\n                    <div class=\"menu-title\">Apprentissage</div>\n                </div>\n            </ion-col>\n            <ion-col size=\"3\">\n                <div class=\"menu\" routerLink=\"/preparation-mental\">\n                    <div class=\"menu-box preparetion-m-menu-box\">\n                        <img src=\"assets/icon/menu/preparation.png\" />\n                    </div>\n                    <div class=\"menu-title\">Préparation mentale</div>\n                </div>\n            </ion-col>\n            <ion-col size=\"3\">\n                <div class=\"menu\" (click)=\"openCart()\">\n                    <div class=\"menu-box ebook-menu-box\">\n                        <img src=\"assets/icon/menu/ebook.png\" />\n                    </div>\n                    <div class=\"menu-title\">eBooks</div>\n                </div>\n            </ion-col>\n            <ion-col size=\"3\">\n                <div class=\"menu\" (click)=\"openDeviceModal()\">\n                    <div class=\"menu-box appareils-menu-box\">\n                        <img src=\"assets/icon/menu/appareils.png\" />\n                    </div>\n                    <div class=\"menu-title\">Appareils</div>\n                </div>\n            </ion-col>\n            <ion-col size=\"12\">\n                <div class=\"devider\"></div>\n            </ion-col>\n            <!-- <ion-col size=\"3\">\n                <div class=\"menu\">\n                    <div class=\"menu-box\">\n                        <img src=\"assets/icon/menu/profil.png\" />\n                    </div>\n                    <div class=\"menu-title\">Profil</div>\n                </div>\n            </ion-col> -->\n            <ion-col size=\"3\">\n                <div class=\"menu\" (click)=\"openSponserShipModal()\"> \n                    <div class=\"menu-box parrinage-menu-box\">\n                        <img src=\"assets/icon/menu/parrainge.png\" />\n                    </div>\n                    <div class=\"menu-title\">Parrainage</div>\n                </div>\n            </ion-col>\n            <ion-col size=\"3\">\n                <div class=\"menu\">\n                    <div class=\"menu-box tutorial-menu-box\">\n                        <img src=\"assets/icon/menu/tutorial.png\" />\n                    </div>\n                    <div class=\"menu-title\">Tutoriel</div>\n                </div>\n            </ion-col>\n            <ion-col size=\"3\">\n                <div class=\"menu\">\n                    <div class=\"menu-box disconnection-menu-box\">\n                        <img src=\"assets/icon/menu/deconnexion.png\" />\n                    </div>\n                    <div class=\"menu-title\">Déconnexion</div>\n                </div>\n            </ion-col>\n        </ion-row>\n    </ion-grid>\n</ion-content>");

/***/ }),

/***/ "./src/app/pages/tabs/menu/menu-routing.module.ts":
/*!********************************************************!*\
  !*** ./src/app/pages/tabs/menu/menu-routing.module.ts ***!
  \********************************************************/
/*! exports provided: MenuPageRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "MenuPageRoutingModule", function() { return MenuPageRoutingModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");
/* harmony import */ var _menu_page__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./menu.page */ "./src/app/pages/tabs/menu/menu.page.ts");




const routes = [
    {
        path: '',
        component: _menu_page__WEBPACK_IMPORTED_MODULE_3__["MenuPage"]
    }
];
let MenuPageRoutingModule = class MenuPageRoutingModule {
};
MenuPageRoutingModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]],
    })
], MenuPageRoutingModule);



/***/ }),

/***/ "./src/app/pages/tabs/menu/menu.module.ts":
/*!************************************************!*\
  !*** ./src/app/pages/tabs/menu/menu.module.ts ***!
  \************************************************/
/*! exports provided: MenuPageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "MenuPageModule", function() { return MenuPageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/__ivy_ngcc__/fesm2015/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/__ivy_ngcc__/fesm2015/forms.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/__ivy_ngcc__/fesm2015/ionic-angular.js");
/* harmony import */ var _menu_routing_module__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./menu-routing.module */ "./src/app/pages/tabs/menu/menu-routing.module.ts");
/* harmony import */ var _menu_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./menu.page */ "./src/app/pages/tabs/menu/menu.page.ts");







let MenuPageModule = class MenuPageModule {
};
MenuPageModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [
            _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonicModule"],
            _menu_routing_module__WEBPACK_IMPORTED_MODULE_5__["MenuPageRoutingModule"]
        ],
        declarations: [_menu_page__WEBPACK_IMPORTED_MODULE_6__["MenuPage"]]
    })
], MenuPageModule);



/***/ }),

/***/ "./src/app/pages/tabs/menu/menu.page.scss":
/*!************************************************!*\
  !*** ./src/app/pages/tabs/menu/menu.page.scss ***!
  \************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("ion-content {\n  --background: url('menubg.jpg') 0 0/100% 100% no-repeat;\n  background-size: cover;\n  --overflow: hidden;\n}\n\n.menu-box {\n  position: relative;\n  border-radius: 15px;\n  font-size: 14px;\n  box-shadow: rgba(0, 0, 0, 0.12) 0px 4px 16px;\n  padding: 15px;\n  background: #fff;\n  text-align: center;\n  height: 65px;\n  width: 65px;\n  margin: 0 auto;\n}\n\n.menu .menu-title {\n  text-align: center;\n  color: #fff;\n  margin-top: 10px;\n  font-size: 10px;\n}\n\n.menu-box img {\n  width: 50px;\n}\n\n.devider {\n  height: 1px;\n  background: #f4b183;\n  width: 80%;\n  margin: 0 auto;\n  margin-top: 15px;\n  margin-bottom: 15px;\n}\n\n.menu-heading {\n  color: #fff;\n}\n\n.coaching-menu-box {\n  background: linear-gradient(to right, #ab8870, #e8b999);\n}\n\n.courses-menu-box {\n  background: linear-gradient(to right, #36571e, #4f812d);\n}\n\n.diate-menu-box {\n  background: linear-gradient(to right, #8b230f, #be3319);\n}\n\n.carnet-menu-box {\n  background: linear-gradient(to right, #ac8020, #f7c133);\n}\n\n.performances-menu-box {\n  background: linear-gradient(to right, #e3f7d7, #e9f8e1);\n}\n\n.record-menu-box {\n  background: linear-gradient(to right, #90a880, #aac698);\n}\n\n.apprentice-menu-box {\n  background: linear-gradient(to right, #b37b54, #efa572);\n}\n\n.ebook-menu-box {\n  background: linear-gradient(to right, #0c0c0c, #0c0c0c);\n}\n\n.appareils-menu-box {\n  background: linear-gradient(to right, #2e2e2e, #3c3c3c);\n}\n\n.parrinage-menu-box {\n  background: linear-gradient(to right, #bdbe2d, #f5ee3b);\n}\n\n.tutorial-menu-box {\n  background: linear-gradient(to right, #5e2708, #823610);\n}\n\n.disconnection-menu-box {\n  background: linear-gradient(to right, #b33017, #f34423);\n}\n\n.physiologie-menu-box {\n  background: linear-gradient(to right, #b1b1b1, #f2f2f2);\n}\n\n.preparetion-m-menu-box {\n  background: linear-gradient(to right, #91d6fc, #d3ecfe);\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvcGFnZXMvdGFicy9tZW51L21lbnUucGFnZS5zY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBO0VBQ0ksdURBQUE7RUFHQSxzQkFBQTtFQUNBLGtCQUFBO0FBQ0o7O0FBRUE7RUFDSSxrQkFBQTtFQUNBLG1CQUFBO0VBQ0EsZUFBQTtFQUNBLDRDQUFBO0VBQ0EsYUFBQTtFQUNBLGdCQUFBO0VBQ0Esa0JBQUE7RUFDQSxZQUFBO0VBQ0EsV0FBQTtFQUVBLGNBQUE7QUFBSjs7QUFHQTtFQUNJLGtCQUFBO0VBQ0EsV0FBQTtFQUNBLGdCQUFBO0VBQ0EsZUFBQTtBQUFKOztBQUdBO0VBQ0ksV0FBQTtBQUFKOztBQUdBO0VBQ0ksV0FBQTtFQUNBLG1CQUFBO0VBQ0EsVUFBQTtFQUNBLGNBQUE7RUFDQSxnQkFBQTtFQUNBLG1CQUFBO0FBQUo7O0FBR0E7RUFDSSxXQUFBO0FBQUo7O0FBSUE7RUFDSSx1REFBQTtBQURKOztBQUtBO0VBQ0ksdURBQUE7QUFGSjs7QUFNQTtFQUNJLHVEQUFBO0FBSEo7O0FBT0E7RUFDSSx1REFBQTtBQUpKOztBQVFBO0VBQ0ksdURBQUE7QUFMSjs7QUFTQTtFQUNJLHVEQUFBO0FBTko7O0FBV0E7RUFDSSx1REFBQTtBQVJKOztBQVlBO0VBQ0ksdURBQUE7QUFUSjs7QUFjQTtFQUNJLHVEQUFBO0FBWEo7O0FBaUJBO0VBQ0ksdURBQUE7QUFkSjs7QUFxQkE7RUFDSSx1REFBQTtBQWxCSjs7QUF1QkE7RUFDSSx1REFBQTtBQXBCSjs7QUF5QkE7RUFDSSx1REFBQTtBQXRCSjs7QUEyQkE7RUFDSSx1REFBQTtBQXhCSiIsImZpbGUiOiJzcmMvYXBwL3BhZ2VzL3RhYnMvbWVudS9tZW51LnBhZ2Uuc2NzcyIsInNvdXJjZXNDb250ZW50IjpbImlvbi1jb250ZW50IHtcbiAgICAtLWJhY2tncm91bmQ6IHVybCguLi8uLi8uLi8uLi9hc3NldHMvaW1hZ2VzL21lbnViZy5qcGcpIDAgMC8xMDAlIDEwMCUgbm8tcmVwZWF0O1xuICAgIC13ZWJraXQtYmFja2dyb3VuZC1zaXplOiBjb3ZlcjtcbiAgICAtbW96LWJhY2tncm91bmQtc2l6ZTogY292ZXI7XG4gICAgYmFja2dyb3VuZC1zaXplOiBjb3ZlcjtcbiAgICAtLW92ZXJmbG93OiBoaWRkZW47XG59XG5cbi5tZW51LWJveCB7XG4gICAgcG9zaXRpb246IHJlbGF0aXZlO1xuICAgIGJvcmRlci1yYWRpdXM6IDE1cHg7XG4gICAgZm9udC1zaXplOiAxNHB4O1xuICAgIGJveC1zaGFkb3c6IHJnYigwIDAgMCAvIDEyJSkgMHB4IDRweCAxNnB4O1xuICAgIHBhZGRpbmc6IDE1cHg7XG4gICAgYmFja2dyb3VuZDogI2ZmZjtcbiAgICB0ZXh0LWFsaWduOiBjZW50ZXI7XG4gICAgaGVpZ2h0OiA2NXB4O1xuICAgIHdpZHRoOiA2NXB4O1xuICAgIC8vIGJvcmRlcjogMnB4IHNvbGlkICNmNGIxODM7XG4gICAgbWFyZ2luOiAwIGF1dG87XG59XG5cbi5tZW51IC5tZW51LXRpdGxlIHtcbiAgICB0ZXh0LWFsaWduOiBjZW50ZXI7XG4gICAgY29sb3I6ICNmZmY7XG4gICAgbWFyZ2luLXRvcDogMTBweDtcbiAgICBmb250LXNpemU6IDEwcHg7XG59XG5cbi5tZW51LWJveCBpbWcge1xuICAgIHdpZHRoOiA1MHB4O1xufVxuXG4uZGV2aWRlciB7XG4gICAgaGVpZ2h0OiAxcHg7XG4gICAgYmFja2dyb3VuZDogI2Y0YjE4MztcbiAgICB3aWR0aDogODAlO1xuICAgIG1hcmdpbjogMCBhdXRvO1xuICAgIG1hcmdpbi10b3A6IDE1cHg7XG4gICAgbWFyZ2luLWJvdHRvbTogMTVweDtcbn1cblxuLm1lbnUtaGVhZGluZyB7XG4gICAgY29sb3I6ICNmZmY7XG59XG5cblxuLmNvYWNoaW5nLW1lbnUtYm94IHtcbiAgICBiYWNrZ3JvdW5kOiBsaW5lYXItZ3JhZGllbnQodG8gcmlnaHQsICNhYjg4NzAsICNlOGI5OTkpO1xufVxuXG5cbi5jb3Vyc2VzLW1lbnUtYm94IHtcbiAgICBiYWNrZ3JvdW5kOiBsaW5lYXItZ3JhZGllbnQodG8gcmlnaHQsICMzNjU3MWUsICM0ZjgxMmQpO1xufSAgIFxuXG5cbi5kaWF0ZS1tZW51LWJveCB7XG4gICAgYmFja2dyb3VuZDogbGluZWFyLWdyYWRpZW50KHRvIHJpZ2h0LCAjOGIyMzBmLCAjYmUzMzE5KTtcbn0gICBcblxuXG4uY2FybmV0LW1lbnUtYm94IHtcbiAgICBiYWNrZ3JvdW5kOiBsaW5lYXItZ3JhZGllbnQodG8gcmlnaHQsICNhYzgwMjAsICNmN2MxMzMpO1xufSAgIFxuXG5cbi5wZXJmb3JtYW5jZXMtbWVudS1ib3gge1xuICAgIGJhY2tncm91bmQ6IGxpbmVhci1ncmFkaWVudCh0byByaWdodCwgI2UzZjdkNywgI2U5ZjhlMSk7XG59ICAgXG5cblxuLnJlY29yZC1tZW51LWJveCB7XG4gICAgYmFja2dyb3VuZDogbGluZWFyLWdyYWRpZW50KHRvIHJpZ2h0LCAjOTBhODgwLCAjYWFjNjk4KTtcbn0gICBcblxuXG5cbi5hcHByZW50aWNlLW1lbnUtYm94IHtcbiAgICBiYWNrZ3JvdW5kOiBsaW5lYXItZ3JhZGllbnQodG8gcmlnaHQsICNiMzdiNTQsICNlZmE1NzIpO1xufSAgIFxuXG5cbi5lYm9vay1tZW51LWJveCB7XG4gICAgYmFja2dyb3VuZDogbGluZWFyLWdyYWRpZW50KHRvIHJpZ2h0LCAjMGMwYzBjLCAjMGMwYzBjKTtcbn0gICBcblxuXG5cbi5hcHBhcmVpbHMtbWVudS1ib3gge1xuICAgIGJhY2tncm91bmQ6IGxpbmVhci1ncmFkaWVudCh0byByaWdodCwgIzJlMmUyZSwgIzNjM2MzYyk7XG59ICAgXG5cblxuXG5cbi5wYXJyaW5hZ2UtbWVudS1ib3gge1xuICAgIGJhY2tncm91bmQ6IGxpbmVhci1ncmFkaWVudCh0byByaWdodCwgI2JkYmUyZCwgI2Y1ZWUzYik7XG59ICAgXG5cblxuXG5cblxuLnR1dG9yaWFsLW1lbnUtYm94IHtcbiAgICBiYWNrZ3JvdW5kOiBsaW5lYXItZ3JhZGllbnQodG8gcmlnaHQsICM1ZTI3MDgsICM4MjM2MTApO1xufSAgIFxuXG5cblxuLmRpc2Nvbm5lY3Rpb24tbWVudS1ib3gge1xuICAgIGJhY2tncm91bmQ6IGxpbmVhci1ncmFkaWVudCh0byByaWdodCwgI2IzMzAxNywgI2YzNDQyMyk7XG59ICAgXG5cblxuXG4ucGh5c2lvbG9naWUtbWVudS1ib3gge1xuICAgIGJhY2tncm91bmQ6IGxpbmVhci1ncmFkaWVudCh0byByaWdodCwgI2IxYjFiMSwgI2YyZjJmMik7XG59ICAgXG5cblxuXG4ucHJlcGFyZXRpb24tbS1tZW51LWJveCB7XG4gICAgYmFja2dyb3VuZDogbGluZWFyLWdyYWRpZW50KHRvIHJpZ2h0LCAjOTFkNmZjLCAjZDNlY2ZlKTtcbn0iXX0= */");

/***/ }),

/***/ "./src/app/pages/tabs/menu/menu.page.ts":
/*!**********************************************!*\
  !*** ./src/app/pages/tabs/menu/menu.page.ts ***!
  \**********************************************/
/*! exports provided: MenuPage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "MenuPage", function() { return MenuPage; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");
/* harmony import */ var _components_modals_check_up_modals_check_up_modals_component__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @components/modals/check-up-modals/check-up-modals.component */ "./src/app/components/modals/check-up-modals/check-up-modals.component.ts");
/* harmony import */ var _components_modals_courses_modal_courses_modal_component__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @components/modals/courses-modal/courses-modal.component */ "./src/app/components/modals/courses-modal/courses-modal.component.ts");
/* harmony import */ var _components_modals_manual_registration_manual_registration_component__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @components/modals/manual-registration/manual-registration.component */ "./src/app/components/modals/manual-registration/manual-registration.component.ts");
/* harmony import */ var _components_modals_my_devices_modal_my_devices_modal_component__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @components/modals/my-devices-modal/my-devices-modal.component */ "./src/app/components/modals/my-devices-modal/my-devices-modal.component.ts");
/* harmony import */ var _components_modals_my_diet_modal_my_diet_modal_component__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! @components/modals/my-diet-modal/my-diet-modal.component */ "./src/app/components/modals/my-diet-modal/my-diet-modal.component.ts");
/* harmony import */ var _components_modals_notebook_modal_notebook_modal_component__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! @components/modals/notebook-modal/notebook-modal.component */ "./src/app/components/modals/notebook-modal/notebook-modal.component.ts");
/* harmony import */ var _components_modals_sponsership_modal_sponsership_modal_component__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! @components/modals/sponsership-modal/sponsership-modal.component */ "./src/app/components/modals/sponsership-modal/sponsership-modal.component.ts");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/__ivy_ngcc__/fesm2015/ionic-angular.js");
/* harmony import */ var _services_userdata_service__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! @services/userdata.service */ "./src/app/services/userdata.service.ts");












let MenuPage = class MenuPage {
    constructor(modal, router, userData) {
        this.modal = modal;
        this.router = router;
        this.userData = userData;
    }
    ngOnInit() {
    }
    segmentChanged(event) {
    }
    openCheckupModal() {
        return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, function* () {
            const modal = yield this.modal.create({
                component: _components_modals_check_up_modals_check_up_modals_component__WEBPACK_IMPORTED_MODULE_3__["CheckUpModalsComponent"],
                cssClass: 'check-up-modal',
                showBackdrop: false,
            });
            return yield modal.present();
        });
    }
    openPerformance() {
        return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, function* () {
            this.userData.changeColor = !this.userData.changeColor;
            this.router.navigate(['/tabs/myaccount']);
        });
    }
    openCourseModal() {
        return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, function* () {
            const modal = yield this.modal.create({
                component: _components_modals_courses_modal_courses_modal_component__WEBPACK_IMPORTED_MODULE_4__["CoursesModalComponent"],
                cssClass: 'check-up-modal',
                showBackdrop: false,
            });
            return yield modal.present();
        });
    }
    openDietModal() {
        return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, function* () {
            const modal = yield this.modal.create({
                component: _components_modals_my_diet_modal_my_diet_modal_component__WEBPACK_IMPORTED_MODULE_7__["MyDietModalComponent"],
                cssClass: 'check-up-modal',
                showBackdrop: false,
            });
            return yield modal.present();
        });
    }
    openManualModal() {
        return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, function* () {
            const modal = yield this.modal.create({
                component: _components_modals_manual_registration_manual_registration_component__WEBPACK_IMPORTED_MODULE_5__["ManualRegistrationComponent"],
                cssClass: 'check-up-modal',
                showBackdrop: false,
            });
            return yield modal.present();
        });
    }
    openNotebookModal() {
        return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, function* () {
            console.log("vfvfvf");
            const modal = yield this.modal.create({
                component: _components_modals_notebook_modal_notebook_modal_component__WEBPACK_IMPORTED_MODULE_8__["NotebookModalComponent"],
                cssClass: 'check-up-modal',
                showBackdrop: false,
                // mode: "ios",
                backdropDismiss: true,
                swipeToClose: true,
            });
            return yield modal.present();
        });
    }
    openCart() {
        return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, function* () {
            this.userData.changeColor = !this.userData.changeColor;
            this.router.navigate(['/tabs/cart']);
        });
    }
    openPhysiologyModal() {
        return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, function* () {
            this.userData.changeColor = !this.userData.changeColor;
            this.router.navigate(['/tabs/myaccount']);
            // console.log("vfvfvf");
            // const modal = await this.modal.create({
            //   component: Phys,
            //   cssClass: 'check-up-modal',
            //   showBackdrop: false,
            //   // mode: "ios",
            //   backdropDismiss: true
            // });
            // return await modal.present();
        });
    }
    openDeviceModal() {
        return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, function* () {
            const modal = yield this.modal.create({
                component: _components_modals_my_devices_modal_my_devices_modal_component__WEBPACK_IMPORTED_MODULE_6__["MyDevicesModalComponent"],
                cssClass: 'check-up-modal',
                showBackdrop: false,
                // mode: "ios",
                backdropDismiss: true
            });
            return yield modal.present();
        });
    }
    openSponserShipModal() {
        return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, function* () {
            const modal = yield this.modal.create({
                component: _components_modals_sponsership_modal_sponsership_modal_component__WEBPACK_IMPORTED_MODULE_9__["SponsershipModalComponent"],
                cssClass: 'check-up-modal',
                showBackdrop: false,
                // mode: "ios",
                backdropDismiss: true
            });
            return yield modal.present();
        });
    }
};
MenuPage.ctorParameters = () => [
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_10__["ModalController"] },
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"] },
    { type: _services_userdata_service__WEBPACK_IMPORTED_MODULE_11__["UserdataService"] }
];
MenuPage = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-menu',
        template: Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(/*! raw-loader!./menu.page.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/pages/tabs/menu/menu.page.html")).default,
        styles: [Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(/*! ./menu.page.scss */ "./src/app/pages/tabs/menu/menu.page.scss")).default]
    })
], MenuPage);



/***/ })

}]);
//# sourceMappingURL=menu-menu-module-es2015.js.map