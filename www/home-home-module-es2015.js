(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["home-home-module"],{

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/pages/tabs/home/home.page.html":
/*!**************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/pages/tabs/home/home.page.html ***!
  \**************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<ion-header>\n  <ion-toolbar>\n    <div class=\"daybox\">\n      <ion-icon\n        name=\"chevron-back-outline\"\n        (click)=\"goBack()\"\n        class=\"back-icon\"\n      ></ion-icon>\n      <ion-slides pager=\"false\" [options]=\"options\" #myslide>\n        <ion-slide *ngFor=\"let day of days; index as i\">\n          <div class=\"daybox-title\" (click)=\"openCalendar()\">\n            {{day.dayName}}\n          </div>\n        </ion-slide>\n      </ion-slides>\n      <ion-icon\n        name=\"chevron-forward-outline\"\n        (click)=\"goAhead()\"\n        class=\"forward-icon\"\n      ></ion-icon>\n    </div>\n    <app-chart-bar  *ngIf=\"segmentModel === 'training'\" [label]=\"'HYP'\"></app-chart-bar>\n    <app-chart-bar  *ngIf=\"segmentModel !== 'training'\" [label]=\"'CAL'\"></app-chart-bar>\n  </ion-toolbar>\n</ion-header>\n\n<ion-content (ionScroll)=\"scrollHandler($event)\" [scrollEvents]=\"true\">\n  <div class=\"header-button-box\">\n    <div class=\"header-button-box-button-left\">\n      <div class=\"header-button-box-button\" (click)=\"openCourseModal()\">\n        <img src=\"assets/icon/menu/listdecourses.png\" />\n      </div>\n      <div class=\"header-button-box-button\" (click)=\"openNotebookModal()\">\n        <img src=\"assets/icon/menu/moncarnet.png\" />\n      </div>\n    </div>\n    <div class=\"header-button-box-button-right\" (click)=\"openCheckupModal()\">\n      <img src=\"assets/icon/menu/right.png\" />\n    </div>\n  </div>\n  <div class=\"ion-padding-start ion-padding-end\">\n    <ion-segment\n      value=\"training\"\n      color=\"tertiary\"\n      scrollable=\"true\"\n      [(ngModel)]=\"segmentModel\"\n      (ionChange)=\"segmentChanged($event)\"\n      style=\"margin-top: 25px\"\n    >\n      <ion-segment-button value=\"training\">\n        <ion-label class=\"segment-btn-text\">TRAINING</ion-label>\n      </ion-segment-button>\n\n      <ion-segment-button value=\"alimentation\">\n        <ion-label class=\"segment-btn-text\">ALIMENTATION</ion-label>\n      </ion-segment-button>\n      <ion-segment-button value=\"plus\">\n        <ion-label class=\"segment-btn-text\">PLUS</ion-label>\n      </ion-segment-button>\n    </ion-segment>\n  </div>\n  <div class=\"segment_container_data\">\n    <div *ngIf=\"segmentModel === 'training'\">\n      <ion-grid>\n        <ion-row>\n          <ion-col size=\"12\">\n            <h4 class=\"medium_text\" style=\"padding-left: 1%;\">Activités physiques du jour</h4>\n          </ion-col>\n          <div *ngIf=\"show\">\n            <ion-col size=\"12\" class=\"p-b-0 m-b-0\" style=\"padding: 0\">\n              <ion-card (click)=\"showDetail()\" class=\"card-data\">\n                <ion-item>\n                  <div class=\"exercise-head\">\n                    <img\n                      src=\"assets/icon/masculation.png\"\n                      style=\"width: 20%; margin-left: 10px\"\n                    />\n                    <p class=\"active-exercise\">MUSCULATION</p>\n                  </div>\n                  <div *ngIf=\"detail\">\n                    <ion-icon\n                      name=\"pencil\"\n                      class=\"edit-icon\"\n                      routerLink=\"/edit-physical-activity\"\n                    ></ion-icon>\n                  </div>\n                  <div *ngIf=\"!detail\">\n                    <p class=\"exercise-detail\">Programme- Jour 1</p>\n                  </div>\n                  <p class=\"very_small_text\" style=\"margin-right: 8px\">\n                    4:58<br />6:12\n                  </p>\n                  <img class=\"image-icon\" src=\"assets/icon/timeline.png\" />\n                </ion-item>\n              </ion-card>\n            </ion-col>\n          </div>\n          <div *ngIf=\"detail\">\n            <p class=\"small_text\">Jours d’entraînement</p>\n            <div style=\"display: flex; flex-direction: row\">\n              <ion-card class=\"active_btn card_btn\">\n                <ion-label class=\"medium_text_bold\"> 1</ion-label>\n              </ion-card>\n              <ion-card class=\"card_btn\">\n                <ion-label class=\"medium_text_bold\"> 2</ion-label>\n              </ion-card>\n              <ion-card class=\"card_btn\">\n                <ion-label class=\"medium_text_bold\"> 3 </ion-label>\n              </ion-card>\n            </div>\n            <ion-row>\n              <ion-col size=\"4\">\n                <img class=\"jour_img\" src=\"assets/icon/2body.png\" />\n              </ion-col>\n\n              <ion-col class=\"exercise_details\" size=\"8\">\n                <ion-label class=\"small_text\">\n                  Groupes musculaires sollicités wsd fvef e efv e edgv edvg :\n                </ion-label>\n\n                <ion-label class=\"small_text\"> Pectoraux </ion-label>\n                <br />\n                <ion-label class=\"small_text\"\n                  >Total: 5 exercises - 17 séries</ion-label\n                >\n              </ion-col>\n            </ion-row>\n          </div>\n          <ion-col size=\"12\" class=\"p-b-0 m-b-0\" style=\"padding: 0\">\n            <ion-card routerLink=\"/physical-activity-creation-menu\" style=\"margin-left: 1%;\">\n              <ion-item style=\"--padding-start: 2%;\">\n                <h4 class=\"medium_text\"> Ajouter une activité physique</h4>\n                <ion-icon slot=\"end\" name=\"add-circle-outline\"></ion-icon>\n              </ion-item>\n            </ion-card>\n          </ion-col>\n          <ion-col size=\"12\">\n            <h4 class=\"medium_text\" style=\"padding-left: 1%;\">Historique des activités</h4>\n          </ion-col>\n          <ion-col size=\"12\">\n            <app-chart></app-chart>\n          </ion-col>\n        </ion-row>\n      </ion-grid>\n    </div>\n\n    <div *ngIf=\"segmentModel === 'alimentation'\">\n      <ion-row class=\"list_device_row ion-margin-top\">\n        <ion-item lines=\"none\">\n          <ion-label class=\"list_section_title\">\n            <ion-label class=\"medium_text_bold\">\n              Alimentation – [OBJECTIF]\n            </ion-label>\n          </ion-label>\n          <ion-label class=\"menu-icon\" (click)=\"openMyDietHomeModal()\">\n            <ion-icon name=\"pencil-outline\"></ion-icon>\n          </ion-label>\n        </ion-item>\n      </ion-row>\n\n      <ion-row class=\"alimentation_row_one\">\n        <ion-card>\n          <ion-card-content>\n            <ion-row>\n              <div class=\"side-container\">\n                <ion-label class=\"large_text_bold\"> 0 </ion-label>\n\n                <ion-label class=\"extra_small_text\"> KCAL CONSOMEES </ion-label>\n              </div>\n\n              <div class=\"inner-container\">\n                <div class=\"circle-container\">\n                  <ion-label class=\"large_text_bold\"> 3240 </ion-label>\n\n                  <ion-label class=\"small_text\"> KCAL RESTANTES </ion-label>\n                </div>\n              </div>\n\n              <div class=\"side-container\">\n                <ion-label class=\"large_text_bold\"> 0 </ion-label>\n\n                <ion-label class=\"extra_small_text\"> KCAL BRULEES </ion-label>\n              </div>\n            </ion-row>\n\n            <ion-row class=\"stepper_ui_section ion-margin-top\">\n              <ion-col size=\"2\">\n                <ion-label class=\"very_small_text\"> PROTEINES </ion-label>\n              </ion-col>\n\n              <ion-col size=\"2\">\n                <div class=\"main_line_section\">\n                  <div class=\"yellow-back\"></div>\n                  <div class=\"black-back\"></div>\n                </div>\n              </ion-col>\n\n              <ion-col size=\"2\">\n                <ion-label class=\"very_small_text\"> LIPIDES </ion-label>\n              </ion-col>\n\n              <ion-col size=\"2\">\n                <div class=\"main_line_section\">\n                  <div class=\"yellow-back\"></div>\n                  <div class=\"black-back\"></div>\n                </div>\n              </ion-col>\n\n              <ion-col size=\"2\">\n                <ion-label class=\"very_small_text\"> LIPIDES </ion-label>\n              </ion-col>\n              <ion-col size=\"2\">\n                <div class=\"main_line_section\">\n                  <div class=\"yellow-back\"></div>\n                  <div class=\"black-back\"></div>\n                </div>\n              </ion-col>\n            </ion-row>\n          </ion-card-content>\n        </ion-card>\n      </ion-row>\n\n      <ion-row class=\"ion-margin-top card_item_section_main_row\">\n        <ion-row class=\"card_item_section ion-margin-top ion-margin-bottom\">\n          <ion-col size=\"6\">\n            <ion-card>\n              <ion-card-header>\n                <ion-button> Petit-Déjeuner </ion-button>\n              </ion-card-header>\n\n              <ion-card-content >\n                <ion-img src=\"assets/icon/tick.png\"></ion-img>\n              </ion-card-content>\n            </ion-card>\n\n            <div class=\"card_footer_data ion-margin-top\">\n              <p class=\"medium_text\">398 kcal</p>\n\n              <p class=\"small_text\">Recommandé : 667kcal</p>\n            </div>\n          </ion-col>\n\n          <ion-col size=\"6\">\n            <ion-card  routerLink=\"/aliments\">\n              <ion-card-header>\n                <ion-button> Petit-Déjeuner </ion-button>\n              </ion-card-header>\n\n              <ion-card-content>\n                <ion-img src=\"assets/icon/add.png\"></ion-img>\n                <p>Ajouter</p>\n              </ion-card-content>\n            </ion-card>\n\n            <div class=\"card_footer_data ion-margin-top\">\n              <p class=\"medium_text\">398 kcal</p>\n\n              <p class=\"small_text\">Recommandé : 667kcal</p>\n            </div>\n          </ion-col>\n        </ion-row>\n\n        <ion-row class=\"card_item_section ion-margin-top\">\n          <ion-col size=\"6\">\n            <ion-card>\n              <ion-card-header>\n                <ion-button> Petit-Déjeuner </ion-button>\n              </ion-card-header>\n\n              <ion-card-content>\n                <ion-img src=\"assets/icon/tick.png\"></ion-img>\n              </ion-card-content>\n            </ion-card>\n\n            <div class=\"card_footer_data ion-margin-top\">\n              <p class=\"medium_text\">398 kcal</p>\n\n              <p class=\"small_text\">Recommandé : 667kcal</p>\n            </div>\n          </ion-col>\n\n          <ion-col size=\"6\">\n            <ion-card>\n              <ion-card-header>\n                <ion-button> Petit-Déjeuner </ion-button>\n              </ion-card-header>\n\n              <ion-card-content>\n                <ion-img src=\"assets/icon/add.png\"></ion-img>\n                <p>Ajouter</p>\n              </ion-card-content>\n            </ion-card>\n\n            <div class=\"card_footer_data ion-margin-top\">\n              <p class=\"medium_text\">398 kcal</p>\n\n              <p class=\"small_text\">Recommandé : 667kcal</p>\n            </div>\n          </ion-col>\n        </ion-row>\n      </ion-row>\n\n      <!-- Alimentation Without Login Section-->\n      <ion-row\n        class=\"ion-padding ion-margin-top almentation_without_login_section\"\n      >\n        <ion-row class=\"header_section\">\n          <ion-label class=\"margin_text_bold\">\n            FONCTIONNALITES NON ACCESSIBLES\n          </ion-label>\n        </ion-row>\n\n        <ion-row class=\"ion-margin-top content_section\">\n          <ion-label class=\"small_text\">\n            Votre formule d’engagement ne vous permet pas d’accéder à ces\n            fonctionnalités. Pour pouvoir en profiter, vous devez opter pour\n            notre formule complète avec engagement 12 mois.\n          </ion-label>\n        </ion-row>\n\n        <ion-row class=\"ion-margin-top content_section\">\n          <ion-label class=\"small_text_bold\">\n            Bénéficiez de 15 jours offerts sur votre tout nouvel abonnement en\n            vous inscrivant maintenant !\n          </ion-label>\n        </ion-row>\n\n        <ion-row class=\"ion-margin-top button_section\">\n          <ion-button> Sélectionner la formule </ion-button>\n        </ion-row>\n\n        <ion-row class=\"ion-margin-top card_section\">\n          <ion-card class=\"ion-margin-top\">\n            <img src=\"assets/images/payment_slide_1.png\" class=\"slide_image\" />\n            <div class=\"price_section\">\n              <p class=\"p1\">24.90€ /mois</p>\n              <p class=\"p2\">Ou 0.83 /jour</p>\n            </div>\n            <ion-card-header>\n              <ion-card-title class=\"medium_text_bold\">\n                Abonnement avec engagement de 12 mois\n              </ion-card-title>\n            </ion-card-header>\n            <ion-card-content class=\"ion-padding\">\n              <p class=\"small_text_light\">\n                Notre formule la plus adéquate. Vous accèderez à l’intégralité\n                fonctionnelle du système Transformatics, ses spécificités, ses\n                check-ups et son coaching personnalisé. Prenez enfin le contrôle\n                de votre rythme de vie et changez votre façon de voir les choses\n                !\n              </p>\n              <ion-button class=\"select-plan ion-margin-top\" expand=\"full\">\n                Sélectionner la formule\n              </ion-button>\n            </ion-card-content>\n          </ion-card>\n        </ion-row>\n      </ion-row>\n    </div>\n    <div *ngIf=\"segmentModel === 'plus'\">\n      <ion-row class=\"ion-padding-start ion-padding-top\">\n        <ion-label class=\"medium_text_bold\">\n          Informations complémentaires du jour\n        </ion-label>\n      </ion-row>\n\n      <ion-row\n        class=\"plan_btn_section ion-padding-start ion-padding-top ion-padding-end\"\n      >\n        <ion-col size=\"4\" class=\"first_col\">\n          <div class=\"steptwofirstbtn\">\n            <ion-card\n              [class]=\"plansBtn == 'Gain muscle' ? 'custom_card_bg' : 'custom_card_bg_light'\"\n              expand=\"full\"\n              (click)=\"plansBtn = 'Gain muscle'\"\n            >\n              <ion-label> NEAT </ion-label>\n\n              <ion-label> Activité physique </ion-label>\n            </ion-card>\n          </div>\n        </ion-col>\n        <ion-col size=\"4\">\n          <div class=\"steptwofirstbtn\">\n            <ion-card\n              [class]=\"plansBtn == 'detail_section' ? 'custom_card_bg' : 'custom_card_bg_light'\"\n              \n              expand=\"full\"\n              (click)=\"plansBtn = 'detail_section'\"\n            >\n              <ion-label> EAU </ion-label>\n\n              <ion-label> Hydratation </ion-label>\n            </ion-card>\n          </div>\n        </ion-col>\n        <ion-col size=\"4\">\n          <div class=\"steptwofirstbtn\">\n            <ion-card\n              [class]=\"plansBtn == 'free_session' ? 'custom_card_bg' : 'custom_card_bg_light'\"\n              expand=\"full\"\n              expand=\"full\"\n              (click)=\"plansBtn = 'free_session'\"\n            >\n              <ion-label> SOMMEIL </ion-label>\n\n              <ion-label>Heures de sommeil </ion-label>\n            </ion-card>\n          </div>\n        </ion-col>\n      </ion-row>\n\n      <ion-row\n        *ngIf=\"plansBtn == 'Gain muscle'\"\n        class=\"ion-padding-start ion-padding-top\"\n      >\n        <ion-row class=\"ion-margin second_header_section\">\n          <ion-label class=\"large_text_bold\">NEAT</ion-label>\n          <ion-label class=\"small_text\">Activité physique</ion-label>\n        </ion-row>\n\n        <ion-row class=\"details_seaction_row ion-padding-start ion-padding-end\">\n          <ion-col size=\"5\" class=\"details_seaction_col\">\n            <div class=\"details_section\">\n              <div>\n                <ion-label class=\"large_text_bold\">216</ion-label>\n                <ion-label class=\"small_text\">pas</ion-label>\n              </div>\n\n              <div>\n                <ion-label class=\"large_text_bold\">0,34</ion-label>\n                <ion-label class=\"small_text\">km</ion-label>\n              </div>\n\n              <div>\n                <ion-label class=\"large_text_bold\">35</ion-label>\n                <ion-label class=\"small_text\">kcal</ion-label>\n              </div>\n            </div>\n\n            <div class=\"step_icon_section\">\n              <ion-img src=\"assets/icon/step.png\"></ion-img>\n            </div>\n          </ion-col>\n\n          <ion-col size=\"7\" class=\"button_section_row\">\n            <ion-button class=\"custom_btn_bg very_small_text\">\n              Connecter un appareil\n            </ion-button>\n\n            <ion-button color=\"dark\" class=\"very_small_text\">\n              Enregistrer manuellement\n            </ion-button>\n          </ion-col>\n        </ion-row>\n      </ion-row>\n\n      <ion-row\n        *ngIf=\"plansBtn == 'detail_section'\"\n        class=\"ion-padding-start ion-padding-top second_btn_header_section\"\n      >\n        <ion-row>\n          <ion-col size=\"4\">\n            <div class=\"first_header\">\n              <ion-label class=\"large_text_bold\">EAU</ion-label>\n              <ion-label class=\"small_text\">Hydratation</ion-label>\n            </div>\n          </ion-col>\n\n          <ion-col size=\"4\">\n            <div class=\"second_header\">\n              <ion-label class=\"small_text\">Objectif:2,5L </ion-label>\n            </div>\n          </ion-col>\n\n          <ion-col size=\"4\">\n            <div class=\"third_header\">\n              <ion-label class=\"small_text\">Actuel:</ion-label>\n              <ion-label class=\"large_text_bold\"> 1,75L</ion-label>\n            </div>\n          </ion-col>\n        </ion-row>\n\n        <ion-row class=\"range_section\" >\n          <ion-item lines=\"none\" style=\"width: 100%;\">\n            <ion-range  pin=\"true\" [min]=\"eauMinRange\" max=\"20\" (ionChnage)=\"eauChange($event)\" class=\"eau\" style=\"margin-top: 30px;\"> \n            <ion-label slot=\"start\">{{eauMinRange}}</ion-label>\n            <ion-label slot=\"end\">{{eauChangeRange}}</ion-label>\n          </ion-range>\n          </ion-item>\n        </ion-row>\n      </ion-row>\n\n      <ion-row\n        *ngIf=\"plansBtn == 'free_session'\"\n        class=\"ion-padding-start ion-padding-top second_btn_header_section\"\n      >\n        <ion-row>\n          <ion-col size=\"4\">\n            <div class=\"third_header\">\n              <ion-label class=\"large_text_bold\">SOMMEIL</ion-label>\n              <ion-label class=\"small_text\"> Heures</ion-label>\n            </div>\n          </ion-col>\n        </ion-row>\n\n        <ion-row class=\"range_section\" >\n          <ion-item lines=\"none\" style=\"width: 100%;\">\n           \n            <ion-range  pin=\"true\" [min]=\"sommeilMinRange\" max=\"15\" (ionChnage)=\"sommeilChange($event)\" class=\"sommeil\" style=\"margin-top: 30px;\">\n              <ion-label slot=\"start\">{{sommeilMinRange}}</ion-label>\n              <ion-label slot=\"end\">{{sommeilChangeRange}}</ion-label>\n            </ion-range>  \n          </ion-item>\n        </ion-row>\n      </ion-row>\n    </div>\n  </div>\n</ion-content>\n");

/***/ }),

/***/ "./src/app/pages/tabs/home/home-routing.module.ts":
/*!********************************************************!*\
  !*** ./src/app/pages/tabs/home/home-routing.module.ts ***!
  \********************************************************/
/*! exports provided: HomePageRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "HomePageRoutingModule", function() { return HomePageRoutingModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");
/* harmony import */ var _home_page__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./home.page */ "./src/app/pages/tabs/home/home.page.ts");




const routes = [
    {
        path: '',
        component: _home_page__WEBPACK_IMPORTED_MODULE_3__["HomePage"]
    },
];
let HomePageRoutingModule = class HomePageRoutingModule {
};
HomePageRoutingModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]],
    })
], HomePageRoutingModule);



/***/ }),

/***/ "./src/app/pages/tabs/home/home.module.ts":
/*!************************************************!*\
  !*** ./src/app/pages/tabs/home/home.module.ts ***!
  \************************************************/
/*! exports provided: HomePageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "HomePageModule", function() { return HomePageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/__ivy_ngcc__/fesm2015/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/__ivy_ngcc__/fesm2015/forms.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/__ivy_ngcc__/fesm2015/ionic-angular.js");
/* harmony import */ var _home_routing_module__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./home-routing.module */ "./src/app/pages/tabs/home/home-routing.module.ts");
/* harmony import */ var _home_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./home.page */ "./src/app/pages/tabs/home/home.page.ts");
/* harmony import */ var _components_shared_module__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! @components/shared.module */ "./src/app/components/shared.module.ts");
/* harmony import */ var ion2_calendar__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ion2-calendar */ "./node_modules/ion2-calendar/__ivy_ngcc__/dist/index.js");
/* harmony import */ var ion2_calendar__WEBPACK_IMPORTED_MODULE_8___default = /*#__PURE__*/__webpack_require__.n(ion2_calendar__WEBPACK_IMPORTED_MODULE_8__);









let HomePageModule = class HomePageModule {
};
HomePageModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [
            _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonicModule"],
            _home_routing_module__WEBPACK_IMPORTED_MODULE_5__["HomePageRoutingModule"],
            ion2_calendar__WEBPACK_IMPORTED_MODULE_8__["CalendarModule"],
            _components_shared_module__WEBPACK_IMPORTED_MODULE_7__["SharedModule"]
        ],
        declarations: [_home_page__WEBPACK_IMPORTED_MODULE_6__["HomePage"]]
    })
], HomePageModule);



/***/ }),

/***/ "./src/app/pages/tabs/home/home.page.scss":
/*!************************************************!*\
  !*** ./src/app/pages/tabs/home/home.page.scss ***!
  \************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("ion-toolbar {\n  --background: #000;\n}\n\nion-item {\n  --inner-border-width: 0 0 0px 0;\n}\n\nion-card {\n  box-shadow: rgba(0, 0, 0, 0.12) 0px 4px 16px;\n}\n\nion-label {\n  font-family: \"Oswald\" !important;\n}\n\n.segment_container_data {\n  padding: 0px;\n  margin-bottom: 25%;\n}\n\n.card_btn {\n  height: 40px;\n  width: 40px;\n  border: 2px solid #565656;\n  margin: 0;\n  display: flex;\n  justify-content: center;\n  margin-right: 5px;\n}\n\n.card_btn ion-label {\n  align-self: center;\n  font-size: 1.2rem;\n  font-weight: 600;\n  color: #000;\n}\n\n.active_btn {\n  background: var(--ion-btn-custom-color);\n}\n\n.active-exercise {\n  font-size: 0.8rem;\n}\n\n.exercise-detail {\n  font-size: 0.7rem !important;\n  margin-right: 38px;\n}\n\n.edit-icon {\n  font-size: 1.2rem;\n  margin-right: 100px;\n}\n\n.exercise-head {\n  display: flex;\n  background: #000;\n  color: #fff;\n  border-radius: 15px;\n  width: 40%;\n  margin-right: 15px;\n}\n\nion-item {\n  --padding-top: 2px;\n  --padding-bottom: 2px;\n}\n\n.daybox {\n  text-align: center;\n  color: #fff;\n  margin-top: 15px;\n}\n\n.daybox-title {\n  display: block;\n  min-width: 100px;\n  background: #626262;\n  padding: 3px 20px;\n  border-radius: 25px;\n  border: 2px solid #333;\n  z-index: 1;\n  position: relative;\n}\n\n.header-button-box {\n  display: flex;\n  position: fixed;\n  z-index: 999;\n  margin-top: -14px;\n  color: #fff;\n  width: 100%;\n}\n\n.header-button-box-button-right ion-icon {\n  font-size: 26px;\n  background: #333;\n  border-radius: 25px;\n  padding: 0px;\n  border: 1px solid #ffd1b3;\n}\n\n.header-button-box-button-left ion-icon {\n  font-size: 26px;\n  background: #333;\n  border-radius: 25px;\n  padding: 0px;\n  border: 1px solid #ffd1b3;\n}\n\n.header-button-box-button-right {\n  position: absolute;\n  right: 12%;\n  display: inline-block;\n  margin-right: 15px;\n  background: #333;\n  border-radius: 40px;\n  padding: 4px 4px 0px 5px;\n  border: 1px solid #ffd1b3;\n}\n\n.header-button-box-button-right img {\n  height: 25px;\n  width: 25px;\n}\n\n.header-button-box-button-left {\n  left: 10%;\n  position: absolute;\n}\n\n.header-button-box-button-left .header-button-box-button {\n  display: inline-block;\n  margin-right: 15px;\n  height: 35px;\n  width: 35px;\n  background: #333;\n  border-radius: 40px;\n  padding: 5px 8px 0px 8px;\n  border: 1px solid #ffd1b3;\n}\n\n.back-icon {\n  position: absolute;\n  top: 25px;\n  left: 110px;\n  z-index: 2;\n}\n\n.forward-icon {\n  position: absolute;\n  right: 110px;\n  top: 25px;\n  z-index: 2;\n}\n\n.list_device_row ion-item {\n  width: 100%;\n}\n\n.list_device_row ion-item div ion-img {\n  width: 50px;\n}\n\n.list_device_row ion-item .list_section_title {\n  padding-left: 2%;\n}\n\n.list_device_row ion-item .menu-icon {\n  position: absolute;\n  right: 7%;\n  font-size: 25px;\n  color: gray;\n}\n\n.alimentation_row_one ion-card {\n  width: 100%;\n}\n\n.alimentation_row_one ion-card ion-card-content ion-row {\n  display: flex;\n  flex-direction: row;\n  justify-content: center;\n}\n\n.alimentation_row_one ion-card ion-card-content ion-row div {\n  justify-content: center;\n  flex-grow: 1;\n}\n\n.circle-container {\n  border: 3px solid var(--ion-color-primary);\n  padding: 14px;\n  border-radius: 50%;\n  height: 110px;\n  width: 110px;\n  display: flex;\n  flex-direction: column;\n  justify-content: center;\n}\n\n.circle-container .small_text {\n  text-align: center;\n  font-size: 0.5rem;\n}\n\n.circle-container .large_text_bold {\n  text-align: center;\n}\n\n.inner-container {\n  display: flex;\n  flex-direction: column;\n  align-items: center;\n}\n\n.side-container {\n  display: flex;\n  flex-direction: column;\n}\n\n.side-container .large_text_bold {\n  text-align: center;\n}\n\n.side-container .extra_small_text {\n  text-align: center;\n}\n\n.main_line_section {\n  display: flex;\n  flex-direction: row;\n  height: 24px;\n}\n\n.main_line_section .yellow-back {\n  background: var(--ion-color-primary);\n  width: 20px;\n  height: 4px;\n  align-self: center;\n}\n\n.main_line_section .black-back {\n  background: #000;\n  width: 20px;\n  height: 4px;\n  align-self: center;\n}\n\n.card_item_section_main_row {\n  display: flex;\n  justify-content: center;\n}\n\n.card_item_section_main_row .card_item_section ion-col ion-card {\n  height: 150px;\n  margin: 0;\n}\n\n.card_item_section_main_row .card_item_section ion-col ion-card ion-card-header ion-button {\n  width: 100%;\n  --background: #2a2a2a;\n}\n\n.card_item_section_main_row .card_item_section ion-col ion-card ion-card-content {\n  display: flex;\n  justify-content: center;\n  flex-direction: column;\n}\n\n.card_item_section_main_row .card_item_section ion-col ion-card ion-card-content ion-img {\n  width: 25px;\n  background: #bebebe;\n  border-radius: 50%;\n  align-self: center;\n}\n\n.card_item_section_main_row .card_item_section ion-col ion-card ion-card-content p {\n  align-self: center;\n}\n\n.card_footer_data p {\n  margin: 0;\n}\n\n.steptwofirstbtn ion-card {\n  height: 100px;\n  margin: 0;\n  display: flex;\n  flex-direction: column;\n  justify-content: flex-start;\n  margin: 0;\n  display: flex;\n  padding: 8px;\n}\n\n.second_header_section {\n  display: flex;\n  align-self: flex-end;\n  margin-left: 10px;\n  width: 100%;\n}\n\n.second_header_section .small_text {\n  align-self: flex-end;\n  margin-bottom: 1px;\n  margin-left: 5px;\n}\n\n.second_header_section .large_text_bold {\n  align-self: flex-end;\n}\n\n.details_seaction_row {\n  width: 100%;\n}\n\n.details_seaction_row .details_seaction_col {\n  display: flex;\n  flex-direction: row;\n  justify-content: center;\n}\n\n.details_seaction_row .details_seaction_col .details_section div .small_text {\n  padding-left: 7px;\n}\n\n.details_seaction_row .details_seaction_col .step_icon_section {\n  padding-left: 10%;\n  display: flex;\n  justify-content: center;\n}\n\n.details_seaction_row .details_seaction_col .step_icon_section ion-img {\n  width: 40px;\n}\n\n.button_section_row {\n  display: flex;\n  flex-direction: column;\n}\n\n.button_section_row .details_section {\n  --background: var(--ion-btn-custom-color) ;\n}\n\n.second_btn_header_section ion-row {\n  width: 100%;\n}\n\n.second_btn_header_section ion-row ion-col .first_header .small_text {\n  padding-left: 5px;\n}\n\n.almentation_without_login_section .header_section {\n  width: 100%;\n  display: flex;\n  justify-content: center;\n}\n\n.almentation_without_login_section .content_section ion-label {\n  text-align: justify;\n}\n\n.almentation_without_login_section .button_section {\n  width: 100%;\n}\n\n.almentation_without_login_section .button_section ion-button {\n  width: 100%;\n  --background: var(--ion-btn-custom-color) ;\n}\n\n.almentation_without_login_section .card_section ion-card {\n  margin: 0;\n  padding: 1%;\n}\n\n.almentation_without_login_section .card_section ion-card ion-card-content p {\n  text-align: justify;\n}\n\n.price_section {\n  position: absolute;\n  top: 4%;\n  left: 4%;\n  text-align: left;\n}\n\n.price_section .p1 {\n  font-family: Oswald, serif;\n  font-weight: bold;\n  color: #fff;\n  font-size: 16px;\n  padding-bottom: 0 !important;\n  margin-bottom: 0 !important;\n}\n\n.price_section .p2 {\n  font-family: Oswald, serif;\n  color: #d2d1d1;\n  font-size: 10px;\n  padding-top: 0 !important;\n  margin-top: 0 !important;\n}\n\n.select-plan {\n  --background: var(--ion-btn-custom-color);\n}\n\nion-range {\n  --bar-height: 10px;\n  --bar-border-radius: 15px;\n  --knob-size: 50px;\n}\n\nion-range ion-label {\n  padding: 10px;\n}\n\n.custom_btn_bg {\n  --background: var(--ion-btn-custom-color) ;\n}\n\n.custom_card_bg {\n  background: #2d2d2d;\n  color: #fff;\n}\n\n.custom_card_bg_light {\n  background: #fff;\n  color: #000;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvcGFnZXMvdGFicy9ob21lL2hvbWUucGFnZS5zY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBO0VBQ0ksa0JBQUE7QUFDSjs7QUFFQTtFQUNJLCtCQUFBO0FBQ0o7O0FBRUE7RUFDSSw0Q0FBQTtBQUNKOztBQUVBO0VBQ0ksZ0NBQUE7QUFDSjs7QUFFQTtFQUNJLFlBQUE7RUFDQSxrQkFBQTtBQUNKOztBQUVBO0VBQ0ksWUFBQTtFQUNBLFdBQUE7RUFDQSx5QkFBQTtFQUNBLFNBQUE7RUFDQSxhQUFBO0VBQ0EsdUJBQUE7RUFDQSxpQkFBQTtBQUNKOztBQUNJO0VBQ0ksa0JBQUE7RUFDQSxpQkFBQTtFQUNBLGdCQUFBO0VBQ0EsV0FBQTtBQUNSOztBQUdBO0VBQ0ksdUNBQUE7QUFBSjs7QUFHQTtFQUNJLGlCQUFBO0FBQUo7O0FBR0E7RUFDSSw0QkFBQTtFQUNBLGtCQUFBO0FBQUo7O0FBR0E7RUFDSSxpQkFBQTtFQUNBLG1CQUFBO0FBQUo7O0FBR0E7RUFDSSxhQUFBO0VBQ0EsZ0JBQUE7RUFDQSxXQUFBO0VBQ0EsbUJBQUE7RUFDQSxVQUFBO0VBQ0Esa0JBQUE7QUFBSjs7QUFHQTtFQUVJLGtCQUFBO0VBQ0EscUJBQUE7QUFESjs7QUFJQTtFQUNJLGtCQUFBO0VBQ0EsV0FBQTtFQUNBLGdCQUFBO0FBREo7O0FBSUE7RUFDSSxjQUFBO0VBQ0EsZ0JBQUE7RUFDQSxtQkFBQTtFQUNBLGlCQUFBO0VBQ0EsbUJBQUE7RUFDQSxzQkFBQTtFQUNBLFVBQUE7RUFDQSxrQkFBQTtBQURKOztBQUlBO0VBQ0ksYUFBQTtFQUNBLGVBQUE7RUFDQSxZQUFBO0VBQ0EsaUJBQUE7RUFDQSxXQUFBO0VBQ0EsV0FBQTtBQURKOztBQUlBO0VBQ0ksZUFBQTtFQUNBLGdCQUFBO0VBQ0EsbUJBQUE7RUFDQSxZQUFBO0VBQ0EseUJBQUE7QUFESjs7QUFJQTtFQUNJLGVBQUE7RUFDQSxnQkFBQTtFQUNBLG1CQUFBO0VBQ0EsWUFBQTtFQUNBLHlCQUFBO0FBREo7O0FBSUE7RUFDSSxrQkFBQTtFQUNBLFVBQUE7RUFDQSxxQkFBQTtFQUNBLGtCQUFBO0VBQ0EsZ0JBQUE7RUFDQSxtQkFBQTtFQUNBLHdCQUFBO0VBQ0EseUJBQUE7QUFESjs7QUFHSTtFQUNJLFlBQUE7RUFDQSxXQUFBO0FBRFI7O0FBS0E7RUFDSSxTQUFBO0VBQ0Esa0JBQUE7QUFGSjs7QUFLQTtFQUNJLHFCQUFBO0VBQ0Esa0JBQUE7RUFDQSxZQUFBO0VBQ0EsV0FBQTtFQUNBLGdCQUFBO0VBQ0EsbUJBQUE7RUFDQSx3QkFBQTtFQUNBLHlCQUFBO0FBRko7O0FBS0E7RUFDSSxrQkFBQTtFQUNBLFNBQUE7RUFDQSxXQUFBO0VBQ0EsVUFBQTtBQUZKOztBQUtBO0VBQ0ksa0JBQUE7RUFDQSxZQUFBO0VBQ0EsU0FBQTtFQUNBLFVBQUE7QUFGSjs7QUFRSTtFQUNJLFdBQUE7QUFMUjs7QUFVWTtFQUNJLFdBQUE7QUFSaEI7O0FBYVE7RUFDSSxnQkFBQTtBQVhaOztBQWNRO0VBQ0ksa0JBQUE7RUFDQSxTQUFBO0VBQ0EsZUFBQTtFQUNBLFdBQUE7QUFaWjs7QUFtQkk7RUFDSSxXQUFBO0FBaEJSOztBQXFCWTtFQUNJLGFBQUE7RUFDQSxtQkFBQTtFQUNBLHVCQUFBO0FBbkJoQjs7QUFxQmdCO0VBQ0ksdUJBQUE7RUFDQSxZQUFBO0FBbkJwQjs7QUEyQkE7RUFDSSwwQ0FBQTtFQUNBLGFBQUE7RUFDQSxrQkFBQTtFQUNBLGFBQUE7RUFDQSxZQUFBO0VBQ0EsYUFBQTtFQUNBLHNCQUFBO0VBQ0EsdUJBQUE7QUF4Qko7O0FBMEJJO0VBQ0ksa0JBQUE7RUFDQSxpQkFBQTtBQXhCUjs7QUEyQkk7RUFDSSxrQkFBQTtBQXpCUjs7QUE2QkE7RUFDSSxhQUFBO0VBQ0Esc0JBQUE7RUFDQSxtQkFBQTtBQTFCSjs7QUErQkE7RUFDSSxhQUFBO0VBQ0Esc0JBQUE7QUE1Qko7O0FBOEJJO0VBQ0ksa0JBQUE7QUE1QlI7O0FBK0JJO0VBQ0ksa0JBQUE7QUE3QlI7O0FBaUNBO0VBQ0ksYUFBQTtFQUNBLG1CQUFBO0VBQ0EsWUFBQTtBQTlCSjs7QUFnQ0k7RUFDSSxvQ0FBQTtFQUNBLFdBQUE7RUFDQSxXQUFBO0VBQ0Esa0JBQUE7QUE5QlI7O0FBaUNJO0VBQ0ksZ0JBQUE7RUFDQSxXQUFBO0VBQ0EsV0FBQTtFQUNBLGtCQUFBO0FBL0JSOztBQXNDQTtFQUNJLGFBQUE7RUFDQSx1QkFBQTtBQW5DSjs7QUF3Q1k7RUFDSSxhQUFBO0VBQ0EsU0FBQTtBQXRDaEI7O0FBeUNvQjtFQUNJLFdBQUE7RUFDQSxxQkFBQTtBQXZDeEI7O0FBMkNnQjtFQUNJLGFBQUE7RUFDQSx1QkFBQTtFQUNBLHNCQUFBO0FBekNwQjs7QUEyQ29CO0VBQ0ksV0FBQTtFQUNBLG1CQUFBO0VBQ0Esa0JBQUE7RUFDQSxrQkFBQTtBQXpDeEI7O0FBNENvQjtFQUNJLGtCQUFBO0FBMUN4Qjs7QUFzREk7RUFDSSxTQUFBO0FBbkRSOztBQXdESTtFQUVJLGFBQUE7RUFDQSxTQUFBO0VBQ0EsYUFBQTtFQUNBLHNCQUFBO0VBRUEsMkJBQUE7RUFDQSxTQUFBO0VBQ0EsYUFBQTtFQUNBLFlBQUE7QUF2RFI7O0FBK0RBO0VBQ0ksYUFBQTtFQUNBLG9CQUFBO0VBQ0EsaUJBQUE7RUFDQSxXQUFBO0FBN0RKOztBQStESTtFQUNJLG9CQUFBO0VBQ0Esa0JBQUE7RUFFQSxnQkFBQTtBQTlEUjs7QUFrRUk7RUFDSSxvQkFBQTtBQWhFUjs7QUFzRUE7RUFDSSxXQUFBO0FBbkVKOztBQXFFSTtFQUNJLGFBQUE7RUFDQSxtQkFBQTtFQUNBLHVCQUFBO0FBbkVSOztBQXVFZ0I7RUFDSSxpQkFBQTtBQXJFcEI7O0FBMEVRO0VBQ0ksaUJBQUE7RUFDQSxhQUFBO0VBQ0EsdUJBQUE7QUF4RVo7O0FBMEVZO0VBQ0ksV0FBQTtBQXhFaEI7O0FBK0VBO0VBQ0ksYUFBQTtFQUNBLHNCQUFBO0FBNUVKOztBQThFSTtFQUNJLDBDQUFBO0FBNUVSOztBQWlGSTtFQUNJLFdBQUE7QUE5RVI7O0FBa0ZnQjtFQUNJLGlCQUFBO0FBaEZwQjs7QUF3Rkk7RUFDSSxXQUFBO0VBQ0EsYUFBQTtFQUNBLHVCQUFBO0FBckZSOztBQTJGUTtFQUNJLG1CQUFBO0FBekZaOztBQTZGSTtFQUNJLFdBQUE7QUEzRlI7O0FBNkZRO0VBQ0ksV0FBQTtFQUNBLDBDQUFBO0FBM0ZaOztBQWdHUTtFQUNJLFNBQUE7RUFDQSxXQUFBO0FBOUZaOztBQWlHZ0I7RUFDSSxtQkFBQTtBQS9GcEI7O0FBd0dBO0VBQ0ksa0JBQUE7RUFDQSxPQUFBO0VBQ0EsUUFBQTtFQUNBLGdCQUFBO0FBckdKOztBQXVHSTtFQUNJLDBCQUFBO0VBQ0EsaUJBQUE7RUFDQSxXQUFBO0VBQ0EsZUFBQTtFQUNBLDRCQUFBO0VBQ0EsMkJBQUE7QUFyR1I7O0FBd0dJO0VBQ0ksMEJBQUE7RUFDQSxjQUFBO0VBQ0EsZUFBQTtFQUNBLHlCQUFBO0VBQ0Esd0JBQUE7QUF0R1I7O0FBOEdBO0VBQ0kseUNBQUE7QUEzR0o7O0FBOEdBO0VBQ0ksa0JBQUE7RUFDQSx5QkFBQTtFQUNBLGlCQUFBO0FBM0dKOztBQTRHSTtFQUNJLGFBQUE7QUExR1I7O0FBdUhBO0VBQ0ksMENBQUE7QUFwSEo7O0FBeUhBO0VBQ0ksbUJBQUE7RUFDQSxXQUFBO0FBdEhKOztBQXlIQTtFQUNJLGdCQUFBO0VBQ0EsV0FBQTtBQXRISiIsImZpbGUiOiJzcmMvYXBwL3BhZ2VzL3RhYnMvaG9tZS9ob21lLnBhZ2Uuc2NzcyIsInNvdXJjZXNDb250ZW50IjpbImlvbi10b29sYmFyIHtcbiAgICAtLWJhY2tncm91bmQ6ICMwMDA7XG59XG5cbmlvbi1pdGVtIHtcbiAgICAtLWlubmVyLWJvcmRlci13aWR0aDogMCAwIDBweCAwO1xufVxuXG5pb24tY2FyZCB7XG4gICAgYm94LXNoYWRvdzogcmdiKDAgMCAwIC8gMTIlKSAwcHggNHB4IDE2cHg7XG59XG5cbmlvbi1sYWJlbCB7XG4gICAgZm9udC1mYW1pbHk6ICdPc3dhbGQnICFpbXBvcnRhbnQ7XG59XG5cbi5zZWdtZW50X2NvbnRhaW5lcl9kYXRhIHtcbiAgICBwYWRkaW5nOiAwcHg7XG4gICAgbWFyZ2luLWJvdHRvbTogMjUlO1xufVxuXG4uY2FyZF9idG4ge1xuICAgIGhlaWdodDogNDBweDtcbiAgICB3aWR0aDogNDBweDtcbiAgICBib3JkZXI6IDJweCBzb2xpZCAjNTY1NjU2O1xuICAgIG1hcmdpbjogMDtcbiAgICBkaXNwbGF5OiBmbGV4O1xuICAgIGp1c3RpZnktY29udGVudDogY2VudGVyO1xuICAgIG1hcmdpbi1yaWdodDogNXB4O1xuXG4gICAgaW9uLWxhYmVsIHtcbiAgICAgICAgYWxpZ24tc2VsZjogY2VudGVyO1xuICAgICAgICBmb250LXNpemU6IDEuMnJlbTtcbiAgICAgICAgZm9udC13ZWlnaHQ6IDYwMDtcbiAgICAgICAgY29sb3I6ICMwMDA7XG4gICAgfVxufVxuXG4uYWN0aXZlX2J0biB7XG4gICAgYmFja2dyb3VuZDogdmFyKC0taW9uLWJ0bi1jdXN0b20tY29sb3IpO1xufVxuXG4uYWN0aXZlLWV4ZXJjaXNlIHtcbiAgICBmb250LXNpemU6IDAuOHJlbTtcbn1cblxuLmV4ZXJjaXNlLWRldGFpbCB7XG4gICAgZm9udC1zaXplOiAwLjdyZW0gIWltcG9ydGFudDtcbiAgICBtYXJnaW4tcmlnaHQ6IDM4cHg7XG59XG5cbi5lZGl0LWljb24ge1xuICAgIGZvbnQtc2l6ZTogMS4ycmVtO1xuICAgIG1hcmdpbi1yaWdodDogMTAwcHg7XG59XG5cbi5leGVyY2lzZS1oZWFkIHtcbiAgICBkaXNwbGF5OiBmbGV4O1xuICAgIGJhY2tncm91bmQ6ICMwMDA7XG4gICAgY29sb3I6ICNmZmY7XG4gICAgYm9yZGVyLXJhZGl1czogMTVweDtcbiAgICB3aWR0aDogNDAlO1xuICAgIG1hcmdpbi1yaWdodDogMTVweDtcbn1cblxuaW9uLWl0ZW0ge1xuICAgIC8vIC0tcGFkZGluZy1zdGFydDogMnB4O1xuICAgIC0tcGFkZGluZy10b3A6IDJweDtcbiAgICAtLXBhZGRpbmctYm90dG9tOiAycHg7XG59XG5cbi5kYXlib3gge1xuICAgIHRleHQtYWxpZ246IGNlbnRlcjtcbiAgICBjb2xvcjogI2ZmZjtcbiAgICBtYXJnaW4tdG9wOiAxNXB4O1xufVxuXG4uZGF5Ym94LXRpdGxlIHtcbiAgICBkaXNwbGF5OiBibG9jaztcbiAgICBtaW4td2lkdGg6IDEwMHB4O1xuICAgIGJhY2tncm91bmQ6ICM2MjYyNjI7XG4gICAgcGFkZGluZzogM3B4IDIwcHg7XG4gICAgYm9yZGVyLXJhZGl1czogMjVweDtcbiAgICBib3JkZXI6IDJweCBzb2xpZCAjMzMzO1xuICAgIHotaW5kZXg6IDE7XG4gICAgcG9zaXRpb246IHJlbGF0aXZlO1xufVxuXG4uaGVhZGVyLWJ1dHRvbi1ib3gge1xuICAgIGRpc3BsYXk6IGZsZXg7XG4gICAgcG9zaXRpb246IGZpeGVkO1xuICAgIHotaW5kZXg6IDk5OTtcbiAgICBtYXJnaW4tdG9wOiAtMTRweDtcbiAgICBjb2xvcjogI2ZmZjtcbiAgICB3aWR0aDogMTAwJTtcbn1cblxuLmhlYWRlci1idXR0b24tYm94LWJ1dHRvbi1yaWdodCBpb24taWNvbiB7XG4gICAgZm9udC1zaXplOiAyNnB4O1xuICAgIGJhY2tncm91bmQ6ICMzMzM7XG4gICAgYm9yZGVyLXJhZGl1czogMjVweDtcbiAgICBwYWRkaW5nOiAwcHg7XG4gICAgYm9yZGVyOiAxcHggc29saWQgI2ZmZDFiMztcbn1cblxuLmhlYWRlci1idXR0b24tYm94LWJ1dHRvbi1sZWZ0IGlvbi1pY29uIHtcbiAgICBmb250LXNpemU6IDI2cHg7XG4gICAgYmFja2dyb3VuZDogIzMzMztcbiAgICBib3JkZXItcmFkaXVzOiAyNXB4O1xuICAgIHBhZGRpbmc6IDBweDtcbiAgICBib3JkZXI6IDFweCBzb2xpZCAjZmZkMWIzO1xufVxuXG4uaGVhZGVyLWJ1dHRvbi1ib3gtYnV0dG9uLXJpZ2h0IHtcbiAgICBwb3NpdGlvbjogYWJzb2x1dGU7XG4gICAgcmlnaHQ6IDEyJTtcbiAgICBkaXNwbGF5OiBpbmxpbmUtYmxvY2s7XG4gICAgbWFyZ2luLXJpZ2h0OiAxNXB4O1xuICAgIGJhY2tncm91bmQ6ICMzMzM7XG4gICAgYm9yZGVyLXJhZGl1czogNDBweDtcbiAgICBwYWRkaW5nOiA0cHggNHB4IDBweCA1cHg7XG4gICAgYm9yZGVyOiAxcHggc29saWQgI2ZmZDFiMztcblxuICAgIGltZyB7XG4gICAgICAgIGhlaWdodDogMjVweDtcbiAgICAgICAgd2lkdGg6IDI1cHg7XG4gICAgfVxufVxuXG4uaGVhZGVyLWJ1dHRvbi1ib3gtYnV0dG9uLWxlZnQge1xuICAgIGxlZnQ6IDEwJTtcbiAgICBwb3NpdGlvbjogYWJzb2x1dGU7XG59XG5cbi5oZWFkZXItYnV0dG9uLWJveC1idXR0b24tbGVmdCAuaGVhZGVyLWJ1dHRvbi1ib3gtYnV0dG9uIHtcbiAgICBkaXNwbGF5OiBpbmxpbmUtYmxvY2s7XG4gICAgbWFyZ2luLXJpZ2h0OiAxNXB4O1xuICAgIGhlaWdodDogMzVweDtcbiAgICB3aWR0aDogMzVweDtcbiAgICBiYWNrZ3JvdW5kOiAjMzMzO1xuICAgIGJvcmRlci1yYWRpdXM6IDQwcHg7XG4gICAgcGFkZGluZzogNXB4IDhweCAwcHggOHB4O1xuICAgIGJvcmRlcjogMXB4IHNvbGlkICNmZmQxYjM7XG59XG5cbi5iYWNrLWljb24ge1xuICAgIHBvc2l0aW9uOiBhYnNvbHV0ZTtcbiAgICB0b3A6IDI1cHg7XG4gICAgbGVmdDogMTEwcHg7XG4gICAgei1pbmRleDogMjtcbn1cblxuLmZvcndhcmQtaWNvbiB7XG4gICAgcG9zaXRpb246IGFic29sdXRlO1xuICAgIHJpZ2h0OiAxMTBweDtcbiAgICB0b3A6IDI1cHg7XG4gICAgei1pbmRleDogMjtcbn1cblxuXG5cbi5saXN0X2RldmljZV9yb3cge1xuICAgIGlvbi1pdGVtIHtcbiAgICAgICAgd2lkdGg6IDEwMCU7XG5cblxuXG4gICAgICAgIGRpdiB7XG4gICAgICAgICAgICBpb24taW1nIHtcbiAgICAgICAgICAgICAgICB3aWR0aDogNTBweDtcbiAgICAgICAgICAgIH1cbiAgICAgICAgfVxuXG5cbiAgICAgICAgLmxpc3Rfc2VjdGlvbl90aXRsZSB7XG4gICAgICAgICAgICBwYWRkaW5nLWxlZnQ6IDIlO1xuICAgICAgICB9XG5cbiAgICAgICAgLm1lbnUtaWNvbiB7XG4gICAgICAgICAgICBwb3NpdGlvbjogYWJzb2x1dGU7XG4gICAgICAgICAgICByaWdodDogNyU7XG4gICAgICAgICAgICBmb250LXNpemU6IDI1cHg7XG4gICAgICAgICAgICBjb2xvcjogZ3JheTtcbiAgICAgICAgfVxuICAgIH1cbn1cblxuXG4uYWxpbWVudGF0aW9uX3Jvd19vbmUge1xuICAgIGlvbi1jYXJkIHtcbiAgICAgICAgd2lkdGg6IDEwMCU7XG4gICAgICAgIC8vIGhlaWdodDogMTYxcHg7XG4gICAgICAgIFxuXG4gICAgICAgIGlvbi1jYXJkLWNvbnRlbnQge1xuICAgICAgICAgICAgaW9uLXJvdyB7XG4gICAgICAgICAgICAgICAgZGlzcGxheTogZmxleDtcbiAgICAgICAgICAgICAgICBmbGV4LWRpcmVjdGlvbjogcm93O1xuICAgICAgICAgICAgICAgIGp1c3RpZnktY29udGVudDogY2VudGVyO1xuXG4gICAgICAgICAgICAgICAgZGl2IHtcbiAgICAgICAgICAgICAgICAgICAganVzdGlmeS1jb250ZW50OiBjZW50ZXI7XG4gICAgICAgICAgICAgICAgICAgIGZsZXgtZ3JvdzogMTtcbiAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICB9XG4gICAgICAgIH1cbiAgICB9XG59XG5cblxuLmNpcmNsZS1jb250YWluZXIge1xuICAgIGJvcmRlcjogM3B4IHNvbGlkIHZhcigtLWlvbi1jb2xvci1wcmltYXJ5KTtcbiAgICBwYWRkaW5nOiAxNHB4O1xuICAgIGJvcmRlci1yYWRpdXM6IDUwJTtcbiAgICBoZWlnaHQ6IDExMHB4O1xuICAgIHdpZHRoOiAxMTBweDtcbiAgICBkaXNwbGF5OiBmbGV4O1xuICAgIGZsZXgtZGlyZWN0aW9uOiBjb2x1bW47XG4gICAganVzdGlmeS1jb250ZW50OiBjZW50ZXI7XG5cbiAgICAuc21hbGxfdGV4dCB7XG4gICAgICAgIHRleHQtYWxpZ246IGNlbnRlcjtcbiAgICAgICAgZm9udC1zaXplOiAwLjVyZW07XG4gICAgfVxuXG4gICAgLmxhcmdlX3RleHRfYm9sZCB7XG4gICAgICAgIHRleHQtYWxpZ246IGNlbnRlcjtcbiAgICB9XG59XG5cbi5pbm5lci1jb250YWluZXIge1xuICAgIGRpc3BsYXk6IGZsZXg7XG4gICAgZmxleC1kaXJlY3Rpb246IGNvbHVtbjtcbiAgICBhbGlnbi1pdGVtczogY2VudGVyO1xuXG59XG5cblxuLnNpZGUtY29udGFpbmVyIHtcbiAgICBkaXNwbGF5OiBmbGV4O1xuICAgIGZsZXgtZGlyZWN0aW9uOiBjb2x1bW47XG5cbiAgICAubGFyZ2VfdGV4dF9ib2xkIHtcbiAgICAgICAgdGV4dC1hbGlnbjogY2VudGVyO1xuICAgIH1cblxuICAgIC5leHRyYV9zbWFsbF90ZXh0IHtcbiAgICAgICAgdGV4dC1hbGlnbjogY2VudGVyO1xuICAgIH1cbn1cblxuLm1haW5fbGluZV9zZWN0aW9uIHtcbiAgICBkaXNwbGF5OiBmbGV4O1xuICAgIGZsZXgtZGlyZWN0aW9uOiByb3c7XG4gICAgaGVpZ2h0OiAyNHB4O1xuXG4gICAgLnllbGxvdy1iYWNrIHtcbiAgICAgICAgYmFja2dyb3VuZDogdmFyKC0taW9uLWNvbG9yLXByaW1hcnkpO1xuICAgICAgICB3aWR0aDogMjBweDtcbiAgICAgICAgaGVpZ2h0OiA0cHg7XG4gICAgICAgIGFsaWduLXNlbGY6IGNlbnRlcjtcbiAgICB9XG5cbiAgICAuYmxhY2stYmFjayB7XG4gICAgICAgIGJhY2tncm91bmQ6ICMwMDA7XG4gICAgICAgIHdpZHRoOiAyMHB4O1xuICAgICAgICBoZWlnaHQ6IDRweDtcbiAgICAgICAgYWxpZ24tc2VsZjogY2VudGVyO1xuICAgIH1cbn1cblxuXG5cblxuLmNhcmRfaXRlbV9zZWN0aW9uX21haW5fcm93IHtcbiAgICBkaXNwbGF5OiBmbGV4O1xuICAgIGp1c3RpZnktY29udGVudDogY2VudGVyO1xuXG4gICAgLmNhcmRfaXRlbV9zZWN0aW9uIHtcblxuICAgICAgICBpb24tY29sIHtcbiAgICAgICAgICAgIGlvbi1jYXJkIHtcbiAgICAgICAgICAgICAgICBoZWlnaHQ6IDE1MHB4O1xuICAgICAgICAgICAgICAgIG1hcmdpbjogMDtcblxuICAgICAgICAgICAgICAgIGlvbi1jYXJkLWhlYWRlciB7XG4gICAgICAgICAgICAgICAgICAgIGlvbi1idXR0b24ge1xuICAgICAgICAgICAgICAgICAgICAgICAgd2lkdGg6IDEwMCU7XG4gICAgICAgICAgICAgICAgICAgICAgICAtLWJhY2tncm91bmQ6ICMyYTJhMmE7XG4gICAgICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgICB9XG5cbiAgICAgICAgICAgICAgICBpb24tY2FyZC1jb250ZW50IHtcbiAgICAgICAgICAgICAgICAgICAgZGlzcGxheTogZmxleDtcbiAgICAgICAgICAgICAgICAgICAganVzdGlmeS1jb250ZW50OiBjZW50ZXI7XG4gICAgICAgICAgICAgICAgICAgIGZsZXgtZGlyZWN0aW9uOiBjb2x1bW47XG5cbiAgICAgICAgICAgICAgICAgICAgaW9uLWltZyB7XG4gICAgICAgICAgICAgICAgICAgICAgICB3aWR0aDogMjVweDtcbiAgICAgICAgICAgICAgICAgICAgICAgIGJhY2tncm91bmQ6ICNiZWJlYmU7XG4gICAgICAgICAgICAgICAgICAgICAgICBib3JkZXItcmFkaXVzOiA1MCU7XG4gICAgICAgICAgICAgICAgICAgICAgICBhbGlnbi1zZWxmOiBjZW50ZXI7XG4gICAgICAgICAgICAgICAgICAgIH1cblxuICAgICAgICAgICAgICAgICAgICBwIHtcbiAgICAgICAgICAgICAgICAgICAgICAgIGFsaWduLXNlbGY6IGNlbnRlcjtcblxuICAgICAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgfVxuICAgICAgICB9XG4gICAgfVxuXG59XG5cblxuLmNhcmRfZm9vdGVyX2RhdGEge1xuICAgIHAge1xuICAgICAgICBtYXJnaW46IDA7XG4gICAgfVxufVxuXG4uc3RlcHR3b2ZpcnN0YnRuIHtcbiAgICBpb24tY2FyZCB7XG5cbiAgICAgICAgaGVpZ2h0OiAxMDBweDtcbiAgICAgICAgbWFyZ2luOiAwO1xuICAgICAgICBkaXNwbGF5OiBmbGV4O1xuICAgICAgICBmbGV4LWRpcmVjdGlvbjogY29sdW1uO1xuXG4gICAgICAgIGp1c3RpZnktY29udGVudDogZmxleC1zdGFydDtcbiAgICAgICAgbWFyZ2luOiAwO1xuICAgICAgICBkaXNwbGF5OiBmbGV4O1xuICAgICAgICBwYWRkaW5nOiA4cHg7XG5cblxuICAgICAgICBpb24tbGFiZWwge31cbiAgICB9XG59XG5cblxuLnNlY29uZF9oZWFkZXJfc2VjdGlvbiB7XG4gICAgZGlzcGxheTogZmxleDtcbiAgICBhbGlnbi1zZWxmOiBmbGV4LWVuZDtcbiAgICBtYXJnaW4tbGVmdDogMTBweDtcbiAgICB3aWR0aDogMTAwJTtcblxuICAgIC5zbWFsbF90ZXh0IHtcbiAgICAgICAgYWxpZ24tc2VsZjogZmxleC1lbmQ7XG4gICAgICAgIG1hcmdpbi1ib3R0b206IDFweDtcblxuICAgICAgICBtYXJnaW4tbGVmdDogNXB4O1xuXG4gICAgfVxuXG4gICAgLmxhcmdlX3RleHRfYm9sZCB7XG4gICAgICAgIGFsaWduLXNlbGY6IGZsZXgtZW5kO1xuXG4gICAgfVxuXG59XG5cbi5kZXRhaWxzX3NlYWN0aW9uX3JvdyB7XG4gICAgd2lkdGg6IDEwMCU7XG5cbiAgICAuZGV0YWlsc19zZWFjdGlvbl9jb2wge1xuICAgICAgICBkaXNwbGF5OiBmbGV4O1xuICAgICAgICBmbGV4LWRpcmVjdGlvbjogcm93O1xuICAgICAgICBqdXN0aWZ5LWNvbnRlbnQ6IGNlbnRlcjtcblxuICAgICAgICAuZGV0YWlsc19zZWN0aW9uIHtcbiAgICAgICAgICAgIGRpdiB7XG4gICAgICAgICAgICAgICAgLnNtYWxsX3RleHQge1xuICAgICAgICAgICAgICAgICAgICBwYWRkaW5nLWxlZnQ6IDdweDtcbiAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICB9XG4gICAgICAgIH1cblxuICAgICAgICAuc3RlcF9pY29uX3NlY3Rpb24ge1xuICAgICAgICAgICAgcGFkZGluZy1sZWZ0OiAxMCU7XG4gICAgICAgICAgICBkaXNwbGF5OiBmbGV4O1xuICAgICAgICAgICAganVzdGlmeS1jb250ZW50OiBjZW50ZXI7XG5cbiAgICAgICAgICAgIGlvbi1pbWcge1xuICAgICAgICAgICAgICAgIHdpZHRoOiA0MHB4O1xuICAgICAgICAgICAgfVxuICAgICAgICB9XG4gICAgfVxufVxuXG5cbi5idXR0b25fc2VjdGlvbl9yb3cge1xuICAgIGRpc3BsYXk6IGZsZXg7XG4gICAgZmxleC1kaXJlY3Rpb246IGNvbHVtbjtcblxuICAgIC5kZXRhaWxzX3NlY3Rpb24ge1xuICAgICAgICAtLWJhY2tncm91bmQ6IHZhcigtLWlvbi1idG4tY3VzdG9tLWNvbG9yKVxuICAgIH1cbn1cblxuLnNlY29uZF9idG5faGVhZGVyX3NlY3Rpb24ge1xuICAgIGlvbi1yb3cge1xuICAgICAgICB3aWR0aDogMTAwJTtcblxuICAgICAgICBpb24tY29sIHtcbiAgICAgICAgICAgIC5maXJzdF9oZWFkZXIge1xuICAgICAgICAgICAgICAgIC5zbWFsbF90ZXh0IHtcbiAgICAgICAgICAgICAgICAgICAgcGFkZGluZy1sZWZ0OiA1cHg7XG4gICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgfVxuICAgICAgICB9XG4gICAgfVxufVxuXG4uYWxtZW50YXRpb25fd2l0aG91dF9sb2dpbl9zZWN0aW9uIHtcbiAgICAuaGVhZGVyX3NlY3Rpb24ge1xuICAgICAgICB3aWR0aDogMTAwJTtcbiAgICAgICAgZGlzcGxheTogZmxleDtcbiAgICAgICAganVzdGlmeS1jb250ZW50OiBjZW50ZXI7XG5cbiAgICB9XG5cblxuICAgIC5jb250ZW50X3NlY3Rpb24ge1xuICAgICAgICBpb24tbGFiZWwge1xuICAgICAgICAgICAgdGV4dC1hbGlnbjoganVzdGlmeTtcbiAgICAgICAgfVxuICAgIH1cblxuICAgIC5idXR0b25fc2VjdGlvbiB7XG4gICAgICAgIHdpZHRoOiAxMDAlO1xuXG4gICAgICAgIGlvbi1idXR0b24ge1xuICAgICAgICAgICAgd2lkdGg6IDEwMCU7XG4gICAgICAgICAgICAtLWJhY2tncm91bmQ6IHZhcigtLWlvbi1idG4tY3VzdG9tLWNvbG9yKVxuICAgICAgICB9XG4gICAgfVxuXG4gICAgLmNhcmRfc2VjdGlvbiB7XG4gICAgICAgIGlvbi1jYXJkIHtcbiAgICAgICAgICAgIG1hcmdpbjogMDtcbiAgICAgICAgICAgIHBhZGRpbmc6IDElO1xuXG4gICAgICAgICAgICBpb24tY2FyZC1jb250ZW50IHtcbiAgICAgICAgICAgICAgICBwIHtcbiAgICAgICAgICAgICAgICAgICAgdGV4dC1hbGlnbjoganVzdGlmeTtcbiAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICB9XG4gICAgICAgIH1cbiAgICB9XG59XG5cblxuXG4ucHJpY2Vfc2VjdGlvbiB7XG4gICAgcG9zaXRpb246IGFic29sdXRlO1xuICAgIHRvcDogNCU7XG4gICAgbGVmdDogNCU7XG4gICAgdGV4dC1hbGlnbjogbGVmdDtcblxuICAgIC5wMSB7XG4gICAgICAgIGZvbnQtZmFtaWx5OiBPc3dhbGQsIHNlcmlmO1xuICAgICAgICBmb250LXdlaWdodDogYm9sZDtcbiAgICAgICAgY29sb3I6ICNmZmY7XG4gICAgICAgIGZvbnQtc2l6ZTogMTZweDtcbiAgICAgICAgcGFkZGluZy1ib3R0b206IDAgIWltcG9ydGFudDtcbiAgICAgICAgbWFyZ2luLWJvdHRvbTogMCAhaW1wb3J0YW50O1xuICAgIH1cblxuICAgIC5wMiB7XG4gICAgICAgIGZvbnQtZmFtaWx5OiBPc3dhbGQsIHNlcmlmO1xuICAgICAgICBjb2xvcjogI2QyZDFkMTtcbiAgICAgICAgZm9udC1zaXplOiAxMHB4O1xuICAgICAgICBwYWRkaW5nLXRvcDogMCAhaW1wb3J0YW50O1xuICAgICAgICBtYXJnaW4tdG9wOiAwICFpbXBvcnRhbnQ7XG4gICAgfVxuXG5cblxufVxuXG5cbi5zZWxlY3QtcGxhbiB7XG4gICAgLS1iYWNrZ3JvdW5kOiB2YXIoLS1pb24tYnRuLWN1c3RvbS1jb2xvcik7XG59XG5cbmlvbi1yYW5nZSAge1xuICAgIC0tYmFyLWhlaWdodDogMTBweDtcbiAgICAtLWJhci1ib3JkZXItcmFkaXVzOiAxNXB4O1xuICAgIC0ta25vYi1zaXplOiA1MHB4O1xuICAgIGlvbi1sYWJlbCB7XG4gICAgICAgIHBhZGRpbmc6IDEwcHg7XG4gICAgfVxufVxuLy8gaW9uLXJhbmdlIHtcbi8vICAgICAtLWtub2ItYmFja2dyb3VuZDogdXJsKCcuLi8uLi8uLi8uLi9hc3NldHMvaWNvbi9lYXVrbm9iLnBuZycpO1xuLy8gICAgICBiYWNrZ3JvdW5kLXNpemU6IGNvbnRhaW47XG4vLyAgICAgIGJhY2tncm91bmQtcmVwZWF0OiBuby1yZXBlYXQ7XG4vLyAgICAgIGJhY2tncm91bmQ6IHJlZCAhaW1wb3J0YW50O1xuLy8gICAgICBib3gtc2hhZG93OiBub25lICFpbXBvcnRhbnQ7XG4vLyB9XG5cblxuXG4uY3VzdG9tX2J0bl9iZyB7XG4gICAgLS1iYWNrZ3JvdW5kOiB2YXIoLS1pb24tYnRuLWN1c3RvbS1jb2xvcilcbn1cblxuXG5cbi5jdXN0b21fY2FyZF9iZyB7XG4gICAgYmFja2dyb3VuZDogIzJkMmQyZDtcbiAgICBjb2xvcjogI2ZmZjtcbn1cblxuLmN1c3RvbV9jYXJkX2JnX2xpZ2h0IHtcbiAgICBiYWNrZ3JvdW5kOiAjZmZmO1xuICAgIGNvbG9yOiAjMDAwO1xufSJdfQ== */");

/***/ }),

/***/ "./src/app/pages/tabs/home/home.page.ts":
/*!**********************************************!*\
  !*** ./src/app/pages/tabs/home/home.page.ts ***!
  \**********************************************/
/*! exports provided: HomePage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "HomePage", function() { return HomePage; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");
/* harmony import */ var _components_modals_check_up_modals_check_up_modals_component__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @components/modals/check-up-modals/check-up-modals.component */ "./src/app/components/modals/check-up-modals/check-up-modals.component.ts");
/* harmony import */ var _components_modals_courses_modal_courses_modal_component__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @components/modals/courses-modal/courses-modal.component */ "./src/app/components/modals/courses-modal/courses-modal.component.ts");
/* harmony import */ var _components_modals_notebook_modal_notebook_modal_component__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @components/modals/notebook-modal/notebook-modal.component */ "./src/app/components/modals/notebook-modal/notebook-modal.component.ts");
/* harmony import */ var _components_modals_my_diet_home_modal_my_diet_home_modal_component__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @components/modals/my-diet-home-modal/my-diet-home-modal.component */ "./src/app/components/modals/my-diet-home-modal/my-diet-home-modal.component.ts");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/__ivy_ngcc__/fesm2015/ionic-angular.js");
/* harmony import */ var _services_userdata_service__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ../../../services/userdata.service */ "./src/app/services/userdata.service.ts");
/* harmony import */ var _components_modals_calendar_modal_calendar_modal__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ./../../../components/modals/calendar-modal/calendar-modal */ "./src/app/components/modals/calendar-modal/calendar-modal.ts");











let HomePage = class HomePage {
    constructor(userData, modalCtrl, router, zone) {
        this.userData = userData;
        this.modalCtrl = modalCtrl;
        this.router = router;
        this.zone = zone;
        this.creationMode = "deux";
        this.plansBtn = "Gain muscle";
        this.lastY = 0;
        this.hidden = false;
        this.triggerDistance = 10;
        this.segmentModel = "training";
        this.detail = false;
        this.days = [];
        this.eauMinRange = 0;
        this.eauChangeRange = 20;
        this.sommeilMinRange = 0;
        this.sommeilChangeRange = 15;
        this.options = {
            centeredSlides: true,
            slidesPerView: 1,
            spaceBetween: 0,
            initialSlide: 5,
            on: {
                beforeInit() {
                    const swiper = this;
                    swiper.classNames.push(`${swiper.params.containerModifierClass}fade`);
                    const overwriteParams = {
                        slidesPerView: 1,
                        slidesPerColumn: 1,
                        slidesPerGroup: 1,
                        watchSlidesProgress: true,
                        spaceBetween: 0,
                        virtualTranslate: true,
                    };
                    swiper.params = Object.assign(swiper.params, overwriteParams);
                    swiper.params = Object.assign(swiper.originalParams, overwriteParams);
                },
                setTranslate() {
                    const swiper = this;
                    const { slides } = swiper;
                    for (let i = 0; i < slides.length; i += 1) {
                        const $slideEl = swiper.slides.eq(i);
                        const offset$$1 = $slideEl[0].swiperSlideOffset;
                        let tx = -offset$$1;
                        if (!swiper.params.virtualTranslate)
                            tx -= swiper.translate;
                        let ty = 0;
                        if (!swiper.isHorizontal()) {
                            ty = tx;
                            tx = 0;
                        }
                        const slideOpacity = swiper.params.fadeEffect.crossFade
                            ? Math.max(1 - Math.abs($slideEl[0].progress), 0)
                            : 1 + Math.min(Math.max($slideEl[0].progress, -1), 0);
                        $slideEl
                            .css({
                            opacity: slideOpacity,
                        })
                            .transform(`translate3d(${tx}px, ${ty}px, 0px)`);
                    }
                },
                setTransition(duration) {
                    const swiper = this;
                    const { slides, $wrapperEl } = swiper;
                    slides.transition(duration);
                    if (swiper.params.virtualTranslate && duration !== 0) {
                        let eventTriggered = false;
                        slides.transitionEnd(() => {
                            if (eventTriggered)
                                return;
                            if (!swiper || swiper.destroyed)
                                return;
                            eventTriggered = true;
                            swiper.animating = false;
                            const triggerEvents = ['webkitTransitionEnd', 'transitionend'];
                            for (let i = 0; i < triggerEvents.length; i += 1) {
                                $wrapperEl.trigger(triggerEvents[i]);
                            }
                        });
                    }
                },
            }
        };
        this.routerWatch();
    }
    ngOnInit() {
        const date = new Date();
        this.days = this.getDaysInMonth(date.getMonth(), date.getFullYear());
        console.log(this.slider);
    }
    ionViewDidEnter() {
        console.log("cc", this.userData.showActivity);
        this.show = this.userData.showActivity;
    }
    ionViewWillEnter() {
        console.log("cc", this.userData.showActivity);
        this.show = this.userData.showActivity;
    }
    openCheckupModal() {
        return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, function* () {
            const modal = yield this.modalCtrl.create({
                component: _components_modals_check_up_modals_check_up_modals_component__WEBPACK_IMPORTED_MODULE_3__["CheckUpModalsComponent"],
                cssClass: 'check-up-modal',
                showBackdrop: false,
            });
            return yield modal.present();
        });
    }
    openNotebookModal() {
        return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, function* () {
            const modal = yield this.modalCtrl.create({
                component: _components_modals_notebook_modal_notebook_modal_component__WEBPACK_IMPORTED_MODULE_5__["NotebookModalComponent"],
                cssClass: 'check-up-modal',
                showBackdrop: false,
                backdropDismiss: true,
            });
            return yield modal.present();
        });
    }
    openCourseModal() {
        return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, function* () {
            const modal = yield this.modalCtrl.create({
                component: _components_modals_courses_modal_courses_modal_component__WEBPACK_IMPORTED_MODULE_4__["CoursesModalComponent"],
                cssClass: 'check-up-modal',
                showBackdrop: false,
            });
            return yield modal.present();
        });
    }
    openMyDietHomeModal() {
        return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, function* () {
            const modal = yield this.modalCtrl.create({
                component: _components_modals_my_diet_home_modal_my_diet_home_modal_component__WEBPACK_IMPORTED_MODULE_6__["MyDietHomeModalComponent"],
                cssClass: 'check-up-modal',
                showBackdrop: false,
            });
            return yield modal.present();
        });
    }
    eauChange(event) {
        this.eauChangeRange = event.detail.value;
    }
    sommeilChange(event) {
        this.sommeilChangeRange = event.detail.value;
    }
    ngAfterViewInit() {
        this.goToSlide();
    }
    segmentChanged(event) {
        console.log(this.segmentModel);
        console.log(event);
    }
    showDetail() {
        this.detail = !this.detail;
    }
    openCalendar() {
        return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, function* () {
            const modal = yield this.modalCtrl.create({
                component: _components_modals_calendar_modal_calendar_modal__WEBPACK_IMPORTED_MODULE_9__["CalendarModal"],
                cssClass: 'calendar-style',
                backdropDismiss: true,
                showBackdrop: false,
                componentProps: {
                    'startDate': new Date(),
                }
            });
            return yield modal.present();
        });
    }
    /**
     * @param {number} The month number, 0 based
     * @param {number} The year, not zero based, required to account for leap years
     * @return {Date[]} List with date objects for each day of the month
     */
    getDaysInMonth(month, year) {
        const date = new Date(year, month, 1);
        const days = [];
        const today = new Date();
        while (date.getMonth() === month) {
            days.push({ date: new Date(date) });
            date.setDate(date.getDate() + 1);
        }
        days.forEach((day, index) => {
            const date = new Date(day.date);
            // days[index]["monthName"] = date.toLocaleString("default", {
            //   month: "short",
            // });
            days[index]["dayName"] = date.toLocaleString("default", {
                weekday: "short",
            });
            days[index]["day"] = date.toLocaleString("default", { day: "2-digit" });
            // days[index]["data"] = this.getRandomInt();
            // days[index]["color"] = "#ff9900";
            if (date.getDate() === today.getDate()) {
                days[index]["isToday"] = true;
                days[index]["dayName"] = 'Today';
            }
            else {
                days[index]["isToday"] = false;
            }
        });
        console.log("days", days);
        return days;
    }
    goToSlide() {
        return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, function* () {
            let currentIndex = yield this.slider.getActiveIndex();
            let index = this.days.findIndex(x => x.isToday === true);
            this.slider.slideTo(index, 1000);
        });
    }
    goBack() {
        return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, function* () {
            let currentIndex = yield this.slider.getActiveIndex();
            this.slider.slideTo(currentIndex - 1, 1000);
        });
    }
    goAhead() {
        return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, function* () {
            let currentIndex = yield this.slider.getActiveIndex();
            this.slider.slideTo(currentIndex + 1, 1000);
        });
    }
    routerWatch() {
        this.routerSubscription = this.router.events.subscribe((event) => {
            if (event instanceof _angular_router__WEBPACK_IMPORTED_MODULE_2__["NavigationEnd"]) {
                if (event.url == '/tabs/home') {
                    console.log(111);
                    this.show = this.userData.showActivity;
                }
            }
        });
    }
    ionPageWillLeave() {
        this.routerSubscription.unsubscribe();
    }
    scrollHandler(event) {
        this.zone.run(() => {
            console.log("event", event);
            let a = 0;
            let elem1 = document.getElementById('chart');
            let elem = document.querySelector('ion-tab-bar');
            // elem.style.setProperty('transition',"0.5s");
            if (event.detail.scrollTop > a) {
                // this.hide();
                // elem1.style.setProperty('display',"none");
                elem.style.setProperty('margin-bottom', "-100px");
                // event.detail.scrollTop = event.detail.scrollTop
            }
            else {
                // this.show1();
                // elem1.style.setProperty('display',"block");
                elem.style.setProperty('margin-bottom', "10px");
            }
            a = event.detail.scrollTop;
        });
    }
};
HomePage.ctorParameters = () => [
    { type: _services_userdata_service__WEBPACK_IMPORTED_MODULE_8__["UserdataService"] },
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_7__["ModalController"] },
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"] },
    { type: _angular_core__WEBPACK_IMPORTED_MODULE_1__["NgZone"] }
];
HomePage.propDecorators = {
    slider: [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_1__["ViewChild"], args: ['myslide',] }]
};
HomePage = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-home',
        template: Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(/*! raw-loader!./home.page.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/pages/tabs/home/home.page.html")).default,
        styles: [Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(/*! ./home.page.scss */ "./src/app/pages/tabs/home/home.page.scss")).default]
    })
], HomePage);



/***/ })

}]);
//# sourceMappingURL=home-home-module-es2015.js.map