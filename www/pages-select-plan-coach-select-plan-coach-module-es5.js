(function () {
  function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

  function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

  function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

  (window["webpackJsonp"] = window["webpackJsonp"] || []).push([["pages-select-plan-coach-select-plan-coach-module"], {
    /***/
    "./node_modules/raw-loader/dist/cjs.js!./src/app/pages/select-plan-coach/select-plan-coach.page.html":
    /*!***********************************************************************************************************!*\
      !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/pages/select-plan-coach/select-plan-coach.page.html ***!
      \***********************************************************************************************************/

    /*! exports provided: default */

    /***/
    function node_modulesRawLoaderDistCjsJsSrcAppPagesSelectPlanCoachSelectPlanCoachPageHtml(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony default export */


      __webpack_exports__["default"] = "\n\n\n<ion-content>\n\n  <ion-row class=\"stepper_section\">\n    <ion-grid>\n      <ion-row>\n        <ion-col>\n          <div [class]=\"activepage == 0 ? 'steps active' : 'steps' \"></div>\n        </ion-col>\n        <ion-col>\n          <!-- <div ng-class=\"(activepage == 2) ? 'active' : 'steps'\"></div> -->\n          <div [class]=\"activepage == 1 ? 'steps active' : 'steps' \"></div>\n        </ion-col>\n        <ion-col>\n          <div [class]=\"activepage == 2 ? 'steps active' : 'steps' \"></div>\n        </ion-col>\n  \n      </ion-row>\n    </ion-grid>\n  </ion-row>\n  \n  <div class=\"main_container\">\n    <div class=\"description_section\">\n      <div class=\"header\">\n        <p>\n          Commencez dès aujourd’hui à reprendre un rythme de vie sain.\n          Sélectionnez ci-dessous l’une des formules d’abonnement suivantes pour\n          accéder au contenu illimité de l’application\n          <strong>Transformatics</strong>.\n        </p>\n      </div>\n    </div>\n\n    <div class=\"slider\">\n      <ion-slides\n        pager=\"false\"\n        class=\"plan-slider\"\n        [options]=\"planSlideOptions\"\n        #slides\n        class=\"select-plan\"\n       \n        (ionSlideDidChange)=\"getIndex($event)\"\n      >\n        <ion-slide>\n          <ion-card>\n            <img\n              src=\"assets/images/JE SUIS UN COACH Page 3.jpg\"\n              class=\"slide_image\"\n            />\n            <div class=\"price_section\">\n              <p class=\"p1\">24.90€ /mois</p>\n              <p class=\"p2\">Ou 0.83 /jour</p>\n            </div>\n            <ion-card-header>\n              <ion-card-title>\n                Abonnement avec engagement de 12 mois\n              </ion-card-title>\n            </ion-card-header>\n            <ion-card-content class=\"common-card-content\">\n              <p>\n                Notre formule la plus adéquate. Vous accèderez à l’intégralité\n                fonctionnelle du système Transformatics, ses spécificités, ses\n                check-ups et son coaching personnalisé. Prenez enfin le contrôle\n                de votre rythme de vie et changez votre façon de voir les choses\n                !\n              </p>\n              <ion-button class=\"select-plan\" (click)=\"openPaymentModal()\">\n                Sélectionner la formule\n              </ion-button>\n              <ion-button\n                (click)=\"goToBodyScan()\"\n                class=\"skip-btn\"\n                fill=\"clear\"\n              >\n                Skip\n              </ion-button>\n            </ion-card-content>\n          </ion-card>\n        </ion-slide>\n\n        <ion-slide>\n          <ion-card>\n            <img\n              src=\"assets/images/JE SUIS UN COACH Page 3.jpg\"\n              class=\"specific_slide_img\"\n            />\n            <div class=\"price_section\">\n              <p class=\"p1\">24.90€ /mois</p>\n              <p class=\"p2\">Ou 0.83 /jour</p>\n            </div>\n            <ion-card-header>\n              <ion-card-title>\n                Abonnement avec engagement de 12 mois\n              </ion-card-title>\n            </ion-card-header>\n            <ion-card-content class=\"extra_p1_card_content\">\n              <p>\n                Notre formule la plus complète. Vous devenez un coach à part\n                entière du système Transformatics, certifié par vos diplômes\n                personnels, vous amassez les clients, recevez vos paiements et\n                gérez toute votre activité de coach du bout des doigts !\n              </p>\n    \n              <!-- <ion-button\n                (click)=\"goToBodyScan()\"\n                class=\"skip-btn\"\n                fill=\"clear\"\n              >\n                Skip\n              </ion-button> -->\n\n              <ion-row class=\"header_section\">\n                <ion-col size=\"12\">\n                  <ion-col size=\"4\"> </ion-col>\n\n                  <ion-col size=\"2\">\n                    <ion-label class=\"very_small_text\"> 1ère a. </ion-label>\n                  </ion-col>\n\n                  <ion-col size=\"2\">\n                    <ion-label class=\"very_small_text\"> 2ème a. </ion-label>\n                  </ion-col>\n\n                  <ion-col size=\"3\">\n                    <ion-label class=\"very_small_text\"> 3ème a. et + </ion-label>\n                  </ion-col>\n                </ion-col>\n              </ion-row>\n\n              <ion-row class=\"header_section\">\n                <ion-col size=\"12\">\n                  <ion-col size=\"4\">\n                    <ion-label class=\"very_small_text text-bold\">\n                      Rémunération du coach/coaching\n                    </ion-label>\n                  </ion-col>\n\n                  <ion-col size=\"2\">\n                    <ion-label class=\"very_small_text\"> 65% </ion-label>\n                  </ion-col>\n\n                  <ion-col size=\"2\">\n                    <ion-label class=\"very_small_text text-bold\"> 70% </ion-label>\n                  </ion-col>\n\n                  <ion-col size=\"3\">\n                    <ion-label class=\"very_small_text text-bold\"> 75% </ion-label>\n                  </ion-col>\n                </ion-col>\n              </ion-row>\n\n              <ion-row class=\"header_section\">\n                <ion-col size=\"12\">\n                  <ion-col size=\"4\">\n                    <ion-label class=\"very_small_text text-bold\">\n                      Abonnement mensuel\n                    </ion-label>\n                  </ion-col>\n\n                  <ion-col size=\"2\">\n                    <ion-label class=\"very_small_text\"> 29,90€ </ion-label>\n                  </ion-col>\n\n                  <ion-col size=\"2\">\n                    <ion-label class=\"very_small_text text-bold\"> 19,90€ </ion-label>\n                  </ion-col>\n\n                  <ion-col size=\"3\">\n                    <ion-label class=\"very_small_text text-bold\"> 19,90€ </ion-label>\n                  </ion-col>\n                </ion-col>\n              </ion-row>\n\n\n              <ion-button class=\"select-plan\">\n                Sélectionner la formule\n              </ion-button>\n            </ion-card-content>\n          </ion-card>\n        </ion-slide>\n\n        <ion-slide>\n          <ion-card>\n            <img\n              src=\"assets/images/TARIFS - TELECHARGER.jpg\"\n              class=\"slide_image\"\n            />\n            <div class=\"price_section\">\n              <p class=\"p1\">24.90€ /mois</p>\n              <p class=\"p2\">Ou 0.83 /jour</p>\n            </div>\n            <ion-card-header>\n              <ion-card-title>\n                Abonnement limité – Training only\n              </ion-card-title>\n            </ion-card-header>\n            <ion-card-content  class=\"common-card-content\">\n              <p>\n                Si vous ne souhaitez pas vous lancer dans un engagement à\n                l’année, cette formule est idéale. Vous avez accès au créateur\n                de programmes d’entraînement, ce qui vous permet de les\n                télécharger en PDF pour les envoyer à vos clients en un clic !\n              </p>\n              <ion-button class=\"select-plan\" routerLink=\"/body-scan-steps-coach\">\n                Sélectionner la formule\n              </ion-button>\n              <ion-button\n                (click)=\"goToBodyScan()\"\n                class=\"skip-btn\"\n                fill=\"clear\"\n              >\n                Skip\n              </ion-button>\n            </ion-card-content>\n          </ion-card>\n        </ion-slide>\n      </ion-slides>\n    </div>\n\n    <div class=\"footer\">\n      <p>\n        En sélectionnant une formule d’abonnement, vous acceptez les Conditions\n        et Termes de Vente et la Politique de Confidentialité. La souscription\n        sera automatiquement renouvelée 24 heures avant la date anniversaire de\n        cette dernière.\n      </p>\n    </div>\n  </div>\n</ion-content>\n";
      /***/
    },

    /***/
    "./src/app/pages/select-plan-coach/select-plan-coach-routing.module.ts":
    /*!*****************************************************************************!*\
      !*** ./src/app/pages/select-plan-coach/select-plan-coach-routing.module.ts ***!
      \*****************************************************************************/

    /*! exports provided: SelectPlanCoachPageRoutingModule */

    /***/
    function srcAppPagesSelectPlanCoachSelectPlanCoachRoutingModuleTs(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "SelectPlanCoachPageRoutingModule", function () {
        return SelectPlanCoachPageRoutingModule;
      });
      /* harmony import */


      var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! tslib */
      "./node_modules/tslib/tslib.es6.js");
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! @angular/core */
      "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
      /* harmony import */


      var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! @angular/router */
      "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");
      /* harmony import */


      var _select_plan_coach_page__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
      /*! ./select-plan-coach.page */
      "./src/app/pages/select-plan-coach/select-plan-coach.page.ts");

      var routes = [{
        path: '',
        component: _select_plan_coach_page__WEBPACK_IMPORTED_MODULE_3__["SelectPlanCoachPage"]
      }];

      var SelectPlanCoachPageRoutingModule = function SelectPlanCoachPageRoutingModule() {
        _classCallCheck(this, SelectPlanCoachPageRoutingModule);
      };

      SelectPlanCoachPageRoutingModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]]
      })], SelectPlanCoachPageRoutingModule);
      /***/
    },

    /***/
    "./src/app/pages/select-plan-coach/select-plan-coach.module.ts":
    /*!*********************************************************************!*\
      !*** ./src/app/pages/select-plan-coach/select-plan-coach.module.ts ***!
      \*********************************************************************/

    /*! exports provided: SelectPlanCoachPageModule */

    /***/
    function srcAppPagesSelectPlanCoachSelectPlanCoachModuleTs(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "SelectPlanCoachPageModule", function () {
        return SelectPlanCoachPageModule;
      });
      /* harmony import */


      var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! tslib */
      "./node_modules/tslib/tslib.es6.js");
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! @angular/core */
      "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
      /* harmony import */


      var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! @angular/common */
      "./node_modules/@angular/common/__ivy_ngcc__/fesm2015/common.js");
      /* harmony import */


      var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
      /*! @angular/forms */
      "./node_modules/@angular/forms/__ivy_ngcc__/fesm2015/forms.js");
      /* harmony import */


      var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
      /*! @ionic/angular */
      "./node_modules/@ionic/angular/__ivy_ngcc__/fesm2015/ionic-angular.js");
      /* harmony import */


      var _select_plan_coach_routing_module__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(
      /*! ./select-plan-coach-routing.module */
      "./src/app/pages/select-plan-coach/select-plan-coach-routing.module.ts");
      /* harmony import */


      var _select_plan_coach_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(
      /*! ./select-plan-coach.page */
      "./src/app/pages/select-plan-coach/select-plan-coach.page.ts");

      var SelectPlanCoachPageModule = function SelectPlanCoachPageModule() {
        _classCallCheck(this, SelectPlanCoachPageModule);
      };

      SelectPlanCoachPageModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"], _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"], _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonicModule"], _select_plan_coach_routing_module__WEBPACK_IMPORTED_MODULE_5__["SelectPlanCoachPageRoutingModule"]],
        declarations: [_select_plan_coach_page__WEBPACK_IMPORTED_MODULE_6__["SelectPlanCoachPage"]]
      })], SelectPlanCoachPageModule);
      /***/
    },

    /***/
    "./src/app/pages/select-plan-coach/select-plan-coach.page.scss":
    /*!*********************************************************************!*\
      !*** ./src/app/pages/select-plan-coach/select-plan-coach.page.scss ***!
      \*********************************************************************/

    /*! exports provided: default */

    /***/
    function srcAppPagesSelectPlanCoachSelectPlanCoachPageScss(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony default export */


      __webpack_exports__["default"] = "ion-header {\n  background: #000;\n}\n\nion-content {\n  --background: url('role.png') 0 0/100% no-repeat;\n  background-size: cover;\n  --overflow: hidden;\n}\n\nion-title {\n  color: #fff;\n  font-family: Oswald, sans-serif;\n  font-size: 17px;\n  letter-spacing: 4px;\n}\n\n.header-img {\n  display: table;\n  /* flex-direction: column; */\n  /* text-align: center; */\n  margin: 0 auto;\n}\n\n.header-img img {\n  height: 50px;\n  width: 190px;\n}\n\n.steps {\n  height: 5px;\n  background: #4e4e4e;\n  border-radius: 10px;\n}\n\n.steps.active {\n  background: #c1c1c1;\n}\n\n.body {\n  color: #fff;\n}\n\n.body .p1,\n.body .p2 {\n  font-family: Oswald, serif;\n  text-align: justify;\n  font-size: 0.9rem;\n  letter-spacing: 1px;\n  margin-top: 5%;\n  line-height: 1rem;\n}\n\n.body .p3 {\n  font-family: Oswald, serif;\n  text-align: justify;\n  font-style: italic;\n  margin-top: 5%;\n  line-height: 0.9rem;\n  font-size: 0.8rem;\n}\n\n.main_container {\n  height: 100%;\n  padding-bottom: 10%;\n  background-color: rgba(0, 0, 0, 0.7);\n  padding-left: 5%;\n  padding-right: 5%;\n  display: flex;\n  flex-direction: column;\n  justify-content: center;\n}\n\n.header p {\n  font-family: Oswald, serif;\n  text-align: center;\n  font-size: 0.7rem;\n  /* letter-spacing: 1px; */\n  line-height: 0.9rem;\n  color: #fff;\n  margin: 0;\n}\n\n.stepper_section {\n  margin-top: 4%;\n}\n\n.slider {\n  margin-top: 10%;\n}\n\n.footer {\n  margin-top: 7%;\n  margin-bottom: 7%;\n}\n\n.footer p {\n  font-family: Oswald, serif;\n  text-align: center;\n  font-size: 0.7rem;\n  /* letter-spacing: 1px; */\n  color: #fff;\n}\n\nion-card-header {\n  padding-right: 5%;\n  padding-left: 5%;\n  height: 38px;\n}\n\nion-card-title {\n  font-family: Oswald, serif;\n  font-size: 0.8rem;\n  font-weight: bold;\n  text-align: left;\n}\n\n.select-plan {\n  --background: var(--ion-btn-custom-color);\n}\n\nion-card {\n  margin: 0;\n}\n\nion-card-content {\n  padding-right: 5%;\n  padding-left: 5%;\n  padding-bottom: 2%;\n}\n\nion-card-content p {\n  font-family: Oswald, serif;\n  font-size: 0.7rem;\n  text-align: justify;\n  color: #000;\n  padding-top: 0.2rem;\n  padding-bottom: 0.2rem;\n}\n\nion-card-content ion-button {\n  width: 100%;\n}\n\n.slide_image {\n  height: 200px;\n  -o-object-fit: cover;\n     object-fit: cover;\n  width: 100%;\n  -o-object-position: center;\n     object-position: center;\n}\n\n.specific_slide_img {\n  height: 130px !important;\n  -o-object-fit: cover;\n     object-fit: cover;\n  width: 100%;\n  -o-object-position: center;\n     object-position: center;\n}\n\n.skip-btn {\n  margin-top: 5px;\n  --background: transparent;\n  --color: var(--ion-color-primary);\n}\n\n.price_section {\n  position: absolute;\n  top: 4%;\n  left: 4%;\n  text-align: left;\n}\n\n.price_section .p1 {\n  font-family: Oswald, serif;\n  font-weight: bold;\n  color: #fff;\n  font-size: 16px;\n  padding-bottom: 0 !important;\n  margin-bottom: 0 !important;\n}\n\n.price_section .p2 {\n  font-family: Oswald, serif;\n  color: #d2d1d1;\n  font-size: 10px;\n  padding-top: 0 !important;\n  margin-top: 0 !important;\n}\n\n.extra_p1_card_content {\n  padding-left: 4%;\n  padding-right: 4%;\n  padding-bottom: 1%;\n}\n\n.extra_p1_card_content .header_section ion-col {\n  display: flex;\n  flex-direction: row;\n}\n\n.extra_p1_card_content .header_section ion-col ion-col {\n  display: flex;\n  flex-direction: row;\n  justify-content: center;\n}\n\n.extra_p1_card_content .header_section ion-col ion-col ion-label {\n  align-self: center;\n  font-weight: 400;\n  color: #000;\n}\n\n.text-bold {\n  font-weight: bold;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvcGFnZXMvc2VsZWN0LXBsYW4tY29hY2gvc2VsZWN0LXBsYW4tY29hY2gucGFnZS5zY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBO0VBQ0ksZ0JBQUE7QUFDSjs7QUFFQTtFQUNJLGdEQUFBO0VBR0Esc0JBQUE7RUFDQSxrQkFBQTtBQUNKOztBQUdBO0VBQ0ksV0FBQTtFQUNBLCtCQUFBO0VBQ0EsZUFBQTtFQUNBLG1CQUFBO0FBQUo7O0FBSUE7RUFDSSxjQUFBO0VBQ0EsNEJBQUE7RUFDQSx3QkFBQTtFQUNBLGNBQUE7QUFESjs7QUFHSTtFQUNJLFlBQUE7RUFDQSxZQUFBO0FBRFI7O0FBT0E7RUFDSSxXQUFBO0VBQ0EsbUJBQUE7RUFDQSxtQkFBQTtBQUpKOztBQU9BO0VBQ0ksbUJBQUE7QUFKSjs7QUFVQTtFQUNJLFdBQUE7QUFQSjs7QUFTSTs7RUFFSSwwQkFBQTtFQUNBLG1CQUFBO0VBQ0EsaUJBQUE7RUFDQSxtQkFBQTtFQUNBLGNBQUE7RUFDQSxpQkFBQTtBQVBSOztBQVVJO0VBQ0ksMEJBQUE7RUFDQSxtQkFBQTtFQUNBLGtCQUFBO0VBQ0EsY0FBQTtFQUNBLG1CQUFBO0VBQ0EsaUJBQUE7QUFSUjs7QUFhQTtFQUNJLFlBQUE7RUFDQSxtQkFBQTtFQUNBLG9DQUFBO0VBQ0EsZ0JBQUE7RUFDQSxpQkFBQTtFQUNBLGFBQUE7RUFDQSxzQkFBQTtFQUNBLHVCQUFBO0FBVko7O0FBY0k7RUFDSSwwQkFBQTtFQUNBLGtCQUFBO0VBQ0EsaUJBQUE7RUFDQSx5QkFBQTtFQUVBLG1CQUFBO0VBQ0EsV0FBQTtFQUVBLFNBQUE7QUFiUjs7QUFrQkE7RUFDSSxjQUFBO0FBZko7O0FBa0JBO0VBRUksZUFBQTtBQWhCSjs7QUF1QkE7RUFDSSxjQUFBO0VBQ0EsaUJBQUE7QUFwQko7O0FBc0JJO0VBQ0ksMEJBQUE7RUFDQSxrQkFBQTtFQUNBLGlCQUFBO0VBQ0EseUJBQUE7RUFDQSxXQUFBO0FBcEJSOztBQXlCQTtFQUNJLGlCQUFBO0VBQ0EsZ0JBQUE7RUFDQSxZQUFBO0FBdEJKOztBQXlCQTtFQUNJLDBCQUFBO0VBQ0EsaUJBQUE7RUFDQSxpQkFBQTtFQUNBLGdCQUFBO0FBdEJKOztBQTJCQTtFQUNJLHlDQUFBO0FBeEJKOztBQTJCQTtFQUNJLFNBQUE7QUF4Qko7O0FBMEJBO0VBQ0ksaUJBQUE7RUFDQSxnQkFBQTtFQUNBLGtCQUFBO0FBdkJKOztBQTRCSTtFQUNJLDBCQUFBO0VBQ0EsaUJBQUE7RUFFQSxtQkFBQTtFQUNBLFdBQUE7RUFDQSxtQkFBQTtFQUNBLHNCQUFBO0FBM0JSOztBQThCSTtFQUNJLFdBQUE7QUE1QlI7O0FBaUNBO0VBQ0ksYUFBQTtFQUNBLG9CQUFBO0tBQUEsaUJBQUE7RUFDQSxXQUFBO0VBQ0EsMEJBQUE7S0FBQSx1QkFBQTtBQTlCSjs7QUFpQ0E7RUFDSSx3QkFBQTtFQUNBLG9CQUFBO0tBQUEsaUJBQUE7RUFDQSxXQUFBO0VBQ0EsMEJBQUE7S0FBQSx1QkFBQTtBQTlCSjs7QUFpQ0E7RUFDSSxlQUFBO0VBQ0EseUJBQUE7RUFDQSxpQ0FBQTtBQTlCSjs7QUFrQ0E7RUFDSSxrQkFBQTtFQUNBLE9BQUE7RUFDQSxRQUFBO0VBQ0EsZ0JBQUE7QUEvQko7O0FBaUNJO0VBQ0ksMEJBQUE7RUFDQSxpQkFBQTtFQUNBLFdBQUE7RUFDQSxlQUFBO0VBQ0EsNEJBQUE7RUFDQSwyQkFBQTtBQS9CUjs7QUFrQ0k7RUFDSSwwQkFBQTtFQUNBLGNBQUE7RUFDQSxlQUFBO0VBQ0EseUJBQUE7RUFDQSx3QkFBQTtBQWhDUjs7QUF5Q0E7RUFDSSxnQkFBQTtFQUNBLGlCQUFBO0VBQ0Esa0JBQUE7QUF0Q0o7O0FBd0NRO0VBQ0ksYUFBQTtFQUNBLG1CQUFBO0FBdENaOztBQXdDWTtFQUNJLGFBQUE7RUFDQSxtQkFBQTtFQUNBLHVCQUFBO0FBdENoQjs7QUF1Q2dCO0VBQ0ksa0JBQUE7RUFDQSxnQkFBQTtFQUNBLFdBQUE7QUFyQ3BCOztBQStDQTtFQUNJLGlCQUFBO0FBNUNKIiwiZmlsZSI6InNyYy9hcHAvcGFnZXMvc2VsZWN0LXBsYW4tY29hY2gvc2VsZWN0LXBsYW4tY29hY2gucGFnZS5zY3NzIiwic291cmNlc0NvbnRlbnQiOlsiaW9uLWhlYWRlciB7XG4gICAgYmFja2dyb3VuZDogIzAwMDtcbn1cblxuaW9uLWNvbnRlbnQge1xuICAgIC0tYmFja2dyb3VuZDogdXJsKC4uLy4uLy4uL2Fzc2V0cy9pbWFnZXMvcm9sZS5wbmcpIDAgMC8xMDAlIG5vLXJlcGVhdDtcbiAgICAtd2Via2l0LWJhY2tncm91bmQtc2l6ZTogY292ZXI7XG4gICAgLW1vei1iYWNrZ3JvdW5kLXNpemU6IGNvdmVyO1xuICAgIGJhY2tncm91bmQtc2l6ZTogY292ZXI7XG4gICAgLS1vdmVyZmxvdzogaGlkZGVuO1xufVxuXG5cbmlvbi10aXRsZSB7XG4gICAgY29sb3I6ICNmZmY7XG4gICAgZm9udC1mYW1pbHk6IE9zd2FsZCwgc2Fucy1zZXJpZjtcbiAgICBmb250LXNpemU6IDE3cHg7XG4gICAgbGV0dGVyLXNwYWNpbmc6IDRweDtcblxufVxuXG4uaGVhZGVyLWltZyB7XG4gICAgZGlzcGxheTogdGFibGU7XG4gICAgLyogZmxleC1kaXJlY3Rpb246IGNvbHVtbjsgKi9cbiAgICAvKiB0ZXh0LWFsaWduOiBjZW50ZXI7ICovXG4gICAgbWFyZ2luOiAwIGF1dG87XG5cbiAgICBpbWcge1xuICAgICAgICBoZWlnaHQ6IDUwcHg7XG4gICAgICAgIHdpZHRoOiAxOTBweDtcblxuICAgIH1cbn1cblxuXG4uc3RlcHMge1xuICAgIGhlaWdodDogNXB4O1xuICAgIGJhY2tncm91bmQ6IHJnYig3OCwgNzgsIDc4KTtcbiAgICBib3JkZXItcmFkaXVzOiAxMHB4O1xufVxuXG4uc3RlcHMuYWN0aXZlIHtcbiAgICBiYWNrZ3JvdW5kOiAjYzFjMWMxO1xufVxuXG5cblxuXG4uYm9keSB7XG4gICAgY29sb3I6ICNmZmY7XG5cbiAgICAucDEsXG4gICAgLnAyIHtcbiAgICAgICAgZm9udC1mYW1pbHk6IE9zd2FsZCwgc2VyaWY7XG4gICAgICAgIHRleHQtYWxpZ246IGp1c3RpZnk7XG4gICAgICAgIGZvbnQtc2l6ZTogMC45cmVtO1xuICAgICAgICBsZXR0ZXItc3BhY2luZzogMXB4O1xuICAgICAgICBtYXJnaW4tdG9wOiA1JTtcbiAgICAgICAgbGluZS1oZWlnaHQ6IDFyZW07XG4gICAgfVxuXG4gICAgLnAzIHtcbiAgICAgICAgZm9udC1mYW1pbHk6IE9zd2FsZCwgc2VyaWY7XG4gICAgICAgIHRleHQtYWxpZ246IGp1c3RpZnk7XG4gICAgICAgIGZvbnQtc3R5bGU6IGl0YWxpYztcbiAgICAgICAgbWFyZ2luLXRvcDogNSU7XG4gICAgICAgIGxpbmUtaGVpZ2h0OiAwLjlyZW07XG4gICAgICAgIGZvbnQtc2l6ZTogMC44cmVtO1xuICAgIH1cblxufVxuXG4ubWFpbl9jb250YWluZXIge1xuICAgIGhlaWdodDogMTAwJTtcbiAgICBwYWRkaW5nLWJvdHRvbTogMTAlO1xuICAgIGJhY2tncm91bmQtY29sb3I6IHJnYmEoMCwgMCwgMCwgMC43KTtcbiAgICBwYWRkaW5nLWxlZnQ6IDUlO1xuICAgIHBhZGRpbmctcmlnaHQ6IDUlO1xuICAgIGRpc3BsYXk6IGZsZXg7XG4gICAgZmxleC1kaXJlY3Rpb246IGNvbHVtbjtcbiAgICBqdXN0aWZ5LWNvbnRlbnQ6IGNlbnRlcjtcbn1cblxuLmhlYWRlciB7XG4gICAgcCB7XG4gICAgICAgIGZvbnQtZmFtaWx5OiBPc3dhbGQsIHNlcmlmO1xuICAgICAgICB0ZXh0LWFsaWduOiBjZW50ZXI7XG4gICAgICAgIGZvbnQtc2l6ZTogMC43cmVtO1xuICAgICAgICAvKiBsZXR0ZXItc3BhY2luZzogMXB4OyAqL1xuICAgICAgICAvLyBtYXJnaW4tdG9wOiAyJTtcbiAgICAgICAgbGluZS1oZWlnaHQ6IDAuOXJlbTtcbiAgICAgICAgY29sb3I6ICNmZmY7XG5cbiAgICAgICAgbWFyZ2luOiAwO1xuXG4gICAgfVxufVxuXG4uc3RlcHBlcl9zZWN0aW9uIHtcbiAgICBtYXJnaW4tdG9wOiA0JTtcbn1cblxuLnNsaWRlciB7XG4gICAgLy8gaGVpZ2h0OiA2MCU7XG4gICAgbWFyZ2luLXRvcDogMTAlO1xufVxuXG5pb24tc2xpZGUge1xuICAgIC8vIHBhZGRpbmctYm90dG9tOiAxNXB4O1xufVxuXG4uZm9vdGVyIHtcbiAgICBtYXJnaW4tdG9wOiA3JTtcbiAgICBtYXJnaW4tYm90dG9tOiA3JTtcblxuICAgIHAge1xuICAgICAgICBmb250LWZhbWlseTogT3N3YWxkLCBzZXJpZjtcbiAgICAgICAgdGV4dC1hbGlnbjogY2VudGVyO1xuICAgICAgICBmb250LXNpemU6IDAuN3JlbTtcbiAgICAgICAgLyogbGV0dGVyLXNwYWNpbmc6IDFweDsgKi9cbiAgICAgICAgY29sb3I6ICNmZmY7XG5cbiAgICB9XG59XG5cbmlvbi1jYXJkLWhlYWRlciB7XG4gICAgcGFkZGluZy1yaWdodDogNSU7XG4gICAgcGFkZGluZy1sZWZ0OiA1JTtcbiAgICBoZWlnaHQ6IDM4cHg7XG59XG5cbmlvbi1jYXJkLXRpdGxlIHtcbiAgICBmb250LWZhbWlseTogT3N3YWxkLCBzZXJpZjtcbiAgICBmb250LXNpemU6IDAuOHJlbTtcbiAgICBmb250LXdlaWdodDogYm9sZDtcbiAgICB0ZXh0LWFsaWduOiBsZWZ0O1xuXG5cbn1cblxuLnNlbGVjdC1wbGFuIHtcbiAgICAtLWJhY2tncm91bmQ6IHZhcigtLWlvbi1idG4tY3VzdG9tLWNvbG9yKTtcbn1cblxuaW9uLWNhcmQge1xuICAgIG1hcmdpbjogMDtcbn1cbmlvbi1jYXJkLWNvbnRlbnQge1xuICAgIHBhZGRpbmctcmlnaHQ6IDUlO1xuICAgIHBhZGRpbmctbGVmdDogNSU7XG4gICAgcGFkZGluZy1ib3R0b206IDIlO1xuXG4gICAgLy8gbWFyZ2luLXRvcDogNyU7XG4gICAgLy8gbWFyZ2luLWJvdHRvbTogNyU7XG5cbiAgICBwIHtcbiAgICAgICAgZm9udC1mYW1pbHk6IE9zd2FsZCwgc2VyaWY7XG4gICAgICAgIGZvbnQtc2l6ZTogMC43cmVtO1xuXG4gICAgICAgIHRleHQtYWxpZ246IGp1c3RpZnk7XG4gICAgICAgIGNvbG9yOiAjMDAwO1xuICAgICAgICBwYWRkaW5nLXRvcDogMC4ycmVtO1xuICAgICAgICBwYWRkaW5nLWJvdHRvbTogMC4ycmVtO1xuICAgIH1cblxuICAgIGlvbi1idXR0b24ge1xuICAgICAgICB3aWR0aDogMTAwJTtcbiAgICB9XG59XG5cblxuLnNsaWRlX2ltYWdlIHtcbiAgICBoZWlnaHQ6IDIwMHB4O1xuICAgIG9iamVjdC1maXQ6IGNvdmVyO1xuICAgIHdpZHRoOiAxMDAlO1xuICAgIG9iamVjdC1wb3NpdGlvbjogY2VudGVyO1xufVxuXG4uc3BlY2lmaWNfc2xpZGVfaW1nIHtcbiAgICBoZWlnaHQ6IDEzMHB4ICFpbXBvcnRhbnQ7XG4gICAgb2JqZWN0LWZpdDogY292ZXI7XG4gICAgd2lkdGg6IDEwMCU7XG4gICAgb2JqZWN0LXBvc2l0aW9uOiBjZW50ZXI7XG59XG5cbi5za2lwLWJ0biB7XG4gICAgbWFyZ2luLXRvcDogNXB4O1xuICAgIC0tYmFja2dyb3VuZDogdHJhbnNwYXJlbnQ7XG4gICAgLS1jb2xvcjogdmFyKC0taW9uLWNvbG9yLXByaW1hcnkpO1xuXG59XG5cbi5wcmljZV9zZWN0aW9uIHtcbiAgICBwb3NpdGlvbjogYWJzb2x1dGU7XG4gICAgdG9wOiA0JTtcbiAgICBsZWZ0OiA0JTtcbiAgICB0ZXh0LWFsaWduOiBsZWZ0O1xuXG4gICAgLnAxIHtcbiAgICAgICAgZm9udC1mYW1pbHk6IE9zd2FsZCwgc2VyaWY7XG4gICAgICAgIGZvbnQtd2VpZ2h0OiBib2xkO1xuICAgICAgICBjb2xvcjogI2ZmZjtcbiAgICAgICAgZm9udC1zaXplOiAxNnB4O1xuICAgICAgICBwYWRkaW5nLWJvdHRvbTogMCAhaW1wb3J0YW50O1xuICAgICAgICBtYXJnaW4tYm90dG9tOiAwICFpbXBvcnRhbnQ7XG4gICAgfVxuXG4gICAgLnAyIHtcbiAgICAgICAgZm9udC1mYW1pbHk6IE9zd2FsZCwgc2VyaWY7XG4gICAgICAgIGNvbG9yOiAjZDJkMWQxO1xuICAgICAgICBmb250LXNpemU6IDEwcHg7XG4gICAgICAgIHBhZGRpbmctdG9wOiAwICFpbXBvcnRhbnQ7XG4gICAgICAgIG1hcmdpbi10b3A6IDAgIWltcG9ydGFudDtcbiAgICB9XG5cblxuXG59XG5cblxuXG4uZXh0cmFfcDFfY2FyZF9jb250ZW50IHtcbiAgICBwYWRkaW5nLWxlZnQ6IDQlO1xuICAgIHBhZGRpbmctcmlnaHQ6IDQlO1xuICAgIHBhZGRpbmctYm90dG9tOiAxJTtcbiAgICAuaGVhZGVyX3NlY3Rpb24ge1xuICAgICAgICBpb24tY29sIHtcbiAgICAgICAgICAgIGRpc3BsYXk6IGZsZXg7XG4gICAgICAgICAgICBmbGV4LWRpcmVjdGlvbjogcm93O1xuXG4gICAgICAgICAgICBpb24tY29sIHtcbiAgICAgICAgICAgICAgICBkaXNwbGF5OiBmbGV4O1xuICAgICAgICAgICAgICAgIGZsZXgtZGlyZWN0aW9uOiByb3c7XG4gICAgICAgICAgICAgICAganVzdGlmeS1jb250ZW50OiBjZW50ZXI7XG4gICAgICAgICAgICAgICAgaW9uLWxhYmVsIHtcbiAgICAgICAgICAgICAgICAgICAgYWxpZ24tc2VsZjogY2VudGVyO1xuICAgICAgICAgICAgICAgICAgICBmb250LXdlaWdodDogNDAwO1xuICAgICAgICAgICAgICAgICAgICBjb2xvcjogIzAwMDtcbiAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICB9XG4gICAgICAgIH1cbiAgICB9XG59XG5cblxuXG5cbi50ZXh0LWJvbGQge1xuICAgIGZvbnQtd2VpZ2h0OiBib2xkO1xufSJdfQ== */";
      /***/
    },

    /***/
    "./src/app/pages/select-plan-coach/select-plan-coach.page.ts":
    /*!*******************************************************************!*\
      !*** ./src/app/pages/select-plan-coach/select-plan-coach.page.ts ***!
      \*******************************************************************/

    /*! exports provided: SelectPlanCoachPage */

    /***/
    function srcAppPagesSelectPlanCoachSelectPlanCoachPageTs(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "SelectPlanCoachPage", function () {
        return SelectPlanCoachPage;
      });
      /* harmony import */


      var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! tslib */
      "./node_modules/tslib/tslib.es6.js");
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! @angular/core */
      "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
      /* harmony import */


      var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! @angular/router */
      "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");
      /* harmony import */


      var _components_modals_payment_modal_payment_modal_component__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
      /*! @components/modals/payment-modal/payment-modal.component */
      "./src/app/components/modals/payment-modal/payment-modal.component.ts");
      /* harmony import */


      var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
      /*! @ionic/angular */
      "./node_modules/@ionic/angular/__ivy_ngcc__/fesm2015/ionic-angular.js");

      var SelectPlanCoachPage = /*#__PURE__*/function () {
        function SelectPlanCoachPage(router, route, modal) {
          _classCallCheck(this, SelectPlanCoachPage);

          this.router = router;
          this.route = route;
          this.modal = modal;
          this.planSlideOptions = {
            centeredSlides: true,
            slidesPerView: 1.1,
            spaceBetween: 10
          };
          this.activepage = 0;
        }

        _createClass(SelectPlanCoachPage, [{
          key: "ngOnInit",
          value: function ngOnInit() {}
        }, {
          key: "getIndex",
          value: function getIndex(event) {
            return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, /*#__PURE__*/regeneratorRuntime.mark(function _callee() {
              return regeneratorRuntime.wrap(function _callee$(_context) {
                while (1) {
                  switch (_context.prev = _context.next) {
                    case 0:
                      console.log(event);
                      _context.next = 3;
                      return this.slider.getActiveIndex();

                    case 3:
                      this.activepage = _context.sent;

                    case 4:
                    case "end":
                      return _context.stop();
                  }
                }
              }, _callee, this);
            }));
          }
        }, {
          key: "goToBodyScan",
          value: function goToBodyScan() {
            console.log("cdsc");
            this.router.navigate(['/body-scan-step-coach']);
          }
        }, {
          key: "openPaymentModal",
          value: function openPaymentModal() {
            return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, /*#__PURE__*/regeneratorRuntime.mark(function _callee2() {
              var modal;
              return regeneratorRuntime.wrap(function _callee2$(_context2) {
                while (1) {
                  switch (_context2.prev = _context2.next) {
                    case 0:
                      _context2.next = 2;
                      return this.modal.create({
                        component: _components_modals_payment_modal_payment_modal_component__WEBPACK_IMPORTED_MODULE_3__["PaymentModalComponent"],
                        cssClass: 'check-up-modal'
                      });

                    case 2:
                      modal = _context2.sent;
                      _context2.next = 5;
                      return modal.present();

                    case 5:
                      return _context2.abrupt("return", _context2.sent);

                    case 6:
                    case "end":
                      return _context2.stop();
                  }
                }
              }, _callee2, this);
            }));
          }
        }]);

        return SelectPlanCoachPage;
      }();

      SelectPlanCoachPage.ctorParameters = function () {
        return [{
          type: _angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"]
        }, {
          type: _angular_router__WEBPACK_IMPORTED_MODULE_2__["ActivatedRoute"]
        }, {
          type: _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["ModalController"]
        }];
      };

      SelectPlanCoachPage.propDecorators = {
        slider: [{
          type: _angular_core__WEBPACK_IMPORTED_MODULE_1__["ViewChild"],
          args: ['slides']
        }]
      };
      SelectPlanCoachPage = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-select-plan-coach',
        template: Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(
        /*! raw-loader!./select-plan-coach.page.html */
        "./node_modules/raw-loader/dist/cjs.js!./src/app/pages/select-plan-coach/select-plan-coach.page.html"))["default"],
        styles: [Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(
        /*! ./select-plan-coach.page.scss */
        "./src/app/pages/select-plan-coach/select-plan-coach.page.scss"))["default"]]
      })], SelectPlanCoachPage);
      /***/
    }
  }]);
})();
//# sourceMappingURL=pages-select-plan-coach-select-plan-coach-module-es5.js.map