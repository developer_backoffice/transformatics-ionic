(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["pages-tabs-home-add-exercise-add-exercise-module"],{

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/pages/tabs/home/add-exercise/add-exercise.page.html":
/*!***********************************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/pages/tabs/home/add-exercise/add-exercise.page.html ***!
  \***********************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("\n<ion-content>\n  <ion-grid>\n    <ion-row>\n      <ion-item lines=\"none\">\n        <ion-label class=\"small_text\">Filtres</ion-label>\n        <ion-select interface=\"action-sheet\" class=\"custom-options\" [(ngModel)]=\"filterSelect\">\n            <ion-select-option value=\"Pectoraux\">Pectoraux</ion-select-option>\n            \n        </ion-select>\n    </ion-item>\n    </ion-row>\n\n    <ion-row class=\"search_section\">\n      <ion-input class=\"small_text\" placeholder=\"Recherchez un exercice par écrit…\"></ion-input>\n    </ion-row>\n\n\n    <ion-row class=\"select_body_section\" routerLink=\"/select-body-groups\">\n      <ion-label class=\"small_text_bold\">\n        Sélectionner les groupes musculaires\n      </ion-label>\n    </ion-row>\n\n\n\n    <ion-row class=\"ion-margin-top list_section\" >\n      <ion-item lines=\"none\" *ngFor=\"let item of ['', '', '', '', '', '', '', '', '', '', '']\">\n        <div class=\"black_box\"></div>\n\n        <ion-label class=\"list_section_title\">\n          <ion-label class=\"small_text\"> Nom de l’exercice </ion-label>\n          <ion-label class=\"small_text\">\n            Sous nom de l’exercice\n          </ion-label>\n        </ion-label>\n        <ion-reorder slot=\"end\"></ion-reorder>\n      </ion-item>\n\n    </ion-row>\n\n  </ion-grid>\n</ion-content>\n\n\n<!-- <ion-footer class=\"ion-no-border footer_section\">\n  <ion-row>\n      <ion-button routerLink=\"/add-exercise\" class=\"next\" shape=\"round\">\n      \n      </ion-button>\n    <ion-button routerLink=\"/add-exercise\" class=\"next footer_section_btn\" shape=\"round\">\n      Ajouter des exercices\n    </ion-button>\n  </ion-row>\n</ion-footer>\n\n -->\n <ion-footer>\n  <ion-toolbar>\n    <ion-grid>\n      <ion-row>\n        <ion-col size=\"3\">\n          <ion-button\n            class=\"back\"\n            expand=\"full\"\n            shape=\"round\"\n            routerLink=\"/hypertrophy-strengthening-program-creation\"\n            >\n            <img src=\"assets/icon/back-triangle.png\" alt=\"\">\n          </ion-button>\n        </ion-col>\n        <ion-col>\n          <!-- <ion-button\n            class=\"next\"\n            expand=\"full\"\n            shape=\"round\"\n            routerLink=\"/excercise-sheet\"\n            >Ajouter [x] exercices\n          </ion-button> -->\n\n          <ion-button\n          class=\"next\"\n          expand=\"full\"\n          shape=\"round\"\n          routerLink=\"/after-excercise-selection\"\n          >Ajouter [x] exercices\n        </ion-button>\n\n        \n        </ion-col>\n      </ion-row>\n    </ion-grid>\n  </ion-toolbar>\n</ion-footer>\n");

/***/ }),

/***/ "./src/app/pages/tabs/home/add-exercise/add-exercise-routing.module.ts":
/*!*****************************************************************************!*\
  !*** ./src/app/pages/tabs/home/add-exercise/add-exercise-routing.module.ts ***!
  \*****************************************************************************/
/*! exports provided: AddExercisePageRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AddExercisePageRoutingModule", function() { return AddExercisePageRoutingModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");
/* harmony import */ var _add_exercise_page__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./add-exercise.page */ "./src/app/pages/tabs/home/add-exercise/add-exercise.page.ts");




const routes = [
    {
        path: '',
        component: _add_exercise_page__WEBPACK_IMPORTED_MODULE_3__["AddExercisePage"]
    }
];
let AddExercisePageRoutingModule = class AddExercisePageRoutingModule {
};
AddExercisePageRoutingModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]],
    })
], AddExercisePageRoutingModule);



/***/ }),

/***/ "./src/app/pages/tabs/home/add-exercise/add-exercise.module.ts":
/*!*********************************************************************!*\
  !*** ./src/app/pages/tabs/home/add-exercise/add-exercise.module.ts ***!
  \*********************************************************************/
/*! exports provided: AddExercisePageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AddExercisePageModule", function() { return AddExercisePageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/__ivy_ngcc__/fesm2015/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/__ivy_ngcc__/fesm2015/forms.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/__ivy_ngcc__/fesm2015/ionic-angular.js");
/* harmony import */ var _add_exercise_routing_module__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./add-exercise-routing.module */ "./src/app/pages/tabs/home/add-exercise/add-exercise-routing.module.ts");
/* harmony import */ var _add_exercise_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./add-exercise.page */ "./src/app/pages/tabs/home/add-exercise/add-exercise.page.ts");







let AddExercisePageModule = class AddExercisePageModule {
};
AddExercisePageModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [
            _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonicModule"],
            _add_exercise_routing_module__WEBPACK_IMPORTED_MODULE_5__["AddExercisePageRoutingModule"]
        ],
        declarations: [_add_exercise_page__WEBPACK_IMPORTED_MODULE_6__["AddExercisePage"]]
    })
], AddExercisePageModule);



/***/ }),

/***/ "./src/app/pages/tabs/home/add-exercise/add-exercise.page.scss":
/*!*********************************************************************!*\
  !*** ./src/app/pages/tabs/home/add-exercise/add-exercise.page.scss ***!
  \*********************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("ion-content {\n  --padding-top: 10%;\n  --padding-start: 3%;\n  --padding-bottom: 5%;\n}\n\nion-item {\n  width: 100%;\n}\n\n.search_section {\n  padding-left: 4%;\n  padding-right: 4%;\n}\n\n.search_section ion-input {\n  background: #f1f0f0;\n  border-radius: 12px;\n  --padding-start: 10px;\n}\n\nion-select::part(text) {\n  font-size: 0.8rem !important;\n  font-family: \"Oswald\" !important;\n}\n\n.select_body_section {\n  margin-left: 4%;\n  margin-top: 10px;\n  margin-right: 4%;\n  border-style: dotted;\n  border-color: #696969;\n  padding: 5px;\n  justify-content: center;\n  border-width: 2px;\n  border-radius: 10px;\n}\n\n.black_box {\n  width: 50px;\n  background: #000;\n  height: 35px;\n}\n\n.list_section_title {\n  padding-left: 10px;\n}\n\n.footer_section_btn {\n  width: 100%;\n  padding-left: 10px;\n  padding-right: 10px;\n  margin-top: 32px;\n  --background: var(--ion-btn-custom-color);\n}\n\n.back,\n.next {\n  --ion-button-round-border-radius: 5px;\n  --ion-button-border-radius: 5px;\n  --ion-border-radius: 5px;\n  --border-radius: 5px;\n  height: 40px;\n}\n\n.next {\n  --background: var(--ion-btn-custom-color);\n}\n\n.back {\n  --background: linear-gradient(to right, #161616e0, #c1c1c1);\n}\n\n.back img {\n  width: 70%;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvcGFnZXMvdGFicy9ob21lL2FkZC1leGVyY2lzZS9hZGQtZXhlcmNpc2UucGFnZS5zY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBO0VBQ0ksa0JBQUE7RUFDQSxtQkFBQTtFQUNBLG9CQUFBO0FBQ0o7O0FBSUE7RUFDSSxXQUFBO0FBREo7O0FBSUE7RUFDSSxnQkFBQTtFQUNBLGlCQUFBO0FBREo7O0FBRUk7RUFDSSxtQkFBQTtFQUNBLG1CQUFBO0VBQ0EscUJBQUE7QUFBUjs7QUFJQTtFQUNJLDRCQUFBO0VBQ0EsZ0NBQUE7QUFESjs7QUFHQTtFQUNJLGVBQUE7RUFDQSxnQkFBQTtFQUNBLGdCQUFBO0VBQ0Esb0JBQUE7RUFDQSxxQkFBQTtFQUNBLFlBQUE7RUFDQSx1QkFBQTtFQUNBLGlCQUFBO0VBQ0EsbUJBQUE7QUFBSjs7QUFVQTtFQUNJLFdBQUE7RUFDQSxnQkFBQTtFQUNBLFlBQUE7QUFQSjs7QUFVQTtFQUNJLGtCQUFBO0FBUEo7O0FBWUE7RUFFSSxXQUFBO0VBQ0Esa0JBQUE7RUFDQSxtQkFBQTtFQUNBLGdCQUFBO0VBQ0EseUNBQUE7QUFWSjs7QUFjQTs7RUFFSSxxQ0FBQTtFQUNBLCtCQUFBO0VBQ0Esd0JBQUE7RUFDQSxvQkFBQTtFQUNBLFlBQUE7QUFYSjs7QUFlQTtFQUNJLHlDQUFBO0FBWko7O0FBZ0JBO0VBQ0ksMkRBQUE7QUFiSjs7QUFjSTtFQUNJLFVBQUE7QUFaUiIsImZpbGUiOiJzcmMvYXBwL3BhZ2VzL3RhYnMvaG9tZS9hZGQtZXhlcmNpc2UvYWRkLWV4ZXJjaXNlLnBhZ2Uuc2NzcyIsInNvdXJjZXNDb250ZW50IjpbImlvbi1jb250ZW50IHtcbiAgICAtLXBhZGRpbmctdG9wOiAxMCU7XG4gICAgLS1wYWRkaW5nLXN0YXJ0OiAzJTtcbiAgICAtLXBhZGRpbmctYm90dG9tOiA1JTtcbn1cblxuXG5cbmlvbi1pdGVtIHtcbiAgICB3aWR0aDogMTAwJTtcbn1cblxuLnNlYXJjaF9zZWN0aW9uIHtcbiAgICBwYWRkaW5nLWxlZnQ6IDQlO1xuICAgIHBhZGRpbmctcmlnaHQ6IDQlO1xuICAgIGlvbi1pbnB1dCB7XG4gICAgICAgIGJhY2tncm91bmQ6ICNmMWYwZjA7XG4gICAgICAgIGJvcmRlci1yYWRpdXM6IDEycHg7XG4gICAgICAgIC0tcGFkZGluZy1zdGFydDogMTBweDtcbiAgICB9XG59XG5cbmlvbi1zZWxlY3Q6OnBhcnQodGV4dCkge1xuICAgIGZvbnQtc2l6ZTogMC44cmVtICFpbXBvcnRhbnQ7XG4gICAgZm9udC1mYW1pbHk6ICdPc3dhbGQnICFpbXBvcnRhbnQ7XG4gIH1cbi5zZWxlY3RfYm9keV9zZWN0aW9uIHtcbiAgICBtYXJnaW4tbGVmdDogNCU7XG4gICAgbWFyZ2luLXRvcDogMTBweDtcbiAgICBtYXJnaW4tcmlnaHQ6IDQlO1xuICAgIGJvcmRlci1zdHlsZTogZG90dGVkO1xuICAgIGJvcmRlci1jb2xvcjogIzY5Njk2OTtcbiAgICBwYWRkaW5nOiA1cHg7XG4gICAganVzdGlmeS1jb250ZW50OiBjZW50ZXI7XG4gICAgYm9yZGVyLXdpZHRoOiAycHg7XG4gICAgYm9yZGVyLXJhZGl1czogMTBweDtcblxufVxuXG5cbi5saXN0X3NlY3Rpb24ge1xuICAgIFxufVxuXG5cbi5ibGFja19ib3gge1xuICAgIHdpZHRoOiA1MHB4O1xuICAgIGJhY2tncm91bmQ6ICMwMDA7XG4gICAgaGVpZ2h0OiAzNXB4XG59XG5cbi5saXN0X3NlY3Rpb25fdGl0bGUge1xuICAgIHBhZGRpbmctbGVmdDogMTBweDtcbn1cblxuXG5cbi5mb290ZXJfc2VjdGlvbl9idG4ge1xuXG4gICAgd2lkdGg6IDEwMCU7XG4gICAgcGFkZGluZy1sZWZ0OiAxMHB4O1xuICAgIHBhZGRpbmctcmlnaHQ6IDEwcHg7XG4gICAgbWFyZ2luLXRvcDogMzJweDtcbiAgICAtLWJhY2tncm91bmQ6IHZhcigtLWlvbi1idG4tY3VzdG9tLWNvbG9yKTtcbn1cblxuXG4uYmFjayxcbi5uZXh0IHtcbiAgICAtLWlvbi1idXR0b24tcm91bmQtYm9yZGVyLXJhZGl1czogNXB4O1xuICAgIC0taW9uLWJ1dHRvbi1ib3JkZXItcmFkaXVzOiA1cHg7XG4gICAgLS1pb24tYm9yZGVyLXJhZGl1czogNXB4O1xuICAgIC0tYm9yZGVyLXJhZGl1czogNXB4O1xuICAgIGhlaWdodDogNDBweDtcbn1cblxuXG4ubmV4dCB7XG4gICAgLS1iYWNrZ3JvdW5kOiB2YXIoLS1pb24tYnRuLWN1c3RvbS1jb2xvcik7XG5cbn1cblxuLmJhY2sge1xuICAgIC0tYmFja2dyb3VuZDogbGluZWFyLWdyYWRpZW50KHRvIHJpZ2h0LCAjMTYxNjE2ZTAsICNjMWMxYzEpO1xuICAgIGltZyB7XG4gICAgICAgIHdpZHRoOiA3MCU7XG4gICAgfVxufVxuXG5cbiJdfQ== */");

/***/ }),

/***/ "./src/app/pages/tabs/home/add-exercise/add-exercise.page.ts":
/*!*******************************************************************!*\
  !*** ./src/app/pages/tabs/home/add-exercise/add-exercise.page.ts ***!
  \*******************************************************************/
/*! exports provided: AddExercisePage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AddExercisePage", function() { return AddExercisePage; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");


let AddExercisePage = class AddExercisePage {
    constructor() {
        this.filterSelect = "Pectoraux";
    }
    ngOnInit() {
    }
};
AddExercisePage.ctorParameters = () => [];
AddExercisePage = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-add-exercise',
        template: Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(/*! raw-loader!./add-exercise.page.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/pages/tabs/home/add-exercise/add-exercise.page.html")).default,
        styles: [Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(/*! ./add-exercise.page.scss */ "./src/app/pages/tabs/home/add-exercise/add-exercise.page.scss")).default]
    })
], AddExercisePage);



/***/ })

}]);
//# sourceMappingURL=pages-tabs-home-add-exercise-add-exercise-module-es2015.js.map