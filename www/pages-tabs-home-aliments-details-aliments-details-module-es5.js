(function () {
  function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

  function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

  function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

  (window["webpackJsonp"] = window["webpackJsonp"] || []).push([["pages-tabs-home-aliments-details-aliments-details-module"], {
    /***/
    "./node_modules/raw-loader/dist/cjs.js!./src/app/pages/tabs/home/aliments-details/aliments-details.page.html":
    /*!*******************************************************************************************************************!*\
      !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/pages/tabs/home/aliments-details/aliments-details.page.html ***!
      \*******************************************************************************************************************/

    /*! exports provided: default */

    /***/
    function node_modulesRawLoaderDistCjsJsSrcAppPagesTabsHomeAlimentsDetailsAlimentsDetailsPageHtml(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony default export */


      __webpack_exports__["default"] = "<ion-content>\n    <div class=\"titlebox\">\n        <div class=\"titlebox-arrow\">\n            <div>\n                <ion-icon name=\"chevron-back-outline\"></ion-icon>\n            </div>\n            <div class=\"ion-text-right\">\n                <ion-icon name=\"heart-outline\"></ion-icon>\n            </div>\n        </div>\n        <div class=\"title-img\">\n            <img src=\"assets/images/food.png\">\n        </div>\n        <div class=\"titlebox-text\">\n            <h4 style=\"margin: 0 !important; padding: 0 !important;\">Blanc de poulet</h4>\n            <h5>Sous-titre</h5>\n        </div>\n    </div>\n    <div class=\"kcal-box-bg\">\n        <ion-row>\n            <ion-col size=\"3\">\n                <div class=\"kcal-box\">\n                    <h4>119 kcal</h4>\n                    <p>Calories</p>\n                </div>\n            </ion-col>\n            <ion-col size=\"3\">\n                <div class=\"kcal-box\">\n                    <h4>4.0g</h4>\n                    <p>Glucides</p>\n                </div>\n            </ion-col>\n            <ion-col size=\"3\">\n                <div class=\"kcal-box\">\n                    <h4>1.0g</h4>\n                    <p>Lipides</p>\n                </div>\n            </ion-col>\n            <ion-col size=\"3\">\n                <div class=\"kcal-box\">\n                    <h4>119 kcal13.5g</h4>\n                    <p>Protéines</p>\n                </div>\n            </ion-col>\n        </ion-row>\n    </div>\n    <div>\n        <ion-list-header>VALEURS NUTRITIONNELLES\n        </ion-list-header>\n        <ion-item>\n            <ion-label>Calories</ion-label>\n            <ion-note slot=\"end\" color=\"dark\">119 kcal</ion-note>\n        </ion-item>\n        <ion-item>\n            <ion-label>Glucides</ion-label>\n            <ion-note slot=\"end\" color=\"dark\">35g</ion-note>\n        </ion-item>\n        <ion-item>\n            <ion-label>Dont sucres </ion-label>\n            <ion-note slot=\"end\" color=\"dark\">3g</ion-note>\n        </ion-item>\n        <ion-item>\n            <ion-label>Lipides</ion-label>\n            <ion-note slot=\"end\" color=\"dark\">12g</ion-note>\n        </ion-item>\n        <ion-item>\n            <ion-label>Fibres alimentaires</ion-label>\n            <ion-note slot=\"end\" color=\"dark\">5g</ion-note>\n        </ion-item>\n        <ion-item>\n            <ion-label>Protéines</ion-label>\n            <ion-note slot=\"end\" color=\"dark\">19g</ion-note>\n        </ion-item>\n\n    </div>\n    <ion-row>\n        <div class=\"viewall\">\n            <ion-button>VOIR TOUS LES ALIMENTS\n                <ion-icon name=\"chevron-forward-outline\"></ion-icon>\n            </ion-button>\n        </div>\n    </ion-row>\n    <ion-row>\n        <ion-col size=\"5\">\n            <div class=\"protin\">\n                <h4 class=\"small_text_bold\">Nombre de portion</h4>\n                <p class=\"p-b-0 m-b-0 p-t-0 m-t-0\">1</p>\n            </div>\n        </ion-col>\n        <ion-col size=\"7\">\n            <div class=\"protin\">\n               \n                    <h4 class=\"small_text_bold\">Taille de la portion</h4>\n                    <h4 class=\"p-b-0 m-b-0 p-t-0 m-t-0\">100<sub>g</sub></h4>\n                    <ion-select value=\"notifications\" interface=\"action-sheet\" style=\"top: -50px;\">\n                        <ion-select-option value=\"enable\">Enable</ion-select-option>\n                        <ion-select-option value=\"mute\">Mute</ion-select-option>\n                        <ion-select-option value=\"mute_week\">Mute for a week</ion-select-option>\n                        <ion-select-option value=\"mute_year\">Mute for a year</ion-select-option>\n                    </ion-select>\n                   \n               \n                <!-- <ion-item>\n                    <ion-label class=\"small_text_bold\">Taille de la portion</ion-label>\n                    <ion-select value=\"notifications\" interface=\"action-sheet\">\n                        <ion-select-option value=\"enable\">Enable</ion-select-option>\n                        <ion-select-option value=\"mute\">Mute</ion-select-option>\n                        <ion-select-option value=\"mute_week\">Mute for a week</ion-select-option>\n                        <ion-select-option value=\"mute_year\">Mute for a year</ion-select-option>\n                    </ion-select>\n                </ion-item>\n                <h4>100<sub>g</sub></h4> -->\n            </div>\n        </ion-col>\n        <ion-col size=\"12\">\n            <div class=\"connect\">\n                <ion-button shape=\"round\" expand=\"full\" routerLink=\"/conseils-nutritionnels\">Ajouter l’aliment\n                </ion-button>\n            </div>\n        </ion-col>\n    </ion-row>\n</ion-content>";
      /***/
    },

    /***/
    "./src/app/pages/tabs/home/aliments-details/aliments-details-routing.module.ts":
    /*!*************************************************************************************!*\
      !*** ./src/app/pages/tabs/home/aliments-details/aliments-details-routing.module.ts ***!
      \*************************************************************************************/

    /*! exports provided: AlimentsDetailsPageRoutingModule */

    /***/
    function srcAppPagesTabsHomeAlimentsDetailsAlimentsDetailsRoutingModuleTs(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "AlimentsDetailsPageRoutingModule", function () {
        return AlimentsDetailsPageRoutingModule;
      });
      /* harmony import */


      var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! tslib */
      "./node_modules/tslib/tslib.es6.js");
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! @angular/core */
      "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
      /* harmony import */


      var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! @angular/router */
      "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");
      /* harmony import */


      var _aliments_details_page__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
      /*! ./aliments-details.page */
      "./src/app/pages/tabs/home/aliments-details/aliments-details.page.ts");

      var routes = [{
        path: '',
        component: _aliments_details_page__WEBPACK_IMPORTED_MODULE_3__["AlimentsDetailsPage"]
      }];

      var AlimentsDetailsPageRoutingModule = function AlimentsDetailsPageRoutingModule() {
        _classCallCheck(this, AlimentsDetailsPageRoutingModule);
      };

      AlimentsDetailsPageRoutingModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]]
      })], AlimentsDetailsPageRoutingModule);
      /***/
    },

    /***/
    "./src/app/pages/tabs/home/aliments-details/aliments-details.module.ts":
    /*!*****************************************************************************!*\
      !*** ./src/app/pages/tabs/home/aliments-details/aliments-details.module.ts ***!
      \*****************************************************************************/

    /*! exports provided: AlimentsDetailsPageModule */

    /***/
    function srcAppPagesTabsHomeAlimentsDetailsAlimentsDetailsModuleTs(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "AlimentsDetailsPageModule", function () {
        return AlimentsDetailsPageModule;
      });
      /* harmony import */


      var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! tslib */
      "./node_modules/tslib/tslib.es6.js");
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! @angular/core */
      "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
      /* harmony import */


      var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! @angular/common */
      "./node_modules/@angular/common/__ivy_ngcc__/fesm2015/common.js");
      /* harmony import */


      var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
      /*! @angular/forms */
      "./node_modules/@angular/forms/__ivy_ngcc__/fesm2015/forms.js");
      /* harmony import */


      var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
      /*! @ionic/angular */
      "./node_modules/@ionic/angular/__ivy_ngcc__/fesm2015/ionic-angular.js");
      /* harmony import */


      var _aliments_details_routing_module__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(
      /*! ./aliments-details-routing.module */
      "./src/app/pages/tabs/home/aliments-details/aliments-details-routing.module.ts");
      /* harmony import */


      var _aliments_details_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(
      /*! ./aliments-details.page */
      "./src/app/pages/tabs/home/aliments-details/aliments-details.page.ts");

      var AlimentsDetailsPageModule = function AlimentsDetailsPageModule() {
        _classCallCheck(this, AlimentsDetailsPageModule);
      };

      AlimentsDetailsPageModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"], _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"], _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonicModule"], _aliments_details_routing_module__WEBPACK_IMPORTED_MODULE_5__["AlimentsDetailsPageRoutingModule"]],
        declarations: [_aliments_details_page__WEBPACK_IMPORTED_MODULE_6__["AlimentsDetailsPage"]]
      })], AlimentsDetailsPageModule);
      /***/
    },

    /***/
    "./src/app/pages/tabs/home/aliments-details/aliments-details.page.scss":
    /*!*****************************************************************************!*\
      !*** ./src/app/pages/tabs/home/aliments-details/aliments-details.page.scss ***!
      \*****************************************************************************/

    /*! exports provided: default */

    /***/
    function srcAppPagesTabsHomeAlimentsDetailsAlimentsDetailsPageScss(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony default export */


      __webpack_exports__["default"] = ".titlebox {\n  position: relative;\n  color: #fff;\n  height: 250px;\n  overflow: hidden;\n}\n\n.titlebox-arrow {\n  display: flex;\n  position: absolute;\n  width: 100%;\n  top: 50%;\n  padding: 0px 5%;\n  transform: translateY(-50%);\n}\n\n.titlebox-arrow div {\n  flex-grow: 1;\n  font-size: 30px;\n}\n\n.titlebox-text {\n  position: absolute;\n  bottom: 10%;\n  left: 5%;\n}\n\n.title-img {\n  background: #000;\n  height: 100%;\n}\n\n.title-img img {\n  height: 100%;\n  -o-object-fit: cover;\n     object-fit: cover;\n  width: 100%;\n}\n\n.titlebox-text h5 {\n  font-size: 12px;\n  margin: 0;\n}\n\n.titlebox {\n  position: relative;\n  color: #fff;\n}\n\n.kcal-box {\n  text-align: center;\n}\n\n.kcal-box h4 {\n  font-size: 14px;\n  font-weight: bold;\n  margin: 0;\n  padding-top: 5px;\n}\n\n.kcal-box p {\n  font-size: 12px;\n  margin: 0;\n}\n\n.kcal-box-bg {\n  background: #eee;\n  border-radius: 16px;\n  margin: 5px;\n  margin-top: 15px;\n}\n\n.viewall {\n  width: 100%;\n  position: relative;\n  margin-bottom: 5px;\n}\n\n.viewall ion-button {\n  --border-radius: 25px;\n  display: table;\n  margin: auto;\n  font-size: 12px;\n}\n\n.viewall:before {\n  content: \"\";\n  position: absolute;\n  background: #c1c1c1;\n  height: 1px;\n  width: 100%;\n  top: 19px;\n  left: 0;\n}\n\n.protin ion-item {\n  --inner-border-width: 0 0 0px 0;\n  --border-width: 0 0 0px 0;\n  --background: transparent;\n  --padding-start: 0px;\n}\n\nion-item {\n  --inner-border-width: 0 0 0px 0;\n  --border-width: 0 0 0px 0;\n  height: 33px;\n}\n\n.protin {\n  background: #ccc;\n  height: 70px;\n  border-radius: 10px;\n  padding: 10px;\n}\n\n.protin h4 {\n  margin: 0;\n}\n\n.connect ion-button {\n  --background: var(--ion-btn-custom-color);\n  --border-radius: 5px;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvcGFnZXMvdGFicy9ob21lL2FsaW1lbnRzLWRldGFpbHMvYWxpbWVudHMtZGV0YWlscy5wYWdlLnNjc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUE7RUFDSSxrQkFBQTtFQUNBLFdBQUE7RUFDQSxhQUFBO0VBQ0EsZ0JBQUE7QUFDSjs7QUFFQTtFQUNJLGFBQUE7RUFDQSxrQkFBQTtFQUNBLFdBQUE7RUFDQSxRQUFBO0VBQ0EsZUFBQTtFQUNBLDJCQUFBO0FBQ0o7O0FBRUE7RUFDSSxZQUFBO0VBQ0EsZUFBQTtBQUNKOztBQUVBO0VBQ0ksa0JBQUE7RUFDQSxXQUFBO0VBQ0EsUUFBQTtBQUNKOztBQUVBO0VBQ0ksZ0JBQUE7RUFDQSxZQUFBO0FBQ0o7O0FBRUE7RUFDSSxZQUFBO0VBQ0Esb0JBQUE7S0FBQSxpQkFBQTtFQUNBLFdBQUE7QUFDSjs7QUFFQTtFQUNJLGVBQUE7RUFDQSxTQUFBO0FBQ0o7O0FBRUE7RUFDSSxrQkFBQTtFQUNBLFdBQUE7QUFDSjs7QUFFQTtFQUNJLGtCQUFBO0FBQ0o7O0FBRUE7RUFDSSxlQUFBO0VBQ0EsaUJBQUE7RUFDQSxTQUFBO0VBQ0EsZ0JBQUE7QUFDSjs7QUFFQTtFQUNJLGVBQUE7RUFDQSxTQUFBO0FBQ0o7O0FBRUE7RUFDSSxnQkFBQTtFQUNBLG1CQUFBO0VBQ0EsV0FBQTtFQUNBLGdCQUFBO0FBQ0o7O0FBRUE7RUFDSSxXQUFBO0VBQ0Esa0JBQUE7RUFFQSxrQkFBQTtBQUFKOztBQUdBO0VBQ0kscUJBQUE7RUFDQSxjQUFBO0VBQ0EsWUFBQTtFQUNBLGVBQUE7QUFBSjs7QUFHQTtFQUNJLFdBQUE7RUFDQSxrQkFBQTtFQUNBLG1CQUFBO0VBQ0EsV0FBQTtFQUNBLFdBQUE7RUFDQSxTQUFBO0VBQ0EsT0FBQTtBQUFKOztBQUdBO0VBQ0ksK0JBQUE7RUFDQSx5QkFBQTtFQUNBLHlCQUFBO0VBQ0Esb0JBQUE7QUFBSjs7QUFHQTtFQUNJLCtCQUFBO0VBQ0EseUJBQUE7RUFDQSxZQUFBO0FBQUo7O0FBR0E7RUFDSSxnQkFBQTtFQUNBLFlBQUE7RUFDQSxtQkFBQTtFQUNBLGFBQUE7QUFBSjs7QUFHQTtFQUNJLFNBQUE7QUFBSjs7QUFHQTtFQUNJLHlDQUFBO0VBQ0Esb0JBQUE7QUFBSiIsImZpbGUiOiJzcmMvYXBwL3BhZ2VzL3RhYnMvaG9tZS9hbGltZW50cy1kZXRhaWxzL2FsaW1lbnRzLWRldGFpbHMucGFnZS5zY3NzIiwic291cmNlc0NvbnRlbnQiOlsiLnRpdGxlYm94IHtcbiAgICBwb3NpdGlvbjogcmVsYXRpdmU7XG4gICAgY29sb3I6ICNmZmY7XG4gICAgaGVpZ2h0OiAyNTBweDtcbiAgICBvdmVyZmxvdzogaGlkZGVuO1xufVxuXG4udGl0bGVib3gtYXJyb3cge1xuICAgIGRpc3BsYXk6IGZsZXg7XG4gICAgcG9zaXRpb246IGFic29sdXRlO1xuICAgIHdpZHRoOiAxMDAlO1xuICAgIHRvcDogNTAlO1xuICAgIHBhZGRpbmc6IDBweCA1JTtcbiAgICB0cmFuc2Zvcm06IHRyYW5zbGF0ZVkoLTUwJSk7XG59XG5cbi50aXRsZWJveC1hcnJvdyBkaXYge1xuICAgIGZsZXgtZ3JvdzogMTtcbiAgICBmb250LXNpemU6IDMwcHg7XG59XG5cbi50aXRsZWJveC10ZXh0IHtcbiAgICBwb3NpdGlvbjogYWJzb2x1dGU7XG4gICAgYm90dG9tOiAxMCU7XG4gICAgbGVmdDogNSU7XG59XG5cbi50aXRsZS1pbWcge1xuICAgIGJhY2tncm91bmQ6ICMwMDA7XG4gICAgaGVpZ2h0OiAxMDAlO1xufVxuXG4udGl0bGUtaW1nIGltZyB7XG4gICAgaGVpZ2h0OiAxMDAlO1xuICAgIG9iamVjdC1maXQ6IGNvdmVyO1xuICAgIHdpZHRoOiAxMDAlO1xufVxuXG4udGl0bGVib3gtdGV4dCBoNSB7XG4gICAgZm9udC1zaXplOiAxMnB4O1xuICAgIG1hcmdpbjogMDtcbn1cblxuLnRpdGxlYm94IHtcbiAgICBwb3NpdGlvbjogcmVsYXRpdmU7XG4gICAgY29sb3I6ICNmZmY7XG59XG5cbi5rY2FsLWJveCB7XG4gICAgdGV4dC1hbGlnbjogY2VudGVyO1xufVxuXG4ua2NhbC1ib3ggaDQge1xuICAgIGZvbnQtc2l6ZTogMTRweDtcbiAgICBmb250LXdlaWdodDogYm9sZDtcbiAgICBtYXJnaW46IDA7XG4gICAgcGFkZGluZy10b3A6IDVweDtcbn1cblxuLmtjYWwtYm94IHAge1xuICAgIGZvbnQtc2l6ZTogMTJweDtcbiAgICBtYXJnaW46IDA7XG59XG5cbi5rY2FsLWJveC1iZyB7XG4gICAgYmFja2dyb3VuZDogI2VlZTtcbiAgICBib3JkZXItcmFkaXVzOiAxNnB4O1xuICAgIG1hcmdpbjogNXB4O1xuICAgIG1hcmdpbi10b3A6IDE1cHg7XG59XG5cbi52aWV3YWxsIHtcbiAgICB3aWR0aDogMTAwJTtcbiAgICBwb3NpdGlvbjogcmVsYXRpdmU7XG4gICAgLy8gbWFyZ2luLXRvcDogMTVweDtcbiAgICBtYXJnaW4tYm90dG9tOiA1cHg7XG59XG5cbi52aWV3YWxsIGlvbi1idXR0b24ge1xuICAgIC0tYm9yZGVyLXJhZGl1czogMjVweDtcbiAgICBkaXNwbGF5OiB0YWJsZTtcbiAgICBtYXJnaW46IGF1dG87XG4gICAgZm9udC1zaXplOiAxMnB4O1xufVxuXG4udmlld2FsbDpiZWZvcmUge1xuICAgIGNvbnRlbnQ6IFwiXCI7XG4gICAgcG9zaXRpb246IGFic29sdXRlO1xuICAgIGJhY2tncm91bmQ6ICNjMWMxYzE7XG4gICAgaGVpZ2h0OiAxcHg7XG4gICAgd2lkdGg6IDEwMCU7XG4gICAgdG9wOiAxOXB4O1xuICAgIGxlZnQ6IDA7XG59XG5cbi5wcm90aW4gaW9uLWl0ZW0ge1xuICAgIC0taW5uZXItYm9yZGVyLXdpZHRoOiAwIDAgMHB4IDA7XG4gICAgLS1ib3JkZXItd2lkdGg6IDAgMCAwcHggMDtcbiAgICAtLWJhY2tncm91bmQ6IHRyYW5zcGFyZW50O1xuICAgIC0tcGFkZGluZy1zdGFydDogMHB4O1xufVxuXG5pb24taXRlbSB7XG4gICAgLS1pbm5lci1ib3JkZXItd2lkdGg6IDAgMCAwcHggMDtcbiAgICAtLWJvcmRlci13aWR0aDogMCAwIDBweCAwO1xuICAgIGhlaWdodDogMzNweDtcbn1cblxuLnByb3RpbiB7XG4gICAgYmFja2dyb3VuZDogI2NjYztcbiAgICBoZWlnaHQ6IDcwcHg7XG4gICAgYm9yZGVyLXJhZGl1czogMTBweDtcbiAgICBwYWRkaW5nOiAxMHB4O1xufVxuXG4ucHJvdGluIGg0IHtcbiAgICBtYXJnaW46IDA7XG59XG5cbi5jb25uZWN0IGlvbi1idXR0b24ge1xuICAgIC0tYmFja2dyb3VuZDogdmFyKC0taW9uLWJ0bi1jdXN0b20tY29sb3IpO1xuICAgIC0tYm9yZGVyLXJhZGl1czogNXB4O1xufSJdfQ== */";
      /***/
    },

    /***/
    "./src/app/pages/tabs/home/aliments-details/aliments-details.page.ts":
    /*!***************************************************************************!*\
      !*** ./src/app/pages/tabs/home/aliments-details/aliments-details.page.ts ***!
      \***************************************************************************/

    /*! exports provided: AlimentsDetailsPage */

    /***/
    function srcAppPagesTabsHomeAlimentsDetailsAlimentsDetailsPageTs(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "AlimentsDetailsPage", function () {
        return AlimentsDetailsPage;
      });
      /* harmony import */


      var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! tslib */
      "./node_modules/tslib/tslib.es6.js");
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! @angular/core */
      "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");

      var AlimentsDetailsPage = /*#__PURE__*/function () {
        function AlimentsDetailsPage() {
          _classCallCheck(this, AlimentsDetailsPage);
        }

        _createClass(AlimentsDetailsPage, [{
          key: "ngOnInit",
          value: function ngOnInit() {}
        }]);

        return AlimentsDetailsPage;
      }();

      AlimentsDetailsPage.ctorParameters = function () {
        return [];
      };

      AlimentsDetailsPage = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-aliments-details',
        template: Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(
        /*! raw-loader!./aliments-details.page.html */
        "./node_modules/raw-loader/dist/cjs.js!./src/app/pages/tabs/home/aliments-details/aliments-details.page.html"))["default"],
        styles: [Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(
        /*! ./aliments-details.page.scss */
        "./src/app/pages/tabs/home/aliments-details/aliments-details.page.scss"))["default"]]
      })], AlimentsDetailsPage);
      /***/
    }
  }]);
})();
//# sourceMappingURL=pages-tabs-home-aliments-details-aliments-details-module-es5.js.map