(function () {
  function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

  function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

  function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

  (window["webpackJsonp"] = window["webpackJsonp"] || []).push([["pages-tabs-menu-find-coach-find-coach-module"], {
    /***/
    "./node_modules/raw-loader/dist/cjs.js!./src/app/pages/tabs/menu/find-coach/find-coach.page.html":
    /*!*******************************************************************************************************!*\
      !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/pages/tabs/menu/find-coach/find-coach.page.html ***!
      \*******************************************************************************************************/

    /*! exports provided: default */

    /***/
    function node_modulesRawLoaderDistCjsJsSrcAppPagesTabsMenuFindCoachFindCoachPageHtml(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony default export */


      __webpack_exports__["default"] = "<ion-header>\n    <ion-toolbar>\n        <ion-buttons slot=\"start\">\n            <ion-back-button></ion-back-button>\n        </ion-buttons>\n        <ion-title>RECHERCHE DE VOTRE COACH IDEAL</ion-title>\n    </ion-toolbar>\n</ion-header>\n\n<ion-content>\n    <ion-grid>\n        <ion-row>\n            <ion-col size=\"12\">\n                <h4>Type de coach</h4>\n            </ion-col>\n            <ion-col size=\"6\">\n                <div class=\"gender\">\n                    <ion-button color=\"primary\" expand=\"full\">Un homme\n                    </ion-button>\n                </div>\n            </ion-col>\n            <ion-col size=\"6\">\n                <div class=\"gender\">\n                    <ion-button color=\"light\" expand=\"full\">Un homme\n                    </ion-button>\n                </div>\n            </ion-col>\n            <ion-col size=\"12\">\n                <h4 class=\"f14\">\n                    Sélectionnez un type de coaching dans la liste suivante :\n                </h4>\n            </ion-col>\n            <ion-col size=\"12\">\n                <ion-item class=\"coachingathlete\">\n                    <ion-avatar slot=\"start\">\n                        <img src=\"assets/icon/masculation.png\">\n                    </ion-avatar>\n                    <ion-label>\n                        COACHING ATHLETE – Préparation physique\n                    </ion-label>\n                </ion-item>\n            </ion-col>\n            <ion-col size=\"12\">\n                <h4 class=\"f14\">Sélectionnez le coach qui vous convient le mieux dans la liste suivante :\n                </h4>\n                <p class=\"f12\">Voici tous les coachs que le système Transformatics met à votre disposition. Libre à vous de choisir celui ou celle qui vous correspond le mieux ! Les coachings TRANSFORMATICS sont établis suivant une tarification fixe. Vous pourrez les\n                    observer dans la grille ci-dessous :\n                </p>\n            </ion-col>\n            <ion-col size=\"12\">\n                <ion-slides class=\"coach-slider\" [options]=\"categories\">\n                    <ion-slide *ngFor=\"let item of [1,2,3,4,5] \">\n                        <ion-card class=\"program\">\n                            <ion-card-content>\n                                <ion-row>\n                                    <ion-col size=\"8\">\n                                        <p class=\"coach-first-name\">Thomas</p>\n                                        <h4 class=\"coach-last-name\">zace</h4>\n                                        <p class=\"coachstaps\">STAPS MS </p>\n                                        <P class=\"coachbpjeps\">BPJEPS AGFF</P>\n                                    </ion-col>\n                                    <ion-col size=\"4\">\n                                        <p class=\"coachnumber ion-text-center\">3</p>\n                                    </ion-col>\n                                    <ion-col size=\"12\">\n                                        <p>\n                                            Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.\n                                        </p>\n\n                                    </ion-col>\n                                    <ion-col size=\"6\">\n                                        <div class=\"ion-text-right coch-box-result\">\n                                            <p>NOMBRD DE COACHINGS</p>\n                                            <p>ACTULS</p>\n                                            <h4>18</h4>\n                                        </div>\n                                    </ion-col>\n                                    <ion-col size=\"6\">\n                                        <div class=\"ion-text-right coch-box-result\">\n                                            <p>% DE SATISFACTION</p>\n                                            <p>CLIENT</p>\n                                            <h4>97%</h4>\n                                        </div>\n                                    </ion-col>\n                                    <ion-col size=\"12\">\n                                        <div class=\"domaine\">\n                                            <div>DOMAINE 1</div>\n                                            <div>DOMAINE 2</div>\n                                            <div>DOMAINE 3</div>\n                                        </div>\n                                    </ion-col>\n                                </ion-row>\n                            </ion-card-content>\n                        </ion-card>\n                    </ion-slide>\n                </ion-slides>\n            </ion-col>\n            <ion-col size=\"12\">\n                <p>Sélectionnez votre offre :\n                </p>\n            </ion-col>\n            <ion-col size=\"12\">\n                <div class=\"coach-price\">\n                    <div class=\"coach-price-box\">\n                        <h4>420€ / 6 mois</h4>\n                        <p>Soit 70€/mois</p>\n                    </div>\n                    <div>\n                        <h4>420€ / 6 mois</h4>\n                        <p>Soit 70€/mois</p>\n                    </div>\n                </div>\n            </ion-col>\n            <ion-col size=\"12\">\n                <div class=\"connect\">\n                    <ion-button shape=\"round\" expand=\"full\" routerLink=\"/chat\">Commencer\n                    </ion-button>\n                </div>\n            </ion-col>\n        </ion-row>\n    </ion-grid>\n</ion-content>";
      /***/
    },

    /***/
    "./src/app/pages/tabs/menu/find-coach/find-coach-routing.module.ts":
    /*!*************************************************************************!*\
      !*** ./src/app/pages/tabs/menu/find-coach/find-coach-routing.module.ts ***!
      \*************************************************************************/

    /*! exports provided: FindCoachPageRoutingModule */

    /***/
    function srcAppPagesTabsMenuFindCoachFindCoachRoutingModuleTs(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "FindCoachPageRoutingModule", function () {
        return FindCoachPageRoutingModule;
      });
      /* harmony import */


      var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! tslib */
      "./node_modules/tslib/tslib.es6.js");
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! @angular/core */
      "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
      /* harmony import */


      var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! @angular/router */
      "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");
      /* harmony import */


      var _find_coach_page__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
      /*! ./find-coach.page */
      "./src/app/pages/tabs/menu/find-coach/find-coach.page.ts");

      var routes = [{
        path: '',
        component: _find_coach_page__WEBPACK_IMPORTED_MODULE_3__["FindCoachPage"]
      }];

      var FindCoachPageRoutingModule = function FindCoachPageRoutingModule() {
        _classCallCheck(this, FindCoachPageRoutingModule);
      };

      FindCoachPageRoutingModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]]
      })], FindCoachPageRoutingModule);
      /***/
    },

    /***/
    "./src/app/pages/tabs/menu/find-coach/find-coach.module.ts":
    /*!*****************************************************************!*\
      !*** ./src/app/pages/tabs/menu/find-coach/find-coach.module.ts ***!
      \*****************************************************************/

    /*! exports provided: FindCoachPageModule */

    /***/
    function srcAppPagesTabsMenuFindCoachFindCoachModuleTs(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "FindCoachPageModule", function () {
        return FindCoachPageModule;
      });
      /* harmony import */


      var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! tslib */
      "./node_modules/tslib/tslib.es6.js");
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! @angular/core */
      "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
      /* harmony import */


      var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! @angular/common */
      "./node_modules/@angular/common/__ivy_ngcc__/fesm2015/common.js");
      /* harmony import */


      var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
      /*! @angular/forms */
      "./node_modules/@angular/forms/__ivy_ngcc__/fesm2015/forms.js");
      /* harmony import */


      var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
      /*! @ionic/angular */
      "./node_modules/@ionic/angular/__ivy_ngcc__/fesm2015/ionic-angular.js");
      /* harmony import */


      var _find_coach_routing_module__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(
      /*! ./find-coach-routing.module */
      "./src/app/pages/tabs/menu/find-coach/find-coach-routing.module.ts");
      /* harmony import */


      var _find_coach_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(
      /*! ./find-coach.page */
      "./src/app/pages/tabs/menu/find-coach/find-coach.page.ts");

      var FindCoachPageModule = function FindCoachPageModule() {
        _classCallCheck(this, FindCoachPageModule);
      };

      FindCoachPageModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"], _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"], _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonicModule"], _find_coach_routing_module__WEBPACK_IMPORTED_MODULE_5__["FindCoachPageRoutingModule"]],
        declarations: [_find_coach_page__WEBPACK_IMPORTED_MODULE_6__["FindCoachPage"]]
      })], FindCoachPageModule);
      /***/
    },

    /***/
    "./src/app/pages/tabs/menu/find-coach/find-coach.page.scss":
    /*!*****************************************************************!*\
      !*** ./src/app/pages/tabs/menu/find-coach/find-coach.page.scss ***!
      \*****************************************************************/

    /*! exports provided: default */

    /***/
    function srcAppPagesTabsMenuFindCoachFindCoachPageScss(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony default export */


      __webpack_exports__["default"] = ".gender ion-button {\n  white-space: normal;\n  font-size: 12px;\n  --padding-start: 0;\n  --padding-end: 0;\n  height: 50px;\n  font-family: Oswald, sans-serif;\n}\n\n.coachingathlete {\n  --inner-border-width: 0 0 0px 0;\n  --background: linear-gradient(to right, #ffc9a4ff, #ffece3ff);\n}\n\nion-card.program {\n  width: 95%;\n  margin-inline: 5px;\n  padding: 0px 10px;\n  --background: linear-gradient(to top, #707070, #bebebe);\n  color: #000;\n  margin: 30px 0px;\n}\n\nion-card-content {\n  -webkit-padding-start: 0px;\n  padding-inline-start: 0px;\n  -webkit-padding-end: 0px;\n  padding-inline-end: 0px;\n}\n\nion-card.program.active {\n  transform: scale(1.1, 1.1);\n  transition: all 0.5s ease;\n  transition: all 0.5s ease;\n}\n\n.coach-slider ion-slide {\n  text-align: left;\n}\n\n.coachnumber {\n  font-size: 118px;\n  line-height: 83px;\n  font-weight: bolder;\n}\n\n.coach-last-name {\n  font-size: 18px;\n  font-weight: bolder;\n  color: #000;\n  text-transform: uppercase;\n}\n\n.coach-first-name {\n  font-size: 20px;\n}\n\n.coachstaps {\n  font-size: 14px;\n  margin-top: 10px;\n  text-transform: uppercase;\n}\n\n.coachbpjeps {\n  font-size: 16px;\n  text-transform: uppercase;\n}\n\n.coch-box-result {\n  position: relative;\n  border-radius: 8px;\n  font-size: 12px;\n  box-shadow: rgba(0, 0, 0, 0.12) 0px 4px 16px;\n  padding: 5px;\n  background: #fff;\n}\n\n.coch-box-result p {\n  font-size: 10px;\n}\n\n.coch-box-result h4 {\n  font-size: 16px;\n  font-weight: bolder;\n}\n\n.domaine div {\n  background: #000;\n  color: #fff;\n  padding: 5px 10px;\n  margin-bottom: 5px;\n  border-radius: 5px;\n  font-size: 12px;\n}\n\n.coach-price {\n  display: flex;\n}\n\n.coach-price div {\n  flex-grow: 1;\n  padding: 10px;\n  position: relative;\n}\n\n.coach-price-box:before {\n  content: \"\";\n  position: absolute;\n  height: 50%;\n  width: 1px;\n  background: #000;\n  right: 5%;\n  top: 23%;\n}\n\n.coach-price {\n  border: 1px solid #7f7f7f;\n}\n\n.connect ion-button {\n  --background: var(--ion-btn-custom-color);\n  --border-radius: 5px;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvcGFnZXMvdGFicy9tZW51L2ZpbmQtY29hY2gvZmluZC1jb2FjaC5wYWdlLnNjc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUE7RUFDSSxtQkFBQTtFQUNBLGVBQUE7RUFDQSxrQkFBQTtFQUNBLGdCQUFBO0VBQ0EsWUFBQTtFQUNBLCtCQUFBO0FBQ0o7O0FBRUE7RUFDSSwrQkFBQTtFQUNBLDZEQUFBO0FBQ0o7O0FBRUE7RUFDSSxVQUFBO0VBQ0Esa0JBQUE7RUFDQSxpQkFBQTtFQUNBLHVEQUFBO0VBQ0EsV0FBQTtFQUNBLGdCQUFBO0FBQ0o7O0FBRUE7RUFDSSwwQkFBQTtFQUNBLHlCQUFBO0VBQ0Esd0JBQUE7RUFDQSx1QkFBQTtBQUNKOztBQUVBO0VBQ0ksMEJBQUE7RUFDQSx5QkFBQTtFQUNBLHlCQUFBO0FBQ0o7O0FBRUE7RUFDSSxnQkFBQTtBQUNKOztBQUVBO0VBQ0ksZ0JBQUE7RUFDQSxpQkFBQTtFQUNBLG1CQUFBO0FBQ0o7O0FBRUE7RUFDSSxlQUFBO0VBQ0EsbUJBQUE7RUFDQSxXQUFBO0VBQ0EseUJBQUE7QUFDSjs7QUFFQTtFQUNJLGVBQUE7QUFDSjs7QUFFQTtFQUNJLGVBQUE7RUFDQSxnQkFBQTtFQUNBLHlCQUFBO0FBQ0o7O0FBRUE7RUFDSSxlQUFBO0VBQ0EseUJBQUE7QUFDSjs7QUFFQTtFQUNJLGtCQUFBO0VBQ0Esa0JBQUE7RUFDQSxlQUFBO0VBQ0EsNENBQUE7RUFDQSxZQUFBO0VBQ0EsZ0JBQUE7QUFDSjs7QUFFQTtFQUNJLGVBQUE7QUFDSjs7QUFFQTtFQUNJLGVBQUE7RUFDQSxtQkFBQTtBQUNKOztBQUVBO0VBQ0ksZ0JBQUE7RUFDQSxXQUFBO0VBQ0EsaUJBQUE7RUFDQSxrQkFBQTtFQUNBLGtCQUFBO0VBQ0EsZUFBQTtBQUNKOztBQUVBO0VBQ0ksYUFBQTtBQUNKOztBQUVBO0VBQ0ksWUFBQTtFQUNBLGFBQUE7RUFDQSxrQkFBQTtBQUNKOztBQUVBO0VBQ0ksV0FBQTtFQUNBLGtCQUFBO0VBQ0EsV0FBQTtFQUNBLFVBQUE7RUFDQSxnQkFBQTtFQUNBLFNBQUE7RUFDQSxRQUFBO0FBQ0o7O0FBRUE7RUFDSSx5QkFBQTtBQUNKOztBQUVBO0VBQ0kseUNBQUE7RUFDQSxvQkFBQTtBQUNKIiwiZmlsZSI6InNyYy9hcHAvcGFnZXMvdGFicy9tZW51L2ZpbmQtY29hY2gvZmluZC1jb2FjaC5wYWdlLnNjc3MiLCJzb3VyY2VzQ29udGVudCI6WyIuZ2VuZGVyIGlvbi1idXR0b24ge1xuICAgIHdoaXRlLXNwYWNlOiBub3JtYWw7XG4gICAgZm9udC1zaXplOiAxMnB4O1xuICAgIC0tcGFkZGluZy1zdGFydDogMDtcbiAgICAtLXBhZGRpbmctZW5kOiAwO1xuICAgIGhlaWdodDogNTBweDtcbiAgICBmb250LWZhbWlseTogT3N3YWxkLCBzYW5zLXNlcmlmO1xufVxuXG4uY29hY2hpbmdhdGhsZXRlIHtcbiAgICAtLWlubmVyLWJvcmRlci13aWR0aDogMCAwIDBweCAwO1xuICAgIC0tYmFja2dyb3VuZDogbGluZWFyLWdyYWRpZW50KHRvIHJpZ2h0LCAjZmZjOWE0ZmYsICNmZmVjZTNmZik7XG59XG5cbmlvbi1jYXJkLnByb2dyYW0ge1xuICAgIHdpZHRoOiA5NSU7XG4gICAgbWFyZ2luLWlubGluZTogNXB4O1xuICAgIHBhZGRpbmc6IDBweCAxMHB4O1xuICAgIC0tYmFja2dyb3VuZDogbGluZWFyLWdyYWRpZW50KHRvIHRvcCwgIzcwNzA3MCwgI2JlYmViZSk7XG4gICAgY29sb3I6ICMwMDA7XG4gICAgbWFyZ2luOiAzMHB4IDBweDtcbn1cblxuaW9uLWNhcmQtY29udGVudCB7XG4gICAgLXdlYmtpdC1wYWRkaW5nLXN0YXJ0OiAwcHg7XG4gICAgcGFkZGluZy1pbmxpbmUtc3RhcnQ6IDBweDtcbiAgICAtd2Via2l0LXBhZGRpbmctZW5kOiAwcHg7XG4gICAgcGFkZGluZy1pbmxpbmUtZW5kOiAwcHg7XG59XG5cbmlvbi1jYXJkLnByb2dyYW0uYWN0aXZlIHtcbiAgICB0cmFuc2Zvcm06IHNjYWxlKDEuMSwgMS4xKTtcbiAgICB0cmFuc2l0aW9uOiBhbGwgMC41cyBlYXNlO1xuICAgIHRyYW5zaXRpb246IGFsbCAwLjVzIGVhc2U7XG59XG5cbi5jb2FjaC1zbGlkZXIgaW9uLXNsaWRlIHtcbiAgICB0ZXh0LWFsaWduOiBsZWZ0O1xufVxuXG4uY29hY2hudW1iZXIge1xuICAgIGZvbnQtc2l6ZTogMTE4cHg7XG4gICAgbGluZS1oZWlnaHQ6IDgzcHg7XG4gICAgZm9udC13ZWlnaHQ6IGJvbGRlcjtcbn1cblxuLmNvYWNoLWxhc3QtbmFtZSB7XG4gICAgZm9udC1zaXplOiAxOHB4O1xuICAgIGZvbnQtd2VpZ2h0OiBib2xkZXI7XG4gICAgY29sb3I6ICMwMDA7XG4gICAgdGV4dC10cmFuc2Zvcm06IHVwcGVyY2FzZTtcbn1cblxuLmNvYWNoLWZpcnN0LW5hbWUge1xuICAgIGZvbnQtc2l6ZTogMjBweDtcbn1cblxuLmNvYWNoc3RhcHMge1xuICAgIGZvbnQtc2l6ZTogMTRweDtcbiAgICBtYXJnaW4tdG9wOiAxMHB4O1xuICAgIHRleHQtdHJhbnNmb3JtOiB1cHBlcmNhc2U7XG59XG5cbi5jb2FjaGJwamVwcyB7XG4gICAgZm9udC1zaXplOiAxNnB4O1xuICAgIHRleHQtdHJhbnNmb3JtOiB1cHBlcmNhc2U7XG59XG5cbi5jb2NoLWJveC1yZXN1bHQge1xuICAgIHBvc2l0aW9uOiByZWxhdGl2ZTtcbiAgICBib3JkZXItcmFkaXVzOiA4cHg7XG4gICAgZm9udC1zaXplOiAxMnB4O1xuICAgIGJveC1zaGFkb3c6IHJnYigwIDAgMCAvIDEyJSkgMHB4IDRweCAxNnB4O1xuICAgIHBhZGRpbmc6IDVweDtcbiAgICBiYWNrZ3JvdW5kOiAjZmZmO1xufVxuXG4uY29jaC1ib3gtcmVzdWx0IHAge1xuICAgIGZvbnQtc2l6ZTogMTBweDtcbn1cblxuLmNvY2gtYm94LXJlc3VsdCBoNCB7XG4gICAgZm9udC1zaXplOiAxNnB4O1xuICAgIGZvbnQtd2VpZ2h0OiBib2xkZXI7XG59XG5cbi5kb21haW5lIGRpdiB7XG4gICAgYmFja2dyb3VuZDogIzAwMDtcbiAgICBjb2xvcjogI2ZmZjtcbiAgICBwYWRkaW5nOiA1cHggMTBweDtcbiAgICBtYXJnaW4tYm90dG9tOiA1cHg7XG4gICAgYm9yZGVyLXJhZGl1czogNXB4O1xuICAgIGZvbnQtc2l6ZTogMTJweDtcbn1cblxuLmNvYWNoLXByaWNlIHtcbiAgICBkaXNwbGF5OiBmbGV4O1xufVxuXG4uY29hY2gtcHJpY2UgZGl2IHtcbiAgICBmbGV4LWdyb3c6IDE7XG4gICAgcGFkZGluZzogMTBweDtcbiAgICBwb3NpdGlvbjogcmVsYXRpdmU7XG59XG5cbi5jb2FjaC1wcmljZS1ib3g6YmVmb3JlIHtcbiAgICBjb250ZW50OiBcIlwiO1xuICAgIHBvc2l0aW9uOiBhYnNvbHV0ZTtcbiAgICBoZWlnaHQ6IDUwJTtcbiAgICB3aWR0aDogMXB4O1xuICAgIGJhY2tncm91bmQ6ICMwMDA7XG4gICAgcmlnaHQ6IDUlO1xuICAgIHRvcDogMjMlO1xufVxuXG4uY29hY2gtcHJpY2Uge1xuICAgIGJvcmRlcjogMXB4IHNvbGlkICM3ZjdmN2Y7XG59XG5cbi5jb25uZWN0IGlvbi1idXR0b24ge1xuICAgIC0tYmFja2dyb3VuZDogdmFyKC0taW9uLWJ0bi1jdXN0b20tY29sb3IpO1xuICAgIC0tYm9yZGVyLXJhZGl1czogNXB4O1xufSJdfQ== */";
      /***/
    },

    /***/
    "./src/app/pages/tabs/menu/find-coach/find-coach.page.ts":
    /*!***************************************************************!*\
      !*** ./src/app/pages/tabs/menu/find-coach/find-coach.page.ts ***!
      \***************************************************************/

    /*! exports provided: FindCoachPage */

    /***/
    function srcAppPagesTabsMenuFindCoachFindCoachPageTs(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "FindCoachPage", function () {
        return FindCoachPage;
      });
      /* harmony import */


      var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! tslib */
      "./node_modules/tslib/tslib.es6.js");
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! @angular/core */
      "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");

      var FindCoachPage = /*#__PURE__*/function () {
        function FindCoachPage() {
          _classCallCheck(this, FindCoachPage);

          this.categories = {
            initialSlide: 1,
            loop: true,
            centeredSlides: true,
            slidesPerView: 1.7,
            slidesPerGroup: 1,
            spaceBetween: 5,
            speed: 500,
            fadeEffect: {
              crossFade: true
            }
          };
        }

        _createClass(FindCoachPage, [{
          key: "ngOnInit",
          value: function ngOnInit() {}
        }]);

        return FindCoachPage;
      }();

      FindCoachPage.ctorParameters = function () {
        return [];
      };

      FindCoachPage = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-find-coach',
        template: Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(
        /*! raw-loader!./find-coach.page.html */
        "./node_modules/raw-loader/dist/cjs.js!./src/app/pages/tabs/menu/find-coach/find-coach.page.html"))["default"],
        styles: [Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(
        /*! ./find-coach.page.scss */
        "./src/app/pages/tabs/menu/find-coach/find-coach.page.scss"))["default"]]
      })], FindCoachPage);
      /***/
    }
  }]);
})();
//# sourceMappingURL=pages-tabs-menu-find-coach-find-coach-module-es5.js.map