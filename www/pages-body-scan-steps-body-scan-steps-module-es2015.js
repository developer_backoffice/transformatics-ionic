(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["pages-body-scan-steps-body-scan-steps-module"],{

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/pages/body-scan-steps/body-scan-steps.page.html":
/*!*******************************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/pages/body-scan-steps/body-scan-steps.page.html ***!
  \*******************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<ion-header>\n  <ion-grid>\n    <ion-row>\n      <ion-col>\n        <div [class]=\"activepage == 1 ? 'steps active' : 'steps' \"></div>\n      </ion-col>\n      <ion-col>\n        <!-- <div ng-class=\"(activepage == 2) ? 'active' : 'steps'\"></div> -->\n        <div [class]=\"activepage == 2 ? 'steps active' : 'steps' \"></div>\n      </ion-col>\n      <ion-col>\n        <div [class]=\"activepage == 3 ? 'steps active' : 'steps' \"></div>\n      </ion-col>\n      <ion-col>\n        <div [class]=\"activepage == 4 ? 'steps active' : 'steps' \"></div>\n      </ion-col>\n      <ion-col>\n        <div [class]=\"activepage == 5 ? 'steps active' : 'steps' \"></div>\n      </ion-col>\n    </ion-row>\n  </ion-grid>\n</ion-header>\n<!--=================================-->\n<!--=================================-->\n<ion-content>\n  <!--=================================-->\n  <app-body-scan-step-one *ngIf=\"activepage == 1\" (stepOneSelected)='submitted($event)'>\n  </app-body-scan-step-one>\n  <!--=================================-->\n  <!--=================================-->\n  <app-body-scan-step-two *ngIf=\"activepage == 2\" (stepTwoSelected)='submitted($event)'> </app-body-scan-step-two>\n  <!--=================================-->\n  <!--=================================-->\n  <app-body-scan-step-three *ngIf=\"activepage == 3\" (stepThreeSelected)='submitted($event)'> </app-body-scan-step-three>\n  <!--=================================-->\n  <!--=================================-->\n  <app-body-scan-step-four *ngIf=\"activepage === 4\" (stepFourSelected)='submitted($event)'> </app-body-scan-step-four>\n  <!--=================================-->\n  <!--=================================-->\n  <app-body-scan-step-five *ngIf=\"activepage == 5\" (stepFiveSelected)='submitted($event)'> </app-body-scan-step-five>\n  <!--=================================-->\n</ion-content>\n<!--=================================-->\n<!--=================================-->\n<ion-footer>\n  <ion-toolbar>\n    <ion-grid>\n      <ion-row>\n        <ion-col>\n          <ion-button class=\"back\" expand=\"full\" shape=\"round\" (click)=\"previousPage()\">Retour\n          </ion-button>\n        </ion-col>\n        <ion-col>\n          <ion-button class=\"next\" expand=\"full\" shape=\"round\" (click)=\"nextPage()\">Etape suivante\n          </ion-button>\n        </ion-col>\n      </ion-row>\n    </ion-grid>\n  </ion-toolbar>\n</ion-footer>");

/***/ }),

/***/ "./src/app/pages/body-scan-steps/body-scan-steps-routing.module.ts":
/*!*************************************************************************!*\
  !*** ./src/app/pages/body-scan-steps/body-scan-steps-routing.module.ts ***!
  \*************************************************************************/
/*! exports provided: BodyScanStepsPageRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "BodyScanStepsPageRoutingModule", function() { return BodyScanStepsPageRoutingModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");
/* harmony import */ var _body_scan_steps_page__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./body-scan-steps.page */ "./src/app/pages/body-scan-steps/body-scan-steps.page.ts");




const routes = [
    {
        path: '',
        component: _body_scan_steps_page__WEBPACK_IMPORTED_MODULE_3__["BodyScanStepsPage"]
    }
];
let BodyScanStepsPageRoutingModule = class BodyScanStepsPageRoutingModule {
};
BodyScanStepsPageRoutingModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]],
    })
], BodyScanStepsPageRoutingModule);



/***/ }),

/***/ "./src/app/pages/body-scan-steps/body-scan-steps.module.ts":
/*!*****************************************************************!*\
  !*** ./src/app/pages/body-scan-steps/body-scan-steps.module.ts ***!
  \*****************************************************************/
/*! exports provided: BodyScanStepsPageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "BodyScanStepsPageModule", function() { return BodyScanStepsPageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/__ivy_ngcc__/fesm2015/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/__ivy_ngcc__/fesm2015/forms.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/__ivy_ngcc__/fesm2015/ionic-angular.js");
/* harmony import */ var _body_scan_steps_routing_module__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./body-scan-steps-routing.module */ "./src/app/pages/body-scan-steps/body-scan-steps-routing.module.ts");
/* harmony import */ var _body_scan_steps_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./body-scan-steps.page */ "./src/app/pages/body-scan-steps/body-scan-steps.page.ts");
/* harmony import */ var _components_shared_module__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! @components/shared.module */ "./src/app/components/shared.module.ts");








let BodyScanStepsPageModule = class BodyScanStepsPageModule {
};
BodyScanStepsPageModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [
            _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonicModule"],
            _body_scan_steps_routing_module__WEBPACK_IMPORTED_MODULE_5__["BodyScanStepsPageRoutingModule"],
            _components_shared_module__WEBPACK_IMPORTED_MODULE_7__["SharedModule"]
        ],
        declarations: [_body_scan_steps_page__WEBPACK_IMPORTED_MODULE_6__["BodyScanStepsPage"]]
    })
], BodyScanStepsPageModule);



/***/ }),

/***/ "./src/app/pages/body-scan-steps/body-scan-steps.page.scss":
/*!*****************************************************************!*\
  !*** ./src/app/pages/body-scan-steps/body-scan-steps.page.scss ***!
  \*****************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = (".steps {\n  height: 5px;\n  background: #000;\n  border-radius: 10px;\n}\n\n.steps.active {\n  background: #c1c1c1;\n}\n\n.back,\n.next {\n  --ion-button-round-border-radius: 5px;\n  --ion-button-border-radius: 5px;\n  --ion-border-radius: 5px;\n  --border-radius: 5px;\n  height: 40px;\n}\n\n.header-md::after {\n  background-image: none;\n}\n\n.next {\n  --background: var(--ion-btn-custom-color);\n}\n\n.back {\n  --background: linear-gradient(to right, #161616e0, #c1c1c1);\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvcGFnZXMvYm9keS1zY2FuLXN0ZXBzL2JvZHktc2Nhbi1zdGVwcy5wYWdlLnNjc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUE7RUFDSSxXQUFBO0VBQ0EsZ0JBQUE7RUFDQSxtQkFBQTtBQUNKOztBQUVBO0VBQ0ksbUJBQUE7QUFDSjs7QUFFQTs7RUFFSSxxQ0FBQTtFQUNBLCtCQUFBO0VBQ0Esd0JBQUE7RUFDQSxvQkFBQTtFQUNBLFlBQUE7QUFDSjs7QUFFQTtFQUNJLHNCQUFBO0FBQ0o7O0FBRUE7RUFDSSx5Q0FBQTtBQUNKOztBQUdBO0VBQ0ksMkRBQUE7QUFBSiIsImZpbGUiOiJzcmMvYXBwL3BhZ2VzL2JvZHktc2Nhbi1zdGVwcy9ib2R5LXNjYW4tc3RlcHMucGFnZS5zY3NzIiwic291cmNlc0NvbnRlbnQiOlsiLnN0ZXBzIHtcbiAgICBoZWlnaHQ6IDVweDtcbiAgICBiYWNrZ3JvdW5kOiAjMDAwO1xuICAgIGJvcmRlci1yYWRpdXM6IDEwcHg7XG59XG5cbi5zdGVwcy5hY3RpdmUge1xuICAgIGJhY2tncm91bmQ6ICNjMWMxYzE7XG59XG5cbi5iYWNrLFxuLm5leHQge1xuICAgIC0taW9uLWJ1dHRvbi1yb3VuZC1ib3JkZXItcmFkaXVzOiA1cHg7XG4gICAgLS1pb24tYnV0dG9uLWJvcmRlci1yYWRpdXM6IDVweDtcbiAgICAtLWlvbi1ib3JkZXItcmFkaXVzOiA1cHg7XG4gICAgLS1ib3JkZXItcmFkaXVzOiA1cHg7XG4gICAgaGVpZ2h0OiA0MHB4O1xufVxuXG4uaGVhZGVyLW1kOjphZnRlciB7XG4gICAgYmFja2dyb3VuZC1pbWFnZTogbm9uZTtcbn1cblxuLm5leHQge1xuICAgIC0tYmFja2dyb3VuZDogdmFyKC0taW9uLWJ0bi1jdXN0b20tY29sb3IpO1xuXG59XG5cbi5iYWNrIHtcbiAgICAtLWJhY2tncm91bmQ6IGxpbmVhci1ncmFkaWVudCh0byByaWdodCwgIzE2MTYxNmUwLCAjYzFjMWMxKTtcbn0iXX0= */");

/***/ }),

/***/ "./src/app/pages/body-scan-steps/body-scan-steps.page.ts":
/*!***************************************************************!*\
  !*** ./src/app/pages/body-scan-steps/body-scan-steps.page.ts ***!
  \***************************************************************/
/*! exports provided: BodyScanStepsPage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "BodyScanStepsPage", function() { return BodyScanStepsPage; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");
/* harmony import */ var _components_body_scan_step_one_body_scan_step_one_component__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../components/body-scan-step-one/body-scan-step-one.component */ "./src/app/components/body-scan-step-one/body-scan-step-one.component.ts");
/* harmony import */ var _services_userdata_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @services/userdata.service */ "./src/app/services/userdata.service.ts");
/* harmony import */ var _services_shared_localstroage_localstorage_service__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @services/shared/localstroage/localstorage.service */ "./src/app/services/shared/localstroage/localstorage.service.ts");






let BodyScanStepsPage = class BodyScanStepsPage {
    constructor(router, route, bodyScanStepOne, userDataService, localStorage) {
        this.router = router;
        this.route = route;
        this.bodyScanStepOne = bodyScanStepOne;
        this.userDataService = userDataService;
        this.localStorage = localStorage;
        this.activepage = 1;
    }
    ngOnInit() {
        console.log(this.activepage);
        // console.log(this.bodyScanStepOne.gender);
        // this.bodyScanStepOne.gender = "female";
        // console.log("here:" + this.bodyScanStepOne.gender);
    }
    nextPage() {
        if (this.activepage >= 5) {
            this.saveBodyScan();
            // this.router.navigate(['/tabs/home'])
            this.activepage = 1;
            return;
        }
        this.activepage += 1;
    }
    previousPage() {
        if (this.activepage <= 1) {
            this.activepage = 1;
            this.router.navigate(['body-scan-arrival']);
            return;
        }
        this.activepage -= 1;
        console.log("next", this.activepage);
    }
    submitted(event) {
        console.log(event);
        if (this.activepage == 1) {
            this.bodyScanStep1Details = event;
        }
        else if (this.activepage == 2) {
            this.bodyScanStep2Details = event;
        }
        else if (this.activepage == 3) {
            this.bodyScanStep3Details = event;
        }
        else if (this.activepage == 4) {
            this.bodyScanStep4Details = event;
        }
        else if (this.activepage == 5) {
            this.bodyScanStep5Details = event;
        }
    }
    saveBodyScan() {
        let user = this.localStorage.getItem('user');
        if (user) {
            user = JSON.parse(user);
        }
        console.log(user);
        this.bodyScanStepsDetails = Object.assign(Object.assign(Object.assign(Object.assign(Object.assign(Object.assign({ 'user_id': user['id'] }, this.bodyScanStep1Details), this.bodyScanStep2Details), this.bodyScanStep3Details), this.bodyScanStep4Details), this.bodyScanStep5Details), this.bodyScanStep1Details);
        console.log(this.bodyScanStepsDetails);
        let res = this.userDataService.bodyScanDetailsStore(this.bodyScanStepsDetails);
    }
};
BodyScanStepsPage.ctorParameters = () => [
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"] },
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_2__["ActivatedRoute"] },
    { type: _components_body_scan_step_one_body_scan_step_one_component__WEBPACK_IMPORTED_MODULE_3__["BodyScanStepOneComponent"] },
    { type: _services_userdata_service__WEBPACK_IMPORTED_MODULE_4__["UserdataService"] },
    { type: _services_shared_localstroage_localstorage_service__WEBPACK_IMPORTED_MODULE_5__["LocalstorageService"] }
];
BodyScanStepsPage.propDecorators = {
    text: [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"] }]
};
BodyScanStepsPage = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-body-scan-steps',
        template: Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(/*! raw-loader!./body-scan-steps.page.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/pages/body-scan-steps/body-scan-steps.page.html")).default,
        styles: [Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(/*! ./body-scan-steps.page.scss */ "./src/app/pages/body-scan-steps/body-scan-steps.page.scss")).default]
    })
], BodyScanStepsPage);



/***/ })

}]);
//# sourceMappingURL=pages-body-scan-steps-body-scan-steps-module-es2015.js.map