(function () {
  function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

  function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

  function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

  (window["webpackJsonp"] = window["webpackJsonp"] || []).push([["pages-tabs-menu-apprentissage-apprentissage-module"], {
    /***/
    "./node_modules/raw-loader/dist/cjs.js!./src/app/pages/tabs/menu/apprentissage/apprentissage.page.html":
    /*!*************************************************************************************************************!*\
      !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/pages/tabs/menu/apprentissage/apprentissage.page.html ***!
      \*************************************************************************************************************/

    /*! exports provided: default */

    /***/
    function node_modulesRawLoaderDistCjsJsSrcAppPagesTabsMenuApprentissageApprentissagePageHtml(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony default export */


      __webpack_exports__["default"] = "<ion-content>\n    <div class=\"title-box\">\n        <div class=\"title-box-img\">\n            <img src=\"assets/images/edit_physical_activity.png\">\n\n\n            <div class=\"back_btn_section\" routerLink=\"/tabs/menu\">\n                <ion-icon name=\"arrow-back-outline\" style=\"color: #fff;\"></ion-icon>\n            </div>\n        </div>\n        \n        <div class=\"title-box-text\">\n            <h4>APPRENTISSAGE</h4>\n            <p>Vidéos formatrices</p>\n        </div>\n    </div>\n    <ion-grid>\n        <ion-row>\n            <ion-col size=\"12\">\n                <div>\n                    <p class=\"small_text\">\n                        <strong>Transformatics</strong> met un point d’honneur à vous former pour que vous puissiez être à même de comprendre le fonctionnement de votre corps, son anatomie, les mécanismes nutritionnels et mentaux. Dans cet optique, nous\n                        vous donnons accès à un panel de vidéos explicatives sur de nombreux sujets relatifs au sport en général. Tournées par des professionnels de santé et spécialistes des milieux étudiés, ils vous aideront à apprendre toujours plus.\n                    </p>\n                    <p class=\"small_text\">\n                        <strong>Transformatics</strong> est plus qu’une application de coaching sportif : c’est une école de vie.\n                    </p>\n                </div>\n            </ion-col>\n            <ion-col size=\"12\">\n                <div>\n                    <h4 class=\"large_text\">Vidéos introductives</h4>\n                </div>\n                <div>\n                    <ion-slides pager=\"false\" [options]=\"categories\" #slides>\n                        <ion-slide>\n                            <ion-card class=\"program active\">\n                                <ion-card-content>\n                                    <iframe width=\"100%\" height=\"180\" src=\"https://www.youtube.com/embed/r2ga-iXS5i4\" frameborder=\"0\" allow=\"accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture\" allowfullscreen></iframe>\n                                </ion-card-content>\n                            </ion-card>\n                        </ion-slide>\n                        <ion-slide>\n                            <ion-card class=\"program\">\n                                <ion-card-content>\n                                    <iframe width=\"100%\" height=\"180\" src=\"https://www.youtube.com/embed/r2ga-iXS5i4\" frameborder=\"0\" allow=\"accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture\" allowfullscreen></iframe>\n                                </ion-card-content>\n                            </ion-card>\n                        </ion-slide>\n                        <ion-slide>\n                            <ion-card class=\"program\">\n                                <ion-card-content>\n                                    <iframe width=\"100%\" height=\"180\" src=\"https://www.youtube.com/embed/r2ga-iXS5i4\" frameborder=\"0\" allow=\"accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture\" allowfullscreen></iframe>\n                                </ion-card-content>\n                            </ion-card>\n                        </ion-slide>\n                    </ion-slides>\n                </div>\n            </ion-col>\n            <ion-col size=\"12\">\n                <div>\n                    <h4 class=\"large_text\">Vidéos introductives</h4>\n                </div>\n                <div>\n                    <iframe width=\"100%\" height=\"180\" src=\"https://www.youtube.com/embed/r2ga-iXS5i4\" frameborder=\"0\" allow=\"accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture\" allowfullscreen></iframe>\n                </div>\n\n            </ion-col>\n            <ion-col size=\"12\">\n                <div>\n                    <h4 class=\"large_text\">Ces erreurs qui m’ont coûté des années</h4>\n                </div>\n                <div>\n                    <iframe width=\"100%\" height=\"180\" src=\"https://www.youtube.com/embed/r2ga-iXS5i4\" frameborder=\"0\" allow=\"accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture\" allowfullscreen></iframe>\n                </div>\n\n            </ion-col>\n        </ion-row>\n    </ion-grid>\n</ion-content>";
      /***/
    },

    /***/
    "./src/app/pages/tabs/menu/apprentissage/apprentissage-routing.module.ts":
    /*!*******************************************************************************!*\
      !*** ./src/app/pages/tabs/menu/apprentissage/apprentissage-routing.module.ts ***!
      \*******************************************************************************/

    /*! exports provided: ApprentissagePageRoutingModule */

    /***/
    function srcAppPagesTabsMenuApprentissageApprentissageRoutingModuleTs(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "ApprentissagePageRoutingModule", function () {
        return ApprentissagePageRoutingModule;
      });
      /* harmony import */


      var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! tslib */
      "./node_modules/tslib/tslib.es6.js");
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! @angular/core */
      "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
      /* harmony import */


      var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! @angular/router */
      "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");
      /* harmony import */


      var _apprentissage_page__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
      /*! ./apprentissage.page */
      "./src/app/pages/tabs/menu/apprentissage/apprentissage.page.ts");

      var routes = [{
        path: '',
        component: _apprentissage_page__WEBPACK_IMPORTED_MODULE_3__["ApprentissagePage"]
      }];

      var ApprentissagePageRoutingModule = function ApprentissagePageRoutingModule() {
        _classCallCheck(this, ApprentissagePageRoutingModule);
      };

      ApprentissagePageRoutingModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]]
      })], ApprentissagePageRoutingModule);
      /***/
    },

    /***/
    "./src/app/pages/tabs/menu/apprentissage/apprentissage.module.ts":
    /*!***********************************************************************!*\
      !*** ./src/app/pages/tabs/menu/apprentissage/apprentissage.module.ts ***!
      \***********************************************************************/

    /*! exports provided: ApprentissagePageModule */

    /***/
    function srcAppPagesTabsMenuApprentissageApprentissageModuleTs(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "ApprentissagePageModule", function () {
        return ApprentissagePageModule;
      });
      /* harmony import */


      var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! tslib */
      "./node_modules/tslib/tslib.es6.js");
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! @angular/core */
      "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
      /* harmony import */


      var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! @angular/common */
      "./node_modules/@angular/common/__ivy_ngcc__/fesm2015/common.js");
      /* harmony import */


      var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
      /*! @angular/forms */
      "./node_modules/@angular/forms/__ivy_ngcc__/fesm2015/forms.js");
      /* harmony import */


      var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
      /*! @ionic/angular */
      "./node_modules/@ionic/angular/__ivy_ngcc__/fesm2015/ionic-angular.js");
      /* harmony import */


      var _apprentissage_routing_module__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(
      /*! ./apprentissage-routing.module */
      "./src/app/pages/tabs/menu/apprentissage/apprentissage-routing.module.ts");
      /* harmony import */


      var _apprentissage_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(
      /*! ./apprentissage.page */
      "./src/app/pages/tabs/menu/apprentissage/apprentissage.page.ts");

      var ApprentissagePageModule = function ApprentissagePageModule() {
        _classCallCheck(this, ApprentissagePageModule);
      };

      ApprentissagePageModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"], _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"], _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonicModule"], _apprentissage_routing_module__WEBPACK_IMPORTED_MODULE_5__["ApprentissagePageRoutingModule"]],
        declarations: [_apprentissage_page__WEBPACK_IMPORTED_MODULE_6__["ApprentissagePage"]]
      })], ApprentissagePageModule);
      /***/
    },

    /***/
    "./src/app/pages/tabs/menu/apprentissage/apprentissage.page.scss":
    /*!***********************************************************************!*\
      !*** ./src/app/pages/tabs/menu/apprentissage/apprentissage.page.scss ***!
      \***********************************************************************/

    /*! exports provided: default */

    /***/
    function srcAppPagesTabsMenuApprentissageApprentissagePageScss(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony default export */


      __webpack_exports__["default"] = "ion-content {\n  --padding-bottom: 4%;\n}\n\n.title-box {\n  position: relative;\n}\n\n.title-box-text {\n  position: absolute;\n  bottom: 5px;\n  background: #0c0c0c66;\n  color: #fff;\n  width: 50%;\n  padding: 10px 10px;\n}\n\n.title-box-text h4 {\n  margin: 0px;\n  font-size: 14px;\n}\n\n.title-box-text p {\n  margin: 0;\n  margin-top: 5px;\n  font-size: 12px;\n}\n\nion-card.program {\n  width: 85%;\n  margin-inline: 5px;\n}\n\nion-card-content {\n  -webkit-padding-start: 0px;\n  padding-inline-start: 0px;\n  -webkit-padding-end: 0px;\n  padding-inline-end: 0px;\n}\n\nion-card.program.active {\n  transform: scale(1.1, 1.1);\n  transition: all 0.5s ease;\n}\n\n.back_btn_section {\n  position: absolute;\n  left: 16px;\n  top: 18px;\n}\n\n.back_btn_section ion-icon {\n  font-size: 20px;\n}\n\n.large_text {\n  font-size: 1rem !important;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvcGFnZXMvdGFicy9tZW51L2FwcHJlbnRpc3NhZ2UvYXBwcmVudGlzc2FnZS5wYWdlLnNjc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUE7RUFDSSxvQkFBQTtBQUNKOztBQUNBO0VBQ0ksa0JBQUE7QUFFSjs7QUFDQTtFQUNJLGtCQUFBO0VBQ0EsV0FBQTtFQUNBLHFCQUFBO0VBQ0EsV0FBQTtFQUNBLFVBQUE7RUFDQSxrQkFBQTtBQUVKOztBQUNBO0VBQ0ksV0FBQTtFQUNBLGVBQUE7QUFFSjs7QUFDQTtFQUNJLFNBQUE7RUFDQSxlQUFBO0VBQ0EsZUFBQTtBQUVKOztBQUNBO0VBQ0ksVUFBQTtFQUNBLGtCQUFBO0FBRUo7O0FBQ0E7RUFDSSwwQkFBQTtFQUNBLHlCQUFBO0VBQ0Esd0JBQUE7RUFDQSx1QkFBQTtBQUVKOztBQUNBO0VBQ0ksMEJBQUE7RUFDQSx5QkFBQTtBQUVKOztBQUNBO0VBQ0ksa0JBQUE7RUFDQSxVQUFBO0VBQ0EsU0FBQTtBQUVKOztBQUFJO0VBQ0ksZUFBQTtBQUVSOztBQUdBO0VBQ0ksMEJBQUE7QUFBSiIsImZpbGUiOiJzcmMvYXBwL3BhZ2VzL3RhYnMvbWVudS9hcHByZW50aXNzYWdlL2FwcHJlbnRpc3NhZ2UucGFnZS5zY3NzIiwic291cmNlc0NvbnRlbnQiOlsiaW9uLWNvbnRlbnQge1xuICAgIC0tcGFkZGluZy1ib3R0b206IDQlO1xufVxuLnRpdGxlLWJveCB7XG4gICAgcG9zaXRpb246IHJlbGF0aXZlO1xufVxuXG4udGl0bGUtYm94LXRleHQge1xuICAgIHBvc2l0aW9uOiBhYnNvbHV0ZTtcbiAgICBib3R0b206IDVweDtcbiAgICBiYWNrZ3JvdW5kOiAjMGMwYzBjNjY7XG4gICAgY29sb3I6ICNmZmY7XG4gICAgd2lkdGg6IDUwJTtcbiAgICBwYWRkaW5nOiAxMHB4IDEwcHg7XG59XG5cbi50aXRsZS1ib3gtdGV4dCBoNCB7XG4gICAgbWFyZ2luOiAwcHg7XG4gICAgZm9udC1zaXplOiAxNHB4O1xufVxuXG4udGl0bGUtYm94LXRleHQgcCB7XG4gICAgbWFyZ2luOiAwO1xuICAgIG1hcmdpbi10b3A6IDVweDtcbiAgICBmb250LXNpemU6IDEycHg7XG59XG5cbmlvbi1jYXJkLnByb2dyYW0ge1xuICAgIHdpZHRoOiA4NSU7XG4gICAgbWFyZ2luLWlubGluZTogNXB4O1xufVxuXG5pb24tY2FyZC1jb250ZW50IHtcbiAgICAtd2Via2l0LXBhZGRpbmctc3RhcnQ6IDBweDtcbiAgICBwYWRkaW5nLWlubGluZS1zdGFydDogMHB4O1xuICAgIC13ZWJraXQtcGFkZGluZy1lbmQ6IDBweDtcbiAgICBwYWRkaW5nLWlubGluZS1lbmQ6IDBweDtcbn1cblxuaW9uLWNhcmQucHJvZ3JhbS5hY3RpdmUge1xuICAgIHRyYW5zZm9ybTogc2NhbGUoMS4xLCAxLjEpO1xuICAgIHRyYW5zaXRpb246IGFsbCAwLjVzIGVhc2U7XG59XG5cbi5iYWNrX2J0bl9zZWN0aW9uIHtcbiAgICBwb3NpdGlvbjogYWJzb2x1dGU7XG4gICAgbGVmdDogMTZweDtcbiAgICB0b3A6IDE4cHg7XG5cbiAgICBpb24taWNvbiB7XG4gICAgICAgIGZvbnQtc2l6ZTogMjBweDtcbiAgICB9XG59XG5cblxuLmxhcmdlX3RleHQge1xuICAgIGZvbnQtc2l6ZTogMS4wcmVtICFpbXBvcnRhbnQ7XG59Il19 */";
      /***/
    },

    /***/
    "./src/app/pages/tabs/menu/apprentissage/apprentissage.page.ts":
    /*!*********************************************************************!*\
      !*** ./src/app/pages/tabs/menu/apprentissage/apprentissage.page.ts ***!
      \*********************************************************************/

    /*! exports provided: ApprentissagePage */

    /***/
    function srcAppPagesTabsMenuApprentissageApprentissagePageTs(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "ApprentissagePage", function () {
        return ApprentissagePage;
      });
      /* harmony import */


      var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! tslib */
      "./node_modules/tslib/tslib.es6.js");
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! @angular/core */
      "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");

      var ApprentissagePage = /*#__PURE__*/function () {
        function ApprentissagePage() {
          _classCallCheck(this, ApprentissagePage);

          this.categories = {
            slidesPerView: 1.8,
            effect: 'flip',
            slidesPerColumn: 1,
            slidesPerGroup: 1,
            watchSlidesProgress: true
          };
        }

        _createClass(ApprentissagePage, [{
          key: "ngOnInit",
          value: function ngOnInit() {}
        }]);

        return ApprentissagePage;
      }();

      ApprentissagePage.ctorParameters = function () {
        return [];
      };

      ApprentissagePage = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-apprentissage',
        template: Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(
        /*! raw-loader!./apprentissage.page.html */
        "./node_modules/raw-loader/dist/cjs.js!./src/app/pages/tabs/menu/apprentissage/apprentissage.page.html"))["default"],
        styles: [Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(
        /*! ./apprentissage.page.scss */
        "./src/app/pages/tabs/menu/apprentissage/apprentissage.page.scss"))["default"]]
      })], ApprentissagePage);
      /***/
    }
  }]);
})();
//# sourceMappingURL=pages-tabs-menu-apprentissage-apprentissage-module-es5.js.map