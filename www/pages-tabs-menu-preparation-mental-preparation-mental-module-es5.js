(function () {
  function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

  function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

  function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

  (window["webpackJsonp"] = window["webpackJsonp"] || []).push([["pages-tabs-menu-preparation-mental-preparation-mental-module"], {
    /***/
    "./node_modules/raw-loader/dist/cjs.js!./src/app/pages/tabs/menu/preparation-mental/preparation-mental.page.html":
    /*!***********************************************************************************************************************!*\
      !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/pages/tabs/menu/preparation-mental/preparation-mental.page.html ***!
      \***********************************************************************************************************************/

    /*! exports provided: default */

    /***/
    function node_modulesRawLoaderDistCjsJsSrcAppPagesTabsMenuPreparationMentalPreparationMentalPageHtml(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony default export */


      __webpack_exports__["default"] = "<ion-content>\n    <div class=\"title-box\">\n        <div class=\"title-box-img\">\n            <img src=\"assets/images/edit_physical_activity.png\">\n\n\n            <div class=\"back_btn_section\" routerLink=\"/tabs/menu\">\n                <ion-icon name=\"arrow-back-outline\" style=\"color: #fff;\"></ion-icon>\n            </div>\n            \n        </div>\n        <div class=\"title-box-text\">\n            <h4>PREPARATION MENTALE</h4>\n            <p>Pour vous aider à atteindre le niveau supérieur</p>\n        </div>\n    </div>\n    <ion-grid>\n        <ion-row>\n            <ion-col size=\"12\">\n                <div>\n                    <h4>Le corps contient l’esprit, et l’esprit renforce le corps.</h4>\n                    <p class=\"small_text\">\n                        Aucun programme d’entraînement n’a jamais été efficace chez une personne n’ayant pas la bonne mentalité pour le suivre, aller de l’avant et progresser sur le long terme. Une profonde remise en question, ou une simple piqûre de rappel pour certain(e)s,\n                        est nécessaire pour maximiser votre façon de voir les choses. Discerner le bon du mauvais et le vrai du faux est une chose ; en revanche, connaître vos motivations profondes et creuser au fond de vous peut-être la clé pour vous\n                        aider à atteindre tous vos objectifs.\n                    </p>\n                </div>\n            </ion-col>\n            <ion-col size=\"12\">\n                <div>\n                    <h4>Recadrer votre rythme de vie\n                    </h4>\n                </div>\n                <div>\n                    <iframe width=\"100%\" height=\"180\" src=\"https://www.youtube.com/embed/r2ga-iXS5i4\" frameborder=\"0\" allow=\"accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture\" allowfullscreen></iframe>\n                </div>\n\n            </ion-col>\n\n        </ion-row>\n    </ion-grid>\n</ion-content>";
      /***/
    },

    /***/
    "./src/app/pages/tabs/menu/preparation-mental/preparation-mental-routing.module.ts":
    /*!*****************************************************************************************!*\
      !*** ./src/app/pages/tabs/menu/preparation-mental/preparation-mental-routing.module.ts ***!
      \*****************************************************************************************/

    /*! exports provided: PreparationMentalPageRoutingModule */

    /***/
    function srcAppPagesTabsMenuPreparationMentalPreparationMentalRoutingModuleTs(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "PreparationMentalPageRoutingModule", function () {
        return PreparationMentalPageRoutingModule;
      });
      /* harmony import */


      var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! tslib */
      "./node_modules/tslib/tslib.es6.js");
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! @angular/core */
      "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
      /* harmony import */


      var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! @angular/router */
      "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");
      /* harmony import */


      var _preparation_mental_page__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
      /*! ./preparation-mental.page */
      "./src/app/pages/tabs/menu/preparation-mental/preparation-mental.page.ts");

      var routes = [{
        path: '',
        component: _preparation_mental_page__WEBPACK_IMPORTED_MODULE_3__["PreparationMentalPage"]
      }];

      var PreparationMentalPageRoutingModule = function PreparationMentalPageRoutingModule() {
        _classCallCheck(this, PreparationMentalPageRoutingModule);
      };

      PreparationMentalPageRoutingModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]]
      })], PreparationMentalPageRoutingModule);
      /***/
    },

    /***/
    "./src/app/pages/tabs/menu/preparation-mental/preparation-mental.module.ts":
    /*!*********************************************************************************!*\
      !*** ./src/app/pages/tabs/menu/preparation-mental/preparation-mental.module.ts ***!
      \*********************************************************************************/

    /*! exports provided: PreparationMentalPageModule */

    /***/
    function srcAppPagesTabsMenuPreparationMentalPreparationMentalModuleTs(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "PreparationMentalPageModule", function () {
        return PreparationMentalPageModule;
      });
      /* harmony import */


      var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! tslib */
      "./node_modules/tslib/tslib.es6.js");
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! @angular/core */
      "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
      /* harmony import */


      var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! @angular/common */
      "./node_modules/@angular/common/__ivy_ngcc__/fesm2015/common.js");
      /* harmony import */


      var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
      /*! @angular/forms */
      "./node_modules/@angular/forms/__ivy_ngcc__/fesm2015/forms.js");
      /* harmony import */


      var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
      /*! @ionic/angular */
      "./node_modules/@ionic/angular/__ivy_ngcc__/fesm2015/ionic-angular.js");
      /* harmony import */


      var _preparation_mental_routing_module__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(
      /*! ./preparation-mental-routing.module */
      "./src/app/pages/tabs/menu/preparation-mental/preparation-mental-routing.module.ts");
      /* harmony import */


      var _preparation_mental_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(
      /*! ./preparation-mental.page */
      "./src/app/pages/tabs/menu/preparation-mental/preparation-mental.page.ts");

      var PreparationMentalPageModule = function PreparationMentalPageModule() {
        _classCallCheck(this, PreparationMentalPageModule);
      };

      PreparationMentalPageModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"], _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"], _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonicModule"], _preparation_mental_routing_module__WEBPACK_IMPORTED_MODULE_5__["PreparationMentalPageRoutingModule"]],
        declarations: [_preparation_mental_page__WEBPACK_IMPORTED_MODULE_6__["PreparationMentalPage"]]
      })], PreparationMentalPageModule);
      /***/
    },

    /***/
    "./src/app/pages/tabs/menu/preparation-mental/preparation-mental.page.scss":
    /*!*********************************************************************************!*\
      !*** ./src/app/pages/tabs/menu/preparation-mental/preparation-mental.page.scss ***!
      \*********************************************************************************/

    /*! exports provided: default */

    /***/
    function srcAppPagesTabsMenuPreparationMentalPreparationMentalPageScss(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony default export */


      __webpack_exports__["default"] = ".title-box {\n  position: relative;\n}\n\n.title-box-text {\n  position: absolute;\n  bottom: 5px;\n  background: #0c0c0c66;\n  color: #fff;\n  width: auto;\n  padding: 10px 10px;\n}\n\n.title-box-text h4 {\n  margin: 0px;\n  font-size: 14px;\n}\n\n.title-box-text p {\n  margin: 0;\n  margin-top: 5px;\n  font-size: 12px;\n}\n\n.back_btn_section {\n  position: absolute;\n  left: 16px;\n  top: 18px;\n}\n\n.back_btn_section ion-icon {\n  font-size: 20px;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvcGFnZXMvdGFicy9tZW51L3ByZXBhcmF0aW9uLW1lbnRhbC9wcmVwYXJhdGlvbi1tZW50YWwucGFnZS5zY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBO0VBQ0ksa0JBQUE7QUFDSjs7QUFFQTtFQUNJLGtCQUFBO0VBQ0EsV0FBQTtFQUNBLHFCQUFBO0VBQ0EsV0FBQTtFQUNBLFdBQUE7RUFDQSxrQkFBQTtBQUNKOztBQUVBO0VBQ0ksV0FBQTtFQUNBLGVBQUE7QUFDSjs7QUFFQTtFQUNJLFNBQUE7RUFDQSxlQUFBO0VBQ0EsZUFBQTtBQUNKOztBQUlBO0VBQ0ksa0JBQUE7RUFDQSxVQUFBO0VBQ0EsU0FBQTtBQURKOztBQUdJO0VBQ0ksZUFBQTtBQURSIiwiZmlsZSI6InNyYy9hcHAvcGFnZXMvdGFicy9tZW51L3ByZXBhcmF0aW9uLW1lbnRhbC9wcmVwYXJhdGlvbi1tZW50YWwucGFnZS5zY3NzIiwic291cmNlc0NvbnRlbnQiOlsiLnRpdGxlLWJveCB7XG4gICAgcG9zaXRpb246IHJlbGF0aXZlO1xufVxuXG4udGl0bGUtYm94LXRleHQge1xuICAgIHBvc2l0aW9uOiBhYnNvbHV0ZTtcbiAgICBib3R0b206IDVweDtcbiAgICBiYWNrZ3JvdW5kOiAjMGMwYzBjNjY7XG4gICAgY29sb3I6ICNmZmY7XG4gICAgd2lkdGg6IGF1dG87XG4gICAgcGFkZGluZzogMTBweCAxMHB4O1xufVxuXG4udGl0bGUtYm94LXRleHQgaDQge1xuICAgIG1hcmdpbjogMHB4O1xuICAgIGZvbnQtc2l6ZTogMTRweDtcbn1cblxuLnRpdGxlLWJveC10ZXh0IHAge1xuICAgIG1hcmdpbjogMDtcbiAgICBtYXJnaW4tdG9wOiA1cHg7XG4gICAgZm9udC1zaXplOiAxMnB4O1xufVxuXG5cblxuLmJhY2tfYnRuX3NlY3Rpb24ge1xuICAgIHBvc2l0aW9uOiBhYnNvbHV0ZTtcbiAgICBsZWZ0OiAxNnB4O1xuICAgIHRvcDogMThweDtcblxuICAgIGlvbi1pY29uIHtcbiAgICAgICAgZm9udC1zaXplOiAyMHB4O1xuICAgIH1cbn0iXX0= */";
      /***/
    },

    /***/
    "./src/app/pages/tabs/menu/preparation-mental/preparation-mental.page.ts":
    /*!*******************************************************************************!*\
      !*** ./src/app/pages/tabs/menu/preparation-mental/preparation-mental.page.ts ***!
      \*******************************************************************************/

    /*! exports provided: PreparationMentalPage */

    /***/
    function srcAppPagesTabsMenuPreparationMentalPreparationMentalPageTs(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "PreparationMentalPage", function () {
        return PreparationMentalPage;
      });
      /* harmony import */


      var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! tslib */
      "./node_modules/tslib/tslib.es6.js");
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! @angular/core */
      "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");

      var PreparationMentalPage = /*#__PURE__*/function () {
        function PreparationMentalPage() {
          _classCallCheck(this, PreparationMentalPage);
        }

        _createClass(PreparationMentalPage, [{
          key: "ngOnInit",
          value: function ngOnInit() {}
        }]);

        return PreparationMentalPage;
      }();

      PreparationMentalPage.ctorParameters = function () {
        return [];
      };

      PreparationMentalPage = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-preparation-mental',
        template: Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(
        /*! raw-loader!./preparation-mental.page.html */
        "./node_modules/raw-loader/dist/cjs.js!./src/app/pages/tabs/menu/preparation-mental/preparation-mental.page.html"))["default"],
        styles: [Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(
        /*! ./preparation-mental.page.scss */
        "./src/app/pages/tabs/menu/preparation-mental/preparation-mental.page.scss"))["default"]]
      })], PreparationMentalPage);
      /***/
    }
  }]);
})();
//# sourceMappingURL=pages-tabs-menu-preparation-mental-preparation-mental-module-es5.js.map