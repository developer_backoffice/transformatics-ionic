(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["pages-tabs-cart-sports-metrial-sports-metrial-module"],{

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/pages/tabs/cart/sports-metrial/sports-metrial.page.html":
/*!***************************************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/pages/tabs/cart/sports-metrial/sports-metrial.page.html ***!
  \***************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<ion-content>\n    <div class=\"banner\">\n        <img src=\"assets/images/metrialstatus.png\">\n    </div>\n    <ion-grid>\n        <ion-row>\n            <ion-col size=12>\n                <div class=\"ebook-title\">\n                    <h4>MATERIEL SPORTIF</h4>\n                </div>\n            </ion-col>\n            <ion-col size=\"4\">\n                <div class=\"brand-logo\">\n                    <img src=\"assets/icon/brand/logo9.png\">\n                </div>\n            </ion-col>\n            <ion-col size=\"4\">\n                <div class=\"brand-logo\">\n                    <img src=\"assets/icon/brand/logo10.png\">\n                </div>\n            </ion-col>\n            <ion-col size=\"4\">\n                <div class=\"brand-logo\">\n                    <img src=\"assets/icon/brand/logo11.png\">\n                </div>\n            </ion-col>\n        </ion-row>\n    </ion-grid>\n</ion-content>");

/***/ }),

/***/ "./src/app/pages/tabs/cart/sports-metrial/sports-metrial-routing.module.ts":
/*!*********************************************************************************!*\
  !*** ./src/app/pages/tabs/cart/sports-metrial/sports-metrial-routing.module.ts ***!
  \*********************************************************************************/
/*! exports provided: SportsMetrialPageRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SportsMetrialPageRoutingModule", function() { return SportsMetrialPageRoutingModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");
/* harmony import */ var _sports_metrial_page__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./sports-metrial.page */ "./src/app/pages/tabs/cart/sports-metrial/sports-metrial.page.ts");




const routes = [
    {
        path: '',
        component: _sports_metrial_page__WEBPACK_IMPORTED_MODULE_3__["SportsMetrialPage"]
    }
];
let SportsMetrialPageRoutingModule = class SportsMetrialPageRoutingModule {
};
SportsMetrialPageRoutingModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]],
    })
], SportsMetrialPageRoutingModule);



/***/ }),

/***/ "./src/app/pages/tabs/cart/sports-metrial/sports-metrial.module.ts":
/*!*************************************************************************!*\
  !*** ./src/app/pages/tabs/cart/sports-metrial/sports-metrial.module.ts ***!
  \*************************************************************************/
/*! exports provided: SportsMetrialPageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SportsMetrialPageModule", function() { return SportsMetrialPageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/__ivy_ngcc__/fesm2015/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/__ivy_ngcc__/fesm2015/forms.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/__ivy_ngcc__/fesm2015/ionic-angular.js");
/* harmony import */ var _sports_metrial_routing_module__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./sports-metrial-routing.module */ "./src/app/pages/tabs/cart/sports-metrial/sports-metrial-routing.module.ts");
/* harmony import */ var _sports_metrial_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./sports-metrial.page */ "./src/app/pages/tabs/cart/sports-metrial/sports-metrial.page.ts");







let SportsMetrialPageModule = class SportsMetrialPageModule {
};
SportsMetrialPageModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [
            _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonicModule"],
            _sports_metrial_routing_module__WEBPACK_IMPORTED_MODULE_5__["SportsMetrialPageRoutingModule"]
        ],
        declarations: [_sports_metrial_page__WEBPACK_IMPORTED_MODULE_6__["SportsMetrialPage"]]
    })
], SportsMetrialPageModule);



/***/ }),

/***/ "./src/app/pages/tabs/cart/sports-metrial/sports-metrial.page.scss":
/*!*************************************************************************!*\
  !*** ./src/app/pages/tabs/cart/sports-metrial/sports-metrial.page.scss ***!
  \*************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("ion-content {\n  --background: #000;\n}\n\n.ebook-title h4 {\n  color: #fff;\n  text-align: center;\n  font-size: 15px;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvcGFnZXMvdGFicy9jYXJ0L3Nwb3J0cy1tZXRyaWFsL3Nwb3J0cy1tZXRyaWFsLnBhZ2Uuc2NzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQTtFQUNJLGtCQUFBO0FBQ0o7O0FBRUE7RUFDSSxXQUFBO0VBQ0Esa0JBQUE7RUFDQSxlQUFBO0FBQ0oiLCJmaWxlIjoic3JjL2FwcC9wYWdlcy90YWJzL2NhcnQvc3BvcnRzLW1ldHJpYWwvc3BvcnRzLW1ldHJpYWwucGFnZS5zY3NzIiwic291cmNlc0NvbnRlbnQiOlsiaW9uLWNvbnRlbnQge1xuICAgIC0tYmFja2dyb3VuZDogIzAwMDtcbn1cblxuLmVib29rLXRpdGxlIGg0IHtcbiAgICBjb2xvcjogI2ZmZjtcbiAgICB0ZXh0LWFsaWduOiBjZW50ZXI7XG4gICAgZm9udC1zaXplOiAxNXB4O1xufSJdfQ== */");

/***/ }),

/***/ "./src/app/pages/tabs/cart/sports-metrial/sports-metrial.page.ts":
/*!***********************************************************************!*\
  !*** ./src/app/pages/tabs/cart/sports-metrial/sports-metrial.page.ts ***!
  \***********************************************************************/
/*! exports provided: SportsMetrialPage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SportsMetrialPage", function() { return SportsMetrialPage; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");


let SportsMetrialPage = class SportsMetrialPage {
    constructor() { }
    ngOnInit() {
    }
};
SportsMetrialPage.ctorParameters = () => [];
SportsMetrialPage = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-sports-metrial',
        template: Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(/*! raw-loader!./sports-metrial.page.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/pages/tabs/cart/sports-metrial/sports-metrial.page.html")).default,
        styles: [Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(/*! ./sports-metrial.page.scss */ "./src/app/pages/tabs/cart/sports-metrial/sports-metrial.page.scss")).default]
    })
], SportsMetrialPage);



/***/ })

}]);
//# sourceMappingURL=pages-tabs-cart-sports-metrial-sports-metrial-module-es2015.js.map