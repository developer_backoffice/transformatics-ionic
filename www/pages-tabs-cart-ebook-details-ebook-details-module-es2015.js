(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["pages-tabs-cart-ebook-details-ebook-details-module"],{

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/pages/tabs/cart/ebook-details/ebook-details.page.html":
/*!*************************************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/pages/tabs/cart/ebook-details/ebook-details.page.html ***!
  \*************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<ion-content>\n    <ion-grid>\n        <ion-row>\n            <ion-col size=12>\n                <div class=\"book-details\">\n                    <div class=\"book-details-img\">\n                        <img src=\"assets/images/e-book.png\">\n                    </div>\n                    <div class=\"book-details-content\">\n                        <div class=\"book-details-title\">\n                            <h4>LA PREPARATION PHYSIQUE\n                            </h4>\n                        </div>\n                        <div class=\"book-details-catagory\">\n                            <p>\n                                E-book\n                            </p>\n                        </div>\n                        <div class=\"book-details-price\">\n                            <h4>\n                                44,90€\n                            </h4>\n                        </div>\n                        <div class=\"book-details-description\">\n                            <p>\n                                L'intégralité des connaissances nécessaires pour maximiser le développement de vos pectoraux. Entre une revue anatomique et physiologique, et plus de 10 programmes d'entraînement disponibles, vous n'aurez pas d'autre choix que de progresser.\n                            </p>\n                        </div>\n                    </div>\n                </div>\n            </ion-col>\n        </ion-row>\n    </ion-grid>\n</ion-content>\n<ion-footer>\n    <ion-toolbar>\n        <ion-grid>\n            <ion-row>\n                <ion-col>\n                    <ion-button class=\"back\" expand=\"full\" shape=\"round\">Ajouter au panier</ion-button>\n                </ion-col>\n                <ion-col>\n                    <ion-button class=\"next\" expand=\"full\" shape=\"round\">Acheté 7 fois aujourd’hui</ion-button>\n                </ion-col>\n            </ion-row>\n        </ion-grid>\n    </ion-toolbar>\n</ion-footer>");

/***/ }),

/***/ "./src/app/pages/tabs/cart/ebook-details/ebook-details-routing.module.ts":
/*!*******************************************************************************!*\
  !*** ./src/app/pages/tabs/cart/ebook-details/ebook-details-routing.module.ts ***!
  \*******************************************************************************/
/*! exports provided: EbookDetailsPageRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "EbookDetailsPageRoutingModule", function() { return EbookDetailsPageRoutingModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");
/* harmony import */ var _ebook_details_page__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./ebook-details.page */ "./src/app/pages/tabs/cart/ebook-details/ebook-details.page.ts");




const routes = [
    {
        path: '',
        component: _ebook_details_page__WEBPACK_IMPORTED_MODULE_3__["EbookDetailsPage"]
    }
];
let EbookDetailsPageRoutingModule = class EbookDetailsPageRoutingModule {
};
EbookDetailsPageRoutingModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]],
    })
], EbookDetailsPageRoutingModule);



/***/ }),

/***/ "./src/app/pages/tabs/cart/ebook-details/ebook-details.module.ts":
/*!***********************************************************************!*\
  !*** ./src/app/pages/tabs/cart/ebook-details/ebook-details.module.ts ***!
  \***********************************************************************/
/*! exports provided: EbookDetailsPageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "EbookDetailsPageModule", function() { return EbookDetailsPageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/__ivy_ngcc__/fesm2015/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/__ivy_ngcc__/fesm2015/forms.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/__ivy_ngcc__/fesm2015/ionic-angular.js");
/* harmony import */ var _ebook_details_routing_module__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./ebook-details-routing.module */ "./src/app/pages/tabs/cart/ebook-details/ebook-details-routing.module.ts");
/* harmony import */ var _ebook_details_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./ebook-details.page */ "./src/app/pages/tabs/cart/ebook-details/ebook-details.page.ts");







let EbookDetailsPageModule = class EbookDetailsPageModule {
};
EbookDetailsPageModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [
            _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonicModule"],
            _ebook_details_routing_module__WEBPACK_IMPORTED_MODULE_5__["EbookDetailsPageRoutingModule"]
        ],
        declarations: [_ebook_details_page__WEBPACK_IMPORTED_MODULE_6__["EbookDetailsPage"]]
    })
], EbookDetailsPageModule);



/***/ }),

/***/ "./src/app/pages/tabs/cart/ebook-details/ebook-details.page.scss":
/*!***********************************************************************!*\
  !*** ./src/app/pages/tabs/cart/ebook-details/ebook-details.page.scss ***!
  \***********************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = (".book-details-title h4 {\n  font-size: 14px;\n  margin: 0;\n  margin-top: 15px;\n}\n\n.book-details-catagory p {\n  margin: 0;\n  margin-top: 5px;\n  font-size: 12px;\n}\n\n.book-details-price h4 {\n  margin: 0;\n  margin-top: 5px;\n  margin-bottom: 5px;\n}\n\n.book-details-description p {\n  margin: 0;\n  font-size: 12px;\n  margin-top: 5px;\n  border-top: 1px solid #333;\n}\n\n.book-details-content {\n  text-align: center;\n}\n\n.next {\n  --background: black;\n  --color: #c1c1c1;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvcGFnZXMvdGFicy9jYXJ0L2Vib29rLWRldGFpbHMvZWJvb2stZGV0YWlscy5wYWdlLnNjc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUE7RUFDSSxlQUFBO0VBQ0EsU0FBQTtFQUNBLGdCQUFBO0FBQ0o7O0FBRUE7RUFDSSxTQUFBO0VBQ0EsZUFBQTtFQUNBLGVBQUE7QUFDSjs7QUFFQTtFQUNJLFNBQUE7RUFDQSxlQUFBO0VBQ0Esa0JBQUE7QUFDSjs7QUFFQTtFQUNJLFNBQUE7RUFDQSxlQUFBO0VBQ0EsZUFBQTtFQUNBLDBCQUFBO0FBQ0o7O0FBRUE7RUFDSSxrQkFBQTtBQUNKOztBQUVBO0VBQ0ksbUJBQUE7RUFDQSxnQkFBQTtBQUNKIiwiZmlsZSI6InNyYy9hcHAvcGFnZXMvdGFicy9jYXJ0L2Vib29rLWRldGFpbHMvZWJvb2stZGV0YWlscy5wYWdlLnNjc3MiLCJzb3VyY2VzQ29udGVudCI6WyIuYm9vay1kZXRhaWxzLXRpdGxlIGg0IHtcbiAgICBmb250LXNpemU6IDE0cHg7XG4gICAgbWFyZ2luOiAwO1xuICAgIG1hcmdpbi10b3A6IDE1cHg7XG59XG5cbi5ib29rLWRldGFpbHMtY2F0YWdvcnkgcCB7XG4gICAgbWFyZ2luOiAwO1xuICAgIG1hcmdpbi10b3A6IDVweDtcbiAgICBmb250LXNpemU6IDEycHg7XG59XG5cbi5ib29rLWRldGFpbHMtcHJpY2UgaDQge1xuICAgIG1hcmdpbjogMDtcbiAgICBtYXJnaW4tdG9wOiA1cHg7XG4gICAgbWFyZ2luLWJvdHRvbTogNXB4O1xufVxuXG4uYm9vay1kZXRhaWxzLWRlc2NyaXB0aW9uIHAge1xuICAgIG1hcmdpbjogMDtcbiAgICBmb250LXNpemU6IDEycHg7XG4gICAgbWFyZ2luLXRvcDogNXB4O1xuICAgIGJvcmRlci10b3A6IDFweCBzb2xpZCAjMzMzO1xufVxuXG4uYm9vay1kZXRhaWxzLWNvbnRlbnQge1xuICAgIHRleHQtYWxpZ246IGNlbnRlcjtcbn1cblxuLm5leHQge1xuICAgIC0tYmFja2dyb3VuZDogYmxhY2s7XG4gICAgLS1jb2xvcjogI2MxYzFjMTtcbn0iXX0= */");

/***/ }),

/***/ "./src/app/pages/tabs/cart/ebook-details/ebook-details.page.ts":
/*!*********************************************************************!*\
  !*** ./src/app/pages/tabs/cart/ebook-details/ebook-details.page.ts ***!
  \*********************************************************************/
/*! exports provided: EbookDetailsPage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "EbookDetailsPage", function() { return EbookDetailsPage; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");


let EbookDetailsPage = class EbookDetailsPage {
    constructor() { }
    ngOnInit() {
    }
};
EbookDetailsPage.ctorParameters = () => [];
EbookDetailsPage = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-ebook-details',
        template: Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(/*! raw-loader!./ebook-details.page.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/pages/tabs/cart/ebook-details/ebook-details.page.html")).default,
        styles: [Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(/*! ./ebook-details.page.scss */ "./src/app/pages/tabs/cart/ebook-details/ebook-details.page.scss")).default]
    })
], EbookDetailsPage);



/***/ })

}]);
//# sourceMappingURL=pages-tabs-cart-ebook-details-ebook-details-module-es2015.js.map