(function () {
  function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function"); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, writable: true, configurable: true } }); if (superClass) _setPrototypeOf(subClass, superClass); }

  function _setPrototypeOf(o, p) { _setPrototypeOf = Object.setPrototypeOf || function _setPrototypeOf(o, p) { o.__proto__ = p; return o; }; return _setPrototypeOf(o, p); }

  function _createSuper(Derived) { var hasNativeReflectConstruct = _isNativeReflectConstruct(); return function _createSuperInternal() { var Super = _getPrototypeOf(Derived), result; if (hasNativeReflectConstruct) { var NewTarget = _getPrototypeOf(this).constructor; result = Reflect.construct(Super, arguments, NewTarget); } else { result = Super.apply(this, arguments); } return _possibleConstructorReturn(this, result); }; }

  function _possibleConstructorReturn(self, call) { if (call && (typeof call === "object" || typeof call === "function")) { return call; } return _assertThisInitialized(self); }

  function _assertThisInitialized(self) { if (self === void 0) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return self; }

  function _isNativeReflectConstruct() { if (typeof Reflect === "undefined" || !Reflect.construct) return false; if (Reflect.construct.sham) return false; if (typeof Proxy === "function") return true; try { Date.prototype.toString.call(Reflect.construct(Date, [], function () {})); return true; } catch (e) { return false; } }

  function _getPrototypeOf(o) { _getPrototypeOf = Object.setPrototypeOf ? Object.getPrototypeOf : function _getPrototypeOf(o) { return o.__proto__ || Object.getPrototypeOf(o); }; return _getPrototypeOf(o); }

  function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

  function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

  function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

  (window["webpackJsonp"] = window["webpackJsonp"] || []).push([["main"], {
    /***/
    "./$$_lazy_route_resource lazy recursive":
    /*!******************************************************!*\
      !*** ./$$_lazy_route_resource lazy namespace object ***!
      \******************************************************/

    /*! no static exports found */

    /***/
    function $$_lazy_route_resourceLazyRecursive(module, exports, __webpack_require__) {
      var map = {
        "./pages/home/home.module": ["./src/app/pages/home/home.module.ts", "common", "pages-home-home-module"]
      };

      function webpackAsyncContext(req) {
        if (!__webpack_require__.o(map, req)) {
          return Promise.resolve().then(function () {
            var e = new Error("Cannot find module '" + req + "'");
            e.code = 'MODULE_NOT_FOUND';
            throw e;
          });
        }

        var ids = map[req],
            id = ids[0];
        return Promise.all(ids.slice(1).map(__webpack_require__.e)).then(function () {
          return __webpack_require__(id);
        });
      }

      webpackAsyncContext.keys = function webpackAsyncContextKeys() {
        return Object.keys(map);
      };

      webpackAsyncContext.id = "./$$_lazy_route_resource lazy recursive";
      module.exports = webpackAsyncContext;
      /***/
    },

    /***/
    "./node_modules/@ionic/core/dist/esm lazy recursive ^\\.\\/.*\\.entry\\.js$ include: \\.entry\\.js$ exclude: \\.system\\.entry\\.js$":
    /*!*****************************************************************************************************************************************!*\
      !*** ./node_modules/@ionic/core/dist/esm lazy ^\.\/.*\.entry\.js$ include: \.entry\.js$ exclude: \.system\.entry\.js$ namespace object ***!
      \*****************************************************************************************************************************************/

    /*! no static exports found */

    /***/
    function node_modulesIonicCoreDistEsmLazyRecursiveEntryJs$IncludeEntryJs$ExcludeSystemEntryJs$(module, exports, __webpack_require__) {
      var map = {
        "./ion-action-sheet.entry.js": ["./node_modules/@ionic/core/dist/esm/ion-action-sheet.entry.js", "common", 0],
        "./ion-alert.entry.js": ["./node_modules/@ionic/core/dist/esm/ion-alert.entry.js", "common", 1],
        "./ion-app_8.entry.js": ["./node_modules/@ionic/core/dist/esm/ion-app_8.entry.js", "common", 2],
        "./ion-avatar_3.entry.js": ["./node_modules/@ionic/core/dist/esm/ion-avatar_3.entry.js", "common", 3],
        "./ion-back-button.entry.js": ["./node_modules/@ionic/core/dist/esm/ion-back-button.entry.js", "common", 4],
        "./ion-backdrop.entry.js": ["./node_modules/@ionic/core/dist/esm/ion-backdrop.entry.js", 5],
        "./ion-button_2.entry.js": ["./node_modules/@ionic/core/dist/esm/ion-button_2.entry.js", "common", 6],
        "./ion-card_5.entry.js": ["./node_modules/@ionic/core/dist/esm/ion-card_5.entry.js", "common", 7],
        "./ion-checkbox.entry.js": ["./node_modules/@ionic/core/dist/esm/ion-checkbox.entry.js", "common", 8],
        "./ion-chip.entry.js": ["./node_modules/@ionic/core/dist/esm/ion-chip.entry.js", "common", 9],
        "./ion-col_3.entry.js": ["./node_modules/@ionic/core/dist/esm/ion-col_3.entry.js", 10],
        "./ion-datetime_3.entry.js": ["./node_modules/@ionic/core/dist/esm/ion-datetime_3.entry.js", "common", 11],
        "./ion-fab_3.entry.js": ["./node_modules/@ionic/core/dist/esm/ion-fab_3.entry.js", "common", 12],
        "./ion-img.entry.js": ["./node_modules/@ionic/core/dist/esm/ion-img.entry.js", 13],
        "./ion-infinite-scroll_2.entry.js": ["./node_modules/@ionic/core/dist/esm/ion-infinite-scroll_2.entry.js", 14],
        "./ion-input.entry.js": ["./node_modules/@ionic/core/dist/esm/ion-input.entry.js", "common", 15],
        "./ion-item-option_3.entry.js": ["./node_modules/@ionic/core/dist/esm/ion-item-option_3.entry.js", "common", 16],
        "./ion-item_8.entry.js": ["./node_modules/@ionic/core/dist/esm/ion-item_8.entry.js", "common", 17],
        "./ion-loading.entry.js": ["./node_modules/@ionic/core/dist/esm/ion-loading.entry.js", "common", 18],
        "./ion-menu_3.entry.js": ["./node_modules/@ionic/core/dist/esm/ion-menu_3.entry.js", "common", 19],
        "./ion-modal.entry.js": ["./node_modules/@ionic/core/dist/esm/ion-modal.entry.js", "common", 20],
        "./ion-nav_2.entry.js": ["./node_modules/@ionic/core/dist/esm/ion-nav_2.entry.js", "common", 21],
        "./ion-popover.entry.js": ["./node_modules/@ionic/core/dist/esm/ion-popover.entry.js", "common", 22],
        "./ion-progress-bar.entry.js": ["./node_modules/@ionic/core/dist/esm/ion-progress-bar.entry.js", "common", 23],
        "./ion-radio_2.entry.js": ["./node_modules/@ionic/core/dist/esm/ion-radio_2.entry.js", "common", 24],
        "./ion-range.entry.js": ["./node_modules/@ionic/core/dist/esm/ion-range.entry.js", "common", 25],
        "./ion-refresher_2.entry.js": ["./node_modules/@ionic/core/dist/esm/ion-refresher_2.entry.js", "common", 26],
        "./ion-reorder_2.entry.js": ["./node_modules/@ionic/core/dist/esm/ion-reorder_2.entry.js", "common", 27],
        "./ion-ripple-effect.entry.js": ["./node_modules/@ionic/core/dist/esm/ion-ripple-effect.entry.js", 28],
        "./ion-route_4.entry.js": ["./node_modules/@ionic/core/dist/esm/ion-route_4.entry.js", "common", 29],
        "./ion-searchbar.entry.js": ["./node_modules/@ionic/core/dist/esm/ion-searchbar.entry.js", "common", 30],
        "./ion-segment_2.entry.js": ["./node_modules/@ionic/core/dist/esm/ion-segment_2.entry.js", "common", 31],
        "./ion-select_3.entry.js": ["./node_modules/@ionic/core/dist/esm/ion-select_3.entry.js", "common", 32],
        "./ion-slide_2.entry.js": ["./node_modules/@ionic/core/dist/esm/ion-slide_2.entry.js", 33],
        "./ion-spinner.entry.js": ["./node_modules/@ionic/core/dist/esm/ion-spinner.entry.js", "common", 34],
        "./ion-split-pane.entry.js": ["./node_modules/@ionic/core/dist/esm/ion-split-pane.entry.js", 35],
        "./ion-tab-bar_2.entry.js": ["./node_modules/@ionic/core/dist/esm/ion-tab-bar_2.entry.js", "common", 36],
        "./ion-tab_2.entry.js": ["./node_modules/@ionic/core/dist/esm/ion-tab_2.entry.js", "common", 37],
        "./ion-text.entry.js": ["./node_modules/@ionic/core/dist/esm/ion-text.entry.js", "common", 38],
        "./ion-textarea.entry.js": ["./node_modules/@ionic/core/dist/esm/ion-textarea.entry.js", "common", 39],
        "./ion-toast.entry.js": ["./node_modules/@ionic/core/dist/esm/ion-toast.entry.js", "common", 40],
        "./ion-toggle.entry.js": ["./node_modules/@ionic/core/dist/esm/ion-toggle.entry.js", "common", 41],
        "./ion-virtual-scroll.entry.js": ["./node_modules/@ionic/core/dist/esm/ion-virtual-scroll.entry.js", 42]
      };

      function webpackAsyncContext(req) {
        if (!__webpack_require__.o(map, req)) {
          return Promise.resolve().then(function () {
            var e = new Error("Cannot find module '" + req + "'");
            e.code = 'MODULE_NOT_FOUND';
            throw e;
          });
        }

        var ids = map[req],
            id = ids[0];
        return Promise.all(ids.slice(1).map(__webpack_require__.e)).then(function () {
          return __webpack_require__(id);
        });
      }

      webpackAsyncContext.keys = function webpackAsyncContextKeys() {
        return Object.keys(map);
      };

      webpackAsyncContext.id = "./node_modules/@ionic/core/dist/esm lazy recursive ^\\.\\/.*\\.entry\\.js$ include: \\.entry\\.js$ exclude: \\.system\\.entry\\.js$";
      module.exports = webpackAsyncContext;
      /***/
    },

    /***/
    "./node_modules/moment/locale sync recursive ^\\.\\/.*$":
    /*!**************************************************!*\
      !*** ./node_modules/moment/locale sync ^\.\/.*$ ***!
      \**************************************************/

    /*! no static exports found */

    /***/
    function node_modulesMomentLocaleSyncRecursive$(module, exports, __webpack_require__) {
      var map = {
        "./af": "./node_modules/moment/locale/af.js",
        "./af.js": "./node_modules/moment/locale/af.js",
        "./ar": "./node_modules/moment/locale/ar.js",
        "./ar-dz": "./node_modules/moment/locale/ar-dz.js",
        "./ar-dz.js": "./node_modules/moment/locale/ar-dz.js",
        "./ar-kw": "./node_modules/moment/locale/ar-kw.js",
        "./ar-kw.js": "./node_modules/moment/locale/ar-kw.js",
        "./ar-ly": "./node_modules/moment/locale/ar-ly.js",
        "./ar-ly.js": "./node_modules/moment/locale/ar-ly.js",
        "./ar-ma": "./node_modules/moment/locale/ar-ma.js",
        "./ar-ma.js": "./node_modules/moment/locale/ar-ma.js",
        "./ar-sa": "./node_modules/moment/locale/ar-sa.js",
        "./ar-sa.js": "./node_modules/moment/locale/ar-sa.js",
        "./ar-tn": "./node_modules/moment/locale/ar-tn.js",
        "./ar-tn.js": "./node_modules/moment/locale/ar-tn.js",
        "./ar.js": "./node_modules/moment/locale/ar.js",
        "./az": "./node_modules/moment/locale/az.js",
        "./az.js": "./node_modules/moment/locale/az.js",
        "./be": "./node_modules/moment/locale/be.js",
        "./be.js": "./node_modules/moment/locale/be.js",
        "./bg": "./node_modules/moment/locale/bg.js",
        "./bg.js": "./node_modules/moment/locale/bg.js",
        "./bm": "./node_modules/moment/locale/bm.js",
        "./bm.js": "./node_modules/moment/locale/bm.js",
        "./bn": "./node_modules/moment/locale/bn.js",
        "./bn-bd": "./node_modules/moment/locale/bn-bd.js",
        "./bn-bd.js": "./node_modules/moment/locale/bn-bd.js",
        "./bn.js": "./node_modules/moment/locale/bn.js",
        "./bo": "./node_modules/moment/locale/bo.js",
        "./bo.js": "./node_modules/moment/locale/bo.js",
        "./br": "./node_modules/moment/locale/br.js",
        "./br.js": "./node_modules/moment/locale/br.js",
        "./bs": "./node_modules/moment/locale/bs.js",
        "./bs.js": "./node_modules/moment/locale/bs.js",
        "./ca": "./node_modules/moment/locale/ca.js",
        "./ca.js": "./node_modules/moment/locale/ca.js",
        "./cs": "./node_modules/moment/locale/cs.js",
        "./cs.js": "./node_modules/moment/locale/cs.js",
        "./cv": "./node_modules/moment/locale/cv.js",
        "./cv.js": "./node_modules/moment/locale/cv.js",
        "./cy": "./node_modules/moment/locale/cy.js",
        "./cy.js": "./node_modules/moment/locale/cy.js",
        "./da": "./node_modules/moment/locale/da.js",
        "./da.js": "./node_modules/moment/locale/da.js",
        "./de": "./node_modules/moment/locale/de.js",
        "./de-at": "./node_modules/moment/locale/de-at.js",
        "./de-at.js": "./node_modules/moment/locale/de-at.js",
        "./de-ch": "./node_modules/moment/locale/de-ch.js",
        "./de-ch.js": "./node_modules/moment/locale/de-ch.js",
        "./de.js": "./node_modules/moment/locale/de.js",
        "./dv": "./node_modules/moment/locale/dv.js",
        "./dv.js": "./node_modules/moment/locale/dv.js",
        "./el": "./node_modules/moment/locale/el.js",
        "./el.js": "./node_modules/moment/locale/el.js",
        "./en-au": "./node_modules/moment/locale/en-au.js",
        "./en-au.js": "./node_modules/moment/locale/en-au.js",
        "./en-ca": "./node_modules/moment/locale/en-ca.js",
        "./en-ca.js": "./node_modules/moment/locale/en-ca.js",
        "./en-gb": "./node_modules/moment/locale/en-gb.js",
        "./en-gb.js": "./node_modules/moment/locale/en-gb.js",
        "./en-ie": "./node_modules/moment/locale/en-ie.js",
        "./en-ie.js": "./node_modules/moment/locale/en-ie.js",
        "./en-il": "./node_modules/moment/locale/en-il.js",
        "./en-il.js": "./node_modules/moment/locale/en-il.js",
        "./en-in": "./node_modules/moment/locale/en-in.js",
        "./en-in.js": "./node_modules/moment/locale/en-in.js",
        "./en-nz": "./node_modules/moment/locale/en-nz.js",
        "./en-nz.js": "./node_modules/moment/locale/en-nz.js",
        "./en-sg": "./node_modules/moment/locale/en-sg.js",
        "./en-sg.js": "./node_modules/moment/locale/en-sg.js",
        "./eo": "./node_modules/moment/locale/eo.js",
        "./eo.js": "./node_modules/moment/locale/eo.js",
        "./es": "./node_modules/moment/locale/es.js",
        "./es-do": "./node_modules/moment/locale/es-do.js",
        "./es-do.js": "./node_modules/moment/locale/es-do.js",
        "./es-mx": "./node_modules/moment/locale/es-mx.js",
        "./es-mx.js": "./node_modules/moment/locale/es-mx.js",
        "./es-us": "./node_modules/moment/locale/es-us.js",
        "./es-us.js": "./node_modules/moment/locale/es-us.js",
        "./es.js": "./node_modules/moment/locale/es.js",
        "./et": "./node_modules/moment/locale/et.js",
        "./et.js": "./node_modules/moment/locale/et.js",
        "./eu": "./node_modules/moment/locale/eu.js",
        "./eu.js": "./node_modules/moment/locale/eu.js",
        "./fa": "./node_modules/moment/locale/fa.js",
        "./fa.js": "./node_modules/moment/locale/fa.js",
        "./fi": "./node_modules/moment/locale/fi.js",
        "./fi.js": "./node_modules/moment/locale/fi.js",
        "./fil": "./node_modules/moment/locale/fil.js",
        "./fil.js": "./node_modules/moment/locale/fil.js",
        "./fo": "./node_modules/moment/locale/fo.js",
        "./fo.js": "./node_modules/moment/locale/fo.js",
        "./fr": "./node_modules/moment/locale/fr.js",
        "./fr-ca": "./node_modules/moment/locale/fr-ca.js",
        "./fr-ca.js": "./node_modules/moment/locale/fr-ca.js",
        "./fr-ch": "./node_modules/moment/locale/fr-ch.js",
        "./fr-ch.js": "./node_modules/moment/locale/fr-ch.js",
        "./fr.js": "./node_modules/moment/locale/fr.js",
        "./fy": "./node_modules/moment/locale/fy.js",
        "./fy.js": "./node_modules/moment/locale/fy.js",
        "./ga": "./node_modules/moment/locale/ga.js",
        "./ga.js": "./node_modules/moment/locale/ga.js",
        "./gd": "./node_modules/moment/locale/gd.js",
        "./gd.js": "./node_modules/moment/locale/gd.js",
        "./gl": "./node_modules/moment/locale/gl.js",
        "./gl.js": "./node_modules/moment/locale/gl.js",
        "./gom-deva": "./node_modules/moment/locale/gom-deva.js",
        "./gom-deva.js": "./node_modules/moment/locale/gom-deva.js",
        "./gom-latn": "./node_modules/moment/locale/gom-latn.js",
        "./gom-latn.js": "./node_modules/moment/locale/gom-latn.js",
        "./gu": "./node_modules/moment/locale/gu.js",
        "./gu.js": "./node_modules/moment/locale/gu.js",
        "./he": "./node_modules/moment/locale/he.js",
        "./he.js": "./node_modules/moment/locale/he.js",
        "./hi": "./node_modules/moment/locale/hi.js",
        "./hi.js": "./node_modules/moment/locale/hi.js",
        "./hr": "./node_modules/moment/locale/hr.js",
        "./hr.js": "./node_modules/moment/locale/hr.js",
        "./hu": "./node_modules/moment/locale/hu.js",
        "./hu.js": "./node_modules/moment/locale/hu.js",
        "./hy-am": "./node_modules/moment/locale/hy-am.js",
        "./hy-am.js": "./node_modules/moment/locale/hy-am.js",
        "./id": "./node_modules/moment/locale/id.js",
        "./id.js": "./node_modules/moment/locale/id.js",
        "./is": "./node_modules/moment/locale/is.js",
        "./is.js": "./node_modules/moment/locale/is.js",
        "./it": "./node_modules/moment/locale/it.js",
        "./it-ch": "./node_modules/moment/locale/it-ch.js",
        "./it-ch.js": "./node_modules/moment/locale/it-ch.js",
        "./it.js": "./node_modules/moment/locale/it.js",
        "./ja": "./node_modules/moment/locale/ja.js",
        "./ja.js": "./node_modules/moment/locale/ja.js",
        "./jv": "./node_modules/moment/locale/jv.js",
        "./jv.js": "./node_modules/moment/locale/jv.js",
        "./ka": "./node_modules/moment/locale/ka.js",
        "./ka.js": "./node_modules/moment/locale/ka.js",
        "./kk": "./node_modules/moment/locale/kk.js",
        "./kk.js": "./node_modules/moment/locale/kk.js",
        "./km": "./node_modules/moment/locale/km.js",
        "./km.js": "./node_modules/moment/locale/km.js",
        "./kn": "./node_modules/moment/locale/kn.js",
        "./kn.js": "./node_modules/moment/locale/kn.js",
        "./ko": "./node_modules/moment/locale/ko.js",
        "./ko.js": "./node_modules/moment/locale/ko.js",
        "./ku": "./node_modules/moment/locale/ku.js",
        "./ku.js": "./node_modules/moment/locale/ku.js",
        "./ky": "./node_modules/moment/locale/ky.js",
        "./ky.js": "./node_modules/moment/locale/ky.js",
        "./lb": "./node_modules/moment/locale/lb.js",
        "./lb.js": "./node_modules/moment/locale/lb.js",
        "./lo": "./node_modules/moment/locale/lo.js",
        "./lo.js": "./node_modules/moment/locale/lo.js",
        "./lt": "./node_modules/moment/locale/lt.js",
        "./lt.js": "./node_modules/moment/locale/lt.js",
        "./lv": "./node_modules/moment/locale/lv.js",
        "./lv.js": "./node_modules/moment/locale/lv.js",
        "./me": "./node_modules/moment/locale/me.js",
        "./me.js": "./node_modules/moment/locale/me.js",
        "./mi": "./node_modules/moment/locale/mi.js",
        "./mi.js": "./node_modules/moment/locale/mi.js",
        "./mk": "./node_modules/moment/locale/mk.js",
        "./mk.js": "./node_modules/moment/locale/mk.js",
        "./ml": "./node_modules/moment/locale/ml.js",
        "./ml.js": "./node_modules/moment/locale/ml.js",
        "./mn": "./node_modules/moment/locale/mn.js",
        "./mn.js": "./node_modules/moment/locale/mn.js",
        "./mr": "./node_modules/moment/locale/mr.js",
        "./mr.js": "./node_modules/moment/locale/mr.js",
        "./ms": "./node_modules/moment/locale/ms.js",
        "./ms-my": "./node_modules/moment/locale/ms-my.js",
        "./ms-my.js": "./node_modules/moment/locale/ms-my.js",
        "./ms.js": "./node_modules/moment/locale/ms.js",
        "./mt": "./node_modules/moment/locale/mt.js",
        "./mt.js": "./node_modules/moment/locale/mt.js",
        "./my": "./node_modules/moment/locale/my.js",
        "./my.js": "./node_modules/moment/locale/my.js",
        "./nb": "./node_modules/moment/locale/nb.js",
        "./nb.js": "./node_modules/moment/locale/nb.js",
        "./ne": "./node_modules/moment/locale/ne.js",
        "./ne.js": "./node_modules/moment/locale/ne.js",
        "./nl": "./node_modules/moment/locale/nl.js",
        "./nl-be": "./node_modules/moment/locale/nl-be.js",
        "./nl-be.js": "./node_modules/moment/locale/nl-be.js",
        "./nl.js": "./node_modules/moment/locale/nl.js",
        "./nn": "./node_modules/moment/locale/nn.js",
        "./nn.js": "./node_modules/moment/locale/nn.js",
        "./oc-lnc": "./node_modules/moment/locale/oc-lnc.js",
        "./oc-lnc.js": "./node_modules/moment/locale/oc-lnc.js",
        "./pa-in": "./node_modules/moment/locale/pa-in.js",
        "./pa-in.js": "./node_modules/moment/locale/pa-in.js",
        "./pl": "./node_modules/moment/locale/pl.js",
        "./pl.js": "./node_modules/moment/locale/pl.js",
        "./pt": "./node_modules/moment/locale/pt.js",
        "./pt-br": "./node_modules/moment/locale/pt-br.js",
        "./pt-br.js": "./node_modules/moment/locale/pt-br.js",
        "./pt.js": "./node_modules/moment/locale/pt.js",
        "./ro": "./node_modules/moment/locale/ro.js",
        "./ro.js": "./node_modules/moment/locale/ro.js",
        "./ru": "./node_modules/moment/locale/ru.js",
        "./ru.js": "./node_modules/moment/locale/ru.js",
        "./sd": "./node_modules/moment/locale/sd.js",
        "./sd.js": "./node_modules/moment/locale/sd.js",
        "./se": "./node_modules/moment/locale/se.js",
        "./se.js": "./node_modules/moment/locale/se.js",
        "./si": "./node_modules/moment/locale/si.js",
        "./si.js": "./node_modules/moment/locale/si.js",
        "./sk": "./node_modules/moment/locale/sk.js",
        "./sk.js": "./node_modules/moment/locale/sk.js",
        "./sl": "./node_modules/moment/locale/sl.js",
        "./sl.js": "./node_modules/moment/locale/sl.js",
        "./sq": "./node_modules/moment/locale/sq.js",
        "./sq.js": "./node_modules/moment/locale/sq.js",
        "./sr": "./node_modules/moment/locale/sr.js",
        "./sr-cyrl": "./node_modules/moment/locale/sr-cyrl.js",
        "./sr-cyrl.js": "./node_modules/moment/locale/sr-cyrl.js",
        "./sr.js": "./node_modules/moment/locale/sr.js",
        "./ss": "./node_modules/moment/locale/ss.js",
        "./ss.js": "./node_modules/moment/locale/ss.js",
        "./sv": "./node_modules/moment/locale/sv.js",
        "./sv.js": "./node_modules/moment/locale/sv.js",
        "./sw": "./node_modules/moment/locale/sw.js",
        "./sw.js": "./node_modules/moment/locale/sw.js",
        "./ta": "./node_modules/moment/locale/ta.js",
        "./ta.js": "./node_modules/moment/locale/ta.js",
        "./te": "./node_modules/moment/locale/te.js",
        "./te.js": "./node_modules/moment/locale/te.js",
        "./tet": "./node_modules/moment/locale/tet.js",
        "./tet.js": "./node_modules/moment/locale/tet.js",
        "./tg": "./node_modules/moment/locale/tg.js",
        "./tg.js": "./node_modules/moment/locale/tg.js",
        "./th": "./node_modules/moment/locale/th.js",
        "./th.js": "./node_modules/moment/locale/th.js",
        "./tk": "./node_modules/moment/locale/tk.js",
        "./tk.js": "./node_modules/moment/locale/tk.js",
        "./tl-ph": "./node_modules/moment/locale/tl-ph.js",
        "./tl-ph.js": "./node_modules/moment/locale/tl-ph.js",
        "./tlh": "./node_modules/moment/locale/tlh.js",
        "./tlh.js": "./node_modules/moment/locale/tlh.js",
        "./tr": "./node_modules/moment/locale/tr.js",
        "./tr.js": "./node_modules/moment/locale/tr.js",
        "./tzl": "./node_modules/moment/locale/tzl.js",
        "./tzl.js": "./node_modules/moment/locale/tzl.js",
        "./tzm": "./node_modules/moment/locale/tzm.js",
        "./tzm-latn": "./node_modules/moment/locale/tzm-latn.js",
        "./tzm-latn.js": "./node_modules/moment/locale/tzm-latn.js",
        "./tzm.js": "./node_modules/moment/locale/tzm.js",
        "./ug-cn": "./node_modules/moment/locale/ug-cn.js",
        "./ug-cn.js": "./node_modules/moment/locale/ug-cn.js",
        "./uk": "./node_modules/moment/locale/uk.js",
        "./uk.js": "./node_modules/moment/locale/uk.js",
        "./ur": "./node_modules/moment/locale/ur.js",
        "./ur.js": "./node_modules/moment/locale/ur.js",
        "./uz": "./node_modules/moment/locale/uz.js",
        "./uz-latn": "./node_modules/moment/locale/uz-latn.js",
        "./uz-latn.js": "./node_modules/moment/locale/uz-latn.js",
        "./uz.js": "./node_modules/moment/locale/uz.js",
        "./vi": "./node_modules/moment/locale/vi.js",
        "./vi.js": "./node_modules/moment/locale/vi.js",
        "./x-pseudo": "./node_modules/moment/locale/x-pseudo.js",
        "./x-pseudo.js": "./node_modules/moment/locale/x-pseudo.js",
        "./yo": "./node_modules/moment/locale/yo.js",
        "./yo.js": "./node_modules/moment/locale/yo.js",
        "./zh-cn": "./node_modules/moment/locale/zh-cn.js",
        "./zh-cn.js": "./node_modules/moment/locale/zh-cn.js",
        "./zh-hk": "./node_modules/moment/locale/zh-hk.js",
        "./zh-hk.js": "./node_modules/moment/locale/zh-hk.js",
        "./zh-mo": "./node_modules/moment/locale/zh-mo.js",
        "./zh-mo.js": "./node_modules/moment/locale/zh-mo.js",
        "./zh-tw": "./node_modules/moment/locale/zh-tw.js",
        "./zh-tw.js": "./node_modules/moment/locale/zh-tw.js"
      };

      function webpackContext(req) {
        var id = webpackContextResolve(req);
        return __webpack_require__(id);
      }

      function webpackContextResolve(req) {
        if (!__webpack_require__.o(map, req)) {
          var e = new Error("Cannot find module '" + req + "'");
          e.code = 'MODULE_NOT_FOUND';
          throw e;
        }

        return map[req];
      }

      webpackContext.keys = function webpackContextKeys() {
        return Object.keys(map);
      };

      webpackContext.resolve = webpackContextResolve;
      module.exports = webpackContext;
      webpackContext.id = "./node_modules/moment/locale sync recursive ^\\.\\/.*$";
      /***/
    },

    /***/
    "./node_modules/raw-loader/dist/cjs.js!./src/app/app.component.html":
    /*!**************************************************************************!*\
      !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/app.component.html ***!
      \**************************************************************************/

    /*! exports provided: default */

    /***/
    function node_modulesRawLoaderDistCjsJsSrcAppAppComponentHtml(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony default export */


      __webpack_exports__["default"] = "<ion-app>\n  <ion-router-outlet></ion-router-outlet>\n</ion-app>\n";
      /***/
    },

    /***/
    "./node_modules/raw-loader/dist/cjs.js!./src/app/components/body-scan-step-five/body-scan-step-five.component.html":
    /*!*************************************************************************************************************************!*\
      !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/components/body-scan-step-five/body-scan-step-five.component.html ***!
      \*************************************************************************************************************************/

    /*! exports provided: default */

    /***/
    function node_modulesRawLoaderDistCjsJsSrcAppComponentsBodyScanStepFiveBodyScanStepFiveComponentHtml(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony default export */


      __webpack_exports__["default"] = "<ion-grid>\n  <ion-row>\n    <ion-col size=\"12\">\n      <h3>PHYSIOLOGIE</h3>\n      <p>\n        <strong>Morphotype</strong> : laquelle des photos suivantes correspond\n        le plus à votre morphologie ? Jugez l’aspect global de votre corps dans\n        ses dimensions plutôt que sur la masse grasse.\n      </p>\n    </ion-col>\n    <ion-col size=\"12\">\n      <ion-slides pager=\"false\" [options]=\"categories\" #slides>\n        <ion-slide *ngFor=\"let src of morphotypeArray\">\n          <ion-card\n          [class]=\"src.activeClass ? 'program active': 'program' \"\n            (click)=\"donsomething($event, src)\"\n          >\n            <ion-card-content>\n              <img [src]=\"src.src\" class=\"slide-img\" />\n            </ion-card-content>\n          </ion-card>\n        </ion-slide>\n      </ion-slides>\n    </ion-col>\n  </ion-row>\n  <ion-row>\n    <ion-col size=\"12\">\n      <p>\n        <strong>Taux de masse grasse</strong> : laquelle des photos suivantes\n        correspond le plus à votre taux de masse grasse actuel ? Essayez d’être\n        le plus objectif possible.\n      </p>\n    </ion-col>\n    <ion-col size=\"12\">\n      <ion-slides class=\"body-fat\" [options]=\"categories\" pager=\"true\" class=\"body-scan-slide2\">\n        <ion-slide *ngFor=\"let item of fatMassRateArray\">\n          \n          <ion-card [class]=\"item.activeClass ? 'program active': 'program' \" (click)=\"fatMassRateArraySelect(item)\">\n            <ion-card-content>\n              <img [src]=\"item.src\" />\n            </ion-card-content>\n          </ion-card>\n        </ion-slide>\n      </ion-slides>\n    </ion-col>\n  </ion-row>\n</ion-grid>\n";
      /***/
    },

    /***/
    "./node_modules/raw-loader/dist/cjs.js!./src/app/components/body-scan-step-four/body-scan-step-four.component.html":
    /*!*************************************************************************************************************************!*\
      !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/components/body-scan-step-four/body-scan-step-four.component.html ***!
      \*************************************************************************************************************************/

    /*! exports provided: default */

    /***/
    function node_modulesRawLoaderDistCjsJsSrcAppComponentsBodyScanStepFourBodyScanStepFourComponentHtml(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony default export */


      __webpack_exports__["default"] = "<ion-grid>\n    <form [formGroup]=\"stepFourForm\">\n        <ion-row>\n            <ion-col size=\"12\">\n                <h3>ANTÉCÉDENTS MÉDICAUX</h3>\n                <p>Avez-vous des antécédents médicaux ?</p>\n            </ion-col>\n            <ion-col size=\"12\">\n                <div>\n                    <div float-left class=\"my-checkbox \">\n                        <ion-label text-uppercase> OUI</ion-label>\n                        <ion-checkbox value=\"yes\" [checked]=\"showMeds\" (ionChange)=\"showMedicines($event)\">\n                        </ion-checkbox>\n                    </div>\n                    <div float-right class=\"my-checkbox\">\n                        <ion-label text-uppercase>NON</ion-label>\n                        <ion-checkbox value=\"no\" [checked]=\"!showMeds\" (ionChange)=\"hideMedicines($event)\">\n                        </ion-checkbox>\n                    </div>\n                </div>\n            </ion-col>\n            <ion-col size=\"12\">\n                <ion-item *ngIf=\"showMeds\">\n                    <ion-label>Sélectionnez vos antécédents</ion-label>\n                    <ion-select interface=\"action-sheet\" class=\"custom-options\" multiple formControlName=\"field2\"\n                        (ionChange)=\"onMedicineSelection($event)\">\n                        <ion-select-option value=\"allergies\">Allergies</ion-select-option>\n                        <ion-select-option value=\"cataracte\">Cataracte</ion-select-option>\n                        <ion-select-option value=\"dmla\">DMLA</ion-select-option>\n                        <ion-select-option value=\"diabète\">Diabète</ion-select-option>\n\n                        <ion-select-option value=\"Fatigue chronique\">Fatigue chronique</ion-select-option>\n                        <ion-select-option value=\"Hépatites\">Hépatites</ion-select-option>\n                        <ion-select-option value=\"Les troubles du comportement alimentaire\">Les troubles du comportement\n                            alimentaire</ion-select-option>\n                        <ion-select-option value=\"Malaria\">Malaria</ion-select-option>\n\n                        <ion-select-option value=\"Méningite\">Méningite</ion-select-option>\n                        <ion-select-option value=\"Obésité\">Obésité</ion-select-option>\n                        <ion-select-option value=\"Paludisme\">Paludisme</ion-select-option>\n                        <ion-select-option value=\"Phlébite\">Phlébite</ion-select-option>\n\n\n                        <ion-select-option value=\"Saturnisme\">Saturnisme</ion-select-option>\n                        <ion-select-option value=\"Thyroïde\">Thyroïde</ion-select-option>\n                        <ion-select-option value=\"Arthrose\">Arthrose</ion-select-option>\n                        <ion-select-option value=\"Lombalgie\">Lombalgie</ion-select-option>\n\n\n                        <ion-select-option value=\"Ostéoporose\">Ostéoporose</ion-select-option>\n                        <ion-select-option value=\"Polyarthrite rhumatoïde\">Polyarthrite rhumatoïde</ion-select-option>\n                        <ion-select-option value=\"Rhumatismes\">Rhumatismes</ion-select-option>\n                        <ion-select-option value=\"Spondylarthropathies\">Spondylarthropathies</ion-select-option>\n\n                        <ion-select-option value=\"Sciatique\">Sciatique</ion-select-option>\n                        <ion-select-option value=\"Problèmes intestinaux\">Problèmes intestinaux</ion-select-option>\n                        <ion-select-option value=\"Reflux gastro-oesophagien\">Reflux gastro-oesophagien\n                        </ion-select-option>\n                        <ion-select-option value=\"Les brûlures d'estomac\">Les brûlures d'estomac</ion-select-option>\n\n                        <ion-select-option value=\"Les maladies chroniques de l'intestin\">Les maladies chroniques de\n                            l'intestin</ion-select-option>\n                        <ion-select-option value=\"Asthme\">Asthme</ion-select-option>\n                        <ion-select-option value=\"Bronchiolite\">Bronchiolite</ion-select-option>\n                        <ion-select-option value=\"Bronchite chronique et BPCO\">Bronchite chronique et BPCO\n                        </ion-select-option>\n\n                        <ion-select-option value=\"Légionellose\">Légionellose</ion-select-option>\n                        <ion-select-option value=\"HyperCholestérolémie\">HyperCholestérolémie</ion-select-option>\n                        <ion-select-option value=\"Hypertension\">Hypertension</ion-select-option>\n                        <ion-select-option value=\"Infarctus\">Infarctus</ion-select-option>\n\n                        <ion-select-option value=\"Maladie veineuse\">Maladie veineuse</ion-select-option>\n                        <ion-select-option value=\"Maladies cardiovasculaires\">Maladies cardiovasculaires\n                        </ion-select-option>\n                        <ion-select-option value=\"Cancer\">Cancer</ion-select-option>\n                        <ion-select-option value=\"Alzheimer\">Alzheimer</ion-select-option>\n\n\n                        <ion-select-option value=\"Attaques cérébrales\">Attaques cérébrales</ion-select-option>\n                        <ion-select-option value=\"Autisme\">Autisme</ion-select-option>\n                        <ion-select-option value=\"Migraine\">Migraine</ion-select-option>\n                        <ion-select-option value=\"Parkinson\">Parkinson</ion-select-option>\n\n\n                        <ion-select-option value=\"Sclérose en plaques\">Sclérose en plaques</ion-select-option>\n                        <ion-select-option value=\"Dépression\">Dépression</ion-select-option>\n                        <ion-select-option value=\"Sclérose Latérale Amyotrophique\">Sclérose Latérale Amyotrophique\n                        </ion-select-option>\n                        <ion-select-option value=\"Syncopes\">Syncopes</ion-select-option>\n\n                        <ion-select-option value=\"Traumatismes crâniens\">Traumatismes crâniens</ion-select-option>\n\n                    </ion-select>\n                </ion-item>\n            </ion-col>\n            <ion-col size=\"12\">\n                <p>Avez-vous des intolérances alimentaires ?</p>\n            </ion-col>\n            <ion-col size=\"12\">\n                <div>\n                    <div float-left class=\"my-checkbox \">\n                        <ion-label text-uppercase> OUI</ion-label>\n                        <ion-checkbox value=\"yes\" [checked]=\"showIntolerance\" (ionChange)=\"showIntolenrances($event)\">\n                        </ion-checkbox>\n                    </div>\n                    <div float-right class=\"my-checkbox\">\n                        <ion-label text-uppercase>NON</ion-label>\n                        <ion-checkbox value=\"no\" [checked]=\"!showIntolerance\" (ionChange)=\"hideIntolenrances($event)\">\n                        </ion-checkbox>\n                    </div>\n                </div>\n            </ion-col>\n            <ion-col size=\"12\">\n                <ion-item *ngIf=\"showIntolerance\">\n                    <ion-label>Sélectionnez vos antécédents</ion-label>\n                    <ion-select interface=\"action-sheet\" class=\"custom-options\" multiple formControlName=\"field3\"\n                        (ionChange)=\"onIntolerenceSelection($event)\">\n                        <ion-select-option value=\"Aux arachides\">Aux arachides</ion-select-option>\n                        <ion-select-option value=\"Au blé\">Au blé</ion-select-option>\n                        <ion-select-option value=\"Aux fruits de mer (poisson, crustacés et mollusques)\">Aux fruits de\n                            mer (poisson, crustacés et mollusques)</ion-select-option>\n                        <ion-select-option value=\"Aux fruits à coque\">Aux fruits à coque</ion-select-option>\n\n\n                        <ion-select-option value=\"Aux graines de sésame\">Aux graines de sésame</ion-select-option>\n                        <ion-select-option value=\"Au lactose\">Au lactose</ion-select-option>\n                        <ion-select-option value=\"Aux œufs\">Aux œufs</ion-select-option>\n                        <ion-select-option value=\"Au soja\">Au soja</ion-select-option>\n\n                        <ion-select-option value=\"Aux arachides\">Aux arachides</ion-select-option>\n                        <ion-select-option value=\"Aux sulfites\">Aux sulfites</ion-select-option>\n                        <ion-select-option value=\"A la moutarde\">A la moutarde</ion-select-option>\n                        <ion-select-option value=\"Aux fruits à coque\">Aux fruits à coque</ion-select-option>\n                    </ion-select>\n                </ion-item>\n            </ion-col>\n        </ion-row>\n    </form>\n</ion-grid>";
      /***/
    },

    /***/
    "./node_modules/raw-loader/dist/cjs.js!./src/app/components/body-scan-step-one/body-scan-step-one.component.html":
    /*!***********************************************************************************************************************!*\
      !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/components/body-scan-step-one/body-scan-step-one.component.html ***!
      \***********************************************************************************************************************/

    /*! exports provided: default */

    /***/
    function node_modulesRawLoaderDistCjsJsSrcAppComponentsBodyScanStepOneBodyScanStepOneComponentHtml(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony default export */


      __webpack_exports__["default"] = "<ion-grid>\n    <form [formGroup]=\"stepOneForm\">\n        <ion-row>\n            <ion-col>\n                <h3 class=\"steper-heading\"> INFORMATIONS GÉNÉRALES ET DONNÉES PHYSIOLOGIQUES</h3>\n            </ion-col>\n        </ion-row>\n        <ion-row>\n            <ion-col size=\"12\">\n                <p class=\"steper-title\">Vous êtes</p>\n            </ion-col>\n            <ion-col>\n                <div class=\"gender\">\n                    <ion-button [color]=\"gender == 'male' ? 'primary' : 'light'\" expand=\"full\"\n                        (click)=\"changeGender('male')\">\n                        Un\n                        homme\n                    </ion-button>\n                </div>\n            </ion-col>\n            <ion-col>\n                <div class=\"gender\">\n                    <ion-button [color]=\"gender == 'female' ? 'primary' : 'light'\" color=\"light\" expand=\"full\"\n                        (click)=\"changeGender('female')\">Une femme\n                    </ion-button>\n                </div>\n            </ion-col>\n            <ion-col size=\"12\">\n                <p class=\"steper-title dateDeNa\">Date de naissance\n                </p>\n                <div>\n                    <ion-list>\n                        <ion-item style=\"--padding-start: 0px !important; --border-width: 0 0 1px 0;\">\n                            <ion-datetime displayFormat=\"MMMM YY\" min=\"1989-06-04\" max=\"2004-08-23\"\n                                value=\"1994-12-15T13:47:20.789\" formControlName=\"birthday_date\"\n                                (ionChange)=\"changeDOB($event)\"></ion-datetime>\n                            <ion-icon class=\"calendar-icon\" name=\"calendar-outline\" item-right></ion-icon>\n                        </ion-item>\n                    </ion-list>\n                </div>\n            </ion-col>\n            <ion-col size=\"12\">\n                <ion-list>\n                    <ion-list-header style=\"padding-inline-start: calc(var(--ion-safe-area-left, 0px) + 0px);\">\n                        <ion-label class=\"steper-title\">Poids <small>en kilogrammes (kg)</small></ion-label>\n                    </ion-list-header>\n                    <ion-item style=\"--padding-start: 0px !important;\">\n                        <ion-range [min]=\"weightMinRange\" max=\"200\" (ionChange)=\"weightChange($event)\">\n                            <ion-label slot=\"start\">{{weightMinRange}}</ion-label>\n                            <ion-label slot=\"end\">{{weightChangeRange}}</ion-label>\n                        </ion-range>\n                    </ion-item>\n                </ion-list>\n                <ion-list>\n                    <ion-list-header style=\"padding-inline-start: calc(var(--ion-safe-area-left, 0px) + 0px);\">\n                        <ion-label class=\"steper-title\">Taille<small> en centimètres (cm)</small></ion-label>\n                    </ion-list-header>\n                    <ion-item style=\"--padding-start: 0px !important;\">\n                        <ion-range [min]=\"heightMinRange\" max=\"250\" (ionChange)=\"heightChange($event)\">\n                            <ion-label slot=\"start\">{{heightMinRange}}</ion-label>\n                            <ion-label slot=\"end\">{{heightChangeRange}}</ion-label>\n                        </ion-range>\n                    </ion-item>\n                </ion-list>\n            </ion-col>\n        </ion-row>\n        <ion-row>\n            <ion-col size=\"12\">\n                <p class=\"steper-title activitePhysiqueExterne\">\n                    <strong>Activité physique externe :</strong> remplissez le tableau suivant en fonction de vos\n                    activités\n                    journalières, sur les jours où vous vous entraînez et où vous êtes en repos, par le nombre d’heures\n                    journalier pour chaque activité\n                    :\n                </p>\n            </ion-col>\n\n            <ion-col size=\"12\">\n\n                <div class=\"segment-box\">\n                    <ion-segment value=\"favorites\" scrollable=\"true\" mode=\"ios\" [(ngModel)]=\"segmentModel\"\n                        (ionChange)=\"segmentChanged($event)\">\n\n\n                        <ion-segment-button value=\"favorites\">\n                            <ion-label>JOURS D’ENTRAINEMENT</ion-label>\n                        </ion-segment-button>\n\n\n                        <ion-segment-button value=\"profile\">\n                            <ion-label> JOURS DE REPOS</ion-label>\n                        </ion-segment-button>\n\n\n                    </ion-segment>\n                </div>\n\n\n                <ion-card class=\"mi-0\" *ngIf=\"segmentModel === 'favorites'\">\n                    <ion-card-content class=\"\">\n\n                        <ion-item class=\"item ios in-list ion-focusable hydrated item-label\"\n                            *ngFor=\"let item of training_days\">\n                            <div class=\"input-price\" slot=\"end\">\n                                <ion-button class=\"p-0 remove\" size=\"small\" fill=\"clear\" (click)=\"decrease(item.id)\">\n                                    <ion-icon name=\"remove-outline\"></ion-icon>\n                                </ion-button>\n                                <div class=\"display-price\">\n                                    {{ item.quantity }} h\n                                </div>\n                                <ion-button class=\"p-0 add\" size=\"small\" fill=\"clear\" (click)=\"increase(item.id)\">\n                                    <ion-icon name=\"add-outline\"></ion-icon>\n                                </ion-button>\n                            </div>\n                            <ion-label class=\"sc-ion-label-ios-h sc-ion-label-ios-s ios hydrated\">\n                                {{ item.name }}\n                            </ion-label>\n                        </ion-item>\n\n                        <ion-item class=\"item ios in-list ion-focusable hydrated item-label \">\n                            <div class=\"total\" slot=\"end\">\n                                <p>\n                                    TOTAL/24 : {{totalTrainingHours}} heures\n                                </p>\n                            </div>\n                        </ion-item>\n\n                    </ion-card-content>\n                </ion-card>\n\n                <ion-card class=\"mi-0\" *ngIf=\"segmentModel === 'profile'\">\n                    <ion-card-content class=\"\">\n                        <ion-item class=\"item ios in-list ion-focusable hydrated item-label\"\n                            *ngFor=\"let item of days_off\">\n                            <div class=\"input-price\" slot=\"end\">\n                                <ion-button class=\"p-0 remove\" size=\"small\" fill=\"clear\"\n                                    (click)=\"decreaseDaysOff(item.id)\">\n                                    <ion-icon name=\"remove-outline\"></ion-icon>\n                                </ion-button>\n                                <div class=\"display-price\">\n                                    {{ item.quantity }} h\n                                </div>\n                                <ion-button class=\"p-0 add\" size=\"small\" fill=\"clear\"\n                                    (click)=\"increaseDaysOff(item.id)\">\n                                    <ion-icon name=\"add-outline\"></ion-icon>\n                                </ion-button>\n                            </div>\n                            <ion-label class=\"sc-ion-label-ios-h sc-ion-label-ios-s ios hydrated\">\n                                {{ item.name }}\n                            </ion-label>\n                        </ion-item>\n\n\n                        <ion-item class=\"item ios in-list ion-focusable hydrated item-label \">\n                            <div class=\"total\" slot=\"end\">\n                                <p>\n                                    TOTAL/24 : {{totalDaysHours}} heures\n                                </p>\n                            </div>\n                        </ion-item>\n                    </ion-card-content>\n                </ion-card>\n            </ion-col>\n        </ion-row>\n\n        <ion-row>\n            <ion-col size=\"12\">\n                <h3>Quels sports pratiquez-vous ?</h3>\n            </ion-col>\n            <ion-col size=\"3\" *ngFor=\"let item of select_sports\">\n                <div class=\"sports-box\">\n                    <ion-button [class]=\"item.isActive ? 'p-0 active' : 'p-0' \" expand=\"full\" shape=\"round\"\n                        (click)=\"clickSportButton(item)\">\n                        <ion-img class=\"ion_img_icon\" [src]=\"item.icon\"></ion-img>\n                    </ion-button>\n                </div>\n            </ion-col>\n        </ion-row>\n    </form>\n</ion-grid>";
      /***/
    },

    /***/
    "./node_modules/raw-loader/dist/cjs.js!./src/app/components/body-scan-step-three/body-scan-step-three.component.html":
    /*!***************************************************************************************************************************!*\
      !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/components/body-scan-step-three/body-scan-step-three.component.html ***!
      \***************************************************************************************************************************/

    /*! exports provided: default */

    /***/
    function node_modulesRawLoaderDistCjsJsSrcAppComponentsBodyScanStepThreeBodyScanStepThreeComponentHtml(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony default export */


      __webpack_exports__["default"] = "<ion-grid>\n  <ion-row>\n    <ion-col size=\"12\">\n      <h3>ALIMENTATION</h3>\n      <p>Combien de repas prenez-vous par jour ?</p>\n      <p>\n        Suivez-vous un régime alimentaire particulier ? Si oui, cochez le régime\n        correspondant dans la liste suivante :\n      </p>\n    </ion-col>\n    <ion-col size=\"12\">\n      <div>\n        <div float-left class=\"my-checkbox\">\n          <ion-label text-uppercase> OUI</ion-label>\n          <ion-checkbox value=\"yes\" [checked]=\"showNutrition\" (ionChange)=\"checkValue1($event)\">\n          </ion-checkbox>\n        </div>\n        <div float-right class=\"my-checkbox\">\n          <ion-label text-uppercase>NON</ion-label>\n          <ion-checkbox value=\"no\" [checked]=\"!showNutrition\" (ionChange)=\"checkValue2($event)\">\n          </ion-checkbox>\n        </div>\n      </div>\n    </ion-col>\n  </ion-row>\n  <div *ngIf=\"showNutrition\">\n    <ion-row>\n      <ion-col size=\"6\">\n        <ion-list>\n          <ion-item>\n            <ion-checkbox value=\"1\" (ionChange)=\"addRemoveNutrition($event)\"></ion-checkbox>\n            <ion-label>Régime végétarien</ion-label>\n          </ion-item>\n        </ion-list>\n      </ion-col>\n      <ion-col size=\"6\">\n        <ion-list>\n          <ion-item>\n            <ion-checkbox value=\"2\" (ionChange)=\"addRemoveNutrition($event)\"></ion-checkbox>\n            <ion-label>Régime végétalien</ion-label>\n          </ion-item>\n        </ion-list>\n      </ion-col>\n      <ion-col size=\"6\">\n        <ion-list>\n          <ion-item>\n            <ion-checkbox value=\"3\" (ionChange)=\"addRemoveNutrition($event)\"></ion-checkbox>\n            <ion-label>Régime vegan</ion-label>\n          </ion-item>\n        </ion-list>\n      </ion-col>\n      <ion-col size=\"6\">\n        <ion-list>\n          <ion-item>\n            <ion-checkbox value=\"4\" (ionChange)=\"addRemoveNutrition($event)\"></ion-checkbox>\n            <ion-label>Régime à base de viande</ion-label>\n          </ion-item>\n        </ion-list>\n      </ion-col>\n      <ion-col size=\"6\">\n        <ion-list>\n          <ion-item>\n            <ion-checkbox value=\"5\" (ionChange)=\"addRemoveNutrition($event)\"></ion-checkbox>\n            <ion-label>Régime sans gluten</ion-label>\n          </ion-item>\n        </ion-list>\n      </ion-col>\n      <ion-col size=\"6\">\n        <ion-list>\n          <ion-item>\n            <ion-checkbox value=\"6\" (ionChange)=\"addRemoveNutrition($event)\"></ion-checkbox>\n            <ion-label>Régime méditerranéen</ion-label>\n          </ion-item>\n        </ion-list>\n      </ion-col>\n    </ion-row>\n  </div>\n  <ion-row>\n    <ion-col size=\"12\">\n      <p>\n        <strong>Cyclage des calories</strong> : Transformatics peut cycler vos\n        calories en fonction de votre activité physique journalière.\n        Souhaitez-vous gérer vos calories vous-même, où préférez-vous que le\n        cyclage soit effectué automatiquement ?\n      </p>\n    </ion-col>\n    <ion-col size=\"6\">\n      <div class=\"process-btn\">\n        <ion-button [color]=\"calorieCycling == 'Automatic management' ? 'primary' : 'light'\" expand=\"full\"\n          (click)=\"calorieCyclingSelection('Automatic management')\">Gestion automatique</ion-button>\n      </div>\n    </ion-col>\n    <ion-col size=\"6\">\n      <div class=\"process-btn\">\n        <ion-button [color]=\"calorieCycling == 'Manual management' ? 'primary' : 'light'\" expand=\"full\"\n          (click)=\"calorieCyclingSelection('Manual management')\">Gestion manuelle</ion-button>\n      </div>\n    </ion-col>\n    <ion-col size=\"12\">\n      <p>\n        <strong>Préférences alimentaires</strong> : sélectionnez dans les listes\n        suivantes vos aliments préférés dans chaque catégorie\n      </p>\n    </ion-col>\n  </ion-row>\n\n  <div class=\"tran\" *ngIf=\"calorieCycling == 'Automatic management' || calorieCycling == 'Manual management'\">\n    <ion-row>\n      <ion-col size=\"12\">\n        <div class=\"vegiteble-box-title\">\n          <h3>Légumes</h3>\n        </div>\n      </ion-col>\n      <ion-col size=\"6\" *ngFor=\"let item of vagetables\">\n        <div [class]=\"item.active ? 'vegiteble-box active' : 'vegiteble-box'\">\n          <ion-button expand=\"full\" (click)=\"selectVeg(item)\">\n            <img slot=\"start\" [src]=\"item.icon\" />\n            <ion-label>\n              {{ item.text }}\n            </ion-label>\n          </ion-button>\n        </div>\n      </ion-col>\n    </ion-row>\n    <ion-row>\n      <ion-col size=\"12\">\n        <div class=\"vegiteble-box-title\">\n          <h3>Fruits</h3>\n        </div>\n      </ion-col>\n      <ion-col size=\"6\" *ngFor=\"let item of fruits\">\n        <div [class]=\"item.active ? 'vegiteble-box active ' : 'vegiteble-box'\">\n          <ion-button expand=\"full\" (click)=\"selectFruits(item)\">\n            <img slot=\"start\" [src]=\"item.icon\" />\n            <ion-label>\n              {{ item.text }}\n            </ion-label>\n          </ion-button>\n        </div>\n      </ion-col>\n    </ion-row>\n    <ion-row>\n      <ion-col size=\"12\">\n        <div class=\"vegiteble-box-title\">\n          <h3>Féculents</h3>\n        </div>\n      </ion-col>\n      <ion-col size=\"6\" *ngFor=\"let item of starchy\">\n        <div [class]=\"item.active ? 'vegiteble-box active' : 'vegiteble-box'\">\n          <ion-button expand=\"full\" (click)=\"selectStarchy(item)\">\n            <img slot=\"start\" [src]=\"item.icon\" />\n            <ion-label>\n              {{ item.text }}\n            </ion-label>\n          </ion-button>\n        </div>\n      </ion-col>\n    </ion-row>\n  </div>\n\n  <!-- <div *ngIf=\"calorieCycling == 'Automatic management'\">\n    <p>Manual management</p>\n  </div> -->\n</ion-grid>";
      /***/
    },

    /***/
    "./node_modules/raw-loader/dist/cjs.js!./src/app/components/body-scan-step-two/body-scan-step-two.component.html":
    /*!***********************************************************************************************************************!*\
      !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/components/body-scan-step-two/body-scan-step-two.component.html ***!
      \***********************************************************************************************************************/

    /*! exports provided: default */

    /***/
    function node_modulesRawLoaderDistCjsJsSrcAppComponentsBodyScanStepTwoBodyScanStepTwoComponentHtml(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony default export */


      __webpack_exports__["default"] = "<ion-grid>\n    <ion-row>\n        <ion-col size=\"12\">\n            <h3>OBJECTIFS ALIMENTAIRES</h3>\n            <p>Quel est votre objectif physique et sportif ? Choisissez l’un des objectifs suivants et sélectionnez un\n                niveau de suivi :\n            </p>\n        </ion-col>\n        <ion-col size=\"4\">\n            <div class=\"steptwofirstbtn\">\n                <ion-button [color]=\"foodGoalsSelection == 'Gain muscle' ? 'primary' : 'light'\" expand=\"full\"\n                    (click)=\"changeFoodGoalsSelection('Gain muscle')\">Prendre du muscle</ion-button>\n            </div>\n        </ion-col>\n        <ion-col size=\"4\">\n            <div class=\"steptwofirstbtn\">\n                <ion-button [color]=\"foodGoalsSelection == 'Losing weight' ? 'primary' : 'light'\" expand=\"full\"\n                    (click)=\"changeFoodGoalsSelection('Losing weight')\">Perdre du poids</ion-button>\n            </div>\n        </ion-col>\n        <ion-col size=\"4\">\n            <div class=\"steptwofirstbtn\">\n                <ion-button [color]=\"foodGoalsSelection == 'Maintain' ? 'primary' : 'light'\" expand=\"full\" expand=\"full\"\n                    (click)=\"changeFoodGoalsSelection('Maintain')\">Se maintenir</ion-button>\n            </div>\n        </ion-col>\n        <div *ngIf=\"foodGoalsSelection == 'Gain muscle'\">\n            <ion-col size=\"12\">\n                <p>Choisissez cet objectif si votre priorité est de développer votre masse musculaire, d’avoir l’air\n                    plus massif et d’être plus fort.\n                </p>\n                <h3>Rythme de prise de muscle</h3>\n            </ion-col>\n        </div>\n        <div *ngIf=\"foodGoalsSelection == 'Losing weight'\">\n            <ion-col size=\"12\">\n                <p>Choisissez cet objectif si votre priorité est de perdre du poids, d’avoir un corps plus tonifié, sec\n                    et dynamique.\n                </p>\n                <h3>Rythme de perte de poids</h3>\n            </ion-col>\n        </div>\n        <div *ngIf=\"foodGoalsSelection == 'Maintain'\">\n            <ion-col size=\"12\">\n                <p>Choisissez cet objectif si vous êtes satisfait de votre niveau physique actuel et que vous souhaitez\n                    simplement maintenir cette condition. Choisissez cette option si vous êtes satisfait(e) et que vous\n                    souhaitez simplement conserver cette condition physique pendant un laps de temps à déterminer.\n                </p>\n            </ion-col>\n        </div>\n\n        <ion-col size=\"4\">\n            <div class=\"steptwofirstbtn\" *ngIf=\"foodGoalsSelection != 'Maintain'\">\n                <ion-button [color]=\"paceOfMuscleGain == 'Soft' ? 'primary' : 'light'\" expand=\"full\"\n                    (click)=\"changePaceOfMuscleGain('Soft')\">Doux</ion-button>\n            </div>\n        </ion-col>\n        <ion-col size=\"4\">\n            <div class=\"steptwofirstbtn\" *ngIf=\"foodGoalsSelection != 'Maintain'\">\n                <ion-button [color]=\"paceOfMuscleGain == 'Moderate' ? 'primary' : 'light'\" expand=\"full\"\n                    (click)=\"changePaceOfMuscleGain('Moderate')\">Modéré</ion-button>\n            </div>\n        </ion-col>\n        <ion-col size=\"4\">\n            <div class=\"steptwofirstbtn\" *ngIf=\"foodGoalsSelection != 'Maintain'\">\n                <ion-button [color]=\"paceOfMuscleGain == 'Fort' ? 'primary' : 'light'\" expand=\"full\"\n                    (click)=\"changePaceOfMuscleGain('Fort')\">Fort</ion-button>\n            </div>\n        </ion-col>\n\n        <ion-col size=\"12\">\n            <div *ngIf=\"foodGoalsSelection == 'Gain muscle'\">\n                <div *ngIf=\"paceOfMuscleGain == 'Soft'\">\n                    <p>Si vous souhaitez prendre du muscle mais tout en gardant une condition de sèche respectable,\n                        choisissez cette option. Vous construirez du muscle plus lentement qu’en choisissant une option\n                        plus agressive, mais prendrez moins de graisse durant cette période.\n                    </p>\n                </div>\n                <div *ngIf=\"paceOfMuscleGain == 'Moderate'\">\n                    <p>Choisissez cette option si vous voulez prendre du muscle et gagner en force, et que vous ne vous\n                        souciez pas vraiment du fait de prendre un peu de graisse durant cette période.\n                    </p>\n                </div>\n                <div *ngIf=\"paceOfMuscleGain == 'Fort'\">\n                    <p>Choisissez cette option si vous désirez prendre du muscle le plus rapidement possible, même\n                        quitte à prendre de la graisse dans le même temps. Sur cette prise de muscle agressive, votre\n                        poids augmentera significativement, mais cela vous donnera la possibilité de construire beaucoup\n                        plus de masse musculaire dans un laps de temps similaire.\n                    </p>\n                </div>\n            </div>\n            <div *ngIf=\"foodGoalsSelection == 'Losing weight'\">\n                <div *ngIf=\"paceOfMuscleGain == 'Soft'\">\n                    <p>Choisissez cette option si vous souhaitez perdre du poids de manière lente et contrôlée, tout en\n                        gardant un maximum de force et de masse musculaire.\n                    </p>\n                </div>\n                <div *ngIf=\"paceOfMuscleGain == 'Moderate'\">\n                    <p>Choisissez cette option si vous voulez perdre assez rapidement du poids sans prendre le risque de\n                        perdre trop de volume musculaire.\n                    </p>\n                </div>\n                <div *ngIf=\"paceOfMuscleGain == 'Fort'\">\n                    <p>Choisissez cette option si vous désirez perdre de la graisse le plus rapidement possible sans\n                        prendre en considération l’éventuelle fonte musculaire. En choisissant cette option, vous\n                        observerez une réduction de votre force et de votre énergie, et prendrez le risque de perdre\n                        plus de muscle que de graisse durant cette période.\n                    </p>\n                </div>\n            </div>\n            <div>\n                <ion-list>\n                    <ion-item>\n                        <ion-checkbox style=\"margin-right: 10px;\"></ion-checkbox>\n                        <ion-label>Vous avez déjà suivi vos macronutriments par le passé ? Cochez cette case.\n                        </ion-label>\n                    </ion-item>\n                </ion-list>\n            </div>\n        </ion-col>\n    </ion-row>\n</ion-grid>";
      /***/
    },

    /***/
    "./node_modules/raw-loader/dist/cjs.js!./src/app/components/bodysvgback/bodysvgback.component.html":
    /*!*********************************************************************************************************!*\
      !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/components/bodysvgback/bodysvgback.component.html ***!
      \*********************************************************************************************************/

    /*! exports provided: default */

    /***/
    function node_modulesRawLoaderDistCjsJsSrcAppComponentsBodysvgbackBodysvgbackComponentHtml(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony default export */


      __webpack_exports__["default"] = "<div class=\"human-body-back\">\n    <svg version=\"1.1\" id=\"Layer_1\" xmlns=\"http://www.w3.org/2000/svg\" xmlns:xlink=\"http://www.w3.org/1999/xlink\" x=\"0px\" y=\"0px\" viewBox=\"0 0 595.3 841.9\" style=\"enable-background:new 0 0 595.3 841.9;\" xml:space=\"preserve\">\n<style type=\"text/css\">\n.st0{fill:#dad3d3;}\n.st1{fill:none;}\n</style>\n<path d=\"M330.7,725.6c0,0,0.1,0.1,0.1,0.1c0-0.1,0-0.1,0.1-0.2C330.8,725.6,330.7,725.6,330.7,725.6z\"/>\n<path d=\"M363.4,638.7L363.4,638.7L363.4,638.7C363.4,638.6,363.4,638.6,363.4,638.7z M337.9,408.1c-0.3,0-0.5,0.1-0.8,0.1\nc0.3,0.1,0.6,0.1,0.9,0.2C338,408.2,337.9,408.2,337.9,408.1z M309,210.8c-0.1,0.8-0.1,1.7,0,2.6C309.1,212.5,309.1,211.7,309,210.8\nz M281.8,176.2L281.8,176.2c0,0.2,0,0.3,0.1,0.4C281.8,176.4,281.8,176.3,281.8,176.2z M259.9,247.7c-4.8-4.5-9.6-9.1-14.4-13.6\nl-0.1,0.1c5,4.8,10.1,9.7,15.2,14.6C260.8,248.1,260.2,248,259.9,247.7z\"/>\n<path d=\"M395.8,396.9L395.8,396.9C395.8,397,395.8,396.9,395.8,396.9C395.8,396.9,395.8,396.9,395.8,396.9z\"/>\n<path d=\"M428.4,320.8c0,1.8,0,3.6,0,5.5c0.1,0,0.1-0.1,0.1-0.1S428.5,324.1,428.4,320.8z\"/>\n<path d=\"M429.4,338.5c-0.7-2.1-0.8-4.5-0.9-6.7c-0.1-1.8-0.1-3.7-0.1-5.5c0-1.8,0-3.6,0-5.5c-0.3-9.7-2.2-30.3-11.8-41.4\nc-0.6-0.7-1.3-1.4-2-2.1c-1.4-1.3-2.9-2.4-4.6-3.2c-0.4-0.6-0.7-1.2-0.8-1.8c-0.6-3,0.1-6.3-0.3-9.3c-0.6-3.9-1.2-8-2.9-11.5\nc-2.3-4.6-6.1-8.3-10.1-11.7c-5.9-5.1-12.5-7.8-20.4-7.4c-0.9,0.1-2,0.4-2.6,1.1c0,0-0.1,0.1-0.1,0.1c0,0,0,0,0.1,0.1\nc-0.3,0.3-0.6,0.6-0.9,0.9c0.3-0.3,0.6-0.6,0.8-0.9c-2-1.6-2.8-2.2-3.6-2.8c-5.4-4.2-11.1-7.7-18-8.9c-6.8-1.2-13.5-2.5-20.3-3.5\nc-2.3-0.3-3.1-1.2-2.6-3.3c0.8-3.8,1.5-7.7,2.4-11.5c0.7-2.7,1.5-5.4,2.5-8c0.3-0.7,1.2-1.2,2-1.6c1.7-1,4-1.5,4.9-2.9\nc3-4.5,4.5-9.7,4-15.2c-0.4-4.7-2.2-5.8-5.6-3c-0.1,0.1-0.3-0.1-0.6-0.2c1.9-6.7,1.8-13.4,0.6-20.1c-1.4-7.4-5.4-13.1-12.5-16\nc-10.3-4.2-21-4-31.5-1c-4.7,1.3-8.6,3.9-11.1,7.9c-2.7,3.2-4,7.4-4.5,11.9c0,0,0,0.1,0,0.1c0,0.1,0,0.1,0,0.2v0.1\nc0,1.2-0.1,2.5-0.2,3.7c0,0.6,0,1.3,0,1.9c0.1,2.4,0.3,4.8,0.7,7.1c-0.1,0-0.1,0-0.2,0c0.2,1.5,0.5,3,0.8,4.7\nc-4-2.5-5.2-2.4-5.7,0.6c-0.2,1.1-0.1,2.3,0.1,3.4c0,0.2,0.1,0.3,0.1,0.5c0.1,0.4,0.1,0.8,0.2,1.2c0,0,0,0.1,0,0.1\nc0,0.3,0.1,0.5,0.1,0.8c0.3,1.9,0.8,4.2,1.2,6c0.5,4.9,2.6,6.8,7.6,8.3c0.5,0.2,1,1.1,1.3,1.7c0.8,2.5,1.7,4.9,2.3,7.5\nc1,4.2,1.9,8.4,2.5,12.7c0.1,0.7-0.9,2.1-1.6,2.3c-2.4,0.7-4.9,1.1-7.4,1.4c-12.5,1.4-24.7,3.7-35.2,14.6c4.8,4.5,9.6,9.1,14.4,13.6\nc0.3,0.3,0.8,0.4,0.7,1.1c-5.1-4.9-10.2-9.8-15.2-14.6l0.1-0.1c-0.1-0.1-0.1-0.1-0.2-0.1c-0.7-0.7-1.8-1.6-2.6-1.5\nc-3.4,0.3-6.9,0.5-10.1,1.5c-6.7,2-12,6.5-16.2,11.9c-2.1,2.7-5,5.1-5.7,8.8c-1.1,2.6-2.4,6.5-3,11.9c-0.2,1.2-0.3,2.4-0.4,3.8\nc0,0.3-0.1,0.5-0.1,0.8c-5.8,9.1-14.1,12.1-16.8,46.1c-0.1-0.1-0.3-0.2-0.4-0.3c-0.4,7.7-0.8,14.8-1.2,22l0,0c0,0,0.1-0.1,0.1-0.1\nc0.2-0.1,0.3-0.3,0.5-0.4c0.2-0.1,0.3-0.2,0.5-0.3c0.7-0.4,1.3-0.6,2-0.8c0.2,0,0.4-0.1,0.5-0.1c0.2,0,0.3,0,0.5,0s0.3,0,0.5,0\nc0.2,0,0.3,0,0.5,0.1c0.2,0,0.4,0.1,0.7,0.2c0.2,0.1,0.3,0.1,0.5,0.2c0.1,0.1,0.3,0.1,0.4,0.2c0.1,0.1,0.3,0.2,0.4,0.2\nc0.1,0.1,0.3,0.2,0.4,0.3c0.1,0.1,0.3,0.2,0.4,0.4c0.4,0.4,0.8,0.9,1.1,1.5c0.2,0.3,0.3,0.6,0.5,0.9c2,4.7,1.4,9.5,0.4,14.3\nc-0.6,3-1.4,6.1-2.3,9.9c-0.9-3.9-1.6-7.1-2.5-10.2c-1.2-4-2.6-7.9-3.9-11.9c-0.5-1.5-0.8-3.1-1.1-4.6c-0.2,1.7-0.7,3.5-0.4,5.1\nc1.2,5.2,2.5,10.4,4.2,15.5c3.4,10.8,4.9,22.3,10.9,32.3c1.5,2.6,3,5.8,0.2,8.9c-0.4,0.4-0.4,1.7-0.1,2.3c1.4,1.9,1.8,3.9,2.1,6.3\nc0.8,5.5,3,10.7,4.5,16.1c0.3,1,0.5,1.9,0.6,2.9c1.1,5.9,3.3,8.2,8.2,8.2c2.2,0,4.5,0.1,6.7-0.1c0,0,0,0,0,0c0.1,0,0.5-0.2,0.9-0.4\nc0,0,0,0,0,0c0.9-0.5,2.1-1.2,2.3-2.2c1.1-0.4,3-1.1,4-2.1c0,0,0.1,0,0.1-0.1c0.3-0.4,0.6-0.9,0.9-1.3c1.3-1.9,2.6-3.7,2.6-5.6\nc0.1-2.5-1.6-5.1-2.5-7.6c0,0,0,0-0.1,0l0.1-1.6v-0.2c0.1,0.1,0.2,0.1,0.3,0.2c0,0,0,0,0,0c0.7,0.6,1.5,1.2,2.4,2v-0.2l1.4,1\nc2.6,3.6,7.4,4,7.4,4s2.3-0.7,1.4-1.8c-0.9-1.1-2.7-7-2.7-7c-2.1-4.2-4.1-8.2-6.6-11.9c-1.1-1.6-3.1-3.2-4.9-3.5\nc-3.1-0.5-6.3-0.4-9.6-0.4l0,0l-1.1,0h-0.1c-1.4-3-0.1-5.6,1.1-8.4c2.2-5.1,4.3-10.2,6.3-15.4c3.1-7.9,2.7-15.7-1-23.4\nc-2.2-4.6-4-9.5-5.8-14.3c-0.3-0.8-0.1-2,0.4-2.7c4.8-6.7,9.7-13.2,14.5-19.9c1-1.5,2-3.1,2.5-4.8c1.2-3.9,2-7.9,3.1-11.9\nc1.1,1,2.1,2.5,3.5,3.3c2.2,1.2,2.4,3,2.6,5.2c0.5,5.2,0.6,10.6,2.3,15.4c3.8,10,8.5,19.6,12.8,29.4c0.6,1.5,1.5,3,1.5,4.5\nc-0.2,4-0.7,8-1.3,12c-0.9,6.1-3.9,12-1.8,18.4c0.2,0.6-0.4,1.5-0.7,2.2c-2.8,7-3.5,14.2-2.4,21.4c-0.8,6.1-3.1,21.9-4,25\nc-0.5,1.6-0.8,3.3-0.9,4.8c-0.3,1.2-0.6,2.4-0.9,3.6c0,0,0,0,0,0c-1.2,3.5-4.3,12.3-5.1,16.8c-2.5,11.1-1.8,22.3-0.7,33.5\nc0.9,8.8,2.3,17.6,3.4,26.4c1.3,10.2,2.6,20.5,3.7,30.7c0.9,8.3,1.9,16.7,2.3,25c0.4,8.5-2,16.6-6.1,24.1c-3.2,5.8-4.9,12-4.8,18.5\nc0.2,8,1.8,15.6,7.6,21.7v0c0.3,1.7,0.6,3.4,0.9,5.1c1.3,7.7,2.4,15.5,4,23.2c1.3,6.1,3.3,12.1,1.8,18.7l-3.4,5.5\nc-0.5,0.8-0.4,1.9,0.4,2.6l4.7,4.4c-1.5,0.7-11.4,5.3-19.2,8.2c-1.1,0.4-2.2,0.8-3.2,1.2h0c-2.5,0.8-4.4,1.4-5.5,1.3\nc-4.3-0.1-5.9,6-1.7,6.3c0,0.3,0,0.6,0.1,0.9c1.5,0,3.1,0,4.6,0c1.4,0,2.9-0.1,4.3,0.2c2.3,0.5,4.6,1.3,6.8,1.9\nc2,0.5,4.3,0.5,5.8,1.6c1.1,0.8,1.9,1.2,2.9,1.2l12.6,4.1v0.1c16,7.2,19.7-3.8,20.2-5.6c0-0.1,0-0.1,0.1-0.2c0,0,0-0.1,0-0.1\nc-0.3-2.8-0.8-5.7-1.2-8.5c-0.2,0-0.5,0-0.7,0c-1.6-2.2-2.9-6.1-3.9-10.2c0.6-2,0.8-4.6,0.8-7.1c0-1.9-0.2-3.9-0.3-5.5\nc0-0.1,0-0.3-0.1-0.4c0-0.3-0.1-0.5-0.1-0.8c-0.1-0.4-0.1-0.8-0.1-1.1c0-0.4-0.1-0.8-0.2-1.2c-0.4-3.1-1.7-6.2-1.3-9.2\nc1.2-8.4,2.6-16.7,4.7-24.9c1.9-7.3,4.9-14.3,7.6-21.3c2.1-5.7,3.1-11.6,1.4-17.6c-1.8-6.3-3.4-12.7-6.1-18.6\nc-2.8-6.1-3.6-11.6,0.5-17.3c0.2-0.2,0.2-0.6,0.3-0.9c2.3-9.3,4.4-18.6,6.8-27.8c4.4-17.2,5.1-34.8,6.6-52.3\nc0.8-9.3,1.1-18.6,1.7-27.9c0.1-1.2,0.8-2.3,1.2-3.5h1c0.4,1.4,1,2.7,1.1,4.1c0.3,4.4,0.3,8.7,0.6,13.1c0.8,10.2,1.6,20.4,2.4,30.5\nc0.5,5.6,0.9,11.1,1.7,16.7c0.8,5.8,1.9,11.5,3.1,17.2c2.5,12,4.3,24.2,9.8,35.4c1.3,2.6,1.1,4.8,0.2,7.5c-2.7,7.5-5.2,15-7.6,22.6\nc-1.7,5.5-1.6,11,0.3,16.4c4.3,11.9,9.5,23.5,11.1,36.3c1,8,3.6,15.7-0.2,23.7c-0.9,1.8-0.7,4.4-0.1,6.4c1.4,5.1,2.2,10.1-0.8,14.3\nc-0.2,0.3-0.4,0.6-0.7,0.9c0,0,0,0.1-0.1,0.1c-0.8,1.2-2.1,3.4-2.6,5.3c0,0-0.1,0.1-0.1,0.1c-2.1,3.3-0.3,8.3,3.4,9.7\nc10.8,3.9,21,1.8,31-3c1.8-0.9,3.7-1.5,5.6-2c2.1-0.6,4.6-0.5,6.4-1.6c2.2-1.3,4.2-1.5,6.6-0.8c0.4,0.1,0.9,0.2,1.4,0.1c0,0,0,0,0,0\nc0.8,0,1.6-0.2,2.3-0.3c0,0,0,0,0,0c1.3-0.5,2.5-1.7,0.4-4.4c-1.9-1.3-3.6-3.1-5.6-3.8c-2.9-1-6.1-1-9-1.9c-5.5-1.8-10.8-4-16.5-6.1\nc0.4-0.6,1.2-1.6,2-2.7c1.6-2.3,4.2-4.6,2.3-7.6c-2.9-4.7-2.4-9.6-1.5-14.5c2.2-12.1,4.6-24.1,6.9-36.2h0c-0.3,0.8-0.6,1.7-0.9,2.5\nc-0.4,1-0.7,1.9-1.1,2.9c-0.1,0.3-0.2,0.6-0.3,1c-0.1,0.3-0.2,0.6-0.3,1c-0.1,0.2-0.1,0.4-0.2,0.7c-0.1,0.2-0.1,0.4-0.2,0.7\nc0,0.1-0.1,0.3-0.1,0.4c0,0.1-0.1,0.3-0.1,0.4c-0.1,0.2-0.1,0.5-0.1,0.8c-1.4,13-5.9,24.8-11.9,36.2c-1.2,2.2-2.1,4.5-3.7,6.7\nc0.9-4.7,1.5-9.5,2.7-14.2c2.7-10.8,5.6-21.6,8.4-32.3c0.1-0.5,0.3-1,0.4-1.4c0.1-0.2,0.2-0.4,0.2-0.6c0.1-0.1,0.1-0.3,0.2-0.4\nc0.1-0.2,0.1-0.3,0.2-0.5c0.1-0.1,0.1-0.3,0.2-0.4c0.1-0.1,0.2-0.3,0.2-0.4c0.1-0.2,0.2-0.4,0.4-0.5c0.1-0.2,0.2-0.3,0.3-0.4\nc0.1-0.1,0.2-0.2,0.3-0.3c0.3-0.3,0.5-0.5,0.9-0.7c0.3-0.2,0.6-0.4,1-0.5c1-0.4,2-0.5,3.3-0.3c0,0,0,0,0,0v0\nc0.3-0.5,0.5-1.1,0.9-1.5c6.1-7,7-15.3,6.3-24.1c-0.4-5.7-2.4-10.8-5.1-15.8c-2.8-5.1-4.3-10.6-5.2-16.2c-0.2-1.3-0.4-2.6-0.5-4V577\nc-0.1-0.3-0.1-0.6-0.2-0.8h0.1c0,0.3,0.1,0.5,0.1,0.8c0,0,0,0.1,0,0.1v0l0.1,0.1l1-17.7l0.4-7.1l0.3-4.5h0.6\nc0.4-3.5,0.8-7.1,1.3-10.6c1.2-8.9,2.7-17.8,3.8-26.7c1.1-8.6,2.3-17.2,2.9-25.9c0.6-9,0.8-18.1-1.6-26.9\nc-1.8-6.6-3.9-13.1-5.9-19.7c-1.5-5.1-2.6-10.3-4.4-15.3c-1.1-3.1-1.6-5.9-1-9.2c1.8-9.2,1.7-18.3-2.1-27.1c-0.3-0.7,0-1.7,0.1-2.5\nc0.2-2.5,1-5,0.6-7.4c-0.8-4.6-2.7-9-3.5-13.6c-0.8-4.9-1.1-9.6,1.6-14.6c4.7-8.8,8.3-18.2,12.2-27.3c0.3-0.7,0.3-1.6,0.4-2.4\nc2.1-6.2,4.1-13.3,4.1-18.5c0.3-0.2,0.6-0.5,1-0.7c0.2-0.1,0.4-0.3,0.5-0.5c0,0,0,0,0,0c0,0,0,0,0,0.1c2.4,6.8,9.7,16.8,13.3,21.5\nc1.8,4.9,5,8.5,8.4,12c1.1,1.1,1.7,1.9,0.9,3.6c-1.8,4-2.9,8.4-4.9,12.3c-4.6,8.9-5.1,17.9-1.1,27.1c2.2,5.1,4.4,10.2,6.3,15.4\nc0.7,1.8,0.3,4,0.5,6c0,0,0,0,0,0c0,0,0,0,0,0c1.3,4.4,3.8,7.8,7.6,10.3c2.3,1.6,4.6,3,8.1,0.4c-1.6,4.9-3,9-4.3,13\nc-1,3.1-2.2,6.2-3.1,9.4c-1.2,4.1-4.1,5.7-8.1,4.1c-0.4-0.2-1.1,0.1-1.6,0.2c-3.1,0.2-5-0.7-3.3-4.1c1.6-3.2,3.3-6.4,5-9.8\nc2.5,0.4,2.8,2.2,2.2,4.2c-0.6,1.8-1.8,3.3-2.7,5c4.4,1.2,5.3,0.4,4.5-3.6c-0.2-1,0-2-0.2-3c-0.2-0.9-0.7-1.8-1.2-2.5\nc-0.9-1.3-2.2-2.3-3-3.6c-1.3-2.2-1.8-4.6,0-6.9c0.3-0.4,0.3-1.1,0.4-1.7c-3.4,0.2-3.7,6.7-8.1,3.8c-1.8,0.4-3.7,0.5-5.2,1.3\nc-3.5,1.9-5.3,5-5.5,9.2c-0.1,2-0.4,4.1-4,4.8c-0.3-4.1-0.8-8-0.9-11.8c0-0.9,1-2,1.8-2.7c2.9-2.7,6.2-5,6.9-9.4\nc0.1-0.7,1.7-1.2,2.5-1.7c-0.1-0.2-0.2-1.1-0.2-1.2c1.3-0.4,2.7-0.9,4-0.9c3.2,0.1,5.9-1.1,8.4-2.7v0c-2.4,0.1-4.8,0.2-7.3,0.3\nc-4.5,0.2-8,1.6-9.8,6.2c-0.9,2.2-1.8,4.8-3.6,6.2c-6,4.6-4.2,11.6-1.9,18.6c3.9,0.3,5.2-2.8,6.6-5.6c2.5,4.1,2.5,9.8,8.7,10.2\nc0.4,2.6,1.9,3.5,4.5,3.4c2.1-0.1,4.3,0,6.4,0c3.5-0.1,5.7-1.7,6.7-5.1c1.9-6.7,3.5-13.5,5.5-20.3c0.9-3,0-6.5,2.9-8.9\nc0.4-0.3,0.4-2,0-2.6c-2.7-3.3-1.1-6.1,0.5-9.1c1.8-3.4,3.9-6.7,4.9-10.3c2.5-8.4,4.4-17.1,6.6-25.6c0.6-2.4,0.8-4.9,1.7-7.2\nC429.3,344.5,430.4,341.7,429.4,338.5z M385.1,414.8c-1.3,2.1-2.6,4.1-3.9,6.3C379.9,418.8,381.1,416.6,385.1,414.8z M383.7,427.1\nc0,0.1-0.2,0-0.3,0c-2-3.4-0.7-3.8,4.6-12.4c1.9,1.4,2.5,3,0.9,5.1C387.2,422.2,385.5,424.6,383.7,427.1z M388.8,430.1\nc-0.4,0.6-1.3,0.9-2,1.3c-0.4-0.9-1.3-1.8-1.2-2.6c0.2-1.1,1-2.2,1.7-3.1c1.8-2.6,3.7-5.1,5.7-7.8c1.4,1.9,1.4,3.3-0.1,5.1\nC391.1,425,390.2,427.7,388.8,430.1z M409.2,274.7C409.2,274.7,409.2,274.8,409.2,274.7c0.5,0.4,3.3,3.3,6.6,8.3\nc5.4,8.1,11.9,21.9,10.3,39.6c0.1,0.3,0.1,0.7,0.2,1v3.2c-0.7-1.7-1.4-3.3-2-4.8c-2.6-10.9-14.1-34-16.3-38.5\nC407.8,280.8,408.9,278,409.2,274.7z M372.8,235.7c1.9-2.5,4.3-2.5,7-1.8c12.6,3.1,20.5,11.4,25.2,23c1.6,3.9,2.4,8,1.6,12.5\nc-0.7,3.7,0.2,7.6,0.4,11.1c-3.2-3-6-6.4-9.5-8.8c-5.1-3.5-11-5.1-17.3-6c-8.8-1.3-17.4-4.1-26.1-6.3c-2.1-0.5-3.8-1.7-2.2-4.3\nc0.8-1.3,1.5-3,2.7-3.8C361.4,246.9,367.9,242.1,372.8,235.7z M361.7,275.1c-1.1,1.5-2.3,3.4-3.6,5.6c0,0,0,0,0,0\nc-0.4,0.6-0.7,1.3-1.1,2c0,0.1-0.1,0.1-0.1,0.2c-1.1,2-2.3,4.1-3.5,6.4c-3.2,6-12.6,11-16.9,13.2c-0.5,0.2-1,0.5-1.3,0.6\nc-0.2,0.1-0.5,0.2-0.6,0.3c-0.2,0.1-0.4,0.2-0.4,0.2c2-3.4,3.4-5.9,4.9-8.4c0.9-1.5,2.1-2.8,2.7-4.4l1.4-4.4l4.4-13.8l-0.1-0.1\nc0.3-1.3,0.3-2.7,1-3.8c1.6-2.5,1.1-5.5,2.2-8.2c6.8,2.1,13.5,4.1,19.8,6c-2.8,2.6-5.8,5.5-8.8,8.3\nC361.6,274.9,361.7,275,361.7,275.1z M365.4,241.8c-2,1.9-4.2,3.5-6.4,5.2c-0.6,0.6-1.1,1.1-1.7,1.7c-0.1,0-0.2-0.1-0.2-0.1\nc0.6-0.5,1.3-1,2-1.5c4.3-4.1,8.5-8.2,12.9-12.4C369.7,237,367.7,239.5,365.4,241.8z M328.9,202.5c-0.3-0.1-0.5-0.1-0.8-0.2\nc-1.2-2.6-2.3-5.2-3.6-8c2.6-1,5.1-1.9,8.5-3.1C331.5,195.4,330.2,199,328.9,202.5z M326.8,216.8c-7.2-0.4-7.3-6-8.2-10.6\nc-0.7-3.7-0.8-7.6-1.1-11.3c3.5-1.1,5.7-0.8,6.1,3.2c0.1,1.2,0.9,2.5,1.6,3.6c2.3,3.7,1.8,7.2,0.3,11\nC325.1,213.6,326.4,215.3,326.8,216.8z M282.3,192.7c-2.7-1.6-2.8-1.7-4.7-6.8c-0.2-0.5-0.6-0.9-1.1-1.3c-0.5-1.9-0.7-4.1-0.8-5.9\nc0.1-1.9,0.3-3.7,0.4-5.5c0.3-0.2,0.6-0.4,0.9-0.5c1.2,0.9,2.5,1.9,3.7,2.8c0.7,3,1.5,5.6,2.1,7.5c0,0,0,0,0,0.1\nc0.2,1.3,0.5,2.6,0.9,3.9C284.5,189.3,283.8,191,282.3,192.7z M281.7,176.1C281.7,176.1,281.8,176.1,281.7,176.1\nc0.1,0.2,0.1,0.3,0.2,0.4c-0.1-0.1-0.1-0.2-0.1-0.3C281.7,176.2,281.7,176.1,281.7,176.1z M283.3,180.3c-3.8-18.3-2.2-27.8-0.2-32.5\nc1.2-2.9,2.6-4,3.1-4.3c0,0,0,0,0,0c0-0.1,0.1-0.1,0.1-0.1c1.9-2.2,4.2-3.9,7.1-5.1c7.6-3.1,15.4-4.1,23.5-2.4\nc8.7,1.9,16,5.5,18.7,14.8c0.9,3.1,1.7,6.3,1.7,9.5c-0.1,5.4-0.8,10.8-1.3,16.3c0,0,0,0,0.1-0.1h0c0.4-0.3,0.8-0.6,1.2-0.9\nc1.2-0.9,2.5-1.9,3.8-2.9c2.5,5.5-0.8,17.4-5.2,20.2c-1.5-1.7-2.4-3.2-1.6-5.8c0.4-1.3,0.7-2.7,0.9-4.1c0.1-0.7,0.2-1.4,0.3-2.1\nc0-0.2,0.1-0.5,0.1-0.7c0.2-1.2,0.3-2.4,0.5-3.6c-1.2,3.3-2.3,6.6-3.7,9.8c-2.2,5.2-7.2,7-12.1,4.2c-2.4-1.4-4.5-3.3-6.8-5\nc-0.4-0.3-0.9-0.5-1.4-0.7c-1.5-1.1-3.4-2.1-5.4-1.4c0-0.3-0.1-0.7-0.1-1c-1.2,1.6-2.1,3.3-3.5,4.5c-1.5,1.3-3.3,2.4-5.1,3.3\nc-5.5,2.9-10.2,1.2-12.5-4.7C284.7,183.8,284,182,283.3,180.3z M300,194c0.8,7.1-0.6,13.4-2.5,19.4c-0.6,1.8-3.6,2.7-5.4,4\nc-0.2-2.6,0-4.6-0.5-6.4c-1.6-5.9,2-10.4,3.7-15.4C295.5,194.6,298.2,194.5,300,194z M289.2,202.6c-1.3-3.5-2.7-7.1-4.3-11.3\nc3.4,1.2,5.8,2.1,8.5,3.1c-1.1,2.6-2.3,5.3-3.4,8C289.7,202.4,289.4,202.5,289.2,202.6z M247.4,233.2c6.3-5.3,12.8-8.1,20-9.4\nc7.4-1.3,14.8-2.1,22.1-3.8c7.5-1.7,13.8-5.8,17.3-13c1.9-4,1.7-8.3-0.2-12.3c-1.3-2.7-4.2-1.9-6.5-2.3c-0.1,0-0.2-0.3-0.3-0.5\nc4.2-2.6,6.1-4.3,6.7-6.4c0.9-0.2,2.7-0.5,4.4-0.1c0,0.1-0.1,0.1-0.1,0.2c2.4,2.3,4.7,4.5,7.1,6.8c-0.4,0.1-1.1,0.3-1.7,0.3\nc-3.4-0.1-4.9,1.7-5.8,4.9c-1.2,4.8,0,9,2.8,12.8c3.8,5.2,9.1,8.4,15.3,9.7c7.3,1.6,14.8,2.4,22.1,3.8c7.1,1.3,13.7,4.1,19.3,8.8\nc-0.2,0.4-0.2,0.8-0.4,0.9c-4.6,4.4-9.4,8.6-13.9,13.1c-2.1,2.1-4.3,4.5-5.2,7.3c-1.9,5.4-3.3,11-4.9,16.8c-0.1,0.3-0.2,0.6-0.2,0.9\nc-0.1,0.9-0.4,2.2-0.9,4.1c-0.4,1.5-1,3.3-1.8,5.4c0,0,0,0,0,0.1c-0.9,2.4-2.2,5.2-3.9,8.2c-0.2,0.5-0.4,1-0.7,1.5\nc-1.6,3.5-3.3,6.9-5.3,10.1c-6.1,9.4-15.9,16-20.7,26.5c0.2-11.9,0.3-23.7,1.2-35.4c0.6-8,1.8-16,3.5-23.9c1.7-7.9,5.3-15,12-20.4\nc-11.1-11.1-19-23.4-19.6-34.3c-0.6,11.1-8.6,23.3-19.1,33.9c2.6,3.5,5.3,6.5,7.3,9.9c5.8,10.1,6.9,21.5,7.8,32.7\nc0.7,8.5,0.5,17.1,0.8,25.6c0.1,4,0.5,8,0.7,11.4c-3.9-4.9-7.5-10.5-12.1-15.1c-8.6-8.7-14.8-18.8-18.5-30.3\nc-2.7-8.3-5-16.8-7.4-25.2c-1.2-4.2-3.4-7.4-6.7-10.2C257,241.9,252.4,237.7,247.4,233.2z M301,211.1c0.4-5.4,0.8-11,1.3-16.8\nc2.3-0.9,3.4-0.4,4,2.4C307.6,202.9,305.4,208.4,301,211.1z M317,211.1c-4.1-1.9-7.3-9.8-4.9-15.6c0.4-1,2.4-1.4,3.6-2\nC316.1,200,316.6,205.7,317,211.1z M318.3,389.5c-7.8-15.6-8.2-32.3-7.7-49.1c0-0.4,0.2-0.7,0.3-0.9c4.8,6.5,9.6,12.9,14.2,19.5\nc3.7,5.2,4.8,11.2,5.6,17.4c0.9,7.7,3,15,8.2,21.6c-6.3,2.9-12.5,5.8-19.2,8.9C322.1,400.6,321,394.8,318.3,389.5z M298.4,406.9\nc-6.7-3.1-12.9-5.9-19.3-8.9c6.1-7.4,7.7-16,8.7-24.7c0.9-7.9,4.7-14.3,9.4-20.4c3.3-4.3,6.6-8.7,9.9-13c-0.1-0.1,0.3,0.1,0.3,0.4\nc0.6,9.6,0.3,19-1.2,28.5c-1.2,7.7-3.5,15-7,22C296.7,395.7,296.1,401.2,298.4,406.9z M306.8,299.6c-0.7-12.2-1.3-24.4-5.4-36.2\nc-2.1-6-5.2-11.3-9.7-15.5c3.6-4.9,7.5-9.3,10.4-14.3c2.9-4.9,4.8-10.3,7.3-15.6c2.1,11.7,9.2,20.7,17.2,29.6\nc-4.6,4.2-8.2,9.2-9.8,15.1c-1.9,7-3.6,14.1-4.3,21.3c-1.1,12-1.4,24.1-2,36.2c-0.2,4-0.4,7.9-0.7,11.9c-0.1,0.9-0.4,1.7-0.6,2.5\nc-0.2,0-0.4,0-0.5,0c-0.2-0.8-0.5-1.5-0.5-2.3C307.7,321.4,307.4,310.5,306.8,299.6z M299.3,399.4c1.3-5.2,3.5-10.1,4.8-15.2\nc1.4-5.6,2.5-11.4,3.3-17.1c0.7-5.5,0.9-11.1,1.2-16.6c0.2,0,0.4,0,0.6,0c0.4,5,1,10,1.2,15.1c0.3,9.4,2.8,18.2,6.5,26.8\nc1.8,4.3,2,8.8,1.5,13.3c-0.6,4.9-7.9,8.4-13,6.3C300.3,409.8,297.8,405.2,299.3,399.4z M283.2,302.7c-6-2.2-11.9-4.5-15.7-9.8\nc-2.7-3.7-4.8-7.7-7.1-11.6c-3.4-5.9-7.5-11.2-13.2-14.7c6.7-2.1,13.3-4.1,20-6.2C271.4,274.9,274.4,289.9,283.2,302.7z\nM212.9,256.9c4.7-11.7,12.7-19.7,25.2-23.1c4.5-1.2,6.8,1.2,9.1,4c4.1,5.2,9.4,9,14.8,12.6c1.2,0.8,2.5,1.9,3.3,3.1\nc2.1,2.8,1.5,5-1.7,5.9c-8.3,2.3-16.7,5.4-25.2,6.1c-5.1,0.4-9.6,2-13.9,3.9c-4.2,1.9-7.6,5.9-11.3,9c-0.6,0.5-1,1.2-1.8,2.2\nc0-3.7,0.7-6.9-0.2-9.8C210,265.8,211.2,261.2,212.9,256.9z M205.3,277.7c0.1,0.1,0.3,0.2,0.4,0.3c1-0.8,1.9-1.6,3.4-3\nc0.3,4.2,0.5,7.8,0.7,11.5c1-1.2,2-2.5,3.1-3.8l-18.4,36.5l-0.1,0.3l-1.5,3C194,311.9,197.4,288.4,205.3,277.7z M206.7,398.4\nc-0.1-1.8-0.3-3.6-0.6-5.4c-0.8-4.6-1.7-9.2-2.4-13.9v6.2c-3.8-7-6.2-13.5-4.8-20.9c0.6-2.9,0.9-5.8,1.7-8.6\nc0.6-1.9,1.3-4.2,2.8-5.5c4.4-4.1,9.2-7.9,14.3-12.1c0,7.5,0.1,14.5,0,21.4c-0.1,5.4-2.6,10.2-5.4,14.6c-5.7,9.2-5.2,18.9-2.9,28.8\nc0.1,0.3,0.4,0.5,1,1.3c-2.2-11.3-1.2-21.4,5.7-30.7c0.7,4.4,1.6,8.7,2.1,13.1c0.3,2.8,0.2,5.7,0.4,8.5c0.4,4.4-4.1,11.5-8.5,12.6\nc-2.2,0.6-3.4-1.1-3.5-3.1C206.5,402.7,206.8,400.6,206.7,398.4z M220.7,359.3c2.2,7.7,1,14.5-0.2,22.1c-1.5-3.7-2.9-6.6-3.9-9.6\nc-0.2-0.7,0.7-1.8,1-2.7C218.6,366,219.5,363,220.7,359.3z M233,430.2c-0.9,1.7-2.6,2.8-3.7,3.3c0-0.1,0-0.1,0-0.2\nc0.1-1.1-0.6-3.4-1.2-5c0,0,0,0,0,0c-0.3-2.1-1.3-3.7-2.7-5.2c-1.7-1.7-1.7-3.2-0.2-5.2c1.9,2.5,2.6,3.3,3.4,3.6\nC230,423.1,234,428.1,233,430.2z M235.7,426.8c-0.3,0.1-0.6,0.3-0.9,0.4c-0.1,0-0.2-0.1-0.3-0.1l-5.3-7.1l-0.2-0.2\nc-1.2-1.7-2.4-3.4-3.5-5c0.4-0.2,0.9-0.3,1.4-0.3h0c1.4-0.1,3.2,0.1,4.9,0.4l1.1,2.8c0.3,1.2,0.6,2.4,1,3.6\nC234.6,423,235.2,424.9,235.7,426.8z M222.1,397.9l1.2-0.1c2.8,0.9,5.5,1.9,8.6,2c1.8,0.1,5.1,2.6,4.9,3.3c-0.6,2.7,1.7,3.5,2.5,4.9\nc0.6,1,1.4,1.9,2.4,2.7v0c1.9,3.1,2.6,8.9,1.9,8.9c-0.8,0-3.7-2.9-3.7-2.9l-2.8-2l-0.7-0.5c-1.1-0.9-2-2.4-3.5-1.5c0,0,0,0,0,0\nc-2.2-0.5-4.2,0.1-5,0.4c-1.9-1.8-3.7-3.5-5.4-5.1c-0.4,1-0.4,1.8,0,2.3c2.7,3.5,0.5,6.8-1.6,8.8c-2,2-2.9,3.8-3.1,6.5\nc-0.4,4.3,0.1,5,4.8,4.3c-1-1.7-2.2-3.2-2.9-5c-0.7-1.9-0.5-3.8,2-4.5c1.9,3.6,3.7,7,5.7,10.8c0,0,0,0,0,0c0.2,0.9,0.3,1.9-0.1,2.6\nc-2.1,0.4-4.2,0.8-6.3,1.2c-2.8,0.5-6-1.2-6.9-3.7c-1.6-4.4-3.2-8.9-4.6-13.3c-1-3.1-1.7-6.3-2.7-10c3.3,2.4,5.6,0.8,8-0.8\nC218.5,404.9,220.3,401.4,222.1,397.9z M228.4,355c2,5.9,1.4,11.6-0.7,17.4c-1.8,5.1-3.5,10.3-5.2,15.4c-0.2,0.7-0.6,1.3-0.9,2\nc-0.2-0.1-0.5-0.2-0.7-0.2c0.6-3.2,1.1-6.4,1.8-9.6c2-8.5,2.1-16.8-1.5-25.2c-1.8-4.2-1.7-9.2-2.3-13.9c-0.1-1,0.3-2.2,0.7-3.2\nc0.2-0.4,1-0.7,1.4-0.7c0.5,0.1,1.1,0.5,1.3,1C224.5,343.6,226.5,349.3,228.4,355z M202.2,349.3c-1.2-4-2.5-8.2-4-13\nc8.3-8.5,21.9-8.4,32.2-15.2C223.2,333,210.7,339.3,202.2,349.3z M241.4,298.3c-0.7,3.8-2.2,7.3-3.4,10.7c-1.6,4.7-5.5,8.1-9.9,10.3\nc-6,3-12.3,5.5-18.5,8.3c-3.2,1.4-6.5,2.8-9.4,4.6c-1.4,0.9-2.2,2.8-3.3,4.3h-6.7c0.1-0.8,0.1-1.7,0.4-2.4c1.4-3.7,2.9-7.3,4.2-11\nl18.5-40.6c0.3-0.3,0.5-0.6,0.8-0.9c2.3-2.7,4.1-5.9,6.9-7.8c3.1-2.1,7-3,10.5-4.6c2.7-1.2,4.4-0.1,5.7,2.1c0.6,0.9,1.1,2,1.2,3.1\nc0.5,4,1.2,8.1,1.2,12.1C239.6,290.4,242.2,293.7,241.4,298.3z M245.8,295.5c-1.8-2.8-3.4-6.1-3.9-9.4c-1-6.2-0.3-12.8-5.7-17.9\nc9.1-2.3,14.2,2.6,18.6,8.5c2.6,3.6,4.9,7.5,7,11.4c4.3,8,10.9,12.9,19.5,15.1c0.4,0.1,0.7,0.3,1.1,0.5c-2.4,2-9.8,2.8-13.6,1.8\nc-3.2-0.9-6.7-1.4-9.9-2.4c-2.7-0.9-5.3-2.2-7.8-3.6C249.2,298.5,246.9,297.3,245.8,295.5z M256.3,327.2c-3.6-7.6-6.3-15.5-5.5-24\nc10.6,3.7,21.1,7.4,32.4,2.4c1.3-0.6,2.3-0.6,3.4,0.6c2.8,3,6,5.7,8.6,8.8c2.8,3.2,5.7,6.5,7.7,10.3c1.6,3.1,2.8,6.9,2.5,10.3\nc-0.2,3-2.3,6.1-4.2,8.8c-3.4,4.8-7.3,9.1-10.8,13.8c-1.1,1.5-1.7,3.5-2.5,5.3c-0.4,0.9-1,1.7-1.8,3.2c-0.5,4.4-0.6,10-1.9,15.4\nc-1.2,5.2-3.5,10.2-5.3,15.3c-0.4-0.1-0.9-0.2-1.3-0.3c0-2.5-0.2-4.9,0-7.4c1.1-12.5-2.4-24-7.8-35.1\nC265.4,345.5,260.7,336.4,256.3,327.2z M267.2,358.6c-0.1-0.8,0.1-1.5,0.1-2.3c7.3,12.1,9.9,25.1,9.4,38.9c-2.7-0.2-5-2.3-5.3-4.5\nc-0.8-0.1-1.6,0.1-1.9-0.3c-2.6-3.3-6.1-6.3-3.8-11.2C263.9,372.1,268,365.5,267.2,358.6z M257.9,543v-10.8c-0.2,0-0.3,0-0.5,0v7.2\nc-1.7-8.8-3-17.5-4.2-26.4c-1.2-9.3-2.3-18.6-3.3-28c-1-9.2-0.9-18.3,1.1-27.4c0.7-2.3,1.6-5.1,2.5-7.3c0.1-0.2,0.2-0.4,0.3-0.6\nl0,0.6l0.3,4.1c-0.4,0.5-0.8,1-0.7,1.4c0.6,6,1.1,12.1,2,18.1c1.2,7.8,3.7,15.5,3.4,23.6c-0.2,4.6,0.3,9.4,1,14\nC261.3,522.1,259.5,532.5,257.9,543z M261.4,508.7c-0.3-7.9-0.5-15.8-0.2-23.6c0.1-2.6,1.7-5.2,2.8-7.6c1.6-3.5,4.8-3.8,8.1-3.8\nc2,0,4,0.2,6,0.2c0.1,0,0.3,0,0.4,0c0.1,0,0.1,0,0.2,0c0.1,0,0.2,0,0.3,0c0.1,0,0.1,0,0.1,0c0,0,0.1,0,0.1,0c0.1,0,0.1,0,0.2,0.1\nc0.2,0.1,0.3,0.2,0.4,0.3c0,0.1,0.1,0.1,0.1,0.2c0.1,0.2,0.1,0.5,0.1,0.9c0,0.1,0,0.2-0.1,0.2c0,0.1,0,0.2-0.1,0.3c0,0,0,0,0,0l0,0\nc0,0,0,0.1,0,0.1h0c1-0.8,2-1.6,3.7-2.9c0.8,6.3,1.4,11.5,2,16.7c0.7,6.7,1.7,13.3,1.9,20c0.2,7.2-0.5,14.4-0.9,21.5\nc-0.2,3.8-0.9,7.7-0.9,11.5c-0.2,8.2-0.1,16.4-0.1,24.6c-0.4,0-0.7,0.1-1.1,0.1c-1-3.4-2.3-6.8-3-10.3c-2.4-11-5.1-22-6.6-33.2\nc-1.7-12.3-0.6-24.7,2.5-36.8c0.2-0.6,0.3-1.2,0.4-1.8c0.1-0.6,0.3-1.2,0.4-1.7c0.6-2.6,1.1-5.2,1.6-7.8c-0.2,0.7-0.5,1.5-0.7,2.2\nc-0.5,1.5-1,3.1-1.5,4.6c-0.5,1.6-1,3.1-1.5,4.6c-0.3,1-0.6,1.9-0.9,2.9c-0.2,0.7-0.4,1.4-0.6,2c-0.2,0.7-0.4,1.4-0.6,2\nc-3,11.4-1.9,23-1.3,34.5c0.2,3.5-0.6,7.1-1,10.6c-0.3,0-0.6-0.1-0.9-0.1v-8.1c-0.5,7.9,0,15.9-4.6,23.1c-0.2-0.6-0.3-1.1-0.5-1.7\nc0.2,7.3-4.8,12.9-7,19.5c0-4.4-0.4-8.8,0.1-13.2c1.1-11.7,2.9-23.3,3.8-35C263,519,261.6,513.8,261.4,508.7z M280.7,566.4\nc-0.8,2.2-2.2,4.2-3.5,6.7c-1.3-6.5-3.5-12.2-7.7-16.9c-0.4-0.4-0.5-1.3-0.4-1.9c1.5-5.4,3.1-10.8,4.6-16.1c0.1-0.3,0.3-0.7,0.8-1.5\nc1.1,4.6,2,8.6,3,12.6c1.1,4.1,2.6,8,3.6,12.1C281.5,562.9,281.3,564.8,280.7,566.4z M254.4,633.8c-0.1,0.1-0.3,0.2-0.4,0.3\nc-0.5-0.7-1.1-1.4-1.4-2.2c-3.5-10.5-4.6-20.9,0.6-31.2c3.5-7.1,5.1-14.6,6.1-22.4c0-0.1,0-0.3,0.1-0.4c0,0,0-0.1,0-0.1\nc0-0.1,0-0.2,0-0.3c0-0.1,0-0.2,0.1-0.4c0-0.2,0.1-0.3,0.1-0.4c0.1-0.1,0.1-0.3,0.2-0.4c0-0.1,0.1-0.1,0.1-0.2\nc0.1-0.1,0.1-0.2,0.2-0.2c0.1-0.1,0.2-0.1,0.3-0.2c0.1,0,0.1,0,0.2-0.1c0.1,0,0.1,0,0.2-0.1c0.1,0,0.3,0,0.4,0h0c0.1,0,0.2,0,0.3,0\nc-0.4-1-0.5-1.9-0.3-2.8c0-0.1,0-0.3,0.1-0.4c0.1-0.4,0.2-0.7,0.3-1c0.1-0.2,0.2-0.4,0.3-0.5c0-0.1,0.1-0.1,0.1-0.1\nc0.1-0.2,0.2-0.3,0.3-0.4c0.1-0.2,0.3-0.4,0.4-0.6c0.1-0.2,0.3-0.4,0.4-0.5c0.3-0.3,0.5-0.6,0.8-0.9v0h0v0c0.9-2.7,1.8-5.4,2.6-8.1\nc0.4,0,0.8,0,1.1-0.1c1.6,4.6,3.2,9.2,4.8,13.8c0.3-0.1,0.5-0.2,0.8-0.3c-1.7-4.3-3.3-8.6-4.9-13c-0.4-1-0.5-2.1-0.8-3.2\nc0.5-0.2,1-0.3,1.5-0.5c1.9,5.3,3.8,10.7,5.9,15.9c1.8,4.5-1.1,8.5-1.2,12.8c-0.1,13-0.2,26-0.3,38.9c0,0.9,0.4,1.8,0.6,2.7\nc2,6.8-2.2,11.5-9.4,10.2c-0.3-0.1-0.6,0.1-0.9,0.1v0c0,0,0,0,0,0c-1.5-0.5-3.1-1-4.6-1.6v0h0c0,0,0,0,0.1-0.1c0,0-0.1,0-0.1,0\nc-0.3,0.2-0.7,0.4-1,0.5c-0.1,0.1-0.2,0.1-0.2,0.1c0,0,0,0-0.1,0c0,0-0.1,0-0.1,0c0,0-0.1,0-0.1,0c-1.4-1.5-2.7-3.2-3.7-4.5\nC254.1,632.5,254.2,633.1,254.4,633.8z M259.5,658.8c-0.9-5.8-2.2-11.5-3.4-17.2c-0.2-1-1-1.9-1.4-2.9c3.4-0.3,5.8,0.9,6.6,4\nc3.3,11.9,6.5,23.9,9.5,35.9c1.1,4.3,1.7,8.8,2,13.3c-1.3-2.9-2.7-5.8-4-8.7C265.2,675.3,261,667.6,259.5,658.8z M260.5,719.8\nc0,0,0.1-0.1,0.1-0.1c-3.7-1.3-7-2.5-10.4-3.6c-2.4-0.8-5-1.2-7.4-1.9c-1.3-0.4-2.2-1.1-3.8-0.3c-0.6,0.3-1.3,0.5-2.1,0.6\nc1-0.6-0.9-1.4-0.9-1.4s-1.1-2,2-2.4c1.1-0.2,3.4-1.1,6-2.2h0c1.3-0.5,2.6-1.1,3.9-1.6h0c2.5-1.1,4.9-2,6.2-2.4\nc0.3-0.1,0.5-0.1,0.6-0.2c0,0,0,0,0,0c0.1,0,0.2,0,0.2,0c1.8,0,4.7,0,6.3-0.9c1-0.6,2.1-1.5,2.8-2.1c-0.3,1.9-0.4,3.1-0.4,4\nc-0.1,0-0.1,0.1-0.2,0.1c-0.8,6.2,2.2,13,3.7,16.1L260.5,719.8z M265.1,708L265.1,708c0.4-2.4,0.7-4.6,1.1-7.1\nc0.7,0.5,1.4,1,1.9,1.6c1.4,1.5,2.1,3.3,2.1,5.4c0,1.7,0,3.5,0,5.2c0.1,3,0.2,5.9,0.3,8.9C266.9,719.6,265.6,712.5,265.1,708z\nM269.4,700.5c-0.5-0.4-1-0.8-1.5-1.2c-3.3-2.5-7.3-5.8-7.4-6.8c-0.2-1.8,2.7-5,2.7-5l0,0c1.7-2.2,0.6-5.4,1.7-8.3\nc2.7,6.3,5.2,12,7.6,17.7c-0.2,1.7-0.5,3.6-0.7,5.4C271.2,701.9,270.3,701.3,269.4,700.5z M288.2,714.8c0.3,1.4,0.6,2.8,0.9,4.2\nc0.1,0.6,0.3,1.1,0.5,1.7c-8.1,7-13.7,4.7-15.7,3.3c-1.3-2.9-3.1-5.5-2-8.8c6.9,2.6,11.4,1.7,14.8-2.6\nC287.5,714,288.1,714.7,288.2,714.8z M284.2,683.6V694c0.1,0,0.1,0,0.2,0c-0.1,0.7-0.2,1.4-0.2,2c-0.5-2.6-0.8-5.1-1.1-7v-5.4\nC283.5,683.6,283.8,683.6,284.2,683.6z M286.5,649.1c-0.6,2.7-1.2,5.5-1.6,8.3c-0.9,5.9-2,11.7-2.6,17.6c-0.5,4.9-0.7,9.8-0.9,14.8\nl-0.1,0.5c-0.2,6.5,0.8,11.7,2.1,15.6c0,0,0,0,0,0c0.3,0.7,0.5,1.4,0.8,2c0.7,1.8,1.5,3.2,2.1,4.3c-1,0.4-2.1,0.9-2.6,0.9\nc-2.8,0.4-5.6,1-8.5,1c-0.9,0-1.7-0.2-2.3-0.7v0c0.3-2.9,1.2-13.6,1.6-17.3c0.8-0.8,0.8-1.5,0.5-3.4c-1.5-8.6-2.7-17.3-4.9-25.8\nc-2.3-9.1-5.6-17.9-8.5-26.8c6,0.5,11.7-0.5,14.4-7c3,3.3,5.7,6.4,8.5,9.4C286.7,644.4,287.1,646.5,286.5,649.1z M293.8,605.8\nc3.8,10.8,1.6,20.5-3.4,30.7c0.6-5,1.2-9.4,1.7-13.9c-0.1,0-0.3,0-0.4,0c-1.3,6.9-2.5,13.9-3.8,20.8c-0.3-0.1-0.7-0.1-1-0.1v-15\nh-0.5v12.5c-0.4,0.1-0.7,0.1-1,0.1c-0.8-5.3-1.6-10.7-2.4-16c-0.2,0-0.5,0.1-0.7,0.1c0.6,4.8,1.3,9.5,1.9,14.2\nc-2.9-2.8-3.4-4.8-4.7-18.1c0,2-0.1,4,0,5.9c0.1,1.9,0.3,3.8,0.5,5.6c-4.5-4.8-5.1-10.7-4.9-16.9c0.3-8.9,0.1-17.9,0.8-26.8\nc0.4-5.8,2-11.4,3-17.2c0.3,0.1,0.5,0.1,0.8,0.2v15c0.8-7.1,0-14,2-20.5c0.4,0,0.7-0.1,1.1-0.1c1,3.9,1.9,7.8,3,11.6\nC288.4,587.4,290.6,596.8,293.8,605.8z M287.8,579c-2.8-8.9,0.2-42.6,1.6-51.3C291.8,533.5,290.4,575.2,287.8,579z M304,489.3\nc-0.5,4.9-0.8,9.9-1.4,14.8c-1.1,10.3-2.1,20.5-3.7,30.7c-1.9,11.9-4.4,23.8-6.7,35.6c-0.3,1.5-0.9,3-1.9,4.4\nc0.5-4.4,1.3-8.8,1.5-13.1c0.4-10.5,1-21.1,0.4-31.5c-0.7-11.6-2.7-23.1-4.2-34.6c-0.9-6.6-2-13.1-3.1-19.7c-0.3-1.9,0.1-2.2,2-2\nc5.1,0.5,15.5-3.3,19.9-7.2C305.7,474.7,304.8,482,304,489.3z M307.6,456.4L307.6,456.4c0,0.9,0,1.6-0.1,2.4\nc-0.8,5.8-4.8,9.4-11.7,11.1c-8,2-16.1,1.9-24.2,1.8c-5.4-0.1-8,1.6-10,6.7c-0.8,2.1-1.5,4.2-2.2,6.3c-1.2-5-2-9.8-2.6-14.7\nc-0.5-4.2-0.6-8.5-1-12.7v-0.1c0-1.8-0.1-4.4-0.1-6.9c1-7.2,1.9-14.4,2.9-21.5l3.8-13.3c0.2-0.4,0.4-0.8,0.3-1.2\nc-0.4-2.9-0.8-5.8-1.4-8.7c-0.8-4.1,0-14.5,2-17.6c2.8,8.3,9.9,12.3,17.3,16.9c-9,2.6-11.9,10.3-17.5,16.3c0.7,0.2,1.3,0.3,1.8,0.4\nc-3.5,7.7-4.3,15.8-4,22.7c0.1,1,0.1,1.9,0.2,2.8c0.3,3,0.7,5.6,1.2,7.9c0.4,2.1,0.9,3.7,1.2,4.9c0,0,0,0,0,0c0,0.1,0.1,0.1,0.1,0.2\nc0.2,0.3,0.3,0.7,0.5,1c2.9,5.1,9.9,8.8,15.1,8c-5.8-1.1-11.5-3.9-14.7-12.2c0,0,0,0,0,0c-0.6-2.4-1-4.9-1.1-7.5\nc0.2,0.1,0.3,0.1,0.5,0.2c-0.2-0.6-0.4-1.3-0.5-1.8c0.7-0.3,1.8-0.9,2.7-1.5c0.1,0.1,0.2,0.2,0.3,0.3c2.5-2.7,5-5.5,7.5-8.3\nc0,0-0.1-0.1-0.1-0.1c3.3-2.7,9.5-8,12-10c-0.2-0.3-0.4-0.5-0.6-0.8c-2.8,2.2-10.3,8.7-13.1,10.9c0.1,0.1,0.2,0.2,0.2,0.3\nc-1.5,1.6-3,3.1-5.1,3.7h0c-0.7-0.1-1.7-0.1-2.5,0.1c-0.4-0.1-0.9-0.1-1.2-0.3c0.8-11.1,4.2-21.5,4.4-22.2c0,0,0,0,0,0\nc2.7-5.1,6-9.8,11.2-13c2.3-1.5,3.8-1,5.5,0.7c-1.5,0.4-2.8,0.7-4.1,1.1c3.3,0,6.4,0.3,9.5,0.8c1,0.2,1.8,1.8,2.7,1.8\nc0.7,0,1.3,0.2,1.9,0.4c0.8,0.3,1.5,0.9,2.1,1.5c1.3,0.9,2.6,2.3,3.8,3.8c3.9,5,6.7,12.2,7.9,15.6\nC307,441.4,307.3,450.9,307.6,456.4z M263.5,444.6c0.1-0.6,0.4-1.1,0.7-1.6c0.5-0.2,1.4-0.6,2.2-0.7c0,0,0,0,0,0\nc0.6,0.1,1.2,0.2,1.9,0.3c-0.7,1-1.2,1.8-1.8,2.6c-0.8,0.1-2.1,0.2-3,0.3c0,0-0.1,0-0.1,0C263.4,445.2,263.4,444.9,263.5,444.6\nC263.5,444.6,263.5,444.6,263.5,444.6z M311.5,466.8c6.4,5.7,14,6.3,21.9,7.3c-0.7,5.1-1.5,10.2-2.2,15.4\nc-1.4,9.8-2.6,19.7-4.2,29.4c-1.7,9.8-2.2,19.7-1.7,29.7c0.4,7.9,1.5,15.7,2.2,23.5c0.1,0.9,0.1,1.8,0.2,2.7\nC317.1,539.8,315.5,503.5,311.5,466.8z M333.6,474.6c0.8-0.3,1.5-0.5,2.2-0.5c0.1,0,0.2,0,0.4,0c0.1,0,0.2,0,0.3,0.1\nc0.1,0,0.1,0,0.2,0.1c0.2,0.1,0.3,0.1,0.5,0.2c0.1,0,0.1,0.1,0.2,0.1c0.1,0,0.1,0.1,0.2,0.2c0,0,0.1,0.1,0.1,0.1\nc0.1,0.1,0.3,0.3,0.4,0.5c0.1,0.1,0.1,0.1,0.1,0.2c0.1,0.1,0.1,0.2,0.2,0.3c-0.8-1.7,0.3-2.1,1.7-2.1c1.8,0,3.7,0.3,5.5,0\nc4.4-0.6,7.5,0.5,9.1,4.5c1.5,3.7,2.9,7.7,2.9,11.6c0,8.2-0.7,16.4-1.6,24.5c-1.1,9.4,0.6,18.6,1.5,27.9c0.2,1.9,0.5,3.7,0.8,5.4\nh0.7l-0.6,23.1c-0.2-0.4-0.4-0.8-0.5-1.2c-0.1-0.6-0.2-1.2-0.5-1.7c-1.7-3-3.7-5.9-5.4-9c-0.6-1.2-0.7-2.7-1-4\nc-1.2-4.2-2.6-8.4-3.5-12.7c-1-4.6-2.1-9.3-2-13.9c0.3-10.4,1.6-20.7-0.7-31c-1.7-7.3-4.2-14.4-6.3-21.6c0.1,0.5,0.2,0.9,0.3,1.4\nc0.2,1,0.4,2,0.6,3c0.1,0.5,0.2,1,0.3,1.5c0.4,1.9,0.8,3.9,1.3,5.8c4.4,16.8,4.1,33.6,0.1,50.5c-2.1,8.8-4.2,17.6-6.3,26.4\nc-0.2,1-0.6,2-1.2,3.9c-0.1-1.4-0.1-1.9-0.1-2.4c-0.4-11.1-0.5-22.1-1.4-33.1c-0.7-8.5-2.1-16.9-1.1-25.5c1-8.2,2-16.5,2.9-24.7\nC333.8,480,333.6,477.3,333.6,474.6z M348,556.9l-0.1,0.2l-5,11.7c0-0.3-0.1-0.7-0.2-1c-0.2,0-0.4,0-0.6-0.1\nc-0.4,1.6-0.7,3.2-1.2,5.3c-1.7-3-3.2-5.6-4.5-8.3c-0.4-0.8,0-2,0.2-2.9c1.5-5.3,3.1-10.6,4.6-16c0.8-2.9,1.5-5.8,2.2-8.8\nc0.9,1.2,1,2.3,1.3,3.3c0.8,2.6,1.4,5.4,2.4,7.9C348.4,551.2,349.8,553.8,348,556.9z M328.8,528.1c1.2,16.9,4.1,33.8,1.7,50.9\nC327.6,576.1,326,533.8,328.8,528.1z M329.1,639.7c0,0-0.3,0.1-0.3,0c-5.1-11.1-8.9-22.1-4.4-34.8c4.3-12.2,7.1-24.9,10.6-37.4\nc0.1-0.4,0.3-0.8,0.5-1.3c0.3,0.1,0.7,0.1,1,0.2c-0.4,3.2-0.8,6.3-1.2,9.5c-0.5,3.3-1,6.6-0.8,9.9c0.8-5.4,1.7-10.9,2.5-16.3\nc0.2,0,0.4-0.1,0.6-0.1c0.4,5.8,0.9,11.6,1.3,17.5v-12.5c0.2-0.1,0.3-0.1,0.5-0.2c0.7,2.9,1.9,5.8,2,8.7c0.6,12.1,1.2,24.2,1.1,36.2\nc-0.1,4.5-2.2,8.9-3.8,13.2c0.2-3.8,0.4-7.6,0.6-11.4c-0.7,6-0.6,12.2-4.7,18.1c0.6-5.4,1.1-9.9,1.6-14.5c-0.2,0-0.4-0.1-0.6-0.1\nc-0.7,5.3-1.4,10.5-2.1,15.8c-0.3,0-0.7-0.1-1-0.1v-12c-0.2,0-0.4,0-0.7,0c0,1.7,0,3.4,0,5.1c0,1.7-0.1,3.4-0.2,5.2\nc-0.1,1.6,1.3,3.7-1.3,4.7c-1.2-7-2.3-14-3.5-20.9c-0.3,0-0.5,0.1-0.8,0.1C327.1,628.3,328.1,634,329.1,639.7z M333.8,683.7h0.9\nv10.6c-0.3,0-0.6,0-0.9,0V683.7z M343,723.4c-1.9,1.4-6.7,1-8.7-0.6c-0.4-0.3-0.8-0.7-1.2-0.7c-2.5-0.1-4.3-1.7-4.2-4.2\nc0-0.7,0.2-1.4,0.3-2.1c0.5-0.7,1-1.5,1.6-2.3c1,1,2.5,2.2,4.4,2.7c0.1,0,0.2,0.1,0.2,0.1c0.1,0,0.2,0,0.2,0h0c0,0,0,0,0,0\nc0.6,0.1,1.1,0.1,1.7,0.1c2.9,0.1,5.8-0.2,8.9-0.3C347.2,719.7,345.6,721.4,343,723.4z M354.6,702c1,0.2,2.6,0.1,3.8,0.8\nc2.8,1.8,6.6,1.4,8.7,5c0.8,1.3,5.1,1.4,7.5,0.7c3.3-0.9,5.4,0.4,7.7,2.2c0.6,0.5,1.2,1,1.8,1.5c0.2,0.5,0.8,1.9,0.1,2.8\nc-1.5-0.2-3.1,0-4.2-0.8c-1.8-1.2-3.2-0.7-4.9,0c-1.6,0.7-3.3,1.3-5,1.8c-2.9,0.8-5.9,1.2-8.7,2.2c-4.9,1.7-9.7,3.7-14.5,5.6\nc-0.2-0.3-0.4-0.6-0.5-1c4.1-1.6,7.8-4.1,8.5-8.3C355.5,710.7,354.7,706.4,354.6,702z M351.8,701c0.8,6.8,3.9,13.7-3.5,18.8\nC346.4,710.7,347.5,704.5,351.8,701z M345.3,699.7c2.2-6.7,4.8-13.2,7.3-20.1c1.4,4.1,2.5,7.4,3.5,10.8c0.7,2.4-0.7,3.7-4.7,3.9\nc1.1,0.3,1.7,0.5,2.4,0.6c-2.9,3.4-5.7,6.8-8.6,10.3C345.3,703.5,344.8,701.4,345.3,699.7z M353.4,647.6c-3.1,11-6.4,22-9,33.2\nc-2.1,9.2-3.4,18.5,0.9,27.6c1.8,3.9,0.6,5.5-3.7,5.7c-2.4,0.1-4.8-0.7-7.3-1c-0.5-0.1-1-0.1-1.5-0.2c-0.4-0.2-0.9-0.6-1.2-1.1\nc0.2-0.4,0.4-0.7,0.5-1c0.1,0.1,0.2,0.2,0.3,0.3c1.7-4.9,2.9-9.5,3.3-14.3c0.6-5.9,1.3-12,0.6-17.9c-1.2-10.4-3.5-20.7-5.2-31\nc-0.2-1.2-0.1-2.9,0.6-3.8c3.1-3.9,6.5-7.6,9.9-11.5c2.8,7.1,8.7,7.5,15,7.3C355.7,642.7,354.2,645.1,353.4,647.6z M364.8,600.6\nc2.5,4.6,3.7,9.5,3.4,14.6c-0.2,5.2-1.1,10.3-1.8,15.4c-0.3-0.1-0.7-0.1-1-0.1c0.4-3.1,0.8-6.2,1.2-9.3c-1.1,2.9-1.4,5.9-2,8.9\nc-0.8,4-2.2,5.8-4.5,6.3c1.1-4.2,2.2-8.7,3.4-13.3c-1.7,4-2.8,8.2-4.4,12.2c-1,2.5-1.8,2.5-4.5,2c3-4.1,4.4-8.7,4.3-13\nc-1.2,3.4-2.6,7.4-4.1,11.3c-0.8,2-2.3,2.7-4.4,2l0,0c-4.9-0.4-8.3-4.4-7.4-8.2c1-4.2,2.2-8.5,2.1-12.8c-0.1-10.2-0.8-20.4-1.5-30.6\nc-0.3-3.9-0.9-7.8-1.1-11.8l8-15.7c-0.2-0.4-0.4-0.7-0.6-1.1l0.9,0.3l-0.4,0.7c1.8,3.6,2.3,7.1,0,10.9h0.9c0.4-1.6,0.9-3.2,1.3-5\nc0.7,1.2,1.3,2.3,2,3.5l2.9,8.7l0.2,0.7c0-0.1,0-0.1,0-0.2c0.1,0.3,0.1,0.5,0.2,0.8l0,0.1C359.8,585.9,361,593.6,364.8,600.6z\nM363.9,466.8c0.4-4.3,0.4-8.7,0.6-13.1c0.1-1,0-1.9,0-3.7c3.6,10.3,4.8,20.1,4,30.1c-0.6,7.9-1.7,15.7-2.6,23.5\nc-0.7,5.9-1.5,11.8-2.2,17.7c-0.5,3.8-1.1,7.5-2.4,11.2c-0.1-1.1-0.1-2.1-0.2-3.2c-0.1,0-0.3,0-0.4,0v16.9c-0.3,0-0.5,0.1-0.8,0.1\nc0-0.2-0.1-0.4-0.1-0.6c-0.3-2.5-0.6-4.9-1-7.4c-2-13.6-0.1-27.1,0.3-40.7C359.4,487.2,362.8,477.2,363.9,466.8z M358.5,484.7\nc-0.5-1.7-0.9-3.4-1.5-5c-2.3-6.2-4.8-7.7-11.4-8c-6.9-0.2-13.8-0.3-20.6-1.3c-7.2-1-13.5-3.8-14.6-12.4c-0.1-0.5-0.2-1.1-0.4-1.6\nc-3.1-28.1,11-43.6,11-43.6c2.4-1.2,4.6-3.1,7.1-3.8c2.9-0.8,6-0.8,9-1c-1.2-0.3-2.5-0.5-4-0.9c1.9-1.4,3.1-2.4,5.5-0.8\nc6.3,4.2,10.5,10,12.9,17.1c0.5,1.5-0.2,3.7,0.6,4.8c2.9,3.9,1.1,8.5,2.4,12.6c0.5,1.7-0.7,1.8-2,1.3c2.4,1.2,2.4,1.2,1.9,6.1\nc-1.7-2-3.2-3.8-4.9-5.8c1.4-0.1,2.3-0.1,3.1-0.2c-1.7-0.7-3.7-0.9-5.1-2c-3-2.5-5.6-5.6-8.6-8.2c-3-2.6-6.3-4.8-9.5-7.1\nc-0.3,0.4-0.7,0.8-1,1.2c0.4,0.2,1,0.2,1.3,0.5c7.9,5.1,14.7,11.5,20.6,18.7c2.1,2.5,5.1,4.9,3.5,8.9c-0.3,0.9-1.2,1.7-1.1,2.5\nc0.3,3.2-2.1,5-3.7,7c-1.2,1.5-3,2.6-4.7,3.5c-1.7,0.9-3.7,1.2-6.3,2c6.3,0.1,10.8-2.2,14.2-6.4c4.5-5.5,4.6-12.4,4.5-19.1\nc-0.1-6-0.3-12-0.4-18c0-0.8,0.2-1.7,0.3-2.9C362.8,432.7,363.9,476.4,358.5,484.7z M356.7,405.1c-0.2,1.5-0.3,3-0.6,4.5\nc-0.6,2.8-1.5,5.5-2.2,8.3c-0.3,1.1-0.2,2.3-0.3,3.7c-3.7-7.2-7.6-13.9-16-16.6c3.4-2.4,6.5-4.2,9.2-6.5c2-1.7,3.5-4,5.3-6.1\nl-0.1,0.1c-7,6.7-15.1,11.4-24.2,14.8c-3.4,1.3-6.5,3.3-9.8,5c-3.7,2.7-6.2,7.2-7.8,12.5c-1.7-4.6-5.6-8.2-8.5-10.5\nc-3.3-3.4-7.5-5.7-12.1-7.4c-8.2-3.1-15.9-7.1-22.1-13.5c-0.9-0.9-1.6-2.1-2.3-3.2c6.3,6,13.4,10.2,21.2,13.5\nc5.5,2.4,11.1,4.8,16.3,8c4.6,2.8,8.4,2.6,12.9-0.1c5-3,10.5-5.1,15.7-7.8c4.9-2.5,9.8-5.1,14.5-8c2.5-1.5,4.5-3.8,7-6.1\nc-0.4,1.2-0.6,1.9-0.9,2.7c1-1.5,1.9-3,3-4.7C356.7,393.5,357.5,399.2,356.7,405.1z M353.1,375.9c0.3,4.4,0.5,9.4-3.2,13\nc-2.5,2.5-5.3,4.7-8.6,7.6c-0.6-14.9,2-27.9,8.7-40.2C351.2,363,352.7,369.4,353.1,375.9z M367.9,305.2c-0.4,3.5-1.2,8.5-2.9,14\nc-4.6,12.2-10.6,23.8-16.7,35.3c-5.8,10.9-9.2,22.7-8,35.3c0.2,2.4,0,4.8,0,7.2c-5.4-7.2-8.1-14.9-7-23.6c-0.1,0.3-0.3,0.7-0.5,1.3\nc-0.2-0.5-0.3-0.8-0.3-1c-0.3-6.9-2.9-12.9-6.9-18.3c-2.5-3.5-5.4-6.8-8.2-10.2c-3-3.7-6.1-7.4-5-12.5c0.6-2.9,1.6-5.9,3.2-8.4\nc2.1-3.4,4.7-6.5,7.3-9.4c2.7-3.1,5.7-5.9,8.8-8.7c0.6-0.6,2.2-0.8,3-0.5c6.3,2.6,12.7,3,19.3,1.4c4.5-1.1,8.9-2.5,13.8-3.8V305.2z\nM371.7,296.2c-3,2.4-5.8,5.1-9.2,6.5c-4.1,1.8-9,1.9-13.5,2.9c-4.1,0.8-8.9,0.5-11.8-0.9c3.9-1.4,15.4-9.7,18.3-14.8\nc3-5.3,11.6-16.7,11.6-16.7s0-0.3-0.1-0.6c0.7-0.7,1.3-1.4,2-2c3.6-3.2,7.8-3.5,12.8-2.4c-2.1,2.4-3.8,4.8-4.2,7.5\nC374.7,282.5,372.6,292.1,371.7,296.2z M385.9,317.7c-5.9-5.6-10.3-17.6-11.5-21.2c0-10.4,7.7-24.6,9.4-27.5c1-0.1,2.2,0.1,3.6,0.6\nc1.4,0.5,2.8,1.2,4.3,1.6c5.2,1.5,9.1,4.5,11.7,9.4c1.3,2.5,2.8,5,4.2,7.5c1.3,2.9,4.1,8.9,6.9,15.2c0.3,0.6,0.5,1.2,0.8,1.8\nc4,9.1,8,18.1,8,18.1s0,0,0.1,0c1,2.3,2,4.6,2.9,6.9c0.8,2.1,1.3,4.3,1.9,6.4h-6.8c-0.8-1.2-1.3-2.9-2.4-3.4\nc-6.5-3.2-13.1-6-19.6-9.1c-3.9-1.8-8.2-2.9-11.5-6C387.3,317.6,386.6,317.6,385.9,317.7z M387.6,321.2c-0.1-0.1-0.1-0.2-0.2-0.3\nc0.1,0,0.1,0.1,0.2,0.1v0c10.2,6.9,23.9,6.7,32.2,15.3c-1.5,4.8-2.8,8.9-4,12.9C407.2,339.4,394.9,332.9,387.6,321.2z M396.2,389.6\nc-0.4-1-0.7-1.9-1.1-2.9c-1.9-5.6-3.9-11.2-5.7-16.8c-2-6.4-0.9-12.6,1.6-18.8c1.9-4.7,3.1-9.7,4.7-14.6c0.2-0.4,0.3-0.9,0.7-1.7\nc1.6,3.4,2.7,6.3,1.8,9.8c-0.4,1.5,0.4,3.4,0,5c-0.5,2.6-1.3,5.3-2.3,7.8c-2.6,6.1-2.5,12.4-1.4,18.7c0.8,4.4,1.7,8.9,2.5,13.3\nC396.7,389.4,396.5,389.5,396.2,389.6z M397.2,359.8c1.5,3.8,2.8,7.4,4.1,10.9c0.2,0.5,0,1.2-0.2,1.7c-1.1,2.7-2.2,5.3-3.3,7.9\nC395.8,377.1,395.4,366.7,397.2,359.8z M419.1,372.4c-0.6,3.9-2.2,7.5-3.4,11.3c-0.2-0.1-0.5-0.1-0.7-0.2v-3.8\nc-0.2,8.4-5.3,16.1-3.7,24.7c0.2,0.9-1.1,2.5-2.2,3.2c-0.5,0.4-2.1-0.1-2.8-0.7c-2.2-1.8-4.2-3.8-6.1-5.9c-0.6-0.7-1-1.9-0.9-2.8\nc0.7-7.6-0.2-15.4,2.3-22.9c0.2-0.6,0.5-1.3,0.9-2.2c6.5,9.4,6.9,19.6,6.1,30.2c0.5-3,1.3-5.9,1.5-8.9c0.6-7.8-1-15.2-5.3-21.9\nc-5.3-8.2-5.1-17.2-4-26.4c0.3-2.4,0-4.8,0-7.8c5.3,4.7,10.9,8.9,15.5,14.2c2,2.2,1.8,6.5,2.3,9.8\nC419,365.7,419.6,369.2,419.1,372.4z M428.3,342c-3.7,6.9-4.2,14.6-6.4,21.9c-0.1,0.3-0.3,0.5-0.7,1.4c-1-5.9-2.1-11.1-2.6-16.5\nc-0.2-2.7,0.7-5.5,1.5-8.2c0.3-1,1.4-2.4,2.3-2.6c1.8-0.4,3.8-0.5,5.5,0C428.4,338.1,428.9,340.9,428.3,342z\"/>\n<path class=\"st0\" (click)=\"onClickPathBack($event)\" d=\"M282.3,192.7c-2.7-1.6-2.8-1.7-4.7-6.8c-0.2-0.5-0.6-0.9-1.1-1.3c-0.5-1.9-0.7-4.1-0.8-5.9\nc0.1-1.9,0.3-3.7,0.4-5.5c0.3-0.2,0.6-0.4,0.9-0.5c1.2,0.9,2.5,1.9,3.7,2.8c0.7,3,1.5,5.6,2.1,7.5c0,0,0,0,0,0.1\nc0.2,1.3,0.5,2.6,0.9,3.9C284.5,189.3,283.8,191,282.3,192.7z\"/>\n<path class=\"st0\" (click)=\"onClickPathBack($event)\" d=\"M235.7,426.8c-0.3,0.1-0.6,0.3-0.9,0.4c-0.1,0-0.2-0.1-0.3-0.1l-5.3-7.1l-0.2-0.2c-1.2-1.7-2.4-3.4-3.5-5\nc0.4-0.2,0.9-0.3,1.4-0.3h0c1.4-0.1,3.2,0.1,4.9,0.4l1.1,2.8c0.3,1.2,0.6,2.4,1,3.6C234.6,423,235.2,424.9,235.7,426.8z\"/>\n<path class=\"st0\" (click)=\"onClickPathBack($event)\" d=\"M233,430.2c-0.9,1.7-2.6,2.8-3.7,3.3c0-0.1,0-0.1,0-0.2c0.1-1.1-0.6-3.4-1.2-5c0,0,0,0,0,0\nc-0.3-2.1-1.3-3.7-2.7-5.2c-1.7-1.7-1.7-3.2-0.2-5.2c1.9,2.5,2.6,3.3,3.4,3.6C230,423.1,234,428.1,233,430.2z\"/>\n<path class=\"st0\" (click)=\"onClickPathBack($event)\" d=\"M243.6,419.7c-0.8,0-3.7-2.9-3.7-2.9l-2.8-2l-0.7-0.5c-1.1-0.9-2-2.4-3.5-1.5c0,0,0,0,0,0\nc-2.2-0.5-4.2,0.1-5,0.4c-1.9-1.8-3.7-3.5-5.4-5.1c-0.4,1-0.4,1.8,0,2.3c2.7,3.5,0.5,6.8-1.6,8.8c-2,2-2.9,3.8-3.1,6.5\nc-0.4,4.3,0.1,5,4.8,4.3c-1-1.7-2.2-3.2-2.9-5c-0.7-1.9-0.5-3.8,2-4.5c1.9,3.6,3.7,7,5.7,10.8c0,0,0,0,0,0c0.2,0.9,0.3,1.9-0.1,2.6\nc-2.1,0.4-4.2,0.8-6.3,1.2c-2.8,0.5-6-1.2-6.9-3.7c-1.6-4.4-3.2-8.9-4.6-13.3c-1-3.1-1.7-6.3-2.7-10c3.3,2.4,5.6,0.8,8-0.8\nc3.6-2.2,5.4-5.7,7.3-9.3l1.2-0.1c2.8,0.9,5.5,1.9,8.6,2c1.8,0.1,5.1,2.6,4.9,3.3c-0.6,2.7,1.7,3.5,2.5,4.9c0.6,1,1.4,1.9,2.4,2.7v0\nC243.7,413.9,244.3,419.7,243.6,419.7z\"/>\n<path class=\"st0\" (click)=\"onClickPathBack($event)\" d=\"M343,723.4c-1.9,1.4-6.7,1-8.7-0.6c-0.4-0.3-0.8-0.7-1.2-0.7c-2.5-0.1-4.3-1.7-4.2-4.2c0-0.7,0.2-1.4,0.3-2.1\nc0.5-0.7,1-1.5,1.6-2.3c1,1,2.5,2.2,4.4,2.7c0.1,0,0.2,0.1,0.2,0.1c0.1,0,0.2,0,0.2,0h0c0,0,0,0,0,0c0.2,0.1,0.5,0.1,0.8,0.1\nc0.4,0,0.7,0,1,0c2.9,0.1,5.8-0.2,8.9-0.3C347.2,719.7,345.6,721.4,343,723.4z\"/>\n<path class=\"st0\" (click)=\"onClickPathBack($event)\" d=\"M384.2,715c-1.5-0.2-3.1,0-4.2-0.8c-1.8-1.2-3.2-0.7-4.9,0c-1.6,0.7-3.3,1.3-5,1.8c-2.9,0.8-5.9,1.2-8.7,2.2\nc-4.9,1.7-9.7,3.7-14.5,5.6c-0.2-0.3-0.4-0.6-0.5-1c4.1-1.6,7.8-4.1,8.5-8.3c0.7-4-0.1-8.2-0.3-12.7c1,0.2,2.6,0.1,3.8,0.8\nc2.8,1.8,6.6,1.4,8.7,5c0.8,1.3,5.1,1.4,7.5,0.7c3.3-0.9,5.4,0.4,7.7,2.2c0.6,0.5,1.2,1,1.8,1.5C384.3,712.7,384.8,714.1,384.2,715z\n\"/>\n<path class=\"st0\" (click)=\"onClickPathBack($event)\" d=\"M284.4,694c-0.1,0.7-0.2,1.4-0.2,2c-0.5-2.6-0.8-5.1-1.1-7v-5.4c0.4,0,0.8,0,1.2,0V694\nC284.3,694,284.3,694,284.4,694z\"/>\n<path class=\"st0\" (click)=\"onClickPathBack($event)\" d=\"M289.5,720.8c-8.1,7-13.7,4.7-15.7,3.3c-1.3-2.9-3.1-5.5-2-8.8c6.9,2.6,11.4,1.7,14.8-2.6\nc0.8,1.3,1.4,2,1.5,2.1c0.3,1.4,0.6,2.8,0.9,4.2C289.2,719.6,289.4,720.2,289.5,720.8z\"/>\n<path class=\"st0\" (click)=\"onClickPathBack($event)\" d=\"M270.5,722.1c-3.6-2.5-4.9-9.7-5.4-14.1l0,0c0.4-2.4,0.7-4.7,1.1-7.1l1.9,1.6c1.4,1.5,2.1,3.3,2.1,5.4\nc0,1.7,0,3.5,0,5.2C270.3,716.2,270.4,719.1,270.5,722.1z\"/>\n<path class=\"st0\" (click)=\"onClickPathBack($event)\" d=\"M260.5,719.8c0,0,0.1-0.1,0.1-0.1c-3.7-1.3-7-2.5-10.4-3.6c-2.4-0.8-5-1.2-7.4-1.9c-1.3-0.4-2.2-1.1-3.8-0.3\nc-0.6,0.3-1.3,0.5-2.1,0.6c1-0.6-0.9-1.4-0.9-1.4s-1.1-2,2-2.4c1.1-0.2,3.4-1.1,6-2.2h0c1.3-0.5,2.6-1.1,3.9-1.6h0\nc2.5-1.1,4.9-2,6.2-2.4c0.3-0.1,0.5-0.1,0.6-0.2c0,0,0,0,0,0c0.1,0,0.2,0,0.2,0c1.8,0,4.7,0,6.3-0.9c1-0.6,2.1-1.5,2.8-2.1\nc-0.3,1.9-0.4,3.1-0.4,4c-0.1,0-0.1,0.1-0.2,0.1c-0.8,6.2,2.2,13,3.7,16.1L260.5,719.8z\"/>\n<path class=\"st0\" (click)=\"onClickPathBack($event)\" d=\"M344.5,680.8c-2.1,9.2-3.4,18.5,0.9,27.6c1.8,3.9,0.6,5.5-3.7,5.7c-2.4,0.1-4.8-0.7-7.3-1\nc-0.5-0.1-1-0.1-1.5-0.2c-0.4-0.2-0.9-0.6-1.2-1.1c0.2-0.4,0.4-0.7,0.5-1c0.1,0.1,0.2,0.2,0.3,0.3c1.7-4.9,2.9-9.5,3.3-14.3\nc0.6-5.9,1.3-12,0.6-17.9c-1.2-10.4-3.5-20.7-5.2-31c-0.2-1.2-0.1-2.9,0.6-3.8c3.1-3.9,6.5-7.6,9.9-11.5c2.8,7.1,8.7,7.5,15,7.3\nc-1.2,2.6-2.7,5-3.4,7.5C350.3,658.7,347,669.7,344.5,680.8z M281.5,689.8c0.2-4.9,0.3-9.9,0.9-14.8c0.6-5.9,1.7-11.8,2.6-17.6\nc0.4-2.8,1-5.6,1.6-8.3c0.6-2.6,0.2-4.7-1.7-6.7c-2.9-3-5.6-6.1-8.5-9.4c-2.8,6.5-8.5,7.5-14.4,7c2.8,8.9,6.1,17.8,8.5,26.8\nc2.2,8.4,3.4,17.1,4.9,25.8c0.3,1.9,0.3,2.6-0.5,3.4c-0.4,3.8-1.4,14.4-1.6,17.3v0c0.6,0.5,1.3,0.7,2.3,0.7c2.8,0,5.7-0.6,8.5-1\nc0.5-0.1,1.6-0.6,2.6-0.9c-0.6-1.1-1.4-2.5-2.1-4.3c-0.3-0.6-0.5-1.3-0.8-2c0,0,0,0,0,0c-1.3-3.9-2.3-9.1-2.1-15.6L281.5,689.8z\"/>\n<path class=\"st0\" (click)=\"onClickPathBack($event)\" d=\"M348,556.9l-0.1,0.2l-5,11.7c0-0.3-0.1-0.7-0.2-1c-0.2,0-0.4,0-0.6-0.1c-0.4,1.6-0.7,3.2-1.2,5.3\nc-1.7-3-3.2-5.6-4.5-8.3c-0.4-0.8,0-2,0.2-2.9c1.5-5.3,3.1-10.6,4.6-16c0.8-2.9,1.5-5.8,2.2-8.8c0.9,1.2,1,2.3,1.3,3.3\nc0.8,2.6,1.4,5.4,2.4,7.9C348.4,551.2,349.8,553.8,348,556.9z\"/>\n<path class=\"st0\" (click)=\"onClickPathBack($event)\" d=\"M352.8,389.8c-0.4,1.2-0.6,1.9-0.9,2.7c-7,6.7-15.1,11.4-24.2,14.8c-3.4,1.3-6.5,3.3-9.8,5\nc-3.7,2.7-6.2,7.2-7.8,12.5c-1.7-4.6-5.6-8.2-8.5-10.5c-3.3-3.4-7.5-5.7-12.1-7.4c-8.2-3.1-15.9-7.1-22.1-13.5\nc-0.9-0.9-1.6-2.1-2.3-3.2c6.3,6,13.4,10.2,21.2,13.5c5.5,2.4,11.1,4.8,16.3,8c4.6,2.8,8.4,2.6,12.9-0.1c5-3,10.5-5.1,15.7-7.8\nc4.9-2.5,9.8-5.1,14.5-8C348.2,394.3,350.2,392,352.8,389.8z\"/>\n<path class=\"st0\" (click)=\"onClickPathBack($event)\" d=\"M392.8,423c-1.7,2-2.6,4.7-4,7.1c-0.4,0.6-1.3,0.9-2,1.3c-0.4-0.9-1.3-1.8-1.2-2.6c0.2-1.1,1-2.2,1.7-3.1\nc1.8-2.6,3.7-5.1,5.7-7.8C394.3,419.8,394.3,421.1,392.8,423z\"/>\n<path class=\"st0\" (click)=\"onClickPathBack($event)\" d=\"M385.1,414.8c-1.3,2.1-2.6,4.1-3.9,6.3C379.9,418.8,381.1,416.6,385.1,414.8z\"/>\n<path class=\"st0\" (click)=\"onClickPathBack($event)\" d=\"M388.9,419.8c-1.8,2.4-3.5,4.9-5.2,7.3c0,0.1-0.2,0-0.3,0c-2-3.4-0.7-3.8,4.6-12.4\nC389.9,416.1,390.5,417.7,388.9,419.8z\"/>\n<path class=\"st0\" (click)=\"onClickPathBack($event)\" d=\"M411.5,407.7c-1.6,4.9-3,9-4.3,13c-1,3.1-2.2,6.2-3.1,9.4c-1.2,4.1-4.1,5.7-8.1,4.1c-0.4-0.2-1.1,0.1-1.6,0.2\nc-3.1,0.2-5-0.7-3.3-4.1c1.6-3.2,3.3-6.4,5-9.8c2.5,0.4,2.8,2.2,2.2,4.2c-0.6,1.8-1.8,3.3-2.7,5c4.4,1.2,5.3,0.4,4.5-3.6\nc-0.2-1,0-2-0.2-3c-0.2-0.9-0.7-1.8-1.2-2.5c-0.9-1.3-2.2-2.3-3-3.6c-1.3-2.2-1.8-4.6,0-6.9c0.3-0.4,0.3-1.1,0.4-1.7\nc-3.4,0.2-3.7,6.7-8.1,3.8c-1.8,0.4-3.7,0.5-5.2,1.3c-3.5,1.9-5.3,5-5.5,9.2c-0.1,2-0.4,4.1-4,4.8c-0.3-4.1-0.8-8-0.9-11.8\nc0-0.9,1-2,1.8-2.7c2.9-2.7,6.2-5,6.9-9.4c0.1-0.7,1.7-1.2,2.5-1.7c-0.1-0.2-0.2-1.1-0.2-1.2c1.3-0.4,2.7-0.9,4-0.9\nc3.2,0.1,5.9-1.1,8.4-2.7c1.3,4.4,3.8,7.8,7.6,10.3C405.7,408.9,408,410.3,411.5,407.7z\"/>\n<path class=\"st0\" (click)=\"onClickPathBack($event)\" d=\"M358.5,484.7c-0.5-1.7-0.9-3.4-1.5-5c-2.3-6.2-4.8-7.7-11.4-8c-6.9-0.2-13.8-0.3-20.6-1.3\nc-7.2-1-13.5-3.8-14.6-12.4c-0.1-0.5-0.2-1.1-0.4-1.6c-3.1-28.1,11-43.6,11-43.6c2.4-1.2,4.6-3.1,7.1-3.8c2.9-0.8,6-0.8,9-1\nc0.3,0.1,0.6,0.1,0.9,0.2c0-0.1-0.1-0.2-0.1-0.3c-0.3,0-0.5,0.1-0.8,0.1c-1.2-0.3-2.5-0.5-4-0.9c1.9-1.4,3.1-2.4,5.5-0.8\nc6.3,4.2,10.5,10,12.9,17.1c0.5,1.5-0.2,3.7,0.6,4.8c2.9,3.9,1.1,8.5,2.4,12.6c0.5,1.7-0.7,1.8-2,1.3c2.4,1.2,2.4,1.2,1.9,6.1\nc-1.7-2-3.2-3.8-4.9-5.8c1.4-0.1,2.3-0.1,3.1-0.2c-1.7-0.7-3.7-0.9-5.1-2c-3-2.5-5.6-5.6-8.6-8.2c-3-2.6-6.3-4.8-9.5-7.1\nc-0.3,0.4-0.7,0.8-1,1.2c0.4,0.2,1,0.2,1.3,0.5c7.9,5.1,14.7,11.5,20.6,18.7c2.1,2.5,5.1,4.9,3.5,8.9c-0.3,0.9-1.2,1.7-1.1,2.5\nc0.3,3.2-2.1,5-3.7,7c-1.2,1.5-3,2.6-4.7,3.5c-1.7,0.9-3.7,1.2-6.3,2c6.3,0.1,10.8-2.2,14.2-6.4c4.5-5.5,4.6-12.4,4.5-19.1\nc-0.1-6-0.3-12-0.4-18c0-0.8,0.2-1.7,0.3-2.9C362.8,432.7,363.9,476.4,358.5,484.7z M308.5,432.3c-1.2-3.4-4-10.6-7.9-15.6\nc-1.2-1.5-2.4-2.8-3.8-3.8c-0.6-0.6-1.3-1.2-2.1-1.5c-0.6-0.2-1.2-0.4-1.9-0.4c-0.9-0.1-1.7-1.6-2.7-1.8c-3.1-0.6-6.2-0.8-9.5-0.8\nc1.3-0.4,2.6-0.7,4.1-1.1c-1.7-1.7-3.2-2.2-5.5-0.7c-5.1,3.2-8.4,7.9-11.2,13c0,0,0,0,0,0c-0.2,0.7-3.6,11.1-4.4,22.2\nc0.3,0.2,0.8,0.2,1.2,0.3c0.8-0.2,1.8-0.2,2.5-0.1h0c2-0.5,3.6-2.1,5.1-3.7c-0.1-0.1-0.2-0.2-0.2-0.3c2.8-2.2,10.3-8.7,13.1-10.9\nc0.2,0.3,0.4,0.5,0.6,0.8c-2.5,2-8.7,7.3-12,10c0,0,0.1,0.1,0.1,0.1c-2.5,2.7-5,5.5-7.5,8.3c-0.1-0.1-0.2-0.2-0.3-0.3\nc-0.9,0.6-2,1.2-2.7,1.5c0.1,0.6,0.3,1.2,0.5,1.8c-0.2-0.1-0.3-0.1-0.5-0.2c0.1,2.6,0.5,5.2,1.1,7.5c0,0,0,0,0,0\nc3.2,8.3,8.9,11.2,14.7,12.2c-5.2,0.9-12.2-2.9-15.1-8c-0.2-0.3-0.4-0.7-0.5-1c0-0.1-0.1-0.1-0.1-0.2c0,0,0,0,0,0\nc-0.3-1.1-0.8-2.8-1.2-4.9c-0.5-2.2-0.9-4.9-1.2-7.9c-0.1-0.9-0.2-1.9-0.2-2.8c-0.3-6.9,0.4-15,4-22.7c-0.5,0-1.1-0.2-1.8-0.4\nc5.6-6,8.6-13.7,17.5-16.3c-7.3-4.7-14.5-8.7-17.3-16.9c-2,3.1-2.8,13.5-2,17.6c0.6,2.9,1,5.8,1.4,8.7c0.1,0.4-0.1,0.8-0.3,1.2\nl-3.8,13.3c-0.9,7.1-1.9,14.2-2.9,21.5c0,2.5,0.1,5.1,0.1,6.9v0.1c0.4,4.2,0.5,8.5,1,12.7c0.5,4.9,1.4,9.7,2.6,14.7\nc0.7-2.1,1.4-4.2,2.2-6.3c2-5,4.6-6.7,10-6.7c8.1,0.1,16.2,0.1,24.2-1.8c6.9-1.7,10.9-5.4,11.7-11.1c0.1-0.8,0.1-1.6,0.1-2.3v-0.1\nC307.3,450.9,307,441.4,308.5,432.3z\"/>\n<path class=\"st0\" (click)=\"onClickPathBack($event)\" d=\"M318.5,405.7c-0.6,4.9-7.9,8.4-13,6.3c-5.2-2.2-7.6-6.7-6.2-12.6c1.3-5.2,3.5-10.1,4.8-15.2\nc1.4-5.6,2.5-11.4,3.3-17.1c0.7-5.5,0.9-11.1,1.2-16.6c0.2,0,0.4,0,0.6,0c0.4,5,1,10,1.2,15.1c0.3,9.4,2.8,18.2,6.5,26.8\nC318.8,396.7,319,401.1,318.5,405.7z\"/>\n<path class=\"st0\" (click)=\"onClickPathBack($event)\" d=\"M326.6,247.6c-4.6,4.2-8.2,9.2-9.8,15.1c-1.9,7-3.6,14.1-4.3,21.3c-1.1,12-1.4,24.1-2,36.2\nc-0.2,4-0.4,7.9-0.7,11.9c-0.1,0.9-0.4,1.7-0.6,2.5c-0.2,0-0.4,0-0.5,0c-0.2-0.8-0.5-1.5-0.5-2.3c-0.5-10.9-0.8-21.7-1.3-32.6\nc-0.7-12.2-1.3-24.4-5.4-36.2c-2.1-6-5.2-11.3-9.7-15.5c3.6-4.9,7.5-9.3,10.4-14.3c2.9-4.9,4.8-10.3,7.3-15.6\nC311.5,229.7,318.6,238.7,326.6,247.6z\"/>\n<path class=\"st0\" (click)=\"onClickPathBack($event)\" d=\"M361.6,274.8c0.1,0.1,0.1,0.2,0.2,0.3c-1.1,1.5-2.3,3.4-3.6,5.6c0,0,0,0,0,0c-0.5,0.5-0.8,1.3-1.1,1.9\nc0,0,0,0.1,0,0.1c0,0.1-0.1,0.1-0.1,0.2c-1.1,2-2.3,4.1-3.5,6.4c-3.2,6-12.6,11-16.9,13.2c-0.5,0.2-1,0.5-1.3,0.6\nc-0.2,0.1-0.5,0.2-0.6,0.3c-0.2,0.1-0.4,0.2-0.4,0.2c2-3.4,3.4-5.9,4.9-8.4c0.9-1.5,2.1-2.8,2.7-4.4c0.6-1.5,1-2.9,1.4-4.4l4.4-13.8\nl-0.1-0.1c0.3-1.3,0.3-2.7,1-3.8c1.6-2.5,1.1-5.5,2.2-8.2c6.8,2.1,13.5,4.1,19.8,6C367.6,269.2,364.6,272,361.6,274.8z\"/>\n<path class=\"st0\" (click)=\"onClickPathBack($event)\" d=\"M381.8,268.2c-2.1,2.4-3.8,4.8-4.2,7.5c-2.9,6.8-5,16.3-5.9,20.5c-3,2.4-5.8,5.1-9.2,6.5\nc-4.1,1.8-9,1.9-13.5,2.9c-4.1,0.8-8.9,0.5-11.8-0.9c3.9-1.4,15.4-9.7,18.3-14.8c3-5.3,11.6-16.7,11.6-16.7s0-0.3-0.1-0.6\nc0.7-0.7,1.3-1.4,2-2C372.7,267.5,376.9,267.2,381.8,268.2z\"/>\n<path class=\"st0\" (click)=\"onClickPathBack($event)\" d=\"M369.9,232.6c-0.2,0.4-0.2,0.8-0.4,0.9c-4.6,4.4-9.4,8.6-13.9,13.1c-2.1,2.1-4.3,4.5-5.2,7.3\nc-1.9,5.4-3.3,11-4.9,16.8l-0.1-0.1c0,0,0,0.3-0.1,1c-0.4,1.4-0.8,2.7-1.1,4.1c0.1,0,0.2,0,0.2-0.1c-0.4,1.5-1,3.3-1.8,5.4\nc0,0,0,0,0,0.1c-0.4,0.8-0.6,1.2-0.8,1.7c-1.1,2.2-2.1,4.3-3.1,6.5c-0.3,0.5-0.5,0.9-0.8,1.4c0.1,0,0.1,0,0.1,0\nc-1.6,3.5-3.3,6.9-5.3,10.1c-6.1,9.4-15.9,16-20.7,26.5c0.2-11.9,0.3-23.7,1.2-35.4c0.6-8,1.8-16,3.5-23.9c1.7-7.9,5.3-15,12-20.4\nc-11.1-11.1-19-23.4-19.6-34.3c0.1-0.9,0.1-1.7,0-2.6c-0.1,0.8-0.1,1.7,0,2.6c-0.6,11.1-8.6,23.3-19.1,33.9c2.6,3.5,5.3,6.5,7.3,9.9\nc5.8,10.1,6.9,21.5,7.8,32.7c0.7,8.5,0.5,17.1,0.8,25.6c0.1,4,0.5,8,0.7,11.4c-3.9-4.9-7.5-10.5-12.1-15.1\nc-8.6-8.7-14.8-18.8-18.5-30.3c-2.7-8.3-5-16.8-7.4-25.2c-1.2-4.2-3.4-7.4-6.7-10.2c-4.8-4-9.4-8.3-14.4-12.8\nc6.3-5.3,12.8-8.1,20-9.4c7.4-1.3,14.8-2.1,22.1-3.8c7.5-1.7,13.8-5.8,17.3-13c1.9-4,1.7-8.3-0.2-12.3c-1.3-2.7-4.2-1.9-6.5-2.3\nc-0.1,0-0.2-0.3-0.3-0.5c4.2-2.6,6.1-4.3,6.7-6.4c0.9-0.2,2.7-0.5,4.4-0.1c0,0.1-0.1,0.1-0.1,0.2c2.4,2.3,4.7,4.5,7.1,6.8\nc-0.4,0.1-1.1,0.3-1.7,0.3c-3.4-0.1-4.9,1.7-5.8,4.9c-1.2,4.8,0,9,2.8,12.8c3.8,5.2,9.1,8.4,15.3,9.7c7.3,1.6,14.8,2.4,22.1,3.8\nC357.7,225.1,364.3,227.9,369.9,232.6z\"/>\n<path class=\"st0\" (click)=\"onClickPathBack($event)\" d=\"M336,176.4C336,176.4,336,176.3,336,176.4c-1.2,3.2-2.3,6.5-3.6,9.7c-2.2,5.2-7.2,7-12.1,4.2\nc-2.4-1.4-4.5-3.3-6.8-5c-0.4-0.3-0.9-0.5-1.4-0.7c-1.5-1.1-3.4-2.1-5.4-1.4c0-0.3-0.1-0.7-0.1-1c-1.2,1.6-2.1,3.3-3.5,4.5\nc-1.5,1.3-3.3,2.4-5.1,3.3c-5.5,2.9-10.2,1.2-12.5-4.7c-0.7-1.7-1.4-3.5-2.1-5.3c-3.8-18.3-2.2-27.8-0.2-32.5c1.2-2.9,2.6-4,3.1-4.3\nc0,0,0,0,0,0c0.1-0.1,0.1-0.1,0.1-0.1s0,0,0,0c1.9-2.2,4.2-3.9,7.1-5.1c7.6-3.1,15.4-4.1,23.5-2.4c8.7,1.9,16,5.5,18.7,14.8\nc0.9,3.1,1.7,6.3,1.7,9.5C337.2,165.5,336.5,171,336,176.4z\"/>\n<path d=\"M372.8,233.6c-0.3,0.3-0.5,0.6-0.8,0.9C372.2,234.2,372.5,233.9,372.8,233.6C372.8,233.6,372.8,233.6,372.8,233.6z\nM357.1,248.5c0.1,0.1,0.2,0.1,0.2,0.1c0.6-0.6,1.1-1.1,1.7-1.7C358.4,247.5,357.7,248,357.1,248.5z M336,176.3L336,176.3L336,176.3\nC336.1,176.3,336,176.3,336,176.3z\"/>\n<path d=\"M281.8,176.2C281.8,176.1,281.7,176.1,281.8,176.2C281.7,176.1,281.7,176.2,281.8,176.2c0,0.2,0,0.3,0.1,0.4\nC281.8,176.4,281.8,176.3,281.8,176.2z\"/>\n<path d=\"M227,414.3c-0.5,0.1-1,0.3-1.4,0.3C225.9,414.5,226.4,414.4,227,414.3z\"/>\n<path d=\"M344.1,275.8c0.1,0,0.2,0,0.2-0.1c0.5-1.8,0.8-3.2,0.9-4.1C344.8,273,344.4,274.4,344.1,275.8z M341.7,282.9\nc-1.1,2.2-2.1,4.3-3.1,6.5c1.7-3,3-5.8,3.9-8.2C342.1,281.9,341.9,282.4,341.7,282.9z M344.1,275.8c0.1,0,0.2,0,0.2-0.1\nc0.5-1.8,0.8-3.2,0.9-4.1C344.8,273,344.4,274.4,344.1,275.8z M341.7,290.7c0.6-1.5,1-2.9,1.4-4.4L341.7,290.7z M341.7,282.9\nc-1.1,2.2-2.1,4.3-3.1,6.5c1.7-3,3-5.8,3.9-8.2C342.1,281.9,341.9,282.4,341.7,282.9z\"/>\n<path d=\"M387.6,321L387.6,321c-0.1,0-0.1-0.1-0.2-0.1c0.1,0.1,0.1,0.2,0.2,0.3C387.6,321.2,387.6,321.1,387.6,321z\"/>\n<path d=\"M222.5,397.5h-0.2c0,0,0,0,0-0.1C222.4,397.5,222.5,397.5,222.5,397.5z\"/>\n<path d=\"M274.7,179.6L274.7,179.6c-0.1-0.3-0.1-0.5-0.1-0.8C274.6,179.1,274.7,179.3,274.7,179.6z\"/>\n<path d=\"M281.8,176.2L281.8,176.2C281.7,176.2,281.7,176.1,281.8,176.2C281.7,176.1,281.8,176.1,281.8,176.2z\"/>\n<path d=\"M233.3,431.4c0,0-0.1,0.1-0.1,0.1L233.3,431.4C233.3,431.5,233.3,431.5,233.3,431.4z\"/>\n<path class=\"st1\" d=\"M309.2,489.5c0.3,5.2,0.6,9.6,0.9,14.3\"/>\n<path d=\"M278.7,161.2c-0.1-0.6-0.1-1.3,0-1.9C278.7,159.9,278.7,160.5,278.7,161.2z\"/>\n<path d=\"M415.2,305.1c-0.3-0.6-0.7-1.2-1-1.8c0.1,0,0.2,0,0.2,0C414.7,303.9,414.9,304.5,415.2,305.1z\"/>\n<path d=\"M265.2,442.2c-0.1,0-0.1,0-0.2,0v0C265.1,442.2,265.1,442.2,265.2,442.2z\"/>\n<path d=\"M360.8,280.2c-1,1.8-2,3.5-3,5.2c-0.4-0.1-0.8-0.1-1.2-0.2c0.1-0.8,0-1.6,0.3-2.3c0-0.1,0.1-0.1,0.1-0.2c0,0,0-0.1,0-0.1\nc0.3-0.6,0.6-1.3,1.1-1.9c0,0,0,0,0,0C358.7,280.1,359.4,279.8,360.8,280.2z\"/>\n<path d=\"M354.7,624c-0.4,5.3-1.2,9.8-4.2,13.8l0,0C351.8,633.4,353.2,629,354.7,624z\"/>\n<path class=\"st0\" (click)=\"onClickPathBack($event)\" d=\"M367.9,303.3v1.9c-0.4,3.5-1.2,8.5-2.9,14c-4.6,12.2-10.6,23.8-16.7,35.3c-5.8,10.9-9.2,22.7-8,35.3\nc0.2,2.4,0,4.8,0,7.2c-5.4-7.2-8.1-14.9-7-23.6c-0.1,0.3-0.3,0.7-0.5,1.3c-0.2-0.5-0.3-0.8-0.3-1c-0.3-6.9-2.9-12.9-6.9-18.3\nc-2.5-3.5-5.4-6.8-8.2-10.2c-3-3.7-6.1-7.4-5-12.5c0.6-2.9,1.6-5.9,3.2-8.4c2.1-3.4,4.7-6.5,7.3-9.4c2.7-3.1,5.7-5.9,8.8-8.7\nc0.6-0.6,2.2-0.8,3-0.5c6.3,2.6,12.7,3,19.3,1.4C358.6,305.9,363,304.6,367.9,303.3z M256.3,327.2c4.4,9.2,9.1,18.2,13.6,27.4\nc5.4,11.1,8.9,22.6,7.8,35.1c-0.2,2.4,0,4.9,0,7.4c0.4,0.1,0.9,0.2,1.3,0.3c1.8-5.1,4.1-10,5.3-15.3c1.2-5.4,1.4-11.1,1.9-15.5\nc0.9-1.5,1.4-2.3,1.8-3.2c0.9-1.8,1.4-3.8,2.5-5.3c3.5-4.7,7.4-9,10.8-13.8c1.9-2.6,4-5.7,4.2-8.8c0.3-3.4-0.9-7.2-2.5-10.3\nc-2-3.7-4.9-7-7.7-10.3c-2.7-3.1-5.8-5.8-8.6-8.8c-1.1-1.2-2.1-1.2-3.4-0.6c-11.3,4.9-21.8,1.2-32.4-2.4\nC250,311.8,252.6,319.7,256.3,327.2z\"/>\n<path class=\"st0\" (click)=\"onClickPathBack($event)\" d=\"M282.5,571.1c-0.2,0-0.5,0.1-0.7,0.1c0.7,4.9,1.5,9.9,2.2,14.8c0.2,0,0.5-0.1,0.7-0.1\nC284,581,283.2,576,282.5,571.1z\"/>\n<path class=\"st0\" (click)=\"onClickPathBack($event)\" d=\"M368.3,615.3c-0.2,5.2-1.1,10.3-1.8,15.4c-0.3-0.1-0.7-0.1-1-0.1c0.4-3.1,0.8-6.2,1.2-9.3\nc-1.1,2.9-1.4,5.9-2,8.9c-0.8,4-2.2,5.8-4.5,6.3c1.1-4.2,2.2-8.7,3.4-13.3c-1.7,4-2.8,8.2-4.4,12.2c-1,2.5-1.8,2.5-4.5,2\nc3-4.1,4.4-8.7,4.3-13c-1.2,3.4-2.6,7.4-4.1,11.3c-0.8,2-2.3,2.7-4.4,2c3-3.9,3.8-8.5,4.2-13.8c-1.5,4.9-2.9,9.4-4.2,13.8\nc-4.9-0.4-8.3-4.4-7.4-8.2c1-4.2,2.2-8.5,2.1-12.8c-0.1-10.2-0.8-20.4-1.5-30.6c-0.3-3.9-0.9-7.8-1.1-11.8l8-15.7\nc1.8,3.6,2.3,7.1,0,10.9h0.9c0.4-1.6,0.9-3.2,1.3-5c0.7,1.2,1.3,2.3,2,3.5l2.9,8.7c-1,0.9-1.1,2.7-0.4,6.6c0.2-2.3,0.4-4.1,0.6-6\nl0.2,0.5l0,0.1c1.6,7.8,2.9,15.5,6.7,22.5C367.3,605.2,368.5,610.2,368.3,615.3z M265.6,584.3c-0.3,0-0.7,0.1-1,0.1\nc-0.2-5.3-0.5-10.7-0.7-16c-2.1,2-3.5,4.3-2.4,7.4c0,4.3,0,8.7,0,13c-0.3,0-0.6,0-0.8,0c0.3-4.3,0.6-8.6,0.9-13\nc-2.1-0.2-2.1,1.3-2.3,2.7c-1,7.7-2.5,15.3-6.1,22.4c-5.2,10.3-4.1,20.7-0.6,31.2c0.3,0.8,0.9,1.4,1.4,2.2c0.1-0.1,0.3-0.2,0.4-0.3\nc-0.1-0.7-0.3-1.3-0.3-1.5c1.1,1.3,2.3,2.9,3.7,4.5c0.1,0.1,1-0.5,1.5-0.7c-3.4-3.8-3.7-8.4-3.7-12.5c1.1,3.8,2.3,8.2,3.6,12.6\nc1.5,0.5,3.1,1,4.6,1.6c-2.5-4.4-4.5-9-3.9-14.4c0.2,0.9,0.3,1.7,0.5,2.6c0.2,1,0.3,2,0.6,3c0.9,3,1.9,5.9,2.8,8.8\nc0.3,0,0.6-0.1,0.9-0.1c7.2,1.2,11.4-3.4,9.4-10.2c-0.3-0.9-0.6-1.8-0.6-2.7c0.1-13,0.2-26,0.3-38.9c0-4.3,2.9-8.3,1.2-12.8\nc-2.1-5.3-3.9-10.6-5.9-15.9c-0.5,0.2-1,0.3-1.5,0.5c0.2,1.1,0.4,2.2,0.8,3.2c1.6,4.3,3.3,8.6,4.9,13c-0.3,0.1-0.5,0.2-0.8,0.3\nc-1.6-4.6-3.2-9.2-4.8-13.8c-0.4,0-0.8,0-1.1,0c-0.9,2.7-1.8,5.4-2.7,8.1C264.4,573.7,265,579,265.6,584.3z M270.2,580.1\nc-0.2,0.1-0.5,0.1-0.7,0.2c-1.2-4.9-2.4-9.9-3.6-14.8c0.3-0.1,0.5-0.1,0.8-0.2C267.9,570.2,269,575.2,270.2,580.1z M263.3,624.2\nc0.2-0.1,0.4-0.1,0.6-0.2c1.1,4,2.1,8.1,3.2,12.1c-0.2,0.1-0.4,0.3-0.6,0.4C265.5,632.4,264.4,628.3,263.3,624.2z M282.9,625.1\nc0.8,5.3,1.6,10.7,2.4,16c0.3,0,0.7-0.1,1-0.1c0-4.2,0-8.4,0-12.5c0.2,0,0.4,0,0.6,0c0,5,0,10,0,15c0.3,0,0.7,0.1,1,0.1\nc1.3-6.9,2.6-13.9,3.8-20.8c0.1,0,0.3,0,0.4,0c-0.5,4.4-1.1,8.9-1.7,13.9c5-10.2,7.2-20,3.4-30.7c-3.2-9.1-5.4-18.4-8.1-27.7\nc-1.1-3.8-2-7.7-3-11.6c-0.4,0-0.7,0.1-1.1,0.1c-2,6.5-1.2,13.4-2,20.5c0-5.3,0-10.2,0-15c-0.3-0.1-0.5-0.1-0.8-0.2\nc-1,5.7-2.6,11.4-3,17.2c-0.7,8.9-0.5,17.9-0.8,26.8c-0.2,6.2,0.4,12.1,4.9,16.9c-0.2-1.8-0.4-3.7-0.5-5.6c-0.1-2,0-4,0-5.9\nc1.3,13.2,1.8,15.3,4.7,18c-0.6-4.7-1.3-9.5-1.9-14.2C282.4,625.1,282.7,625.1,282.9,625.1z M326.9,622.5c1.1,6.9,2.3,13.9,3.5,20.9\nc2.6-1,1.2-3.1,1.3-4.7c0.1-1.7,0.1-3.5,0.2-5.2c0-1.7,0-3.4,0-5.1c0.2,0,0.4,0,0.6,0c0,4,0,8,0,12c0.3,0,0.7,0.1,1,0.1\nc0.7-5.3,1.4-10.5,2.1-15.8c0.2,0,0.4,0,0.6,0.1c-0.5,4.5-1,9-1.6,14.5c4.1-6,4.1-12.1,4.7-18.1c-0.2,3.8-0.4,7.6-0.6,11.4\nc1.6-4.3,3.8-8.8,3.8-13.2c0.1-12.1-0.5-24.2-1.1-36.2c-0.1-2.9-1.3-5.8-2-8.7c-0.2,0.1-0.3,0.1-0.5,0.2c0,4.1,0,8.2,0,12.5\nc-0.4-5.9-0.9-11.7-1.3-17.5c-0.2,0-0.4,0.1-0.6,0.1c-0.8,5.4-1.7,10.9-2.5,16.3c-0.2-3.4,0.3-6.7,0.8-10c0.4-3.2,0.8-6.3,1.2-9.5\nc-0.3-0.1-0.7-0.1-1-0.2c-0.2,0.4-0.4,0.8-0.5,1.3c-3.5,12.5-6.3,25.2-10.6,37.4c-4.5,12.7-0.7,23.7,4.4,34.8c0,0.1,0.3,0,0.3,0\nc-1-5.7-2-11.4-3-17.1C326.4,622.6,326.6,622.5,326.9,622.5z\"/>\n<path class=\"st0\" (click)=\"onClickPathBack($event)\" d=\"M298.4,406.9c-6.7-3.1-12.9-5.9-19.3-8.9c6.1-7.4,7.7-16,8.7-24.8c0.9-7.9,4.7-14.3,9.4-20.4\nc3.3-4.3,6.6-8.7,9.9-13c-0.1-0.1,0.2,0.1,0.3,0.4c0.6,9.6,0.3,19-1.2,28.5c-1.2,7.7-3.5,15-7,22\nC296.7,395.7,296.1,401.2,298.4,406.9z\"/>\n<path class=\"st0\" (click)=\"onClickPathBack($event)\" d=\"M310.9,339.5c4.8,6.5,9.6,12.9,14.2,19.5c3.7,5.2,4.8,11.2,5.6,17.4c0.9,7.7,3,15,8.2,21.6\nc-6.3,2.9-12.5,5.8-19.2,8.9c2.4-6.3,1.4-12-1.3-17.4c-7.8-15.6-8.2-32.3-7.7-49.1C310.6,340,310.8,339.7,310.9,339.5z\"/>\n<path class=\"st0\" (click)=\"onClickPathBack($event)\" d=\"M236.2,268.3c9.1-2.3,14.2,2.7,18.5,8.5c2.6,3.6,4.9,7.5,7,11.4c4.3,8,10.9,12.9,19.5,15.1\nc0.4,0.1,0.7,0.3,1.1,0.5c-2.4,2-9.8,2.8-13.6,1.8c-3.3-0.9-6.7-1.4-9.9-2.4c-2.7-0.9-5.3-2.2-7.8-3.6c-2-1.1-4.2-2.2-5.4-4\nc-1.8-2.8-3.4-6.1-3.9-9.4C240.9,280,241.5,273.4,236.2,268.3z\"/>\n<path class=\"st0\" (click)=\"onClickPathBack($event)\" d=\"M247.2,266.6c6.7-2.1,13.3-4.1,20-6.2c4.2,14.4,7.2,29.4,16,42.3c-6-2.2-11.9-4.5-15.7-9.8\nc-2.7-3.7-4.8-7.7-7.1-11.6C256.9,275.5,252.9,270.2,247.2,266.6z\"/>\n<path class=\"st0\" (click)=\"onClickPathBack($event)\" d=\"M257.9,543v-10.8c-0.2,0-0.3,0-0.5,0v7.2c-1.7-8.8-3-17.5-4.2-26.4c-1.2-9.3-2.3-18.6-3.3-28\nc-1-9.2-0.9-18.3,1.1-27.4c0.7-2.3,1.6-5.1,2.5-7.3h0.3l0.3,4.1c-0.4,0.5-0.8,1-0.7,1.4c0.6,6,1.1,12.1,2,18.1\nc1.2,7.8,3.7,15.5,3.4,23.6c-0.2,4.6,0.3,9.4,1,14C261.3,522.1,259.5,532.5,257.9,543z M358.1,547.8c-0.3-1.7-0.6-3.6-0.8-5.4\nc-1-9.3-2.6-18.5-1.5-27.9c0.9-8.1,1.6-16.4,1.6-24.5c0-3.9-1.5-7.9-2.9-11.6c-1.6-4-4.6-5.1-9.1-4.5c-1.8,0.2-3.6,0-5.5,0\nc-1.4,0-2.5,0.3-1.7,2.1l0,0.1c2.1,7.2,4.6,14.3,6.3,21.6c2.4,10.2,1.1,20.6,0.7,31c-0.1,4.6,1,9.3,2,13.9c0.9,4.3,2.3,8.5,3.5,12.7\nc0.4,1.3,0.4,2.9,1,4c1.7,3.1,3.7,5.9,5.4,9c0.3,0.5,0.4,1.1,0.5,1.7c0.2,0.4,0.4,0.8,0.5,1.2l0.6-23.1H358.1z M286.8,473.8\nc-1.9-0.2-2.3,0.1-2,2c1.1,6.6,2.2,13.1,3.1,19.7c1.6,11.5,3.5,23,4.2,34.6c0.6,10.5,0,21-0.4,31.5c-0.1,4.4-1,8.8-1.5,13.1\nc1.1-1.4,1.6-2.8,1.9-4.4c2.3-11.9,4.8-23.7,6.7-35.6c1.6-10.2,2.5-20.5,3.7-30.7c0.6-4.9,0.8-9.9,1.4-14.8\nc0.8-7.3,1.7-14.6,2.7-22.7C302.3,470.6,292,474.3,286.8,473.8z M327.5,572.1c-0.8-7.8-1.9-15.7-2.2-23.5c-0.5-9.9,0-19.8,1.7-29.7\nc1.7-9.8,2.8-19.6,4.2-29.4c0.7-5.2,1.5-10.3,2.2-15.4c-7.9-1-15.5-1.6-21.9-7.3c4,36.6,5.6,73,16.2,108\nC327.6,573.9,327.6,573,327.5,572.1z M279.8,475.9c0.5-1.6,0.1-2.2-1.7-2.2c-2,0.1-4-0.2-6-0.2c-3.3,0-6.5,0.3-8.1,3.8\nc-1.1,2.5-2.7,5.1-2.8,7.6c-0.3,7.9-0.1,15.8,0.2,23.6c0.2,5.2,1.6,10.4,1.2,15.5c-0.9,11.7-2.6,23.3-3.8,35\nc-0.4,4.3-0.1,8.8-0.1,13.2c2.2-6.6,7.2-12.2,7-19.5c0.2,0.5,0.3,1.1,0.5,1.7c4.6-7.2,4.2-15.2,4.6-23.1c0,2.7,0,5.4,0,8.1\nc0.3,0,0.6,0.1,0.9,0.1c0.4-3.5,1.2-7.1,1-10.6c-0.6-11.6-1.8-23.1,1.3-34.5C275.7,488.2,277.9,482.1,279.8,475.9L279.8,475.9z\nM279.8,475.9c-0.8,3.8-1.5,7.6-2.5,11.4c-3.2,12.1-4.3,24.4-2.5,36.8c1.6,11.2,4.3,22.2,6.6,33.2c0.7,3.5,2,6.9,3,10.3\nc0.4,0,0.7-0.1,1.1-0.1c0-8.2-0.1-16.4,0-24.6c0.1-3.8,0.7-7.7,0.9-11.5c0.4-7.2,1-14.4,0.9-21.5c-0.2-6.7-1.2-13.3-1.9-20\nc-0.6-5.2-1.2-10.4-2-16.7C281.8,474.4,280.8,475.2,279.8,475.9C279.8,475.9,279.8,475.9,279.8,475.9z M340.7,487.5\nc-0.5-1.9-0.9-3.8-1.3-5.8c-0.1-0.5-0.2-1-0.3-1.5c-0.2-1-0.4-2-0.6-3c-0.1-0.5-0.2-1-0.3-1.4c0,0,0,0,0,0l0-0.1\nc-0.1-0.1-0.1-0.2-0.2-0.3c0-0.1-0.1-0.2-0.1-0.2c-0.1-0.2-0.3-0.4-0.4-0.5c0,0-0.1-0.1-0.1-0.1c-0.1-0.1-0.1-0.1-0.2-0.2\nc-0.1-0.1-0.1-0.1-0.2-0.1c-0.2-0.1-0.3-0.2-0.5-0.2c-0.1,0-0.1,0-0.2-0.1c-0.1,0-0.2,0-0.3-0.1c-0.1,0-0.2,0-0.4,0\nc-0.7,0-1.4,0.2-2.2,0.5c0,2.7,0.2,5.4,0,8c-0.9,8.2-1.9,16.5-2.9,24.7c-1.1,8.6,0.4,17,1.1,25.5c0.9,11,1,22.1,1.4,33.1\nc0,0.5,0.1,1,0.1,2.4c0.6-1.8,1-2.8,1.2-3.9c2.1-8.8,4.2-17.6,6.3-26.4C344.8,521.1,345.1,504.3,340.7,487.5z M364.5,450\nc0,1.7,0,2.7,0,3.7c-0.2,4.4-0.2,8.7-0.6,13.1c-1.1,10.4-4.4,20.4-4.8,31.1c-0.4,13.6-2.3,27.1-0.3,40.7c0.4,2.4,0.7,4.9,1,7.4\nc0,0.2,0.1,0.4,0.1,0.6c0.3,0,0.5-0.1,0.8-0.1v-16.9c0.2,0,0.3,0,0.4,0c0.1,1.1,0.1,2.1,0.2,3.2c1.2-3.7,1.9-7.5,2.4-11.2\nc0.8-5.9,1.5-11.8,2.2-17.7c0.9-7.8,2-15.6,2.6-23.5C369.3,470.2,368.1,460.4,364.5,450z\"/>\n<path class=\"st0\" (click)=\"onClickPathBack($event)\" d=\"M426.2,322.6c0.1,0.3,0.1,0.7,0.2,1v3.2c-0.7-1.7-1.4-3.3-2-4.8c-2.6-10.9-14.1-34-16.3-38.5\nc-0.2-2.7,0.9-5.4,1.2-8.7c0,0,0,0,0.1,0.1c0.4,0.4,3.3,3.3,6.6,8.2C421.3,291.1,427.8,304.9,426.2,322.6z M387.6,321\nc0,0.1,0,0.1,0,0.2c7.3,11.7,19.6,18.2,28.2,28c1.2-4,2.5-8.1,4-12.9C411.5,327.8,397.8,328,387.6,321z M426.2,330.1\nc-0.9-2.3-1.9-4.6-2.9-6.9c-0.1,0-0.1,0-0.1,0s-3.9-9-8-18.1c-0.3-0.6-0.7-1.2-1-1.8c0.1,0,0.2,0,0.2,0c-2.8-6.3-5.5-12.4-6.9-15.2\nc-1.4-2.5-2.9-4.9-4.2-7.5c-2.5-4.9-6.5-7.9-11.7-9.4c-1.5-0.4-2.8-1.1-4.3-1.6c-1.4-0.5-2.6-0.7-3.6-0.6\nc-1.7,2.9-9.5,17.2-9.4,27.5c1.2,3.5,5.6,15.6,11.5,21.2c0.7-0.1,1.4-0.1,1.8,0.3c3.3,3.1,7.6,4.1,11.5,6c6.5,3.1,13.2,5.9,19.6,9.1\nc1.1,0.5,1.6,2.3,2.4,3.4h6.8C427.5,334.4,427,332.2,426.2,330.1z M209.9,286.6c-0.2-3.7-0.5-7.3-0.7-11.5c-1.5,1.3-2.5,2.1-3.4,3\nc-0.1-0.1-0.3-0.2-0.4-0.3c-7.9,10.7-11.3,34.1-12.5,44.9l1.5-3l0.1-0.3h0.1l18.4-36.5C211.9,284.1,210.9,285.4,209.9,286.6z\nM239.6,286.4c0-4-0.7-8.1-1.2-12.1c-0.1-1.1-0.6-2.2-1.2-3.1c-1.3-2.2-3-3.3-5.7-2.1c-3.5,1.6-7.5,2.4-10.5,4.6\nc-2.8,1.9-4.7,5.2-6.9,7.8c-0.3,0.3-0.5,0.6-0.8,0.9L194.8,323c-1.4,3.7-2.9,7.3-4.2,11c-0.3,0.8-0.3,1.7-0.4,2.4h6.7\nc1.1-1.5,1.9-3.4,3.3-4.3c3-1.8,6.2-3.2,9.4-4.6c6.2-2.8,12.5-5.2,18.5-8.3c4.4-2.2,8.3-5.6,9.9-10.3c1.2-3.4,2.7-6.9,3.4-10.7\nC242.2,293.7,239.6,290.4,239.6,286.4z M198.2,336.4c1.5,4.8,2.8,9,4,13c8.5-10,21-16.4,28.2-28.2\nC220.1,327.9,206.5,327.8,198.2,336.4z\"/>\n<path class=\"st0\" (click)=\"onClickPathBack($event)\" d=\"M350.1,356.2c1.1,6.8,2.6,13.2,3,19.7c0.3,4.4,0.5,9.4-3.2,13c-2.5,2.5-5.3,4.7-8.6,7.6\nC340.7,381.5,343.4,368.5,350.1,356.2z\"/>\n<path class=\"st0\" (click)=\"onClickPathBack($event)\" d=\"M267.3,356.3c7.3,12.1,9.9,25.1,9.4,38.9c-2.7-0.2-5-2.3-5.3-4.5c-0.8-0.1-1.6,0.1-1.9-0.3\nc-2.6-3.3-6.1-6.3-3.8-11.2c-1.8-7,2.3-13.6,1.6-20.6C267.1,357.8,267.3,357.1,267.3,356.3z\"/>\n<path class=\"st0\" (click)=\"onClickPathBack($event)\" d=\"M351.9,392.5c1-1.5,1.9-3,3-4.7c1.7,5.7,2.5,11.5,1.7,17.4c-0.2,1.5-0.3,3-0.6,4.5c-0.7,2.8-1.5,5.5-2.2,8.3\nc-0.3,1.1-0.2,2.2-0.3,3.7c-3.7-7.2-7.6-13.9-16-16.6c3.4-2.4,6.5-4.2,9.2-6.5c2-1.7,3.5-4,5.3-6.1\nC352,392.4,351.9,392.5,351.9,392.5z\"/>\n<path class=\"st0\" (click)=\"onClickPathBack($event)\" d=\"M363.4,638.6c-1.1,3.4-2.8,6.8-3.2,10.2c-1.4,13-5.9,24.8-11.9,36.2c-1.2,2.2-2.1,4.5-3.7,6.7\nc0.9-4.7,1.5-9.6,2.7-14.2c2.7-10.8,5.6-21.6,8.4-32.3C357.1,640.1,359.5,638.2,363.4,638.6C363.4,638.7,363.4,638.6,363.4,638.6z\"\n/>\n<path class=\"st0\" (click)=\"onClickPathBack($event)\" d=\"M272.8,691.9c-1.3-2.9-2.7-5.8-4-8.7c-3.7-8-7.9-15.7-9.3-24.5c-0.9-5.8-2.2-11.5-3.4-17.2\nc-0.2-1-1-1.9-1.4-2.9c3.4-0.3,5.8,0.9,6.6,4c3.3,11.9,6.5,23.9,9.5,35.9C271.9,682.9,272.5,687.4,272.8,691.9z\"/>\n<path class=\"st0\" (click)=\"onClickPathBack($event)\" d=\"M277.2,573c-1.3-6.5-3.5-12.2-7.7-16.9c-0.4-0.4-0.5-1.3-0.4-1.9c1.5-5.4,3.1-10.8,4.6-16.1\nc0.1-0.4,0.3-0.7,0.8-1.5c1.1,4.6,2,8.6,3,12.6c1.1,4.1,2.6,8,3.6,12.1c0.4,1.6,0.1,3.5-0.4,5C279.9,568.6,278.5,570.6,277.2,573z\"\n/>\n<path class=\"st0\" (click)=\"onClickPathBack($event)\" d=\"M328.8,528.1c1.2,16.9,4.1,33.8,1.7,50.9C327.6,576.1,326.1,533.8,328.8,528.1z\"/>\n<path class=\"st0\" (click)=\"onClickPathBack($event)\" d=\"M289.4,527.7c2.4,5.8,1,47.6-1.6,51.3C285,570.1,288,536.4,289.4,527.7z\"/>\n<path class=\"st0\" (click)=\"onClickPathBack($event)\" d=\"M352.7,679.6c1.4,4.1,2.5,7.4,3.5,10.8c0.7,2.4-0.7,3.7-4.7,3.9c1.1,0.3,1.7,0.4,2.4,0.6\nc-2.9,3.4-5.7,6.8-8.6,10.3c0-1.7-0.5-3.8,0.1-5.5C347.6,693.1,350.1,686.5,352.7,679.6z\"/>\n<path class=\"st0\" (click)=\"onClickPathBack($event)\" d=\"M351.8,701c0.8,6.8,4,13.7-3.5,18.8C346.4,710.7,347.5,704.5,351.8,701z\"/>\n<path class=\"st0\" (click)=\"onClickPathBack($event)\" d=\"M401.2,372.5c-1.1,2.7-2.2,5.3-3.3,7.9c-2.1-3.3-2.5-13.7-0.7-20.5c1.5,3.8,2.8,7.4,4.1,10.9\nC401.5,371.3,401.4,372,401.2,372.5z M398.2,344.6c1-3.5-0.2-6.4-1.8-9.8c-0.3,0.8-0.5,1.3-0.7,1.7c-1.5,4.9-2.7,9.9-4.7,14.6\nc-2.5,6.2-3.7,12.3-1.6,18.8c1.8,5.7,3.8,11.2,5.7,16.8c0.3,1,0.7,1.9,1.1,2.9c0.3-0.1,0.6-0.2,0.8-0.3c-0.9-4.4-1.8-8.8-2.5-13.3\nc-1.1-6.3-1.2-12.6,1.4-18.7c1.1-2.5,1.8-5.1,2.3-7.8C398.5,348,397.8,346.1,398.2,344.6z M427.9,338c-1.7-0.5-3.7-0.4-5.5,0\nc-1,0.2-2,1.6-2.3,2.6c-0.7,2.7-1.7,5.5-1.5,8.2c0.4,5.3,1.6,10.6,2.6,16.5c0.4-0.9,0.6-1.1,0.7-1.4c2.2-7.3,2.7-15.1,6.4-21.9\nC428.9,340.9,428.4,338.1,427.9,338z M418.6,362.4c-0.5-3.4-0.3-7.6-2.3-9.8c-4.6-5.2-10.2-9.5-15.5-14.2c0,3,0.3,5.4,0,7.8\nc-1.2,9.2-1.3,18.2,4,26.4c4.3,6.7,5.9,14.1,5.3,21.9c-0.2,3-1,6-1.5,8.9c0.8-10.6,0.4-20.8-6.1-30.2c-0.4,0.9-0.6,1.6-0.9,2.2\nc-2.5,7.4-1.6,15.2-2.3,22.9c-0.1,0.9,0.3,2.1,0.9,2.8c1.9,2.1,3.9,4.1,6.1,5.9c0.7,0.6,2.3,1.1,2.8,0.7c1-0.7,2.3-2.3,2.2-3.2\nc-1.7-8.7,3.5-16.3,3.7-24.7v3.8c0.3,0.1,0.5,0.1,0.7,0.2c1.2-3.8,2.9-7.4,3.4-11.3C419.6,369.2,419,365.7,418.6,362.4z\nM218.7,395.2c-0.2-2.8-0.1-5.7-0.4-8.5c-0.6-4.4-1.4-8.7-2.1-13.1c-6.9,9.3-7.9,19.4-5.7,30.7c-0.7-0.8-1-1.1-1-1.3\nc-2.3-9.9-2.8-19.6,2.9-28.8c2.8-4.4,5.3-9.1,5.4-14.6c0.1-6.9,0-13.8,0-21.4c-5.1,4.2-9.9,8-14.3,12.1c-1.4,1.3-2.2,3.5-2.8,5.5\nc-0.8,2.8-1.1,5.7-1.7,8.6c-1.4,7.4,1,13.9,4.8,20.9v-6.2c0.6,4.7,1.6,9.3,2.4,13.9c0.3,1.8,0.5,3.6,0.6,5.4c0.1,2.1-0.2,4.3,0,6.4\nc0.2,2,1.3,3.6,3.5,3.1C214.6,406.7,219.1,399.7,218.7,395.2z M221.6,389.8c0.3-0.7,0.7-1.3,0.9-2c1.7-5.1,3.4-10.3,5.2-15.4\nc2-5.7,2.7-11.5,0.7-17.4c-2-5.7-3.9-11.4-6-17c-0.2-0.4-0.8-0.9-1.3-1c-0.5,0-1.3,0.3-1.4,0.7c-0.4,1-0.9,2.2-0.7,3.2\nc0.6,4.7,0.5,9.7,2.3,13.9c3.5,8.4,3.4,16.6,1.5,25.2c-0.7,3.2-1.2,6.4-1.8,9.6C221.2,389.6,221.4,389.7,221.6,389.8z M188.9,338.7\nc0.4,1.5,0.7,3.1,1.1,4.6c1.3,4,2.7,7.9,3.9,11.9c0.9,3.1,1.6,6.3,2.5,10.2c0.9-3.9,1.6-6.9,2.2-9.9c1-4.8,1.6-9.6-0.4-14.3\nC196.4,336.9,192.4,335.9,188.9,338.7C188.8,338.8,188.9,338.7,188.9,338.7z M217.6,369c-0.3,0.9-1.3,2-1,2.7c1,3,2.4,6,3.9,9.6\nc1.2-7.6,2.4-14.4,0.2-22.1C219.5,363,218.6,366,217.6,369z\"/>\n<path class=\"st0\" (click)=\"onClickPathBack($event)\" d=\"M334.6,683.7c0,3.5,0,7.1,0,10.6c-0.3,0-0.6,0-0.9,0c0-3.5,0-7.1,0-10.6C334,683.7,334.3,683.7,334.6,683.7z\"/>\n<path class=\"st0\" (click)=\"onClickPathBack($event)\" d=\"M263.7,259.4c-8.3,2.3-16.7,5.4-25.2,6.1c-5.1,0.4-9.6,2-13.9,3.9c-4.2,1.9-7.6,5.9-11.3,9\nc-0.6,0.5-1,1.2-1.8,2.2c0-3.7,0.7-6.9-0.2-9.8c-1.4-5-0.3-9.6,1.5-13.9c4.7-11.7,12.7-19.7,25.2-23.1c4.5-1.2,6.8,1.2,9.1,4\nc4.1,5.2,9.4,9,14.8,12.6c1.2,0.8,2.5,1.9,3.3,3.1C267.4,256.3,266.9,258.5,263.7,259.4z M406.6,269.3c0.9-4.5,0-8.6-1.6-12.5\nc-4.7-11.6-12.7-19.9-25.2-23c-2.7-0.7-5.1-0.7-7,1.8c-5,6.4-11.4,11.2-18.1,15.7c-1.2,0.8-1.9,2.5-2.7,3.8\nc-1.6,2.6,0.1,3.7,2.2,4.3c8.7,2.2,17.3,5,26.1,6.3c6.3,0.9,12.2,2.5,17.3,6c3.5,2.4,6.3,5.8,9.5,8.8\nC406.9,277,405.9,273,406.6,269.3z\"/>\n<path class=\"st0\" (click)=\"onClickPathBack($event)\" d=\"M335.8,192.7c-1.5-1.7-2.4-3.2-1.6-5.8c0.4-1.3,0.7-2.7,0.9-4.1c0.1-0.7,0.2-1.4,0.3-2.1c0-0.2,0.1-0.5,0.1-0.7\nc0.2-1.2,0.3-2.4,0.5-3.6c0,0,0,0-0.1,0.1l0.1-0.1c0,0,0,0,0,0c0.4-0.3,0.8-0.6,1.2-0.9c1.2-0.9,2.5-1.9,3.8-2.9\nC343.5,178,340.3,189.9,335.8,192.7z\"/>\n<path class=\"st0\" (click)=\"onClickPathBack($event)\" d=\"M292.1,217.3c-0.2-2.6,0-4.6-0.5-6.4c-1.6-5.8,2-10.4,3.7-15.4c0.3-0.9,3-1,4.7-1.5c0.8,7.1-0.6,13.4-2.5,19.4\nC296.9,215.1,293.9,216,292.1,217.3z M306.3,196.8c-0.6-2.8-1.6-3.3-4-2.4c-0.4,5.8-0.9,11.4-1.3,16.8\nC305.4,208.4,307.6,202.9,306.3,196.8z M289.2,202.6c0.3-0.1,0.5-0.1,0.8-0.2c1.2-2.7,2.3-5.4,3.4-8c-2.7-1-5.1-1.9-8.5-3.1\nC286.5,195.5,287.8,199,289.2,202.6z M325.5,212.6c1.6-3.8,2-7.4-0.3-11c-0.7-1.1-1.5-2.3-1.6-3.6c-0.4-4-2.6-4.3-6.1-3.2\nc0.3,3.7,0.4,7.6,1.1,11.3c0.9,4.7,1,10.2,8.2,10.6C326.4,215.3,325.1,213.6,325.5,212.6z M315.7,193.5c-1.2,0.7-3.2,1-3.6,2\nc-2.4,5.7,0.8,13.7,4.9,15.6C316.6,205.7,316.1,200,315.7,193.5z M324.5,194.4c1.2,2.8,2.4,5.4,3.6,8c0.3,0.1,0.5,0.1,0.8,0.2\nc1.3-3.6,2.6-7.1,4.2-11.3C329.6,192.5,327.1,193.4,324.5,194.4z\"/>\n<path d=\"M266.7,565.2c1.2,5,2.3,9.9,3.5,14.9c-0.2,0.1-0.5,0.1-0.7,0.2c-1.2-4.9-2.4-9.9-3.6-14.8\nC266.2,565.4,266.4,565.3,266.7,565.2z\"/>\n<path d=\"M263.8,637.7c-2.5-4.4-4.5-9-3.9-14.4c0.2,0.9,0.3,1.7,0.5,2.6c0.2,1,0.3,2,0.6,3C261.9,631.9,262.8,634.8,263.8,637.7\nC263.8,637.8,263.8,637.7,263.8,637.7z\"/>\n<path d=\"M263.9,568.4c0.6,5.3,1.2,10.6,1.8,15.9c-0.3,0-0.7,0.1-1,0.1C264.3,579.1,264.1,573.7,263.9,568.4\nC263.9,568.4,263.9,568.4,263.9,568.4z\"/>\n<path d=\"M259.3,636.1c-3.4-3.8-3.7-8.4-3.7-12.5C256.7,627.4,257.9,631.8,259.3,636.1C259.2,636.2,259.3,636.1,259.3,636.1z\"/>\n<path d=\"M266.6,636.5c-1.1-4.1-2.2-8.2-3.2-12.3c0.2-0.1,0.4-0.1,0.6-0.2c1.1,4,2.1,8.1,3.2,12.1C267,636.2,266.8,636.4,266.6,636.5\nz\"/>\n<path d=\"M261.5,575.7c0,4.3,0,8.7,0,13c-0.3,0-0.6,0-0.8,0C260.9,584.4,261.2,580.1,261.5,575.7\nC261.5,575.7,261.5,575.7,261.5,575.7z\"/>\n<path d=\"M282.5,571.1c0.7,4.9,1.5,9.9,2.2,14.8c-0.2,0-0.5,0.1-0.7,0.1c-0.7-4.9-1.5-9.9-2.2-14.8\nC282,571.2,282.3,571.1,282.5,571.1z\"/>\n<path d=\"M278.9,155.6c0,1.2-0.1,2.5-0.2,3.7C278.7,158,278.8,156.8,278.9,155.6z\"/>\n<path d=\"M286.4,143.4c0,0-0.1,0-0.1,0.1C286.3,143.4,286.3,143.4,286.4,143.4C286.4,143.4,286.4,143.4,286.4,143.4z\"/>\n<path d=\"M274.6,178.7c-0.1-0.4-0.1-0.8-0.2-1.2C274.5,177.9,274.5,178.3,274.6,178.7z\"/>\n<path d=\"M207.3,270.2c0.1-1.3,0.2-2.6,0.4-3.8C206.9,267.7,206.6,268.9,207.3,270.2z\"/>\n<path d=\"M194.5,319.3l-0.1,0.3L194.5,319.3L194.5,319.3z M194.5,319.3l-0.1,0.3L194.5,319.3L194.5,319.3z\"/>\n<path d=\"M285.6,687.9c0.2,1.7,0.3,3.6,0.3,5.5c0.1-0.1,0.2-0.2,0.2-0.3C285.9,691.4,285.8,689.6,285.6,687.9z\"/>\n<path d=\"M267.8,699.4c0.5,0.4,1.1,0.8,1.5,1.2C268.9,700.1,268.4,699.7,267.8,699.4z M261.5,685.5l-0.1,0.1\nc-0.3,1.2-0.6,2.4-1.2,3.7c1.4-0.5,2.3-1.1,2.8-1.9L261.5,685.5z\"/>\n<path class=\"st0\" (click)=\"onClickPathBack($event)\" d=\"M272.4,696.9c-0.2,1.7-0.5,3.6-0.7,5.4c-0.5-0.4-1.3-1-2.3-1.7c-0.5-0.5-1-0.9-1.5-1.2\nc-3.3-2.5-7.3-5.8-7.4-6.8c-0.2-1.8,2.7-5,2.7-5l0,0c1.7-2.2,0.6-5.4,1.7-8.3C267.5,685.5,269.9,691.2,272.4,696.9z\"/>\n<path d=\"M331.2,707.4c-0.2,0.3-0.4,0.6-0.7,0.9C330.9,707.7,331.1,707.3,331.2,707.4C331.1,707.3,331.1,707.4,331.2,707.4z\"/>\n<path d=\"M337.4,716.3c-0.2,0-0.5,0-1,0c-0.3,0-0.5-0.1-0.8-0.1C336.3,716.3,336.9,716.3,337.4,716.3z\"/>\n<path d=\"M263.1,687.5c-0.6,0.8-1.5,1.4-2.8,1.9c0.5-1.2,0.9-2.5,1.2-3.7l0.1-0.1L263.1,687.5z\"/>\n<path d=\"M268.1,702.6l-1.9-1.6C267,701.5,267.6,702,268.1,702.6z\"/>\n<path d=\"M384.4,715.9L384.4,715.9L384.4,715.9C384.4,715.9,384.4,715.9,384.4,715.9z\"/>\n<path d=\"M359.8,576.2L359.8,576.2c0,0.3,0,0.5,0.1,0.8C359.8,576.7,359.8,576.5,359.8,576.2z M357.7,576.8c-1,0.9-1.1,2.7-0.4,6.6\nc0.2-2.3,0.4-4.1,0.6-6L357.7,576.8z\"/>\n<path d=\"M350,557.7c0.2,0.4,0.4,0.7,0.6,1.1l0.4-0.7L350,557.7z\"/>\n<path d=\"M358.1,578l-0.2-0.5c0-0.1,0-0.1,0-0.2C358,577.5,358.1,577.8,358.1,578z\"/>\n<path d=\"M360.4,581.1c-0.2-1.3-0.4-2.6-0.5-4V577c0,0,0,0.1,0,0.1v0C360.1,578.3,360.3,579.6,360.4,581.1z\"/>\n<path d=\"M360.7,545.2v1.2c-0.3,0-0.5,0.1-0.8,0.1c0-0.2-0.1-0.4-0.1-0.6L360.7,545.2z\"/>\n<path d=\"M359.8,576.2c0,0.3,0.1,0.5,0.1,0.8C359.8,576.7,359.8,576.5,359.8,576.2L359.8,576.2z\"/>\n<path d=\"M359.9,577.1L359.9,577.1C359.9,577.1,359.9,577.1,359.9,577.1L359.9,577.1z\"/>\n<path d=\"M351,558l-0.4,0.7c-0.2-0.4-0.4-0.7-0.6-1.1L351,558z\"/>\n<path d=\"M253.8,449.7c-0.1,0.2-0.2,0.4-0.3,0.6h0.3L253.8,449.7z\"/>\n<path d=\"M226,436.2c0.3,0,0.6-0.2,0.9-0.4C226.4,436,226.1,436.2,226,436.2z\"/>\n<path d=\"M234.3,430.2c-0.1,0.2-0.2,0.5-0.4,0.7c-0.1,0.2-0.3,0.4-0.5,0.6C233.6,431,234,430.6,234.3,430.2z\"/>\n<path d=\"M234.7,415.3c-0.1,0-0.2,0-0.3-0.1v-0.2C234.5,415.2,234.6,415.2,234.7,415.3z\"/>\n<path d=\"M338.6,289.4c-0.2,0.5-0.4,1-0.7,1.5c-0.1,0-0.1,0-0.1,0C338,290.3,338.3,289.9,338.6,289.4z\"/>\n<path d=\"M345.4,270.7c-0.1,0.3-0.2,0.6-0.2,0.9c0.1-0.6,0.1-1,0.1-1L345.4,270.7z\"/>\n<path d=\"M372.3,299C372.3,299,372.3,299,372.3,299C372.3,299,372.3,299,372.3,299C372.3,299,372.3,299,372.3,299z\"/>\n</svg>\n\n</div>";
      /***/
    },

    /***/
    "./node_modules/raw-loader/dist/cjs.js!./src/app/components/bodysvgfront/bodysvg.component.html":
    /*!******************************************************************************************************!*\
      !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/components/bodysvgfront/bodysvg.component.html ***!
      \******************************************************************************************************/

    /*! exports provided: default */

    /***/
    function node_modulesRawLoaderDistCjsJsSrcAppComponentsBodysvgfrontBodysvgComponentHtml(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony default export */


      __webpack_exports__["default"] = "<div>\n    <div class=\"human-body-front\">\n        <svg version=\"1.1\" id=\"Layer_1\" xmlns=\"http://www.w3.org/2000/svg\" xmlns:xlink=\"http://www.w3.org/1999/xlink\" x=\"0px\" y=\"0px\" viewBox=\"0 0 595.3 841.9\">\n\t\t\t<style type=\"text/css\">\n\t\t\t  .st0{fill:#dad3d3;}\n\t\t\t  .st1{fill:#030505;}\n\t\t\t</style>\n  \n  <path d=\"M193.1,321.9c-1.3-4.9-1.6-9.2-1.5-12.9C191.5,312.7,191.8,317.1,193.1,321.9c0.7,1.2,1.4,2.3,2,3.3c0,0,0-0.1,0-0.1\n  C194.5,324.1,193.8,323,193.1,321.9z M199.8,285.4C199.8,285.5,199.7,285.5,199.8,285.4c0.1,0.1,0.3,0.2,0.5,0.2c0,0,0-0.1,0-0.1\n  C200.1,285.5,199.9,285.5,199.8,285.4z M201.2,315.4C201.2,315.4,201.2,315.4,201.2,315.4c-1.6-4.5-3.7-12-3.4-19.4\n  C197.5,303.5,199.6,311,201.2,315.4C201.2,315.5,201.2,315.5,201.2,315.4c1.3,1.3,2.7,2.6,4,3.8v-0.1\n  C203.9,317.9,202.5,316.7,201.2,315.4z M202.2,282.4c-0.8,1-1.4,2-1.9,3.1c0,0,0,0,0,0C200.8,284.4,201.5,283.4,202.2,282.4\n  L202.2,282.4z M209.7,308.9c-0.1,0.2-0.2,0.5-0.3,0.7C209.6,309.3,209.6,309.1,209.7,308.9C209.7,308.9,209.7,308.9,209.7,308.9z\n  M208.2,313.6c-0.6,1.2-1.2,2.5-2,4.1c-0.3-1.1-0.4-1.6-0.6-2.1c-0.1,0-0.2,0-0.3,0.1v0.1c0.1,0,0.2,0,0.3-0.1\n  c0.1,0.5,0.3,1,0.6,2.1C206.9,316.2,207.5,314.9,208.2,313.6c0,0.1,0.1,0,0.1-0.2C208.2,313.5,208.2,313.6,208.2,313.6z\n  M210.1,274.8c0,0.1,0,0.2,0,0.2c0,0,0.1,0,0.1,0C210.2,274.9,210.2,274.9,210.1,274.8z M216.1,296.1\n  C216.1,296.1,216.1,296.1,216.1,296.1c-0.2,0.1-0.3,0.3-0.4,0.4h0c-0.9,0.9-1.8,2.5-2.7,4.2c0.9-1.8,1.9-3.3,2.8-4.2\n  C215.9,296.5,216,296.4,216.1,296.1C216.1,296.1,216.1,296.1,216.1,296.1z M213,300.7c-0.2,0.3-0.3,0.6-0.5,0.9\n  C212.7,301.3,212.8,301,213,300.7z M209.7,308.8L209.7,308.8c0.2-0.5,0.4-1.1,0.6-1.7C210,307.7,209.9,308.2,209.7,308.8z\n  M227.2,277.4c-3.8,6-7.5,12.1-11.3,18.1c-0.1,0.1-0.1,0.2-0.2,0.3c0,0,0,0,0,0c0.1,0.1,0.2,0.2,0.3,0.3c-0.1-0.2-0.1-0.3-0.2-0.5\n  C219.7,289.5,223.4,283.5,227.2,277.4c0.2,0.2,0.4,0.3,0.6,0.4c0,0,0,0,0-0.1C227.6,277.6,227.4,277.5,227.2,277.4z M216.1,372.5\n  c-1-3.3-1.7-6.7-3.2-9.8c-2-4.1-3.1-8.6-5.7-12.6c-4.8-7.3-8.3-15.4-12.3-23.2c-0.1-0.1-0.1-0.2-0.1-0.3c0,0.1,0,0.3,0.1,0.4\n  c4,7.8,7.5,15.9,12.3,23.2c2.7,4,3.7,8.4,5.7,12.6C214.4,365.8,215.1,369.3,216.1,372.5c0.2-0.1,0.5-0.1,0.5-0.2v-0.1\n  C216.6,372.3,216.3,372.4,216.1,372.5z M219.4,251c-1.9,5.7-3.8,11.3-5.8,17c0-1.8,0.1-3.7,0.2-5.4c-0.1,1.8-0.2,3.7-0.2,5.5\n  C215.6,262.5,217.5,256.8,219.4,251c0.1,0.1,0.3,0.2,0.4,0.2c0,0,0-0.1,0-0.1C219.7,251.2,219.6,251.1,219.4,251z M234.8,312.6\n  c-5.6,8.7-14.2,13.3-23.3,17.6c-0.3-0.6-0.4-1.2-0.8-1.5c-2.5-2.1-3.4-4.4-3.2-7c-0.2,2.6,0.7,5,3.2,7.1c0.4,0.3,0.5,0.9,0.8,1.5\n  C220.6,326,229.2,321.4,234.8,312.6c0.6-1.2,1.5-2.6,1.8-3.1c0,0,0-0.1,0-0.1C236.3,309.9,235.4,311.4,234.8,312.6z M242.2,271.6\n  c-1.4,1.8-2.8,3.7-4.2,5.5c0,0,0,0,0,0C239.5,275.3,240.9,273.5,242.2,271.6C242.2,271.6,242.2,271.6,242.2,271.6z M237.1,307.6\n  c-0.1,0.1-0.1,0.2-0.1,0.3C237,307.9,237.1,307.8,237.1,307.6c1.4-2.7,2.5-5.5,3.5-8.4C239.7,302.1,238.5,304.9,237.1,307.6z\n  M245.7,273.6c-0.9,2.5-1.7,5-2.6,7.5h0C244,278.6,244.8,276.1,245.7,273.6L245.7,273.6z M246.3,233c-1.2,0.3-2.2,0.6-3.2,0.9\n  c-1,0.3-2,0.6-3.1,0.8v0.1c1-0.3,2-0.5,3.1-0.8c1-0.3,2.1-0.6,3.3-0.9C246.3,233.1,246.3,233.1,246.3,233z M246.9,267.9\n  c-1.3,2.4-2.6,4.8-3.9,7.3c0,0,0,0,0,0C244.3,272.8,245.6,270.4,246.9,267.9C246.9,268,246.9,267.9,246.9,267.9z M265.7,236.8\n  c-4.5,0.5-9,1.1-13.4,1.6h0.6c4.3-0.5,8.7-1,13-1.6C265.8,236.8,265.7,236.8,265.7,236.8z M273.9,238.1c-0.6,0.2-1.1,0.4-2,0.7h0.2\n  C272.9,238.5,273.4,238.3,273.9,238.1C274,238.1,274,238.1,273.9,238.1z M297.7,242.3c-0.5,0-1-0.2-1.5-0.3c-6-1.6-12-1-18.1,0\n  c-11.7,1.8-22.2,5.7-28.8,16.4c-6.1,9.8-15.1,16.6-24.1,23.5c0,0-0.1,0.1-0.1,0.1c9-6.9,18-13.7,24.1-23.5\n  c6.6-10.6,17.1-14.5,28.8-16.4c6-0.9,12-1.6,18.1,0C296.7,242.2,297.2,242.4,297.7,242.3c1.2,0.2,2.5,0.2,3.7,0.3v-0.1\n  C300.2,242.4,299,242.4,297.7,242.3z\"/>\n  <path d=\"M427.9,369.2C427.9,369.3,427.9,369.3,427.9,369.2C427.9,369.3,427.9,369.2,427.9,369.2\n  C427.9,369.2,427.9,369.2,427.9,369.2z M425.5,378c0.3-0.9,0.7-1.7,1-2.6C426.2,376.3,425.8,377.1,425.5,378z M426.7,375\n  c0.1-0.1,0.1-0.3,0.1-0.4c0,0,0-0.1,0-0.1C426.8,374.6,426.7,374.8,426.7,375z M423.5,364.1c-0.3-1.5-0.4-2.8-0.3-4.1\n  C423,361.3,423.1,362.7,423.5,364.1c0.4,1.9,0.4,3.8,0.2,5.8C423.8,367.9,423.8,365.9,423.5,364.1z M420,421.7\n  c-1.9,0.8-4,1.2-6.1,1.8c0,0,0,0,0.1,0.1C416,423,418.1,422.6,420,421.7c0.4-0.1,0.7-0.6,1-1.1C420.6,421.1,420.3,421.6,420,421.7z\n  M418.7,401.7c0,0,0,0.1,0,0.1c0.5,0.3,1.1,0.8,2.1,1.4c0,0,0-0.1,0-0.1C419.8,402.4,419.2,402,418.7,401.7z M420.8,407.6\n  c-0.3,0.1-0.5,0.3-0.8,0.4c-0.5-0.3-1-0.6-2.3-1.3c0,0.1,0.1,0.1,0.1,0.2c1.2,0.7,1.7,1,2.2,1.3\n  C420.3,407.9,420.6,407.8,420.8,407.6C420.8,407.6,420.8,407.6,420.8,407.6z M415.2,425c-1.6,0.2-2.8,0.3-4.3,0.4\n  c0,0,0.1,0.1,0.1,0.1C412.5,425.4,413.6,425.3,415.2,425C415.2,425.1,415.2,425.1,415.2,425z M413.5,427.5c-2.1,0.4-3.6,0.6-5.2,0.9\n  c0.1,0,0.1,0,0.1,0.1C410,428.2,411.5,427.9,413.5,427.5C413.6,427.5,413.5,427.5,413.5,427.5z M410.5,430.2\n  c-2.3,0.8-4.5,1.7-6.8,2.5c-2.3,0.7-3.3-0.2-3.1-2.3c-0.2,2.2,0.8,3.1,3.1,2.4C406,431.9,408.2,431,410.5,430.2\n  C410.5,430.2,410.5,430.2,410.5,430.2z M409.2,377.9c-0.9,1.7-1.9,3.3-3.1,5.5c-0.2-1.5-0.6-2.6-0.7-3.6c0.1,1,0.5,2.2,0.7,3.7\n  c0.3-0.5,0.5-0.9,0.8-1.4l0,0l0,0C407.8,380.6,408.5,379.3,409.2,377.9c0.1,0.1,0.2,0.2,0.4,0.2c0,0,0,0,0-0.1\n  C409.5,378,409.3,378,409.2,377.9z M408.3,413.1c-0.1-1-1.2-2.7-2.1-2.9c-3.1-0.8-8,2.5-9.7,6.4c1.8-3.8,6.6-7.1,9.7-6.3\n  C407.1,410.5,408.2,412.1,408.3,413.1c0.1,1.9,0.2,3.8,0.1,5.6C408.5,416.8,408.4,414.9,408.3,413.1z M406.8,394.4L406.8,394.4\n  c0.4-1.2,0.8-2.5,1.2-3.7C407.5,392,407.2,393.2,406.8,394.4z M403.4,395.2c-0.2-0.5-0.3-1-0.3-1.4\n  C403.1,394.3,403.2,394.8,403.4,395.2c0,0.2,0,0.3,0,0.4C403.5,395.4,403.5,395.3,403.4,395.2z M400.3,429.4\n  c-0.4,0.4-0.7,0.8-1.7,1.9c-1.1-3.5-2.1-6.4-3.1-9.5c0,0,0,0,0,0.1c1,3.1,1.9,6,3.1,9.5C399.6,430.3,400,429.9,400.3,429.4\n  c0.1,0.2,0.2,0.3,0.2,0.5c0,0,0,0,0-0.1C400.6,429.7,400.4,429.6,400.3,429.4z M399.8,424.1C399.8,424,399.8,424,399.8,424.1\n  L399.8,424.1l0,0.1L399.8,424.1z M395.2,419.9c0.2-0.4,0.4-0.8,0.6-1.2c0,0,0-0.1,0-0.1C395.6,419,395.4,419.4,395.2,419.9z\n  M391.8,429.5C391.7,429.5,391.7,429.5,391.8,429.5c0,0-4,1-3.5-4.7c-0.5,5.8,3.5,4.8,3.5,4.8S391.7,429.6,391.8,429.5\n  c0.9-2,1.8-4,2.7-6v-0.1C393.6,425.4,392.6,427.4,391.8,429.5z\"/>\n  <path d=\"M200.9,407.6c0.1,0.7,0.4,1.5,1.2,2.3c0,0,0,0,0,0c0-0.1,0-0.1,0-0.1C201.3,409,201,408.2,200.9,407.6z M200.3,404.2\n  c-1.6-9-3.1-17.6-4.6-26c0,0.1-0.1,0.2-0.1,0.2c-0.2,0-0.5,0-0.7,0c-1.2-11-2.3-22.1-3.5-33.1c0,0,0,0,0,0\n  c1.2,11.1,2.3,22.1,3.5,33.2c0.2,0,0.5,0,0.7,0c0-0.1,0.1-0.2,0.1-0.2C197.3,386.7,198.8,395.2,200.3,404.2c1.3-0.9,2-1.5,3-2.4\n  c0,0,0-0.1,0-0.1C202.4,402.6,201.6,403.1,200.3,404.2z M203.2,422.5c-2.2,0-3-0.9-3-2.3C200.2,421.6,201,422.6,203.2,422.5\n  c1.4,0.1,2.9,1.2,4.2,1.8c0,0,0.1,0,0.1-0.1C206.2,423.6,204.7,422.5,203.2,422.5z M197.2,350c-4-7-5.6-14.8-4.9-23.2l0,0\n  C191.5,335.3,193.2,343,197.2,350c4.8,8.6,9.4,17.1,12.3,26.5l0.1,0.1C206.7,367.2,202,358.6,197.2,350z M205.8,425.5\n  c0,0-0.1,0-0.1,0.1c2.1,0.2,3.5,0.3,5.5,0.5c0,0,0.1,0,0.1-0.1C209.3,425.8,207.9,425.7,205.8,425.5z M207.7,427.8\n  c0,0-0.1,0-0.1,0.1c2.3,0.5,3.7,0.9,5.2,1.2c0,0,0.1,0,0.1-0.1C211.4,428.6,210,428.3,207.7,427.8z M210.4,379.8\n  c-0.1-0.3-0.1-0.6-0.2-0.8l0,0C210.2,379.2,210.3,379.5,210.4,379.8c1,4.2,2,8.3,3,12.4c0,0,0,0,0,0\n  C212.4,388,211.4,383.9,210.4,379.8z M213.7,393.9c0-0.1,0-0.2,0-0.3l0-0.1l-0.3-1.2c0,0,0,0,0,0l0.3,1.3l0,0.1\n  C213.7,393.8,213.7,393.9,213.7,393.9c0,0.2,0,0.4,0,0.5C213.7,394.2,213.7,394.1,213.7,393.9z M212.4,378.3c-0.1,0-0.2,0.1-0.3,0.1\n  c0,0,0,0,0,0.1C212.2,378.4,212.3,378.4,212.4,378.3c0.9,1.6,1.8,3.1,2.8,4.9c0,0,0-0.1,0-0.1C214.2,381.3,213.3,379.8,212.4,378.3z\n  M218.5,413C218.5,413,218.5,413,218.5,413c-1.3,2.5-2.5,4.6-3.7,6.9v-9c-0.4,0.2-0.7,0.4-0.9,0.6c-0.1,0.2-0.2,0.4-0.3,0.6\n  c0.1-0.2,0.2-0.4,0.3-0.6c0.2-0.2,0.5-0.5,0.9-0.6v9C215.9,417.7,217.1,415.5,218.5,413z M220.7,432.8c-1.7,1.5-6.7,0-9.5-3\n  c0,0,0,0-0.1,0C213.9,432.8,219,434.3,220.7,432.8c2.1-1.8,3.5-3.9,4.1-5.9C224.2,428.9,222.8,430.9,220.7,432.8z M229.5,425.5\n  C229.5,425.6,229.5,425.6,229.5,425.5c1.7-0.5,2.4-0.9,2.6-1.8C231.8,424.6,231.1,425,229.5,425.5z M230.4,430.2\n  c-3-2-2.7-5.1-3.1-7.6c-1.1-6.9-4.1-10.5-10.9-11.8c-0.4-0.1-1,0-1.5,0.2c0,0,0.1,0,0.1,0.1c0.5-0.2,1-0.2,1.4-0.2\n  c6.8,1.3,9.8,4.9,10.9,11.8c0.4,2.6,0.1,5.6,3.2,7.7C230.4,430.2,230.4,430.2,230.4,430.2z\"/>\n  <path d=\"M365.6,463.9c0-0.1-0.1-0.2-0.1-0.3c3.2,10.5,6.4,21,7.6,32c0,0.1,0,0.1,0,0.2C371.9,484.8,368.7,474.3,365.6,463.9z\n  M371.8,511C371.8,511,371.7,511,371.8,511c0.1,1,0.1,2,0.1,2.9C371.9,512.9,371.9,512,371.8,511z M370.7,522.9\n  c0.7-1.9,1.3-3.8,1.8-5.7C372,519.1,371.4,521,370.7,522.9C370.7,522.9,370.7,522.9,370.7,522.9z M367.5,473.6\n  C367.5,473.6,367.5,473.6,367.5,473.6c-0.1,6,0.7,11.9,0.8,17.8c0.1,6.5,0.2,13-0.3,19.5c0.5-6.5,0.4-13.1,0.3-19.6\n  C368.3,485.5,367.5,479.6,367.5,473.6z M359.2,553.4c3.5-4.7,5.7-9.8,7.2-15.2C364.8,543.5,362.7,548.6,359.2,553.4L359.2,553.4z\n  M359.2,556c-1.5,1.5-2.8,1.2-3.2-0.9c-0.4-2.4-0.7-4.8-0.6-7.2c-0.1,2.4,0.2,4.8,0.6,7.2C356.4,557.2,357.7,557.6,359.2,556\n  c0.7-0.6,1.2-1.6,1.9-2.7h0C360.4,554.3,359.9,555.2,359.2,556z M353.2,569.1h0.1c2.4-1.3,4.8-2.1,6.2-3.8c0.3-0.3,0.5-0.7,0.7-1.1\n  c-0.2,0.4-0.4,0.8-0.7,1.1C358.2,566.9,355.6,567.7,353.2,569.1z M363.6,521.7c-1.7,5.5-4,10.8-6.6,15.9c-1,2-1.8,4-2.4,6\n  c0.6-2,1.4-4,2.4-5.9C359.6,532.5,361.9,527.2,363.6,521.7z M353.3,574.7c-2,5.8-6.3,7.2-11.2,3.6c-3.1-2.3-5-5.2-4.8-9.5\n  c-0.8,0.7-1.3,1.2-1.8,1.7c-1.2-2.8-2.3-5.4-3.7-8.5c0,0,0,0-0.1,0c1.4,3.2,2.5,5.8,3.8,8.7c0.6-0.6,1.1-1,1.8-1.7\n  c-0.2,4.3,1.7,7.2,4.8,9.5C347.1,582,351.3,580.6,353.3,574.7c0.7-2,2.6-3.7,4-5.6h-0.1C355.9,571,354,572.7,353.3,574.7z\n  M344.3,554.8c0-0.9-0.1-1.8-0.1-2.7C344.3,553.1,344.3,554,344.3,554.8c0,0.4,0,0.7,0,1C344.3,555.5,344.3,555.2,344.3,554.8z\n  M333.5,484.7c0.7,9,1.7,18,2.3,27c0.5,7.5,2.6,14.6,5.6,21.4c2.7,6.2,2.7,12.6,2.9,19c-0.2-6.4-0.2-12.9-2.9-19\n  c-3-6.8-5.2-13.8-5.6-21.4C335.2,502.6,334.2,493.7,333.5,484.7c0-0.3,0-0.6-0.1-0.8C333.4,484.2,333.5,484.4,333.5,484.7z\n  M343.6,450L343.6,450c-0.4,8-0.9,16-1.3,24h0C342.7,465.9,343.1,458,343.6,450z M336.5,563c2,1.7,4.4,3,6.5,4.7c0,0,0.1,0,0.1,0\n  C341,565.9,338.6,564.6,336.5,563c-1.9-1.7-3.5-3.7-5.2-5.5c0,0-0.1,0-0.1,0C333,559.3,334.6,561.3,336.5,563z M334.7,543.2\n  c0.2-0.1,0.3-0.2,0.5-0.3l6.4,8.6c0.3-0.2,0.6-0.4,1-0.6c0,0,0,0,0-0.1c-0.3,0.2-0.6,0.4-0.9,0.6l-6.4-8.6\n  C335,542.9,334.8,543,334.7,543.2C334.7,543.1,334.7,543.1,334.7,543.2z M335.3,539.6C335.3,539.6,335.3,539.6,335.3,539.6\n  c0.2-0.1,0.4-0.2,0.5-0.3c1.9,2.7,3.8,5.3,5.7,8c0.3-0.2,0.6-0.4,0.9-0.6c0,0,0,0,0-0.1c-0.3,0.2-0.6,0.4-0.9,0.5\n  c-1.9-2.7-3.8-5.3-5.7-8C335.7,539.4,335.5,539.5,335.3,539.6z M341.4,542.9c-1.9-2.8-3.8-5.6-5.7-8.4c-0.2,0.1-0.4,0.2-0.6,0.4\n  c0,0,0,0,0,0.1c0.2-0.1,0.4-0.2,0.6-0.3C337.6,537.4,339.5,540.2,341.4,542.9c0.3-0.1,0.6-0.2,0.9-0.3c0,0,0,0,0-0.1\n  C342,542.6,341.7,542.8,341.4,542.9z M333.8,545.5c2.5,3.2,5,6.4,7.7,10.1c0,0,0-0.1,0-0.1C338.8,551.9,336.3,548.6,333.8,545.5\n  c-0.2,0.1-0.5,0.3-0.7,0.4c0,0,0,0,0,0C333.4,545.8,333.6,545.7,333.8,545.5z M338.1,556.2c0.5,0.7,1.8,0.8,2.7,1.2c0,0,0-0.1,0-0.1\n  C339.8,557,338.6,556.8,338.1,556.2c-2.5-3.3-4.8-6.8-7.2-10.2c0,0.1,0,0.1,0,0.1C333.3,549.5,335.6,552.9,338.1,556.2z M339.9,486\n  L339.9,486c-0.2-2.3-0.3-4.6-0.5-6.9c-0.1-2.3-0.4-4.7-0.3-7c0-1.2,0.1-2.4,0.2-3.6c-0.1,1.2-0.1,2.5-0.2,3.7c0,2.3,0.2,4.7,0.3,7\n  C339.6,481.5,339.8,483.8,339.9,486z M337.2,558.1c0.2-0.2,0.5-0.4,0.7-0.7c0,0,0,0,0,0C337.7,557.6,337.4,557.8,337.2,558.1\n  c-1.4-1.3-3.2-2.2-4.1-3.7c-6-9-7.4-19.5-9.7-29.6c0,0,0,0,0,0c2.3,10.2,3.7,20.7,9.7,29.7C334.1,555.9,335.8,556.9,337.2,558.1z\n  M337.3,492.4L337.3,492.4c0.5-5.2-0.9-10.5-0.5-15.7C336.4,481.9,337.8,487.1,337.3,492.4z M333.5,481.6c-0.1,0.7-0.1,1.4,0,2.2\n  C333.4,483.1,333.4,482.3,333.5,481.6z M330.7,507.5L330.7,507.5c0-2.2,0-4.5,0-6.7c0-2.3-0.2-4.7-0.1-7c0-0.6,0.1-1.1,0.1-1.7\n  c0,0.6-0.1,1.2-0.1,1.8c-0.1,2.3,0.1,4.7,0.1,7C330.8,503.1,330.7,505.3,330.7,507.5z M328.5,513.6c0.4-5.4-0.3-10.8,0.2-16.2\n  C328.2,502.8,329,508.2,328.5,513.6z M326.6,533.1c-0.2-7.5-3.8-14.7-2.5-22.3C322.8,518.3,326.3,525.5,326.6,533.1\n  C326.6,533,326.6,533,326.6,533.1z M326.4,499.4L326.4,499.4l0,20.8h0V499.4z M284.2,557.6c0,0,0,0.1-0.1,0.1c3.8-1.7,6-4.7,7.1-8.2\n  c2.2-6.6,4-13.4,6-20.1c0,0,0-0.1,0-0.1c-1.9,6.7-3.8,13.4-6,20.1C290.1,553,288,555.9,284.2,557.6z M296.6,509.5L296.6,509.5\n  c-0.2,3.5-0.3,7-0.5,10.5h0C296.3,516.5,296.4,513,296.6,509.5z M291.8,491.4L291.8,491.4l0,16.1h0V491.4z M290.6,546\n  c0.2,0.2,0.5,0.3,0.8,0.5c0,0,0,0,0-0.1C291.1,546.3,290.9,546.1,290.6,546c-3.3,4.8-5.3,10.9-12.3,13.1c0,0,0.1,0,0.1,0\n  C285.4,556.9,287.3,550.8,290.6,546z M290.4,556.3c-2.5,5.5-7.4,7.5-11.1,10.5c0,0,0,0,0,0C283.1,563.8,287.9,561.9,290.4,556.3\n  C290.4,556.3,290.4,556.3,290.4,556.3z M284.6,568.6C284.6,568.7,284.6,568.7,284.6,568.6c0.3,0.6,0.7,1.2,1.2,2.2\n  c1.3-2.8,2.6-5.5,4.1-8.8c0,0,0,0,0,0c-1.5,3.3-2.7,5.8-4.1,8.7C285.2,569.8,284.9,569.2,284.6,568.6z M289.5,488.1\n  c-0.4,6.3-0.6,12.7-1.1,19C288.9,500.8,289,494.4,289.5,488.1C289.5,488,289.5,488.1,289.5,488.1z M276.4,609.1\n  c0.9-3.1,2.3-6,4.2-8.8c4-5.9,8.8-11.5,10.6-18.5c-1.8,6.9-6.6,12.5-10.6,18.4C278.7,603,277.3,606,276.4,609.1z M286,534.5\n  c0.2,0.1,0.4,0.3,0.7,0.4c0,0,0,0,0-0.1C286.5,534.7,286.3,534.5,286,534.5c-1.9,2.8-3.9,5.8-5.9,8.7c-0.4-0.2-0.8-0.4-1.2-0.5\n  c0,0,0,0.1,0,0.1c0.4,0.2,0.8,0.4,1.2,0.5C282.1,540.3,284.1,537.4,286,534.5z M286,539.3c0.2,0.2,0.4,0.3,0.6,0.5c0,0,0,0,0-0.1\n  C286.5,539.5,286.2,539.3,286,539.3c-2.1,2.8-4.2,5.7-6.3,8.6c-0.3-0.2-0.6-0.4-0.9-0.6c0,0,0,0,0,0.1c0.3,0.2,0.6,0.4,0.9,0.6\n  C281.8,545,283.9,542.2,286,539.3z M286.4,542.9c0,0,0-0.1,0.1-0.1c-2.2,3-4.5,6-6.7,9c-0.3-0.2-0.7-0.4-1-0.6c0,0,0,0,0,0.1\n  c0.3,0.2,0.7,0.4,1,0.7C282,548.9,284.2,545.9,286.4,542.9z M286,710.6c0-0.6,0.1-1,0.1-1.6C286.1,709.6,286.1,710,286,710.6\n  c0.9-0.4,1.3-1,1.5-1.5C287.4,709.6,286.9,710.1,286,710.6z M286.2,708.2c0-0.3,0.1-0.7,0.1-1.1c-2.4,6.7-8.3,4.2-12,6.5\n  c0,0,0,0.1,0,0.1c3.7-2.4,9.7,0.2,12.1-6.5C286.3,707.6,286.2,707.9,286.2,708.2z M286.1,477.1L286.1,477.1\n  c-0.3,5.1-0.6,10.2-0.9,15.4h0C285.5,487.3,285.8,482.2,286.1,477.1z M269,577.1c-0.8-3.3-3.6-4.9-5-7.7c0,0,0,0,0,0\n  C265.3,572.2,268.2,573.8,269,577.1c0.6,2.6,4.1,3.9,6.8,3c3.8-1.1,6.4-3.8,8.4-7c0.1-0.1,0.1-0.2,0.1-0.3c0,0.1-0.1,0.2-0.1,0.2\n  c-2,3.2-4.6,5.9-8.4,7C273,580.8,269.5,579.6,269,577.1z M284.4,682.2c-0.2,0.6-0.4,1.2-0.5,1.7c-0.1,0.2-0.1,0.3-0.2,0.5\n  c0.1-0.1,0.1-0.3,0.2-0.4C284.1,683.4,284.3,682.8,284.4,682.2z M283.5,684.9c0.1-0.1,0.1-0.3,0.2-0.4\n  C283.6,684.6,283.6,684.8,283.5,684.9z M280.4,690.4C280.4,690.4,280.4,690.4,280.4,690.4c1.3-2.2,2.4-3.8,3.1-5.5\n  C282.8,686.6,281.6,688.2,280.4,690.4z M279.1,700c2.8,0.8,4.5,2.8,4.2,5.8C283.6,702.7,282,700.7,279.1,700\n  C279.1,700,279.1,700,279.1,700z M283.7,523.2c-0.6,2.2-1.4,4.4-2.4,6.5c-0.7,1.5-1.2,2.9-1.7,4.4c0.5-1.5,1-2.9,1.7-4.3\n  C282.3,527.6,283.1,525.4,283.7,523.2z M281.1,704.5C281.1,704.5,281.1,704.5,281.1,704.5c-5.9,1.3-8.3,5.7-10.5,10.3\n  c-1.3,2.8-2.6,5.4-5.7,6.8c-1.5,0.7-2.9,1.8-4.2,2.8c-1.9,1.5-4,1.3-4.8-0.5c-5.1,2.4-11.5,1.8-13.1-1.4h-0.1c1.6,3.2,8,4,13.2,1.5\n  c0.8,1.8,3,2,4.8,0.5c1.3-1.1,2.7-2.2,4.2-2.8c3.1-1.4,4.4-4,5.7-6.8C272.8,710.2,275.2,705.8,281.1,704.5z M278.5,583.7\n  c0.4-0.8,0.9-1.6,1.7-3.1c0,0,0,0,0,0C279.4,582.2,278.9,583,278.5,583.7C278.5,583.7,278.5,583.7,278.5,583.7z M280.1,474\n  L280.1,474c0-2.3,0.1-4.6,0-6.9c-0.1-2.3-0.3-4.5-0.4-6.8c0,0,0,0,0,0c0.2,2.3,0.4,4.6,0.5,6.9C280.2,469.5,280.1,471.7,280.1,474z\n  M275.7,444.9L275.7,444.9C275.6,444.9,275.6,444.9,275.7,444.9L275.7,444.9z M275.2,687.2c-2.7-33.1-1.1-66.6-9.2-99.3\n  c-0.1,0.1-0.1,0.3-0.1,0.4c0,0,0,0,0,0c0-0.1,0.1-0.2,0.1-0.4C274,620.6,272.5,654.1,275.2,687.2L275.2,687.2z M274,439.1\n  c0.3-0.7,0.5-1.3,0.7-2C274.5,437.8,274.3,438.4,274,439.1c-0.5,1-0.9,2.2-1.2,3.3C273.1,441.3,273.5,440.2,274,439.1z M273.7,691.7\n  c-2.6-2-5.8-4.1-8.7-6.8c-1.9-1.7-3-6.5-2.9-8.8c-0.2,2.2,1,7.1,2.8,8.9c2.8,2.6,6,4.7,8.6,6.7H273.7z M273.1,673.4\n  c-0.4,0.1-0.8,0.1-1.2,0.2c-1.8-10.9-3.5-21.9-5.3-32.8v0.2c1.8,10.9,3.5,21.8,5.3,32.7C272.3,673.5,272.7,673.5,273.1,673.4\n  C273.1,673.4,273.1,673.4,273.1,673.4z M272,431.9c0.4-1.6,0.9-3.2,1.3-4.9C272.9,428.6,272.4,430.3,272,431.9z M270.1,412.5\n  c0,0.1,0,0.2,0,0.3c0,1.5,0,3-0.1,4.5c0.1-1.5,0.2-3,0.1-4.6C270.1,412.7,270.1,412.6,270.1,412.5z M267.8,436.4\n  c0.9-3.3,1.9-6.7,2.8-10C269.7,429.7,268.7,433,267.8,436.4z M263.6,536c1.2,2.5,2.3,4.9,3.3,7.5c0.3,0.6,0.4,1.4,0.6,2.7\n  c0,0,0,0,0,0c-0.3-1.3-0.4-2.1-0.7-2.8C265.9,540.9,264.8,538.4,263.6,536c-4.2-9-7.7-18.2-8.7-28\n  C255.9,517.8,259.4,527.1,263.6,536z M263.4,642c0.3-0.4,0.5-0.9,0.5-0.9c0.6,2.8,1.4,6,2.1,9.1c0.2-0.1,0.4-0.1,0.6-0.2v-0.1\n  c-0.2,0.1-0.4,0.1-0.6,0.2c-0.7-3.1-1.4-6.3-2.1-9.1C263.9,641,263.6,641.5,263.4,642c-0.3,0-0.7,0-1,0c-1-5.2-2.1-10.4-3.1-15.6\n  c-0.1,0-0.2,0-0.3,0v0.1c0.1,0,0.2,0,0.3,0c1,5.2,2.1,10.4,3.1,15.6C262.7,642.1,263,642,263.4,642z M266,690.6\n  c-1.1,0.5-2.8,1.2-5.4,2.3c0,0,0,0.1,0,0.1C263.2,691.9,264.9,691.2,266,690.6C266,690.7,266,690.6,266,690.6z M265.5,568.6\n  C265.5,568.6,265.6,568.6,265.5,568.6c-0.4-0.5-0.7-1.1-1.2-1.4c-2.2-1.3-3.9-2.8-4.3-5.5c-0.1-0.4-0.4-0.8-0.8-1.1\n  c-0.1-1.6,0-3.3,0.1-4.7c-0.1,1.3-0.2,3.1-0.1,4.8c0.4,0.3,0.7,0.7,0.8,1.1c0.4,2.7,2.2,4.2,4.3,5.5\n  C264.8,567.6,265.1,568.2,265.5,568.6z M260.7,553C260.7,553,260.7,553,260.7,553c0.7,2.6,1.7,4.4,4.7,3.5c0.1,0.9,0.1,1.5,0.1,2\n  c0.1-0.5,0-1.2-0.1-2.1C262.4,557.4,261.4,555.6,260.7,553z M261.1,571.7L261.1,571.7C261.1,571.8,261.2,571.8,261.1,571.7\n  c1.2,3,2.2,6,3.4,9.2c0,0,0,0,0.1,0c-1.2-3.2-2.2-6.2-3.3-9.2C261.2,571.7,261.2,571.7,261.1,571.7z M263.5,457.4c0-2.5-0.1-5,0-7.5\n  c0-1.1,0-2.3,0-3.4c0,1.1,0,2.3,0,3.4C263.4,452.5,263.5,454.9,263.5,457.4L263.5,457.4z M250,510.9c-0.2,0-0.4,0.1-0.6,0.1\n  c0,0,0,0.1,0,0.1C249.6,511.1,249.8,511,250,510.9c1.3,7.4,2.5,14.7,4,22c1.5,7.2,3.8,14.2,8.5,20.4c0,0,0.1,0,0.1,0\n  c-4.7-6.3-7-13.3-8.5-20.5C252.5,525.6,251.4,518.2,250,510.9z M261.5,695.4C261.5,695.3,261.5,695.3,261.5,695.4\n  c-1.4,3.1-2.7,6.2-4.1,9.4c0,0,0,0,0,0C258.7,701.6,260.1,698.5,261.5,695.4z M256.1,711.9c1.3-1.7,2.7-3.4,4.1-5.1\n  c0.4,0.4,0.7,0.7,1.1,1c0,0,0,0,0-0.1c-0.4-0.4-0.7-0.7-1.1-1.1C258.8,708.5,257.4,710.1,256.1,711.9c-2.3,2.9-5.3,4.5-8.9,5.2\n  c-3.7,0.8-5.2,1.3-6,3.3c0.8-1.9,2.4-2.5,6-3.3C250.8,716.5,253.8,714.9,256.1,711.9z M260.9,688.1c0.2,0.4,0.2,0.9,0.2,1.4\n  C261.1,689,261.1,688.5,260.9,688.1c-0.4-1.2-0.8-2.3-0.7-3.3C260.1,685.8,260.5,686.9,260.9,688.1z M260.1,581L260.1,581\n  C260.1,581,260.1,581,260.1,581L260.1,581z M256.6,625.4c0.6,2.6,1.3,5.5,1.9,8.5c0.3-0.1,0.6-0.1,1-0.2v-0.1\n  c-0.3,0.1-0.6,0.1-1,0.2C257.9,630.8,257.2,627.9,256.6,625.4c0,0-0.2,0.5-0.8,1.7c-2.3-5.4-3.7-10.6-3.3-15.8\n  c-0.4,5.2,1,10.4,3.3,15.9C256.4,626,256.6,625.5,256.6,625.4z M256.4,463.3c-2.3,8.4-4.5,16.6-6.8,24.9\n  C251.8,479.9,254.1,471.6,256.4,463.3L256.4,463.3z M255,491c-0.4,5-0.5,10.1-0.2,15.1c0,0.6,0.1,1.3,0.2,1.9\n  c-0.1-0.7-0.1-1.3-0.1-2C254.5,501,254.7,496,255,491z M252.2,721.3c0.1-0.7-0.2-1.6-0.8-2.9c0,0,0,0,0,0\n  C252,719.8,252.2,720.7,252.2,721.3z M251.6,525.9c-3.9-6.9-5.1-20.5-4-33.5c-1,13,0.1,26.7,4.1,33.6\n  C251.6,526,251.6,526,251.6,525.9z M247.4,496.3c0.7-2.7,1.5-5.4,2.2-8.2C248.8,490.9,248.1,493.6,247.4,496.3L247.4,496.3z\n  M237.6,723.9c0.8,0.3,1.8,0.6,2.8,0.9c0,0,0-0.1,0-0.1C239.4,724.4,238.4,724.1,237.6,723.9C237.6,723.9,237.6,723.9,237.6,723.9z\"\n  />\n  <path d=\"M431.8,314.7c-0.1,0-0.1,0.1-0.2,0.1c0.7-9.5-5.9-18.4-5.9-18.4l-0.1,0.3c-0.9-8.5-5.3-15.6-8.8-20.1\n  c-0.3,0.1-0.6,0.3-0.8,0.4c5.8,9.7,7,18.5,6.4,25.6c-0.2,2.6-0.7,4.9-1.3,7c-1,3.8-2.5,6.7-3.5,8.4c-0.4,0.2-0.9,0.4-1.3,0.6v-5.9\n  h-0.6v8.3c0,0,0,0,0,0v0.1c-0.4-1.3-0.7-2.4-1-3.5v-0.1c-0.1-4.2-5.2-14.3-5.2-14.3c-0.2-0.3-0.5-0.6-0.7-0.8\n  c0.6,1.5,1.5,2.9,2.1,4.4c0.3,0.6,0.3,1.3,0.7,2.6c0,0,0-0.1-0.1-0.1c-0.3-1.2-0.4-1.9-0.6-2.5c-0.6-1.5-1.6-3-2.2-4.5\n  c-0.4-0.9-0.6-1.8-0.7-2.6c-0.3-3.9-0.6-7.8-0.4-11.7c0.1-1.6,0.3-3.2,0.6-4.7c0.1-0.7,0.2-1.3,0.3-1.8c0-0.5,0-1-0.1-1.5\n  c0-0.1,0-0.3-0.1-0.4c0-0.1-0.1-0.3-0.1-0.4c0.1-1.6,0.3-3.9,0.4-5.5h0v-0.1c0.6-0.3,1-0.5,1.5-0.6c0.1-0.1,0.3-0.1,0.4-0.1\n  c2.2-0.6,2.9,0.4,3.5,3.8c0.4,2.2,0.9,4.3,1.4,6.5c0.1-0.2,0.2-0.4,0.3-0.6c0.1-0.2,0.1-0.4,0.1-0.6c0-1.1-0.7-2.4-0.6-3.4\n  c0-0.1,0-0.2,0.1-0.3c0.1-0.4,0.3-0.7,0.7-1c-1-1.8-2.2-3.6-3.6-5.4c0.3,0.3,2.2,2,4.4,4.9c0.1-0.1,0.2-0.1,0.4-0.1\n  c-1.2-1.4-2.3-2.5-3.3-3.8c-1.3-1.7-3.1-3.3-3.5-5.2c-0.9-4.1-0.5-8.5-1.5-12.6c-2.6-11-8.4-19.8-20.1-22.3c-6-1.3-12.5-1-18.8-1\n  c-2.9,0-5.4-0.2-7.6-2.2c-2.5-2.4-5.2-4.4-8-6.2c0,0-0.1-0.1-0.1-0.1c-1.1-0.8-2.1-1.4-3-1.7l0.1,0c-3.8-2.2-7.9-4.1-12-5.9\n  c-2.6-1.1-5.1-2.4-7.3-4.1c-2.7-2.1-2.5-4.7-0.4-7.4c1.3-1.7,2-3.8,2.6-5.8c0.6-1.9,0.6-4,0.9-5.7c1.6-0.6,3.4-0.9,4.3-1.9\n  c4.9-5.6,6.8-12,4.1-19.3c-0.5-1.3-1.1-2.9-2.2-3.7c-0.9-0.7-2.5-0.5-4-0.8c1.5-10.6,0.7-16.8,0.1-19.5c-0.5-3.6-2.9-7.3-6.2-8.3\n  c-6.3-2-12.9-3.9-19.4-4.2c-2.8-0.1-5.5,0.2-8.3,0.6c-1.6,0.3-3.2,0.6-4.8,0.9c-2,0.4-3.9,0.9-5.9,1.3c-5.1,1.1-8.6,4.5-9.4,9.6\n  c-0.7,4.8-0.6,9.9-0.6,14.8c0,1.7,0.5,3.4,0.9,5.5c-3.6-1.4-5.6-0.1-6.1,2.8c-0.7,3.8-1.9,8.1-0.9,11.6c1.2,4.5,2.9,9.9,9.5,10.3\n  c0,1.6-0.3,3,0.1,4.4c0.7,2.5,1.3,5.1,2.7,7.2c2.4,3.3,2.4,6.2-1.1,8.4c-1.8,1.2-3.9,2.2-5.9,3.1c-7.9,3.5-15.5,7.3-22.1,13\n  c-0.7,0.6-1.3,1.2-2,1.7c-0.1,0.1-0.2,0.1-0.2,0.2c-0.2,0.1-0.3,0.2-0.5,0.3c0,0-0.1,0.1-0.1,0.1c-0.1,0.1-0.2,0.1-0.3,0.2\n  c-1.1,0.6-2.3,1.1-3.9,1c-5.2-0.3-10.4-0.4-15.5,0c-7.9,0.5-14.6,3.8-19.3,10.4c-5.1,7.1-7.1,15.2-6.9,23.9c0,1.5-0.9,3.1-1.8,4.4\n  c-2.9,4.2-6.1,8.3-9.1,12.5c-0.1,0.2-0.2,0.4-0.3,0.6c-1.3,1.6-3.8,5.3-4.5,11c-9.3,11.5-4.8,22.9-3,26.6c-0.7-1.1-1.3-2.1-2-3.2\n  v11.6c0,6.1,0.1,12.2,0,18.3c0,3.1-0.6,6.3-0.4,9.4c0.4,10.8,2.2,21.4,5.8,31.5c4.2,11.6,3.7,23,0.8,34.7\n  c-1.3,5.4-0.4,10.1,5.1,12.3c0.5,0.2,0.8,0.6,1.2,0.9c4.3,3.7,8.6,5.3,13.6,0.9c1.6-1.4,4.4-1.6,6-3.1c1.6-1.5,2.3-3.9,3.5-6.3\n  c0.4,0.8,0.6,1.6,1.2,2.1c0.9,0.8,2.2,2.2,3,2c1.1-0.3,2.4-1.7,2.6-2.8c0.6-2.8,1-5.8,0.8-8.7c-0.2-3.5-1.1-6.9-1.7-10.4\n  c-0.6-3.4-1.6-6.5-5.2-8c-1.2-0.5-2.2-1.5-3.3-2.1c-2.8-1.6-5-3.4-4.3-7.1c0.1-0.7-0.5-1.5-0.5-2.3c-0.1-2.9-0.8-5.9-0.2-8.6\n  c1.2-5.2,3.3-10.1,4.8-15.2c2.7-8.6,4.3-17.2,2.6-26.2c-0.8-4.2-1.5-8.5-1.9-12.8c-0.1-1,0.6-2.3,1.3-3.1c2.5-2.6,5.4-4.8,7.9-7.5\n  c0.4-0.5,0.8-0.9,1.1-1.4c0.1-0.1,0.2-0.3,0.4-0.5c1-1.3,2.9-4,3-4.6c0,0,0-0.1,0-0.1c1.5-2.9,3-5.8,4.7-9.1\n  c0.6,2.2,1.1,4.1,1.5,5.8c0,0,0,0,0,0c0.2,1.2,1,5,3.3,8.7c0.2,0.2,0.3,0.5,0.5,0.7c1,1.2,2.1,2.4,3.1,3.6c1.8,2,3.6,4,5.3,6.1\n  c0.4,0.4,0.9,0.9,1,1.4c0.9,3.4,2.1,6.7,2.5,10.2c0.4,3.6,0.2,7.3,0.1,11c-0.3,11-0.7,21.9-1,32.9c0,0.6-0.3,1.2-0.3,1.8\n  c-0.3,5-0.7,10-0.9,15c-0.1,1.9,0.2,3.9-0.1,5.8c-1.8,10.9-3.8,21.9-5.8,32.8c-1.9,10.8-3.1,21.6-2.7,32.6c0.1,3.5-0.4,7.2-1.4,10.6\n  c-4.4,14.5-4.7,28.8,0.4,43.1c3.5,9.9,6.9,19.9,10.4,29.9c0.1,0.2,0.1,0.3,0.2,0.4l-0.7,6.3c-0.3,1-0.4,1.8-0.3,2.6\n  c0,0.2,0.1,0.4,0.1,0.5c0.3,1.5,1.3,2.6,2.3,3.7l-0.2,5.7v0.1c-0.3,3.4-0.7,6.8-1,10.2c0,0,0,0,0.1,0l-0.9,4.8\n  c-1.5,4.1-2.8,8.1-4.4,11.9c-3,7-4.4,13.9-2.1,21.4c1.1,3.7,1.9,7.6,2.5,11.5c1.7,11.2,3.2,22.4,4.7,33.6c0.8,6.3,1.8,12.6-0.6,18.8\n  c-1,2.5-0.9,5.2,0.7,7.7c0.4,0.5,0,2-0.5,2.6c-2.8,3.8-5.8,7.5-8.7,11.1c-1.4,1.7-2.6,3.8-4.5,4.8c-3.6,1.9-7.5,3.4-11.4,4.5\n  c-3.5,0.9-5,3.6-7.5,6c2.8,1.2,5.2,2.5,7.7,3.2c-0.5-1.2-1.2-2.3-1-2.6c1.5-2.1,3.1-4.1,4.8-6c0.3-0.3,0.9-0.4,1.9-0.1\n  c-1.7,2.7-3.5,5.4-5.6,8.8c0.2,0.1,0.4,0.1,0.5,0.2c2.4,0.7,5.4,0,7.5,1.1c4.9,2.4,9.9,1.3,14.9,1.1c2.3-0.1,4.4-0.6,5.3-3.2\n  c0.2-0.6,1.6-1.2,2.3-1.1c3.1,0.2,4.7-1.8,6.1-3.9c1.5-2.2,3.4-3.2,5.9-3.7c3.2-0.7,6.4-1.7,9.5-2.8c2.5-0.9,3.2-3,2.6-5.4\n  c-0.5-2-1.4-4-1.9-6c-1.4-6.1-3.6-12.1-0.1-18.3c0.6-1,0.6-2.6,0.4-3.8c-0.6-2.9-1.4-5.9-2.5-8.7c-1.6-4.2-3-8.2-1.9-12.9\n  c2.2-10.1,4.2-20.1,9.6-29.2c2.7-4.6,4-9.9,3.2-15.4c-0.6-4.8-1.5-9.6-2.1-14.4c-0.9-7.5-2.4-14.7,1.1-22.5\n  c4.4-9.8,4.7-20.8,4.7-31.7c0-6.1,0.3-12.2,0.7-18.3c0.6-7.7,1.4-15.5,2.3-23.2c0.4-3.7,0.7-7.6,2-11c4-10.5,5.6-21.2,4.7-32.4\n  c-0.1-1.5-0.4-3.1-0.6-4.8c1.1,0,1.8,0,2.5,0c0.8,0,1.5,0.2,2.3,0.2c-2.2,9.6-1,18.9,1.1,28.1c1,4.2,3.1,8.2,3.7,12.4\n  c1.4,9.6,2.4,19.2,3.1,28.9c0.7,9.2,0.8,18.5,1.3,27.7c0.6,10.1,1.7,20.1,6.5,29.4c0.6,1.3,0.8,3,0.6,4.4\n  c-0.9,7.7-1.7,15.5-3.2,23.1c-1.7,8.4,0.4,15.8,4.4,22.8c3.8,6.5,5.6,13.5,6.9,20.8c0.8,4.6,2.5,9.2,0.6,13.9c-0.3,0.7-1.1,2-1.1,2\n  c-0.3,1.1-0.8,2.2-1.2,3.2c-1.1,2.8-1.8,5.7-2.5,8.7c-0.3,1.2-0.2,2.8,0.4,3.8c3.5,6.2,1.3,12.2-0.1,18.3c-0.5,2-1.4,4-1.9,6\n  c-0.6,2.4,0.2,4.5,2.6,5.4c3.1,1.1,6.2,2.2,9.5,2.8c2.5,0.5,4.4,1.5,5.9,3.7c1.4,2.1,3,4.2,6.1,3.9c0.8-0.1,2.1,0.5,2.3,1.1\n  c1,2.6,3,3.1,5.3,3.2c5,0.2,9.9,1.3,14.9-1.1c2.1-1.1,5.1-0.4,7.5-1.1c0.2-0.1,0.4-0.1,0.5-0.2c-2.2-3.4-3.9-6.1-5.6-8.8\n  c1.1-0.3,1.7-0.2,1.9,0.1c1.7,1.9,3.3,3.9,4.8,6c0.3,0.4-0.5,1.4-1,2.6c2.5-0.8,4.8-2,7.7-3.2c-2.5-2.5-4-5.1-7.5-6\n  c-3.9-1.1-7.8-2.6-11.4-4.5c-1.8-1-3.1-3.1-4.5-4.8c-3-3.7-5.9-7.3-8.7-11.1c-0.5-0.7-0.9-2.1-0.5-2.6c1.7-2.5,1.7-5.2,0.7-7.7\n  c-1.5-4-1.7-8-1.4-12h0c0.5-7.3,2.4-14.7,3.5-21.9c1.5-9.8,2.8-19.6,4.8-29.3c1.4-6.6,2.9-13.1-0.1-19.5c-4.3-9.3-7.3-18.8-7.5-29\n  c-0.1-2.8,0.2-5.8,1-8.5c0.7-2.7,1.2-5.1,0.6-8c-0.5-2.3-0.6-4.9,0.1-7.1c2.5-7.6,5.3-15.2,8.2-22.7c6.6-16.6,6.9-33.3,1.2-50.3\n  c-0.9-2.6-0.7-5.6-0.8-8.4c-0.2-5.4-0.1-10.8-0.3-16.1c0-0.2-0.1-0.5-0.3-0.7c-0.1-2.8-0.5-6-0.9-9.3v0c-0.1-1-0.3-2.1-0.4-3.1\n  c0,0,0,0,0,0c-1.5-10.7-3.8-22.3-4.8-26.8c-0.9-11.1-1.9-22.2-2.7-33.4c-0.6-8-1.1-16.1-1.3-24.1c-0.2-7.6-0.2-15.2,0.3-22.8\n  c0.1-2.4,1.7-5.1,3.5-6.8c4.9-4.6,8.7-9.8,11.1-16.1c1-2.7,1.8-5.6,3-9c0.4,1,0.5,1.4,0.7,1.7c3.1,7.9,7.7,14.8,14.5,19.8\n  c3.2,2.4,3.4,4.6,2.8,7.9c-0.9,5.2-1.8,10.3-2.2,15.6c-0.8,10,2.5,19.2,5.8,28.4c2,5.6,4.2,11.2,1.2,17.2c-0.3,0.5-0.4,1.2-0.3,1.8\n  c0.6,2.4-0.3,3.8-2.4,4.9c-2,1.1-4,2.4-5.8,3.8c-1.1,0.8-2.5,1.8-2.8,3c-1.3,5.2-2.3,10.5-3.4,15.8v0c-0.1,0.1-0.1,0.2-0.1,0.2\n  s-1.7,6.6-0.5,8.1c1.1,1.4,3.7,4.5,5.8,1.5c0.1,0.2,0.2,0.3,0.3,0.5c1-1.5,1.8-2.6,2.7-4c0.6,1.6,1,3.1,1.8,4.3\n  c0.8,1.2,1.9,2.3,3.1,3.1c2.4,1.5,4.8,2.9,7.4,3.9c1.6,0.6,3.9,1,5.3,0.3c3.1-1.5,5.9-3.5,8.7-5.6c1.2-0.9,2.6-2.6,2.6-4\n  c0-4.3-0.5-8.6-1-12.8c-0.4-3.7-1.6-7.4-1.4-11.1c0.3-4.9,1.3-9.8,2.7-14.4c4.2-13.7,7-27.6,6.3-42.1c-0.4-7.8-0.5-15.7-0.6-23.6\n  c0-1.7,0.5-3.4,0.8-5.1C431.5,318.8,431.6,317.1,431.8,314.7z M230.1,721.4c-0.2-0.3-0.4-0.5-0.6-0.8c1.5-1.3,2.9-2.6,4.4-3.9\n  c0.2,0.3,0.5,0.6,0.7,0.9C233.5,719.5,232.6,721.5,230.1,721.4z M289.5,200.2c-0.8-5.6-1-11.3-2-16.8c-0.6-3.5-1.6-7-3.2-10.1\n  c-1.3-2.4-2.7-1.9-4.2,0.6c-1.5,2.4-1.3,4.5,0.7,6.5c-0.6-6.6,0.1-6,1.7-7.8c1,1.4,3.4,2.3,1.3,4.6c1.8,1.2,2.4,2.9,1.5,5.1\n  c-0.3,0.6-0.5,1.2-0.7,1.8c0.4,0.1,1.1,0.1,1.1,0.2c0.2,2.2,0.3,4.4,0.4,6.5c-0.3,0.2-0.5,0.4-0.8,0.6c-1.2-1.2-2.8-2.1-3.5-3.5\n  c-1.2-2.5-1.6-5.5-3-7.9c-1.9-3.4-1.2-6.5,0.5-9.4c0.6-1.1,2.3-2,3.6-2.2c0.6-0.1,1.7,1.6,2.3,2.6c1.2,1.9,2.3,4,3.4,6\n  c0.4-0.1,0.7-0.2,1.1-0.4c0.3-4.1,0.7-8.2,1-12.3c0.3-5-1.1-10.2,2.5-14.7c0.1-0.2,0.1-0.5,0.1-0.9c4-1.7,13.4-4.7,16.2,2\n  c6.9-3.3,17-7.3,20.7,1c0.1,0.3,0.2,0.5,0.3,0.8c0.4,3.2,0.6,5.9,0.8,8.1c0.1,2.4,0.1,4.2,0.1,5.6c-0.2,0-0.4,0.1-0.6,0.1\n  c0.5,3.4,1,6.9,1.5,10.4c0.3,0,0.6,0.1,0.8,0.1c0.2-0.9,0.2-1.9,0.6-2.7c0.8-1.4,2.1-2.5,2.7-3.9c0-0.1,0.1-0.1,0.1-0.2c0,0,0,0,0,0\n  c0.1-0.2,0.2-0.3,0.3-0.5c1.1-1.5,2.7-1.8,4.3-0.5c1.8,1.5,3.3,6.3,2.5,8.6c-0.5,1.7-1,3.5-1.7,5.1c-0.9,2.1-1.8,4.3-3,6.2\n  c-0.6,1-2.1,1.5-3.9,2.7c0.3-4.6,0.5-8.2,0.7-11.8c-0.1,0-0.1,0-0.1,0c2.7-9.3,4.6,0.2,4.6,0.2s0.6-1.7,1.1-4s-0.9-2.7-3.2-2.7\n  c-2.1,0-3.4,6.1-3.6,7.3c0,0,0,0,0,0c-0.2,0.4-0.4,0.8-0.4,1.1c-0.5,4.7-0.8,9.5-1.3,14.2c-0.5,5.5-2,10.3-8.2,12\n  c-3.1,0.9-6.1,2.3-9.3,3.5c-0.1-0.5-0.2-0.9-0.3-1.1c-1.3,0.8-2.8,2.2-4.1,2.2c-1.5-0.1-2.9-1.5-4.3-2.3c0,0.1-0.1,0.7-0.3,1.4\n  c-0.6-1-1-1.5-1.4-2.1c0,0.3-0.1,0.6-0.1,0.9c-0.7,0.1-1.4,0.4-2,0.2c-2.4-1-4.7-2.3-7.2-3.2C292.1,206.2,290,203.7,289.5,200.2z\n  M314.4,244.6c1-0.7,2-1.4,3.2-2.2c0.7,0.7,1.3,1.3,1.3,1.4c-0.6,2-1.3,3.8-1.9,5.5c-0.4,1.3-0.9,3.1-1.1,4.4\n  c-0.8,5.9-1,11.9-1.4,17.9c-0.3,5-0.4,9.9-0.6,14.9c-0.3,8.4,1,11.1,8.8,13c3.5,0.9,6.8,2.6,10.4,3.3c3.6,0.8,5.6,2.8,6.6,6.2\n  c0.8,2.7,1.8,5.3,2.3,8.1c1,5.7,1.7,11.4,2.5,17.1c0.5,3.6,1.6,4.4,5.1,3.6c3.3-0.8,5.9-2.6,7.8-5.4c0.7-1,1.6-1.9,2.5-2.9\n  c-2.1,5.8-5.5,10.3-12,11.4c-5.2,0.9-5.8,1.8-5.8,7.1c0,1.2,0.1,2.4,0.3,3.6c0.4,3.4,0.9,6.8,1.4,10.2c0,0,0.1,0,0.1,0\n  c-0.4,1-1,2.1-1.2,3.2c-1.7,9.6-2.9,19.3-5,28.9c-1.7,8-3.1,16.3-7.9,23.4c-0.3,0.5-0.3,1.2-0.5,2.3c0.2-0.1,0.5-0.2,0.7-0.3\n  l-8.3,12c-3.5,3.2-6.5,6.9-11,7.4c-6.5-2.4-14.1-13-17.7-18.7c-0.1-0.7-0.4-1.4-0.7-1.9c-3.1-4.7-5-9.9-6.2-15.4\n  c-1.9-9-3.9-18.1-5.7-27.2c-1-4.8-1.7-9.7-2.5-14.5c-0.1-0.7-0.1-1.4,0.1-2.1c0.5-3.6,1.1-7.2,1.5-10.8c0.4-4.9-0.7-6.4-5.7-7\n  c-5.3-0.7-11.4-6.9-11.9-11.7c1,1.2,1.6,1.9,2.2,2.7c2.6,3.4,5.7,6.1,10.4,5.8c0.9-0.1,2.2-0.9,2.4-1.6c1-5.6,1.8-11.1,2.7-16.7\n  l1.4-6.7c0.1,0,0.1,0.1,0.2,0.1c0.6-6,3.5-9.6,9.5-10.6c3.9-0.7,7.7-2.4,11.5-3.7c4-1.4,5.4-3.2,5.3-7.4\n  c-0.1-10.1-0.1-20.2-0.8-30.2c-0.4-5.6-0.2-11.7-4.6-16.4c0.5-0.9,1-1.7,1.5-2.5c1.4,0.9,2.7,1.8,3.9,2.6c0.2-0.2,0.4-0.5,0.6-0.7\n  c-1-1.6-2.2-3.1-3.1-4.8c-1.2-2.3-2.2-4.7-3.1-7.2c-1.7-5-3-10-4.7-15c-0.8-2.4-2.1-4.7-3.2-7c1,0.2,1.6,0.6,2.2,1.1\n  c5.5,4.5,7,11,8.6,17.4c1.2,4.5,2.2,9.1,3.3,13.7c0.3-0.1,0.6-0.1,0.9-0.2c-0.8-5.8-1.6-11.7-2.3-17.4h3.6c0.4,0.1,0.9,0.1,1.4,0\n  h3.4c-0.8,5.9-1.6,11.8-2.4,17.8c0.9-0.5,1.2-1.1,1.4-1.8c2-7.4,3.7-14.9,6.2-22.1c1.2-3.4,4.2-6.1,6.3-9.2c0.3,0.2,0.7,0.4,1,0.7\n  c-0.4,0.9-0.8,1.8-1.1,2.6c-2,5.4-4.6,10.6-6,16.1c-1.2,4.9-2.6,9.5-5.8,13.4c-0.4,0.5-0.5,1.3-0.8,2\n  C314.1,244.4,314.2,244.5,314.4,244.6z M262.2,348.9c4.1,3.5,8.3,7,13,11c-1.4-0.2-2.2-0.3-3-0.4c0.1,0.2,0.1,0.4,0.2,0.4\n  c1.2,0.4,2.5,0.8,3.7,1.2c1.9,0.7,2.6,1.8,1.3,3.6c-0.1,0.1,0.2,0.5,0.3,0.8c0.1,1.3,0.3,2.5,0.4,4.1c-1.1-0.2-1.9-0.4-2.2-0.4\n  c0.7,1,1.6,2.2,2.5,3.4c-0.3,0.3-0.6,0.7-0.9,1c-2.2-0.9-4.4-1.8-6.6-2.7c-0.1,0.2-0.2,0.4-0.2,0.5c1.7,0.8,3.3,2.1,5,2.3\n  c2.9,0.4,2.6,2.2,3,4.9c-4.2-2.8-7.8-5-11.1-7.4c-4.8-3.4-5.4-8.7-5.9-14C265,354.7,261.5,351.6,262.2,348.9z M262.8,338.2l0.5-0.7\n  c2.8,4.3,7.2,4.2,12.2,4.6c-1.2,0.7-1.6,0.9-2,1.1c4.2,0.2,4.5,0.7,3.3,7.4c-2-0.8-3.9-1.6-5.7-2.4c-0.1,0.1-0.2,0.2-0.3,0.4\n  c1.4,0.8,2.9,1.5,4.2,2.3c0.6,0.4,1.6,0.9,1.6,1.4c0,2-0.2,4.1-0.4,6.7c-5.5-5.2-12-8.9-13.7-17.1c0.8,0.3,1.2,0.5,1.8,0.8\n  C263.8,340.9,263.3,339.5,262.8,338.2z M261.7,369.9c5.4,3.9,10.8,7.6,16,11.6c2.6,2,4.1,4.6,3.6,8.1c-0.2,1.4,1.4,3.1,1.2,4.5\n  c-0.5,4.6,2.6,8.3,2.4,12.9c-0.1,2.1,2,4.3,3.1,6.4c0.3,0.7,0.5,1.4,0.8,2.1c-0.2,0.2-0.5,0.4-0.7,0.5c-1.5-1.7-3-3.4-4.6-5.2\n  c-0.1,0.4-0.2,0.8-0.4,1.8c-2.1-2.6-3.8-4.9-5.6-7.1c-1.9-2.3-3.6-4.7-6-6.8c-3.8-3.4-6.5-8.4-8.9-13.2c-1.2-2.4-0.9-5.5-1.2-8.3\n  c-0.2-1.8-0.4-3.6-0.5-5.4c0.2-0.1,0.4-0.2,0.6-0.3c0.3,0.4,0.5,0.9,0.8,1.3c0.1-0.1,0.2-0.2,0.4-0.3\n  C262.3,371.7,262,370.7,261.7,369.9z M281.4,305.1c-1.1,2-2.3,4-3.3,6c0.1,0.1,0.3,0.2,0.5,0.4l0,0.1h0l-1.7,5.2\n  c-0.3,0.2-0.5,0.4-0.8,0.6c0.3,0.1,0.5,0.1,0.8,0.1c0.1,1.2,0.2,2.5,0.2,3.2h0c-2.8,0-5.2-0.1-7.6,0c-1.8,0.1-3-0.4-4.1-1.9\n  c-0.2-0.2-0.4-0.4-0.6-0.5c-1.9-3.1,5.5-8.2,7.6-9.5c0,0,0,0,0.1,0c0.6,0.5,1.3,0.4,2.4-0.4c1.9-1.5,4-2.7,6-4\n  C281,304.5,281.2,304.8,281.4,305.1C281.4,305,281.4,305.1,281.4,305.1C281.4,305.1,281.4,305.1,281.4,305.1z M266.8,323.5\n  c0.4-0.5,1.2-0.9,1.7-0.9c2.8,0.1,5.5,0.3,8.3,0.5c-0.1,2-0.3,2.1-4.1,1.9c1.2,0.3,2.5,0.5,3.8,0.8c0.1,0.3,0.2,0.5,0.3,0.8\n  c-1.1,0.4-2.2,0.8-2.8,1.1c0.5,0.4,1.5,1,2.4,1.7c-1.5,0.4-3.1,0.8-4.6,1.2c1.1,0.2,2.3,0.5,4,0.9c-0.1,0.8-0.3,1.7-0.5,3.1\n  c-1.8-0.9-3.2-1.6-4.7-2.3c-0.1,0.2-0.2,0.3-0.2,0.5c1.7,1,3.5,1.9,5.2,2.9c-0.2,0.3-0.4,0.6-0.6,0.9c-5.8-0.3-8.2-5.7-12.3-8.4\n  C264.2,326.5,265.5,325,266.8,323.5z M197.8,296c0-0.6,0.1-1.1,0.1-1.7c0.1-1.5,0.4-3,0.8-4.4c0.4-1.5,0.9-2.9,1.6-4.3\n  c-0.2,0-0.3-0.1-0.5-0.1c0,0,0-0.1,0-0.1c0.2,0,0.3,0.1,0.5,0.1c0.5-1.1,1.2-2.1,1.9-3.1h0.1c-0.8,1-1.4,2-2,3.1\n  c0.1,0,0.1,0,0.2,0.1c1.9-3.2,3.8-6.3,5.7-9.5c0.2,0.1,0.3,0.2,0.5,0.3c0,0,0,0.1,0,0.1c-0.2,1.5-0.4,3.1-0.6,4.6\n  c0.9-1.7,1.2-3.4,1.7-5.1c0.5-1.4,1.2-2.8,1.8-4.1c0.1-0.1,0.1-0.2,0.2-0.3c0.3,0.1,0.5,0.2,0.8,0.3c0,0,0,0.1,0,0.1\n  c-0.3-0.1-0.5-0.2-0.8-0.3c0,0.1-0.1,0.2-0.1,0.3c0.1,0.8,0.3,1.7,0.5,2.9c0,0.1,0,0.2,0,0.2c0,0,0,0.1,0,0.1\n  c1.3,7.5,3.8,21.3,5.5,21.4h0c0.1-0.2,0.3-0.3,0.4-0.4c-0.1-0.1-0.2-0.2-0.4-0.3c0,0,0,0,0-0.1c0,0,0,0,0,0c0.1-0.1,0.1-0.2,0.2-0.3\n  c-1.7-5.3-2.8-13.3-3.4-19.4c0,0,0-0.1,0-0.1c0.1,0,0.2,0.1,0.2,0.1c1-3.6,2-7.1,3.1-10.6c0.3-1.2,0.7-2.4,1-3.6c1-3.5,2-7,3-10.5\n  c-0.1-0.1-0.3-0.1-0.4-0.2c-1.9,5.7-3.8,11.3-5.8,17c0-1.9,0.1-3.7,0.2-5.5c0.2-3.1,0.7-6.2,1.5-9.1c0.2-0.7,0.4-1.5,0.7-2.2\n  c0,0,0,0,0,0c0.2-0.7,0.5-1.3,0.7-1.9c0.1-0.3,0.2-0.5,0.3-0.8c0.1-0.3,0.3-0.6,0.4-0.9v0c1.7-3.6,4.2-6.9,7.6-9.8\n  c6-5,16.5-7.2,21.3-4.9c0,0-0.1,0-0.1,0c0,0,0.1,0,0.1,0.1c-1.2,0.3-2.2,0.6-3.3,0.9c-1,0.3-2,0.6-3.1,0.8c0,0.4,0,0.8,0.1,1.1\n  c9-5.3,17.3-1.2,25.8,0.8c-0.1,0-0.1,0-0.2,0c0.1,0,0.1,0,0.2,0.1c-4.3,0.5-8.7,1-13,1.6c7.1,0,14-1.6,21.1-1.2\n  c0,0.3,0.1,0.5,0.1,0.8c0,0,0,0,0,0c0,0,0,0,0,0.1c-0.5,0.2-1,0.3-1.8,0.6h16c1.1-0.5,1.7-0.9,2.3-1c2.3-0.3,4.6,0.6,6.9-1.1\n  c0.6-0.5,3.3,0.4,3.9,1.4c0.7,1,0.2,2.9,0.2,4.5h0v0.1c-1.2-0.1-2.5-0.1-3.7-0.2c-0.5,0-1-0.2-1.5-0.3c-6-1.6-12-1-18.1,0\n  c-11.7,1.8-22.2,5.7-28.8,16.4c-6.1,9.8-15.1,16.6-24.1,23.5c0,0,0.1-0.1,0.1-0.1c0,0,0,0-0.1,0.1c0.8-1.3,1.6-2.7,2.5-4.1\n  c-0.2-0.1-0.4-0.2-0.6-0.3c-3.7,6-7.5,12-11.2,18.1c0.1,0.2,0.1,0.4,0.2,0.5c0,0,0,0,0.1,0.1c0,0,0,0,0,0c0,0,0,0,0,0.1\n  c-0.1,0.2-0.2,0.3-0.4,0.4c-1,1.1-2.2,3-3.2,5.1c-0.2,0.3-0.3,0.6-0.4,0.9c-0.2,0.4-0.3,0.7-0.5,1.1c0,0,0,0,0,0.1\n  c-0.4,0.9-0.7,1.8-1,2.6c-0.1,0.3-0.2,0.6-0.3,0.9c-0.2,0.6-0.4,1.1-0.5,1.7c0,0,0,0,0.1,0c0,0,0,0,0,0.1c0,0,0,0,0,0\n  c-0.1,0.2-0.2,0.4-0.3,0.7c-0.1,0.4-0.3,0.8-0.4,1.2c-0.1,0.4-0.2,0.8-0.3,1.1c-0.1,0.3-0.2,0.6-0.3,0.9c0,0,0,0,0,0.1\n  c0,0.1-0.1,0.2-0.1,0.3c0,0,0,0,0,0.1c0,0.1-0.1,0.2-0.1,0.2c-0.1,0.2-0.1,0.3-0.1,0.3c-0.6,1.2-1.2,2.5-2,4.1\n  c-0.3-1.1-0.4-1.6-0.6-2.1c-0.1,0-0.2,0-0.3,0.1v3.4l0,0v0.1c-1.3-1.2-2.7-2.5-4.1-3.7c0,0,0,0,0,0C199.6,311,197.5,303.5,197.8,296\n  z M191.6,309c0-0.1,0-0.1,0-0.2c0-0.5,0.1-1,0.1-1.4c0.2-2.5,0.6-4.6,1.1-6.4c0.2-0.6,0.4-1.2,0.5-1.7c0.1-0.3,0.2-0.6,0.3-0.9\n  c0.7-1.9,1.4-3.2,1.8-3.9c0,0,0,0.1,0,0.1c-0.1,0.5-0.1,1.1-0.2,1.6c-0.1,1.5-0.1,3,0.1,4.7c0.1,0.5,0.1,1.1,0.2,1.6\n  c0.1,1,0.3,2,0.6,3c0.7,3.1,2,6.6,3.8,10.3c0.3,0.6,0.6,1.2,0.9,1.8c0.1,0.2,0.2,0.3,0.3,0.5c1.4,2.4,2.8,4.9,4.2,7.4\n  c2.4,4.2,5.3,8.2,6.9,12.7c3.9,10.9,3.8,22.5,4.2,33.9c0,0,0,0,0,0v0.1c0,0.1-0.3,0.2-0.5,0.3c-1-3.3-1.7-6.7-3.2-9.8\n  c-2-4.1-3.1-8.6-5.7-12.6c-4.8-7.3-8.3-15.4-12.3-23.2c-0.1-0.1-0.1-0.2-0.1-0.4c0-0.4,0.2-0.9,0.3-1.4c-0.6-1-1.3-2.1-2-3.2\n  C191.8,317.1,191.5,312.7,191.6,309z M192.2,326.9L192.2,326.9L192.2,326.9c1.1,2.1,2.1,4.3,3.3,6.4c3.9,7.2,8.2,14.2,11.6,21.6\n  c3.4,7.6,6.1,15.5,8.8,23.4c0.1,0.4,0.2,0.9,0.1,1.4c-0.1,1-0.6,2.3-0.8,3.5c0,0,0,0.1,0,0.1c-1-1.8-1.9-3.3-2.8-4.9\n  c-0.1,0-0.2,0.1-0.2,0.1c0.4,0.9,0.7,1.7,1.1,2.5l-0.1-0.1l-3.7-4.3l-0.1-0.1c-2.8-9.4-7.5-17.9-12.3-26.4\n  C193.2,343,191.5,335.3,192.2,326.9z M206.4,394.6c-0.6,0.5-1.2,0.9-2.2,1.6c-1.6-12.1-3.2-23.7-4.8-35.3c0.4-0.1,0.9-0.3,1.3-0.4\n  c2.8,6.3,5.5,12.5,8.4,18.9c-0.6,0.1-1,0.2-1.1,0.2c1.6,3.9,3.1,7.8,4.7,11.8l0.1,2c-2.6-0.2-5.1-0.3-7.5-0.5\n  C205.3,393,205.8,393.7,206.4,394.6z M211.5,438.7c-5.3-1.5-9.1-5.7-13.8-8.2c0.2-0.3,0.4-0.7,0.7-1c4.6,2.8,9.1,5.5,13.7,8.3\n  C211.8,438.1,211.7,438.4,211.5,438.7z M201.7,428.4c0.5-0.2,1.1-0.3,2.6-0.8c-3.3,0-6.1,0.4-5.6-4.5c6.3,4.3,12.1,8.2,18,12.2\n  C214.2,436.5,204.7,432.1,201.7,428.4z M219.1,398.3c1.2,1.4,2.5,2.8,4,3.6c4.7,2.3,6.5,6.2,7,11.2c0.3,2.8,1,5.5,1.7,8.3\n  c0.1,0.6,0.2,1.1,0.3,1.5c0,0.2,0,0.4,0,0.6c0,0.1,0,0.2-0.1,0.3c-0.2,0.9-0.9,1.3-2.6,1.8c0.3,1.4,0.6,2.8,0.9,4.6c0,0,0,0,0,0\n  c0,0,0,0.1,0,0.1c-3.1-2-2.8-5.1-3.2-7.7c-1.1-6.9-4.1-10.5-10.9-11.8c-0.4-0.1-0.9,0-1.4,0.2c1.8,1,2.9,1.6,3.5,2c0,0,0,0,0,0.1\n  c0,0,0,0,0,0c-1.3,2.5-2.5,4.7-3.7,7v-9c-0.4,0.2-0.7,0.4-0.9,0.6c-0.1,0.2-0.2,0.4-0.3,0.6c0,0.1-0.1,0.2-0.1,0.3\n  c0,0.1-0.1,0.2-0.1,0.4c-0.1,0.5-0.2,1.1-0.3,1.7c-0.3,3.7,0.8,9.1,2.4,10.7c2.4,2.3,3.6,2.3,5.8-0.2c1-1.1,1.9-2.3,2.9-3.6\n  c0.7,1.2,1,2.5,0.9,3.8c0,0.5-0.1,1-0.3,1.4c-0.5,2.1-1.9,4.2-4.1,6c-1.7,1.5-6.8-0.1-9.6-3.1c0,0,0,0,0.1,0c0,0,0,0-0.1-0.1\n  c0.6-0.3,1.1-0.5,1.7-0.7c-1.5-0.3-2.9-0.7-5.2-1.2c0,0,0.1,0,0.1-0.1c0,0-0.1,0-0.1,0c1.6-0.8,2.3-1.1,3.5-1.7\n  c-2-0.2-3.4-0.3-5.5-0.5c0,0,0.1-0.1,0.1-0.1c0,0-0.1,0-0.1,0c1-0.7,1.5-1,1.8-1.2c-1.4-0.6-2.8-1.7-4.3-1.7c-2.3,0-3-0.9-3-2.4\n  c0-0.3,0-0.5,0-0.8c0-0.2,0-0.3,0.1-0.5c0.4-2.8,1-5.5,1.5-8.3c0,0,0-0.1,0-0.1c0-0.2,0.2-0.5,0.3-0.7c0,0,0,0,0,0c0,0,0,0,0,0\n  c-0.8-0.8-1.1-1.6-1.2-2.3c0-0.2,0-0.4,0-0.6c0-0.1,0-0.2,0-0.3c0.1-0.2,0.1-0.4,0.2-0.6c0.5-1.4,1.7-2.7,2.2-4.2\n  c-1.1,0.8-1.8,1.4-3.1,2.4c-1.6-9-3.1-17.6-4.6-26c0,0.1-0.1,0.2-0.1,0.2c-0.2,0-0.5,0-0.7,0c-1.2-11.1-2.3-22.1-3.5-33.2\n  c0,0,0,0,0,0c0,0,0-0.1,0-0.1c0.3-0.1,0.6-0.2,0.9-0.3c0.6,1.6,1,3.3,2,4.6c3.9,5.4,3.1,11.5,3.7,17.5c1,9.8,3.3,19.5,5.1,29.2\n  c0.1,0.6,0.6,1.2,0.9,1.7c3.3-1.2,6.5-2.3,9.6-3.5c0,0,0,0,0-0.1v-0.1c0-0.1,0-0.3,0-0.4c0-0.1,0-0.2,0-0.3l0-0.1l-0.3-1.3\n  c0,0,0,0,0,0l0-0.1c0,0,0,0,0,0c-1-4.1-2-8.2-3-12.3c-0.1-0.3-0.2-0.6-0.3-1l0,0l3.7,3.5l0.1,0.1c1.2,2.8,2.5,5.6,3.6,8.5\n  c0.3,0.7,0.3,1.4,0.4,2.2c0,0,0,0.1,0,0.1c0.1,0.7,0.1,1.5,0.2,2.2C218.2,396.5,218.5,397.6,219.1,398.3z M220.6,414.9\n  c0.3-0.2,0.6-0.3,0.9-0.5c0.9,1.6,1.9,3.1,2.5,4.8c0.1,0.3-0.9,1.2-1.8,2.3C221.5,418.6,221.1,416.8,220.6,414.9z M219.4,424.4\n  c-2.4-2-2.9-5.3-1.2-8.9c1,2.4,2,4.6,2.6,6.8C220.9,422.8,219.9,423.7,219.4,424.4z M224.6,343.7c0.3,5.7,0.3,11.4-2.3,16.8\n  c-1.1,2.4-1,5.4-1.4,8.2c-0.2,0-0.5-0.1-0.7-0.1v-16.2c-0.4,7-0.9,14.3-1.3,21.7c-0.3,0-0.5,0-0.8,0c0-14.5-0.1-29.1-5.9-42.9\n  c3.7-1.7,7.3-3.3,10.6-4.8C223.4,332.2,224.3,337.9,224.6,343.7z M242.5,293.4c-0.5,2-1.1,3.9-1.8,5.8c-1,2.9-2.2,5.7-3.6,8.4\n  c0,0.1-0.1,0.2-0.1,0.3c0,0.1,0,0.1-0.1,0.1c-0.1,0.2-0.1,0.4-0.2,0.6c0,0.1-0.1,0.3-0.1,0.4c0,0.1-0.1,0.3-0.1,0.4c0,0,0,0.1,0,0.1\n  c-0.3,0.5-1.2,2-1.8,3.2c-5.6,8.7-14.2,13.3-23.3,17.6c-0.3-0.6-0.4-1.2-0.8-1.5c-2.5-2.1-3.4-4.5-3.2-7.1c0-0.4,0.1-0.7,0.1-1.1\n  c0-0.2,0.1-0.4,0.1-0.6c0.1-0.4,0.2-0.8,0.3-1.2c0.1-0.4,0.2-0.7,0.3-1.1c0,0,0,0,0-0.1c0.1-0.2,0.1-0.3,0.2-0.6\n  c0.2-0.5,0.4-1.2,0.6-2c0.1-0.2,0.1-0.4,0.2-0.5c0.1-0.2,0.1-0.5,0.2-0.7c0.5-1.7,0.9-3.6,1.2-4.8c0,0,0-0.1,0-0.1\n  c1.5-3,5.3-9.8,6.9-13c0.3-0.2,0.5-0.3,0.7-0.5c1.8-2.8,3.4-5.7,5.3-8.4c1.1-1.6,2.4-3,3.9-4.2c2.4-2,5.1-3.7,7.5-5.6\n  c2.4-1.9,4.6-4,6.9-6c0.1,0.1,0.2,0.3,0.4,0.4c0,0,0,0,0,0.1c0,0,0,0,0,0c-1.4,1.8-2.8,3.7-4.2,5.5c0.2,0.1,0.4,0.3,0.6,0.4\n  c2.5-3.4,5.1-6.7,7.6-10.1c0.2,0.1,0.4,0.3,0.6,0.4c0,0,0,0,0,0.1c0,0,0,0,0,0c-1.3,2.4-2.6,4.8-3.9,7.3c0.1,0.1,0.3,0.1,0.4,0.2\n  c0.6-0.7,1.2-1.4,1.8-2.1c0.2,0.1,0.3,0.2,0.5,0.2c0,0,0,0.1,0,0.1h0c-0.9,2.5-1.7,5-2.6,7.5c0.1,0,0.2,0.1,0.2,0.1\n  c0.6-0.9,1.2-1.8,2.1-3.1c0,0,0,0.1,0,0.1C244.7,283.4,243.8,288.5,242.5,293.4z M257.5,313c0,1.1-0.8,2.3-1.4,3.9\n  c0.2,0.6,0.6,1.9,1.3,4c-1.2-0.6-1.6-0.8-2-1.1c1.1,1.9,2.1,3.7,3.1,5.4c-0.3,0.2-0.6,0.4-0.9,0.6c-2.1-3-4.2-6-6.5-8.8\n  c0,0-0.1-0.1-0.1-0.1c0,0,0,0,0-0.1c-0.7-1.1-1.9-3-3-5.1c-0.3-0.6-0.6-1.1-0.8-1.7c-0.4-1-0.8-2-1.1-2.9c-0.1-0.4-0.2-0.8-0.3-1.2\n  c0,0,0,0,0-0.1c-0.2-0.7-0.6-1.4-0.9-2c-1.5-3-2-5.9-1.1-9.3c1.3-4.9,1.9-10.1,2.9-15.1c0.7-3.6,1.5-7.2,2.3-10.7\n  c0.3-0.1,0.7-0.2,1-0.2c0.9,1.5,2.1,2.8,2.6,4.4c0.5,1.5,0.7,3,0.9,4.6v0c0.3,2.9,1.1,8.7,2.6,13.4c0.6,2.1,1.8,4,3,5.6\n  c-1,1.9-2.2,6.5-2.2,7.2C257,306.9,257.5,310,257.5,313z M259.4,326.7v-26c-0.1,0-0.1-0.1-0.1-0.2c0-0.1,0-0.2,0.1-0.4\n  c0.1-0.5,0.5-1.5,0.8-2.2c2.3,2.6,4.6,4.1,4.6,4.1l0.3,0c3.4,2,7.3,2.5,11.4,3.1c0,0-0.1,0-0.1,0.1c0,0,0.1,0,0.1,0\n  c-1.6,0.7-3.2,1.4-4.9,2.2v0h0c-8.2,3.1-9.3,10-9.4,10.9v0l0,0c0.1,0.1,0.1,0.1,0.2,0.2c1.1,1.2,2.2,2.3,3.5,3.7c0,0,0,0-0.1,0\n  c0,0,0,0,0,0c-1.9,1.7-3.7,3.2-5.5,4.8C260,326.9,259.7,326.8,259.4,326.7z M253.3,449.4c0.2-0.8,0.9-1.8,0.6-2.3\n  c-2.2-3.2,0-6.3,0.4-9.2c1.5-11,3.6-21.9,5.2-32.9c0.4-2.6,0.1-5.3,0.5-8c2.7,4.2,0.9,9,2.1,13.4v-9.6c4.1,3.2,1.5,7.8,2.9,12.3\n  c0.3-3.3,0.5-5.9,0.8-8.5c2.6,1.4,1.9,3.9,1.7,6.1c-0.2,2.2-0.7,4.4-0.4,6.7c0.5-3,1.1-6.1,1.6-9.1c0.2-0.1,0.4-0.2,0.6-0.2\n  c0.2,1,0.6,1.9,0.5,2.9c-1.4,11.7-4.6,22.9-8.8,33.9c-2.9,7.4-5.9,14.7-8.8,22.1c-0.4-0.1-0.8-0.2-1.2-0.3\n  c1.6-5.6,3.3-11.3,4.9-16.9c-0.2-0.1-0.3-0.1-0.5-0.2c-1.3,4.1-2.6,8.1-3.9,12.2c-0.2-0.1-0.5-0.1-0.7-0.2\n  C251.5,457.4,252.4,453.4,253.3,449.4z M252.6,611.3c0-0.3,0.1-0.7,0.1-1c0-0.4,0.1-0.8,0.1-1.1c0.1-0.8,0.3-1.6,0.5-2.4\n  c0,0,0-0.1,0-0.1c1.6-5.9,4-11.6,5.8-17.5l0.2-1.7v0l0.7-6.4v0c0,0,0,0,0,0c0.3-1.4,0.5-2.8,0.8-4.4c1,3.2,3.5,13.6,5.3,21.3\n  c0,0.1,0,0.1,0,0.1c1.4,13.1,2.9,26.3,4.2,39.4c0.8,7.9,1.2,15.8,1.7,23.7c0.1,2.1,0.1,4.3,0.2,6.4c0.1,2,0.5,4,0.7,6c0,0,0,0,0,0\n  c0,0,0,0.1,0,0.1c-0.4,0.1-0.8,0.1-1.2,0.2c-1.8-10.9-3.5-21.8-5.3-32.7v9h0v0.1c-0.2,0.1-0.4,0.1-0.6,0.2c-0.7-3.1-1.4-6.3-2.1-9.1\n  c0,0-0.3,0.5-0.5,0.9c-0.3,0-0.7,0.1-1,0.1c-1-5.2-2.1-10.4-3.1-15.6c-0.1,0-0.2,0-0.3,0c0.2,2.4,0.4,4.7,0.6,7.1h0v0.1\n  c-0.3,0.1-0.6,0.1-1,0.2c-0.7-3-1.4-5.9-1.9-8.5c0,0.1-0.3,0.6-0.8,1.8C253.6,621.7,252.2,616.5,252.6,611.3z M272.1,677.3\n  c-5.6-1.3-5.7-1.6-5.9-6.7c-0.2-5.1-0.6-10.2-0.9-15.3c0.2,0,0.4,0,0.6-0.1C268,662.5,270,669.8,272.1,677.3z M256.1,635.9\n  c0-0.1,0.2-0.3,0.9-1.3c2,5.4,4.5,10.1,5.3,15c1.4,7.6,1.6,15.4,2.3,23.3c-2.8-1.5-3.4-4.2-3.9-6.9C259.2,656,257.7,646,256.1,635.9\n  z M287,706.4c0.3,0.7,0.6,1.5,0.5,2.2c0,0.1,0,0.2,0,0.2c0,0.1,0,0.1-0.1,0.2c-0.1,0.6-0.5,1.1-1.5,1.6c0-0.6,0.1-1.1,0.1-1.6\n  c0-0.1,0-0.2,0-0.3c0-0.2,0-0.3,0-0.5c0-0.3,0-0.6,0.1-1c-2.4,6.7-8.3,4.1-12.1,6.5c0,0,0-0.1,0-0.1c0,0,0,0,0,0\n  c1.5-3.6,3.5-6.7,7.2-8.2c0.5,1,0.9,1.8,1.3,2.7c0.1-0.3,0.2-0.5,0.3-0.8c0.1-0.5,0.2-1,0.3-1.4c0.3-3.1-1.3-5-4.2-5.8\n  c0.6,1.4,1.3,2.8,2,4.4c0,0,0,0,0,0c0,0,0,0,0,0.1c-5.9,1.3-8.3,5.7-10.5,10.4c-1.3,2.8-2.6,5.4-5.7,6.8c-1.5,0.7-2.9,1.8-4.2,2.8\n  c-1.9,1.5-4,1.3-4.8-0.5c-5.2,2.5-11.6,1.7-13.2-1.5h0.1c0,0,0-0.1,0-0.1c2.1,0.1,4.2,0.3,6.3,0.3c1.9,0,2.9-0.4,3.2-1.3\n  c0,0,0-0.1,0-0.1c0.1-0.7-0.2-1.6-0.8-2.8c0,0,0,0,0,0c0,0,0,0,0-0.1c5.1-1.9,7.6-6.1,9.9-10.5c-0.4-0.4-0.7-0.7-1.1-1\n  c-1.4,1.7-2.7,3.4-4.1,5.1c-2.3,3-5.3,4.6-8.9,5.3c-3.6,0.7-5.2,1.3-6,3.3c-0.1,0.2-0.1,0.4-0.2,0.6c-0.3,0.9-0.4,2.1-0.6,3.7\n  c0,0,0,0.1,0,0.1c-1-0.3-1.9-0.6-2.8-0.9c0,0,0,0,0-0.1c0,0,0,0,0,0c1.1-1.8,1.8-3.3,2.8-4.6c1.6-2,3.3-3.9,5.1-5.7\n  c1.7-1.8,3.8-3.3,5.4-5.3c2.1-2.7,3.7-5.8,5.8-8.5c1.3-1.8,3.1-3.3,4.7-4.9c0.1,0.1,0.2,0.3,0.2,0.4c0,0,0,0,0,0.1c0,0,0,0,0,0\n  c-1.4,3.1-2.7,6.2-4.1,9.4c0.1,0,0.1,0,0.2,0c2.9-4.8,5.7-9.5,8.4-14c-1.1,0.5-2.8,1.2-5.4,2.3c0,0,0-0.1,0-0.1\n  c0.1-1.3,0.4-2.5,0.5-3.4c0-0.5,0-1-0.2-1.4c-0.4-1.2-0.9-2.3-0.7-3.3c0-0.2,0-0.3,0.1-0.5c0-0.1,0.1-0.2,0.1-0.4\n  c0.1-0.4,0.3-0.8,0.6-1.2c5.5,6.3,9.6,9,12.6,8.9c-2.6-1.9-5.8-4.1-8.6-6.7c-1.9-1.7-3-6.6-2.8-8.9c0-0.3,0.1-0.5,0.1-0.7\n  c0,0,0-0.1,0-0.1c2.8,1.1,5.5,2.5,8.4,3.2c1.7,0.4,2.1,1,2.4,2.5c0.3,2.1,0.9,4.1,1.3,6.2v0c0.3,0,0.5-0.1,0.8-0.1v0.1\n  c-0.3,0-0.5,0.1-0.8,0.1l6.1,3c1.3-0.8,1.9-1.2,2.3-1.5c0.1,2.2-0.1,4.3,0.4,6.2C284.3,698.8,285.7,702.6,287,706.4z M243.5,720.1\n  h6.2c0,0.3,0,0.7,0,1c-1.9,1.8-4,0.5-6.1-0.1C243.6,720.7,243.5,720.4,243.5,720.1z M281,657.7c-1.1-11.3-2.1-21.9-3.2-32.8\n  C283.8,629.9,285.4,646.8,281,657.7z M293.5,612.9c0.2,2.6-0.3,5.2-0.4,7.9c-0.3,0-0.7,0-1,0l-0.3-3.3c-0.1,0-0.1,0-0.2,0\n  c-0.3,3-0.5,6.1-0.7,9.1c-0.4,0-0.8,0-1.1,0v-6.8c-0.3,0-0.6,0-0.9-0.1c0,5.6,0.4,11.2-3.3,16.2v-18.7c-0.1,0-0.3,0-0.4,0V632\n  c-3.4-4.1-1.9-9.2-3.2-13.7v6.4c-2.6-3.3-2.2-7.3-2.7-11.1c-0.1,1.4-0.2,2.7-0.3,4.7c-2.1-4.4-0.9-8.1,0.5-11.7c1-2.6,2.5-5,4-7.4\n  c2.4-3.9,4.9-7.8,7.5-12c0.3,4.1,0.6,8,1,11.9C292.4,603.7,293.1,608.3,293.5,612.9z M297,546.2c-0.1,0.9-0.2,1.7-0.3,2.6\n  c-1.7,10.6-3.1,21.2-5.2,31.7c-0.1,0.4-0.2,0.9-0.3,1.3c-1.8,7-6.6,12.6-10.6,18.5c-1.9,2.8-3.3,5.8-4.2,8.8\n  c-0.5,1.7-0.8,3.4-0.9,5.2c-0.1,1.1-0.1,2.3-0.1,3.5v0.1c0,0.4,0.1,0.9,0.1,1.3c0.6,6.5,0.9,13,1.7,19.4c1.8,13.2,3.3,26.4,7.4,39.1\n  c0.2,0.5,0.2,1,0.2,1.6c0,0.2,0,0.4,0,0.6c-0.1,0.8-0.2,1.6-0.5,2.4c-0.2,0.6-0.4,1.2-0.6,1.8c0,0.1-0.1,0.2-0.1,0.2\n  c-0.4,1.1-1,2.1-1.6,3.3c-0.2,0.3-0.4,0.7-0.6,1.1c0.2-0.4,0.4-0.7,0.6-1.1c-3.2,0.2-6.3-2.6-7.2-3.5c-2.4-32.1-1.1-64.5-9-96\n  c-0.1,0.1-0.1,0.2-0.1,0.4c-0.7-2.7-1.5-5.4-2.1-7.6c0,0,0-0.1,0-0.1c0.2,0.1,0.5,0.2,0.9,0.3c-1.2-3.2-2.2-6.2-3.3-9.2\n  c0,0-0.1,0-0.1,0l0-0.1l0.5-3.2l0-0.1c0.9,0.9,1.6,1.9,1.9,3.1c0.4,1.7,2.1,3.2,2.6,4.9c1.1,3.7,5.8,6.4,9.4,5.2\n  c1.2-0.4,2.6-0.6,4.6-1.1c0,0,0,0.1-0.1,0.1c0,0,0,0,0,0c-0.8,1.6-1.3,2.4-1.7,3.1c1-0.1,1.7-0.4,2.1-1c1.9-2.6,4-5.1,5.6-7.9\n  c1.7-2.9,3-5.9,3.9-9c0.3-1,0.6-2.1,0.9-3.1c0.3-1,0.5-2,0.7-3.1c0.2-1,0.4-1.9,0.6-2.9c0-0.2,0.1-0.4,0.1-0.6\n  c0.3-1.4,0.5-2.8,0.8-4.2c0.1-0.6,0.2-1.2,0.4-1.8c0.2-0.8,0.4-1.7,0.6-2.5c0.2-0.5,0.3-1,0.5-1.5c0.5-1.5,1-3.1,1.5-4.6\n  c0.3,0.1,0.7,0.2,1,0.3C297.2,543.2,297.1,544.7,297,546.2z M297.4,517.6C297.4,517.6,297.4,517.6,297.4,517.6\n  c-0.2,2.7-0.9,5.3-1.4,7.9c-0.5,2.5-1,5-1.5,7.5c0.1,0,0.3,0.1,0.4,0.1c0.6-2.1,1.3-4.1,1.9-6.2c0.3,0.7,0.5,1.3,0.5,1.9\n  c0,0.2,0,0.4-0.1,0.6c0,0,0,0.1,0,0.1c-1.9,6.7-3.8,13.4-6,20.1c-1.2,3.6-3.4,6.5-7.1,8.2c0,0,0-0.1,0.1-0.1c0,0,0,0-0.1,0\n  c2.5-3.9,4.8-7.5,7.2-11.2c-0.3-0.2-0.5-0.3-0.8-0.5c-3.3,4.8-5.3,10.9-12.2,13.2c5.1,2.5,8.1-0.4,12-3c0,0,0,0.1-0.1,0.1\n  c0,0,0,0,0,0c-2.5,5.6-7.4,7.5-11.1,10.6c0.2,0.3,0.5,0.7,0.7,1c3-1.8,6.1-3.7,9.9-6c0,0,0,0.1-0.1,0.1c0,0,0,0,0,0\n  c-1.6,3.3-2.8,5.9-4.1,8.8c-0.6-1-0.9-1.5-1.2-2.1c-0.1,0.3-0.1,0.5-0.2,0.8c-0.1,0.6,0,1.1,0,1.6c0,0,0,0.1,0,0.1c0,0.4,0,0.7,0,1\n  c0,0.2,0,0.4-0.1,0.5c0,0.1-0.1,0.2-0.1,0.3c-2,3.2-4.6,5.9-8.4,7c-2.7,0.8-6.2-0.4-6.8-3c-0.8-3.3-3.7-5-5.1-7.7c0,0,0,0,0,0\n  c0,0,0,0,0-0.1c0.6-0.2,1.1-0.5,1.6-0.7c-0.4-0.5-0.7-1.1-1.2-1.3c-2.2-1.3-3.9-2.8-4.3-5.5c-0.1-0.4-0.4-0.8-0.8-1.1\n  c-0.1-1.6,0-3.4,0.1-4.8c0.1-0.5,0.1-0.9,0.1-1.3c0,0,0-0.1,0-0.1c0.3,0.2,0.5,0.5,0.7,0.8c0.9,1.1,1.7,2.3,3,4.1\n  c1.4,0.3,2,0.3,2.2-0.4c0.1-0.2,0.1-0.3,0.1-0.5c0-0.5,0-1.1-0.1-2c-3.1,0.9-4.1-0.9-4.8-3.5c0,0,0,0,0,0c0,0,0-0.1,0-0.1\n  c0.6,0.2,1.1,0.3,1.8,0.5c-4.6-6.3-6.9-13.2-8.5-20.4c-1.5-7.3-2.7-14.6-4-21.9c-0.2,0-0.4,0.1-0.6,0.1c0.7,4.7,1.4,9.4,2.2,14.9\n  c0,0,0,0,0-0.1c0,0.1,0,0.1,0,0.1c-4-6.9-5.1-20.6-4.1-33.6c0.5-5.9,1.4-11.7,2.7-16.6c1.5-5.8,3.6-10.4,6.1-12.7c0,0,0,0.1,0,0.1\n  l0,0c-2.3,8.4-4.5,16.6-6.8,24.9c-0.7,2.7-1.5,5.4-2.2,8.2c0.4,0.1,0.7,0.2,1,0.3c1.8-7.2,3.6-14.5,5.4-21.7c0,0,0-0.1,0-0.1\n  c0,5.5-0.4,10.9-0.8,16.3c-0.1,1.8-0.3,3.5-0.4,5.3c-0.7,8.5-0.2,16.9,2.8,24.9c3.1,8,6.9,15.8,10.4,23.6c0.2,0.4,0.7,0.5,1.7,1.2\n  c-0.3-1.3-0.4-2-0.6-2.7c-1.1-2.5-2.2-5-3.3-7.5c-4.2-9-7.8-18.2-8.7-28c-0.1-0.6-0.1-1.3-0.2-1.9c-0.3-5.1-0.2-10.1,0.2-15.1\n  c0.7-9.2,2.3-18.4,4-27.6c0,0,0-0.1,0-0.1c0.1-0.4,0.8-0.8,1.3-1.3c0.4-4.3,0.8-9.1,1.3-14c0,0,0-0.1,0-0.1c0.4,0,0.8,0.1,1.2,0.1\n  v9.4c0.2,0,0.5,0,0.7,0c0-2.5-0.1-5,0-7.4c0-1.1,0-2.3,0-3.4c0-0.3,0-0.5,0-0.8c0-0.1,0-0.3,0-0.4c0-0.2,0-0.3,0-0.5\n  c0-0.2,0-0.3,0.1-0.5c0.1-0.4,0.1-0.8,0.2-1.1c0-0.2,0.1-0.4,0.2-0.6c1.2-3.6,2.4-7.1,3.4-10.8c1.3-4.8,2.2-9.6,2.6-14.6\n  c0.1-1.5,0.1-3,0.1-4.5c0-0.1,0-0.2,0-0.3c0-0.2,0.1-0.4,0.2-0.6c0.1-0.3,0.2-0.5,0.3-0.8c1.1,2.4,2.3,4.9,2,7.2\n  c0,0.2,0,0.4-0.1,0.6c-0.5,2.5-1.1,5-1.8,7.4c-0.9,3.3-1.9,6.7-2.8,10c-0.2,0.8-0.4,1.7-0.7,2.5c-0.8,3-1.2,6.1-1.7,9.2\n  c0.2,0,0.3,0.1,0.5,0.1c2.4-9.5,4.8-19,7.2-28.5c0,0,0-0.1,0-0.1c0.7,1.2,1.3,2.5,1.1,3.7c0,0.1,0,0.2,0,0.3\n  c-0.2,1.2-0.5,2.4-0.9,3.5c-0.4,1.6-0.9,3.2-1.3,4.9c-0.1,0.4-0.2,0.8-0.3,1.2c-1.3,5.3-2.5,10.6-3.7,15.9c0.3,0.1,0.5,0.1,0.8,0.2\n  c1.8-7.1,3.5-14.3,5.3-21.4c0,0,0-0.1,0-0.1c0.6,2.7,1.2,5.1,1,7.7c-0.1,0.6-0.2,1.2-0.3,1.8c-0.2,0.6-0.4,1.3-0.7,2\n  c-0.5,1.1-0.9,2.2-1.2,3.3c-1,3.4-1.3,7.1-2.1,10.6c0.3,0.1,0.6,0.1,0.9,0.2c1.2-5.2,2.3-10.5,3.5-15.7c0,0,0-0.1,0-0.1l1.4,0.2\n  c0,0,0,0.1,0,0.1c-0.3,2.4-0.5,4.7-0.8,7.1v0c0.6,0.9,1.6,1.7,1.6,2.5c0.7,7.7,1.3,15.4,1.8,23.1c0.1,1.2,0,2.4,0,3.6\n  c0.3,0,0.7,0,1-0.1c0-2.3,0.1-4.6,0-6.8c-0.1-2.3-0.3-4.6-0.5-6.9c0,0,0,0,0,0c0,0,0-0.1,0-0.1c4.6,8.2,2.5,17,2.3,25.6\n  c0.2,0,0.5,0,0.7,0c0.4-4.6,0.8-9.1,1.1-13.7c0,0,0-0.1,0-0.1c0.4,0,0.8,0,1.3,0.1c0,2.2,0.1,4.5,0,6.7c0,0.6-0.1,1.2-0.1,1.8\n  c-0.1,1.7-0.3,3.4-0.4,5.2c-0.2,2.2-0.3,4.4-0.4,6.6c0.3,0,0.6,0,1,0c0.3-5.1,0.6-10.3,0.9-15.4h0V477c0.3,0,0.5-0.1,0.8-0.1\n  c0.3,2.1,0.9,4.2,0.8,6.3c-0.3,6.6-0.8,13.2-1.3,19.8c0,0.4-0.1,0.7-0.1,1.1c-0.4,6.5-0.9,13-2.7,19.2c-0.6,2.2-1.4,4.4-2.4,6.6\n  c-0.7,1.4-1.2,2.9-1.7,4.3c0,0.1-0.1,0.3-0.1,0.4c-0.1,0.4-0.2,0.8-0.3,1.2c-0.2,0.6-0.3,1.2-0.5,1.9c-0.3,1.4-0.6,2.8-0.8,4.2\n  c-0.2,1.6-0.4,3.2-0.5,4.8c-0.2,2.7-0.3,5.5-0.3,8.4c4.6-3.5,7.5-7.2,9.2-11.9c-2.2,3-4.4,6-6.7,8.9c-0.3-0.2-0.7-0.4-1-0.7\n  c0,0,0,0,0-0.1c0,0,0,0,0,0c2.6-3.8,5.3-7.6,7.9-11.4c-0.2-0.2-0.4-0.3-0.6-0.5c-2.1,2.9-4.2,5.8-6.3,8.7c-0.3-0.2-0.6-0.4-0.9-0.6\n  c0,0,0,0,0-0.1c0,0,0,0,0,0c2.6-4.1,5.3-8.2,7.9-12.3c-0.2-0.2-0.4-0.3-0.7-0.4c-2,2.9-3.9,5.8-5.9,8.8c-0.4-0.2-0.8-0.4-1.2-0.5\n  c0,0,0-0.1,0-0.1h0c1.2-3.5,2.4-7,3.5-10.5c0.2-0.7,0.4-1.3,0.7-2c0.3-0.7,0.5-1.4,0.8-2.1c0.8-2.4,1.7-4.8,2.4-7.2\n  c0.5-1.7,0.9-3.4,1.1-5.1c0.4-2.9,0.7-5.7,0.9-8.6c0.5-6.3,0.7-12.7,1.1-19c0,0,0-0.1,0-0.1c0-0.5,0.2-1.1,0.3-1.6c0,0,0-0.1,0-0.1\n  c0.3,0,0.6,0.1,0.9,0.1v21.2h1.1v-16.1h0v-0.1c0.3,0,0.6,0,0.9,0v23.4l0.5,0c0.4-4.9,0.8-9.8,1.2-14.7c0,0,0-0.1,0-0.1\n  c0.3,0,0.5,0,0.8,0v20.1h0.8c0.2-3.5,0.3-7,0.5-10.5h0v-0.1c0.3,0,0.6,0,0.9,0.1C297.5,512.2,297.7,514.9,297.4,517.6z M301.1,499.4\n  c-0.1,0.8,0,1.6,0,3.1c-1.1-3.7-2-6.8-2.9-9.8c1.2,7.6,4.2,15,2,22.9c-1.5-6.7-2.5-13.5-4.5-19.9c-2.3-7.4-5.2-14.6-8.1-21.8\n  c-5.4-13.4-7.9-27.5-10.3-41.6c-1.2-6.8-2.8-13.5-4.3-20.9c4,4.7,8.3,8.9,11.5,13.9c1.8,2.9,2.1,6.9,2.5,10.5\n  c0.9,9.8,4.5,18.8,7.2,28.1c1.9,6.5,4.4,13.2,4.1,19.7C297.9,489.4,301.5,493.9,301.1,499.4z M290.8,446c-2.6-4.4-2.5-6.4,0.7-9.5\n  c0.4,0.7,0.8,1.2,1.1,1.8c0.8,1.4,1.2,2.4,1.1,3.3C293.6,442.8,292.7,444,290.8,446z M294.2,443.6c0.3,0.2,0.9,0.7,1.5,1.1\n  c0,0,0,0,0,0c0.7,1.5,1.4,3.2,1.9,5l-1.8,2c-0.1-0.1-0.2-0.1-0.3-0.1c-0.8,0.7-1.5,1.4-2.3,2c-1.5-2-1.7-4,0-6.2\n  C293.9,446.5,293.8,445.1,294.2,443.6z M288.5,435c-0.3-2.1-0.6-4.1-0.9-6.1C290.8,432.4,290.9,433.7,288.5,435z M301.6,489.9\n  c-0.4-2.6-0.6-5.3-1.2-7.9c-1.3-6.4-2.6-12.9-4.3-19.2c-0.7-2.6-1.3-5-0.4-7.6l2.6-3c1.4,5.5,2,11.1,2.2,12.6c0,0.3,0,0.4,0,0.4\n  c0.7,8.2,1.4,16.3,2.1,24.5C302.4,489.8,302,489.8,301.6,489.9z M306.5,477.6c-0.1,3-1.1,6-1.7,8.9c-0.3,0-0.6-0.1-0.9-0.1\n  c-0.5-10.2,0.1-20.6-2.1-30.6c0,0,0-0.1,0-0.1c-1.2-8.6-4.5-13.5-6.3-15.6c-1.1-1.8-2.3-3.6-2.4-5.7c2.9,2.6,9.6,12.7,12.9,17.9\n  c0,0.3,0,0.5,0,0.8c0.1,1.3,0.6,2.6,0.6,3.9C306.6,463.8,306.7,470.7,306.5,477.6z M317,486.4c-3.1-11.2-3.1-22.4-1.4-33.9\n  c1.7-3.4,7.2-13.6,13.6-19.1c-3.9,5.5-6.5,12.6-8.1,19.3C317.3,463.7,318.5,475.3,317,486.4z M331.7,441.3c-0.5,2.2-0.9,4.3-1.3,6.3\n  c-0.2-0.6-0.4-1.3-0.6-1.9c-1.1-2.6-2.6-4.9-1.3-7.9c0,0-0.1-0.1-0.4-0.3c0.4-0.9,0.9-1.7,1.3-2.5L331.7,441.3z M330.6,433.5\n  C330.7,433.5,330.7,433.5,330.6,433.5c0.7-1.3,1.4-2.5,2-3.8c0,0,0,0,0,0c0.2-0.3,0.3-0.6,0.4-0.9c0.1-0.1,0.1-0.2,0.2-0.4\n  c0,0,0.1,0.1,0.1,0.1c0,0,0.1,0,0.1,0.1c0.1,0.1,0.2,0.1,0.3,0.2l-0.2,2.8c-0.4,1.7-0.8,3.8-1.2,5.9L330.6,433.5z M328.3,453.4\n  c-0.7-0.3-1.3-0.6-1.9-0.9l-2.7-3.3c0.4-1.4,0.8-2.7,1.2-3.9c0.6-0.4,1.2-0.7,1.8-1.1C328.5,447.3,331.2,450.1,328.3,453.4z\n  M319,487c0.3-4.7,0.5-9.3,1.1-13.9c0.7-6.4,1.7-12.7,2.5-19.1c0.2-0.9,0.4-1.7,0.6-2.6l2.4,2.8c1.3,3.8,0.6,7.4-1.2,11\n  c-0.4,0.7-0.5,1.5-0.6,2.3c-1.3,7.5-2.7,15-4,22.5C319.2,489,318.9,488,319,487z M321,516c-1.3-8.3,1.2-15.6,2.5-23.2\n  c-1.1,3.7-2.1,7.4-3.3,11.8c-0.7-7.1,2.5-12.6,3.9-18.6c-0.5,0.2-1,0.4-1.5,0.6c-0.3-0.1-0.6-0.3-0.9-0.4c2-7.6,4-15.3,6.1-22.9\n  c-0.1,0.5-0.3,1.1-0.4,1.7c2.4-6.2,4.6-15.1,6.3-23v0c0.1-0.3,0.1-0.6,0.2-0.9c1.6-7.5,2.7-13.9,3-15.9v0c1.3-2.5,3.6-4.6,5.5-6.8\n  c2-2.2,4.3-4.3,6.4-6.4c0.1,0.1,0.2,0.2,0.3,0.3c-0.8,2.1-1.6,4.2-2.4,6.2l-1.2,5l-0.2,0.7v0l-0.3,1.2l-4.6,28.5\n  c-0.4,0.9-0.7,1.8-1,2.7c-1.1,3-1.7,6.2-2.8,9.2c-3,7.9-6.7,15.6-9.2,23.6C324.8,498,323.2,506.8,321,516z M353.5,553.4\n  c0.4,5.5,2.9,7.2,8.2,5c0,0,0,0.1,0,0.1c-0.2,1.3-0.4,2.7-0.8,4c-0.1,0.3-0.2,0.5-0.3,0.8c-0.1,0.3-0.2,0.5-0.3,0.8\n  c-0.2,0.4-0.4,0.8-0.7,1.1c-1.4,1.7-3.9,2.5-6.2,3.8h3.9c0,0,0,0.1-0.1,0.1h0.1c-1.4,1.9-3.3,3.6-4,5.7c-2,5.8-6.3,7.2-11.2,3.6\n  c-3.1-2.3-5-5.2-4.8-9.5c-0.8,0.7-1.3,1.2-1.8,1.7c-1.2-2.8-2.4-5.4-3.8-8.7c0,0,0,0,0.1,0c0,0,0-0.1-0.1-0.1\n  c4.1,2.5,7.1,5.4,11.3,5.8c-2-1.8-4.4-3.1-6.5-4.7c-2-1.6-3.5-3.7-5.3-5.5c0,0,0.1,0,0.1,0c0,0-0.1-0.1-0.1-0.1\n  c2.2,0.5,3.6,1.6,5.3,2.3c3.5,1.6,6.8,0.4,7.6-2.8c0.1-0.3,0.1-0.7,0.2-1c0-0.3,0-0.6,0-0.9c0-0.9-0.1-1.8-0.1-2.7c0,0,0-0.1,0-0.1\n  c-0.2-6.4-0.2-12.8-2.9-19c-3-6.8-5.2-13.8-5.6-21.4c-0.5-9-1.5-18-2.3-27c0-0.3-0.1-0.6-0.1-0.8c0,0,0-0.1,0-0.1c0-0.7,0-1.5,0-2.2\n  c0.1-0.7,0.2-1.4,0.4-2.1c0.3-1.2,0.9-2.3,1.7-3.4c0.3,5.7,0.6,11,1,16.4c0.3,0,0.5,0,0.8-0.1c0.5-5.2-0.9-10.5-0.5-15.7\n  c0.1-1.2,0.2-2.4,0.5-3.5c0.1-0.6,0.3-1.2,0.6-1.9c0.4,5,0.8,9.9,1.2,14.9c0.3,0,0.6-0.1,0.9-0.1c-0.2-2.3-0.3-4.6-0.5-6.8\n  c-0.1-2.3-0.4-4.7-0.3-7c0-1.2,0.1-2.5,0.2-3.7c0.1-1.2,0.2-2.4,0.4-3.5c0.1-0.5,0.2-1.1,0.4-1.6c0.4-1.6,1-3.2,1.5-4.8\n  c0.1,0,0.2,0.1,0.3,0.1v15.2h0.4c0.4-8,0.9-15.9,1.3-23.9l0-0.1l6.1-36.2l0-0.1c0.3-0.7,0.5-1.5,0.8-2.2c0.7,2.3,0.5,4.7,1.2,6.8\n  l9.1,36.1c0.3,3.3,0.4,6.7,1.6,9.9c2.2,6.1,2.5,12.9,3.2,19.5c0.6,6.3,1.3,12.7,1.2,19c0,0.2,0,0.3,0,0.5c0,0.4,0,0.8,0,1.3\n  c0,0.8-0.1,1.5-0.2,2.3c-0.2,2.3-0.5,4.6-1,6.9c-0.1,0.6-0.2,1.1-0.3,1.7c-0.5,2.1-1,4.2-1.7,6.3c-1.7,5.5-4,10.8-6.6,16\n  c-1,2-1.8,3.9-2.4,5.9c-0.6,2-1,4-1.1,6.1C353.5,550.9,353.5,552.1,353.5,553.4z M323.4,524.7c0.3-0.1,0.6-0.2,0.9-0.3\n  c0.8,2.9,1.5,5.7,2.3,8.6c-0.3-7.5-3.8-14.6-2.4-22.2c0.1-0.4,0.2-0.8,0.3-1.2c0,0,0-0.1,0-0.1c0.3,0,0.5-0.1,0.8-0.1v10.9h1.2\n  v-20.8h0v-0.1c0.3,0,0.5,0,0.8,0l1.2,15.4c0-0.4,0.1-0.8,0.1-1.2c0.4-5.4-0.3-10.8,0.2-16.2c0.1-1.8,0.4-3.5,0.9-5.3\n  c0.1-0.5,0.3-0.9,0.4-1.4v16.8h0.7c0-2.2,0-4.4,0-6.6c0-2.3-0.2-4.7-0.1-7c0-0.6,0.1-1.2,0.1-1.8c0.1-1.7,0.3-3.3,0.4-5h0v-0.1\n  c0.3,0,0.6,0,0.9,0c0.1,1.1,0.1,2.2,0.2,3.3c0.1,1.1,0.1,2.3,0.2,3.4c0,0,0,0.1,0,0.1c0.3,5.7,0.5,11.4,1,17\n  c0.2,2.3,0.5,4.5,0.8,6.8c0.8,5.3,3.4,10.3,5.1,15.5c1,3.1,2,6.3,2.9,9.4c0,0,0,0,0,0c0,0,0,0,0,0.1c-0.3,0.1-0.6,0.3-0.9,0.4\n  c-1.9-2.8-3.8-5.6-5.7-8.4c-0.2,0.1-0.4,0.2-0.6,0.3c2.4,3.9,4.9,7.8,7.3,11.8c0,0,0,0,0,0c0,0,0,0,0,0.1c-0.3,0.2-0.6,0.4-0.9,0.6\n  c-1.9-2.7-3.8-5.3-5.7-8c-0.2,0.1-0.3,0.2-0.5,0.3c1.2,1.9,2.4,3.8,3.6,5.6c1.2,1.8,2.4,3.6,3.7,5.4c0,0,0,0-0.1,0c0,0,0,0,0,0.1\n  c-0.3,0.2-0.6,0.4-1,0.6l-6.4-8.6c-0.2,0.1-0.3,0.2-0.5,0.3c1.3,2,2.7,3.8,3.8,5.9c0.9,1.7,3.6,2.5,3.3,5.4c0,0.3-0.1,0.7-0.2,1.1\n  c0,0,0,0.1,0,0.1c-2.8-3.6-5.3-6.8-7.7-10.1c-0.2,0.2-0.4,0.3-0.6,0.5c2.2,2.9,4.5,5.8,6.7,8.7c0.5,0.7,0.6,1.8,1,2.7c0,0,0,0,0,0\n  c0,0,0,0.1,0,0.1c-0.9-0.4-2.2-0.5-2.7-1.2c-2.5-3.2-4.8-6.7-7.2-10c0.9,4.5,4.1,7.8,7.1,11.2c0,0,0,0,0,0c0,0,0,0,0,0\n  c-0.2,0.2-0.5,0.4-0.7,0.7c-1.4-1.2-3.2-2.1-4.1-3.6C327.1,545.4,325.7,535,323.4,524.7C323.4,524.7,323.4,524.7,323.4,524.7\n  C323.4,524.7,323.4,524.7,323.4,524.7z M337.1,631.2c-0.2-4.9-0.5-9.4-0.7-13.9c-0.1,0-0.2,0-0.2,0v19.5c-3.5-5.5-3.4-11.4-3.9-17.2\n  v7.3c-0.4,0-0.8,0.1-1.2,0.1c-0.3-3-0.7-6.1-1.1-9.4c-0.1,1.4-0.2,2.6-0.3,4.6c-2.4-3.9-1.9-7.4-1.6-10.8c0.6-7.6,1.5-15.1,2.3-22.6\n  c0.1-0.5,0.2-0.9,0.3-1.6c3.8,6.4,7.6,12.3,10.9,18.4c2.5,4.5,2.6,8,1.3,11c-0.2-1.1-0.3-1.9-0.4-2.6c-0.5,3.4-1,6.8-1.5,10.2\n  c-0.4-0.1-0.8-0.1-1.2-0.2c0.1-2.3,0.3-4.5,0.5-7.6C337.8,621.5,340.4,626.8,337.1,631.2z M343.6,625.4c-1,10.5-2,20.9-3,31.6\n  C335.9,650.4,337.3,631.8,343.6,625.4z M335.9,679c0.5-1.6,1-3.3,1.5-4.9c0,0,2.8-10.4,3.7-15.7c2.1-12.8,4.1-25.5,4.8-38.5\n  c0.4-8.7-2.1-15.8-6.9-22.7c-3.4-4.9-6.5-10.1-8.5-15.6c-4.5-12.3-5.3-25.3-6.1-38.2c0-0.6,0.2-1.2,0.3-1.8c0.3,0,0.5,0,0.8,0\n  c1.4,5.7,3.2,11.4,4.1,17.2c1,6.9,3.7,13.1,7.3,18.8c2.4,3.7,5.6,6.9,8.4,10.3c0.3-0.2,0.5-0.4,0.8-0.7c-1.4-2-2.8-4-4.6-6.5\n  c2.2,0.5,3.8,0.9,5.3,1.2c3.6,0.6,8.2-1.9,8.4-5.3c0.3-3.5,2.5-4.9,5.5-6.8c-0.6,1.8-1,3-1.4,4.2c-3.8,10.7-7,21.5-8,32.8\n  c-1.5,15.9-2.2,31.8-3.2,47.7c-0.3,4.6-0.7,9.1-1,13.7c0,0-1,11.6-1.5,18.7c-1,1.8-5.1,2.3-6.8,2.4c-0.9-1.4-1.6-2.7-2-4\n  C336.1,683.3,335.3,680.8,335.9,679z M349.3,671.2c0.7-1.1,4.6-10.2,6.1-15.5c0.2,0.1,0.5,0.1,0.7,0.2c-0.3,4.1-0.4,8.2-0.8,12.4\n  c0,0,0.2,4.4,0,6.4c0,0-1.2,2.7-6.8,4c0.4-1.5,0.4-4.6,0.4-4.6c0.1-0.9,0.3-1.8,0.4-2.7C349.4,671.3,349.3,671.3,349.3,671.2z\n  M364.1,701.2c2,2.7,3.7,5.9,5.8,8.5c1.5,2,3.6,3.5,5.4,5.3c1.8,1.8,3.5,3.7,5.1,5.7c1,1.3,1.7,2.8,2.8,4.6\n  c-0.8,0.3-1.8,0.6-2.8,0.9c-0.6-5.7-1.3-6.5-6.8-7.6c-3.6-0.7-6.7-2.3-8.9-5.3c-1.3-1.7-2.7-3.4-4.1-5.1c-0.4,0.4-0.7,0.7-1.1,1.1\n  c2.3,4.4,4.8,8.7,9.9,10.6c-1.6,3.2-1,4.3,2.4,4.4c2.1,0,4.2-0.2,6.3-0.3c-1.6,3.2-8,4-13.2,1.5c-0.8,1.8-3,2-4.8,0.5\n  c-1.3-1.1-2.7-2.2-4.2-2.8c-3.1-1.4-4.4-4-5.7-6.8c-2.2-4.6-4.7-9-10.5-10.4c0.7-1.6,1.3-3,2-4.5c-3.5,1-5.2,3.8-3.7,8.2\n  c0.4-0.8,0.8-1.7,1.3-2.7c3.7,1.5,5.7,4.6,7.2,8.2c-3.7-2.4-9.7,0.2-12.1-6.5c0.1,1.6,0.2,2.3,0.2,3.4c-2.3-1.2-1.5-2.7-1-4.2\n  c1.3-3.8,2.8-7.5,3.8-11.4c0.5-1.9,0.3-4,0.4-6.2c0.5,0.3,1.1,0.7,2.4,1.6l6.1-3.1c0.2-0.9,0.4-1.7,0.6-2.6c0,0,0,0,0,0\n  c0.3-1.2,0.5-2.4,0.7-3.6c0.2-1.5,0.7-2.1,2.4-2.5c2.9-0.7,5.6-2.1,8.4-3.2c0.5,1.6-0.6,7.6-2.7,9.5c-2.8,2.6-6.1,4.8-8.7,6.8\n  c3,0.1,7.2-2.6,12.7-8.9c1.3,1.8,0.8,3.4,0.1,5.3c-0.4,1.2,0.2,2.8,0.3,4.9c-2.6-1.1-4.3-1.8-5.4-2.3c2.7,4.5,5.6,9.3,8.5,14.1\n  c0.1,0,0.2,0,0.2,0c-1.4-3.2-2.7-6.3-4.1-9.4c0.1-0.1,0.2-0.3,0.2-0.4C361,697.9,362.7,699.4,364.1,701.2z M363.8,645.1\n  c-1.2,7.5-2.2,14.9-3.6,22.4c0,0,0,0,0,0c-0.3,2-0.9,3.9-2.3,5.4l-1.1,1.2c0.4-0.9,0.5-10.3,2-19.2c1.3-7.7,3.8-15.6,6.3-19.1\n  C364.8,638.8,364.3,642,363.8,645.1z M367.6,604c2.4,6.8,1.5,13.6-0.8,20.2c-1,3.1-2.7,5.9-4.1,8.8l-0.5-0.2\n  c0.2-2.3,0.4-4.6,0.6-6.8c-0.3-0.1-0.6-0.1-0.9-0.2c1,8.8-3.7,15.7-6.8,23.2V641c-1.5,8.8-4.3,19.2-5.9,28.1\n  c-0.3-16.6,5.3-74.4,7.8-80.1c0.1,3,0.3,5.2,0.4,7.4c1-5.7-0.5-11.7,1.6-17.3c0.3,0,0.7,0,1,0v14.5h0.6v-12.9c0.1,0,0.2-0.1,0.3-0.1\n  c0.3,1.9,0.3,4,0.9,5.8C363.6,592.4,365.5,598.2,367.6,604z M374.4,506.3c-0.3,3.6-1,7.3-1.9,11c-0.5,1.9-1.1,3.8-1.8,5.7\n  c0,0,0-0.1,0-0.1c0.2-1.9,0.5-3.8,0.8-5.7c0.2-1.1,0.3-2.2,0.4-3.3c0.1-0.9,0.1-1.9-0.1-2.8c-1.4,7.3-2.6,14.5-4.1,21.8\n  c-0.4,1.8-0.8,3.6-1.3,5.3c-1.5,5.3-3.7,10.5-7.2,15.2c0.7-0.1,1.1-0.1,2-0.2c0,0,0,0.1-0.1,0.1h0c-0.7,1.1-1.2,2.1-1.9,2.8\n  c-1.5,1.5-2.8,1.2-3.2-0.9c-0.4-2.4-0.7-4.8-0.6-7.2c0.1-1.4,0.3-2.9,0.7-4.3c0.3-1.1,0.8-2.2,1.4-3.3c3.5-6.6,7.3-13.2,9.2-20.5\n  c0.4-1.6,0.8-3.3,1-5c0.2-1.3,0.3-2.6,0.4-3.8c0.5-6.5,0.4-13,0.3-19.5c-0.1-5.9-0.9-11.8-0.9-17.9c0,0,0,0,0,0.1v-0.1\n  c1.9,7.4,3.7,14.7,5.6,22.1c-1.3-11-4.5-21.5-7.6-32c0,0.1,0.1,0.2,0.1,0.3c0-0.1-0.1-0.2-0.1-0.3\n  C371.7,477.4,375.7,491.6,374.4,506.3z M364.3,419.6c0.1,0,0.1,0,0.2,0l5.5,37.1c0.2,3.6,0.5,7.3,0.7,10.9c-0.7-0.3-1.1-0.8-1.3-1.3\n  c-1.8-4.3-3.3-8.7-5.6-12.7c-0.3-0.6-0.6-1.2-0.9-1.8c-0.5-1.2-0.8-2.4-1-3.7c0,0,0-0.1,0-0.1c-3.4-12.9-9.2-34.2-10.2-37.9\n  c0-0.9,0.4-1.7,1.7-2.5c0.1,1.8,0.1,3.4,0.3,5c0.2,1.5,0.8,3,1.2,4.6c0.1-2.4-0.4-4.6-0.6-6.9c-0.3-3.2-0.8-5,1.9-5.8\n  c0.2,2.6,0.3,5.2,0.5,7.9c1.4-4-1-8.6,2.6-12.6c0.2,4.5,0.5,8.4,0.7,12.4c0.5-5.1-0.4-10.2,1.4-15.7\n  C362.4,404.6,363.3,412.1,364.3,419.6z M361.3,387.3c1,3.6,0.1,6.9-2.4,9.7c-5.3,5.9-10.8,11.6-16,17.6c-4.1,4.8-7.9,9.9-11.7,14.9\n  c-3.5,3.2-13.3,12.6-17.9,22.5c-2.1,0.3-4.4,1.2-5.8-0.8c0-0.1,0-0.2,0-0.3c-4.1-10.5-14.2-18.1-14.9-18.6c0,0,0,0,0,0\n  c-2.5-3.4-4.9-6.9-7.7-10.1c-5.6-6.4-11.5-12.6-17.1-19c-2.6-3-5.1-6.3-7.3-9.7c-1.3-1.9-1.4-4.2-0.6-5.8\n  c4.7,5.9,9.1,12.2,14.2,17.9c3.4,3.8,7.4,7.2,11.5,10.2c1.4,1,3.1,1.8,4.6,2.8c3,8.8,10.3,15.4,15.1,19c-0.1,0-0.1-0.1-0.2-0.1\n  c4.7,4.1,5.2,4.3,9.9,1.4c1.4-0.9,2.8-1.9,4.1-3.1c8.5-6.8,15.9-18,17.3-20.3C347.3,408.4,354.2,397.8,361.3,387.3z M362.4,248.8\n  c-13-7.6-27.1-9.9-42.1-5.5c-0.8-0.7-1.6-1.5-1.5-2.5c0-0.2,0-0.5,0.1-0.7c0.1-0.3,0.2-0.6,0.5-1c1.2-1.8,2.7-3.8,4.8-2.4\n  c2.5,1.6,4.9,0.9,7.4,1.2c0.7,0.1,1.2,0.8,1.7,1.1c12-1.3,24-2.5,36.1,0.3c0-0.3,0-0.5,0-0.8c-4.6-0.5-9.2-1.1-13.8-1.6\n  c0,0,0-0.1,0.1-0.1h-0.1c0.5-0.6,0.9-0.8,1.4-0.9c2.4-0.3,5.2,0.1,7.1-1.1c3.7-2.4,7.3-1.7,11.1-1.1c2.2,0.4,4.4,1.1,6.5,1.6\n  c0.1-0.2,0.1-0.4,0.1-0.5c-2.1-0.6-4.3-1.2-7-2c0.1,0,0.1,0,0.2,0c-0.1,0-0.1,0-0.2-0.1c19.6-3.2,33.7,12.4,32.9,33.4h0v0.1h-1.3\n  c0.5,2.1,1,4.2,1.5,6.3l0,0.1l-0.6,4.2l0,0.1c0-0.1-0.1-0.1-0.1-0.1c-0.1-0.2-0.3-0.3-0.4-0.5v19.7h0v0.1c-0.2,0.1-0.4,0.1-0.6,0.2\n  c-3-4.9-5.9-9.7-9-14.8c-0.1,0.8-0.2,1.2-0.3,1.7c0,0,0,0.1,0,0.1c-2.9-2.6-5.6-5.2-8.6-7.4c-6.4-4.7-11.3-10.7-16-17\n  C369.5,255.1,366.4,251.1,362.4,248.8z M376.4,273.1C376.4,273.1,376.4,273.1,376.4,273.1L376.4,273.1c0.8,0.8,1.4,1.6,2,2.3\n  c0.1-0.1,0.2-0.1,0.3-0.2c-1.3-2.5-2.6-5.1-3.9-7.6c0,0,0,0,0,0c0,0,0,0,0-0.1c0.2-0.1,0.4-0.2,0.5-0.3c1.6,2.1,3.2,4.3,5,6.7\n  c0.1-0.8,0.1-1.2,0.3-2.4c6,5.9,12.3,11.1,17.3,17.2c3.8,4.7,7,9.9,10.4,15.3l4.6,12.4c-0.1-0.4-0.3-0.9-0.4-1.3h0\n  c0.2,0.6,0.4,1.2,0.6,1.8c0,0,0,0,0,0c0.5,1.8,0.8,3.5,0.7,5.1c0,0.5-0.1,1.1-0.2,1.6c-0.5,2.3-1.8,4.3-4.4,6\n  C388.1,319.8,377,302.8,376.4,273.1z M403.6,369.6c-0.3,0-0.7-0.1-1-0.1c-0.3-5.9-0.7-11.7-1-18v17.6c-6.2-13.6-5-28.3-2.8-43.1\n  c3.3,1.5,6.9,3.1,10.5,4.8C404,343.2,403.6,356.4,403.6,369.6z M408.9,437.6c4.9-3,9.4-5.7,14.3-8.7\n  C422.2,432,412.2,438.1,408.9,437.6z M422.3,422.9c0.7,3.8,0.4,4-3.2,4.3c-0.2,0.9-0.2,1.9-0.6,2.2c-2.8,1.8-5.7,3.7-8.8,5.2\n  c-1.5,0.7-3.2,0.8-4.9,1.1c0-0.3-0.1-0.6-0.1-0.9C410.6,430.9,416.3,427,422.3,422.9z M429.4,355.6c0,0.8-0.1,1.5-0.2,2.3\n  c-0.3,2.3-0.8,4.6-1,6.9c-0.1,1.3-0.2,2.6-0.1,3.9c0,0,0,0.1,0,0.1c0,0,0,0,0,0c0,0,0,0.1,0,0.1c0,0.1-0.1,0.2-0.1,0.3\n  c0,0,0,0.1,0,0.1c-0.3,1.7-0.6,3.5-1,5.2c0,0,0,0.1,0,0.1c0,0.1-0.1,0.3-0.1,0.4c-0.1,0.1-0.1,0.3-0.2,0.4c-0.3,0.9-0.7,1.7-1,2.6\n  c0,0.1-0.1,0.3-0.1,0.4c-0.1,0.2-0.1,0.4-0.1,0.6c-0.5,2.6-0.4,5.3-0.9,7.8c-1.1,5.4-2.3,10.7-3.5,16.3c0,0,0,0.1,0,0.1\n  c-1-0.7-1.6-1.1-2.1-1.4c0.7,2,1.4,3.9,2.1,5.8c0,0,0,0,0,0c0,0,0,0,0,0.1c-0.3,0.1-0.6,0.3-0.8,0.4c-0.5-0.3-1-0.6-2.2-1.3\n  c3.2,4.1,3.5,8.1,3.5,12.2c0,0,0,0.1,0,0.1c0,0.1,0,0.2,0,0.3c0,0.1-0.1,0.3-0.1,0.4c0,0.1-0.1,0.2-0.1,0.3c0,0.1-0.1,0.2-0.1,0.3\n  c0,0.1-0.1,0.2-0.1,0.2c-0.2,0.5-0.6,1-1,1.2c-1.9,0.8-4,1.2-6,1.7c0.4,0.4,0.7,0.8,1.4,1.5c0,0-0.1,0-0.1,0c0,0,0,0,0.1,0.1\n  c-1.6,0.2-2.8,0.3-4.3,0.4c0.8,0.6,1.4,1,2.6,1.9c0,0-0.1,0-0.1,0c0,0,0.1,0,0.1,0.1c-2.1,0.4-3.5,0.6-5.2,0.9\n  c0.8,0.3,1.4,0.6,2,0.8c0.1,0.3,0.1,0.6,0.2,0.9c0,0,0,0,0,0c0,0,0,0,0,0.1c-2.3,0.8-4.6,1.7-6.9,2.5c-2.3,0.7-3.3-0.2-3.1-2.4\n  c0-0.1,0-0.2,0-0.3c0,0,0,0,0,0c0-0.1-0.1-0.3-0.2-0.4c-0.4,0.4-0.7,0.8-1.7,1.9c-1.1-3.5-2.1-6.4-3.1-9.5c0,0,0,0,0-0.1\n  c0.1-0.3,0.1-0.6,0.2-0.8c0,0,0-0.1,0-0.1l4,3.2l0-0.1c0,0,0,0,0,0l0-0.1c1.3,1.1,3,2.6,3.7,2.2c1.8-0.8,3.7-2.6,4.3-4.5\n  c0.1-0.2,0.1-0.4,0.2-0.6c0.2-0.8,0.3-1.6,0.4-2.4c0.1-1.8,0-3.7-0.1-5.5c-0.1-1-1.2-2.7-2.1-2.9c-3.1-0.8-7.9,2.5-9.7,6.3\n  c-0.1,0.2-0.2,0.5-0.3,0.7c-0.1,0.2-0.2,0.5-0.2,0.7c-0.1,0.2-0.1,0.5-0.1,0.7c0,0,0,0.1,0,0.1c-0.2,0.3-0.4,0.7-0.6,1.2\n  c-0.1,0.1-0.1,0.3-0.2,0.4c-0.1,0.2-0.1,0.4-0.2,0.6c-0.2,0.8-0.3,1.7-0.4,2.5v0.1c-0.9,2-1.9,4-2.7,6.1c0,0,0,0,0,0s-4,1-3.5-4.8\n  c0.1-1,0.3-2.1,0.7-3.5c0,0,0-0.1,0-0.1c0.5-0.8,1-1.5,1.2-2.3c0.6-2.2,1.1-4.5,1.3-6.7c0.1-1.1,0.2-2.2,0.5-3.2\n  c0.1-0.2,0.1-0.4,0.2-0.6c0.8-2.7,2.5-5,5.4-6.5c1.5-0.8,2.9-1.9,4-3.2c0.7-0.7,1.5-1.8,1.8-2.7c0-0.1,0.1-0.2,0.1-0.3\n  c0-0.1,0-0.1,0-0.2c0-0.1,0-0.2-0.1-0.3c-0.2-0.5-0.3-1-0.3-1.5c0-0.2,0-0.3,0-0.4c0-0.1,0-0.3,0-0.4c0-0.3,0.1-0.7,0.2-1\n  c0.1-0.2,0.1-0.4,0.2-0.5c0.4-1.2,1.1-2.3,1.7-3.5c0.7-1.4,1.3-2.9,2-4.3l0-0.1l4.9-6.1c-0.2,0.6-0.4,1.2-0.6,1.8\n  c-0.4,1.3-0.8,2.6-1.2,3.9c-0.3,1-0.6,1.9-0.9,2.9c-0.4,1.4-0.9,2.8-1.3,4.2c0,0.2-0.1,0.3-0.2,0.5c-0.4,1.2-0.8,2.5-1.2,3.7\n  c4,1.1,7.3,1.9,10.8,2.8c0.3-0.7,0.9-1.6,1.1-2.5c0.5-2.2,1.1-4.4,1.6-6.6c0.3-1.2,0.6-2.4,0.8-3.7c0.2-0.8,0.3-1.5,0.5-2.3\n  c0.3-1.7,0.7-3.4,1-5.1c0.4-2.4,0.9-4.8,1.1-7.2c0.2-2,0.2-3.9-0.2-5.7c-0.3-1.5-0.4-2.9-0.3-4.2c0.1-1.1,0.3-2.1,0.6-3.1\n  c0.2-0.5,0.3-1,0.5-1.5v0c0.2-0.4,0.4-0.9,0.5-1.3c0.2-0.4,0.4-0.8,0.5-1.1c0.3-0.6,0.6-1.3,1-1.9c0.7-1.3,1.3-2.6,1.9-3.9\n  c0.3-0.7,0.5-1.3,0.8-2c0.1,1.4,0.2,2.7,0.3,4.1c0.1,0.7,0.1,1.3,0.1,2c0,1,0.1,2,0,3C429.4,354.7,429.4,355.2,429.4,355.6z\n  M402.9,423.9c-2.6-1.4-2.7-4.8-0.2-8.9C405.3,418,404,420.9,402.9,423.9z M403,412.4c0.7-0.3,2-0.9,3.6-1.6c0.2,3.2,0.3,6,0.5,8.7\n  c-0.2-0.1-0.5-0.2-0.7-0.2C405.3,417.1,404.2,414.9,403,412.4z M397.2,420.5c-0.4-0.4-0.7-0.9-0.8-1.4c0.1-0.3,0.2-0.5,0.3-0.8\n  c1.2-1.3,2.3-2.7,3.5-4.1c0.3,0.2,0.5,0.4,0.8,0.5C399.2,416.3,401.3,420,397.2,420.5z M422,360.6c-1.5,11.7-3.1,23.3-4.5,34.4\n  c-3.2-1.1-5.9-1.9-9-3c1.5-3.7,3.1-7.9,4.8-12.2C412.1,378.9,417.2,365.4,422,360.6z M416.7,363.7c-0.2,0.5-0.3,0.9-0.5,1.4\n  c-0.2,0.4-0.3,0.9-0.4,1.3c-0.2,0.6-0.4,1.2-0.6,1.7c-0.8,2.4-1.6,4.8-2.4,7.2l-3.9,4.5l-0.1,0.1c0.3-0.6,0.5-1.2,0.8-1.8\n  c-0.1,0-0.2-0.1-0.4-0.2c-0.7,1.3-1.5,2.6-2.3,4.1l0,0l0,0c-0.3,0.4-0.5,0.9-0.8,1.4c-0.2-1.5-0.7-2.7-0.7-3.7c0-0.2,0-0.4,0-0.6\n  c0,0,0-0.1,0-0.1c0-0.1,0-0.2,0.1-0.3c0.5-2.1,1.1-4.2,1.7-6.4c0.4-1.6,0.9-3.2,1.4-4.7c1.5-4.7,3.4-9.3,5.9-13.6\n  c4.2-7.4,8-14.9,12-22.4c1-1.9,2-3.7,3-5.6c0.1,2.2,0.1,4.3-0.1,6.4c-0.2,2.6-0.6,5.2-1.3,7.8c-0.8,2.9-1.8,5.8-3.2,8.5\n  C422.1,353.9,418.6,358.5,416.7,363.7z M430.1,315.9c-1.3,3.8-2.3,7.8-4,11.4c-2.8,6.1-6.1,12.1-9.3,18c-1.1,2-3.1,3.5-3.9,5.5\n  c-2.4,6.2-4.5,12.4-6.7,18.7c-0.3,0.8-0.4,1.7-0.6,2.5c-0.4-0.1-0.9-0.2-1.3-0.2c1-9.6,1.9-19.1,3.2-28.7\n  c1.3-9.9,8.7-16.9,12.8-25.4c0,0-0.1-0.1-0.1-0.1c4.2-6.2,5.7-12.3,5.6-17.9c1.9,2.9,4,11.5,4.9,15.6\n  C430.3,315.6,430.1,315.7,430.1,315.9z\"/>\n  <path class=\"st0\" (click)=\"onClickPath($event)\" d=\"M343.7,177.6c-0.5,1.7-1,3.5-1.7,5.1c-0.9,2.1-1.8,4.3-3,6.2c-0.6,1-2.1,1.5-3.9,2.7c0.3-4.6,0.5-8.2,0.7-11.8\n  c-0.1,0-0.1,0-0.1,0c2.7-9.3,4.6,0.2,4.6,0.2s0.6-1.7,1.1-4c0.5-2.3-0.9-2.7-3.2-2.7c-2.1,0-3.4,6.1-3.6,7.3c0,0,0,0,0,0\n  c-0.2,0.4-0.4,0.8-0.4,1.1c-0.5,4.7-0.8,9.5-1.3,14.2c-0.5,5.5-2,10.3-8.2,12c-3.1,0.9-6.1,2.3-9.3,3.5c-0.1-0.5-0.2-0.9-0.3-1.1\n  c-1.3,0.8-2.8,2.2-4.1,2.2c-1.5-0.1-2.9-1.5-4.3-2.3c0,0.1-0.1,0.7-0.3,1.4c-0.6-1-1-1.5-1.4-2.1c0,0.3-0.1,0.6-0.1,0.9\n  c-0.7,0.1-1.4,0.4-2,0.2c-2.4-1-4.7-2.3-7.2-3.2c-3.5-1.2-5.6-3.7-6.1-7.2c-0.8-5.6-1-11.3-2-16.8c-0.6-3.5-1.6-7-3.2-10.1\n  c-1.3-2.4-2.7-1.9-4.2,0.6c-1.5,2.4-1.3,4.5,0.7,6.5c-0.6-6.6,0.1-6,1.7-7.8c1,1.4,3.4,2.3,1.3,4.6c1.8,1.2,2.4,2.9,1.5,5.1\n  c-0.3,0.6-0.5,1.2-0.7,1.8c0.4,0.1,1.1,0.1,1.1,0.2c0.2,2.2,0.3,4.4,0.4,6.5c-0.3,0.2-0.5,0.4-0.8,0.6c-1.2-1.2-2.8-2.1-3.5-3.5\n  c-1.2-2.5-1.6-5.5-3-7.9c-1.9-3.4-1.2-6.5,0.5-9.4c0.6-1.1,2.3-2,3.6-2.2c0.6-0.1,1.7,1.6,2.3,2.6c1.2,1.9,2.3,4,3.4,6\n  c0.4-0.1,0.7-0.2,1.1-0.4c0.3-4.1,0.7-8.2,1-12.3c0.3-5-1.1-10.2,2.5-14.7c0.1-0.2,0.1-0.5,0.1-0.9c4-1.7,13.4-4.7,16.2,2\n  c6.9-3.3,17-7.3,20.7,1c0.1,0.3,0.2,0.5,0.3,0.8c0.4,3.2,0.6,5.9,0.8,8.1c0.1,2.4,0.1,4.2,0.1,5.6c-0.2,0-0.4,0.1-0.6,0.1\n  c0.5,3.4,1,6.9,1.5,10.4c0.3,0,0.6,0.1,0.8,0.1c0.2-0.9,0.2-1.9,0.6-2.7c0.8-1.4,2.1-2.5,2.7-3.9c0-0.1,0.1-0.1,0.1-0.2c0,0,0,0,0,0\n  c0.1-0.2,0.2-0.3,0.3-0.5c1.1-1.5,2.7-1.8,4.3-0.5C343,170.4,344.4,175.2,343.7,177.6z\"/>\n  <path class=\"st0\" (click)=\"onClickPath($event)\" d=\"M365.2,635.7c-0.5,3.1-0.9,6.3-1.4,9.4c-1.2,7.5-2.2,14.9-3.6,22.4c0,0,0,0,0,0c-0.3,2-0.9,3.9-2.3,5.4\n  l-1.1,1.2c0.4-0.9,0.5-10.3,2-19.2C360.2,647,362.7,639.2,365.2,635.7z\"/>\n  <path class=\"st0\" (click)=\"onClickPath($event)\" d=\"M355.3,668.3c0,0,0.2,4.4,0,6.4c0,0-1.2,2.7-6.8,4c0.4-1.5,0.4-4.6,0.4-4.6c0.1-0.9,0.3-1.8,0.4-2.7\n  c0-0.1,0-0.1,0-0.1c0.7-1.1,4.6-10.2,6.1-15.5c0.2,0.1,0.5,0.1,0.7,0.2C355.8,660,355.7,664.2,355.3,668.3z\"/>\n  <path class=\"st0\" (click)=\"onClickPath($event)\" d=\"M343.6,625.4c-1,10.5-2,20.9-3,31.6C335.9,650.4,337.3,631.8,343.6,625.4z\"/>\n  <path d=\"M259.1,589.2L259.1,589.2c0.2-0.5,0.2-1.1,0.2-1.7L259.1,589.2z\"/>\n  <path class=\"st0\" (click)=\"onClickPath($event)\" d=\"M264.7,673c-2.8-1.5-3.4-4.2-3.9-6.9c-1.6-10-3.2-20.1-4.7-30.1c0-0.1,0.2-0.3,0.9-1.3c2,5.4,4.5,10.1,5.3,15\n  C263.7,657.3,264,665.1,264.7,673z\"/>\n  <path class=\"st0\" (click)=\"onClickPath($event)\" d=\"M272.1,677.4c-5.6-1.3-5.7-1.6-5.9-6.7c-0.2-5.1-0.6-10.2-0.9-15.3c0.2,0,0.4,0,0.6-0.1\n  C268,662.6,270,669.9,272.1,677.4z\"/>\n  <path class=\"st0\" (click)=\"onClickPath($event)\" d=\"M249.7,721.2c-1.9,1.8-4,0.5-6.1-0.1c0-0.3-0.1-0.6-0.1-1h6.2C249.7,720.5,249.7,720.8,249.7,721.2z\"/>\n  <path class=\"st0\" (click)=\"onClickPath($event)\" d=\"M234.5,717.8c-1,1.9-1.9,3.8-4.5,3.8c-0.2-0.3-0.4-0.5-0.6-0.8c1.5-1.3,2.9-2.6,4.4-3.9\n  C234,717.2,234.3,717.5,234.5,717.8z\"/>\n  <path class=\"st0\" (click)=\"onClickPath($event)\" d=\"M280.9,657.7c-1.1-11.3-2.1-21.9-3.2-32.8C283.7,630,285.4,646.9,280.9,657.7z\"/>\n  <path class=\"st0\" (click)=\"onClickPath($event)\" d=\"M287,706.5c0.3,0.7,0.6,1.4,0.6,2.1c0,0.1,0,0.2,0,0.2c0,0.1,0,0.1-0.1,0.2c-0.1,0.6-0.5,1.1-1.5,1.6\n  c0-0.6,0.1-1.1,0.1-1.6c0-0.1,0-0.2,0-0.3c0-0.2,0-0.3,0-0.5c0-0.3,0-0.6,0.1-1c-2.4,6.7-8.3,4.1-12.1,6.5c0,0,0-0.1,0-0.1\n  c1.5-3.6,3.5-6.6,7.2-8.1c0.5,1,0.9,1.8,1.3,2.7c0.1-0.3,0.2-0.6,0.3-0.9c0.1-0.5,0.2-1,0.3-1.4c0.3-3.1-1.3-5-4.2-5.8\n  c0,0,0,0-0.1,0c0.6,1.4,1.3,2.8,2,4.4c0,0,0,0,0,0.1c-5.9,1.3-8.3,5.7-10.5,10.4c-1.3,2.8-2.6,5.4-5.7,6.8c-1.5,0.7-2.9,1.8-4.2,2.8\n  c-1.9,1.5-4,1.3-4.8-0.5c-5.2,2.5-11.6,1.7-13.2-1.5h0.1c2.1,0.1,4.1,0.3,6.2,0.3c2,0,3-0.4,3.2-1.4c0,0,0-0.1,0-0.1\n  c0.1-0.7-0.2-1.6-0.8-2.8c0,0,0,0,0,0c5.1-1.9,7.6-6.2,9.9-10.6l0,0c-0.4-0.4-0.7-0.7-1.1-1c-1.4,1.7-2.7,3.4-4.1,5.1\n  c-2.3,3-5.3,4.6-8.9,5.3c-3.6,0.7-5.2,1.3-6,3.3c-0.1,0.2-0.1,0.4-0.2,0.6c-0.3,0.9-0.4,2.1-0.6,3.7c0,0,0,0.1,0,0.1\n  c-1-0.3-1.9-0.6-2.8-0.9c0,0,0,0,0-0.1c1-1.7,1.7-3.2,2.7-4.5c1.6-2,3.3-3.9,5.1-5.7c1.7-1.8,3.8-3.3,5.4-5.3\n  c2.1-2.7,3.7-5.8,5.8-8.5c1.3-1.8,3.1-3.3,4.7-4.9c0.1,0.1,0.2,0.2,0.2,0.4c0,0,0,0,0,0c-1.4,3.1-2.7,6.2-4.1,9.4c0,0,0,0.1,0,0.1\n  c0.1,0,0.1,0,0.2,0c2.9-4.8,5.8-9.6,8.5-14.1c0,0,0,0,0,0c-1.1,0.5-2.8,1.2-5.4,2.3c0,0,0-0.1,0-0.1c0.1-1.3,0.4-2.5,0.5-3.4\n  c0-0.5,0-1-0.2-1.4c-0.4-1.2-0.9-2.3-0.7-3.3c0-0.2,0-0.3,0.1-0.5c0-0.1,0.1-0.2,0.1-0.4c0.1-0.4,0.3-0.7,0.6-1.1\n  c5.5,6.3,9.7,9.1,12.7,8.9c0,0-0.1-0.1-0.1-0.1c-2.6-1.9-5.8-4.1-8.6-6.7c-1.9-1.7-3-6.6-2.8-8.9c0-0.3,0.1-0.5,0.1-0.7\n  c2.8,1.1,5.5,2.5,8.4,3.2c1.7,0.4,2.1,1,2.4,2.5c0.3,2.1,0.9,4.1,1.3,6.2l6.1,3c0,0,0,0.1-0.1,0.1c1.3-0.9,1.9-1.2,2.4-1.6\n  c0.1,2.2-0.1,4.3,0.4,6.2C284.2,698.9,285.7,702.7,287,706.5z\"/>\n  <path class=\"st0\" (click)=\"onClickPath($event)\" d=\"M297.3,541.9c-0.1,1.4-0.2,2.9-0.3,4.3c-0.1,0.9-0.2,1.7-0.3,2.6c-1.7,10.6-3.1,21.2-5.2,31.7\n  c-0.1,0.4-0.2,0.9-0.3,1.3c-1.8,7-6.6,12.6-10.6,18.5c-1.9,2.8-3.3,5.8-4.2,8.8c-0.5,1.7-0.8,3.4-0.9,5.2c-0.1,1.2-0.1,2.3-0.1,3.5\n  v0.1c0,0.4,0.1,0.9,0.1,1.3c0.6,6.5,0.9,13,1.7,19.4c1.8,13.2,3.3,26.4,7.4,39.1c0.2,0.5,0.2,1,0.2,1.5c0,0.2,0,0.4,0,0.6\n  c-0.1,0.8-0.2,1.6-0.5,2.4c-0.2,0.6-0.4,1.2-0.6,1.8c0,0.1-0.1,0.2-0.1,0.2c0,0.1-0.1,0.1-0.1,0.2c-0.1,0.2-0.1,0.3-0.2,0.4\n  c-0.4,0.9-0.8,1.7-1.4,2.6c-3.2,0.2-6.3-2.6-7.2-3.5c-2.4-32.1-1.1-64.5-9-96c-0.1,0.1-0.1,0.2-0.1,0.4c-0.7-2.7-1.5-5.4-2.1-7.6\n  c0.3,0.1,0.5,0.2,0.9,0.3c0,0,0-0.1,0-0.1c-1.2-3.2-2.2-6.2-3.3-9.2c0,0-0.1,0-0.1,0l0-0.1l0.5-3.2c0.9,0.9,1.6,1.9,1.9,3.1\n  c0.4,1.7,2.1,3.2,2.6,4.9c1.1,3.7,5.8,6.4,9.4,5.2c1.2-0.4,2.6-0.6,4.6-1.1c0,0,0,0,0,0c-0.8,1.6-1.3,2.4-1.7,3.1c0,0,0,0.1,0,0.1\n  c1,0,1.7-0.4,2.2-1c1.9-2.6,4-5.1,5.6-7.9c1.7-2.9,3-5.9,3.9-9c0.3-1,0.6-2.1,0.9-3.1c0.3-1,0.5-2,0.7-3.1c0.2-1,0.4-1.9,0.6-2.9\n  c0-0.2,0.1-0.4,0.1-0.6c0.3-1.4,0.5-2.8,0.8-4.2c0.1-0.6,0.2-1.2,0.4-1.8c0.2-0.8,0.4-1.7,0.6-2.5c0.2-0.5,0.3-1,0.5-1.5\n  c0.5-1.5,1-3,1.5-4.5C296.6,541.7,297,541.8,297.3,541.9z\"/>\n  <path class=\"st0\" (click)=\"onClickPath($event)\" d=\"M269.5,410.9c-1.4,11.7-4.6,22.9-8.8,33.9c-2.8,7.4-5.9,14.7-8.8,22.1c-0.4-0.1-0.8-0.2-1.2-0.3\n  c1.6-5.6,3.3-11.3,4.9-16.9c-0.2-0.1-0.3-0.1-0.5-0.2c-1.3,4.1-2.6,8.1-3.9,12.2c-0.2-0.1-0.5-0.1-0.7-0.2c0.9-4,1.8-8.1,2.7-12.1\n  c0.2-0.8,0.9-1.8,0.6-2.3c-2.2-3.2,0-6.3,0.4-9.2c1.5-11,3.6-21.9,5.2-32.9c0.4-2.6,0.1-5.3,0.5-8c2.7,4.2,0.9,9,2.1,13.4v-9.6\n  c4.1,3.2,1.5,7.8,2.9,12.3c0.3-3.3,0.5-5.9,0.8-8.5c2.6,1.4,1.9,3.9,1.7,6.1c-0.2,2.2-0.7,4.4-0.4,6.7c0.5-3,1.1-6.1,1.6-9.1\n  c0.2-0.1,0.4-0.2,0.6-0.2C269.2,409,269.6,410,269.5,410.9z\"/>\n  <path class=\"st0\" (click)=\"onClickPath($event)\" d=\"M333.9,429c-0.1,0.9-0.2,1.9-0.2,2.8c-0.4,1.7-0.8,3.8-1.2,5.9l-1.8-4.1c0,0,0.1,0,0.1,0c0.7-1.2,1.3-2.5,2-3.7\n  c0,0,0,0,0,0c0.2-0.3,0.3-0.6,0.4-0.9c0.1-0.1,0.1-0.2,0.2-0.4c0,0,0.1,0.1,0.1,0.1c0,0,0.1,0,0.1,0.1\n  C333.7,428.8,333.8,428.9,333.9,429z\"/>\n  <path class=\"st0\" (click)=\"onClickPath($event)\" d=\"M331.7,441.4c-0.5,2.2-0.9,4.3-1.3,6.3c-0.2-0.6-0.4-1.3-0.6-1.9c-1.1-2.6-2.6-4.9-1.3-7.9c0,0-0.1-0.1-0.4-0.3\n  c0.4-0.9,0.9-1.7,1.3-2.5L331.7,441.4z\"/>\n  <path class=\"st0\" (click)=\"onClickPath($event)\" d=\"M328.3,453.5c-0.7-0.3-1.3-0.6-1.9-0.9l-2.7-3.3c0.4-1.4,0.8-2.7,1.2-3.9c0.6-0.4,1.2-0.7,1.8-1.1\n  C328.5,447.4,331.2,450.1,328.3,453.5z\"/>\n  <path class=\"st0\" (click)=\"onClickPath($event)\" d=\"M324.3,465.2c-0.4,0.7-0.5,1.5-0.6,2.3c-1.3,7.5-2.7,15-4,22.5c-0.5-1-0.7-2-0.6-3.1c0.3-4.7,0.5-9.3,1.1-13.9\n  c0.7-6.4,1.7-12.7,2.5-19.1c0.2-0.9,0.4-1.7,0.6-2.6l2.4,2.8C326.8,458,326.1,461.7,324.3,465.2z\"/>\n  <path class=\"st0\" (click)=\"onClickPath($event)\" d=\"M329.1,433.5c-3.9,5.5-6.5,12.6-8.1,19.3c-3.8,11-2.5,22.6-4.1,33.7c-3.1-11.2-3.1-22.4-1.4-33.9\n  C317.2,449.2,322.6,439,329.1,433.5z\"/>\n  <path class=\"st0\" (click)=\"onClickPath($event)\" d=\"M370.7,467.7c-0.7-0.3-1.1-0.8-1.3-1.3c-1.8-4.3-3.3-8.7-5.6-12.7c-0.3-0.6-0.6-1.2-0.9-1.8\n  c-0.5-1.2-0.8-2.4-1-3.7c0,0,0-0.1,0-0.1c-3.4-12.9-9.2-34.2-10.2-37.9c0-0.9,0.4-1.7,1.7-2.5c0.1,1.8,0.1,3.4,0.3,5\n  c0.2,1.5,0.8,3,1.2,4.6c0.1-2.4-0.4-4.6-0.6-6.9c-0.3-3.2-0.8-5,1.9-5.8c0.2,2.6,0.3,5.2,0.5,7.9c1.4-4-1-8.6,2.6-12.6\n  c0.2,4.5,0.5,8.4,0.7,12.4c0.5-5.1-0.4-10.2,1.4-15.7c1,8.2,2,15.7,3,23.2c0.1,0,0.1,0,0.2,0l5.5,37.1\n  C370.2,460.4,370.5,464.1,370.7,467.7z\"/>\n  <path class=\"st0\" (click)=\"onClickPath($event)\" d=\"M349.2,412.3c-0.8,2.1-1.6,4.2-2.4,6.2l-1.2,5l-0.2,0.7v0l-0.3,1.2l-4.6,28.5c-0.4,0.9-0.7,1.8-1,2.7\n  c-1.1,3-1.7,6.2-2.8,9.2c-3,7.9-6.7,15.6-9.2,23.6c-2.7,8.5-4.3,17.3-6.5,26.5c-1.3-8.3,1.2-15.6,2.5-23.2\n  c-1.1,3.7-2.1,7.4-3.3,11.8c-0.7-7.1,2.5-12.6,3.9-18.6c-0.5,0.2-1,0.4-1.5,0.6c-0.3-0.1-0.6-0.3-0.9-0.4c2-7.6,4-15.3,6.1-22.9\n  c-0.1,0.5-0.3,1.1-0.4,1.7c2.4-6.2,4.6-15.1,6.3-23v0c0.1-0.3,0.1-0.6,0.2-0.9c1.6-7.5,2.7-13.9,3-15.9v0c1.3-2.5,3.6-4.6,5.5-6.8\n  c2-2.2,4.3-4.3,6.4-6.4C349,412.1,349.1,412.2,349.2,412.3z\"/>\n  <path class=\"st0\" (click)=\"onClickPath($event)\" d=\"M296.1,525.4c0.5-2.6,1.1-5.2,1.4-7.8c0.2-2.7,0.1-5.4,0.1-8c-0.3,0-0.6,0-0.9-0.1h0c-0.2,3.5-0.3,7-0.5,10.5\n  v0.1h-0.8v-20.1c-0.3,0-0.5,0-0.8,0c-0.4,4.9-0.8,9.8-1.2,14.7c0,0,0,0.1,0,0.1l-0.5,0v-23.4c-0.3,0-0.6,0-0.9,0h0v16.2h-1.1v-21.2\n  c-0.3,0-0.6-0.1-0.9-0.1c-0.1,0.5-0.3,1.1-0.3,1.6c0,0,0,0.1,0,0.1c-0.4,6.3-0.6,12.7-1.1,19c-0.2,2.9-0.5,5.8-0.9,8.6\n  c-0.2,1.7-0.6,3.4-1.1,5.1c-0.7,2.4-1.6,4.8-2.4,7.2c-0.2,0.7-0.5,1.4-0.8,2.1c-0.2,0.7-0.5,1.4-0.7,2.1c-1.1,3.5-2.3,6.9-3.5,10.4\n  c0,0,0,0.1,0,0.1c0.4,0.2,0.8,0.4,1.2,0.5c2-2.9,3.9-5.9,5.9-8.8c0.2,0.1,0.4,0.3,0.7,0.4c0,0,0,0,0,0c-2.6,4.1-5.3,8.2-7.9,12.3\n  c0,0,0,0,0,0.1c0.3,0.2,0.6,0.4,0.9,0.6c2.1-2.9,4.2-5.8,6.3-8.7c0.2,0.2,0.4,0.3,0.6,0.5c0,0,0,0,0,0c-2.6,3.8-5.3,7.6-7.9,11.4\n  c0,0,0,0,0,0.1c0.3,0.2,0.7,0.4,1,0.7c2.2-3,4.4-6,6.7-8.9c0,0,0,0,0-0.1c-1.8,4.7-4.7,8.5-9.3,12c0.1-2.9,0.1-5.7,0.3-8.4\n  c0.1-1.6,0.3-3.2,0.5-4.8c0.2-1.4,0.4-2.8,0.8-4.2c0.1-0.6,0.3-1.3,0.5-1.9c0.1-0.4,0.2-0.8,0.3-1.2c0-0.1,0.1-0.3,0.1-0.4\n  c0.5-1.5,1-2.9,1.7-4.3c1-2.1,1.8-4.3,2.4-6.6c1.8-6.2,2.3-12.7,2.7-19.2c0-0.3,0-0.7,0.1-1c0.4-6.6,1-13.2,1.3-19.8\n  c0.1-2.1-0.5-4.2-0.8-6.3c-0.3,0-0.5,0.1-0.8,0.1h0c-0.3,5.1-0.6,10.2-0.9,15.4v0.1c-0.3,0-0.6,0-1,0c0.1-2.2,0.3-4.4,0.4-6.6\n  c0,0,0-0.1,0-0.1c0.1-1.7,0.3-3.4,0.4-5.2c0.1-0.6,0.1-1.2,0.1-1.8c0.1-2.2,0-4.5,0-6.7c-0.4,0-0.8,0-1.3-0.1\n  c-0.4,4.5-0.8,9.1-1.1,13.7c0,0,0,0.1,0,0.1c-0.2,0-0.5,0-0.7,0c0.2-8.6,2.2-17.3-2.3-25.5c0,0,0,0,0,0c0.2,2.3,0.4,4.6,0.5,6.9\n  c0.1,2.3,0,4.6,0,6.8v0.1c-0.3,0-0.7,0-1,0.1c0-1.2,0.1-2.4,0-3.6c-0.6-7.7-1.1-15.4-1.8-23.1c-0.1-0.9-1.1-1.7-1.6-2.5c0,0,0,0,0,0\n  h0c0,0,0,0,0-0.1v0c0.3-2.4,0.5-4.7,0.8-7.1l-1.4-0.2c-1.2,5.2-2.3,10.4-3.5,15.7c0,0,0,0.1,0,0.1c-0.3-0.1-0.6-0.1-0.9-0.2\n  c0,0,0-0.1,0-0.1c0.8-3.5,1.2-7.2,2.1-10.6c0.3-1.1,0.7-2.3,1.2-3.3c0.3-0.7,0.5-1.3,0.7-2c0.2-0.6,0.3-1.2,0.3-1.8\n  c0.2-2.5-0.3-5-1-7.6c-1.8,7.1-3.5,14.3-5.3,21.4c0,0,0,0.1,0,0.1c-0.3-0.1-0.5-0.1-0.8-0.2c0,0,0-0.1,0-0.1\n  c1.2-5.3,2.4-10.6,3.7-15.9c0.1-0.4,0.2-0.8,0.3-1.2c0.4-1.6,0.9-3.2,1.3-4.9c0.3-1.2,0.6-2.4,0.9-3.5c0-0.1,0-0.2,0-0.3\n  c0.1-1.1-0.4-2.4-1.1-3.6c-2.4,9.5-4.8,19-7.2,28.5c0,0,0,0.1,0,0.1c-0.2,0-0.3-0.1-0.5-0.1c0,0,0-0.1,0-0.1c0.6-3.1,1-6.2,1.7-9.2\n  c0.2-0.8,0.4-1.7,0.7-2.5c0.9-3.3,1.9-6.7,2.8-10c0.7-2.5,1.3-4.9,1.8-7.4c0-0.2,0.1-0.4,0.1-0.6c0.2-2.3-0.9-4.8-2-7.1\n  c-0.1,0.2-0.2,0.5-0.3,0.7c-0.1,0.2-0.1,0.4-0.2,0.6c0,0.1,0,0.2,0,0.3c0,1.5,0,3-0.1,4.5c-0.3,5-1.3,9.8-2.6,14.6\n  c-1,3.6-2.1,7.2-3.4,10.8c-0.1,0.2-0.1,0.3-0.2,0.5c-0.1,0.4-0.2,0.7-0.2,1.1c0,0.2,0,0.3-0.1,0.5c0,0.2,0,0.3,0,0.5\n  c0,0.1,0,0.3,0,0.4c0,0.3,0,0.5,0,0.8c0,1.1,0,2.3,0,3.4c-0.1,2.5,0,4.9,0,7.4v0.1c-0.3,0-0.5,0-0.7,0v-9.4c-0.4,0-0.8-0.1-1.2-0.1\n  c-0.4,4.8-0.9,9.7-1.3,14c0,0,0,0.1,0,0.1c-0.5,0.5-1.2,0.8-1.3,1.3c-1.7,9.1-3.3,18.3-4,27.6c-0.4,5-0.5,10.1-0.2,15.1\n  c0,0.6,0.1,1.3,0.2,1.9c0.9,9.8,4.4,19.1,8.7,28c1.2,2.5,2.3,4.9,3.3,7.5c0.3,0.6,0.4,1.4,0.6,2.7c0,0,0,0.1,0,0.1\n  c-1-0.7-1.6-0.8-1.8-1.2c-3.5-7.8-7.4-15.6-10.4-23.6c-3.1-8-3.5-16.4-2.8-24.9c0,0,0-0.1,0-0.1c0.1-1.8,0.3-3.5,0.4-5.3\n  c0.4-5.4,0.7-10.8,0.8-16.2c-1.8,7.2-3.6,14.4-5.4,21.7c0,0,0,0.1,0,0.1c-0.4-0.1-0.7-0.2-1.1-0.3c0,0,0-0.1,0-0.1h0\n  c0.7-2.7,1.5-5.4,2.2-8.2c2.3-8.2,4.5-16.5,6.8-24.9l0,0c-2.5,2.3-4.6,6.8-6.1,12.6c-1.3,4.9-2.2,10.7-2.7,16.6\n  c-1,13,0.1,26.7,4.1,33.6c0-0.1,0-0.1,0-0.1c-0.8-5.4-1.5-10.1-2.2-14.8c0,0,0,0,0,0c0.2,0,0.4-0.1,0.6-0.1c1.3,7.3,2.5,14.7,4,21.9\n  c1.5,7.2,3.8,14.2,8.5,20.4c0,0,0.1,0.1,0.1,0.1c-0.8-0.2-1.2-0.3-1.9-0.5c0,0,0,0,0,0c0.7,2.6,1.7,4.4,4.8,3.5\n  c0.1,0.9,0.1,1.5,0.1,2c0,0.2,0,0.4-0.1,0.5c-0.2,0.8-0.8,0.7-2.2,0.4c-1.3-1.8-2.1-3-3-4.1c-0.2-0.3-0.5-0.5-0.7-0.8\n  c0,0.3-0.1,0.8-0.1,1.3c-0.1,1.3-0.2,3.1-0.1,4.8c0.4,0.3,0.7,0.7,0.8,1.1c0.4,2.7,2.2,4.2,4.3,5.5c0.5,0.3,0.8,0.9,1.2,1.3\n  c0,0,0,0,0,0.1c-0.5,0.2-1.1,0.5-1.6,0.7c0,0,0,0,0,0c1.4,2.7,4.3,4.4,5.1,7.7c0.6,2.6,4.1,3.8,6.8,3c3.8-1.1,6.4-3.8,8.4-7\n  c0.1-0.1,0.1-0.2,0.1-0.3c0.1-0.2,0.1-0.3,0.1-0.5c0-0.3,0-0.7,0-1c0,0,0-0.1,0-0.1c0-0.5-0.1-1,0-1.6c0-0.3,0.1-0.5,0.2-0.8\n  c0.3,0.6,0.7,1.1,1.2,2.1c1.3-2.8,2.6-5.5,4.1-8.8c0,0,0,0,0,0c-3.8,2.3-6.8,4.1-9.8,6c-0.3-0.4-0.5-0.7-0.8-1.1c0,0,0,0,0,0\n  c3.7-3.1,8.6-5,11.1-10.6c0,0,0,0,0,0c-3.9,2.6-6.9,5.5-12,2.9c0,0,0.1,0,0.1,0c6.9-2.3,8.9-8.3,12.2-13.2c0.2,0.2,0.5,0.3,0.8,0.5\n  c0,0,0,0,0,0c-2.3,3.6-4.7,7.3-7.2,11.1c0,0,0,0.1-0.1,0.1c3.8-1.7,6-4.7,7.1-8.2c2.2-6.6,4-13.4,6-20.1c0,0,0-0.1,0-0.1\n  c0.1-0.2,0.1-0.4,0.1-0.6c0-0.6-0.2-1.2-0.5-1.9c-0.6,2.1-1.3,4.1-1.9,6.2c-0.1,0-0.3-0.1-0.4-0.1c0,0,0-0.1,0-0.1\n  C295.1,530.4,295.6,527.9,296.1,525.4z M365.6,484c-0.7-6.6-1-13.4-3.2-19.5c-1.1-3.3-1.2-6.6-1.6-9.9l-9.1-36.1\n  c-0.8-2.1-0.5-4.5-1.2-6.8c-0.3,0.7-0.5,1.5-0.8,2.2l-6.1,36.2l0,0.1c-0.4,8-0.9,15.9-1.3,23.9v0.1h-0.4v-15.2\n  c-0.1,0-0.2-0.1-0.3-0.1c-0.5,1.6-1.1,3.1-1.5,4.7c-0.1,0.5-0.3,1.1-0.4,1.6c-0.2,1.2-0.3,2.3-0.4,3.5c-0.1,1.2-0.1,2.5-0.2,3.7\n  c0,2.3,0.2,4.7,0.3,7c0.1,2.3,0.3,4.6,0.5,6.8v0.1c-0.3,0-0.6,0.1-0.9,0.1c-0.4-5-0.8-9.9-1.2-14.9c-0.2,0.6-0.4,1.2-0.6,1.8\n  c-0.3,1.2-0.5,2.4-0.5,3.5c-0.4,5.2,1,10.5,0.5,15.7v0.1c-0.3,0-0.5,0-0.8,0.1c-0.3-5.3-0.6-10.7-1-16.4c-0.8,1.1-1.4,2.2-1.7,3.4\n  c-0.2,0.7-0.3,1.4-0.4,2.1c-0.1,0.7-0.1,1.4,0,2.2c0,0,0,0.1,0,0.1c0,0.3,0,0.6,0.1,0.8c0.7,9,1.7,18,2.3,27\n  c0.5,7.5,2.6,14.6,5.6,21.4c2.7,6.2,2.7,12.6,2.9,19c0,0,0,0.1,0,0.1c0,0.9,0,1.8,0.1,2.7c0,0.3,0,0.6,0,0.9c0,0.4-0.1,0.7-0.2,1\n  c-0.8,3.2-4,4.5-7.6,2.9c-1.6-0.7-3.1-1.8-5.2-2.3c0,0-0.1,0-0.1,0c1.8,1.9,3.3,3.9,5.3,5.5c2,1.7,4.4,3,6.5,4.7\n  c0,0,0.1,0.1,0.1,0.1c-4.3-0.3-7.3-3.3-11.3-5.8c0,0,0,0-0.1,0c1.4,3.2,2.5,5.8,3.8,8.7c0.6-0.6,1.1-1,1.8-1.7\n  c-0.2,4.3,1.7,7.2,4.8,9.5c4.9,3.7,9.2,2.2,11.2-3.6c0.7-2.1,2.6-3.8,4-5.7h-4.1c0.1,0,0.1-0.1,0.1-0.1c2.4-1.3,4.8-2.1,6.2-3.8\n  c0.3-0.3,0.5-0.7,0.7-1.1c0.1-0.3,0.2-0.5,0.3-0.8c0.1-0.3,0.2-0.5,0.3-0.8c0.4-1.3,0.6-2.7,0.8-4c-5.3,2.2-7.8,0.5-8.2-5\n  c-0.1-1.3-0.1-2.5,0-3.8c0.1-2.1,0.5-4.1,1.1-6.1c0.6-2,1.4-4,2.4-5.9c2.6-5.2,4.9-10.5,6.6-16c0.6-2.1,1.2-4.2,1.7-6.3\n  c0.1-0.6,0.2-1.1,0.3-1.7c0.5-2.3,0.8-4.6,1-6.9c0.1-0.7,0.1-1.5,0.1-2.2c0-0.4,0-0.9,0.1-1.3c0-0.2,0-0.3,0-0.5\n  C366.9,496.6,366.2,490.2,365.6,484z M365.6,463.9c0-0.1-0.1-0.2-0.1-0.3c3.2,10.5,6.4,21,7.6,32c0,0.1,0,0.2,0,0.2\n  c-1.9-7.4-3.7-14.8-5.6-22.2c0,0,0,0,0-0.1c-0.1,6,0.8,11.9,0.9,17.9c0.1,6.5,0.2,13-0.3,19.5c-0.1,1.3-0.2,2.6-0.4,3.8\n  c-0.2,1.7-0.5,3.4-1,5c-1.9,7.3-5.7,13.9-9.2,20.5c-0.6,1.1-1,2.2-1.3,3.3c-0.4,1.4-0.7,2.8-0.7,4.3c-0.1,2.4,0.2,4.8,0.6,7.2\n  c0.4,2.1,1.6,2.4,3.2,0.9c0.7-0.7,1.2-1.7,1.9-2.8h0c-0.9,0.1-1.3,0.2-2,0.2c0,0,0-0.1,0.1-0.1c3.5-4.7,5.7-9.8,7.2-15.2\n  c0.5-1.8,0.9-3.5,1.3-5.3c1.5-7.2,2.7-14.5,4.1-21.8c0.1,1,0.1,1.9,0.1,2.8c-0.1,1.1-0.2,2.2-0.4,3.3c-0.3,1.9-0.5,3.8-0.8,5.7\n  c0,0,0,0.1,0,0.1c0.7-1.9,1.3-3.8,1.8-5.7c1-3.7,1.6-7.3,1.9-11C375.7,491.7,371.7,477.7,365.6,463.9z M338.5,549.1\n  c-1.1-2.1-2.5-4-3.8-5.9c0,0,0,0,0,0c0.2-0.1,0.3-0.2,0.5-0.3l6.4,8.6c0.3-0.2,0.6-0.4,1-0.6c0,0,0,0,0-0.1\n  c-1.2-1.8-2.4-3.5-3.6-5.4c-1.2-1.9-2.4-3.8-3.6-5.7c0,0,0,0,0,0c0.2-0.1,0.3-0.2,0.5-0.3c1.9,2.7,3.8,5.3,5.7,8\n  c0.3-0.2,0.6-0.4,0.9-0.6c0,0,0,0,0-0.1c-2.5-3.9-4.9-7.8-7.3-11.8c0,0,0,0,0,0c0.2-0.1,0.4-0.2,0.6-0.3c1.9,2.8,3.8,5.6,5.7,8.4\n  c0.3-0.1,0.6-0.3,0.9-0.4c0,0,0,0,0-0.1c-1-3.1-1.9-6.3-2.9-9.4c-1.7-5.2-4.4-10.2-5.1-15.5c-0.3-2.3-0.6-4.6-0.8-6.9\n  c-0.5-5.6-0.7-11.3-1-17c0,0,0-0.1,0-0.1c-0.1-1.1-0.1-2.2-0.2-3.3c-0.1-1.1-0.1-2.2-0.2-3.3c-0.3,0-0.5,0-0.8,0h0\n  c-0.1,1.7-0.3,3.4-0.4,5c0,0.6-0.1,1.2-0.1,1.8c-0.1,2.3,0.1,4.7,0.1,7c0,2.2,0,4.4,0,6.6v0.1H330v-16.8c-0.2,0.4-0.3,0.9-0.4,1.3\n  c-0.5,1.8-0.8,3.5-0.9,5.3c-0.5,5.4,0.3,10.8-0.2,16.2c0,0.4-0.1,0.8-0.1,1.2c0,0,0,0.1,0,0.1l-1.2-15.5c-0.3,0-0.5,0-0.8,0h0v20.9\n  h-1.2v-10.9c-0.3,0-0.5,0-0.8,0.1c-0.1,0.4-0.2,0.8-0.3,1.2c-1.4,7.6,2.2,14.7,2.4,22.2c0,0.1,0,0.1,0,0.2c-0.8-2.9-1.6-5.8-2.3-8.7\n  c-0.3,0.1-0.6,0.2-0.9,0.3c0,0,0,0,0,0c2.3,10.2,3.7,20.7,9.7,29.7c1,1.5,2.7,2.4,4.1,3.6c0.2-0.2,0.5-0.4,0.7-0.7c0,0,0,0,0,0\n  c-3-3.4-6.2-6.7-7.1-11.3c0,0,0,0,0,0.1c2.4,3.4,4.7,6.8,7.2,10c0.5,0.7,1.8,0.8,2.7,1.2c0,0,0-0.1,0-0.1c-0.3-0.9-0.4-1.9-0.9-2.6\n  c-2.2-3-4.5-5.8-6.7-8.7c0,0,0,0,0.1,0c0.2-0.2,0.4-0.3,0.6-0.5c2.5,3.2,5,6.4,7.7,10.1c0,0,0-0.1,0-0.1c0.1-0.4,0.2-0.7,0.2-1.1\n  C342,551.6,339.4,550.9,338.5,549.1z\"/>\n  <path class=\"st0\" (click)=\"onClickPath($event)\" d=\"M359.8,329.5c-2.1,5.8-5.5,10.3-12,11.4c-5.2,0.9-5.8,1.8-5.8,7.1c0,1.2,0.1,2.4,0.3,3.6\n  c0.4,3.4,0.9,6.8,1.4,10.2c0,0,0.1,0,0.1,0c-0.4,1-1,2.1-1.2,3.2c-1.7,9.6-2.9,19.3-5,28.9c-1.7,8-3.1,16.3-7.9,23.4\n  c-0.3,0.5-0.3,1.2-0.5,2.3c0.2-0.1,0.5-0.2,0.7-0.3l-8.3,12c-3.5,3.2-6.5,6.9-11,7.4c-6.5-2.4-14.1-13-17.7-18.7\n  c-0.1-0.7-0.4-1.4-0.7-1.9c-3.1-4.7-5-9.9-6.2-15.4c-1.9-9-3.9-18.1-5.7-27.2c-1-4.8-1.7-9.7-2.5-14.5c-0.1-0.7-0.1-1.4,0.1-2.1\n  c0.5-3.6,1.1-7.2,1.5-10.8c0.4-4.9-0.7-6.4-5.7-7c-5.3-0.7-11.4-6.9-11.9-11.7c1,1.2,1.6,1.9,2.2,2.7c2.6,3.4,5.7,6.1,10.4,5.8\n  c0.9-0.1,2.2-0.9,2.4-1.6c1-5.6,1.8-11.1,2.7-16.7l1.4-6.7c0.1,0,0.1,0.1,0.2,0.1c0.6-6,3.5-9.6,9.5-10.6c3.9-0.7,7.7-2.4,11.5-3.7\n  c4-1.4,5.4-3.2,5.3-7.4c-0.1-10.1-0.1-20.2-0.8-30.2c-0.4-5.6-0.2-11.7-4.6-16.4c0.5-0.9,1-1.7,1.5-2.5c1.4,0.9,2.7,1.8,3.9,2.6\n  c0.2-0.2,0.4-0.5,0.6-0.7c-1-1.6-2.2-3.1-3.1-4.8c-1.2-2.3-2.2-4.7-3.1-7.2c-1.7-5-3-10-4.7-15c-0.8-2.4-2.1-4.7-3.2-7\n  c1,0.2,1.6,0.6,2.2,1.1c5.5,4.5,7,11,8.6,17.4c1.2,4.5,2.2,9.1,3.3,13.7c0.3-0.1,0.6-0.1,0.9-0.2c-0.8-5.8-1.6-11.7-2.3-17.4h3.6\n  c0.4,0.1,0.9,0.1,1.4,0h3.4c-0.8,5.9-1.6,11.8-2.4,17.8c0.9-0.5,1.2-1.1,1.4-1.8c2-7.4,3.7-14.9,6.2-22.1c1.2-3.4,4.2-6.1,6.3-9.2\n  c0.3,0.2,0.7,0.4,1,0.7c-0.4,0.9-0.8,1.8-1.1,2.6c-2,5.4-4.6,10.6-6,16.1c-1.2,4.9-2.6,9.5-5.8,13.4c-0.4,0.5-0.5,1.3-0.8,2\n  c0.2,0.1,0.4,0.3,0.6,0.4c1-0.7,2-1.4,3.2-2.2c0.7,0.7,1.3,1.3,1.3,1.4c-0.6,2-1.3,3.8-1.9,5.5c-0.4,1.3-0.9,3.1-1.1,4.4\n  c-0.8,5.9-1,11.9-1.4,17.9c-0.3,5-0.4,9.9-0.6,14.9c-0.3,8.4,1,11.1,8.8,13c3.5,0.9,6.8,2.6,10.4,3.3c3.6,0.8,5.6,2.8,6.6,6.2\n  c0.8,2.7,1.8,5.3,2.3,8.1c1,5.7,1.7,11.4,2.5,17.1c0.5,3.6,1.6,4.4,5.1,3.6c3.3-0.8,5.9-2.6,7.8-5.4\n  C358.1,331.4,359,330.5,359.8,329.5z\"/>\n  <path class=\"st0\" (click)=\"onClickPath($event)\" d=\"M358.9,397c-5.3,5.9-10.8,11.6-16,17.6c-4.1,4.8-7.9,9.9-11.7,14.9c-3.5,3.2-13.3,12.6-17.9,22.5\n  c-2.1,0.3-4.4,1.2-5.8-0.8c0-0.1,0-0.2,0-0.3c-4.1-10.5-14.2-18.1-14.9-18.6c0,0,0,0,0,0c-2.5-3.4-4.9-6.9-7.7-10.1\n  c-5.6-6.4-11.5-12.6-17.1-19c-2.6-3-5.1-6.3-7.3-9.7c-1.3-1.9-1.4-4.2-0.6-5.8c4.7,5.9,9.1,12.2,14.2,17.9\n  c3.4,3.8,7.4,7.2,11.5,10.2c1.4,1,3.1,1.8,4.6,2.8c3,8.8,10.3,15.4,15.1,19c-0.1,0-0.1-0.1-0.2-0.1c4.7,4.1,5.2,4.3,9.9,1.4\n  c1.4-0.9,2.8-1.9,4.1-3.1c8.5-6.8,15.9-18,17.3-20.3c10.9-7.1,17.8-17.8,24.9-28.2C362.3,391,361.4,394.3,358.9,397z\"/>\n  <path class=\"st0\" (click)=\"onClickPath($event)\" d=\"M306.5,477.7c-0.1,3-1.1,6-1.7,8.9c-0.3,0-0.6-0.1-0.9-0.1c-0.5-10.2,0.1-20.6-2.1-30.6c0,0,0-0.1,0-0.1\n  c-1.2-8.6-4.5-13.5-6.3-15.6c-1.1-1.8-2.3-3.6-2.4-5.7c2.9,2.6,9.6,12.7,12.9,17.9c0,0.3,0,0.5,0,0.8c0.1,1.3,0.6,2.6,0.6,3.9\n  C306.6,463.9,306.7,470.8,306.5,477.7z\"/>\n  <path class=\"st0\" (click)=\"onClickPath($event)\" d=\"M288.4,435.1c-0.3-2.1-0.6-4.1-0.9-6.1C290.8,432.5,290.9,433.7,288.4,435.1z\"/>\n  <path class=\"st0\" (click)=\"onClickPath($event)\" d=\"M293.7,441.6c-0.1,1.2-1.1,2.4-2.9,4.5c-2.6-4.4-2.5-6.4,0.7-9.5c0.4,0.7,0.8,1.2,1.1,1.8\n  C293.4,439.8,293.8,440.7,293.7,441.6z\"/>\n  <path class=\"st0\" (click)=\"onClickPath($event)\" d=\"M297.6,449.7l-1.8,2c-0.1-0.1-0.2-0.1-0.3-0.1c-0.8,0.7-1.5,1.4-2.3,2c-1.5-2-1.7-4,0-6.2\n  c0.6-0.9,0.6-2.3,0.9-3.8c0.3,0.2,0.9,0.7,1.5,1.1c0,0,0,0,0,0C296.4,446.2,297.1,447.9,297.6,449.7z\"/>\n  <path class=\"st0\" (click)=\"onClickPath($event)\" d=\"M302.7,489.8c-0.4,0-0.8,0.1-1.2,0.1c-0.4-2.6-0.6-5.3-1.2-7.9c-1.3-6.4-2.6-12.9-4.3-19.2\n  c-0.7-2.6-1.3-5-0.4-7.6l2.6-3c1.4,5.5,2,11.1,2.2,12.6c0,0.3,0,0.4,0,0.4C301.3,473.5,302,481.6,302.7,489.8z\"/>\n  <path class=\"st0\" (click)=\"onClickPath($event)\" d=\"M301.1,499.5c-0.1,0.8,0,1.6,0,3.1c-1.1-3.7-2-6.8-2.9-9.8c1.2,7.6,4.2,15,2,22.9c-1.5-6.7-2.5-13.5-4.5-19.9\n  c-2.3-7.4-5.2-14.6-8.1-21.8c-5.4-13.4-7.9-27.5-10.3-41.6c-1.2-6.8-2.8-13.5-4.3-20.9c4,4.7,8.3,8.9,11.5,13.9\n  c1.8,2.9,2.1,6.9,2.5,10.5c0.9,9.8,4.5,18.8,7.2,28.1c1.9,6.5,4.4,13.2,4.1,19.7C297.9,489.5,301.5,494,301.1,499.5z\"/>\n  <path d=\"M265.9,636.9c0.2,1.3,0.4,2.6,0.6,3.9v-4C266.4,636.9,266.1,636.9,265.9,636.9z M359.5,446.8\n  C359.6,446.9,359.6,446.9,359.5,446.8C359.6,446.8,359.6,446.8,359.5,446.8L359.5,446.8z M234,724.9c0,0.1,0,0.1,0.1,0.2\n  c0,0,0.1-0.1,0.1-0.1C234,724.9,234,724.9,234,724.9z\"/>\n  <path class=\"st0\" (click)=\"onClickPath($event)\" d=\"M239.7,716.1c-1.7,2.7-3.5,5.4-5.6,8.8c-0.1,0-0.1,0-0.1,0c-0.5-1.2-1.2-2.3-1-2.6c1.5-2.1,3.1-4.1,4.8-6\n  C238.1,715.9,238.7,715.9,239.7,716.1z\"/>\n  <path d=\"M406.4,266.3L406.4,266.3L406.4,266.3L406.4,266.3C406.4,266.3,406.4,266.3,406.4,266.3z\"/>\n  <path d=\"M302.8,248.7c0.2,0.2-0.2,0.8-0.3,1.2l0.1-0.1c2.7,2.5,0.9,5.5,1,8.3c0.5,10.4,1.1,20.7,1.6,31.1c0.1,1.2-0.2,2.4-0.4,4\n  c-2.7-1.2-4.9-2.1-7-3c-0.1,0.3-0.2,0.5-0.3,0.8c2.3,1,4.7,1.9,7.1,2.9c-0.9,2.6-2.5,3.4-5,2.1c-2.3-1.2-4.5-2.4-6.8-3.6\n  c-0.1,0.2-0.2,0.4-0.3,0.5c2.5,1.5,5.1,3.1,8,4.8c-1.5,0.3-2.8,0.5-4,0.6c-0.5,0.1-1.3-0.2-1.5,0c-2.1,3.5-4.7,1.5-7.2,0.8\n  c-1.4-0.4-2.8-1-4.1-1.5c-0.1,0.2-0.2,0.3-0.2,0.5c1.6,0.8,3.2,1.6,5.4,2.7c-6.7,1-12.7,2.1-17.4-3.7c-0.1,0.3-0.2,0.6-0.2,1\n  c1.4,1.2,2.9,2.3,4.3,3.5c-2.7-0.9-5.7-0.3-8.4-0.8c-2.6-1.5-5.7-3.8-7.5-7.1c-1.7-3.1-2.9-8-3.6-12.3c0,0,0,0,0.1,0\n  c-1.1-4.7,0.3-10.1-4.5-13.8c-0.5-0.4-0.6-1.7-0.3-2.4c0.2-0.5,1.2-1.1,1.8-1c2.2,0.3,4.3,0.8,6.4,1.3c0.1-0.2,0.1-0.4,0.2-0.6\n  c-2.5-0.6-4.9-1.2-7.9-1.8c1.1-1.9,2.4-3.9,3.3-6.1c2.2-4.9,5.2-8.9,10.7-10c9-1.9,18.1-3.7,27.4-2.9\n  C296.8,244.2,300.2,245.6,302.8,248.7z\"/>\n  <path d=\"M246.1,307.2c-0.1-0.3-0.1-0.7-0.3-1c0.1,0.4,0.1,0.8,0.3,1.2C246.1,307.3,246.1,307.2,246.1,307.2z\"/>\n  <path d=\"M202.2,282.4c-0.8,1-1.4,2-1.9,3.1c-0.2,0-0.3-0.1-0.5-0.1c0,0,0,0.1,0,0.1c0.2,0,0.3,0.1,0.5,0.1c0,0,0,0,0,0l0-0.1\n  C200.8,284.4,201.5,283.4,202.2,282.4L202.2,282.4z\"/>\n  <path d=\"M193.1,322c-0.2-0.3-0.4-0.6-0.6-0.9c0.4,0.8,0.6,1.2,0.6,1.2C193.1,322.2,193.1,322.1,193.1,322z\"/>\n  <path d=\"M213.6,393.6l0,0.1L213.6,393.6L213.6,393.6z M213.6,393.6l0,0.1L213.6,393.6L213.6,393.6z\"/>\n  <path d=\"M412.5,271.8c1.4,1.8,2.6,3.6,3.6,5.4c0.2-0.2,0.5-0.3,0.8-0.4C414.6,273.8,412.8,272,412.5,271.8z\"/>\n  <path class=\"st0\" (click)=\"onClickPath($event)\" d=\"M401,414.9c-1.8,1.5,0.3,5.2-3.7,5.7c-0.4-0.4-0.7-0.9-0.8-1.4c0.1-0.3,0.2-0.5,0.3-0.8\n  c1.2-1.3,2.3-2.7,3.5-4.1C400.4,414.5,400.7,414.7,401,414.9z\"/>\n  <path class=\"st0\" (click)=\"onClickPath($event)\" d=\"M402.9,424c-2.6-1.4-2.7-4.8-0.2-8.9C405.3,418.1,404,421,402.9,424z\"/>\n  <path class=\"st0\" (click)=\"onClickPath($event)\" d=\"M407.1,419.7c-0.2-0.1-0.5-0.2-0.7-0.2c-1.1-2.2-2.1-4.4-3.3-6.9c0.7-0.3,2-0.9,3.6-1.6\n  C406.8,414.2,406.9,416.9,407.1,419.7z\"/>\n  <path class=\"st0\" (click)=\"onClickPath($event)\" d=\"M429.4,354.3c0,0.4,0,0.9-0.1,1.3c0,0.8-0.1,1.5-0.2,2.3c-0.3,2.3-0.8,4.6-1,6.9c-0.1,1.3-0.2,2.6-0.1,4\n  c0,0,0,0,0,0c0,0,0,0.1,0,0.1c0,0.1-0.1,0.2-0.1,0.3c0,0,0,0.1,0,0.1c-0.3,1.7-0.6,3.5-1,5.2c0,0,0,0.1,0,0.1c0,0.1-0.1,0.3-0.1,0.4\n  c-0.1,0.1-0.1,0.3-0.2,0.4c-0.3,0.9-0.7,1.7-1,2.6c0,0.1-0.1,0.3-0.1,0.4c-0.1,0.2-0.1,0.4-0.1,0.6c-0.5,2.6-0.4,5.3-0.9,7.8\n  c-1.1,5.4-2.3,10.7-3.5,16.3c0,0,0,0.1,0,0.1c-1-0.7-1.6-1.1-2.1-1.4c0,0,0,0-0.1,0c0.7,2,1.5,3.9,2.2,5.9c0,0,0,0,0,0.1\n  c-0.3,0.1-0.6,0.3-0.8,0.4c-0.5-0.3-1-0.6-2.2-1.3c0,0-0.1-0.1-0.1-0.1c3.3,4.1,3.6,8.2,3.6,12.4c0,0.1,0,0.2,0,0.3\n  c0,0.1-0.1,0.3-0.1,0.4c0,0.1-0.1,0.2-0.1,0.3c0,0.1-0.1,0.2-0.1,0.3c0,0.1-0.1,0.2-0.1,0.2c-0.2,0.5-0.6,1-1,1.2\n  c-1.9,0.8-4,1.2-6,1.7c0,0,0,0-0.1,0c0.4,0.5,0.7,0.8,1.4,1.5c0,0,0,0,0.1,0.1c-1.6,0.2-2.8,0.3-4.3,0.4c0,0-0.1,0-0.1,0\n  c0.8,0.6,1.4,1,2.6,1.9c0,0,0.1,0,0.1,0.1c-2.1,0.4-3.5,0.6-5.2,0.9c-0.1,0-0.1,0-0.1,0c0.8,0.3,1.5,0.6,2.1,0.9\n  c0.1,0.3,0.1,0.6,0.1,0.8c0,0,0,0,0,0.1c-2.3,0.8-4.6,1.7-6.9,2.5c-2.3,0.7-3.3-0.2-3.1-2.4c0-0.1,0-0.2,0-0.3c0,0,0,0,0,0\n  c0-0.1-0.1-0.3-0.2-0.4c-0.4,0.4-0.7,0.8-1.7,1.9c-1.1-3.5-2.1-6.4-3.1-9.5c0,0,0,0,0-0.1c0.1-0.3,0.1-0.6,0.2-0.8l4,3.2l0-0.1\n  l0-0.1c0,0,0,0,0,0c1.3,1.1,3,2.5,3.7,2.2c1.8-0.8,3.7-2.6,4.3-4.5c0,0,0-0.1,0-0.1c0.1-0.2,0.1-0.4,0.2-0.6\n  c0.2-0.8,0.3-1.6,0.4-2.4c0.1-1.8,0-3.7-0.1-5.5c-0.1-1-1.2-2.7-2.1-2.9c-3.1-0.8-7.9,2.5-9.7,6.3c-0.1,0.2-0.2,0.5-0.3,0.7\n  c-0.1,0.2-0.2,0.5-0.2,0.7c-0.1,0.2-0.1,0.5-0.1,0.7c0,0,0,0.1,0,0.1c-0.2,0.3-0.4,0.7-0.6,1.2c-0.1,0.1-0.1,0.3-0.2,0.4\n  c-0.1,0.2-0.1,0.4-0.2,0.6c-0.2,0.8-0.3,1.7-0.4,2.5v0.1c-0.9,2-1.9,4-2.7,6.1c0,0,0,0,0,0s-4,1-3.5-4.8c0.1-1,0.3-2.1,0.7-3.5\n  c0.5-0.8,1-1.5,1.2-2.3c0,0,0-0.1,0-0.1c0.6-2.2,1.1-4.5,1.3-6.7c0.1-1.1,0.2-2.2,0.5-3.2c0.1-0.2,0.1-0.4,0.2-0.6\n  c0.8-2.7,2.5-4.9,5.4-6.4c1.5-0.8,2.9-1.9,4-3.2c0.7-0.8,1.6-1.9,1.8-2.8c0-0.1,0.1-0.2,0.1-0.3c0-0.1,0-0.1,0-0.2\n  c0-0.1,0-0.2-0.1-0.3c-0.2-0.5-0.3-1-0.3-1.5c0-0.2,0-0.3,0-0.4c0-0.1,0-0.3,0-0.4c0-0.3,0.1-0.7,0.2-1c0.1-0.2,0.1-0.4,0.2-0.5\n  c0.4-1.2,1.1-2.3,1.7-3.5c0.7-1.5,1.3-3,2-4.4l4.9-6.1c-0.2,0.6-0.4,1.2-0.6,1.8c-0.4,1.3-0.8,2.6-1.2,3.9c-0.3,1-0.6,1.9-0.9,2.9\n  c-0.4,1.4-0.9,2.8-1.3,4.2c0,0.2-0.1,0.3-0.2,0.5c-0.4,1.2-0.8,2.5-1.2,3.7c0,0,0,0.1,0,0.1c4,1.1,7.3,1.9,10.8,2.8\n  c0.3-0.7,0.9-1.6,1.1-2.5c0,0,0-0.1,0-0.1c0.5-2.2,1.1-4.4,1.6-6.6c0.3-1.2,0.6-2.4,0.8-3.7c0.2-0.8,0.3-1.5,0.5-2.3\n  c0.3-1.7,0.7-3.4,1-5.1c0.4-2.4,0.9-4.8,1.1-7.2c0.2-2,0.2-3.9-0.2-5.7c-0.3-1.5-0.4-2.9-0.3-4.2c0.1-1.1,0.3-2.1,0.6-3.1\n  c0.2-0.5,0.3-1,0.5-1.5v0c0.2-0.4,0.4-0.9,0.5-1.3c0.2-0.4,0.4-0.8,0.5-1.1c0.3-0.6,0.6-1.3,1-1.9c0.7-1.3,1.3-2.6,1.9-3.9\n  c0.3-0.6,0.5-1.3,0.8-1.9c0.1,1.3,0.2,2.7,0.3,4c0,0.7,0.1,1.3,0.1,2C429.4,352.3,429.4,353.3,429.4,354.3z\"/>\n  <path class=\"st0\" (click)=\"onClickPath($event)\" d=\"M265.8,322.2C265.8,322.2,265.8,322.2,265.8,322.2c-1.9,1.7-3.7,3.3-5.5,4.8c-0.3-0.1-0.6-0.2-0.9-0.3v-26\n  c-0.1,0-0.1-0.1-0.1-0.2c0-0.1,0-0.2,0.1-0.4c0.2-0.6,0.5-1.4,0.8-2.1c2.3,2.6,4.6,4.1,4.6,4.1l0.3,0c3.4,2,7.2,2.4,11.2,3.1\n  c0,0,0.1,0,0.1,0c-1.6,0.7-3.2,1.4-4.9,2.2c0,0,0,0,0,0c-8.2,3.1-9.3,10-9.4,10.9v0l0,0c0,0,0,0,0,0.1\n  C263.3,319.5,264.4,320.8,265.8,322.2z\"/>\n  <path class=\"st0\" (click)=\"onClickPath($event)\" d=\"M224,419.3c0.1,0.3-0.9,1.2-1.8,2.3c-0.7-2.9-1.1-4.7-1.6-6.6c0.3-0.2,0.6-0.3,0.9-0.5\n  C222.3,416.1,223.3,417.7,224,419.3z\"/>\n  <path class=\"st0\" (click)=\"onClickPath($event)\" d=\"M219.4,424.5c-2.4-2-2.9-5.3-1.2-8.9c1,2.4,2,4.6,2.6,6.8C220.9,422.9,219.9,423.7,219.4,424.5z\"/>\n  <path class=\"st0\" (click)=\"onClickPath($event)\" d=\"M212,437.8c-0.2,0.3-0.4,0.6-0.6,0.9c-5.3-1.5-9.1-5.7-13.8-8.2c0.2-0.3,0.4-0.7,0.7-1\n  C202.9,432.3,207.5,435.1,212,437.8z\"/>\n  <path class=\"st0\" (click)=\"onClickPath($event)\" d=\"M216.6,435.4c-2.5,1.2-11.9-3.2-14.9-6.9c0.5-0.2,1.1-0.3,2.6-0.8c-3.3,0-6.1,0.4-5.6-4.5\n  C205,427.5,210.8,431.4,216.6,435.4z\"/>\n  <path class=\"st0\" (click)=\"onClickPath($event)\" d=\"M232.1,422.8c0,0.2,0,0.4,0,0.6c0,0.1,0,0.2-0.1,0.3c-0.2,0.9-0.9,1.3-2.6,1.8c0,0,0,0,0,0\n  c0.3,1.4,0.6,2.8,0.9,4.6c0,0,0,0.1,0,0.1c-3.1-2-2.8-5.1-3.2-7.7c-1.1-6.9-4.1-10.5-10.9-11.8c-0.4-0.1-0.9,0-1.4,0.2\n  c0,0-0.1,0-0.1,0c1.8,1,2.9,1.7,3.6,2c0,0,0,0,0,0c-1.3,2.5-2.5,4.7-3.7,7v-9c-0.4,0.2-0.7,0.4-0.9,0.6c-0.1,0.2-0.2,0.4-0.3,0.6\n  c0,0.1-0.1,0.2-0.1,0.3c0,0.1-0.1,0.2-0.1,0.4c-0.1,0.5-0.2,1.1-0.3,1.7c-0.3,3.7,0.7,9.2,2.4,10.8c2.4,2.3,3.6,2.3,5.8-0.2\n  c1-1.1,1.9-2.3,2.9-3.6c0.7,1.1,1,2.4,0.9,3.8c0,0.5-0.1,1-0.3,1.4c-0.5,2.1-1.9,4.2-4.1,6c-1.7,1.5-6.8-0.1-9.6-3.1c0,0,0,0,0.1,0\n  c0.6-0.3,1.1-0.5,1.7-0.7c0,0-0.1,0-0.1,0c-1.5-0.3-2.9-0.7-5.2-1.2c0,0,0.1,0,0.1-0.1c1.5-0.8,2.3-1.1,3.6-1.7c0,0-0.1,0-0.1,0\n  c-2-0.2-3.4-0.3-5.5-0.5c0,0,0.1-0.1,0.1-0.1c1.1-0.7,1.5-1,1.8-1.2c0,0,0,0-0.1,0c-1.4-0.6-2.8-1.7-4.3-1.7c-2.3,0-3-0.9-3-2.4\n  c0-0.3,0-0.5,0-0.8c0-0.2,0-0.3,0.1-0.5c0.4-2.8,1-5.5,1.5-8.3c0.1-0.3,0.3-0.6,0.3-0.8c0,0,0,0,0,0c0,0,0,0,0,0\n  c-0.8-0.8-1.1-1.6-1.2-2.3c0-0.2,0-0.4,0-0.6c0-0.1,0-0.2,0-0.3c0.1-0.2,0.1-0.4,0.2-0.6c0.5-1.4,1.8-2.7,2.3-4.2c0,0,0,0,0,0\n  c-1.1,0.8-1.8,1.4-3.1,2.4c-1.6-9-3.1-17.6-4.6-26c0,0.1-0.1,0.2-0.1,0.2c-0.2,0-0.5,0-0.7,0c-1.2-11.1-2.3-22.1-3.5-33.2\n  c0,0,0,0,0,0c0.3-0.1,0.6-0.2,0.9-0.3c0.6,1.6,1,3.3,2,4.6c3.9,5.4,3.1,11.5,3.7,17.5c1,9.8,3.3,19.5,5.1,29.2\n  c0.1,0.6,0.6,1.2,0.9,1.7c3.3-1.2,6.5-2.3,9.6-3.5c0,0,0-0.1,0.1-0.1v-0.1c0-0.1,0-0.3,0-0.4c0-0.1,0-0.2,0-0.3l0-0.1l-0.3-1.3\n  c0,0,0,0,0,0s0,0,0,0c0,0,0,0,0-0.1c-1-4.1-2-8.2-3-12.3c-0.1-0.3-0.2-0.6-0.3-1l0,0l3.7,3.5c1.2,2.9,2.5,5.7,3.6,8.7\n  c0.3,0.7,0.3,1.4,0.4,2.1c0,0,0,0.1,0,0.1c0,0.8,0,1.6,0.2,2.3c0.2,1,0.4,2,1,2.7c1.2,1.4,2.5,2.8,4,3.6c4.7,2.3,6.5,6.2,7,11.2\n  c0.3,2.8,1,5.5,1.7,8.3C232,421.9,232.1,422.4,232.1,422.8z\"/>\n  <path class=\"st0\" (click)=\"onClickPath($event)\" d=\"M205.3,393c0,0,0.5,0.7,1.1,1.7c-0.6,0.5-1.2,0.9-2.2,1.6c-1.6-12.1-3.2-23.7-4.8-35.3c0.4-0.1,0.9-0.3,1.3-0.4\n  c2.8,6.3,5.5,12.5,8.4,18.9c-0.6,0.1-1,0.2-1.1,0.2c1.6,3.9,3.1,7.8,4.7,11.8l0.1,2C210.2,393.3,207.6,393.2,205.3,393z\"/>\n  <path class=\"st0\" (click)=\"onClickPath($event)\" d=\"M255.4,319.9c1.1,1.9,2.1,3.7,3.1,5.4c-0.3,0.2-0.6,0.4-0.9,0.6c-2.1-3-4.2-6-6.5-8.8c0,0-0.1-0.1-0.1-0.1\n  c0,0,0,0,0-0.1c-0.7-1.1-1.9-3-3-5.1c-0.3-0.6-0.6-1.1-0.8-1.7c-0.4-1-0.8-2-1.1-2.9c-0.1-0.4-0.2-0.8-0.3-1.2c0,0,0,0,0-0.1\n  c-0.2-0.7-0.6-1.4-0.9-2c-1.5-3-2-5.9-1.1-9.3c1.3-4.9,1.9-10.1,2.9-15.1c0.7-3.6,1.5-7.2,2.3-10.7c0.3-0.1,0.7-0.2,1-0.2\n  c0.9,1.5,2.1,2.8,2.6,4.4c0.5,1.5,0.7,3,0.9,4.6v0c0.3,2.9,1.1,8.7,2.6,13.4c0.6,2.1,1.8,4,3,5.6c-1,1.9-2.2,6.5-2.2,7.2\n  c0,3.1,0.4,6.1,0.4,9.1c0,1.1-0.8,2.3-1.4,3.9c0.2,0.6,0.6,1.9,1.3,4C256.2,320.3,255.9,320.2,255.4,319.9z\"/>\n  <path class=\"st0\" (click)=\"onClickPath($event)\" d=\"M311.5,222.5h-0.2c-0.3,0-0.6,0-0.9,0c-6.2-0.1-6.7-5.8-9.3-9.7c6.2,2.1,12.4,2.3,18.7,0.1\n  C317.8,216.9,317,222,311.5,222.5z\"/>\n  <path class=\"st0\" (click)=\"onClickPath($event)\" d=\"M269.7,234.7c-2.6-0.4-5.2-1.1-7.8-1.8c-0.3-0.1-0.4-1-0.6-1.5c-1,0.3-2.2,0.7-3.4,1c-0.2-0.4-0.4-0.8-0.5-1.2\n  c0.3-0.2,0.5-0.3,0.8-0.4c0.3-0.1,0.6-0.2,0.8-0.5c9.7-5.4,19.5-10.8,29.2-16.2c0.2,0.3,0.4,0.5,0.6,0.8c-1.3,4.8-3.7,8.8-6.7,12.2\n  c0,0,0,0,0,0c-0.4,0.4-0.9,0.8-1.2,1.3c-2.1,2.2-4.4,4.1-7,5.9C273,235.1,271.1,234.9,269.7,234.7z M295.8,231\n  c0.6-1.8,1.2-3.8,1.9-6c1,3.8,1.8,6.9,2.7,10.3h-6.1c0.1-0.3,0.2-0.5,0.3-0.8c0,0,0,0,0,0S295.7,232.6,295.8,231\n  C295.8,231,295.8,231,295.8,231z M295.3,223.3c-0.3,2.7-0.9,5.4-1.5,8h0c-0.2,0.6-0.5,1.4-0.6,2.2c-0.6,2.1-1.4,2-5,1.7\n  c0.5-1.3,0.9-2.4,1.3-3.5c-0.2-0.1-0.4-0.2-0.6-0.3c-0.6,1.2-1.2,2.5-1.8,3.8h-6.9c1.2-1.3,2.2-2.4,3.2-3.5\n  c-0.1-0.1-0.2-0.2-0.3-0.3c-1.9,1.5-3.8,3-5.7,4.5c-0.3-0.3-0.5-0.6-0.8-0.9c2.2-2.2,4.5-4.3,6.5-6.6h-0.1c0.7-0.6,1.5-1.4,1.7-1.8\n  c0,0,0-0.1,0-0.1c3.7-4.5,6.6-9.7,7.3-16.6C294.7,214.6,295.8,218.8,295.3,223.3z M348.2,222.5c0,0,0.1,0,0.1,0.1\n  c0.5,0.3,1.1,0.6,1.8,1c0.1,0.1,0.3,0.2,0.4,0.2c4.5,2.5,8.8,4.9,13.2,7.3c-0.1,0.4-0.3,0.7-0.4,1.1c-1-0.3-2.1-0.6-3.1-0.9\n  c0.1,0.2,0.3,0.4,0.4,0.6c-1.7,0.7-3.6,2.2-5.2,1.9c-1.9-0.3-3.5-2.1-5.3-3.2c-0.2,0.2-0.5,0.4-0.7,0.7c1.2,0.9,2.5,1.8,3.8,2.8\n  c-3.8,2.2-4-2.2-6.7-2.9c1.1,1.3,1.9,2.3,2.9,3.4c-4.6-0.3-15.1-10.6-17.3-21C337.7,216.7,343,219.6,348.2,222.5z M329.2,209.9\n  c1.4,11,8.1,18.2,15.6,25c-1.6,0.2-1.6,0.2-6.5-3.3c1,1.1,1.9,2.3,3.1,3.7h-12.6c-0.4-1.3-0.8-2.5-1.1-3.8h0\n  c-0.1-0.6-0.2-1.1-0.3-1.5C325.7,223.5,325.3,216.9,329.2,209.9z M323.5,225.7c0.2,0,0.4,0,0.6,0.1c0.4,1.5,0.9,2.9,1.3,4.3\n  c0.1,0.7,0.3,1.4,0.6,1.9c0.3,1,0.6,2.1,1,3.1h-6C321.9,231.9,322.7,228.8,323.5,225.7z\"/>\n  <path class=\"st0\" (click)=\"onClickPath($event)\" d=\"M297.8,290.2c-0.1,0.3-0.2,0.5-0.3,0.8c2.3,1,4.7,1.9,7.1,2.9c-0.9,2.6-2.5,3.4-5,2.1c-2.3-1.2-4.5-2.4-6.8-3.6\n  c-0.1,0.2-0.2,0.4-0.3,0.5c2.5,1.5,5.1,3.1,8,4.8c-1.5,0.3-2.8,0.5-4,0.6c-0.5,0.1-1.3-0.2-1.5,0c-2.1,3.5-4.7,1.5-7.2,0.8\n  c-1.4-0.4-2.8-1-4.1-1.5c-0.1,0.2-0.2,0.3-0.2,0.5c1.6,0.8,3.2,1.6,5.4,2.7c-6.7,1-12.7,2.1-17.4-3.7c-0.1,0.3-0.2,0.6-0.2,1\n  c1.4,1.2,2.9,2.3,4.3,3.5c-2.7-0.9-5.7-0.3-8.4-0.8c-2.6-1.5-5.7-3.8-7.5-7.1c-1.7-3.1-2.9-8-3.6-12.3c0,0,0,0,0.1,0\n  c-1.1-4.7,0.3-10.1-4.5-13.8c-0.5-0.4-0.6-1.7-0.3-2.4c0.2-0.5,1.2-1.1,1.8-1c2.2,0.3,4.3,0.8,6.4,1.3c0.1-0.2,0.1-0.4,0.2-0.6\n  c-2.5-0.6-4.9-1.2-7.9-1.8c1.1-1.9,2.4-3.9,3.3-6.1c2.2-4.9,5.2-8.9,10.7-10c9-1.9,18.1-3.7,27.4-2.9c3.8,0.3,7.3,1.7,9.8,4.8\n  c0.2,0.2-0.2,0.8-0.3,1.2l0.1-0.1c2.7,2.5,0.9,5.5,1,8.3c0.5,10.4,1.1,20.7,1.6,31.1c0.1,1.2-0.2,2.4-0.4,4\n  C302.1,292,299.9,291.1,297.8,290.2z M347.4,299.5c0.7-0.5,1.5-0.9,2.2-1.3c0.1,0.1,0.2,0.3,0.3,0.4c-1.2,1-2.3,1.9-3.5,2.9\n  c0.1,0.2,0.2,0.5,0.3,0.7c1.9-0.7,3.8-1.4,5.8-2.1c0.5-0.2,1.3-0.6,1.3-0.5c1.5,2.4,2.8,0.1,3.4-0.5c2-2.2,4.4-4.5,5.4-7.2\n  c1.6-4.3,2.3-8.9,3.2-13.5c0.8-3.9,0.4-8.2,4.3-10.9c0.4-0.3,0.6-1.5,0.4-2.1c-0.2-0.6-1.1-1.4-1.6-1.3c-2.2,0.3-4.4,0.9-6.6,1.3\n  c-0.1-0.2-0.1-0.4-0.2-0.5c2.4-0.7,4.8-1.3,7.5-2.1c-0.8-1.7-1.8-3.5-2.3-5.5c-0.6-2.3-2.3-2.5-4.2-2.9c-4.1-0.7-8.2-1.6-12.4-2.4\n  c0-0.2,0.1-0.4,0.1-0.6c4.8,0.9,9.5,1.7,15.2,2.7c-1.6-1.1-3-1.5-3.4-2.4c-1.8-4.3-5.6-4.7-9.4-5.4c-4.3-0.8-8.5-1.9-12.8-2.2\n  c-5.2-0.3-10.6-0.4-15.7,0.6c-3.3,0.7-5.5,1.9-6.5,4.6c-0.4,1.2-0.7,3.2-0.7,5c0.1,1.7,0.1,3.4,0.1,5.2c-0.4,9.6-0.9,19.3-1.3,29\n  c-0.1,1.7,0.3,3.4,0.6,5c0.6,3,1.9,3.8,4.7,2.6c2.5-1.1,4.9-2.5,7.3-3.7c0.1,0.2,0.2,0.4,0.3,0.5c-2.7,1.6-5.4,3.1-7.9,4.6\n  c3.8,2.5,7.1-0.5,10.9-1.1c-1.9,0.9-3.7,1.9-5.9,2.9c5,2.3,8.8-1.1,13.1-1.7c-2,1-4,1.9-6,2.9c1.2,0.7,2.4,0.8,3.5,0.8\n  c0.9,0,2.2-0.8,2.6-0.4C342.8,303.4,345,301.1,347.4,299.5z\"/>\n  <path class=\"st0\" (click)=\"onClickPath($event)\" d=\"M362.1,306.8c-0.1,5.6-2.8,7.5-7.6,4.7c-3.3-1.9-6.2-4.5-9.8-7.1c6.6,0,12.5-1.1,17.4-6.7\n  C362.2,301.4,362.2,304.1,362.1,306.8z\"/>\n  <path class=\"st0\" (click)=\"onClickPath($event)\" d=\"M288.9,415.7c-0.2,0.2-0.5,0.4-0.7,0.5c-1.5-1.7-3-3.4-4.6-5.2c-0.1,0.4-0.2,0.8-0.4,1.8\n  c-2.1-2.6-3.8-4.9-5.6-7.1c-1.9-2.3-3.6-4.7-6-6.8c-3.8-3.4-6.5-8.4-8.9-13.2c-1.2-2.4-0.9-5.5-1.2-8.3c-0.2-1.8-0.4-3.6-0.5-5.4\n  c0.2-0.1,0.4-0.2,0.6-0.3c0.3,0.4,0.5,0.9,0.8,1.3c0.1-0.1,0.2-0.2,0.4-0.3c-0.3-1-0.6-2-0.9-2.8c5.4,3.9,10.8,7.6,16,11.6\n  c2.6,2,4.1,4.6,3.6,8.1c-0.2,1.4,1.4,3.1,1.2,4.5c-0.5,4.6,2.6,8.3,2.4,12.9c-0.1,2.1,2,4.3,3.1,6.4\n  C288.5,414.2,288.7,415,288.9,415.7z M275.6,373.8c-1.7-0.2-3.3-1.5-5-2.3c0.1-0.2,0.2-0.3,0.2-0.5c2.2,0.9,4.4,1.8,6.6,2.7\n  c0.3-0.3,0.6-0.7,0.9-1c-0.9-1.2-1.8-2.4-2.5-3.4c0.3,0.1,1,0.2,2.2,0.4c-0.2-1.6-0.3-2.8-0.4-4.1c0-0.3-0.3-0.6-0.3-0.8\n  c1.3-1.8,0.6-3-1.3-3.6c-1.2-0.4-2.5-0.8-3.7-1.2c-0.1,0-0.1-0.3-0.2-0.4c0.8,0.1,1.6,0.2,3,0.4c-4.7-3.9-8.8-7.5-13-11\n  c-0.8,2.6,2.8,5.7-0.7,8.3c0.5,5.3,1.1,10.6,5.9,14c3.4,2.4,6.9,4.6,11.1,7.4C278.2,376,278.5,374.2,275.6,373.8z M276.8,350.6\n  c1.2-6.6,0.9-7.1-3.3-7.4c0.4-0.2,0.8-0.4,2-1.1c-5-0.4-9.5-0.3-12.2-4.6c-0.2,0.2-0.3,0.5-0.5,0.7c0.5,1.4,1,2.7,1.6,4.3\n  c-0.6-0.3-1-0.4-1.8-0.8c1.7,8.2,8.2,11.9,13.7,17.1c0.2-2.6,0.5-4.6,0.4-6.7c0-0.5-1-1-1.6-1.4c-1.4-0.8-2.8-1.5-4.2-2.3\n  c0.1-0.1,0.2-0.2,0.3-0.4C272.9,348.9,274.8,349.7,276.8,350.6z M276.5,326c-1.3-0.3-2.5-0.5-3.8-0.8c3.8,0.2,4,0.1,4.1-1.9\n  c-2.8-0.2-5.5-0.4-8.3-0.5c-0.6,0-1.3,0.4-1.7,0.9c-1.3,1.4-2.6,2.9-4.1,4.6c4,2.7,6.5,8.2,12.3,8.4c0.2-0.3,0.4-0.6,0.6-0.9\n  c-1.7-1-3.5-2-5.2-2.9c0.1-0.2,0.2-0.3,0.2-0.5c1.4,0.7,2.9,1.4,4.7,2.3c0.2-1.4,0.4-2.3,0.5-3.1c-1.7-0.4-3-0.7-4-0.9\n  c1.5-0.4,3-0.8,4.6-1.2c-0.9-0.6-1.9-1.3-2.4-1.7c0.7-0.2,1.8-0.7,2.9-1.1C276.7,326.5,276.6,326.3,276.5,326z M276.9,316.7l1.7-5.2\n  h0c-0.2-0.2-0.4-0.3-0.6-0.4c0,0,0,0,0-0.1c1.1-2,2.2-4,3.3-6c0,0,0,0,0,0c-0.2-0.2-0.4-0.5-0.6-0.7c-2,1.3-4.1,2.6-6,4\n  c-1.1,0.8-1.9,0.9-2.5,0.4c-2.1,1.3-9.6,6.5-7.6,9.5c0.3,0.1,0.5,0.3,0.6,0.5c1.1,1.5,2.3,2,4.1,1.9c2.4-0.1,4.8,0,7.6,0\n  c0-0.8-0.1-2-0.2-3.2c-0.3-0.1-0.6-0.1-0.9-0.2c0,0,0.1,0,0.1-0.1C276.4,317.2,276.7,316.9,276.9,316.7z M344,318.8\n  c0.1,1.9,0.3,3.3,2.9,2.2c1.1-0.5,2.6-0.1,3.9-0.1c4.1,0,6.1-1.6,7.2-5.8c-7.5-0.6-11-8.2-17.7-10.3c1.5,2.1,0.9,5.9,4,5.9\n  c-0.2,0.8-0.8,1.7-0.8,2.6C343.5,315.1,343.8,317,344,318.8z M358.7,328.3c-1.6-1.9-2.9-3.6-4.3-5.1c-0.4-0.4-1.2-0.5-1.9-0.5\n  c-2.6,0.1-5.1,0.3-8.1,0.6c0.4,2.9,3.1,1.1,4.6,2c-1.5,0.3-3,0.5-4.4,0.8c0,0.3-0.1,0.6-0.1,0.9c1.3,0.3,2.6,0.7,3.3,0.9\n  c-0.6,0.4-1.7,1-2.7,1.7c1.6,0.4,3.2,0.8,4.9,1.2c-1.2,0.2-2.5,0.5-4.5,0.9c0.4,1,0.7,2,1.1,3.2c1.7-0.8,3-1.5,4.3-2.1\n  c0.1,0.2,0.1,0.3,0.2,0.5c-1.7,0.9-3.4,1.9-5.2,2.8c0.2,0.3,0.4,0.5,0.6,0.8C352.3,336.6,354.3,330.7,358.7,328.3z M344.4,344.7\n  c0,4.6,0.4,9.1,0.7,13.7c0.3,0.1,0.6,0.2,0.9,0.3c0.8-1,1.5-2.1,2.5-2.9c6-4.7,11-9.9,9.9-18.6c-2.5,4.5-7.2,4.5-11.6,5.5\n  c0.3,0.1,0.6,0.2,0.7,0.2C346.7,343.4,344.4,344.1,344.4,344.7z M346.5,369.5c-0.9,0.4-1.8,0.8-2.5,1.1c-0.3,2.4-0.5,4.7-0.9,8\n  c4.3-2.9,8-5.2,11.4-7.8c3.5-2.6,4.5-6.5,5.1-10.6c0.1-0.5-0.4-1.1-0.7-1.7c-0.3,0.1-0.6,0.3-0.9,0.4c0.5-3.5,1-7,1.5-10.2\n  c-4.2,3.4-8.5,6.5-12.2,10.2c-1.1,1.1-1.8,2.5-3.5,2.9c0.3,1.1-1.7,3.1,1.3,3.2c-1.5,1-2.1,4-0.9,4.7c0.6-0.2,1.4-0.3,2.1-0.5\n  C346.4,369.3,346.5,369.4,346.5,369.5z M360.3,371.6c-0.8,2.1-1.4,3.8-2.1,5.6c-0.2-0.1-0.4-0.1-0.5-0.2c0.9-2.8,1.8-5.6,2.6-8.3\n  c-3,2.5-5.9,5-9,7.4c-2.3,1.8-4.9,3.2-7.3,4.9c-2.8,2.1-4.1,5-4,8.7c0.8-1,1.5-2.1,2.2-3.1c-0.1,1.2-0.7,2-1.1,3\n  c-1,1.9-2.5,3.9-2.6,5.9c-0.1,2.9-1.4,5.3-1.8,7.9c-0.6,4.1-2.4,7.6-4.1,11.2c-0.1,0.3-0.2,0.7-0.3,1.3c3.8-0.6,5.1-4.2,7.8-6\n  c4.8-3.1,7.1-9,11.6-13C358.9,390.6,360.9,381.6,360.3,371.6z\"/>\n  <path class=\"st0\" (click)=\"onClickPath($event)\" d=\"M361.7,327.4c-2.4-2.2-4.1-3.7-5.7-5.1c2.1-3,4.1-5.8,6.7-9.3C362.4,318.1,362.1,322.4,361.7,327.4z\"/>\n  <path class=\"st0\" (click)=\"onClickPath($event)\" d=\"M378,300.2c-3.3,8.9-6.9,17.7-13.5,24.8c-0.2,0.2-0.5,0.4-0.8,0.6c0.3-1.9,0.5-3.7,0.8-5.4\n  c1-5.2,0.2-10.4-0.1-15.5c-0.1-2.3-0.3-4.6-0.4-6.8c-0.1-1.1-0.3-2.4,0.1-3.3c3-6.3,3.1-13.1,4.2-19.8c0.4-2.2,1.9-4.1,2.9-6.2\n  c0.4,0.1,0.9,0.2,1.3,0.3c0.9,4.6,1.8,9.2,2.7,13.8c1,5,2,9.9,2.9,14.9C378.2,298.4,378.3,299.4,378,300.2z\"/>\n  <path d=\"M214.7,410.9v0.1c0,0,0.1,0,0.1,0C214.8,411,214.8,410.9,214.7,410.9z M214.7,410.9v0.1c0,0,0.1,0,0.1,0\n  C214.8,411,214.8,410.9,214.7,410.9z M214.7,410.9v0.1c0,0,0.1,0,0.1,0C214.8,411,214.8,410.9,214.7,410.9z M199.9,316.1\n  c0.3,0.6,0.7,1.2,1,1.8C200.6,317.2,200.3,316.6,199.9,316.1C200,316,200,316.1,199.9,316.1z\"/>\n  <g>\n  <path d=\"M341.6,323.8c-1-4.2-1.9-8.6-3.2-12.7c-0.9-2.9-3.3-4.5-6.3-5.1c-4.4-0.9-8.8-1.9-13.3-2.7c-2.1-0.4-3.5,0.4-3.7,2.8\n  c-0.3,2.2-0.7,4.3-1.2,6.4c-0.1,0.4-0.3,1.1-0.4,1.6c-1.2,5.3,0.5,8,5.6,10c4.7,1.8,9.3,4.2,13.9,6.3c1.2,0.5,2.3,1.3,3.4,1.8\n  c2.7,1.1,4.1,0.1,4.2-2.7c0.1-1.8,0-3.7,0-5.5C341,323.9,341.3,323.8,341.6,323.8z\"/>\n  <path d=\"M339.4,328.8c-0.5-1-0.8-1.4-1-1.9c-0.2-0.4-0.2-0.9-0.8-1.3c0.2,1.6,0.5,3.1,0.7,4.7c-0.3,0.1-0.6,0.2-0.9,0.4\n  c-3.3-1.8,0-6.3-2.7-8.5c0.2,2.3,0.5,4.6,0.8,7.3c-4.2-2.8-9.2-4.2-8-10.8c-0.3,0.2-0.6,0.3-0.9,0.5c0.2,1.6,0.5,3.3,0.8,4.9\n  c-9-1.8-12.1-4.7-11.7-11c0-0.1,0-0.4,0-0.5c-0.1-1.5-0.6-3.4,1.7-4.7c-0.1-0.6-0.3-1.7-0.5-2.7c0.2-0.1,0.5-0.2,0.7-0.3\n  c0.9,1.5,1.8,3.1,2.7,4.6c0.1-0.1,0.3-0.1,0.4-0.2c-0.5-1.3-0.9-2.6-1.4-3.9c0.2-0.1,0.4-0.2,0.5-0.3c0.8,1.1,1.6,2.3,2.4,3.5\n  c0.1-0.7,0.2-1.4,0.4-2.7c1,1.4,1.8,2.5,2.6,3.7c0.1-0.7,0.1-1.4,0.2-3c1.2,1.7,2,2.8,3,4.3c-0.2-1.2-0.4-2.1-0.5-3\n  c4.8,1.8,4.8,1.8,5.9,4.4v-3.2C340.1,314.4,339.7,321.2,339.4,328.8z\"/>\n  </g>\n  <g>\n  <path d=\"M308.5,316.5c-0.1-1.1-0.3-2.8-0.5-3.9c-0.3-1.9-0.6-3.7-0.8-5.6c-0.3-3.3-1.5-4.2-4.7-3.5c-3.8,0.8-7.7,1.6-11.5,2.2\n  c-3.8,0.7-6.7,2.4-7.8,6.2c-1.1,3.8-1.9,7.6-2.8,11.4c0.1,0.1,0.3,0.1,0.4,0.2c0.2,2.3,0.5,4.6,0.7,6.9c0.1,1.7,1.1,2.4,2.6,2\n  c1.1-0.2,2.1-0.7,3.1-1.2c5.5-2.5,10.9-5,16.4-7.4C307.4,322.3,308.8,320.6,308.5,316.5z\"/>\n  <path d=\"M305.3,319.3c0.2-1.2,0.4-2.4,0.6-3.7c-0.1-0.1-0.2-0.2-0.3-0.2c-0.3,0.7-0.9,1.4-0.8,2c0.5,3.2-1.4,3.9-3.9,4.8\n  c-4.9,1.9-9.5,4.2-14.6,6.6c0.2-2.1,0.5-4.6,0.8-7c-0.2,0-0.4-0.1-0.6-0.1c-0.2,2.2-0.4,4.4-0.7,6.5c-0.2,0.9-0.9,1.8-1.3,2.7\n  c-0.3-0.2-0.7-0.3-1-0.5c0.2-1.6,0.4-3.1,0.7-5.1c-0.6,0.7-1,1.1-1.8,2c-0.1-6.8,0.1-12.7,5.2-17.7c0.1,0.9,0.2,1.5,0.3,2.1\n  c0.3-3.8,3.8-2.8,5.9-4.2c-0.2,1-0.3,1.8-0.5,2.8c1.5-0.5,1.2-4.2,3.9-2c0.5-0.6,1-1.2,1.8-2.2c0.2,1.1,0.3,1.8,0.5,2.5\n  c0.8-1.2,1.5-2.3,2.3-3.5c0.2,0.1,0.5,0.2,0.7,0.3c-0.5,1.2-0.9,2.4-1.4,3.5c0.1,0.1,0.2,0.2,0.4,0.2c1-1.5,1.9-3,2.8-4.4\n  c0.3,0.2,0.5,0.3,0.8,0.5c-0.3,1.1-0.6,2.3-0.9,3.4c0.2,0.1,0.3,0.1,0.5,0.2c0.3-0.5,0.6-0.9,1.3-2.1c0.1,2.1,0.2,4,0.3,5.9\n  c0.1,2.1,0.3,4.7,0.4,6.7C306.2,319.3,305.8,319.3,305.3,319.3z\"/>\n  </g>\n  <g>\n  <path d=\"M300.5,326.2c-4.9,2.2-9.5,4.9-14.5,6.9c-3.5,1.5-4.9,3.9-5.1,7.5c-0.1,1.5,0,3.1,0,4.6h-0.1c0,1,0.1,2,0,3.1\n  c-0.5,6,2.8,6.7,7,6.3c4.4-0.5,8.8-1,13.1-1.9c2-0.4,5.1-1.7,5.3-3c0.2-1.2,0.4-2.3,0.6-3.5c0-0.1,0-0.3,0.1-0.4\n  c0.3-1.8,0.5-3.7,0.7-5.5c0,0,0-0.1,0-0.1c0.3-2.9,0.5-6.6,0.6-9.5C308.3,325.4,305.4,324,300.5,326.2z\"/>\n  <path d=\"M306.3,339.3c0,0.9-0.1,2.6-0.1,3.5l-0.1,3.6c-0.3,0.1-0.6,0.1-1,0.1c-0.5-1.7-1.1-3.3-1.6-5c-0.2,0.1-0.3,0.2-0.5,0.3\n  c0.4,1.6,0.7,3.1,1.2,4.7c1.1,3.7,1.2,3.8-2.5,4.3c-4.9,0.7-9.9,1.1-14.8,1.5c-0.4,0-0.8-0.1-1.4-0.2c-0.3-2.5-0.6-4.9-1-7.4\n  c-0.1,0-0.3,0-0.4,0c0.1,2.2,0.3,4.5,0.4,6.7c-2.4-1.7-2.1-14.2,0.6-16.7c0.2,0.6,0.3,1.1,0.5,1.6c0.1,0,0.2,0.1,0.2,0.1\n  c0.8-2.1,2.2-2.8,4.4-3.1c1.8-0.2,3.4-1.6,5.1-2.4c0.1-0.1,0.6,0.8,1,1.2c0.7-0.8,1.5-1.7,2.8-3.1v2c0.2,0,0.3,0.1,0.3,0\n  c1.1-0.7,2.3-1.4,3.4-2.1c0.7-0.4,1.4-0.9,2.1-1.4c0.5,0.8,1.3,1.7,1.3,2.5c0.2,3.3,0.2,4.2,0.2,7.5\n  C306.3,338.3,306.3,338.8,306.3,339.3L306.3,339.3z\"/>\n  </g>\n  <g>\n  <path d=\"M340.6,338.6c-0.2-1.4-1.3-3.2-2.5-3.9c-5.8-3-11.7-5.9-17.7-8.5c-3.7-1.6-6.5,0.2-6.4,4.1c0.1,2.8,0.3,6.5,0.5,9.4h0\n  c0.3,3.2,0.6,6.4,0.9,9.6c0.1,1.2,1.7,2.9,2.8,3.1c5.8,1.1,11.7,1.7,18.5,2.6c2.8,0,4.6-1.6,4.6-5.2\n  C341.3,346.1,341.1,342.3,340.6,338.6z\"/>\n  <path d=\"M334.9,352.6c-5.4-0.4-10.9-1.1-16.3-1.8c-1.7-0.2-1.8-1.1-1.4-2.6c0.7-2.1,1.2-4.2,1.8-6.3c-0.2-0.1-0.4-0.1-0.7-0.2\n  c-0.4,1.5-0.8,2.9-1.3,4.4c-0.3,0-0.6,0-1-0.1c0-2.1-0.1-4.2-0.1-6.2h0c-0.1-2.8-0.1-6.2-0.1-8.9c0-0.9,1-1.8,1.6-2.7\n  c0.6,0.5,1.2,1.2,1.9,1.6c1,0.5,2.2,0.9,3.5,1.4c0,0,0.1-0.5,0.2-1c2,0.9,4,1.8,5.9,2.7c0.1-0.1,0.1-0.2,0.2-0.3\n  c1.2,0.6,2.3,1.2,3.5,1.8c0,0,0.3-0.4,0.3-0.4c1.5,0.4,3,0.5,4.1,1.2c0.9,0.6,1.8,1.8,1.9,2.8c0.3,3.9,0.3,7.9,0.3,11.8\n  C339.2,352.6,336.7,352.7,334.9,352.6z\"/>\n  </g>\n  <path d=\"M338.2,356.8c-7-0.8-14-1.9-21-2.8c-2.3-0.3-4.2,0.5-4.7,3.2c-0.9,5.1-0.3,10,1.4,14.9c0,0,0,0-0.1,0\n  c0.4,0.4,0.8,1.1,1.1,1.1c1.8,0.1,3.7,0.2,5.4-0.2c4.5-1.1,9-2.1,13.7-1.1c3.5,0.7,6-1.3,6.6-5c0.4-2.2,0.5-4.4,0.6-6.6\n  C341.4,358.4,340.7,357.1,338.2,356.8z\"/>\n  <path d=\"M308.6,371.4C308.6,371.3,308.6,371.3,308.6,371.4c-1.2,3.8-4.6,2.2-6.6,1.8c-4.9-1-9.6-2.2-14.6-1.1c-1.2,0.3-2.8-0.4-4-1\n  c-2.8-1.5-4.1-10.9-2.1-12.9c1.2-1.1,3.2-1.5,4.9-1.8c6-0.9,12-1.5,18-2.3c3.4-0.4,5.5,1.1,5.4,4.4\n  C309.4,362.8,308.9,367.1,308.6,371.4c-0.3-1.3-0.7-2.7-0.8-4.1c-0.2-3.4-0.3-6.8-0.4-10.8c-1.1,0.8-1.5,1.1-1.9,1.5\n  c-0.1-0.8-0.7-1.2-0.8-1.9c-1.9,0.7-3.1,1-4.8,1.6c0,0-0.2-0.6-0.4-1.6c-1,0.9-1.7,1.5-2.4,2.2c-2-3.2-2.6-0.2-3.7,0.9\n  c-0.9-3.2-2-1-3-0.2c-1.9-3.8-2.5,0.3-3.8,0.8c0.1-0.8,0.1-1.5,0.1-2.1c-0.3-0.2-0.6-0.5-0.9-0.7c-0.5,0.9-1,1.8-1.5,2.7\n  c-0.3-0.6-0.5-1.1-0.8-1.7c-2.3,4-1.5,9.4,1.6,11.5c-0.2-1.2-0.4-2.3-0.6-3.6c0.9,1.5,1.7,2.8,2.5,4c0.2-0.2,0.4-0.3,0.6-0.5\n  c-0.6-1.5-1.2-3.1-1.8-4.6c1.2,1.3,1.6,3.3,2.8,4.2c1,0.8,3,0.4,4.8,0.6c-0.3-1.6-0.5-2.9-0.7-4.2c0.8,0.9,1.1,2,1.5,3\n  c0.8,1.9,2,1.8,2.9,0.2c0.8,0.7,1.6,1.4,2.9,2.5c-0.3-2.2-0.5-3.6-0.8-5C300.8,371.5,304,373.4,308.6,371.4z\"/>\n  <path d=\"M363,502.8c1.8,13.3-2.6,25.1-8.6,36.6C358.6,527.6,363.8,515.9,363,502.8z\"/>\n  <path d=\"M258.8,502.7c-1,13.2,4.3,24.8,8.5,36.7C261.3,527.9,256.8,516,258.8,502.7z\"/>\n  <path d=\"M300.5,198c5.3-2.7,14.6-2.8,20,0C314.1,198,307.7,198,300.5,198z\"/>\n  <path d=\"M316.8,189.9c-0.2-0.7-0.3-1.5-0.5-2.2c0.2-0.1,0.4-0.2,0.6-0.2c0.5,1.1,1,2.2,1.9,4.2c-5.3,0-9.8,0-15.1,0\n  c0.5-1.8,1-3.1,1.4-4.4c0.2,0.1,0.5,0.1,0.7,0.2c-0.2,0.8-0.3,1.7-0.5,2.5c0.3,0.1,0.6,0.3,0.9,0.4c0.8-1.9,1.6-3.8,2.5-5.9\n  c1.3,2.2-2.8,6.8,2.4,6.6c5-0.2,0.6-4.5,2.1-6.9c0.9,2.2,1.7,4.3,2.6,6.3C316.1,190.3,316.5,190.1,316.8,189.9z\"/>\n  <path class=\"st0\" (click)=\"onClickPath($event)\" d=\"M342.8,616.8c-0.2-1.1-0.3-1.9-0.4-2.6c-0.5,3.4-1,6.8-1.5,10.2c-0.4-0.1-0.8-0.1-1.2-0.2\n  c0.1-2.3,0.3-4.5,0.5-7.6c-2.4,5,0.2,10.3-3.1,14.7c-0.2-4.9-0.5-9.4-0.7-13.9c-0.1,0-0.2,0-0.2,0v19.5c-3.5-5.5-3.4-11.4-3.9-17.2\n  v7.3c-0.4,0-0.8,0.1-1.2,0.1c-0.3-3-0.7-6.1-1.1-9.4c-0.1,1.4-0.2,2.6-0.3,4.6c-2.4-3.9-1.9-7.4-1.6-10.8c0.6-7.6,1.5-15.1,2.3-22.6\n  c0.1-0.5,0.2-0.9,0.3-1.6c3.8,6.4,7.6,12.3,10.9,18.4C344,610.3,344.2,613.7,342.8,616.8z M293.5,613c-0.3-4.6-1.1-9.2-1.6-13.8\n  c-0.4-3.9-0.6-7.7-1-11.9c-2.6,4.2-5.2,8-7.5,12c-1.5,2.4-3,4.8-4,7.4c-1.4,3.5-2.6,7.2-0.5,11.7c0.1-2,0.2-3.4,0.3-4.7\n  c0.5,3.8,0.1,7.8,2.7,11.1v-6.4c1.3,4.5-0.2,9.5,3.2,13.7v-14.8c0.1,0,0.3,0,0.4,0V636c3.7-5,3.3-10.6,3.3-16.2c0.3,0,0.6,0,0.9,0.1\n  v6.8c0.4,0,0.8,0,1.1,0c0.2-3,0.5-6.1,0.7-9.1c0.1,0,0.1,0,0.2,0c0.1,1.1,0.2,2.2,0.3,3.3c0.3,0,0.7,0,1,0\n  C293.2,618.2,293.6,615.6,293.5,613z M256.6,625.4c0.6,2.6,1.3,5.5,1.9,8.5c0.3-0.1,0.6-0.1,1-0.2v-0.1c-0.2-2.4-0.4-4.7-0.6-7.1h0\n  c0.1,0,0.2,0,0.3,0c1,5.2,2.1,10.4,3.1,15.6c0.3,0,0.7-0.1,1-0.1c0.3-0.4,0.5-0.9,0.5-0.9c0.6,2.8,1.4,6,2.1,9.1\n  c0.2-0.1,0.4-0.1,0.6-0.2v-9.2c0,0,0,0.1,0,0.1c1.8,10.9,3.5,21.8,5.3,32.7c0.4-0.1,0.8-0.1,1.2-0.2c0,0,0-0.1,0-0.1\n  c-0.2-2-0.5-3.9-0.7-5.9c-0.1-2.1-0.1-4.3-0.2-6.4c-0.5-7.9-0.9-15.8-1.7-23.7c-1.3-13.2-2.8-26.4-4.2-39.6\n  c-1.8-7.7-4.4-18.1-5.3-21.3c-0.3,1.6-0.6,3-0.8,4.4c0,0,0,0,0,0v0l-0.7,6.4v0l-0.2,1.7v0.1c-1.8,5.9-4.2,11.6-5.8,17.5\n  c-0.2,0.8-0.4,1.6-0.5,2.4c-0.1,0.4-0.1,0.8-0.1,1.1c0,0.3-0.1,0.7-0.1,1c-0.4,5.2,1,10.4,3.3,15.9\n  C256.4,626,256.6,625.5,256.6,625.4z M367.5,604c-2-5.8-3.9-11.6-5.7-17.5c-0.6-1.9-0.6-3.9-0.9-5.8c-0.1,0-0.2,0.1-0.3,0.1v12.9\n  H360v-14.5c-0.3,0-0.7,0-1,0c-2.1,5.6-0.5,11.5-1.6,17.3c-0.1-2.2-0.2-4.5-0.4-7.4c-2.5,5.7-8.1,63.5-7.8,80.1\n  c1.6-8.9,4.4-19.3,5.9-28.1v7.9c3-7.5,7.7-14.4,6.8-23.2c0.3,0.1,0.6,0.1,0.9,0.2c-0.2,2.3-0.4,4.6-0.6,6.8l0.5,0.2\n  c1.4-2.9,3.1-5.7,4.1-8.8C369,617.6,369.9,610.9,367.5,604z\"/>\n  <path class=\"st0\" (click)=\"onClickPath($event)\" d=\"M339,597.2c-3.4-4.9-6.5-10.1-8.5-15.6c-4.5-12.3-5.3-25.3-6.1-38.2c0-0.6,0.2-1.2,0.3-1.8c0.3,0,0.5,0,0.8,0\n  c1.4,5.7,3.2,11.4,4.1,17.2c1,6.9,3.7,13.1,7.3,18.8c2.4,3.7,5.6,6.9,8.4,10.3c0.3-0.2,0.5-0.4,0.8-0.7c-1.4-2-2.8-4-4.6-6.5\n  c2.2,0.5,3.8,0.9,5.3,1.2c3.6,0.6,8.2-1.9,8.4-5.3c0.3-3.5,2.5-4.9,5.5-6.8c-0.6,1.8-1,3-1.4,4.2c-3.8,10.7-7,21.5-8,32.8\n  c-1.5,15.9-2.2,31.8-3.2,47.7c-0.3,4.6-0.7,9.1-1,13.7c0,0-1,11.6-1.5,18.7c-1,1.8-5.1,2.3-6.8,2.4c-0.9-1.4-1.6-2.7-2-4\n  c-0.7-2-1.4-4.4-0.8-6.3c0.5-1.6,1-3.3,1.5-4.9c0,0,2.8-10.4,3.7-15.7c2.1-12.8,4.1-25.5,4.8-38.5\n  C346.3,611.3,343.8,604.2,339,597.2z\"/>\n  <path class=\"st0\" (click)=\"onClickPath($event)\" d=\"M383.1,725.2c-0.8,0.3-1.8,0.6-2.8,0.9c-0.6-5.7-1.3-6.5-6.8-7.6c-3.6-0.7-6.7-2.3-8.9-5.3\n  c-1.3-1.7-2.7-3.4-4.1-5.1c-0.4,0.4-0.7,0.7-1.1,1.1c2.3,4.4,4.8,8.7,9.9,10.6c-1.6,3.2-1,4.3,2.4,4.4c2.1,0,4.2-0.2,6.3-0.3\n  c-1.6,3.2-8,4-13.2,1.5c-0.8,1.8-3,2-4.8,0.5c-1.3-1.1-2.7-2.2-4.2-2.8c-3.1-1.4-4.4-4-5.7-6.8c-2.2-4.6-4.7-9-10.5-10.4\n  c0.7-1.6,1.3-3,2-4.5c-3.5,1-5.2,3.8-3.7,8.2c0.4-0.8,0.8-1.7,1.3-2.7c3.7,1.5,5.7,4.6,7.2,8.2c-3.7-2.4-9.7,0.2-12.1-6.5\n  c0.1,1.6,0.2,2.3,0.2,3.4c-2.3-1.2-1.5-2.7-1-4.2c1.3-3.8,2.8-7.5,3.8-11.4c0.5-1.9,0.3-4,0.4-6.2c0.5,0.3,1.1,0.7,2.4,1.6l6.1-3.1\n  c0.2-0.9,0.4-1.7,0.6-2.6c0,0,0,0,0,0c0.3-1.2,0.5-2.4,0.7-3.6c0.2-1.5,0.7-2.1,2.4-2.5c2.9-0.7,5.6-2.1,8.4-3.2\n  c0.5,1.6-0.6,7.6-2.7,9.5c-2.8,2.6-6.1,4.8-8.7,6.8c3,0.1,7.2-2.6,12.7-8.9c1.3,1.8,0.8,3.4,0.1,5.3c-0.4,1.2,0.2,2.8,0.3,4.9\n  c-2.6-1.1-4.3-1.8-5.4-2.3c2.7,4.5,5.6,9.3,8.5,14.1c0.1,0,0.2,0,0.2,0c-1.4-3.2-2.7-6.3-4.1-9.4c0.1-0.1,0.2-0.3,0.2-0.4\n  c1.6,1.6,3.3,3.1,4.7,4.9c2,2.7,3.7,5.9,5.8,8.5c1.5,2,3.6,3.5,5.4,5.3c1.8,1.8,3.5,3.7,5.1,5.7C381.3,721.9,382,723.4,383.1,725.2z\n  \"/>\n  <path class=\"st0\" (click)=\"onClickPath($event)\" d=\"M370.9,722.5c1.9,1.8,4,0.5,6.1-0.1c0-0.3,0.1-0.6,0.1-1H371C371,721.8,370.9,722.2,370.9,722.5z\"/>\n  <path class=\"st0\" (click)=\"onClickPath($event)\" d=\"M386.1,719.1c1,1.9,1.9,3.8,4.5,3.8c0.2-0.3,0.4-0.5,0.6-0.8c-1.5-1.3-2.9-2.6-4.4-3.9\n  C386.6,718.5,386.3,718.8,386.1,719.1z\"/>\n  <path class=\"st0\" (click)=\"onClickPath($event)\" d=\"M380.9,717.5c1.7,2.7,3.5,5.4,5.6,8.8c0.1,0,0.1,0,0.1,0c0.5-1.2,1.2-2.3,1-2.6c-1.5-2.1-3.1-4.1-4.8-6\n  C382.5,717.3,382,717.2,380.9,717.5z\"/>\n  <path d=\"M349.7,540.8c0.8-9.3,1.7-18.6,2.5-27.1C354.5,522.4,351.4,531.6,349.7,540.8z\"/>\n  <path d=\"M269.6,513.7c0.8,8.5,1.6,17.9,2.4,27.2C270.2,531.6,267.3,522.4,269.6,513.7z\"/>\n  <path d=\"M262.2,547.8c-5.1-8.8-8.2-17.8-9.2-26.5C255.8,529.4,258.9,538.4,262.2,547.8z\"/>\n  <path d=\"M347.5,540.3c-0.5-9.2-1-18.3-1.5-27.5C348.6,521.9,348,531.1,347.5,540.3z\"/>\n  <path d=\"M275.7,513.1c-0.5,9.1-1,18.1-1.5,27.2C273.7,531.2,273.2,522,275.7,513.1z\"/>\n  <path d=\"M356.8,512.9c1.4,8.5-1.7,17.4-4.4,26.5C353.9,530.2,355.5,521.1,356.8,512.9z\"/>\n  <path d=\"M341.2,511.3c1.4,9.1,2.9,18.1,4.3,27.2c-0.3,0-0.6,0.1-0.9,0.1c-1.4-9.1-2.8-18.2-4.3-27.3\n  C340.6,511.3,340.9,511.3,341.2,511.3z\"/>\n  <path d=\"M276.2,538.6c1.4-9.1,2.8-18.3,4.3-27.4c0.3,0,0.6,0.1,0.8,0.1c-1.4,9.1-2.8,18.3-4.2,27.4\n  C276.8,538.6,276.5,538.6,276.2,538.6z\"/>\n  <path d=\"M265,513.3c1.3,7.6,2.8,16.8,4.4,26.5C266.4,530.3,263.6,521.4,265,513.3z\"/>\n  <path d=\"M369,433.8c-3-1-4.4-7.9-2-10.2C367.7,427.2,368.3,430.4,369,433.8z\"/>\n  <path d=\"M261.7,249.8c7-1.4,14-2.7,21-4.1c0,0.2,0.1,0.4,0.1,0.7c-7,1.4-13.9,2.8-20.9,4.3C261.9,250.4,261.8,250.1,261.7,249.8z\"/>\n  <path d=\"M404.8,287.5L404,288c-3.4-5.4-6.8-10.8-10.2-16.2c0.2-0.2,0.5-0.4,0.7-0.5C398,276.6,401.4,282.1,404.8,287.5z\"/>\n  <path d=\"M370.3,446.3c-3.4-1.9-2-5.2-2.9-7.8C370.3,438.6,371.1,441.1,370.3,446.3z\"/>\n  <path d=\"M347.1,423.7c-0.4,3.3-0.8,6.6-1.2,9.9c-0.6-0.1-1.1-0.1-1.7-0.2c0.4-3.3,0.9-6.6,1.3-9.9\n  C346.1,423.6,346.6,423.6,347.1,423.7z\"/>\n  <path d=\"M424.7,297.1c-3.2-2.8-1.7-6.4-3.6-9.1C424,288.6,425.2,291.7,424.7,297.1z\"/>\n  <path d=\"M423.4,309.2c-1.1-0.9-1.3-6.8,0-7.8c0.3-0.2,1.2-0.4,1.3-0.2c0.4,0.6,1,1.5,0.8,2.1C425,305.3,424.1,307.2,423.4,309.2z\"/>\n  <path d=\"M223.3,254.2c-2.7,6.4-5.2,12.3-7.8,18.7C216.7,265.9,219.2,260,223.3,254.2z\"/>\n  <path d=\"M215.5,279c2.3-6,4.6-12,6.9-18.1c0.2,0.1,0.5,0.2,0.7,0.3c-2.3,6-4.6,12-6.9,18.1C216,279.2,215.8,279.1,215.5,279z\"/>\n  <path d=\"M396.1,279.2c-0.2,0.2-0.4,0.4-0.6,0.6c-3.9-4.4-7.8-8.7-11.7-13.1c0.2-0.1,0.3-0.3,0.5-0.4\n  C388.2,270.6,392.1,274.9,396.1,279.2z\"/>\n  <path d=\"M306.6,202c0.7-0.5,1.8-1.6,2.2-1.4c1.9,1,5.2-2,6.2,1.9c-2.6,0-5.2,0-7.7,0C307,202.4,306.8,202.2,306.6,202z\"/>\n  <path d=\"M406,278.9c-0.2,0.1-0.5,0.2-0.7,0.2c-1.9-5.2-3.9-10.5-5.8-15.7c0.3-0.1,0.5-0.3,0.8-0.4\n  C402.2,268.3,404.1,273.6,406,278.9z\"/>\n  <path d=\"M406.3,266.2C406.4,266.2,406.4,266.2,406.3,266.2l0,0.1C406.3,266.2,406.3,266.2,406.3,266.2z\"/>\n  <path d=\"M406.3,266.2c-1.9-5.5-3.8-11-5.5-16.1c0.5,0.9,1.6,2.2,2.2,3.7C404.7,257.7,407,261.6,406.3,266.2z\"/>\n  <path class=\"st0\" (click)=\"onClickPath($event)\" d=\"M400.4,256.1c2.1,5.3,4.2,10.5,6.3,15.8c-0.2,0.1-0.5,0.2-0.7,0.3c-2.1-5.3-4.2-10.5-6.4-15.8\n  C399.8,256.3,400.1,256.2,400.4,256.1z\"/>\n  <path class=\"st0\" (click)=\"onClickPath($event)\" d=\"M400.3,263.1c1.9,5.3,3.8,10.6,5.7,15.8c-0.2,0.1-0.5,0.2-0.7,0.2c-1.9-5.2-3.9-10.5-5.8-15.7\n  C399.8,263.3,400,263.2,400.3,263.1z\"/>\n  <path class=\"st0\" (click)=\"onClickPath($event)\" d=\"M384.2,266.3c4,4.3,7.9,8.6,11.8,12.8c-0.2,0.2-0.4,0.4-0.6,0.6c-3.9-4.4-7.8-8.7-11.7-13.1\n  C383.9,266.6,384.1,266.5,384.2,266.3z\"/>\n  <path class=\"st0\" (click)=\"onClickPath($event)\" d=\"M393.9,271.7c0.2-0.2,0.5-0.4,0.7-0.5c3.4,5.4,6.8,10.9,10.2,16.3L404,288C400.7,282.6,397.3,277.1,393.9,271.7\n  z\"/>\n  <path class=\"st0\" (click)=\"onClickPath($event)\" d=\"M243.1,281.1C243.1,281.1,243.1,281.1,243.1,281.1c0.1,0.1,0.1,0.2,0.2,0.2c0.6-0.9,1.2-1.8,2.1-3.1\n  c-0.7,5.3-1.6,10.4-2.9,15.3c-0.5,2-1.1,3.9-1.8,5.8c-1,2.9-2.2,5.7-3.6,8.4c0,0.1-0.1,0.2-0.1,0.3c0,0.1,0,0.1-0.1,0.1\n  c-0.1,0.2-0.1,0.4-0.2,0.6c0,0.1-0.1,0.3-0.1,0.4c0,0.1-0.1,0.3-0.1,0.4c0,0,0,0.1,0,0.1c-0.3,0.5-1.2,2-1.8,3.2\n  c-5.6,8.7-14.2,13.3-23.3,17.6c-0.3-0.6-0.4-1.2-0.8-1.5c-2.5-2.1-3.4-4.5-3.2-7.1c0-0.4,0.1-0.7,0.1-1.1c0-0.2,0.1-0.4,0.1-0.6\n  c0.1-0.4,0.2-0.8,0.3-1.2c0.1-0.3,0.2-0.7,0.3-1c0,0,0-0.1,0-0.1c0.1-0.2,0.1-0.3,0.2-0.6c0.2-0.5,0.4-1.2,0.6-2\n  c0.1-0.2,0.1-0.4,0.2-0.5c0.1-0.2,0.1-0.5,0.2-0.7c0.5-1.7,0.9-3.6,1.2-4.8c1.5-3,5.3-9.8,6.9-13c0.3-0.2,0.5-0.3,0.7-0.5\n  c1.8-2.8,3.4-5.7,5.3-8.4c1.1-1.6,2.4-3,3.9-4.2c2.4-2,5.1-3.7,7.5-5.6c2.4-1.9,4.6-4,6.9-6c0.1,0.1,0.2,0.3,0.3,0.4c0,0,0,0,0,0\n  c-1.4,1.8-2.8,3.7-4.2,5.5c0,0,0,0,0,0.1c0.2,0.2,0.4,0.3,0.6,0.5c2.5-3.4,5.1-6.7,7.6-10.1c0.2,0.1,0.4,0.3,0.6,0.4c0,0,0,0,0,0\n  c-1.3,2.4-2.6,4.8-3.9,7.3c0,0,0,0,0,0.1c0.1,0.1,0.3,0.2,0.4,0.2c0.6-0.7,1.2-1.4,1.8-2.1c0.2,0.1,0.3,0.2,0.5,0.2h0\n  C244.8,276.1,244,278.6,243.1,281.1z M216.1,296.1C216.1,296.1,216.1,296.1,216.1,296.1c-0.2,0.1-0.3,0.2-0.4,0.4h0\n  c-1.7-0.1-4.2-13.9-5.5-21.4c0,0-0.1,0-0.1,0c0,0,0-0.1,0-0.1c0-0.1,0-0.1,0-0.2c-0.2-1.1-0.4-2.1-0.5-2.9c-0.6,1.4-1.3,2.7-1.8,4.2\n  c-0.6,1.7-0.9,3.5-1.7,5.1c0,0,0-0.1,0-0.1c0.2-1.5,0.4-3.1,0.6-4.6c-0.2-0.1-0.3-0.2-0.5-0.3c-1.9,3.2-3.8,6.3-5.7,9.5\n  c-0.1,0-0.1,0-0.2-0.1c-0.7,1.4-1.2,2.8-1.6,4.3c-0.4,1.4-0.6,2.9-0.8,4.4c-0.1,0.6-0.1,1.1-0.1,1.7c-0.3,7.5,1.8,15,3.3,19.5\n  c0,0,0,0,0,0c1.4,1.2,2.7,2.5,4.1,3.7v-3.5h0c0.1,0,0.2,0,0.3-0.1c0.1,0.5,0.3,1,0.6,2.1c0.8-1.6,1.4-2.9,2-4.1c0,0,0.1-0.1,0.1-0.3\n  c0-0.1,0.1-0.1,0.1-0.2c0,0,0,0,0-0.1c0-0.1,0.1-0.2,0.1-0.3c0,0,0,0,0-0.1c0.1-0.3,0.2-0.5,0.3-0.9c0.1-0.4,0.2-0.7,0.3-1.1\n  c0.1-0.4,0.3-0.8,0.4-1.2c0.1-0.2,0.2-0.5,0.3-0.7c0,0,0,0,0,0c0,0,0,0-0.1,0c0,0,0-0.1,0-0.1c0.2-0.5,0.4-1.1,0.5-1.7\n  c0.1-0.3,0.2-0.6,0.3-0.9c0.3-0.9,0.7-1.8,1-2.6c0,0,0,0,0-0.1c0.2-0.4,0.3-0.7,0.5-1.1c0.1-0.3,0.3-0.6,0.4-0.9\n  c0.1-0.3,0.3-0.6,0.5-0.9c0.9-1.8,1.9-3.3,2.8-4.2C215.9,296.4,216,296.2,216.1,296.1z M301.2,238.1c-0.7-1-3.3-1.9-3.9-1.4\n  c-2.2,1.8-4.6,0.8-6.9,1.1c-0.6,0.1-1.1,0.5-2.3,1h-16.2c0.1,0,0.2-0.1,0.2-0.1c0.8-0.3,1.3-0.4,1.8-0.6c0,0,0,0,0-0.1\n  c0-0.3,0-0.5-0.1-0.8c-7.3-0.4-14.4,1.3-21.7,1.2c0.2,0,0.4-0.1,0.6-0.1c4.3-0.5,8.7-1,13-1.6c-0.1,0-0.1,0-0.2-0.1\n  c-8.4-2-16.7-6-25.6-0.8c0-0.4,0-0.8-0.1-1.2h0c1-0.3,2-0.5,3.1-0.8c1-0.3,2.1-0.6,3.3-0.9c0,0-0.1,0-0.1-0.1\n  c-4.8-2.2-15.2,0-21.2,5c-3.4,2.9-5.9,6.1-7.6,9.7c0,0,0,0,0,0c-0.1,0.3-0.3,0.6-0.4,0.9c-0.1,0.3-0.2,0.5-0.3,0.8\n  c-0.3,0.6-0.5,1.3-0.7,1.9c0,0,0,0,0,0c-0.2,0.7-0.5,1.4-0.7,2.2c-0.8,2.9-1.2,6-1.5,9.1c-0.1,1.8-0.2,3.7-0.2,5.5\n  c1.9-5.7,3.8-11.3,5.8-17c0.1,0.1,0.3,0.1,0.4,0.2h0c-1,3.5-2,7-3,10.5c-0.3,1.2-0.7,2.4-1,3.6c-1,3.5-2,7.1-3.1,10.7\n  c-0.1,0-0.1-0.1-0.2-0.1c0.6,6.1,1.7,14.1,3.4,19.4c-0.1,0.1-0.1,0.2-0.2,0.3c0,0,0,0,0,0c0-0.1,0.1-0.2,0.1-0.2\n  c3.8-6,7.5-12,11.2-18.1c0.2,0.1,0.4,0.2,0.6,0.3c0,0,0,0,0,0c-0.8,1.3-1.6,2.7-2.4,4c0,0-0.1,0.1-0.1,0.1c9-6.9,18-13.7,24.1-23.5\n  c6.6-10.6,17.1-14.5,28.8-16.4c6-0.9,12-1.6,18.1,0c0.5,0.1,1,0.3,1.5,0.3c1.2,0.1,2.5,0.1,3.7,0.2v-0.1\n  C301.4,241,301.9,239.1,301.2,238.1z M407.9,272.7l-0.6,4.2l0,0.1c0-0.1-0.1-0.1-0.1-0.1c-0.1-0.2-0.3-0.3-0.4-0.5l0,0v19.8\n  c-0.2,0.1-0.4,0.1-0.6,0.2c-3-4.9-5.9-9.7-9-14.8c-0.1,0.8-0.2,1.2-0.3,1.7c0,0,0,0.1,0,0.1c-2.9-2.6-5.6-5.2-8.6-7.4\n  c-6.4-4.7-11.3-10.7-16-17c-2.8-3.8-5.9-7.8-9.8-10.1c-13-7.6-27.1-9.9-42.1-5.5c-0.8-0.7-1.6-1.5-1.5-2.5c0-0.2,0-0.5,0.1-0.7\n  c0.1-0.3,0.2-0.6,0.5-0.9c1.2-1.8,2.7-3.8,4.8-2.4c2.5,1.6,4.9,0.9,7.4,1.2c0.7,0.1,1.2,0.8,1.7,1.1c12-1.3,24-2.5,36.1,0.3v-0.1\n  c0-0.3,0-0.5,0-0.8c-4.6-0.5-9.2-1.1-13.8-1.6c0,0,0-0.1,0.1-0.1c0.5-0.6,0.9-0.7,1.3-0.8c2.4-0.3,5.2,0.1,7.1-1.1\n  c3.7-2.4,7.3-1.7,11.1-1.1c2.2,0.4,4.4,1.1,6.5,1.6c0,0,0-0.1,0-0.1c0.1-0.2,0.1-0.4,0.1-0.5c-2.1-0.6-4.3-1.2-7-2\n  c0.1,0,0.1,0,0.2,0c19.4-3.1,33.5,12.5,32.7,33.4v0.1h-1.3c0,0,0,0,0,0c0,0,0-0.1,0-0.1c0.7-4.6-1.6-8.5-3.3-12.4\n  c-0.6-1.5-1.6-2.8-2.2-3.7c1.7,5.1,3.6,10.6,5.5,16.1c0,0,0,0.1,0,0.1l0,0h0.1C406.9,268.4,407.4,270.5,407.9,272.7z M422.4,302.6\n  c-0.2,2.6-0.7,4.9-1.3,7c-1,3.8-2.5,6.7-3.5,8.4c-0.4,0.2-0.9,0.4-1.3,0.6v-5.9h-0.6v8.3c-0.4-1.3-0.7-2.4-1-3.5v-0.1\n  c-0.1-4.2-5.2-14.3-5.2-14.3c-0.2-0.3-0.5-0.6-0.7-0.8c0.6,1.5,1.5,2.9,2.1,4.4c0.3,0.6,0.3,1.3,0.7,2.6c0,0,0-0.1-0.1-0.1\n  c-0.3-1.2-0.4-1.9-0.6-2.5c-0.6-1.5-1.6-3-2.2-4.5c-0.4-0.9-0.6-1.8-0.7-2.6c-0.3-3.9-0.6-7.8-0.4-11.7c0.1-1.6,0.3-3.2,0.6-4.7\n  c0.1-0.7,0.2-1.3,0.3-1.8c0-0.5,0-1-0.1-1.5c0-0.1,0-0.3-0.1-0.4c0-0.1-0.1-0.3-0.1-0.4c0.1-1.6,0.3-3.9,0.4-5.5h0\n  c0.6-0.3,1-0.5,1.5-0.6c0.1-0.1,0.3-0.1,0.4-0.1c2.2-0.6,2.9,0.4,3.5,3.8c0.4,2.2,0.9,4.3,1.4,6.5c0.2-0.2,0.3-0.4,0.3-0.6\n  c0.1-0.2,0.1-0.4,0.1-0.6c0-1.1-0.7-2.4-0.6-3.4c0-0.1,0-0.2,0.1-0.3c0.1-0.3,0.3-0.6,0.7-0.9C421.8,286.8,423,295.6,422.4,302.6z\n  M413,316.7c0.6,1.9,0.9,3.7,0.8,5.4c0,0.5-0.1,1.1-0.2,1.6c-0.5,2.3-1.8,4.3-4.4,6c-21-9.9-32.2-26.9-32.8-56.6c0,0,0,0,0,0\n  c0.8,0.9,1.3,1.6,1.9,2.3c0.1-0.1,0.2-0.1,0.4-0.2c0,0,0,0,0-0.1c-1.3-2.5-2.6-5.1-3.9-7.6c0,0,0,0,0,0c0.2-0.1,0.3-0.2,0.5-0.3\n  c1.6,2.1,3.2,4.3,5,6.7c0,0,0-0.1,0-0.1c0.1-0.8,0.1-1.2,0.3-2.4c6,5.9,12.3,11.1,17.3,17.2c3.8,4.7,7,9.9,10.4,15.3l4.6,12.4\n  c0,0,0,0,0,0C412.9,316.6,413,316.6,413,316.7C413,316.7,413,316.7,413,316.7z\"/>\n  <path d=\"M406.6,271.8c-0.2,0.1-0.5,0.2-0.7,0.3c-2.1-5.3-4.2-10.5-6.4-15.8c0.3-0.1,0.5-0.2,0.8-0.3\n  C402.4,261.3,404.5,266.6,406.6,271.8z\"/>\n  <path d=\"M302.6,249.8c-5.6,0.4-11.2,0.8-15.5,1.1C291.2,249.9,296.5,246.7,302.6,249.8C302.5,249.8,302.6,249.8,302.6,249.8z\"/>\n  <path d=\"M331.8,445.6c0.1-2.4,0.3-4.8,0.4-7.2C334.5,438.6,334.7,441.9,331.8,445.6z\"/>\n  <path d=\"M229,270c-2.5,3.8-4.9,7.5-7.4,11.3c-0.1,0.2-0.5,0.3-1.1,0.1c2.6-3.9,5.3-7.8,7.9-11.8C228.6,269.7,228.8,269.9,229,270z\"\n  />\n  <path d=\"M352.8,449.2c-0.7-3.7-1.5-7.4-2.2-11.1c0.3-0.1,0.6-0.1,0.9-0.2c0.7,3.8,1.3,7.5,2,11.3\n  C353.3,449.2,353.1,449.2,352.8,449.2z\"/>\n  <path d=\"M288.4,546c-2,2.8-3.9,5.6-5.9,8.3c-0.3-0.2-0.6-0.4-0.8-0.6c2-2.8,4.1-5.5,6.1-8.3C288,545.6,288.2,545.8,288.4,546z\"/>\n  <path d=\"M346.2,445.9c0.6,3.3,1.3,6.7,1.9,10c-0.3,0.1-0.6,0.1-0.9,0.2c-0.6-3.3-1.3-6.7-1.9-10C345.6,446,345.9,445.9,346.2,445.9z\n  \"/>\n  <path d=\"M410.3,347.2c-0.9,4.4-1.7,8.9-2.6,13.3c-0.3-0.1-0.5-0.1-0.8-0.2c0.9-4.4,1.8-8.9,2.7-13.3\n  C409.8,347.1,410,347.2,410.3,347.2z\"/>\n  <path d=\"M275.6,445c0.7,3.3-0.1,8.2-2.1,10.8C274.3,452.2,274.9,448.6,275.6,445C275.6,445,275.6,445,275.6,445z\"/>\n  <path d=\"M255.6,235.6c-4.1,0.7-8.2,1.4-12.3,2.1c0-0.2-0.1-0.5-0.1-0.7c4.1-0.7,8.2-1.4,12.3-2.1\n  C255.5,235.1,255.6,235.3,255.6,235.6z\"/>\n  <path d=\"M214,360.2c-0.8-4.3-1.7-8.6-2.5-13c0.2,0,0.4-0.1,0.6-0.1c0.9,4.3,1.8,8.7,2.7,13C214.5,360.1,214.2,360.2,214,360.2z\"/>\n  <path d=\"M271.4,252.1c-4.2,0.7-8.4,1.4-12.6,2c0-0.2-0.1-0.5-0.1-0.7c4.2-0.6,8.4-1.3,12.6-1.9C271.3,251.7,271.4,251.9,271.4,252.1\n  z\"/>\n  <path d=\"M366.5,235c4.1,0.7,8.1,1.3,12.2,2c0,0.2-0.1,0.5-0.1,0.7c-4-0.7-8.1-1.4-12.1-2C366.4,235.4,366.5,235.2,366.5,235z\"/>\n  <path d=\"M217.7,281.1c1.7-3.7,3.4-7.3,5.1-11c0.2,0.1,0.4,0.2,0.7,0.3c-1.7,3.7-3.3,7.3-5,11C218.2,281.3,218,281.2,217.7,281.1z\"/>\n  <path d=\"M355.1,445.5c-0.7-3-1.4-5.9-2-8.9c0.3-0.1,0.6-0.1,0.8-0.2c0.7,3,1.3,6,2,8.9C355.6,445.4,355.4,445.5,355.1,445.5z\"/>\n  <path d=\"M257.2,256c3.7,0.6,7.4,1.2,11.1,1.8c0,0.2-0.1,0.3-0.1,0.5c-3.7-0.5-7.4-1-11.1-1.6C257.1,256.6,257.1,256.3,257.2,256z\"/>\n  <path d=\"M359.5,547.9c3.3-9.4,6.4-18.5,9.2-26.6C367.9,530,364.4,539,359.5,547.9z\"/>\n  <path d=\"M356,290.8c0.3,2-1,2.6-2.9,2.6c-3.2,0-6.7-2.8-6.1-5.3c0.3-1.2,1.8-2.8,3.1-3C352.7,284.6,356.1,288,356,290.8z\n  M348.6,288.5c1.9,1.1,3.3,2.1,4.8,2.9c0,0,0.5-0.7,0.7-1.1c-1.4-0.8-2.8-1.7-4.2-2.5C349.9,287.7,349.6,287.9,348.6,288.5z\"/>\n  <path d=\"M276.7,283.3c1.2,0.2,2.8,1.8,3.1,3c0.6,2.5-2.9,5.3-6.1,5.3c-1.9,0-3.2-0.6-2.9-2.6C270.7,286.2,274.1,282.8,276.7,283.3z\n  M276.8,285.9c-1.4,0.8-2.8,1.7-4.2,2.5c0.2,0.4,0.7,1.1,0.7,1.1c1.5-0.9,3-1.8,4.8-2.9C277.1,286.1,276.9,285.8,276.8,285.9z\"/>\n  <path d=\"M339,245.7c7,1.4,14,2.8,21.1,4.2c-0.1,0.3-0.1,0.5-0.2,0.8c-7-1.4-14-2.9-21-4.3C338.9,246.2,338.9,245.9,339,245.7z\"/>\n  <path d=\"M320.5,249c5-1.5,10.2,0.5,16,2.5C330.6,250.6,325.3,249.8,320.5,249z\"/>\n  <path d=\"M202.2,282.4c-0.8,1-1.4,2-1.9,3.1c-0.2,0-0.3-0.1-0.5-0.1c0,0,0,0.1,0,0.1c0.2,0,0.3,0.1,0.5,0.1c0,0,0,0,0,0l0-0.1\n  C200.8,284.4,201.5,283.4,202.2,282.4L202.2,282.4z\"/>\n  <path d=\"M201.2,318.3C201.2,318.3,201.2,318.3,201.2,318.3c-0.1-0.2-0.2-0.3-0.3-0.5C201,318,201.1,318.2,201.2,318.3z\"/>\n  <path d=\"M193.1,322.2c0,0-0.3-0.4-0.6-1.2c0.2,0.3,0.4,0.6,0.6,0.9C193.1,322.1,193.1,322.2,193.1,322.2z\"/>\n  <path class=\"st0\" (click)=\"onClickPath($event)\" d=\"M423.2,429c-1,3.1-11,9.2-14.3,8.7C413.8,434.7,418.3,432,423.2,429z\"/>\n  <path class=\"st0\" (click)=\"onClickPath($event)\" d=\"M419.1,427.3c-0.2,0.9-0.2,1.9-0.6,2.2c-2.8,1.8-5.7,3.7-8.8,5.2c-1.5,0.7-3.2,0.8-4.9,1.1\n  c0-0.3-0.1-0.6-0.1-0.9c5.8-3.9,11.5-7.8,17.5-11.9C422.9,426.8,422.7,427,419.1,427.3z\"/>\n  <path class=\"st0\" (click)=\"onClickPath($event)\" d=\"M422,360.6c-1.5,11.7-3.1,23.3-4.5,34.4c-3.2-1.1-5.9-1.9-9-3c1.5-3.7,3.1-7.9,4.8-12.2\n  C412.1,379,417.2,365.5,422,360.6z\"/>\n  <path class=\"st0\" (click)=\"onClickPath($event)\" d=\"M429.3,332.5c-0.2,2.6-0.6,5.2-1.3,7.8c-0.8,2.9-1.8,5.8-3.2,8.5c-2.6,5-6.1,9.7-8.1,14.9\n  c-0.2,0.5-0.3,0.9-0.5,1.4c-0.4,1-0.7,2-1.1,3c-0.8,2.4-1.6,4.8-2.4,7.2l-3.9,4.5c0.3-0.6,0.5-1.1,0.8-1.7l0,0\n  c-0.1,0-0.2-0.1-0.4-0.2c-0.7,1.3-1.5,2.6-2.3,4.1l0,0l0,0c-0.3,0.4-0.5,0.9-0.8,1.4c-0.2-1.5-0.7-2.7-0.7-3.7c0-0.2,0-0.4,0-0.6\n  c0,0,0-0.1,0-0.1c0-0.1,0-0.2,0.1-0.3c0.5-2.1,1.1-4.2,1.7-6.4c0.4-1.6,0.9-3.2,1.4-4.7c1.5-4.7,3.4-9.2,5.9-13.5\n  c4.2-7.4,8-14.9,12-22.4c1-1.9,2-3.7,3-5.6C429.4,328.3,429.4,330.4,429.3,332.5z M224.5,343.8c-0.3-5.8-1.2-11.5-1.8-17.3\n  c-3.3,1.5-6.9,3.1-10.6,4.8c5.8,13.8,5.9,28.3,5.9,42.9c0.3,0,0.5,0,0.8,0c0.4-7.3,0.9-14.6,1.3-21.7v16.2c0.2,0,0.5,0.1,0.7,0.1\n  c0.4-2.7,0.3-5.8,1.4-8.2C224.9,355.1,224.8,349.5,224.5,343.8z M215.9,378.3c-2.7-7.9-5.4-15.8-8.8-23.4\n  c-3.4-7.4-7.7-14.4-11.6-21.6c-1.1-2.1-2.2-4.3-3.2-6.5l0,0c-0.7,8.4,0.9,16.2,4.9,23.2c4.8,8.5,9.4,17,12.3,26.4l0.1,0.1l3.7,4.3\n  c-0.4-0.8-0.7-1.6-1.1-2.4c0,0,0,0,0,0c0.1-0.1,0.1-0.1,0.2-0.1c0.9,1.5,1.8,3.1,2.8,4.9c0,0,0-0.1,0-0.1c0.2-1.3,0.7-2.5,0.8-3.5\n  C216,379.1,216,378.7,215.9,378.3z M212.4,338.4c-1.6-4.5-4.6-8.5-6.9-12.7c-1.4-2.5-2.8-4.9-4.2-7.4c-0.1-0.2-0.2-0.3-0.3-0.5\n  c-0.3-0.6-0.6-1.2-0.9-1.8c-1.9-3.8-3.1-7.3-3.8-10.4c-0.2-1.1-0.4-2.1-0.6-3c-0.1-0.6-0.1-1.1-0.2-1.6c-0.2-1.7-0.2-3.2-0.1-4.7\n  c0-0.6,0.1-1.1,0.2-1.6c-0.4,0.6-1.1,1.9-1.8,3.8c-0.1,0.3-0.2,0.6-0.3,0.9c-0.2,0.5-0.4,1.1-0.5,1.7c-0.5,1.8-0.9,4-1.1,6.4\n  c0,0.5-0.1,1-0.1,1.4c0,0.1,0,0.1,0,0.2c-0.2,3.7,0.2,8,1.5,13c0.7,1.1,1.4,2.2,2,3.2c-0.1,0.5-0.3,1-0.3,1.4c0,0.1,0,0.3,0.1,0.4\n  c4,7.8,7.5,15.9,12.3,23.2c2.7,4,3.7,8.4,5.7,12.6c1.5,3.1,2.2,6.5,3.2,9.8c0.2-0.1,0.5-0.2,0.5-0.3v-0.1\n  C216.2,360.8,216.2,349.3,212.4,338.4z M398.7,326c-2.1,14.9-3.4,29.5,2.8,43.1v-17.6c0.4,6.3,0.7,12.2,1,18c0.3,0,0.7,0.1,1,0.1\n  c0-13.2,0.4-26.4,5.7-38.9C405.6,329.1,402,327.5,398.7,326z M425.8,299.8c0.1,5.6-1.5,11.7-5.6,17.9c0,0,0.1,0.1,0.1,0.1\n  c-4.1,8.5-11.6,15.5-12.8,25.4c-1.2,9.5-2.1,19.1-3.2,28.7c0.4,0.1,0.9,0.2,1.3,0.2c0.2-0.8,0.4-1.7,0.6-2.5\n  c2.2-6.2,4.3-12.5,6.7-18.7c0.8-2,2.8-3.5,3.9-5.5c3.2-6,6.5-11.9,9.3-18c1.7-3.6,2.6-7.6,4-11.4c0.1-0.2,0.3-0.3,0.7-0.5\n  C429.8,311.3,427.7,302.7,425.8,299.8z\"/>\n  <path d=\"M416.9,276.7c-0.3,0.1-0.6,0.3-0.8,0.4c-1-1.8-2.2-3.6-3.6-5.4C412.8,272,414.6,273.8,416.9,276.7z\"/>\n  <path d=\"M213.7,393.9L213.7,393.9c-0.1-0.1-0.1-0.2-0.1-0.3L213.7,393.9z\"/>\n  <path class=\"st0\" (click)=\"onClickPath($event)\" d=\"M311.7,432.8v0.2l-0.3-0.1C311.4,432.9,311.5,432.8,311.7,432.8z\"/>\n  <path d=\"M333.8,375.1c0,0-0.2,0-0.4,0v6.3c0.2,0,0.5,0,0.7,0v-6.2C334,375.1,333.8,375.1,333.8,375.1z\"/>\n  <path d=\"M336.6,380.8c0.2-1.1,1.1-6.7-2.4-6.5v-0.1c-2.1,0.3-3.8,0.5-5.5,0.8c-2.7,0.4-5.3,1.2-8,1.3c-4.1,0.2-7.9,0.6-9.4,4.4\n  c-2.4-1.4-4.4-3.3-6.8-3.9c-4.8-1.2-9.7-1.6-14.6-2.3c-0.7-0.1-1.4,0.1-2.2,0.2v0.1c-1,0.3-1.9,1.3-2.1,3.5\n  c-0.1-0.5-0.2-1.1-0.3-1.6c-1.1,12.6,3.2,24.4,7.3,36.2c0.1-0.3,0.2-0.6,0.3-0.8c4.8,8.1,16.8,20.4,16.8,20.4s0,0,0.1,0l1.3,0.3\n  l0.3,0.1c0,0,0,0,0,0c0.1,0,0.2-0.1,0.3-0.1c2.6-0.6,12.1-4.1,18-20.7c2.4-8.7,4.8-17.5,6.7-26.3\n  C336.7,384.2,336.7,382.6,336.6,380.8L336.6,380.8z M292.4,406.8c-2.6-8-4.5-16.1-6.1-24.4c0.1-2,0.4-4.2,1.3-5.6\n  c0.2-0.3,0.4-0.6,0.7-0.9c0.4-0.4,1-0.6,1.6-0.7c4.1,1.2,8,2.3,11.7,3.4c0.2,2.1,0.3,4,0.5,6c0.2,0,0.4-0.1,0.6-0.1\n  c-0.1-2-0.2-3.9-0.4-6.3c1.3,0.4,2.2,0.7,3,1v6.3c0.2,0,0.4,0,0.7,0v-5.8c0.3,0,0.7,0,1-0.1c0.1,2.2,0.2,4.3,0.3,6.5h0.5\n  c0.1-1.2,0.2-2.5,0.2-3.7c0.3,0,0.6,0,0.9,0v27.7c0.1,0,0.2,0.1,0.3,0.1l0.5,19.8C305.8,426,293.2,408,292.4,406.8z M335.8,379.2\n  c-1.9,11.2-4.6,22-8.4,32.5c0,0,0,0.1,0,0.1c0,0,0,0,0,0c-2.5,6.2-7.8,16.9-15.7,19.2l-0.2-21.3v0c0.3-0.4,0.5-0.8,0.5-1.2\n  c0.1-5.8,0-11.6,0.1-17.4c0.1-3.2,0.6-6.3,1-9.4c0.3,0,0.5,0.1,0.8,0.1v4.5c0.2,0,0.4,0,0.6,0v-6.4c1.6-0.6,3.1-1.1,5-1.8\n  c-0.2,2.6-0.3,4.7-0.4,6.9c0.2,0,0.5,0,0.7,0c0.1-2.1,0.2-4.3,0.3-6.4c4.1-1.2,8.1-2.4,12.5-3.7c0.2,0.1,0.5,0.1,0.8,0.1\n  c0.2,0,0.4,0,0.4,0s0.1,0,0.4,0.1C334.8,375.4,336,376.3,335.8,379.2z\"/>\n  <path class=\"st0\" (click)=\"onClickPath($event)\"  d=\"M336.6,369.7c0.3-1.4,0.5-2.5,0.9-4.4c-1.1,2-1.8,3.3-2.5,4.6c-0.3-0.1-0.6-0.3-0.9-0.4\n  c0.4-1.1,0.8-2.2,1.3-3.3c-1.2,3.1-3.4,4.1-6.8,3.1c0.2-1.3,0.4-2.6,0.7-4.5c-2.2,4.8-2.2,4.8-4.3,4.8c0.4-1.2,0.8-2.4,1.2-3.6\n  c-0.1-0.1-0.2-0.1-0.3-0.2c-1,1.8-2,3.5-3.1,5.3c-0.3-0.1-0.6-0.3-0.9-0.4c0.3-1.5,0.6-2.9,1-5c-0.6,0.7-0.8,0.8-0.9,1\n  c-1.5,5.8-0.2,4.6-6.7,5.1c-0.4,0.1-0.9,0.2-1.3,0.4c0.2-4.4,0.3-8.8,0.6-13.2c0.1-1,1.4-1.8,2.4-3.1c0.5,0.6,1.2,1.3,2.1,2.3v-2.7\n  c1.2,1.2,1.8,2,2.6,2.8c0.1-0.5,0.1-1,0.2-2.1c1,0.7,1.8,1.3,2.7,2c1.7-2.7,2.6-0.4,3.7,0.9c1.1-2.7,2-1.1,3,0\n  c1.8-3.2,2.6-0.5,3.7,0.9v-2.1c0.3-0.3,0.6-0.5,1-0.8c0.5,0.9,1.1,1.7,1.6,2.6c0.1-0.4,0.3-0.9,0.5-1.5\n  C340.9,361.4,340.3,367.4,336.6,369.7z M307.8,367.3c-0.2-3.4-0.3-6.8-0.4-10.8c-1.1,0.8-1.5,1.1-1.9,1.5c-0.1-0.8-0.7-1.2-0.8-1.9\n  c-1.9,0.7-3.1,1-4.8,1.6c0,0-0.2-0.6-0.4-1.6c-1,0.9-1.7,1.5-2.4,2.2c-2-3.2-2.6-0.2-3.7,0.9c-0.9-3.2-2-1-3-0.2\n  c-1.9-3.8-2.5,0.3-3.8,0.8c0.1-0.8,0.1-1.5,0.1-2.1c-0.3-0.2-0.6-0.5-0.9-0.7c-0.5,0.9-1,1.8-1.5,2.7c-0.3-0.6-0.5-1.1-0.8-1.7\n  c-2.3,4-1.5,9.4,1.6,11.5c-0.2-1.2-0.4-2.3-0.6-3.6c0.9,1.5,1.7,2.8,2.5,4c0.2-0.2,0.4-0.3,0.6-0.5c-0.6-1.5-1.2-3.1-1.8-4.6\n  c1.2,1.3,1.6,3.3,2.8,4.2c1,0.8,3,0.4,4.8,0.6c-0.3-1.6-0.5-2.9-0.7-4.2c0.8,0.9,1.1,2,1.5,3c0.8,1.9,2,1.8,2.9,0.2\n  c0.8,0.7,1.6,1.4,2.9,2.5c-0.3-2.2-0.5-3.6-0.8-5c1.7,5.4,4.9,7.3,9.5,5.3C308.3,370,307.9,368.7,307.8,367.3z M306.3,337.7\n  c0-3.3,0-4.2-0.2-7.5c0-0.9-0.9-1.7-1.3-2.5c-0.7,0.5-1.4,1-2.1,1.4c-1.1,0.7-2.2,1.4-3.4,2.1c-0.1,0-0.2,0-0.3,0v-2\n  c-1.2,1.4-2.1,2.3-2.8,3.1c-0.3-0.4-0.9-1.2-1-1.2c-1.7,0.8-3.3,2.2-5.1,2.4c-2.2,0.3-3.7,1-4.4,3.1c0,0-0.1,0-0.2-0.1\n  c-0.1-0.5-0.3-1-0.5-1.6c-2.7,2.5-3,15-0.6,16.7c-0.1-2.2-0.3-4.5-0.4-6.7c0.1,0,0.3,0,0.4,0c0.3,2.5,0.6,4.9,1,7.4\n  c0.7,0.1,1.1,0.2,1.4,0.2c4.9-0.5,9.9-0.9,14.8-1.5c3.7-0.5,3.6-0.6,2.5-4.3c-0.5-1.5-0.8-3.1-1.2-4.7c0.2-0.1,0.3-0.2,0.5-0.3\n  c0.5,1.7,1.1,3.3,1.6,5c0.3,0,0.6-0.1,1-0.1l0.1-3.6c0-0.9,0-2.5,0.1-3.5h0C306.3,338.8,306.3,338.3,306.3,337.7z M338.8,337.8\n  c-0.1-1-1-2.2-1.9-2.8c-1.1-0.7-2.6-0.8-4.1-1.2c0,0-0.2,0.3-0.3,0.4c-1.2-0.6-2.4-1.2-3.5-1.8c-0.1,0.1-0.1,0.2-0.2,0.3\n  c-1.9-0.9-3.8-1.7-5.9-2.7c-0.1,0.5-0.2,1-0.2,1c-1.3-0.5-2.4-0.9-3.5-1.4c-0.7-0.4-1.3-1-1.9-1.6c-0.6,0.9-1.6,1.8-1.6,2.7\n  c0,2.7,0,6.2,0.1,8.9h0c0,2.1,0.1,4.2,0.1,6.2c0.3,0,0.6,0,1,0.1c0.4-1.5,0.8-2.9,1.3-4.4c0.2,0.1,0.4,0.1,0.7,0.2\n  c-0.6,2.1-1.1,4.2-1.8,6.3c-0.5,1.5-0.3,2.4,1.4,2.6c5.4,0.6,10.8,1.4,16.3,1.8c1.8,0.1,4.3,0,4.2-3\n  C339.1,345.7,339.1,341.8,338.8,337.8z M333.9,309.1v3.2c-1.1-2.7-1.1-2.7-5.9-4.4c0.2,0.9,0.3,1.7,0.5,3c-1-1.5-1.8-2.5-3-4.3\n  c-0.1,1.6-0.1,2.2-0.2,3c-0.8-1.2-1.6-2.2-2.6-3.7c-0.2,1.3-0.3,2-0.4,2.7c-0.8-1.2-1.6-2.3-2.4-3.5c-0.2,0.1-0.4,0.2-0.5,0.3\n  c0.5,1.3,0.9,2.6,1.4,3.9c-0.1,0.1-0.3,0.1-0.4,0.2c-0.9-1.6-1.8-3.1-2.7-4.6c-0.2,0.1-0.5,0.2-0.7,0.3c0.2,1,0.4,2.1,0.5,2.7\n  c-2.3,1.3-1.7,3.2-1.7,4.7c0,0.1,0,0.4,0,0.5c-0.4,6.4,2.7,9.2,11.7,11c-0.3-1.7-0.5-3.3-0.8-4.9c0.3-0.2,0.6-0.3,0.9-0.5\n  c-1.2,6.6,3.8,8,8,10.8c-0.3-2.7-0.5-5-0.8-7.3c2.7,2.1-0.6,6.7,2.7,8.5c0.3-0.1,0.6-0.2,0.9-0.4c-0.2-1.6-0.5-3.1-0.7-4.7\n  c0.6,0.4,0.6,0.8,0.8,1.3c0.2,0.5,0.5,0.9,1,1.9C339.7,321.2,340.1,314.4,333.9,309.1z M306.6,319.3c-0.1-2-0.3-4.6-0.4-6.7\n  c-0.1-1.9-0.2-3.8-0.3-5.9c-0.7,1.1-1,1.6-1.3,2.1c-0.2-0.1-0.3-0.1-0.5-0.2c0.3-1.1,0.6-2.3,0.9-3.4c-0.3-0.2-0.5-0.3-0.8-0.5\n  c-0.9,1.5-1.9,3-2.8,4.4c-0.1-0.1-0.2-0.2-0.4-0.2c0.4-1.2,0.9-2.4,1.4-3.5c-0.2-0.1-0.5-0.2-0.7-0.3c-0.8,1.2-1.5,2.3-2.3,3.5\n  c-0.1-0.7-0.3-1.4-0.5-2.5c-0.8,1-1.3,1.6-1.8,2.2c-2.7-2.2-2.4,1.5-3.9,2c0.2-0.9,0.3-1.7,0.5-2.8c-2.2,1.4-5.7,0.5-5.9,4.2\n  c-0.1-0.6-0.2-1.2-0.3-2.1c-5.1,4.9-5.3,10.9-5.2,17.7c0.8-0.9,1.2-1.3,1.8-2c-0.3,2-0.5,3.5-0.7,5.1c0.3,0.2,0.7,0.3,1,0.5\n  c0.5-0.9,1.2-1.7,1.3-2.7c0.4-2.2,0.5-4.3,0.7-6.5c0.2,0,0.4,0.1,0.6,0.1c-0.3,2.5-0.6,4.9-0.8,7c5.1-2.3,9.7-4.7,14.6-6.6\n  c2.5-1,4.5-1.6,3.9-4.8c-0.1-0.6,0.5-1.4,0.8-2c0.1,0.1,0.2,0.1,0.3,0.2c-0.2,1.2-0.4,2.4-0.6,3.7\n  C305.8,319.3,306.2,319.3,306.6,319.3z M334.2,375.2c-0.2-0.1-0.4-0.1-0.4-0.1s-0.2,0-0.4,0c-0.3,0-0.6-0.1-0.8-0.1\n  c-4.4,1.3-8.5,2.5-12.5,3.7c-0.1,2.1-0.2,4.2-0.3,6.4c-0.2,0-0.5,0-0.7,0c0.1-2.1,0.2-4.3,0.4-6.9c-1.9,0.7-3.4,1.2-5,1.8v6.4\n  c-0.2,0-0.4,0-0.6,0v-4.5c-0.3,0-0.5-0.1-0.8-0.1c-0.3,3.1-0.9,6.3-1,9.4c-0.2,5.8,0,11.6-0.1,17.4c0,0.4-0.2,0.8-0.5,1.2v0\n  l0.2,21.3c8-2.4,13.2-13.1,15.7-19.2c0,0,0,0,0,0c0,0,0-0.1,0-0.1c3.8-10.5,6.5-21.3,8.4-32.5C336,376.3,334.8,375.4,334.2,375.2z\n  M309.1,410.3c-0.1,0-0.2-0.1-0.3-0.1v-27.7c-0.3,0-0.6,0-0.9,0c-0.1,1.2-0.2,2.5-0.2,3.7h-0.5c-0.1-2.2-0.2-4.3-0.3-6.5\n  c-0.3,0-0.7,0-1,0.1v5.8c-0.2,0-0.4,0-0.7,0v-6.3c-0.8-0.3-1.7-0.5-3-1c0.2,2.4,0.3,4.4,0.4,6.3c-0.2,0-0.4,0-0.6,0.1\n  c-0.2-1.9-0.3-3.9-0.5-6c-3.7-1.1-7.6-2.2-11.7-3.4c-0.6,0.1-1.1,0.4-1.6,0.7c-0.3,0.2-0.5,0.5-0.7,0.9c-0.9,1.4-1.2,3.6-1.3,5.6\n  c1.6,8.3,3.5,16.4,6.1,24.4c0.8,1.2,13.5,19.2,17.2,23.3L309.1,410.3z\"/>\n  <path d=\"M288.3,376v5.4c-0.2,0-0.5,0-0.7,0v-4.5C287.8,376.5,288,376.2,288.3,376z\"/>\n  <path class=\"st0\" (click)=\"onClickPath($event)\" d=\"M311.3,432.9c-0.2,0.1-0.4,0.1-0.4,0.1s0,0,0-0.2L311.3,432.9z\"/>\n  <path d=\"M362.9,451.9c-0.5-1.2-0.8-2.4-1-3.7C362.3,449.5,362.6,450.8,362.9,451.9z\"/>\n  <path d=\"M257.7,563.3c-0.1-0.1-0.1-0.1-0.1-0.1l0.1-0.4C257.7,562.9,257.7,563.1,257.7,563.3z\"/>\n  <path d=\"M303.8,135.4c-1.6,0.3-3.2,0.6-4.8,0.9C300.7,135.9,302.3,135.6,303.8,135.4z\"/>\n  <path d=\"M336.8,169.4c0,0.2-0.1,0.3-0.3,0.5C336.6,169.7,336.7,169.6,336.8,169.4z\"/>\n  <path d=\"M281.4,305.1C281.4,305.1,281.4,305.1,281.4,305.1c-1.1,2-2.3,4-3.4,6c0,0,0,0,0,0C279.2,309.1,280.3,307.1,281.4,305.1z\n  M277,316.7c-0.3,0.2-0.6,0.5-0.9,0.7c0,0,0,0,0.1,0C276.4,317.2,276.7,316.9,277,316.7l1.6-5.1h0L277,316.7z M276.4,305.2\n  c-1.6,0.7-3.2,1.4-4.8,2.1c0,0,0,0,0,0.1c1.7-0.7,3.3-1.4,4.9-2.2C276.5,305.2,276.5,305.2,276.4,305.2z M260.3,326.9\n  c-0.3-0.1-0.6-0.2-0.9-0.3v-26c0,0-0.1-0.1-0.1-0.1c0,0.1,0,0.2,0.1,0.2v26C259.7,326.8,260,326.9,260.3,326.9\n  c1.8-1.5,3.6-3,5.5-4.7c0,0,0,0,0,0C263.9,323.8,262.1,325.3,260.3,326.9z\"/>\n  <path d=\"M262.3,318.4c-0.1-0.1-0.2-0.1-0.2-0.1s0,0,0-0.1C262.2,318.3,262.3,318.4,262.3,318.4z\"/>\n  <path d=\"M411.7,309.3c-1.2-1.8-2.3-3.5-3.4-5.3l-0.9-2.4c0,0,0.5-0.3,1.4,0.5c0.6,1.5,1.6,3,2.2,4.5\n  C411.3,307.3,411.3,308,411.7,309.3z\"/>\n  <path d=\"M411,306.6c0.3,0.6,0.3,1.3,0.7,2.6c0,0,0-0.1-0.1-0.1C411.3,308,411.3,307.3,411,306.6c-0.6-1.4-1.6-2.9-2.2-4.4\n  c0,0,0.1,0.1,0.1,0.1C409.5,303.7,410.4,305.2,411,306.6z\"/>\n  <path d=\"M413.1,316.9c0-0.1-0.1-0.2-0.1-0.2c0,0,0,0,0,0c0-0.1,0-0.1-0.1-0.2c0,0,0,0,0,0c-0.1-0.4-0.3-0.9-0.4-1.3h0\n  C412.7,315.8,412.9,316.4,413.1,316.9z\"/>\n  <path d=\"M210.6,271.8C210.6,271.9,210.6,271.9,210.6,271.8c-0.3,0-0.5-0.1-0.8-0.2c0,0.1-0.1,0.2-0.1,0.3c0,0,0,0,0,0\n  c0.1-0.1,0.1-0.2,0.2-0.3C210.1,271.6,210.3,271.7,210.6,271.8z\"/>\n  <path d=\"M216.1,296.1c-0.1,0.2-0.2,0.3-0.4,0.4c0,0,0,0-0.1,0C215.8,296.4,215.9,296.2,216.1,296.1c-0.2-0.1-0.3-0.3-0.4-0.4\n  c0,0,0,0,0-0.1c0,0,0,0,0,0c0.1-0.1,0.1-0.2,0.2-0.3c0,0,0,0,0,0c0.1,0.2,0.1,0.4,0.2,0.5C216.1,296,216.1,296.1,216.1,296.1\n  C216.1,296.1,216.1,296.1,216.1,296.1z\"/>\n  <path d=\"M274.4,687.3c0.3,0,0.5-0.1,0.8-0.1v0.1C274.9,687.3,274.6,687.3,274.4,687.3L274.4,687.3z\"/>\n  <path d=\"M282.1,687.5c-0.2,0.3-0.4,0.7-0.6,1.1C281.7,688.2,281.9,687.9,282.1,687.5C282.1,687.5,282.1,687.5,282.1,687.5z\"/>\n  <path d=\"M346.9,686.3l-0.6,2.4c0.2-0.9,0.4-1.7,0.6-2.6C346.9,686.2,346.9,686.3,346.9,686.3z\"/>\n  <g>\n  <path class=\"st1\" d=\"M293.6,174c0.5,0,0.9-0.2,1.3-0.5c0.5-0.4,1.4-1,2.6-1.7c2.1-1.3,4.7,0,7,1.6c1.2,1.6,0.9,1.9-0.1,2.2\n  c-1,0.3-2.1,1-2.1,1s2.5,2.3,2.6,3.5l-0.2,0.3c0,0-2.8-2.9-3.5-3.2c-0.7-0.3-1.8-0.7-2.6-0.7c-0.8,0,1.1-0.6,2.1-0.6\n  s2.4-0.9,2.4-0.9s-1.4-1.4-2.6-1.4c-1.2,0,0.3,1.1,0.1,1.5c-0.2,0.4-0.6,0.7-0.6,0.7s-1.8,0.9-3.4,0.2c-1.6-0.7,0.3-1.3,0.8-1.2\n  c0.5,0.1,1.9-0.1,1.4-0.6c-0.4-0.4-1.1-0.9-1.6-0.4c-0.5,0.5-1.4,1.5-1.4,1.5s-2.1-0.4-3.1-1.4L293.6,174z\"/>\n  <path class=\"st1\" d=\"M306.3,174.7c0,0-1-4.3-4.9-4.1c-0.7-0.2,1.8-0.3,2.9,0.1S307.1,172,306.3,174.7z\"/>\n  <path class=\"st1\" d=\"M304.8,169.1c0,0-1.3-1.1-2.9-0.9c-1.7,0.2-3.2,1.2-4.9,1.3c-1.8,0.2-3.7,0.6-4.8-0.1c0,0,1.8-0.7,5.1-2.7\n  c3.3-1.9,7.3-0.4,8.5,1.7C305.6,169.3,304.8,169.1,304.8,169.1z\"/>\n  <path class=\"st1\" d=\"M308.1,176.8c0,0-1.2,5.4-2.2,6.1c0.3,0.5,1.3,0.4,1.6-0.2C307.9,182.2,308.3,177.2,308.1,176.8z\"/>\n  </g>\n  <g>\n  <path class=\"st1\" d=\"M328.4,173.9c-0.8,0.1-1.5-0.3-2.2-0.8c-0.6-0.3-1.4-0.8-2.3-1.4c-2.1-1.3-4.7,0-7,1.6\n  c-1.2,1.6-0.9,1.9,0.1,2.2c1,0.3,2.1,1,2.1,1s-2.5,2.3-2.6,3.5l0.2,0.3c0,0,2.8-2.9,3.5-3.2c0.7-0.3,1.8-0.7,2.6-0.7\n  c0.8,0-1.1-0.6-2.1-0.6s-2.4-0.9-2.4-0.9s1.4-1.4,2.6-1.4c1.2,0-0.3,1.1-0.1,1.5c0.2,0.4,0.6,0.7,0.6,0.7s1.8,0.9,3.4,0.2\n  c1.6-0.7-0.3-1.1-0.8-1c-0.5,0.1-1.9-0.2-1.5-0.7c0.4-0.4,1.6-0.7,2.1-0.2c0.5,0.5,0.9,1.3,0.9,1.3s3.1-1,4.1-1.9L328.4,173.9z\"/>\n  <path class=\"st1\" d=\"M315.2,174.7c0,0,1-4.3,4.9-4.1c0.7-0.2-1.8-0.3-2.9,0.1S314.4,172,315.2,174.7z\"/>\n  <path class=\"st1\" d=\"M316.7,169.1c0,0,1.3-1.1,2.9-0.9c1.7,0.2,3.2,1.2,4.9,1.3c1.8,0.2,3.7,0.6,4.8-0.1c0,0-1.8-0.7-5.1-2.7\n  c-3.3-1.9-7.3-0.4-8.5,1.7C315.9,169.3,316.7,169.1,316.7,169.1z\"/>\n  <path class=\"st1\" d=\"M313.3,176.8c0,0,1.2,5.4,2.2,6.1c-0.3,0.5-1.3,0.4-1.6-0.2C313.6,182.2,313.2,177.2,313.3,176.8z\"/>\n  </g>\n  </svg>\n    </div>\n</div>";
      /***/
    },

    /***/
    "./node_modules/raw-loader/dist/cjs.js!./src/app/components/book/book.component.html":
    /*!*******************************************************************************************!*\
      !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/components/book/book.component.html ***!
      \*******************************************************************************************/

    /*! exports provided: default */

    /***/
    function node_modulesRawLoaderDistCjsJsSrcAppComponentsBookBookComponentHtml(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony default export */


      __webpack_exports__["default"] = "<ion-grid>\n    <ion-row>\n        <ion-col size=\"6\">\n            <div class=\"product-bg\" routerLink=\"/ebook-details\">\n                <div>\n                    <img src=\"assets/images/e-book.png\">\n                </div>\n                <div class=\"product-descrption\">\n                    <h4>365 RECETTES DE CUISINE</h4>\n                    <p>\n                        19,90€\n                    </p>\n                </div>\n            </div>\n        </ion-col>\n        <ion-col size=\"6\">\n            <div class=\"product-bg\" routerLink=\"/ebook-details\">\n                <div>\n                    <img src=\"assets/images/e-book.png\">\n                </div>\n                <div class=\"product-descrption\">\n                    <h4>365 RECETTES DE CUISINE</h4>\n                    <p>\n                        19,90€\n                    </p>\n                </div>\n            </div>\n        </ion-col>\n        <ion-col size=\"6\">\n            <div class=\"product-bg\" routerLink=\"/ebook-details\">\n                <div>\n                    <img src=\"assets/images/e-book.png\">\n                </div>\n                <div class=\"product-descrption\">\n                    <h4>365 RECETTES DE CUISINE</h4>\n                    <p>\n                        19,90€\n                    </p>\n                </div>\n            </div>\n        </ion-col>\n        <ion-col size=\"6\">\n            <div class=\"product-bg\" routerLink=\"/ebook-details\">\n                <div>\n                    <img src=\"assets/images/e-book.png\">\n                </div>\n                <div class=\"product-descrption\">\n                    <h4>365 RECETTES DE CUISINE</h4>\n                    <p>\n                        19,90€\n                    </p>\n                </div>\n            </div>\n        </ion-col>\n    </ion-row>\n</ion-grid>";
      /***/
    },

    /***/
    "./node_modules/raw-loader/dist/cjs.js!./src/app/components/chart-bar/chart-bar.component.html":
    /*!*****************************************************************************************************!*\
      !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/components/chart-bar/chart-bar.component.html ***!
      \*****************************************************************************************************/

    /*! exports provided: default */

    /***/
    function node_modulesRawLoaderDistCjsJsSrcAppComponentsChartBarChartBarComponentHtml(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony default export */


      __webpack_exports__["default"] = "<div id=\"chart\">\n    <ul id=\"bars\">\n        <ion-slides [pager]=\"false\" [options]=\"slideOpts\" #mySlider>\n            <ion-slide *ngFor=\"let item of Charts\" style=\"height: 160px;\">\n                <li>\n                    <div *ngIf=\"item?.data > 0\" [ngStyle]=\"{'height' : item?.data ? (item.data+'%') : '0%'}\" style=\"background-color: #ff9800;\n                        background-image: linear-gradient( to top,  #926444, #f4b183);\" class=\"bar\">\n                        <span class=\"chartbar-velue\"> {{label}}</span>\n                    </div>\n\n                    <div *ngIf=\"item?.data <= 0\" style=\"height: 2%;background-color: red;\" class=\"bar\">\n                        <span class=\"chartbar-velue\"> {{item?.data}}%</span>\n                    </div>\n                    <span [ngClass]=\"{'active': item.isToday}\">{{item?.day}}<br>{{item?.dayName}}</span>\n                </li>\n            </ion-slide>\n        </ion-slides>\n    </ul>\n</div>";
      /***/
    },

    /***/
    "./node_modules/raw-loader/dist/cjs.js!./src/app/components/chart/chart.component.html":
    /*!*********************************************************************************************!*\
      !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/components/chart/chart.component.html ***!
      \*********************************************************************************************/

    /*! exports provided: default */

    /***/
    function node_modulesRawLoaderDistCjsJsSrcAppComponentsChartChartComponentHtml(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony default export */


      __webpack_exports__["default"] = "<ion-card>\n  <ion-card-content>\n    <canvas class=\"canvas\" #lineCanvas></canvas>\n  </ion-card-content>\n\n  <ion-card-footer>\n    <ion-row>\n      <ion-col size=\"6\">\n        <div>\n          <p class=\"p-disabled\">1 Mar 19</p>\n        </div>\n      </ion-col>\n  \n      <ion-col size=\"6\">\n        <div class=\"ion-text-right\">\n          <p class=\"p-disabled\">Aujourd'hui</p>\n        </div>\n      </ion-col>\n    </ion-row>\n  </ion-card-footer>\n\n\n</ion-card>";
      /***/
    },

    /***/
    "./node_modules/raw-loader/dist/cjs.js!./src/app/components/charts-checkup/charts-checkup.component.html":
    /*!***************************************************************************************************************!*\
      !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/components/charts-checkup/charts-checkup.component.html ***!
      \***************************************************************************************************************/

    /*! exports provided: default */

    /***/
    function node_modulesRawLoaderDistCjsJsSrcAppComponentsChartsCheckupChartsCheckupComponentHtml(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony default export */


      __webpack_exports__["default"] = "<ion-card>\n  <ion-card-header class=\"card_header\">\n    <ion-row>\n      <ion-col size=\"6\">\n        <div>\n          <p>Poids</p>\n          <h4>77,6kg</h4>\n        </div>\n      </ion-col>\n\n      <ion-col size=\"6\">\n        <div class=\"ion-text-right\">\n          <p>+ 1,5 kg</p>\n        </div>\n      </ion-col>\n    </ion-row>\n  </ion-card-header>\n  <ion-card-content>\n    <canvas class=\"canvas\" #lineCanvas></canvas>\n  </ion-card-content>\n\n  <ion-card-footer>\n    <ion-row>\n      <ion-col size=\"6\">\n        <div>\n          <p class=\"p-disabled\">1 Mar 19</p>\n        </div>\n      </ion-col>\n  \n      <ion-col size=\"6\">\n        <div class=\"ion-text-right\">\n          <p class=\"p-disabled\">Aujourd'hui</p>\n        </div>\n      </ion-col>\n    </ion-row>\n  </ion-card-footer>\n</ion-card>\n";
      /***/
    },

    /***/
    "./node_modules/raw-loader/dist/cjs.js!./src/app/components/check-ups-profile/check-ups-profile.component.html":
    /*!*********************************************************************************************************************!*\
      !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/components/check-ups-profile/check-ups-profile.component.html ***!
      \*********************************************************************************************************************/

    /*! exports provided: default */

    /***/
    function node_modulesRawLoaderDistCjsJsSrcAppComponentsCheckUpsProfileCheckUpsProfileComponentHtml(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony default export */


      __webpack_exports__["default"] = "<ion-row class=\"main_container\">\n  <ion-col size=\"12\">\n    <p class=\"performance_text\">\n      Vous avez ci-dessus accès à toutes les données relatives à vos Check-ups\n      hebdomadaires. Sélectionnez les groupes musculaires souhaités pour\n      observer leur évolution.\n    </p>\n\n\n    <app-charts-checkup></app-charts-checkup>\n\n\n    <app-chart> </app-chart>\n  </ion-col>\n \n</ion-row>\n";
      /***/
    },

    /***/
    "./node_modules/raw-loader/dist/cjs.js!./src/app/components/coach-payment-visibility/coach-payment-visibility.component.html":
    /*!***********************************************************************************************************************************!*\
      !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/components/coach-payment-visibility/coach-payment-visibility.component.html ***!
      \***********************************************************************************************************************************/

    /*! exports provided: default */

    /***/
    function node_modulesRawLoaderDistCjsJsSrcAppComponentsCoachPaymentVisibilityCoachPaymentVisibilityComponentHtml(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony default export */


      __webpack_exports__["default"] = "<ion-row class=\"main-container\">\n  <ion-col size=\"12\">\n    <p class=\"performance_text\">\n      Vous avez la possibilité d’améliorer temporairement votre classement au sein du classement des coachs du système Transformatics en mettant votre Fiche de Coach en valeur pour les clients. Grâce à cette publicité, vous serez propulsé au TOP 1 du classement de votre choix pendant une durée variable que vous fixerez au préalable.\n    </p>\n  </ion-col>\n  <ion-col size=\"12\">\n    <p class=\"small_text\"><u>Quelques chiffres:</u> un coach placé au TOP 1 du classement du système :</p>\n    <ion-list>\n      <ion-item lines=\"none\">\n        <ion-icon name=\"checkmark-outline\" slot=\"start\"></ion-icon>\n        \n        <ion-label class=\"small_text\">\n          Augmente sa sélection auprès des clients de 82%,\n        </ion-label>\n      </ion-item>\n      <ion-item lines=\"none\">\n        <ion-icon name=\"checkmark-outline\" slot=\"start\"></ion-icon>\n       \n        <ion-label class=\"small_text\">\n          Acquiert en moyenne 13 clients supplémentaires par mois grâce à une publicité de 72 heures ; \n        </ion-label>\n      </ion-item>\n      <ion-item lines=\"none\">\n        <ion-icon name=\"checkmark-outline\" slot=\"start\"></ion-icon>\n      \n        <ion-label class=\"small_text\">\n          Bénéficie d’un revenu moyen mensuel supplémentaire de 667€. \n        </ion-label>\n      </ion-item>\n    </ion-list>\n  </ion-col>\n  <ion-col size=\"12\">\n    <p class=\"small_text\"><u>Grille tarifaire:</u></p>\n    <ion-col size=\"12\">\n    <table class=\"subscription\">\n      <tbody>\n        <tr class=\"small_text\">\n          <td >Durée de la publicité\n          </td>\n          <td >24h</td>\n          <td >48h</td>\n          <td>72h</td>\n        </tr>\n        <tr class=\"small_text\">\n          <td >Membre depuis moins d’un an\n          </td>\n          <td >50€</td>\n          <td >90€</td>\n          <td>110€</td>\n        </tr>\n        <tr class=\"small_text\">\n          <td >Membre depuis plus d’un an\n          </td>\n          <td >30€</td>\n          <td >60€</td>\n          <td>80€</td>\n        </tr>\n      </tbody>\n    </table>\n  </ion-col>\n  </ion-col>\n  <div class=\"container_data\">\n  <ion-row>\n    <div class=\"profile-viewall\">\n        <ion-button (click)=\"openChat()\">Je me lance\n            \n        </ion-button>\n    </div>\n</ion-row>\n</div>\n<ion-grid>\n<ion-row style=\"width: 100%;\">\n\n  <ion-col size=\"8\">\n    <div class=\"tag\">\n      <p class=\"small_text\">Durée de la publicité\n      </p>\n    </div>\n  </ion-col>\n  <ion-col size=\"4\">\n    <div class=\"value\">\n      <p class=\"medium_text\">72 heures\n      </p>\n    </div>\n  </ion-col>\n</ion-row>\n<ion-row style=\"width: 100%;\">\n  <ion-col size=\"8\">\n    <div class=\"tag\">\n      <p class=\"small_text\">Tarif pratiqué \n      </p>\n    </div>\n  </ion-col>\n  <ion-col size=\"4\">\n    <div class=\"value\">\n      <p class=\"medium_text\">110€\n      </p>\n    </div>\n  </ion-col>\n</ion-row>\n<ion-row style=\"width: 100%;\">\n  <ion-col size=\"8\">\n    <div class=\"tag\">\n      <p class=\"small_text\">Sélection de la date de mise en ligne\n      </p>\n    </div>\n  </ion-col>\n  <ion-col size=\"4\">\n    <div class=\"value\">\n      <p class=\"medium_text\">02/11/2021\n      </p>\n    </div>\n  </ion-col>\n</ion-row>\n<ion-row>\n  <ion-col>\n    <ion-button class=\"next\" expand=\"full\" >Valider la campagne de visibilité\n  </ion-button>\n  </ion-col>\n</ion-row>\n<div class=\"container_data\">\n  <ion-row>\n    <div class=\"profile-viewall\">\n        <ion-button (click)=\"openNewRequest()\">Historique des campagnes\n        </ion-button>\n    </div>\n</ion-row>\n</div>\n</ion-grid>\n<ion-grid>\n  <ion-row>\n    <ion-col size=\"12\">\n      <table class=\"history\">\n        <tbody>\n          <tr>\n            <td>MONTANT</td>\n            <td>DESCRIPTION</td>\n            <td>DATE</td>\n          </tr>\n          <tr>\n            <td>9.9€<span>Reused</span></td>\n            <td>lorem ipsum</td>\n            <td>02/11/2021</td>\n          </tr>\n          <tr>\n            <td>11.24€ <span>Reused</span></td>\n            <td>lorem ipsum</td>\n            <td>02/11/2021</td>\n          </tr>\n          <tr>\n            <td>9.99€ <span>Reused</span></td>\n            <td>lorem ipsum</td>\n            <td>02/11/2021</td>\n          </tr>\n        </tbody>\n      </table>\n    </ion-col>\n  </ion-row>\n</ion-grid>\n</ion-row>";
      /***/
    },

    /***/
    "./node_modules/raw-loader/dist/cjs.js!./src/app/components/coach/chart-one/chart-one.component.html":
    /*!***********************************************************************************************************!*\
      !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/components/coach/chart-one/chart-one.component.html ***!
      \***********************************************************************************************************/

    /*! exports provided: default */

    /***/
    function node_modulesRawLoaderDistCjsJsSrcAppComponentsCoachChartOneChartOneComponentHtml(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony default export */


      __webpack_exports__["default"] = "<ion-card>\n  <ion-card-header class=\"card_header\">\n    <ion-row>\n      <ion-col size=\"6\">\n        <div>\n          <p>Février\n          </p>\n          <h4>1670,90€\n          </h4>\n        </div>\n      </ion-col>\n\n      <!-- <ion-col size=\"6\">\n        <div class=\"ion-text-right\">\n          <p>+ 1,5 kg</p>\n        </div>\n      </ion-col> -->\n    </ion-row>\n  </ion-card-header>\n  <ion-card-content>\n    <canvas class=\"canvas\" #lineCanvas></canvas>\n  </ion-card-content>\n\n  <ion-card-footer>\n    <ion-row>\n      <ion-col size=\"6\">\n        <div>\n          <p class=\"p-disabled\">1 Mar 19</p>\n        </div>\n      </ion-col>\n  \n      <ion-col size=\"6\">\n        <div class=\"ion-text-right\">\n          <p class=\"p-disabled\">Aujourd'hui</p>\n        </div>\n      </ion-col>\n    </ion-row>\n  </ion-card-footer>\n</ion-card>\n";
      /***/
    },

    /***/
    "./node_modules/raw-loader/dist/cjs.js!./src/app/components/coach/chart-two/chart-two.component.html":
    /*!***********************************************************************************************************!*\
      !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/components/coach/chart-two/chart-two.component.html ***!
      \***********************************************************************************************************/

    /*! exports provided: default */

    /***/
    function node_modulesRawLoaderDistCjsJsSrcAppComponentsCoachChartTwoChartTwoComponentHtml(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony default export */


      __webpack_exports__["default"] = "<ion-card>\n  <ion-card-header class=\"card_header\">\n    <ion-row>\n      <ion-col size=\"6\">\n        <div>\n          <p>Nouveaux clients\n          </p>\n          <h4>+8\n          </h4>\n        </div>\n      </ion-col>\n\n      <!-- <ion-col size=\"6\">\n        <div class=\"ion-text-right\">\n          <p>+ 1,5 kg</p>\n        </div>\n      </ion-col> -->\n    </ion-row>\n  </ion-card-header>\n  <ion-card-content>\n    <canvas class=\"canvas\" #lineCanvas></canvas>\n  </ion-card-content>\n\n  <ion-card-footer>\n    <ion-row>\n      <ion-col size=\"6\">\n        <div>\n          <p class=\"p-disabled\">1 Mar 19</p>\n        </div>\n      </ion-col>\n  \n      <ion-col size=\"6\">\n        <div class=\"ion-text-right\">\n          <p class=\"p-disabled\">Aujourd'hui</p>\n        </div>\n      </ion-col>\n    </ion-row>\n  </ion-card-footer>\n</ion-card>\n";
      /***/
    },

    /***/
    "./node_modules/raw-loader/dist/cjs.js!./src/app/components/coach/coach-body-scan-step-one/coach-body-scan-step-one.component.html":
    /*!*****************************************************************************************************************************************!*\
      !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/components/coach/coach-body-scan-step-one/coach-body-scan-step-one.component.html ***!
      \*****************************************************************************************************************************************/

    /*! exports provided: default */

    /***/
    function node_modulesRawLoaderDistCjsJsSrcAppComponentsCoachCoachBodyScanStepOneCoachBodyScanStepOneComponentHtml(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony default export */


      __webpack_exports__["default"] = "<ion-grid>\n  <ion-row>\n    <ion-col>\n      <h3 class=\"steper-heading\">INFORMATIONS GENERALES</h3>\n    </ion-col>\n  </ion-row>\n  <ion-row>\n    <ion-col size=\"12\">\n      <p class=\"steper-title\">Vous êtes</p>\n    </ion-col>\n    <ion-col>\n      <div class=\"gender\">\n        <ion-button\n          [color]=\"gender == 'male' ? 'primary' : 'light'\"\n          expand=\"full\"\n          (click)=\"gender = 'male'\"\n          >Un homme\n        </ion-button>\n      </div>\n    </ion-col>\n    <ion-col>\n      <div class=\"gender\">\n        <ion-button\n          [color]=\"gender == 'female' ? 'primary' : 'light'\"\n          color=\"light\"\n          expand=\"full\"\n          (click)=\"gender = 'female'\"\n          >Une femme\n        </ion-button>\n      </div>\n    </ion-col>\n    <ion-col size=\"12\">\n      <p class=\"steper-title dateDeNa\">Date de naissance</p>\n    </ion-col>\n\n    <ion-col size=\"12\">\n      <p class=\"steper-title dateDeNa\">\n        <strong> Adresse postale : </strong> renseignez votre adresse afin que\n        nous puissions vous envoyer vos factures papier autant que numériques.\n      </p>\n    </ion-col>\n\n    <ion-col size=\"12\" class=\"text_desc\">\n      <ion-card mode=\"ios\">\n        <ion-label class=\"small_text_light\"> Adresse </ion-label>\n      </ion-card>\n    </ion-col>\n\n    <ion-col size=\"12\" class=\"text_desc\">\n      <ion-card mode=\"ios\">\n        <ion-label class=\"small_text_light\"> Adresse 2 </ion-label>\n      </ion-card>\n    </ion-col>\n\n    <ion-col size=\"12\">\n      <ion-row>\n        <ion-col size=\"5\" class=\"text_desc\">\n          <ion-card mode=\"ios\">\n            <ion-label class=\"small_text_light\"> Code postal </ion-label>\n          </ion-card>\n        </ion-col>\n\n        <ion-col size=\"7\" class=\"text_desc\">\n          <ion-card mode=\"ios\">\n            <ion-label class=\"small_text_light\"> Ville </ion-label>\n          </ion-card>\n        </ion-col>\n      </ion-row>\n    </ion-col>\n\n    <ion-col size=\"12\">\n      <ion-row>\n        <ion-col size=\"5\" class=\"text_desc\">\n          <ion-card mode=\"ios\">\n            <ion-label class=\"small_text_bold\"> FRANCE </ion-label>\n          </ion-card>\n        </ion-col>\n      </ion-row>\n    </ion-col>\n\n    <ion-col size=\"12\">\n      <p class=\"steper-title dateDeNa\">\n        <strong> Type de diplôme : </strong> sélectionnez votre ou vos diplômes\n        dans la liste suivante. Une photo des diplômes sélectionnés et de votre\n        carte professionnelle vous sera demandée pour authentifier votre\n        déclaration.\n      </p>\n    </ion-col>\n\n    <ion-col size=\"12\" class=\"text_desc\">\n      <ion-card mode=\"ios\">\n        <ion-label class=\"small_text_light\"> Liste de diplômes </ion-label>\n      </ion-card>\n    </ion-col>\n\n    <ion-col size=\"12\">\n      <p class=\"steper-title dateDeNa\">\n        <strong> Domaines de compétences : </strong> sélectionnez dans la liste\n        suivante vos domaines de prédilection et votre activité principale de\n        coaching. déclaration.\n      </p>\n    </ion-col>\n\n\n    <ion-col size=\"12\" class=\"text_desc\">\n      <ion-card mode=\"ios\">\n        <ion-label class=\"small_text_light\"> Domaines de compétences\n        </ion-label>\n      </ion-card>\n    </ion-col>\n  </ion-row>\n</ion-grid>\n";
      /***/
    },

    /***/
    "./node_modules/raw-loader/dist/cjs.js!./src/app/components/coach/coach-body-scan-step-three/coach-body-scan-step-three.component.html":
    /*!*********************************************************************************************************************************************!*\
      !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/components/coach/coach-body-scan-step-three/coach-body-scan-step-three.component.html ***!
      \*********************************************************************************************************************************************/

    /*! exports provided: default */

    /***/
    function node_modulesRawLoaderDistCjsJsSrcAppComponentsCoachCoachBodyScanStepThreeCoachBodyScanStepThreeComponentHtml(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony default export */


      __webpack_exports__["default"] = "<ion-grid>\n  <ion-row>\n    <ion-col>\n      <h3 class=\"steper-heading\">PIECES JUSTIFICATIVES\n      </h3>\n    </ion-col>\n  </ion-row>\n  <ion-row>\n\n\n    <ion-col size=\"12\">\n      <p class=\"steper-title dateDeNa\">\n        <strong> Informations d’auto-entreprise : </strong> entrez votre n° SIREN de votre auto-entreprise ci-dessous, ainsi que vos informations bancaires pour que Transformatics puisse effectuer vos virements mensuels. \n\n      </p>\n    </ion-col>\n\n    <ion-col size=\"12\" class=\"text_desc\">\n      <ion-card mode=\"ios\">\n        <ion-label class=\"small_text_light\"> SIREN </ion-label>\n      </ion-card>\n    </ion-col>\n\n    <ion-col size=\"12\" class=\"text_desc\">\n      <ion-card mode=\"ios\">\n        <ion-label class=\"small_text_light\"> Propriétaire du compte bancaire \n        </ion-label>\n      </ion-card>\n    </ion-col>\n    <ion-col size=\"12\" class=\"text_desc\">\n      <ion-card mode=\"ios\">\n        <ion-label class=\"small_text_light\"> IBAN </ion-label>\n      </ion-card>\n    </ion-col>\n   \n\n    <ion-col size=\"12\">\n      <p class=\"steper-title dateDeNa\">\n        <strong> Type de diplôme : </strong> sélectionnez votre ou vos diplômes\n        dans la liste suivante. Une photo des diplômes sélectionnés et de votre\n        carte professionnelle vous sera demandée pour authentifier votre\n        déclaration.\n      </p>\n    </ion-col>\n\n    <ion-col size=\"12\" class=\"text_desc\">\n      <ion-card mode=\"ios\">\n        <ion-label class=\"small_text_light\"> Liste de diplômes </ion-label>\n      </ion-card>\n    </ion-col>\n\n    <ion-col size=\"12\">\n      <p class=\"steper-title dateDeNa\">\n        <strong> Domaines de compétences : </strong> sélectionnez dans la liste\n        suivante vos domaines de prédilection et votre activité principale de\n        coaching. déclaration.\n      </p>\n    </ion-col>\n\n    <ion-col size=\"12\" class=\"text_desc\">\n      <ion-card mode=\"ios\">\n        <ion-label class=\"small_text_light\">\n          Domaines de compétences\n        </ion-label>\n      </ion-card>\n    </ion-col>\n  </ion-row>\n\n\n\n\n\n\n<ion-row>\n  <ion-col size=\"12\">\n    <ion-label class=\"small_text_bold\">\n      Carte professionnelle et diplômes\n  \n    </ion-label>\n  </ion-col>\n  \n  \n</ion-row>\n\n\n\n  <ion-row class=\"card_item_section ion-margin-top\">\n    <ion-col size=\"6\">\n      <ion-card mode=\"ios\">\n  \n        <ion-card-content>\n          <p>Carte professionnelle\n\n          </p>\n            <ion-img src=\"assets/icon/add.png\"></ion-img>\n      \n        </ion-card-content>\n      </ion-card>\n\n     \n    </ion-col>\n\n\n    <ion-col size=\"6\">\n      <ion-card mode=\"ios\">\n  \n        <ion-card-content>\n          <p>Diplôme STAPS MS\n          </p>\n            <ion-img src=\"assets/icon/add.png\"></ion-img>\n      \n        </ion-card-content>\n      </ion-card>\n\n    \n    </ion-col>\n\n  </ion-row>\n\n\n  <ion-row>\n    <ion-col size=\"12\">\n      <p class=\"steper-title dateDeNa\">\n        <strong> Image : </strong> sélectionnez une photo qui sera utilisée comme fond de page sur votre Fiche de Coach. Elle devrait idéalement vous représenter et faire basculer le futur client vers votre coaching plutôt que celui d’un concurrent ! \n\n      </p>\n    </ion-col>\n\n  </ion-row>\n\n\n\n\n  <ion-row class=\"card_item_section ion-margin-top\">\n    <ion-col size=\"6\">\n      <ion-card mode=\"ios\">\n  \n        <ion-card-content>\n          <p>Image\n          </p>\n            <ion-img src=\"assets/icon/add.png\"></ion-img>\n      \n        </ion-card-content>\n      </ion-card>\n\n    </ion-col>\n\n\n\n  </ion-row>\n\n\n</ion-grid>\n";
      /***/
    },

    /***/
    "./node_modules/raw-loader/dist/cjs.js!./src/app/components/coach/coach-body-scan-step-two/coach-body-scan-step-two.component.html":
    /*!*****************************************************************************************************************************************!*\
      !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/components/coach/coach-body-scan-step-two/coach-body-scan-step-two.component.html ***!
      \*****************************************************************************************************************************************/

    /*! exports provided: default */

    /***/
    function node_modulesRawLoaderDistCjsJsSrcAppComponentsCoachCoachBodyScanStepTwoCoachBodyScanStepTwoComponentHtml(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony default export */


      __webpack_exports__["default"] = "<ion-grid>\n  <ion-row>\n    <ion-col>\n      <h3 class=\"steper-heading\">INFORMATIONS GENERALES</h3>\n    </ion-col>\n  </ion-row>\n  <ion-row>\n    <ion-col size=\"12\">\n      <p class=\"steper-title\">Vous êtes</p>\n    </ion-col>\n\n    <ion-col size=\"12\">\n      <p class=\"steper-title dateDeNa\">\n        <strong>Tarification : </strong>fixez vos tarifs en fonction de ceux des autres coachs du système Transformatics. Les tarifs fixés automatiquement sont établis en fonction de la concurrence et de la sélection client. \n\n      </p>\n    </ion-col>\n\n\n\n    <ion-col size=\"12\">\n      <ion-row>\n        <ion-col size=\"7\" class=\"text_desc\">\n          <ion-card mode=\"ios\">\n            <ion-label class=\"small_text_light\">Domaine de compétences 1\n            </ion-label>\n          </ion-card>\n        </ion-col>\n\n        <ion-col size=\"5\" class=\"text_desc\">\n          <ion-card mode=\"ios\">\n            <ion-label class=\"large_text_bold\"> 79,90€\n            </ion-label>/mois\n          </ion-card>\n        </ion-col>\n      </ion-row>\n    </ion-col>\n\n    <ion-col size=\"12\">\n      <ion-row>\n        <ion-col size=\"7\" class=\"text_desc\">\n          <ion-card mode=\"ios\">\n            <ion-label class=\"small_text_light\">Domaine de compétences 2\n\n            </ion-label>\n          </ion-card>\n        </ion-col>\n\n        <ion-col size=\"5\" class=\"text_desc\">\n          <ion-card mode=\"ios\">\n            <ion-label class=\"large_text_bold\"> 89,90€\n            </ion-label>/mois\n          </ion-card>\n        </ion-col>\n      </ion-row>\n    </ion-col>\n\n\n    <ion-col size=\"12\">\n      <p class=\"steper-title dateDeNa\">\n        <strong> Description : </strong>  dans l’encadré ci-dessous, écrivez quelques lignes que vos futurs clients pourront lire sur votre Fiche de Coach. Ces quelques mots doivent être accrocheurs, mentionner votre parcours ou votre expérience, votre passion du métier, …  \n\n      </p>\n    </ion-col>\n\n    <ion-col size=\"12\" class=\"text_desc\">\n      <ion-card mode=\"ios\">\n        <ion-label class=\"small_text_light\"> Liste de diplômes </ion-label>\n      </ion-card>\n    </ion-col>\n\n    <ion-col size=\"12\">\n      <p class=\"steper-title dateDeNa\">\n        <strong> Domaines de compétences : </strong> sélectionnez dans la liste\n        suivante vos domaines de prédilection et votre activité principale de\n        coaching. déclaration.\n      </p>\n    </ion-col>\n\n\n    <ion-col size=\"12\" class=\"text_desc\">\n      <ion-card mode=\"ios\">\n        <ion-label class=\"small_text_light\"> Domaines de compétences\n        </ion-label>\n      </ion-card>\n    </ion-col>\n\n\n    <ion-col size=\"12\">\n      <ion-row class=\"excercise_input_section\">\n        <ion-item class=\"small_text\" style=\"border-bottom: 1px solid #a9a5a5;\">\n          <ion-input placeholder=\"Description de votre Fiche de Coach…\">\n          </ion-input>\n\n          <ion-label class=\"small_text\"> 0/200 </ion-label>\n        </ion-item>\n      </ion-row>\n    </ion-col>\n  </ion-row>\n\n\n</ion-grid>\n";
      /***/
    },

    /***/
    "./node_modules/raw-loader/dist/cjs.js!./src/app/components/header/header.component.html":
    /*!***********************************************************************************************!*\
      !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/components/header/header.component.html ***!
      \***********************************************************************************************/

    /*! exports provided: default */

    /***/
    function node_modulesRawLoaderDistCjsJsSrcAppComponentsHeaderHeaderComponentHtml(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony default export */


      __webpack_exports__["default"] = "<ion-header>\n  <ion-toolbar>\n    <ion-title>Hello Gopal</ion-title>\n\n  </ion-toolbar>\n</ion-header>";
      /***/
    },

    /***/
    "./node_modules/raw-loader/dist/cjs.js!./src/app/components/modals/add-excercise-wood-modal/add-excercise-wood-modal.component.html":
    /*!******************************************************************************************************************************************!*\
      !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/components/modals/add-excercise-wood-modal/add-excercise-wood-modal.component.html ***!
      \******************************************************************************************************************************************/

    /*! exports provided: default */

    /***/
    function node_modulesRawLoaderDistCjsJsSrcAppComponentsModalsAddExcerciseWoodModalAddExcerciseWoodModalComponentHtml(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony default export */


      __webpack_exports__["default"] = "<ion-content>\n  <ion-grid>\n    <ion-row class=\"ion-margin-top\">\n      <ion-label class=\"small_text\">\n        Sélectionnez une catégorie du <strong>WOD</strong> : \n\n      </ion-label>\n    </ion-row>\n\n    <ion-row>\n      <ion-col size=\"6\">\n        <ion-button expand=\"full\" [color]=\"categoryWood =='warm-up' ? 'primary' : 'light'\" class=\"small_text\" (click)=\"categoryWood ='warm-up'\">\n          Warm-up\n\n        </ion-button>\n      </ion-col>\n\n      <ion-col size=\"6\">\n        <ion-button expand=\"full\" [color]=\"categoryWood =='skill' ? 'primary' : 'light'\" class=\"small_text\" (click)=\"categoryWood ='skill'\">\n          Skill\n        </ion-button>\n      </ion-col>\n    </ion-row>\n    <ion-row>\n      <ion-col size=\"6\">\n        <ion-button expand=\"full\" [color]=\"categoryWood =='mini-wood' ? 'primary' : 'light'\" class=\"small_text\" (click)=\"categoryWood ='mini-wood'\">\n          Mini-WOD’s\n\n\n        </ion-button>\n      </ion-col>\n\n      <ion-col size=\"6\">\n        <ion-button expand=\"full\" [color]=\"categoryWood =='etirements' ? 'primary' : 'light'\" class=\"small_text\" (click)=\"categoryWood ='etirements'\">\n          Etirements\n        </ion-button>\n      </ion-col>\n    </ion-row>\n\n\n\n\n\n\n\n  </ion-grid>\n\n\n\n\n  <ion-grid>\n    <ion-row class=\"ion-margin-top\">\n      <ion-label class=\"small_text\">\n        Sélectionnez un type de <strong>WOD</strong> :\n \n\n      </ion-label>\n    </ion-row>\n\n    <ion-row>\n      <ion-col size=\"6\">\n        <ion-button expand=\"full\" [color]=\"typeWood =='MetCon' ? 'primary' : 'light'\" class=\"small_text\" (click)=\"typeWood ='MetCon'\">\n          MetCon\n        </ion-button>\n      </ion-col>\n\n      <ion-col size=\"6\">\n        <ion-button expand=\"full\" [color]=\"typeWood =='Powerlifting' ? 'primary' : 'light'\" class=\"small_text\" (click)=\"typeWood ='Powerlifting'\">\n          Powerlifting\n        </ion-button>\n      </ion-col>\n    </ion-row>\n\n\n\n\n\n\n\n  </ion-grid>\n\n\n\n\n  <ion-grid>\n    <ion-row class=\"ion-margin-top\">\n      <ion-label class=\"small_text\">\n        Sélectionnez une consigne : \n\n \n\n      </ion-label>\n    </ion-row>\n\n    <ion-row>\n      <ion-col size=\"6\">\n        <ion-button expand=\"full\" [color]=\"selectLocker =='for_time' ? 'primary' : 'light'\" class=\"small_text\" (click)=\"selectLocker ='for_time'\">\n          For Time\n\n        </ion-button>\n      </ion-col>\n\n      <ion-col size=\"6\">\n        <ion-button expand=\"full\" [color]=\"selectLocker =='for_rounds' ? 'primary' : 'light'\" class=\"small_text\" (click)=\"selectLocker ='for_rounds'\">\n          For Rounds\n        </ion-button>\n      </ion-col>\n    </ion-row>\n\n\n\n\n\n\n\n  </ion-grid>\n\n\n\n\n\n\n\n\n\n\n\n\n\n  <ion-grid>\n    <ion-row class=\"ion-margin-top\">\n      <ion-label class=\"small_text\">\n        Sélectionnez un <strong>Mini-WoD</strong> :\n\n\n      </ion-label>\n    </ion-row>\n\n    <ion-row>\n      <ion-col size=\"6\">\n        <ion-button expand=\"full\" [color]=\"selectMiniWood =='faq_afap' ? 'primary' : 'light'\" class=\"small_text\" (click)=\"selectMiniWood ='faq_afap'\">\n          FAQAYC / AFAP \n\n\n        </ion-button>\n      </ion-col>\n\n      <ion-col size=\"6\">\n        <ion-button expand=\"full\" [color]=\"selectMiniWood =='sprints' ? 'primary' : 'light'\" class=\"small_text\" (click)=\"selectMiniWood ='sprints'\">\n          Sprints\n        </ion-button>\n      </ion-col>\n    </ion-row>\n    <ion-row>\n      <ion-col size=\"6\">\n        <ion-button expand=\"full\" [color]=\"selectMiniWood =='tabata' ? 'primary' : 'light'\" class=\"small_text\" (click)=\"selectMiniWood ='tabata'\">\n          Tabata\n\n        </ion-button>\n      </ion-col>\n\n      <ion-col size=\"6\">\n        <ion-button expand=\"full\" [color]=\"selectMiniWood =='chipper' ? 'primary' : 'light'\" class=\"small_text\" (click)=\"selectMiniWood ='chipper'\">\n          Chipper\n        </ion-button>\n      </ion-col>\n    </ion-row>\n\n\n\n\n\n\n\n  </ion-grid>\n</ion-content>";
      /***/
    },

    /***/
    "./node_modules/raw-loader/dist/cjs.js!./src/app/components/modals/before-aliments/before-aliments.component.html":
    /*!************************************************************************************************************************!*\
      !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/components/modals/before-aliments/before-aliments.component.html ***!
      \************************************************************************************************************************/

    /*! exports provided: default */

    /***/
    function node_modulesRawLoaderDistCjsJsSrcAppComponentsModalsBeforeAlimentsBeforeAlimentsComponentHtml(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony default export */


      __webpack_exports__["default"] = "<ion-toolbar class=\"ion-border\">\n  <ion-buttons slot=\"start\" (click)=\"closeModal()\">\n    <ion-button>\n      <ion-icon name=\"close-outline\" class=\"large_text_bold close-btn-icon\"></ion-icon>\n    </ion-button>\n  </ion-buttons>\n\n  <ion-text class=\"large_text_bold ion-text-center\"> MA DIETE </ion-text>\n</ion-toolbar>\n<ion-content>\n  <ion-grid>\n\n    <ion-row class=\"card_item_section ion-margin-top ion-margin-bottom\">\n      <ion-col size=\"6\">\n        <ion-card>\n          <ion-card-header>\n            <ion-button (click)=\"gotoAliments()\">\n              Petit-Déjeuner\n  \n            </ion-button>\n          </ion-card-header>\n\n          <ion-card-content>\n              <ion-img src=\"assets/icon/tick.png\"></ion-img>\n          </ion-card-content>\n        </ion-card>\n\n        <div class=\"card_footer_data ion-margin-top\">\n          <p class=\"medium_text\">\n            398 kcal\n\n          </p>\n\n          <p class=\"small_text\">\n            Recommandé : 667kcal\n\n          </p>\n        </div>\n\n\n      </ion-col>\n\n      <ion-col size=\"6\">\n        <ion-card>\n          <ion-card-header>\n            <ion-button (click)=\"gotoAliments()\">\n              Petit-Déjeuner\n  \n            </ion-button>\n          </ion-card-header>\n\n          <ion-card-content>\n              <ion-img src=\"assets/icon/add.png\"></ion-img>\n              <p>Ajouter</p>\n          </ion-card-content>\n        </ion-card>\n\n        <div class=\"card_footer_data ion-margin-top\">\n          <p class=\"medium_text\">\n            398 kcal\n\n          </p>\n\n          <p class=\"small_text\">\n            Recommandé : 667kcal\n\n          </p>\n        </div>\n      </ion-col>\n\n\n    </ion-row>\n\n\n    <ion-row class=\"card_item_section ion-margin-top\">\n      <ion-col size=\"6\">\n        <ion-card>\n          <ion-card-header>\n            <ion-button (click)=\"gotoAliments()\">\n              Petit-Déjeuner\n  \n            </ion-button>\n          </ion-card-header>\n\n          <ion-card-content>\n              <ion-img src=\"assets/icon/tick.png\"></ion-img>\n          </ion-card-content>\n        </ion-card>\n\n        <div class=\"card_footer_data ion-margin-top\">\n          <p class=\"medium_text\">\n            398 kcal\n\n          </p>\n\n          <p class=\"small_text\">\n            Recommandé : 667kcal\n\n          </p>\n        </div>\n\n        \n      </ion-col>\n\n      <ion-col size=\"6\">\n        <ion-card>\n          <ion-card-header>\n            <ion-button>\n              Petit-Déjeuner\n  \n            </ion-button>\n          </ion-card-header>\n\n          <ion-card-content>\n              <ion-img src=\"assets/icon/add.png\"></ion-img>\n              <p>Ajouter</p>\n          </ion-card-content>\n        </ion-card>\n\n        <div class=\"card_footer_data ion-margin-top\">\n          <p class=\"medium_text\">\n            398 kcal\n\n          </p>\n\n          <p class=\"small_text\">\n            Recommandé : 667kcal\n\n          </p>\n        </div>\n      </ion-col>\n\n\n    </ion-row>\n\n  </ion-grid>\n</ion-content>\n";
      /***/
    },

    /***/
    "./node_modules/raw-loader/dist/cjs.js!./src/app/components/modals/calendar-modal/calendar-modal.html":
    /*!************************************************************************************************************!*\
      !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/components/modals/calendar-modal/calendar-modal.html ***!
      \************************************************************************************************************/

    /*! exports provided: default */

    /***/
    function node_modulesRawLoaderDistCjsJsSrcAppComponentsModalsCalendarModalCalendarModalHtml(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony default export */


      __webpack_exports__["default"] = "<ion-content>\n  <!-- <div class=\"daybox\">\n    <ion-icon name=\"chevron-back-outline\" color=\"primary\"></ion-icon>\n    <div class=\"daybox-title\">AUJOURD’HUI</div>\n  \n    <ion-icon name=\"chevron-forward-outline\" color=\"primary\"></ion-icon>\n</div> -->\n  <div class=\"toggle-button\">\n  \n      <ion-toggle class=\"rainy-toggle\" (ionChange)=\"showDetail($event)\"></ion-toggle>\n   \n    \n  </div>\n  <div *ngIf=\"!showDetails\">\n    <ion-calendar  [(ngModel)]=\"date\"\n    (onChange)=\"onChange($event)\"\n    [options]=\"options\"\n    type=\"string\"\n    format=\"YYYY-MM-DD\">\n  </ion-calendar>\n  </div>\n  <div *ngIf=\"showDetails\">\n    <ion-calendar  class=\"custom-calendar\" [(ngModel)]=\"date\"\n    (onChange)=\"onChange($event)\"\n    [options]=\"options\"\n    type=\"string\"\n    format=\"YYYY-MM-DD\">\n  </ion-calendar>\n  </div>\n  <div *ngIf=\"showDetails\">\n    <div class=\"detail-box\">\n      <p class=\"small_text\">Le calendrier vous permet accessoirement de délimiter vos périodes menstruelles. Pour intégrer cette fonctionnalité sur votre calendrier personnel, remplissez les lignes suivantes:</p>\n      <ion-list>\n        <ion-item lines=\"none\">\n          <ion-label>\n            Date de début de vos dernières menstruations\n          </ion-label>\n          <ion-datetime  placeholder=\"Select Date\"></ion-datetime>\n        </ion-item>\n        <ion-item lines=\"none\">\n          <ion-label>\n            Date de fin de vos dernières menstruations\n          </ion-label>\n          <ion-datetime placeholder=\"Select Date\"></ion-datetime>\n        </ion-item>\n        <ion-item lines=\"none\">\n          <ion-label>\n            Durée moyenne d’un cycle menstruel\n          </ion-label>\n          <ion-select placeholder=\"Select Days\">\n            <ion-select-option *ngFor=\"let val of days; index as i\">{{i + 1}}</ion-select-option>\n          </ion-select>\n          \n        </ion-item>\n      </ion-list>\n      <ion-button class=\"next\" expand=\"full\" shape=\"round\" (click)=\"save()\">Sauvegarder les dates des menstruations\n      </ion-button>\n    </div>\n  </div>\n</ion-content>\n";
      /***/
    },

    /***/
    "./node_modules/raw-loader/dist/cjs.js!./src/app/components/modals/check-up-modals/check-up-modals.component.html":
    /*!************************************************************************************************************************!*\
      !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/components/modals/check-up-modals/check-up-modals.component.html ***!
      \************************************************************************************************************************/

    /*! exports provided: default */

    /***/
    function node_modulesRawLoaderDistCjsJsSrcAppComponentsModalsCheckUpModalsCheckUpModalsComponentHtml(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony default export */


      __webpack_exports__["default"] = "<ion-toolbar class=\"ion-border\" >\n  <ion-buttons slot=\"start\" (click)=\"closeModal()\">\n    <ion-button>\n      <ion-icon\n        name=\"close-outline\"\n        class=\"large_text_bold close-btn-icon\"\n      ></ion-icon>\n    </ion-button>\n  </ion-buttons>\n\n  <ion-text class=\"large_text_bold\"> CHECK-UP – </ion-text>\n  <ion-text class=\"medium_text\"> LUNDI 17 FEVRIER 2020 </ion-text>\n</ion-toolbar>\n\n<ion-content>\n  <ion-grid>\n    <ion-row>\n      <ion-col size=\"6\">\n        <ion-card class=\"check-out-big-cards\">\n          <p>POIDS</p>\n          <div>\n            <!-- <h4 class=\"large_text_bold\">88</h4>\n            <ion-label class=\"small_text\"> kg</ion-label> -->\n            <ion-item>\n              <ion-input placeholder=\"88 kg\"></ion-input>\n            </ion-item>\n          </div>\n        </ion-card>\n      </ion-col>\n\n      <ion-col size=\"6\">\n        <ion-card class=\"check-out-big-cards\">\n          <p>TAILLE</p>\n          <div>\n            <!-- <h4 class=\"large_text_bold\">88</h4>\n            <ion-label class=\"small_text\"> kg</ion-label> -->\n            <ion-item>\n              <ion-input placeholder=\"182 kg\"></ion-input>\n            </ion-item>\n          </div>\n        </ion-card>\n      </ion-col>\n    </ion-row>\n\n    <!--          \n            <h4 class=\"medium_text_bold\">63</h4>\n            <ion-label class=\"small_text\"> cm</ion-label> -->\n\n    <ion-row>\n      <ion-col size=\"3\">\n        <ion-card class=\"check-out-small-cards\">\n          <div class=\"small_card_title\">\n            <p class=\"very_small_text\">Biceps droit</p>\n          </div>\n          <div class=\"small_card_desc\">\n            <ion-item>\n              <ion-input placeholder=\"63 kg\"></ion-input>\n            </ion-item>\n          </div>\n        </ion-card>\n      </ion-col>\n\n      <ion-col size=\"3\">\n        <ion-card class=\"check-out-small-cards\">\n          <div class=\"small_card_title\">\n            <p class=\"very_small_text\">Biceps gauche</p>\n          </div>\n          <div class=\"small_card_desc\">\n            <ion-item>\n              <ion-input placeholder=\"63 kg\"></ion-input>\n            </ion-item>\n          </div>\n        </ion-card>\n      </ion-col>\n\n      <ion-col size=\"3\">\n        <ion-card class=\"check-out-small-cards\">\n          <div class=\"small_card_title\">\n            <p class=\"very_small_text\">Avant-bras droit</p>\n          </div>\n          <div class=\"small_card_desc\">\n            <ion-item>\n              <ion-input placeholder=\"63 kg\"></ion-input>\n            </ion-item>\n          </div>\n        </ion-card>\n      </ion-col>\n\n      <ion-col size=\"3\">\n        <ion-card class=\"check-out-small-cards\">\n          <div class=\"small_card_title\">\n            <p class=\"very_small_text\">Avant-bras gauche</p>\n          </div>\n          <div class=\"small_card_desc\">\n            <ion-item>\n              <ion-input placeholder=\"63 kg\"></ion-input>\n            </ion-item>\n          </div>\n        </ion-card>\n      </ion-col>\n    </ion-row>\n\n    <ion-row>\n      <ion-col size=\"3\">\n        <ion-card class=\"check-out-small-cards\">\n          <div class=\"small_card_title\">\n            <p class=\"very_small_text\">Cuisse droite</p>\n          </div>\n          <div class=\"small_card_desc\">\n            <ion-item>\n              <ion-input placeholder=\"63 kg\"></ion-input>\n            </ion-item>\n          </div>\n        </ion-card>\n      </ion-col>\n\n      <ion-col size=\"3\">\n        <ion-card class=\"check-out-small-cards\">\n          <div class=\"small_card_title\">\n            <p class=\"very_small_text\">Cuisse gauche</p>\n          </div>\n          <div class=\"small_card_desc\">\n            <ion-item>\n              <ion-input placeholder=\"63 kg\"></ion-input>\n            </ion-item>\n          </div>\n        </ion-card>\n      </ion-col>\n\n      <ion-col size=\"3\">\n        <ion-card class=\"check-out-small-cards\">\n          <div class=\"small_card_title\">\n            <p class=\"very_small_text\">Tour de taille</p>\n          </div>\n          <div class=\"small_card_desc\">\n            <ion-item>\n              <ion-input placeholder=\"63 kg\"></ion-input>\n            </ion-item>\n          </div>\n        </ion-card>\n      </ion-col>\n\n      <ion-col size=\"3\">\n        <ion-card class=\"check-out-small-cards\">\n          <div class=\"small_card_title\">\n            <p class=\"very_small_text\">Tour de poitrine</p>\n          </div>\n          <div class=\"small_card_desc\">\n            <ion-item>\n              <ion-input placeholder=\"63 kg\"></ion-input>\n            </ion-item>\n          </div>\n        </ion-card>\n      </ion-col>\n    </ion-row>\n\n    <ion-row>\n      <ion-col size=\"3\">\n        <ion-card class=\"check-out-small-cards\">\n          <div class=\"small_card_title\">\n            <p class=\"very_small_text\">Largeur d’épaules</p>\n          </div>\n          <div class=\"small_card_desc\">\n            <ion-item>\n              <ion-input placeholder=\"63 kg\"></ion-input>\n            </ion-item>\n          </div>\n        </ion-card>\n      </ion-col>\n\n      <ion-col size=\"3\">\n        <ion-card class=\"check-out-small-cards\">\n          <div class=\"small_card_title\">\n            <p class=\"very_small_text\">Largeur de hanches</p>\n          </div>\n          <div class=\"small_card_desc\">\n            <ion-item>\n              <ion-input placeholder=\"63 kg\"></ion-input>\n            </ion-item>\n          </div>\n        </ion-card>\n      </ion-col>\n    </ion-row>\n\n    <ion-row class=\"ion-margin-top\">\n      \n        <ion-col size=\"4\">\n          <ion-card\n          [class]=\"program\"\n          (click)=\"donsomething($event, src)\"\n          style=\"height: 100%;\"\n        >\n          <ion-card-content class=\"slide_card_content\">\n            <img [src]=\"morphotypeArray[0].src\" class=\"slide-img\" />\n          </ion-card-content>\n        </ion-card>\n        </ion-col>\n        <ion-col size=\"4\">\n          <ion-card\n          [class]=\"program\"\n          (click)=\"donsomething($event, src)\"\n          style=\"height: 100%;\"\n        >\n          <ion-card-content class=\"slide_card_content\">\n            <img [src]=\"morphotypeArray[1].src\" class=\"slide-img\" />\n          </ion-card-content>\n        </ion-card>\n        </ion-col>\n        <ion-col size=\"4\">\n          <ion-card\n          [class]=\"program\"\n          (click)=\"donsomething($event, src)\"\n          style=\"   height: 100%;\"\n        >\n          <ion-card-content class=\"slide_card_content\">\n            <img [src]=\"morphotypeArray[2].src\" class=\"slide-img\" />\n          </ion-card-content>\n        </ion-card>\n        </ion-col>\n        <!-- <ion-slides pager=\"false\" [options]=\"categories\" #slides>\n          <ion-slide *ngFor=\"let src of morphotypeArray\">\n            <ion-card\n              [class]=\"src.activeClass ? 'program active' : 'program'\"\n              (click)=\"donsomething($event, src)\"\n            >\n              <ion-card-content>\n                <img [src]=\"src.src\" class=\"slide-img\" />\n              </ion-card-content>\n            </ion-card>\n          </ion-slide>\n        </ion-slides> -->\n      \n    </ion-row>\n\n    <ion-row class=\"ion-margin-top ion-margin-bottom\">\n      <ion-button expand=\"full\" class=\"validate_btn\">\n        Valider le check-up\n      </ion-button>\n    </ion-row>\n  </ion-grid>\n</ion-content>\n";
      /***/
    },

    /***/
    "./node_modules/raw-loader/dist/cjs.js!./src/app/components/modals/click-add-excercise-modal/click-add-excercise-modal.component.html":
    /*!********************************************************************************************************************************************!*\
      !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/components/modals/click-add-excercise-modal/click-add-excercise-modal.component.html ***!
      \********************************************************************************************************************************************/

    /*! exports provided: default */

    /***/
    function node_modulesRawLoaderDistCjsJsSrcAppComponentsModalsClickAddExcerciseModalClickAddExcerciseModalComponentHtml(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony default export */


      __webpack_exports__["default"] = "<ion-content>\n  <ion-grid>\n    <ion-row class=\"ion-margin-top\">\n      <ion-label class=\"medium_text\">\n        Sélectionnez le type d’exercice à ajouter :  \n      </ion-label>\n    </ion-row>\n\n\n\n    <ion-row >\n      <ion-col size=\"12\">\n        <ion-button class=\"small_text\" (click)=\"goToNext()\" expand=\"full\" [color]=\"typeOfSupersetBtnArray =='excercise_de_force' ? 'primary' : 'light'\" (click)=\"typeOfSupersetBtnArray ='excercise_de_force'\">\n          Exercice de force\n        </ion-button>\n        \n      </ion-col>\n    </ion-row>\n\n\n\n    <ion-row >\n      <ion-col size=\"12\">\n        <ion-button class=\"small_text\" (click)=\"goToNext()\" expand=\"full\" [color]=\"typeOfSupersetBtnArray =='excercise_normal' ? 'primary' : 'light'\" (click)=\"typeOfSupersetBtnArray ='excercise_normal'\">\n          Exercice normal\n        </ion-button>\n        \n      </ion-col>\n    </ion-row>\n\n\n\n  </ion-grid>\n</ion-content>";
      /***/
    },

    /***/
    "./node_modules/raw-loader/dist/cjs.js!./src/app/components/modals/coach-chat-modal/coach-chat-modal.component.html":
    /*!**************************************************************************************************************************!*\
      !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/components/modals/coach-chat-modal/coach-chat-modal.component.html ***!
      \**************************************************************************************************************************/

    /*! exports provided: default */

    /***/
    function node_modulesRawLoaderDistCjsJsSrcAppComponentsModalsCoachChatModalCoachChatModalComponentHtml(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony default export */


      __webpack_exports__["default"] = "<ion-toolbar class=\"ion-border\">\n  <ion-buttons slot=\"start\" (click)=\"closeModal()\">\n    <ion-button>\n      <ion-icon name=\"close-outline\" class=\"large_text_bold close-btn-icon\"></ion-icon>\n    </ion-button>\n  </ion-buttons>\n</ion-toolbar>\n\n<ion-content>\n  <ion-grid>\n    <ion-row>\n      <ion-col size=\"4\">\n        <ion-button class=\"next\" expand=\"full\" (click)=\"openCheckUp()\">CHECK-UPS\n        </ion-button>\n      </ion-col>\n      <ion-col size=\"4\">\n        <ion-button class=\"next\" expand=\"full\" >MODIFIER LE PROGRAMME\n        </ion-button>\n      </ion-col>\n      <ion-col size=\"4\">\n        <ion-button class=\"next\" expand=\"full\" >CRÉER UN NOUVEAU PROGRAMME\n        </ion-button>\n      </ion-col>\n    </ion-row>\n    <ion-row>\n      <ion-col size=\"12\">\n        <ion-item>\n          <ion-buttons class=\"ion-no-margin\" slot=\"start\">\n              <ion-back-button></ion-back-button>\n          </ion-buttons>\n          <ion-avatar slot=\"start\">\n              <img src=\"assets/icon/man.png\">\n          </ion-avatar>\n          <ion-label>\n              <h3>JEAN MICHEL ANDRAS</h3>\n              <p>Connecté</p>\n          </ion-label>\n      </ion-item>\n      </ion-col>\n    </ion-row>\n    <ion-row>\n      <ion-col size=\"12\">\n        <div class=\"header-button-box\">\n          <div class=\"header-button-box-button-right\">\n              <div class=\"header-button-box-button \">\n                  <img src=\"assets/icon/attach.png\">\n              </div>\n              <div class=\"header-button-box-button\">\n                  <img src=\"assets/icon/video.png\">\n              </div>\n              <div class=\"header-button-box-button\">\n                  <img src=\"assets/icon/call.png\">\n              </div>\n          </div>\n      </div>\n      <div style=\"margin-top: 10px;\">\n          <ion-list class=\"chat-screen-box\">\n              <div class=\"ion-text-center startchartdate\">\n                  <p>Thursday,January 22,2015</p>\n              </div>\n              <ion-item class=\"chat-input\">\n                  <ion-text slot=\"end\" class=\"f10\">4:31 PM</ion-text>\n                  <ion-text class=\"chat-text-custemer f12\">\n                      <p>Reference site about Lorem Ipsum, giving information on its origins, as well as a random Lipsum generator.</p>\n                  </ion-text>\n              </ion-item>\n              <ion-item class=\"chat-input\">\n                  <ion-text slot=\"end\" class=\"chat-text-user f12\">\n                      <p>Reference site about Lorem Ipsum, giving information on its origins, as well as a random Lipsum generator.</p>\n                  </ion-text>\n                  <ion-text slot=\"start\" class=\"f10\">4:31 PM</ion-text>\n              </ion-item>\n              <div class=\"ion-text-center seendate\">\n                  <span>Thursday,January 22,2015</span>\n              </div>\n              <ion-item class=\"chat-input\">\n                  <ion-text slot=\"end\" class=\"f10\">4:31 PM</ion-text>\n                  <ion-text class=\"chat-text-custemer f12\">\n                      <p>Reference site about Lorem Ipsum, giving information on its origins, as well as a random Lipsum generator.</p>\n                  </ion-text>\n                  <!-- <ion-avatar slot=\"start\">\n                      <img src=\"assets/icon/man.png\">\n                  </ion-avatar> -->\n              </ion-item>\n              <ion-item class=\"chat-input\">\n                  <ion-text slot=\"end\" class=\"chat-text-user f12\">\n                      <p>Reference site about Lorem Ipsum, giving information on its origins, as well as a random Lipsum generator.</p>\n                  </ion-text>\n                  <ion-text slot=\"start\" class=\"f10\">4:31 PM</ion-text>\n              </ion-item>\n              <ion-item class=\"chat-input\">\n                  <ion-text slot=\"end\" class=\"f10\">4:31 PM</ion-text>\n                  <ion-text class=\"chat-text-custemer f12\">\n                      <p>Reference site about Lorem Ipsum, giving information on its origins, as well as a random Lipsum generator.</p>\n                  </ion-text>\n              </ion-item>\n              <div class=\"ion-text-center seendate\">\n                  <span>Thursday,January 22,2015</span>\n              </div>\n              <ion-item class=\"chat-input\">\n                  <ion-text slot=\"end\" class=\"chat-text-user f12\">\n                      <p>Reference site about Lorem Ipsum, giving information on its origins, as well as a random Lipsum generator.</p>\n                  </ion-text>\n                  <ion-text slot=\"start\" class=\"f10\">4:31 PM</ion-text>\n              </ion-item>\n          </ion-list>\n      </div>\n      </ion-col>\n    </ion-row>\n  </ion-grid>\n  <ion-footer>\n    <ion-toolbar>\n        <ion-grid>\n            <ion-row>\n                <ion-col>\n                    <ion-item class=\"chat-input\">\n                        <ion-icon slot=\"start\" name=\"image-outline\"></ion-icon>\n                        <ion-input placeholder=\"Ecrivez votre message ici\"></ion-input>\n                        <ion-icon slot=\"end\" name=\"send-outline\"></ion-icon>\n                    </ion-item>\n                </ion-col>\n            </ion-row>\n        </ion-grid>\n    </ion-toolbar>\n</ion-footer>\n</ion-content>";
      /***/
    },

    /***/
    "./node_modules/raw-loader/dist/cjs.js!./src/app/components/modals/coach-checkup-modal/coach-checkup-modal.component.html":
    /*!********************************************************************************************************************************!*\
      !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/components/modals/coach-checkup-modal/coach-checkup-modal.component.html ***!
      \********************************************************************************************************************************/

    /*! exports provided: default */

    /***/
    function node_modulesRawLoaderDistCjsJsSrcAppComponentsModalsCoachCheckupModalCoachCheckupModalComponentHtml(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony default export */


      __webpack_exports__["default"] = "<ion-toolbar class=\"ion-border\">\n  <ion-buttons slot=\"start\" (click)=\"closeModal()\">\n    <ion-button>\n      <ion-icon name=\"close-outline\" class=\"large_text_bold close-btn-icon\"></ion-icon>\n    </ion-button>\n  </ion-buttons>\n</ion-toolbar>\n\n<ion-content>\n  <ion-grid>\n    <ion-row>\n      <ion-col size=\"12\">\n        <div class=\"daybox\">\n          <ion-icon\n            name=\"chevron-back-outline\"\n            (click)=\"goBack()\"\n            class=\"back-icon\"\n            color=\"primary\"\n          ></ion-icon>\n          <ion-slides pager=\"false\" [options]=\"options\" #myslide>\n            <ion-slide *ngFor=\"let day of days; index as i\">\n              <div class=\"daybox-title\" (click)=\"openCalendar()\">\n                {{day.dayName}}\n              </div>\n            </ion-slide>\n          </ion-slides>\n          <ion-icon\n            name=\"chevron-forward-outline\"\n            (click)=\"goAhead()\"\n            class=\"forward-icon\"\n            color=\"primary\"\n          ></ion-icon>\n        </div>\n      </ion-col>\n    </ion-row>\n    <ion-row>\n      <ion-col size=\"6\">\n        <ion-card class=\"ion-padding-end check-out-big-cards\">\n          <p>POIDS</p>\n          <div>\n         \n            <h4 class=\"large_text_bold\">88</h4>\n            <ion-label class=\"small_text\"> kg</ion-label>\n          </div>\n        </ion-card>\n      </ion-col>\n\n      <ion-col size=\"6\">\n        <ion-card class=\"ion-padding-end check-out-big-cards\">\n          <p>TAILLE</p>\n          <div>\n         \n            <h4 class=\"large_text_bold\">182</h4>\n            <ion-label class=\"small_text\"> cm</ion-label>\n          </div>\n        </ion-card>\n      </ion-col>\n    </ion-row>\n\n\n    <ion-row>\n      <ion-col size=\"3\">\n        <ion-card class=\"check-out-small-cards\">\n          <div class=\"small_card_title\">\n            <p class=\"very_small_text\">Biceps droit</p>\n          </div>\n          <div class=\"small_card_desc\">\n         \n            <h4 class=\"medium_text_bold\">63</h4>\n            <ion-label class=\"small_text\"> cm</ion-label>\n          </div>\n        </ion-card>\n      </ion-col>\n\n      <ion-col size=\"3\">\n        <ion-card class=\"check-out-small-cards\">\n          <div class=\"small_card_title\">\n            <p class=\"very_small_text\">Biceps gauche</p>\n          </div>\n          <div class=\"small_card_desc\">\n         \n            <h4 class=\"medium_text_bold\">63</h4>\n            <ion-label class=\"small_text\"> cm</ion-label>\n          </div>\n        </ion-card>\n      </ion-col>\n\n      <ion-col size=\"3\">\n        <ion-card class=\"check-out-small-cards\">\n          <div class=\"small_card_title\">\n            <p class=\"very_small_text\">Avant bras droit</p>\n          </div>\n          <div class=\"small_card_desc\">\n         \n            <h4 class=\"medium_text_bold\">63</h4>\n            <ion-label class=\"small_text\"> cm</ion-label>\n          </div>\n        </ion-card>\n      </ion-col>\n\n\n\n      <ion-col size=\"3\">\n        <ion-card class=\"check-out-small-cards\">\n          <div class=\"small_card_title\">\n            <p class=\"very_small_text\">Avant-bras gauche</p>\n          </div>\n          <div class=\"small_card_desc\">\n         \n            <h4 class=\"medium_text_bold\">63</h4>\n            <ion-label class=\"small_text\"> cm</ion-label>\n          </div>\n        </ion-card>\n      </ion-col>\n\n    </ion-row>\n\n\n\n\n\n    <ion-row>\n      <ion-col size=\"3\">\n        <ion-card class=\"check-out-small-cards\">\n          <div class=\"small_card_title\">\n            <p class=\"very_small_text\">Cuisse droite</p>\n          </div>\n          <div class=\"small_card_desc\">\n         \n            <h4 class=\"medium_text_bold\">63</h4>\n            <ion-label class=\"small_text\"> cm</ion-label>\n          </div>\n        </ion-card>\n      </ion-col>\n\n      <ion-col size=\"3\">\n        <ion-card class=\"check-out-small-cards\">\n          <div class=\"small_card_title\">\n            <p class=\"very_small_text\">Biceps gauche</p>\n          </div>\n          <div class=\"small_card_desc\">\n         \n            <h4 class=\"medium_text_bold\">63</h4>\n            <ion-label class=\"small_text\"> cm</ion-label>\n          </div>\n        </ion-card>\n      </ion-col>\n\n      <ion-col size=\"3\">\n        <ion-card class=\"check-out-small-cards\">\n          <div class=\"small_card_title\">\n            <p class=\"very_small_text\">Avant bras ajit</p>\n          </div>\n          <div class=\"small_card_desc\">\n         \n            <h4 class=\"medium_text_bold\">63</h4>\n            <ion-label class=\"small_text\"> cm</ion-label>\n          </div>\n        </ion-card>\n      </ion-col>\n\n\n\n      <ion-col size=\"3\">\n        <ion-card class=\"check-out-small-cards\">\n          <div class=\"small_card_title\">\n            <p class=\"very_small_text\">Avant-bras gauche</p>\n          </div>\n          <div class=\"small_card_desc\">\n         \n            <h4 class=\"medium_text_bold\">63</h4>\n            <ion-label class=\"small_text\"> cm</ion-label>\n          </div>\n        </ion-card>\n      </ion-col>\n\n    </ion-row>\n\n\n\n    <ion-row>\n      <ion-col size=\"3\">\n        <ion-card class=\"check-out-small-cards\">\n          <div class=\"small_card_title\">\n            <p class=\"very_small_text\">Biceps droit</p>\n          </div>\n          <div class=\"small_card_desc\">\n         \n            <h4 class=\"medium_text_bold\">63</h4>\n            <ion-label class=\"small_text\"> cm</ion-label>\n          </div>\n        </ion-card>\n      </ion-col>\n\n      <ion-col size=\"3\">\n        <ion-card class=\"check-out-small-cards\">\n          <div class=\"small_card_title\">\n            <p class=\"very_small_text\">Biceps gauche</p>\n          </div>\n          <div class=\"small_card_desc\">\n         \n            <h4 class=\"medium_text_bold\">63</h4>\n            <ion-label class=\"small_text\"> cm</ion-label>\n          </div>\n        </ion-card>\n      </ion-col>\n\n      \n\n    </ion-row>\n\n\n    <ion-row class=\"ion-margin-top\">\n      <ion-col size=\"4\">\n        <ion-card\n        [class]=\"program\"\n        (click)=\"donsomething($event, src)\"\n      >\n        <ion-card-content>\n          <img [src]=\"morphotypeArray[0].src\" class=\"slide-img\" />\n        </ion-card-content>\n      </ion-card>\n      </ion-col>\n      <ion-col size=\"4\">\n        <ion-card\n        [class]=\"program\"\n        (click)=\"donsomething($event, src)\"\n      >\n        <ion-card-content>\n          <img [src]=\"morphotypeArray[1].src\" class=\"slide-img\" />\n        </ion-card-content>\n      </ion-card>\n      </ion-col>\n      <ion-col size=\"4\">\n        <ion-card\n        [class]=\"program\"\n        (click)=\"donsomething($event, src)\"\n      >\n        <ion-card-content>\n          <img [src]=\"morphotypeArray[2].src\" class=\"slide-img\" />\n        </ion-card-content>\n      </ion-card>\n      </ion-col>\n      <!-- <ion-col size=\"12\">\n        <ion-slides pager=\"false\" [options]=\"categories\" #slides>\n          <ion-slide *ngFor=\"let src of morphotypeArray\">\n            <ion-card\n            [class]=\"src.activeClass ? 'program active': 'program' \"\n              (click)=\"donsomething($event, src)\"\n            >\n              <ion-card-content>\n                <img [src]=\"src.src\" class=\"slide-img\" />\n              </ion-card-content>\n            </ion-card>\n          </ion-slide>\n        </ion-slides>\n      </ion-col> -->\n    </ion-row>\n\n\n\n\n  </ion-grid>\n</ion-content>";
      /***/
    },

    /***/
    "./node_modules/raw-loader/dist/cjs.js!./src/app/components/modals/coach-new-request/coach-new-request.component.html":
    /*!****************************************************************************************************************************!*\
      !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/components/modals/coach-new-request/coach-new-request.component.html ***!
      \****************************************************************************************************************************/

    /*! exports provided: default */

    /***/
    function node_modulesRawLoaderDistCjsJsSrcAppComponentsModalsCoachNewRequestCoachNewRequestComponentHtml(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony default export */


      __webpack_exports__["default"] = "<ion-toolbar class=\"ion-border\">\n  <ion-buttons slot=\"start\" (click)=\"closeModal()\">\n    <ion-button>\n      <ion-icon name=\"close-outline\" class=\"large_text_bold close-btn-icon\"></ion-icon>\n    </ion-button>\n  </ion-buttons>\n  <ion-text class=\"medium_text\">\n    NOUVELLE DEMANDE DE COACHING\n  </ion-text>\n</ion-toolbar>\n\n<ion-content>\n  <ion-grid>\n    <ion-row>\n      <ion-col size=\"12\">\n        <ion-item>\n          <ion-buttons class=\"ion-no-margin\" slot=\"start\">\n              <ion-back-button></ion-back-button>\n          </ion-buttons>\n          <ion-avatar slot=\"start\">\n              <img src=\"assets/icon/man.png\">\n          </ion-avatar>\n          <ion-label>\n              <h3>JEAN MICHEL ANDRAS</h3>\n              <p>Connecté</p>\n          </ion-label>\n      </ion-item>\n      </ion-col>\n    </ion-row>\n    <hr>\n    <ion-row>\n      <ion-col size=\"6\">\n        <ion-card class=\"ion-padding-end check-out-big-cards\">\n          <p  class=\"medium_text\">SEXE\n          </p>\n          <div>\n         \n            <h4 class=\"large_text_bold\">HOMME\n            </h4>\n          \n          </div>\n        </ion-card>\n      </ion-col>\n\n      <ion-col size=\"6\">\n        <ion-card class=\"ion-padding-end check-out-big-cards\">\n          <p class=\"medium_text\">AGE</p>\n          <div>\n         \n            <h4 class=\"large_text_bold\">26\n            </h4>\n           \n          </div>\n        </ion-card>\n      </ion-col>\n    </ion-row>\n    <ion-row>\n      <ion-col size=\"6\">\n        <ion-card class=\"ion-padding-end check-out-big-cards\">\n          <p  class=\"medium_text\">POIDS\n          </p>\n          <div>\n            <h4 class=\"large_text_bold\">88<span class=\"small_text\">kg</span>\n            </h4>\n          </div>\n        </ion-card>\n      </ion-col>\n\n      <ion-col size=\"6\">\n        <ion-card class=\"ion-padding-end check-out-big-cards\">\n          <p class=\"medium_text\">TAILLE</p>\n          <div>\n         \n            <h4 class=\"large_text_bold\">182<span class=\"small_text\">cm</span>\n            </h4>\n           \n          </div>\n        </ion-card>\n      </ion-col>\n    </ion-row>\n    <ion-row>\n      <ion-col size=\"6\">\n        <ion-card class=\"ion-padding-end check-out-big-cards\">\n          <p  class=\"medium_text\">OBJECTIF\n          </p>\n          <div>\n            <h4 class=\"large_text_bold\">PERTE DE POIDS\n            </h4>\n          </div>\n        </ion-card>\n      </ion-col>\n\n      <ion-col size=\"6\">\n        <ion-card class=\"ion-padding-end check-out-big-cards\">\n          <p class=\"medium_text\">REGIME ALIMENTAIRE</p>\n          <div>\n            <h4 class=\"large_text_bold\">AUCUN\n            </h4>\n          </div>\n        </ion-card>\n      </ion-col>\n    </ion-row>\n    <ion-row>\n      <ion-col size=\"6\">\n        <ion-card class=\"ion-padding-end check-out-big-cards\">\n          <p  class=\"medium_text\">ANTECEDENTS MEDICAUX\n          </p>\n          <div>\n            <h4 class=\"large_text_bold\">AUCUN\n            </h4>\n          </div>\n        </ion-card>\n      </ion-col>\n\n      <ion-col size=\"6\">\n        <ion-card class=\"ion-padding-end check-out-big-cards\">\n          <p class=\"medium_text\" style=\"text-align: right;\">INTOLERANCES ALIMENTAIRES</p>\n          <div>\n            <h4 class=\"large_text_bold\">LACTOSE\n            </h4>\n           \n          </div>\n        </ion-card>\n      </ion-col>\n    </ion-row>\n    <ion-row>\n      <ion-col size=\"6\">\n        <ion-card class=\"ion-padding-end check-out-big-cards\">\n          <p  class=\"medium_text\">MORPHOTYPE\n          </p>\n          <div>\n         \n            <h4 class=\"large_text_bold\">MESOMORPHE\n            </h4>\n          \n          </div>\n        </ion-card>\n      </ion-col>\n\n      <ion-col size=\"6\">\n        <ion-card class=\"ion-padding-end check-out-big-cards\">\n          <p class=\"medium_text\">TAUX DE MASSE GRASSE\n          </p>\n          <div>\n         \n            <h4 class=\"large_text_bold\">20%\n            </h4>\n           \n          </div>\n        </ion-card>\n      </ion-col>\n    </ion-row>\n  </ion-grid>\n  <div class=\"container_data\">\n    <ion-row>\n      <div class=\"profile-viewall\">\n          <ion-button (click)=\"openNewRequest()\">Historique des campagnes\n          </ion-button>\n      </div>\n  </ion-row>\n  </div>\n  <ion-row>\n    <ion-col size=\"12\" class=\"p-b-0 m-b-0\">\n        <ion-item class=\"height-40\" lines=\"none\">\n            <ion-avatar slot=\"start\">\n                <img src=\"assets/icon/masculation.png\" />\n            </ion-avatar>\n            <ion-label class=\"activity_title\">MUSCULATION</ion-label>\n        </ion-item>\n    </ion-col>\n    <ion-col size=\"12\" class=\"p-b-0 m-b-0\">\n        <ion-item class=\"height-40\" lines=\"none\">\n            <ion-avatar slot=\"start\">\n                <img src=\"assets/icon/natation.png\" />\n            </ion-avatar>\n            <ion-label class=\"activity_title \">NATATION</ion-label>\n        </ion-item>\n    </ion-col>\n    <ion-col size=\"12\" class=\"p-b-0 m-b-0\">\n        <ion-item class=\"height-40\" lines=\"none\">\n            <ion-avatar slot=\"start\">\n                <img src=\"assets/icon/football.png\" />\n            </ion-avatar>\n            <ion-label class=\"activity_title\">FOOTBALL </ion-label>\n        </ion-item>\n    </ion-col>\n</ion-row>\n</ion-content>\n";
      /***/
    },

    /***/
    "./node_modules/raw-loader/dist/cjs.js!./src/app/components/modals/courses-modal/courses-modal.component.html":
    /*!********************************************************************************************************************!*\
      !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/components/modals/courses-modal/courses-modal.component.html ***!
      \********************************************************************************************************************/

    /*! exports provided: default */

    /***/
    function node_modulesRawLoaderDistCjsJsSrcAppComponentsModalsCoursesModalCoursesModalComponentHtml(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony default export */


      __webpack_exports__["default"] = "<ion-toolbar class=\"ion-border\">\n  <ion-buttons slot=\"start\" (click)=\"closeModal()\">\n    <ion-button>\n      <ion-icon name=\"close-outline\" class=\"large_text_bold close-btn-icon\"></ion-icon>\n    </ion-button>\n  </ion-buttons>\n\n  <ion-text class=\"large_text_bold\"> MA LISTE DE COURSES</ion-text>\n</ion-toolbar>\n\n<ion-content>\n  <ion-grid>\n    <ion-row class=\"header_row\">\n      <ion-label class=\"medium_text_bold\"> LUNDI 4 SEPTEMBRE </ion-label>\n\n      <div class=\"dotted-line\"></div>\n      <ion-avatar>\n        <img src=\"assets/icon/close-icon.png\" alt=\"\" />\n      </ion-avatar>\n    </ion-row>\n\n    <ion-row class=\"large_card_row\">\n      <ion-card>\n        <ion-grid>\n          <ion-row>\n            <ion-col size=\"6\">\n              <ion-label class=\"medium_text\"> [Catégorie 1] </ion-label>\n            </ion-col>\n\n            <ion-col size=\"6\">\n              <div class=\"first_side_section\">\n                <ion-button class=\"small_text\">\n                  CATEGORIE\n                </ion-button>\n\n                <div class=\"circle\"></div>\n              </div>\n            </ion-col>\n          </ion-row>\n\n          <ion-row>\n            <ion-col size=\"6\">\n              <ion-label class=\"small_text\"> Blancs de poulet </ion-label>\n            </ion-col>\n\n            <ion-col size=\"6\">\n              <div class=\"second_side_section\">\n                <ion-button class=\"small_text second_btn\">\n                  ALIMENT\n\n                </ion-button>\n\n                <div class=\"second_circle\">\n                  <img src=\"assets/icon/knife-spoon.png\" alt=\"\" />\n                </div>\n              </div>\n            </ion-col>\n          </ion-row>\n\n          <ion-row>\n            <ion-col size=\"6\">\n              <ion-label class=\"small_text\"> Blancs de dinde </ion-label>\n            </ion-col>\n          </ion-row>\n\n          <ion-row>\n            <ion-col size=\"6\">\n              <ion-label class=\"small_text\"> Knackis </ion-label>\n            </ion-col>\n          </ion-row>\n\n          <ion-row>\n            <ion-col size=\"6\">\n              <ion-label class=\"small_text\"> Nuggets de poulet </ion-label>\n            </ion-col>\n          </ion-row>\n        </ion-grid>\n      </ion-card>\n    </ion-row>\n\n    <ion-row class=\"small_card_row ion-margin-top\">\n      <ion-card>\n        <ion-label class=\"medium_text_bold\">Catégorie 2 </ion-label>\n      </ion-card>\n    </ion-row>\n\n    <ion-row class=\"small_card_row ion-margin-top\">\n      <ion-card>\n        <ion-label class=\"medium_text_bold\"> Catégorie 3 </ion-label>\n      </ion-card>\n    </ion-row>\n  </ion-grid>\n</ion-content>\n";
      /***/
    },

    /***/
    "./node_modules/raw-loader/dist/cjs.js!./src/app/components/modals/manual-registration/manual-registration.component.html":
    /*!********************************************************************************************************************************!*\
      !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/components/modals/manual-registration/manual-registration.component.html ***!
      \********************************************************************************************************************************/

    /*! exports provided: default */

    /***/
    function node_modulesRawLoaderDistCjsJsSrcAppComponentsModalsManualRegistrationManualRegistrationComponentHtml(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony default export */


      __webpack_exports__["default"] = "<ion-toolbar class=\"ion-border\">\n  <ion-buttons slot=\"start\" (click)=\"closeModal()\">\n    <ion-button>\n      <ion-icon name=\"close-outline\"  class=\"large_text_bold close-btn-icon\"></ion-icon>\n    </ion-button>\n  </ion-buttons>\n\n  <ion-text class=\"large_text_bold\">NEAT – ENREGISTREMENT MANUEL\n  </ion-text>\n</ion-toolbar>\n\n<ion-content>\n  <ion-grid>\n    <ion-row class=\"small_card_row ion-margin-top\">\n      <ion-card>\n        <ion-card-content>\n          <ion-label class=\"small_text\">\n            Nombre de pas\n\n  \n          </ion-label>\n          <ion-label class=\"extra_large_text\">10250\n          </ion-label>\n        </ion-card-content>\n      </ion-card>\n    </ion-row>\n\n\n    <ion-row class=\"small_card_row ion-margin-top\">\n      <ion-card>\n        <ion-card-content>\n          <ion-label class=\"small_text\">\n            Nombre de pasDistance (km)\n\n          </ion-label>\n          <ion-label class=\"extra_large_text\">7,65\n          </ion-label>\n        </ion-card-content>\n      </ion-card>\n    </ion-row>\n\n\n\n    <ion-row class=\"big_card_row ion-margin-top\">\n      <ion-card>\n        <ion-card-content>\n\n          <ion-img src=\"assets/icon/run.png\"></ion-img>\n          <ion-label class=\"extra_large_text\">\n            550 kcal\n\n\n          </ion-label>\n         \n        </ion-card-content>\n      </ion-card>\n    </ion-row>\n\n\n\n  </ion-grid>\n</ion-content>\n";
      /***/
    },

    /***/
    "./node_modules/raw-loader/dist/cjs.js!./src/app/components/modals/my-devices-modal/my-devices-modal.component.html":
    /*!**************************************************************************************************************************!*\
      !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/components/modals/my-devices-modal/my-devices-modal.component.html ***!
      \**************************************************************************************************************************/

    /*! exports provided: default */

    /***/
    function node_modulesRawLoaderDistCjsJsSrcAppComponentsModalsMyDevicesModalMyDevicesModalComponentHtml(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony default export */


      __webpack_exports__["default"] = "<ion-toolbar class=\"ion-border\">\n  <ion-buttons slot=\"end\">\n    <ion-button  *ngIf=\"showRow == 'first'\">\n      <ion-img\n        src=\"assets/icon/add-bold.png\"\n        class=\"large_text_bold close-btn-icon\"\n      ></ion-img>\n    </ion-button>\n  </ion-buttons>\n\n  <ion-text class=\"large_text_bold ion-text-center\"> MES APPAREILS </ion-text>\n</ion-toolbar>\n\n<ion-content>\n  <!-- My Notebook-->\n  <ion-row class=\"ion-margin-top ion-margin-start\" *ngIf=\"showRow == 'first'\">\n    <ion-grid>\n      <ion-row class=\"list_device_row ion-margin-top\">\n        <ion-item lines=\"none\" (click)=\"showRow = 'second'\">\n          <div>\n            <ion-img src=\"assets/icon/starva.png\"></ion-img>\n          </div>\n          <ion-label class=\"list_section_title\">\n            <ion-label class=\"medium_text\"> Strava </ion-label>\n          </ion-label>\n          <ion-label class=\"menu-icon\">\n            <ion-icon name=\"ellipsis-vertical-outline\"></ion-icon>\n          </ion-label>\n        </ion-item>\n      </ion-row>\n\n      <ion-row class=\"list_device_row ion-margin-top\">\n        <ion-item lines=\"none\" class=\"ion-margin-top\">\n          <div>\n            <ion-img src=\"assets/icon/google-fit.png\"></ion-img>\n          </div>\n          <ion-label class=\"list_section_title\">\n            <ion-label class=\"medium_text\"> Google Fit </ion-label>\n          </ion-label>\n          <ion-label class=\"menu-icon\">\n            <ion-icon name=\"ellipsis-vertical-outline\"></ion-icon>\n          </ion-label>\n        </ion-item>\n      </ion-row>\n\n      <ion-row class=\"list_device_row ion-margin-top\">\n        <ion-item lines=\"none\" class=\"ion-margin-top\">\n          <div>\n            <ion-img src=\"assets/icon/adidas.png\"></ion-img>\n          </div>\n          <ion-label class=\"list_section_title\">\n            <ion-label class=\"medium_text\"> Adidas Running </ion-label>\n          </ion-label>\n          <ion-label class=\"menu-icon\">\n            <ion-icon name=\"ellipsis-vertical-outline\"></ion-icon>\n          </ion-label>\n        </ion-item>\n      </ion-row>\n    </ion-grid>\n  </ion-row>\n\n\n\n\n\n\n\n  <!--Second Section-->\n  <ion-row class=\"ion-margin-top logo_compare_section\" *ngIf=\"showRow == 'second'\">\n    <ion-grid class=\"ion-margin-top\">\n      <ion-row class=\"device_section ion-margin-bottom\">\n        <ion-img class=\"logo_one\" src=\"assets/icon/logo.png\"> </ion-img>\n\n        <div class=\"line\"></div>\n\n        <ion-img src=\"assets/icon/starva.png\"> </ion-img>\n      </ion-row>\n\n      <ion-row class=\"ion-margin description_section_one\">\n        <ion-label\n          class=\"small_text ion-margin-left ion-margin-top ion-margin-right ion-padding-start ion-padding-end\"\n        >\n          Connectez votre compte Strava à Transformatics pour partager vos\n          activités\n        </ion-label>\n      </ion-row>\n\n      <ion-row class=\"ion-margin-top device_compare_body_section\">\n     \n          <ion-label class=\"ion-margin-top\"> SE CONNECTER AVEC STRAVA ? </ion-label>\n    \n          <ion-label class=\"ion-margin-top small_text\">\n            Cela signifie que vos données personnelles de Transformatics seront\n            échangées avec Strava (géolocalisation, calories brûlées, …) afin de\n            permettre une compréhension optimale de vos caractéristiques\n            physiologiques durant un effort.\n          </ion-label>\n\n          <ion-label class=\"ion-margin-top small_text\">\n            En cliquant sur le bouton ci-dessous, vous acceptez la politique de\n            confidentialité de Strava et Transformatics.\n          </ion-label>\n        </ion-row>\n    </ion-grid>\n  </ion-row>\n</ion-content>\n\n\n<ion-footer class=\"ion-padding ion-no-border\">\n  <ion-button>\n    {{\n      showRow == \"first\"\n        ? \"Connecter un nouvel appareil\" : \"Connecter Strava\"\n    }}\n\n  </ion-button>\n</ion-footer>\n";
      /***/
    },

    /***/
    "./node_modules/raw-loader/dist/cjs.js!./src/app/components/modals/my-diet-home-modal/my-diet-home-modal.component.html":
    /*!******************************************************************************************************************************!*\
      !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/components/modals/my-diet-home-modal/my-diet-home-modal.component.html ***!
      \******************************************************************************************************************************/

    /*! exports provided: default */

    /***/
    function node_modulesRawLoaderDistCjsJsSrcAppComponentsModalsMyDietHomeModalMyDietHomeModalComponentHtml(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony default export */


      __webpack_exports__["default"] = "<ion-toolbar>\n  <ion-text class=\"large_text_bold ion-text-center\"> MA DIETE </ion-text>\n</ion-toolbar>\n\n\n<ion-content class=\"ion-padding\">\n  <ion-row class=\"ion-margin-top\">\n    <ion-label class=\"small_text_bold\"> OBJECTIFS SPORTIFS\n    </ion-label>\n  </ion-row>\n\n  <ion-row class=\"ion-margin-top\">\n    <ion-label class=\"small_text\">\n      Quel est votre objectif physique et sportif ? Choisissez l’un des\n      objectifs suivants et sélectionnez un niveau de suivi :\n    </ion-label>\n  </ion-row>\n\n  <ion-row class=\"plan_btn_section ion-margin-top\">\n    <ion-col size=\"4\" class=\"first_col\">\n      <div class=\"steptwofirstbtn\">\n        <ion-button\n          [color]=\"plansBtn == 'Gain muscle' ? 'primary' : 'light'\"\n          expand=\"full\"\n          (click)=\"plansBtn = 'Gain muscle'\"\n          >Prendre du muscle\n\n        </ion-button>\n      </div>\n    </ion-col>\n    <ion-col size=\"4\">\n      <div class=\"steptwofirstbtn\">\n        <ion-button\n          [color]=\"plansBtn == 'detail_section' ? 'primary' : 'light'\"\n          expand=\"full\"\n          (click)=\"plansBtn = 'detail_section'\"\n          >Perdre du poids\n\n        </ion-button>\n      </div>\n    </ion-col>\n    <ion-col size=\"4\">\n      <div class=\"steptwofirstbtn\">\n        <ion-button\n          [color]=\"plansBtn == 'free_session' ? 'primary' : 'light'\"\n          expand=\"full\"\n          expand=\"full\"\n          (click)=\"plansBtn = 'free_session'\"\n          >Se maintenir\n\n        </ion-button>\n      </div>\n    </ion-col>\n  </ion-row>\n\n\n\n  <ion-row class=\"ion-margin-top\">\n    <ion-label class=\"small_text ion-margin-top\">\n      Choisissez cet objectif si votre priorité est de développer votre masse musculaire, d’avoir l’air plus massif et d’être plus fort. \n\n    </ion-label>\n  </ion-row>\n\n\n  <ion-row class=\"ion-margin-top\">\n    <ion-label class=\"small_text\"> Rythme de prise de muscle\n    </ion-label>\n  </ion-row>\n\n  <ion-row class=\"plan_btn_section ion-margin-top\">\n    <ion-col size=\"4\" class=\"first_col\">\n      <div class=\"steptwofirstbtn\">\n        <ion-button\n          [color]=\"creationMode == 'deux' ? 'primary' : 'light'\"\n          expand=\"full\"\n          (click)=\"creationMode = 'deux'\"\n          >Doux\n        </ion-button>\n      </div>\n    </ion-col>\n    <ion-col size=\"4\">\n      <div class=\"steptwofirstbtn\">\n        <ion-button\n          [color]=\"creationMode == 'mode' ? 'primary' : 'light'\"\n          expand=\"full\"\n          (click)=\"creationMode = 'mode'\"\n          >Modéré\n        </ion-button>\n      </div>\n    </ion-col>\n\n    <ion-col size=\"4\">\n      <div class=\"steptwofirstbtn\">\n        <ion-button\n          [color]=\"creationMode == 'fort' ? 'primary' : 'light'\"\n          expand=\"full\"\n          (click)=\"creationMode = 'fort'\"\n          >Fort\n\n        </ion-button>\n      </div>\n    </ion-col>\n\n  </ion-row>\n</ion-content>\n\n<ion-footer class=\"ion-padding ion-no-border\">\n  <ion-button>\n    Enregistrer\n  </ion-button>\n</ion-footer>\n";
      /***/
    },

    /***/
    "./node_modules/raw-loader/dist/cjs.js!./src/app/components/modals/my-diet-modal/my-diet-modal.component.html":
    /*!********************************************************************************************************************!*\
      !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/components/modals/my-diet-modal/my-diet-modal.component.html ***!
      \********************************************************************************************************************/

    /*! exports provided: default */

    /***/
    function node_modulesRawLoaderDistCjsJsSrcAppComponentsModalsMyDietModalMyDietModalComponentHtml(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony default export */


      __webpack_exports__["default"] = "<ion-toolbar class=\"ion-border\">\n  <ion-buttons slot=\"start\" (click)=\"closeModal()\">\n    <ion-button>\n      <ion-icon name=\"close-outline\" class=\"large_text_bold close-btn-icon\"></ion-icon>\n    </ion-button>\n  </ion-buttons>\n\n  <ion-text class=\"large_text_bold ion-text-center\"> MA DIETE </ion-text>\n</ion-toolbar>\n\n<ion-content>\n  <ion-grid>\n    <ion-row class=\"header_row\">\n      <ion-label class=\"medium_text_bold\"> LUNDI 4 SEPTEMBRE </ion-label>\n\n      <div class=\"dotted-line\"></div>\n      <ion-avatar>\n        <img src=\"assets/icon/pen-icon.png\" alt=\"\" />\n      </ion-avatar>\n    </ion-row>\n\n    <ion-row>\n      <ion-item lines=\"none\" class=\"toggle_btn_section\">\n        <ion-label>Utiliser comme diète par défaut </ion-label>\n        <ion-toggle></ion-toggle>\n      </ion-item>\n    </ion-row>\n\n    <ion-row>\n      <ion-col size=\"2\">\n        <ion-label class=\"very_small_text\"> PROTEINES </ion-label>\n      </ion-col>\n\n      <ion-col size=\"2\">\n        <div class=\"main_line_section\">\n          <div class=\"yellow-back\"></div>\n          <div class=\"black-back\"></div>\n        </div>\n      </ion-col>\n\n      <ion-col size=\"2\">\n        <ion-label class=\"very_small_text\"> LIPIDES </ion-label>\n      </ion-col>\n\n      <ion-col size=\"2\">\n          <div class=\"main_line_section\">\n          <div class=\"yellow-back\"></div>\n          <div class=\"black-back\"></div>\n        </div>\n      </ion-col>\n\n      <ion-col size=\"2\">\n        <ion-label class=\"very_small_text\"> LIPIDES </ion-label>\n      </ion-col>\n      <ion-col size=\"2\">\n          <div class=\"main_line_section\">\n          <div class=\"yellow-back\"></div>\n          <div class=\"black-back\"></div>\n        </div>\n      </ion-col>\n    </ion-row>\n\n\n    <ion-row class=\"card_item_section ion-margin-top ion-margin-bottom\">\n      <ion-col size=\"6\">\n        <ion-card>\n          <ion-card-header>\n            <ion-button>\n              Petit-Déjeuner\n  \n            </ion-button>\n          </ion-card-header>\n\n          <ion-card-content>\n              <ion-img src=\"assets/icon/tick.png\"></ion-img>\n          </ion-card-content>\n        </ion-card>\n\n        <div class=\"card_footer_data ion-margin-top\">\n          <p class=\"medium_text\">\n            398 kcal\n\n          </p>\n\n          <p class=\"small_text\">\n            Recommandé : 667kcal\n\n          </p>\n        </div>\n\n\n      </ion-col>\n\n      <ion-col size=\"6\">\n        <ion-card>\n          <ion-card-header>\n            <ion-button>\n              Petit-Déjeuner\n  \n            </ion-button>\n          </ion-card-header>\n\n          <ion-card-content>\n              <ion-img src=\"assets/icon/add.png\"></ion-img>\n              <p>Ajouter</p>\n          </ion-card-content>\n        </ion-card>\n\n        <div class=\"card_footer_data ion-margin-top\">\n          <p class=\"medium_text\">\n            398 kcal\n\n          </p>\n\n          <p class=\"small_text\">\n            Recommandé : 667kcal\n\n          </p>\n        </div>\n      </ion-col>\n\n\n    </ion-row>\n\n\n    <ion-row class=\"card_item_section ion-margin-top\">\n      <ion-col size=\"6\">\n        <ion-card>\n          <ion-card-header>\n            <ion-button>\n              Petit-Déjeuner\n  \n            </ion-button>\n          </ion-card-header>\n\n          <ion-card-content>\n              <ion-img src=\"assets/icon/tick.png\"></ion-img>\n          </ion-card-content>\n        </ion-card>\n\n        <div class=\"card_footer_data ion-margin-top\">\n          <p class=\"medium_text\">\n            398 kcal\n\n          </p>\n\n          <p class=\"small_text\">\n            Recommandé : 667kcal\n\n          </p>\n        </div>\n\n        \n      </ion-col>\n\n      <ion-col size=\"6\">\n        <ion-card>\n          <ion-card-header>\n            <ion-button>\n              Petit-Déjeuner\n  \n            </ion-button>\n          </ion-card-header>\n\n          <ion-card-content>\n              <ion-img src=\"assets/icon/add.png\"></ion-img>\n              <p>Ajouter</p>\n          </ion-card-content>\n        </ion-card>\n\n        <div class=\"card_footer_data ion-margin-top\">\n          <p class=\"medium_text\">\n            398 kcal\n\n          </p>\n\n          <p class=\"small_text\">\n            Recommandé : 667kcal\n\n          </p>\n        </div>\n      </ion-col>\n\n\n    </ion-row>\n\n  </ion-grid>\n</ion-content>\n";
      /***/
    },

    /***/
    "./node_modules/raw-loader/dist/cjs.js!./src/app/components/modals/notebook-modal/notebook-modal.component.html":
    /*!**********************************************************************************************************************!*\
      !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/components/modals/notebook-modal/notebook-modal.component.html ***!
      \**********************************************************************************************************************/

    /*! exports provided: default */

    /***/
    function node_modulesRawLoaderDistCjsJsSrcAppComponentsModalsNotebookModalNotebookModalComponentHtml(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony default export */


      __webpack_exports__["default"] = "<ion-toolbar class=\"ion-border\">\n  <ion-buttons\n    slot=\"end\"\n    (click)=\"closeModal()\"\n    *ngIf=\"showRow == 'first' || showRow == 'third'\"\n  >\n    <ion-button>\n      <ion-img\n        src=\"assets/icon/more_notebook.png\"\n        class=\"large_text_bold close-btn-icon\"\n      ></ion-img>\n    </ion-button>\n  </ion-buttons>\n\n  <ion-text class=\"large_text_bold ion-text-center\">\n    {{\n      showRow == \"first\"\n        ? \"MON CARNET\"\n        : showRow == \"second\"\n        ? \"PERSONNALISER MON CARNET\"\n        : \"MON CARNET\"\n    }}\n  </ion-text>\n</ion-toolbar>\n\n<ion-content>\n  \n  <!-- My Notebook-->\n  <ion-row *ngIf=\"showRow == 'first'\">\n    <ion-grid>\n      <ion-row class=\"header_row ion-margin-top\">\n        <ion-label class=\"medium_text_bold\"> LUNDI 4 SEPTEMBRE </ion-label>\n      </ion-row>\n\n      <ion-row class=\"ion-margin-top note_book_list_row\">\n        <ion-col size=\"12\">\n          <ion-item lines=\"none\" >\n            <ion-avatar>\n              <ion-img src=\"assets/icon/tick-plain.png\"></ion-img>\n            </ion-avatar>\n            <ion-label class=\"list_section_title\">\n              <ion-label class=\"medium_text\"> Vacuum </ion-label>\n              <ion-label class=\"small_text_light\"> 10 minutes </ion-label>\n            </ion-label>\n          </ion-item>\n        </ion-col>\n\n        <ion-col size=\"12\">\n          <ion-item lines=\"none\">\n            <ion-avatar>\n              <ion-img src=\"assets/icon/tick-plain.png\"></ion-img>\n            </ion-avatar>\n            <ion-label class=\"list_section_title\">\n              <ion-label class=\"medium_text\"> Méditation </ion-label>\n              <ion-label class=\"small_text_light\"> 20 minutes </ion-label>\n            </ion-label>\n          </ion-item>\n        </ion-col>\n\n        <ion-col size=\"12\">\n          <ion-item lines=\"none\">\n            <ion-avatar>\n              <ion-img src=\"assets/icon/close-faded.png\"></ion-img>\n            </ion-avatar>\n            <ion-label class=\"list_section_title\">\n              <ion-label class=\"medium_text_light\">\n                Jeune intermittent\n              </ion-label>\n            </ion-label>\n          </ion-item>\n        </ion-col>\n      </ion-row>\n\n      <ion-row class=\"ion-margin-top\">\n        <div class=\"profile-viewall\">\n          <ion-button\n            >VOIR LES QUESTIONNAIRES DES JOURS PRECEDENTS\n\n            <ion-icon name=\"chevron-forward-outline\"></ion-icon>\n          </ion-button>\n        </div>\n      </ion-row>\n\n      <ion-row class=\"ion-margin-top ion-margin-bottom\" *ngIf=\"showRow == 'first'\">\n        <ion-button expand=\"full\" class=\"validate_btn\" (click)=\"showRow = 'third'\">\n          Lancer le questionnaire du jour\n        </ion-button>\n      </ion-row>\n   \n    </ion-grid>\n\n    \n\n  </ion-row>\n\n  <!-- CARNET SUITE -->\n\n  <ion-grid class=\"ion-margin-top list_section_row\" *ngIf=\"showRow == 'second'\">\n      <ion-row class=\"row-padding-left\">\n        <ion-col size=\"10\">\n          <ion-item lines=\"none\">\n            <ion-label class=\"list_section_title\">\n              <ion-label class=\"medium_text\"> Vacuum </ion-label>\n              <ion-label class=\"medium_text_light\"> 10 minutes </ion-label>\n            </ion-label>\n          </ion-item>\n        </ion-col>\n\n        <ion-col size=\"2\" class=\"tick_col\">\n          <div class=\"tick_icon_background\">\n            <ion-icon name=\"checkmark-outline\"></ion-icon>\n          </div>\n        </ion-col>\n      </ion-row>\n\n      <ion-row class=\"list_section_row row-padding-left\">\n        <ion-col size=\"10\">\n          <ion-item lines=\"none\">\n            <ion-label class=\"list_section_title\">\n              <ion-label class=\"medium_text\"> Méditation </ion-label>\n              <ion-label class=\"medium_text_light\">\n                Evacuation du stress\n              </ion-label>\n            </ion-label>\n          </ion-item>\n        </ion-col>\n\n        <ion-col size=\"2\" class=\"tick_col\">\n          <div class=\"tick_icon_background\">\n            <ion-icon name=\"checkmark-outline\"></ion-icon>\n          </div>\n        </ion-col>\n      </ion-row>\n\n      <!-- Add Rows -->\n      <ion-row class=\"list_section_row row-padding-left\">\n        <ion-col size=\"10\">\n          <ion-item lines=\"none\">\n            <ion-label class=\"list_section_title\">\n              <ion-label class=\"medium_text\"> Jeune intermittent </ion-label>\n              <ion-label class=\"medium_text_light\"> Alimentation </ion-label>\n            </ion-label>\n          </ion-item>\n        </ion-col>\n\n        <ion-col size=\"2\" class=\"tick_col\">\n          <div class=\"tick_icon_background_add\">\n            <ion-img src=\"assets/icon/add.png\"></ion-img>\n          </div>\n        </ion-col>\n      </ion-row>\n\n      <ion-row class=\"list_section_row row-padding-left\">\n        <ion-col size=\"10\">\n          <ion-item lines=\"none\">\n            <ion-label class=\"list_section_title\">\n              <ion-label class=\"medium_text\"> Alcool </ion-label>\n              <ion-label class=\"medium_text_light\">\n                Peu recommandable pour un sportif !\n              </ion-label>\n            </ion-label>\n          </ion-item>\n        </ion-col>\n\n        <ion-col size=\"2\" class=\"tick_col\">\n          <div class=\"tick_icon_background_add\">\n            <ion-img src=\"assets/icon/add.png\"></ion-img>\n          </div>\n        </ion-col>\n      </ion-row>\n      <ion-row class=\"list_section_row\">\n        <ion-col size=\"10\">\n          <ion-item lines=\"none\">\n            <ion-label class=\"list_section_title\">\n              <ion-label class=\"medium_text\"> Fumer </ion-label>\n              <ion-label class=\"medium_text_light\">\n                Peu recommandable pour un sportif !\n              </ion-label>\n            </ion-label>\n            <ion-reorder slot=\"end\"></ion-reorder>\n          </ion-item>\n        </ion-col>\n\n        <ion-col size=\"2\" class=\"tick_col\">\n          <div class=\"tick_icon_background_add\">\n            <ion-img src=\"assets/icon/add.png\"></ion-img>\n          </div>\n        </ion-col>\n      </ion-row>\n      <ion-row class=\"list_section_row\">\n        <ion-col size=\"10\">\n          <ion-item lines=\"none\">\n            <ion-label class=\"list_section_title\">\n              <ion-label class=\"medium_text\"> Marcher  </ion-label>\n              <ion-label class=\"medium_text_light\">\n                activité physique !\n              </ion-label>\n            </ion-label>\n            <ion-reorder slot=\"end\"></ion-reorder>\n          </ion-item>\n        </ion-col>\n\n        <ion-col size=\"2\" class=\"tick_col\">\n          <div class=\"tick_icon_background_add\">\n            <ion-img src=\"assets/icon/add.png\"></ion-img>\n          </div>\n        </ion-col>\n      </ion-row>\n      <ion-row class=\"list_section_row\">\n        <ion-col size=\"10\">\n          <ion-item lines=\"none\">\n            <ion-label class=\"list_section_title\">\n              <ion-label class=\"medium_text\">Souplesse ou étirements</ion-label>\n              <ion-label class=\"medium_text_light\">\n                activité physique !\n              </ion-label>\n            </ion-label>\n            <ion-reorder slot=\"end\"></ion-reorder>\n          </ion-item>\n        </ion-col>\n\n        <ion-col size=\"2\" class=\"tick_col\">\n          <div class=\"tick_icon_background_add\">\n            <ion-img src=\"assets/icon/add.png\"></ion-img>\n          </div>\n        </ion-col>\n      </ion-row>\n      <ion-row class=\"list_section_row\">\n        <ion-col size=\"10\">\n          <ion-item lines=\"none\">\n            <ion-label class=\"list_section_title\">\n              <ion-label class=\"medium_text\"> Consommer 5 fruits et légumes   </ion-label>\n              <ion-label class=\"medium_text_light\">\n                alimentation\n              </ion-label>\n            </ion-label>\n            <ion-reorder slot=\"end\"></ion-reorder>\n          </ion-item>\n        </ion-col>\n\n        <ion-col size=\"2\" class=\"tick_col\">\n          <div class=\"tick_icon_background_add\">\n            <ion-img src=\"assets/icon/add.png\"></ion-img>\n          </div>\n        </ion-col>\n      </ion-row>\n      <ion-row class=\"ion-margin-top\">\n        <div class=\"profile-viewall\">\n          <ion-button (click)=\"showRow = 'third'\">\n            VOIR TOUS LES THEMES\n\n            <ion-icon name=\"chevron-forward-outline\"></ion-icon>\n          </ion-button>\n        </div>\n      </ion-row>\n\n      <ion-row class=\"ion-margin-top card_data_section row-padding-left\">\n        <ion-card mode=\"ios\" class=\"card-box\">\n          <ion-card-header>\n            <ion-item lines=\"none\">\n              <ion-avatar>\n                <ion-img\n                  src=\"assets/icon/transformatics_small_logo.png\"\n                ></ion-img>\n              </ion-avatar>\n              <ion-label class=\"list_section_title\">\n                <ion-label class=\"medium_text\">\n                  NOTE SUR LES THEMES DU CARNET\n                </ion-label>\n              </ion-label>\n            </ion-item>\n          </ion-card-header>\n\n          <ion-card-content>\n            <ion-label class=\"small_text\">\n              Afin de créer un carnet riche en informations, sélectionnez les\n              différents thèmes que vous souhaitez afficher en récurrence sur\n              votre carnet. Ceux-ci seront directement intégrés dans votre base\n              de données et vous permettra de les sélectionner plus rapidement\n              et de gagner du temps !\n            </ion-label>\n          </ion-card-content>\n\n          <div class=\"ribbon-wrapper-1\">\n            <div class=\"ribbon-1\">Ribbon</div>\n        </div>\n        \n        </ion-card>\n      </ion-row>\n\n  </ion-grid>\n\n  <!--  CARNET SUITE-->\n  <ion-row\n    class=\"my_note_row_two_section ion-margin\"\n    *ngIf=\"showRow == 'third'\"\n  >\n    <ion-grid>\n      <ion-row>\n        <ion-label> Hier, avez-vous… </ion-label>\n      </ion-row>\n       <ion-row>\n        <p class=\"small_text_light\">\n          Swipez vers la droite la ou gauche pour répondre\n        </p>\n       </ion-row>\n       \n      \n      \n      <ion-row>\n        <app-swiper-card [cards]=\"cards\" (choiceMade)=\"logChoice($event)\"></app-swiper-card>\n      </ion-row>\n    </ion-grid>\n  </ion-row>\n</ion-content>\n";
      /***/
    },

    /***/
    "./node_modules/raw-loader/dist/cjs.js!./src/app/components/modals/payment-modal/payment-modal.component.html":
    /*!********************************************************************************************************************!*\
      !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/components/modals/payment-modal/payment-modal.component.html ***!
      \********************************************************************************************************************/

    /*! exports provided: default */

    /***/
    function node_modulesRawLoaderDistCjsJsSrcAppComponentsModalsPaymentModalPaymentModalComponentHtml(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony default export */


      __webpack_exports__["default"] = "<ion-content>\n    <ion-toolbar>\n        <ion-title>Paiement</ion-title>\n    </ion-toolbar>\n    <ion-item>\n        <img class=\"ion-no-margin ion-margin-end\" style=\"width: 40px;\" slot=\"start\" src=\"assets/icon/logo.png\">\n        <ion-label>\n            <h3>Monthly</h3>\n            <p class=\"f12\">OWNUt</p>\n        </ion-label>\n    </ion-item>\n    <ion-grid>\n        <ion-row>\n            <ion-col size=\"12\">\n                <div class=\"timeline-box-group\">\n                    <div class=\"timeline-box\">\n                        <div class=\"timeline-badge \"></div>\n                        <div class=\"timeline-body\">\n                            <div>lorenipsom:domy text</div>\n                            <div class=\"ion-text-right\">lorenipsom:domy text</div>\n                        </div>\n                    </div>\n                    <div class=\"timeline-box\">\n                        <div class=\"timeline-badge \"></div>\n                        <div class=\"timeline-body\">\n                            <div>20 nov 2020</div>\n                            <div class=\"ion-text-right\">17,26 $US/mois</div>\n                        </div>\n                    </div>\n                </div>\n            </ion-col>\n            <ion-col size=\"12\">\n                <p class=\"ion-no-margin f12\">Vous bénéficiez des 15 premiers jours d’utilisation offerts. Cela signifie que votre premier prélèvement sera effectué dans 45 jours, c’est-à-dire un mois après la fin de vos 15 jours gratuits.\n                </p>\n            </ion-col>\n            <ion-col size=\"12\">\n                <h4 class=\"ion-no-margin f14 ion-margin-top\">Sélection de la méthode de paiement</h4>\n            </ion-col>\n            <ion-col size=\"12\">\n                <img src=\"assets/icon/payment.png\">\n            </ion-col>\n            <ion-col size=\"12\">\n                <p class=\"ion-no-margin f12 color-gray\">Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen\n                    book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and\n                    more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>\n            </ion-col>\n\n        </ion-row>\n    </ion-grid>\n</ion-content>\n<ion-footer class=\"ion-no-border\">\n    <ion-button class=\"payment-btn ion-margin-start ion-margin-end ion-margin-bottom\" shape=\"round\" expand=\"full\">Sabonner</ion-button>\n</ion-footer>";
      /***/
    },

    /***/
    "./node_modules/raw-loader/dist/cjs.js!./src/app/components/modals/recipes-filters/recipes-filters.component.html":
    /*!************************************************************************************************************************!*\
      !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/components/modals/recipes-filters/recipes-filters.component.html ***!
      \************************************************************************************************************************/

    /*! exports provided: default */

    /***/
    function node_modulesRawLoaderDistCjsJsSrcAppComponentsModalsRecipesFiltersRecipesFiltersComponentHtml(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony default export */


      __webpack_exports__["default"] = "<ion-content>\n    <div>\n        <ion-item>\n            <img class=\"logo ion-no-margin ion-margin-end\" slot=\"start\" src=\"assets/icon/repascomplets.png\">\n            <ion-label>REPAS COMPLETS\n            </ion-label>\n            <ion-icon (click)=\"closeModal()\" slot=\"end\" name=\"close-outline\"></ion-icon>\n        </ion-item>\n        <ion-grid>\n            <ion-row>\n                <ion-col size=\"12\">\n                    <div class=\"recipes\">\n                        <div>VIANDES</div>\n                        <div>POISSONS</div>\n                        <div>PATES</div>\n                        <div>BURGERS, SANDWICHS, WRAPS</div>\n                        <div>VEGAN</div>\n                        <div>SALADES</div>\n                        <div>PIZZAS</div>\n                        <div>SOUPES</div>\n                        <div>BASE D’ŒUF</div>\n                        <div>ACCOMPAGNEMENTS</div>\n                        <div>ENTREES</div>\n                    </div>\n                </ion-col>\n            </ion-row>\n        </ion-grid>\n    </div>\n    <div>\n        <ion-item>\n            <img class=\"logo ion-no-margin ion-margin-end\" slot=\"start\" src=\"assets/icon/snacks.png\">\n            <ion-label>DESSERTS, SNACKS & COLLATIONS</ion-label>\n        </ion-item>\n        <ion-grid>\n            <ion-row>\n                <ion-col size=\"12\">\n                    <div class=\"recipes\">\n                        <div>VIANDES</div>\n                        <div>POISSONS</div>\n                        <div>PATES</div>\n                        <div>BURGERS, SANDWICHS, WRAPS</div>\n                        <div>VEGAN</div>\n                        <div>SALADES</div>\n                        <div>PIZZAS</div>\n                        <div>SOUPES</div>\n                        <div>BASE D’ŒUF</div>\n                        <div>ACCOMPAGNEMENTS</div>\n                        <div>ENTREES</div>\n                    </div>\n                </ion-col>\n            </ion-row>\n        </ion-grid>\n    </div>\n    <div>\n        <ion-item>\n            <img class=\"logo ion-no-margin ion-margin-end\" slot=\"start\" src=\"assets/icon/cup.png\">\n            <ion-label>REPAS COMPLETS\n            </ion-label>\n        </ion-item>\n        <ion-grid>\n            <ion-row>\n                <ion-col size=\"12\">\n                    <div class=\"recipes\">\n                        <div>VIANDES</div>\n                        <div>POISSONS</div>\n                        <div>PATES</div>\n                        <div>BURGERS, SANDWICHS, WRAPS</div>\n                        <div>VEGAN</div>\n                        <div>SALADES</div>\n                        <div>PIZZAS</div>\n                        <div>SOUPES</div>\n                        <div>BASE D’ŒUF</div>\n                        <div>ACCOMPAGNEMENTS</div>\n                        <div>ENTREES</div>\n                    </div>\n                </ion-col>\n            </ion-row>\n        </ion-grid>\n    </div>\n</ion-content>";
      /***/
    },

    /***/
    "./node_modules/raw-loader/dist/cjs.js!./src/app/components/modals/sponsership-modal/sponsership-modal.component.html":
    /*!****************************************************************************************************************************!*\
      !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/components/modals/sponsership-modal/sponsership-modal.component.html ***!
      \****************************************************************************************************************************/

    /*! exports provided: default */

    /***/
    function node_modulesRawLoaderDistCjsJsSrcAppComponentsModalsSponsershipModalSponsershipModalComponentHtml(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony default export */


      __webpack_exports__["default"] = "<ion-toolbar class=\"ion-border\">\n  <ion-text class=\"large_text_bold ion-text-center\"> PARRAINAGE </ion-text>\n</ion-toolbar>\n\n<ion-content class=\"ion-padding\">\n  <!-- My Notebook-->\n  <ion-row *ngIf=\"showRow == 'first'\">\n    <ion-segment\n      value=\"principle\"\n      color=\"tertiary\"\n      scrollable=\"false\"\n      [(ngModel)]=\"segmentModel\"\n      (ionChange)=\"segmentChanged($event)\"\n    >\n      <ion-segment-button value=\"principle\">\n        <ion-label>PRINCIPE</ion-label>\n      </ion-segment-button>\n\n      <ion-segment-button class=\"second-second-btn\" value=\"history\">\n        <ion-label>HISTORIQUE</ion-label>\n      </ion-segment-button>\n    </ion-segment>\n\n    <ion-row class=\"ion-margin-top\">\n      <div *ngIf=\"segmentModel === 'principle'\" class=\"principle_section\">\n        <ion-label class=\"small_text_bold\">\n          CUMULEZ DES MOIS GRATUITS EN PARRAINANT VOS AMIS\n        </ion-label>\n\n        <ion-img src=\"assets/icon/principle_segment_section.png\"> </ion-img>\n\n        <ion-label class=\"small_text\">\n          Avec le système de parrainage de Transformatics, vous avez la\n          possibilité d’inviter vos amis à s’inscrire sur l’application et à\n          souscrire à une formule d’abonnement.\n        </ion-label>\n\n        <ion-row class=\"ion-margin-top numbered_data_section\">\n          <ion-col size=\"1\">\n            <ion-label> 1 </ion-label>\n          </ion-col>\n\n          <ion-col size=\"11\" class=\"text_desc\">\n            <ion-label class=\"small_text\">\n              Vous entrez le mail de la personne que vous souhaitez inviter\n            </ion-label>\n          </ion-col>\n        </ion-row>\n\n        <ion-row class=\"numbered_data_section\">\n          <ion-col size=\"1\">\n            <ion-label> 2 </ion-label>\n          </ion-col>\n\n          <ion-col size=\"11\" class=\"text_desc\">\n            <ion-label class=\"small_text\">\n              Votre ami s’inscrit sur Transformatics\n            </ion-label>\n          </ion-col>\n        </ion-row>\n\n        <ion-row class=\"numbered_data_section\">\n          <ion-col size=\"1\">\n            <ion-label> 3 </ion-label>\n          </ion-col>\n\n          <ion-col size=\"11\" class=\"text_desc\">\n            <ion-label class=\"small_text_bold\">\n              Vous obtenez un mois gratuit, et votre ami aussi !\n            </ion-label>\n          </ion-col>\n\n          <ion-col size=\"12\" class=\"desc_text\">\n            <ion-label class=\"small_text\">\n              Commencez dès maintenant en remplissant la ligne ci-dessous :  \n            </ion-label>\n          </ion-col>\n        </ion-row>\n\n        <ion-row class=\"item_white_section ion-margin-top\">\n          <ion-card mode=\"ios\">\n            <ion-card-content>\n              <ion-label class=\"small_text_light\">\n                nicolas.martin1997@yahoo.fr\n              </ion-label>\n\n              <div class=\"tick_icon_background\">\n                <ion-icon name=\"checkmark-outline\"></ion-icon>\n              </div>\n            </ion-card-content>\n          </ion-card>\n        </ion-row>\n\n        <ion-row class=\"item_white_section ion-margin-top\">\n          <ion-card mode=\"ios\">\n            <ion-card-content>\n              <ion-label class=\"small_text_light\">\n                Entrez ici l’adresse mail de l’ami que vous souhaitez parrainer…\n              </ion-label>\n            </ion-card-content>\n          </ion-card>\n        </ion-row>\n      </div>\n\n      <div *ngIf=\"segmentModel === 'history'\" class=\"history_section\">\n        <ion-row class=\"item_white_section ion-margin-top\">\n          <ion-card>\n            <ion-card-content>\n              <ion-label> 22/11/2021 </ion-label>\n\n              <ion-label class=\"small_text_light\">\n                nicolas.martin1997@yahoo.fr\n              </ion-label>\n\n              <div class=\"main_tick_div\">\n                <ion-label> applied </ion-label>\n\n                <div class=\"tick_icon_background\">\n                  <ion-icon name=\"checkmark-outline\"></ion-icon>\n                </div>\n              </div>\n            </ion-card-content>\n          </ion-card>\n        </ion-row>\n\n        <ion-row class=\"item_white_section ion-margin-top\">\n          <ion-card>\n            <ion-card-content>\n              <ion-label> 22/11/2021 </ion-label>\n\n              <ion-label class=\"small_text_light\">\n                nicolas.martin1997@yahoo.fr\n              </ion-label>\n\n              <div class=\"main_tick_div\">\n                <ion-label> applied </ion-label>\n\n                <div class=\"tick_icon_background\">\n                  <ion-icon name=\"checkmark-outline\"></ion-icon>\n                </div>\n              </div>\n            </ion-card-content>\n          </ion-card>\n        </ion-row>\n\n        <ion-row class=\"item_white_section ion-margin-top\">\n          <ion-card>\n            <ion-card-content>\n              <ion-label> 22/11/2021 </ion-label>\n\n              <ion-label class=\"small_text_light\">\n                nicolas.martin1997@yahoo.fr\n              </ion-label>\n\n              <div class=\"main_tick_div\">\n                <ion-label> applied </ion-label>\n\n                <div class=\"tick_icon_background_none\">\n                  <ion-img src=\"assets/icon/progress.png\"></ion-img>\n                </div>\n              </div>\n            </ion-card-content>\n          </ion-card>\n        </ion-row>\n\n        <ion-row class=\"item_white_section ion-margin-top\">\n          <ion-card class=\"shadow_less_card\">\n            <ion-card-content>\n              <ion-label class=\"small_text_light\">\n                Relancer votre ami en lui renvoyant un mail ?\n              </ion-label>\n            </ion-card-content>\n          </ion-card>\n        </ion-row>\n\n\n        <ion-row class=\"item_white_section ion-margin-top\">\n          <ion-card>\n            <ion-card-content>\n              <ion-label> 22/11/2021 </ion-label>\n\n              <ion-label class=\"small_text_light\">\n                nicolas.martin1997@yahoo.fr\n              </ion-label>\n\n              <div class=\"main_tick_div\">\n                <ion-label> applied </ion-label>\n\n                <div class=\"tick_icon_background_none\">\n                  <ion-icon name=\"close-outline\" class=\"close_icon\"></ion-icon>\n                </div>\n              </div>\n            </ion-card-content>\n          </ion-card>\n        </ion-row>\n\n\n        \n        <ion-row class=\"ion-margin-top\">\n          <div class=\"profile-viewall\">\n            <ion-button>VOIR TOUS LES PARRAINAGES </ion-button>\n          </div>\n        </ion-row>\n\n        <ion-row class=\"ion-margin-top card_data_section\">\n          <ion-card mode=\"ios\" class=\"card-box\">\n            <ion-card-header>\n              <ion-item lines=\"none\">\n                <ion-avatar>\n                  <ion-img\n                    src=\"assets/icon/transformatics_small_logo.png\"\n                  ></ion-img>\n                </ion-avatar>\n                <ion-label class=\"list_section_title\">\n                  <ion-label class=\"medium_text\">\n                    NOTE SUR LES THEMES DU CARNET\n                  </ion-label>\n                </ion-label>\n                <ion-reorder slot=\"end\"></ion-reorder>\n              </ion-item>\n            </ion-card-header>\n\n            <ion-card-content>\n              <ion-label class=\"small_text ion-text-justify\">\n                Lorsque vous parvenez à 5 personnes parrainées en l’espace de 30\n                jours, vous bénéficierez automatiquement d’un mois\n                supplémentaire offert sur votre abonnement Transformatics, sans\n                que vous n’ayez quoi que ce soit à faire !\n              </ion-label>\n\n              <ion-label class=\"small_text ion-margin-top ion-text-justify\">\n                Vos parrainages sont valables 3 mois. Passé ce délai,\n                l’invitation à votre ami sera automatiquement supprimée.\n              </ion-label>\n            </ion-card-content>\n\n            <div class=\"ribbon-wrapper-1\">\n              <div class=\"ribbon-1\">Ribbon</div>\n          </div>\n            \n          </ion-card>\n        </ion-row>\n      </div>\n    </ion-row>\n  </ion-row>\n\n</ion-content>\n";
      /***/
    },

    /***/
    "./node_modules/raw-loader/dist/cjs.js!./src/app/components/musculation-auto-seances-session/musculation-auto-seances-session.component.html":
    /*!***************************************************************************************************************************************************!*\
      !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/components/musculation-auto-seances-session/musculation-auto-seances-session.component.html ***!
      \***************************************************************************************************************************************************/

    /*! exports provided: default */

    /***/
    function node_modulesRawLoaderDistCjsJsSrcAppComponentsMusculationAutoSeancesSessionMusculationAutoSeancesSessionComponentHtml(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony default export */


      __webpack_exports__["default"] = "<ion-content>\n    <div class=\"titlebox\">\n        <div class=\"title-img\">\n            <img src=\"assets/images/musculetion-auto.png\">\n        </div>\n        <div class=\"titlebox-text\">\n            <h4>FULLBODY</h4>\n            <h5>1 séance </h5>\n        </div>\n\n    </div>\n\n    <ion-grid>\n        <div class=\"titlebox-add\" routerLink=\"/musculation-auto-programms-session\">\n            <ion-icon name=\"add-outline\"></ion-icon>\n        </div>\n        <ion-row>\n            <ion-col size=\"12\">\n                <h4 class=\"f14\">\n                    SEANCES D’ENTRAINEMENT TRANSFORMATICS\n                </h4>\n            </ion-col>\n            <ion-col size=\"12\">\n                <ion-item>\n                    <img slot=\"start\" src=\"assets/images/MODELE 2D TRAINING.png\" style=\"width: 100px;\">\n                    <ion-label>\n                        <h4>Groupes musculaires sollicités : </h4>\n                        <p>Pectoraux</p>\n                    </ion-label>\n                </ion-item>\n            </ion-col>\n        </ion-row>\n        <ion-row>\n            <ion-col size=\"12\"></ion-col>\n            <h4 class=\"f14\">\n                Visualisation des exercices\n            </h4>\n            <ion-col size=\"12\">\n                <ion-slides [options]=\"categories\" pager=\"false\">\n                    <ion-slide>\n                        <div class=\"musculetion-vid\">\n                            <div class=\"musculetion-vid-img\">\n                                <img src=\"assets/images/musculetion-auto.png\">\n                                <ion-icon name=\"play-circle-outline\"></ion-icon>\n                            </div>\n                            <div class=\"musculetion-vid-title\">\n                                <h4>[Nom de l’exercice]</h4>\n                                <h6>3 séries</h6>\n                            </div>\n                        </div>\n                    </ion-slide>\n                    <ion-slide>\n                        <div class=\"musculetion-vid\">\n                            <div class=\"musculetion-vid-img\">\n                                <img src=\"assets/images/payment_slide_3.png\">\n                                <ion-icon name=\"play-circle-outline\"></ion-icon>\n                            </div>\n                            <div class=\"musculetion-vid-title\">\n                                <h4>[Nom de l’exercice]</h4>\n                                <h6>3 séries</h6>\n                            </div>\n                        </div>\n                    </ion-slide>\n                    <ion-slide>\n                        <div class=\"musculetion-vid\">\n                            <div class=\"musculetion-vid-img\">\n                                <img src=\"assets/images/payment_slide_2.png\">\n                                <ion-icon name=\"play-circle-outline\"></ion-icon>\n                            </div>\n                            <div class=\"musculetion-vid-title\">\n                                <h4>[Nom de l’exercice]</h4>\n                                <h6>3 séries</h6>\n                            </div>\n                        </div>\n                    </ion-slide>\n                    <ion-slide>\n                        <div class=\"musculetion-vid\">\n                            <div class=\"musculetion-vid-img\">\n                                <img src=\"assets/images/musculetion-auto.png\">\n                                <ion-icon name=\"play-circle-outline\"></ion-icon>\n                            </div>\n                            <div class=\"musculetion-vid-title\">\n                                <h4>[Nom de l’exercice]</h4>\n                                <h6>3 séries</h6>\n                            </div>\n                        </div>\n                    </ion-slide>\n                    <ion-slide>\n                        <div class=\"musculetion-vid\">\n                            <div class=\"musculetion-vid-img\">\n                                <img src=\"assets/images/musculetion-auto.png\">\n                                <ion-icon name=\"play-circle-outline\"></ion-icon>\n                            </div>\n                            <div class=\"musculetion-vid-title\">\n                                <h4>[Nom de l’exercice]</h4>\n                                <h6>3 séries</h6>\n                            </div>\n                        </div>\n                    </ion-slide>\n                </ion-slides>\n            </ion-col>\n        </ion-row>\n    </ion-grid>\n</ion-content>";
      /***/
    },

    /***/
    "./node_modules/raw-loader/dist/cjs.js!./src/app/components/musculation-auto-seances/musculation-auto-seances.component.html":
    /*!***********************************************************************************************************************************!*\
      !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/components/musculation-auto-seances/musculation-auto-seances.component.html ***!
      \***********************************************************************************************************************************/

    /*! exports provided: default */

    /***/
    function node_modulesRawLoaderDistCjsJsSrcAppComponentsMusculationAutoSeancesMusculationAutoSeancesComponentHtml(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony default export */


      __webpack_exports__["default"] = "<ion-content>\n    <div class=\"titlebox\">\n        <div class=\"titlebox-arrow\">\n            <div>\n                <ion-icon name=\"chevron-back-outline\" [routerLink]=\"backpage\"></ion-icon>\n            </div>\n        </div>\n        <div class=\"title-img\">\n            <img src=\"assets/images/musculetion-auto.png\">\n        </div>\n        <div class=\"titlebox-text\">\n            <h5>INTRODUCING</h5>\n            <h4>GYMSHARK TRAIN</h4>\n        </div>\n    </div>\n    <ion-grid>\n        <ion-row>\n            <ion-col size=\"12\">\n                <h4 class=\"f14\">\n                    SEANCES D’ENTRAINEMENT TRANSFORMATICS\n                </h4>\n            </ion-col>\n            <ion-col size=\"12\">\n                <ion-item class=\"ion-no-padding\">\n                    <ion-label>Filtres de séances</ion-label>\n                    <ion-select value=\"notifications\" interface=\"action-sheet\">\n                        <ion-select-option value=\"enable\">Fullbody</ion-select-option>\n                        <ion-select-option value=\"mute\">Fullbody</ion-select-option>\n                        <ion-select-option value=\"mute_week\">Fullbody</ion-select-option>\n                        <ion-select-option value=\"mute_year\">Fullbody</ion-select-option>\n                    </ion-select>\n                </ion-item>\n            </ion-col>\n        </ion-row>\n        <ion-row>\n            <ion-col size=\"12\">\n                <ion-slides [options]=\"categories\" pager=\"false\">\n                    <ion-slide>\n                        <div class=\"musculetion-vid\" routerLink=\"/musculation-auto-seances-programms-session\">\n                            <div class=\"musculetion-vid-img\">\n                                <img src=\"assets/images/musculetion-auto.png\">\n                            </div>\n                            <div class=\"musculetion-vid-title\">\n                                <h4>FULLBODY</h4>\n                            </div>\n                        </div>\n                    </ion-slide>\n                    <ion-slide>\n                        <div class=\"musculetion-vid\">\n                            <div class=\"musculetion-vid-img\">\n                                <img src=\"assets/images/payment_slide_3.png\">\n                            </div>\n                            <div class=\"musculetion-vid-title\">\n                                <h4>FULLBODY</h4>\n                            </div>\n                        </div>\n                    </ion-slide>\n                    <ion-slide>\n                        <div class=\"musculetion-vid\">\n                            <div class=\"musculetion-vid-img\">\n                                <img src=\"assets/images/payment_slide_2.png\">\n                            </div>\n                            <div class=\"musculetion-vid-title\">\n                                <h4>FULLBODY</h4>\n                            </div>\n                        </div>\n                    </ion-slide>\n                    <ion-slide>\n                        <div class=\"musculetion-vid\">\n                            <div class=\"musculetion-vid-img\">\n                                <img src=\"assets/images/musculetion-auto.png\">\n                            </div>\n                            <div class=\"musculetion-vid-title\">\n                                <h4>FULLBODY</h4>\n                            </div>\n                        </div>\n                    </ion-slide>\n                    <ion-slide>\n                        <div class=\"musculetion-vid\">\n                            <div class=\"musculetion-vid-img\">\n                                <img src=\"assets/images/musculetion-auto.png\">\n                            </div>\n                            <div class=\"musculetion-vid-title\">\n                                <h4>FULLBODY</h4>\n                            </div>\n                        </div>\n                    </ion-slide>\n                </ion-slides>\n            </ion-col>\n        </ion-row>\n    </ion-grid>\n</ion-content>";
      /***/
    },

    /***/
    "./node_modules/raw-loader/dist/cjs.js!./src/app/components/performance-profile/performance-profile.component.html":
    /*!*************************************************************************************************************************!*\
      !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/components/performance-profile/performance-profile.component.html ***!
      \*************************************************************************************************************************/

    /*! exports provided: default */

    /***/
    function node_modulesRawLoaderDistCjsJsSrcAppComponentsPerformanceProfilePerformanceProfileComponentHtml(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony default export */


      __webpack_exports__["default"] = "\n    \n    <ion-row class=\"main_container\">\n\n      <ion-col size=\"12\">\n        <p class=\"performance_text\">\n          Vous pouvez observer le suivi de vos performances à l’entraînement\n          depuis le début de votre dernier programme. Vous avez la possibilité de\n          sélectionner les exercices et jours d’entraînement que vous souhaitez\n          analyser.\n        </p>\n  \n        \n    \n     \n        <app-chart></app-chart>\n  \n      </ion-col>\n      <ion-col size=\"12\">\n        <ion-item lines=\"none\">\n            <ion-label class=\"filter-text\">Filtrer</ion-label>\n            <ion-select interface=\"action-sheet\" class=\"custom-options\">\n                <ion-select-option value=\"Aux arachides\">Volume Jour 1</ion-select-option>\n                <ion-select-option value=\"Aux arachides\">Volume Jour 2</ion-select-option>\n\n                \n            </ion-select>\n        </ion-item>\n    </ion-col>\n      \n\n    </ion-row>";
      /***/
    },

    /***/
    "./node_modules/raw-loader/dist/cjs.js!./src/app/components/physiology-profile/physiology-profile.component.html":
    /*!***********************************************************************************************************************!*\
      !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/components/physiology-profile/physiology-profile.component.html ***!
      \***********************************************************************************************************************/

    /*! exports provided: default */

    /***/
    function node_modulesRawLoaderDistCjsJsSrcAppComponentsPhysiologyProfilePhysiologyProfileComponentHtml(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony default export */


      __webpack_exports__["default"] = "<ion-row class=\"main_container\">\n    <ion-col size=\"12\">\n        <p class=\"performance_text\">\n            La simulation de progression vous permet d’observer votre progression à travers plusieurs photos depuis votre inscription au système Transformatics. Déplacez simplement le curseur vers la droite ou la gauche pour observer les progrès que vous avez fait\n            sur une période donnée !\n        </p>\n    </ion-col>\n    <ion-col size=\"12\">\n        <ion-item lines=\"none\" class=\"padding-start-0\">\n            <ion-label class=\"date_label\">Date des photos  « avant »\n            </ion-label>\n            <ion-datetime value=\"1990-02-19\" placeholder=\"Select Date\"></ion-datetime>\n        </ion-item>\n\n        <ion-item lines=\"none\" class=\"padding-start-0\" style=\"margin-bottom: 15px;\">\n            <ion-label class=\"date_label\">Date des photos  « avant »\n            </ion-label>\n            <ion-datetime value=\"1990-02-19\" placeholder=\"Select Date\"></ion-datetime>\n        </ion-item>\n\n        <ion-segment value=\"phote-de-face\" color=\"tertiary\" scrollable=\"true\" [(ngModel)]=\"physiologySegmentModel\" (ionChange)=\"physiologySegmentChanged($event)\">\n            <ion-segment-button value=\"phote-de-face\">\n                <ion-label class=\"segment-btn-text\">PHOTO DE FACE</ion-label>\n            </ion-segment-button>\n\n            <ion-segment-button value=\"photo-de-dos\">\n                <ion-label class=\"segment-btn-text\">PHOTO DE DOS</ion-label>\n            </ion-segment-button>\n            <ion-segment-button value=\"photo-de-profil\">\n                <ion-label class=\"segment-btn-text\">PHOTO DE PROFIL</ion-label>\n            </ion-segment-button>\n        </ion-segment>\n\n        <div class=\"segment_container_data\">\n            <div *ngIf=\"physiologySegmentModel === 'phote-de-face'\">\n                <div style=\"margin-top: 15px;\">\n                    <img-comparison-slider>\n                        <img slot=\"before\" class=\"image-compair\" src=\"/assets/images/before.jpg\" />\n                        <img slot=\"after\" class=\"image-compair\" src=\"/assets/images/after.jpg\" />\n                    </img-comparison-slider>\n                </div>\n            </div>\n\n            <div *ngIf=\"physiologySegmentModel === 'photo-de-dos'\">\n                <div style=\"margin-top: 15px;\">\n                    <img-comparison-slider>\n                        <img slot=\"before\" class=\"image-compair\" src=\"/assets/images/before.jpg\" />\n                        <img slot=\"after\" class=\"image-compair\" src=\"/assets/images/after.jpg\" />\n                    </img-comparison-slider>\n                </div>\n            </div>\n            <div *ngIf=\"physiologySegmentModel === 'photo-de-profil'\">\n                <div style=\"margin-top: 15px;\">\n                    <img-comparison-slider>\n                        <img slot=\"before\" class=\"image-compair\" src=\"/assets/images/before.jpg\" />\n                        <img slot=\"after\" class=\"image-compair\" src=\"/assets/images/after.jpg\" />\n                    </img-comparison-slider>\n                </div>\n            </div>\n        </div>\n\n    </ion-col>\n</ion-row>";
      /***/
    },

    /***/
    "./node_modules/raw-loader/dist/cjs.js!./src/app/components/records-profile/records-profile.component.html":
    /*!*****************************************************************************************************************!*\
      !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/components/records-profile/records-profile.component.html ***!
      \*****************************************************************************************************************/

    /*! exports provided: default */

    /***/
    function node_modulesRawLoaderDistCjsJsSrcAppComponentsRecordsProfileRecordsProfileComponentHtml(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony default export */


      __webpack_exports__["default"] = "<ion-row class=\"main_container\">\n  <ion-col size=\"12\">\n    <p class=\"performance_text\">\n      Voici vos performances maximales reportées sur plusieurs types\n      d’exercices. Ces valeurs évoluent en fonction de vos performances en temps\n      réel à l’entraînement, et peuvent être modifiées manuellement en entrant\n      vous-même vos nouvelles performances dans le tableau ci-dessous.\n    </p>\n\n    <app-chart></app-chart>\n  </ion-col>\n</ion-row>\n<ion-grid>\n  <ion-row>\n    <!-- <ion-col size=\"12\">\n      <p class=\"performance_text\">\n        Voici vos performances maximales reportées sur plusieurs types\n        d’exercices. Ces valeurs évoluent en fonction de vos performances en\n        temps réel à l’entraînement, et peuvent être modifiées manuellement en\n        entrant vous-même vos nouvelles performances dans le tableau ci-dessous.\n      </p>\n    </ion-col>\n    <ion-col size=\"12\">\n      <p>CHART</p>\n    </ion-col> -->\n    <ion-col size=\"6\">\n      <div class=\"records-velue\">\n        <p class=\"t-a-r\">DEVELOPPE COUCHE</p>\n        <h4 class=\"t-a-r\">100<sub> KG</sub></h4>\n      </div>\n    </ion-col>\n    <ion-col size=\"6\">\n      <div class=\"records-velue\">\n        <p class=\"t-a-r\">SOULEVE DE TERRE</p>\n        <h4 class=\"t-a-r\">160<sub> KG</sub></h4>\n      </div>\n    </ion-col>\n\n    <ion-col size=\"6\">\n      <div class=\"records-velue\">\n        <p class=\"t-a-r\">SQUAT BARRE</p>\n        <h4 class=\"t-a-r\">140<sub> KG</sub></h4>\n      </div>\n    </ion-col>\n    <ion-col size=\"6\">\n      <div class=\"records-velue\">\n        <p class=\"t-a-r\">COMBINE</p>\n        <h4 class=\"t-a-r\">400<sub> KG</sub></h4>\n      </div>\n    </ion-col>\n    <ion-col>\n      <div>\n        <h4 class=\"f-size-16\">TABLEAU DES RECORDS</h4>\n        <p class=\"f-size-13\">\n          Vous pourrez noter ici vos performances au fil de votre progression\n          pour fixer les objectifs à atteindre durant vos entraînements. De\n          cette manière, vous pourrez observer l’évolution de vos charges\n          maximales avec le temps et progresser indéfiniment.\n        </p>\n      </div>\n    </ion-col>\n    <ion-col size=\"12\">\n      <ion-segment\n        class=\"segmentOne\"\n        value=\"1rm\"\n        color=\"tertiary\"\n        scrollable=\"true\"\n        [(ngModel)]=\"segmentRMModel\"\n        (ionChange)=\"segmentChanged($event)\"\n      >\n        <ion-segment-button class=\"segmentOne-button\" value=\"1rm\">\n          <ion-label class=\"segment-btn-text\">1 RM</ion-label>\n        </ion-segment-button>\n\n        <ion-segment-button class=\"segmentOne-button\" value=\"3rm\">\n          <ion-label class=\"segment-btn-text\">3 RM</ion-label>\n        </ion-segment-button>\n        <ion-segment-button class=\"segmentOne-button\" value=\"5rm\">\n          <ion-label class=\"segment-btn-text\">5 RM</ion-label>\n        </ion-segment-button>\n        <ion-segment-button class=\"segmentOne-button\" value=\"10rm\">\n          <ion-label class=\"segment-btn-text\">10 RM</ion-label>\n        </ion-segment-button>\n      </ion-segment>\n\n      <div class=\"segment_container_data\">\n        <div *ngIf=\"segmentRMModel === '1rm'\">\n          <ion-grid>\n            <ion-row>\n              <ion-col size=\"10\">\n                <ion-item>\n                  <ion-avatar slot=\"start\">\n                    <img src=\"assets/icon/rm.png\" />\n                  </ion-avatar>\n                  <ion-label>\n                    <p>Nom de l’exercice</p>\n                    <p>Sous nom de l’exercice</p>\n                  </ion-label>\n                </ion-item>\n              </ion-col>\n              <ion-col size=\"2\">\n                <div class=\"ion-text-right p-t-15\">180 <sub>kg</sub></div>\n              </ion-col>\n              <ion-col size=\"10\">\n                <ion-item>\n                  <ion-avatar slot=\"start\">\n                    <img src=\"assets/icon/rm.png\" />\n                  </ion-avatar>\n                  <ion-label>\n                    <p>Nom de l’exercice</p>\n                    <p>Sous nom de l’exercice</p>\n                  </ion-label>\n                </ion-item>\n              </ion-col>\n              <ion-col size=\"2\">\n                <div class=\"ion-text-right\">\n                  <ion-icon name=\"add-circle-outline\" class=\"add\"></ion-icon>\n                </div>\n              </ion-col>\n              <ion-col size=\"10\">\n                <ion-item>\n                  <ion-avatar slot=\"start\">\n                    <img src=\"assets/icon/rm.png\" />\n                  </ion-avatar>\n                  <ion-label>\n                    <p>Nom de l’exercice</p>\n                    <p>Sous nom de l’exercice</p>\n                  </ion-label>\n                </ion-item>\n              </ion-col>\n              <ion-col size=\"2\">\n                <div class=\"ion-text-right p-t-15\">160 <sub>kg</sub></div>\n              </ion-col>\n            </ion-row>\n          </ion-grid>\n        </div>\n\n        <div *ngIf=\"segmentRMModel === '3rm'\">\n          <ion-grid>\n            <ion-row>\n              <ion-col size=\"10\">\n                <ion-item>\n                  <ion-avatar slot=\"start\">\n                    <img src=\"assets/icon/rm.png\" />\n                  </ion-avatar>\n                  <ion-label>\n                    <p>Nom de l’exercice</p>\n                    <p>Sous nom de l’exercice</p>\n                  </ion-label>\n                </ion-item>\n              </ion-col>\n              <ion-col size=\"2\">\n                <div class=\"ion-text-right p-t-15\">140 <sub>kg</sub></div>\n              </ion-col>\n              <ion-col size=\"10\">\n                <ion-item>\n                  <ion-avatar slot=\"start\">\n                    <img src=\"assets/icon/rm.png\" />\n                  </ion-avatar>\n                  <ion-label>\n                    <p>Nom de l’exercice</p>\n                    <p>Sous nom de l’exercice</p>\n                  </ion-label>\n                </ion-item>\n              </ion-col>\n              <ion-col size=\"2\">\n                <div class=\"ion-text-right\">\n                  <ion-icon name=\"add-circle-outline\" class=\"add\"></ion-icon>\n                </div>\n              </ion-col>\n              <ion-col size=\"10\">\n                <ion-item>\n                  <ion-avatar slot=\"start\">\n                    <img src=\"assets/icon/rm.png\" />\n                  </ion-avatar>\n                  <ion-label>\n                    <p>Nom de l’exercice</p>\n                    <p>Sous nom de l’exercice</p>\n                  </ion-label>\n                </ion-item>\n              </ion-col>\n              <ion-col size=\"2\">\n                <div class=\"ion-text-right p-t-15\">160 <sub>kg</sub></div>\n              </ion-col>\n            </ion-row>\n          </ion-grid>\n        </div>\n        <div *ngIf=\"segmentRMModel === '5rm'\">\n          <ion-grid>\n            <ion-row>\n              <ion-col size=\"10\">\n                <ion-item>\n                  <ion-avatar slot=\"start\">\n                    <img src=\"assets/icon/rm.png\" />\n                  </ion-avatar>\n                  <ion-label>\n                    <p>Nom de l’exercice</p>\n                    <p>Sous nom de l’exercice</p>\n                  </ion-label>\n                </ion-item>\n              </ion-col>\n              <ion-col size=\"2\">\n                <div class=\"ion-text-right p-t-15\">190 <sub>kg</sub></div>\n              </ion-col>\n              <ion-col size=\"10\">\n                <ion-item>\n                  <ion-avatar slot=\"start\">\n                    <img src=\"assets/icon/rm.png\" />\n                  </ion-avatar>\n                  <ion-label>\n                    <p>Nom de l’exercice</p>\n                    <p>Sous nom de l’exercice</p>\n                  </ion-label>\n                </ion-item>\n              </ion-col>\n              <ion-col size=\"2\">\n                <div class=\"ion-text-right\">\n                  <ion-icon name=\"add-circle-outline\" class=\"add\"></ion-icon>\n                </div>\n              </ion-col>\n              <ion-col size=\"10\">\n                <ion-item>\n                  <ion-avatar slot=\"start\">\n                    <img src=\"assets/icon/rm.png\" />\n                  </ion-avatar>\n                  <ion-label>\n                    <p>Nom de l’exercice</p>\n                    <p>Sous nom de l’exercice</p>\n                  </ion-label>\n                </ion-item>\n              </ion-col>\n              <ion-col size=\"2\">\n                <div class=\"ion-text-right p-t-15\">180 <sub>kg</sub></div>\n              </ion-col>\n            </ion-row>\n          </ion-grid>\n        </div>\n        <div *ngIf=\"segmentRMModel === '10rm'\">\n          <ion-grid>\n            <ion-row>\n              <ion-col size=\"10\">\n                <ion-item>\n                  <ion-avatar slot=\"start\">\n                    <img src=\"assets/icon/rm.png\" />\n                  </ion-avatar>\n                  <ion-label>\n                    <p>Nom de l’exercice</p>\n                    <p>Sous nom de l’exercice</p>\n                  </ion-label>\n                </ion-item>\n              </ion-col>\n              <ion-col size=\"2\">\n                <div class=\"ion-text-right p-t-15\">110 <sub>kg</sub></div>\n              </ion-col>\n              <ion-col size=\"10\">\n                <ion-item>\n                  <ion-avatar slot=\"start\">\n                    <img src=\"assets/icon/rm.png\" />\n                  </ion-avatar>\n                  <ion-label>\n                    <p>Nom de l’exercice</p>\n                    <p>Sous nom de l’exercice</p>\n                  </ion-label>\n                </ion-item>\n              </ion-col>\n              <ion-col size=\"2\">\n                <div class=\"ion-text-right\">\n                  <ion-icon name=\"add-circle-outline\" class=\"add\"></ion-icon>\n                </div>\n              </ion-col>\n              <ion-col size=\"10\">\n                <ion-item>\n                  <ion-avatar slot=\"start\">\n                    <img src=\"assets/icon/rm.png\" />\n                  </ion-avatar>\n                  <ion-label>\n                    <p>Nom de l’exercice</p>\n                    <p>Sous nom de l’exercice</p>\n                  </ion-label>\n                </ion-item>\n              </ion-col>\n              <ion-col size=\"2\">\n                <div class=\"ion-text-right p-t-15\">130 <sub>kg</sub></div>\n              </ion-col>\n            </ion-row>\n          </ion-grid>\n        </div>\n      </div>\n    </ion-col>\n  </ion-row>\n</ion-grid>\n";
      /***/
    },

    /***/
    "./node_modules/raw-loader/dist/cjs.js!./src/app/components/swiper-card/swiper-card.component.html":
    /*!*********************************************************************************************************!*\
      !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/components/swiper-card/swiper-card.component.html ***!
      \*********************************************************************************************************/

    /*! exports provided: default */

    /***/
    function node_modulesRawLoaderDistCjsJsSrcAppComponentsSwiperCardSwiperCardComponentHtml(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony default export */


      __webpack_exports__["default"] = "<div class=\"swiper\" [hidden]=\"!cards.length\">\n\n  <!-- <div class=\"swiper--status\">\n\n      <div [style.opacity]=\"crossVisible? '1':'0'\">\n\n      </div>\n\n      <div [style.opacity]=\"heartVisible? '1':'0'\">\n         \n      </div>\n\n  </div> -->\n\n  <div class=\"swiper--cards\" >\n\n      <div #swiperCard class=\"swiper--card\" (transitionend)=\"handleShift()\" *ngFor=\"let card of cards; let i = index\"\n          [ngStyle]=\"{ zIndex: cards.length - i, transform: 'scale(' + (15 - i) / 15 + ') translateY(' + 15 * i + 'px)' }\">\n          <ion-card>\n            <ion-card-content>\n              <h3 class=\"medium_text_bold\">{{ card.title }}</h3>\n              <p class=\"small_text\">{{ card.description }}</p>\n              <ion-range [min]=\"minutesMinRange\" max=\"60\" (ionChange)=\"minuteChange($event)\">\n                <ion-label slot=\"end\">{{minutesChangeRange}}</ion-label>\n            </ion-range>\n            </ion-card-content>\n            <div class=\"swiper--buttons\">\n\n              <button (click)=\"userClickedButton($event, false)\" class=\"left\">\n                <ion-icon name=\"arrow-back-outline\"></ion-icon>\n                  No\n              </button>\n          \n              <button (click)=\"userClickedButton($event, true)\" class=\"right\">\n                <ion-icon name=\"arrow-forward-outline\"></ion-icon>\n                  Yes\n              </button>\n          \n            </div>\n          </ion-card>\n          <!-- <img #swiperCardImage [src]=\"card.img\" (load)=\"swiperCardImage.style.opacity = 1\"> -->\n         \n\n      </div>\n\n  </div>\n\n  \n\n</div>";
      /***/
    },

    /***/
    "./src/app/app-routing.module.ts":
    /*!***************************************!*\
      !*** ./src/app/app-routing.module.ts ***!
      \***************************************/

    /*! exports provided: AppRoutingModule */

    /***/
    function srcAppAppRoutingModuleTs(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "AppRoutingModule", function () {
        return AppRoutingModule;
      });
      /* harmony import */


      var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! tslib */
      "./node_modules/tslib/tslib.es6.js");
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! @angular/core */
      "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
      /* harmony import */


      var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! @angular/router */
      "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");

      var routes = [{
        path: '',
        redirectTo: 'landing-page',
        pathMatch: 'full'
      }, {
        path: 'home',
        loadChildren: './pages/home/home.module#HomePageModule'
      }, {
        path: 'landing-page',
        loadChildren: function loadChildren() {
          return __webpack_require__.e(
          /*! import() | pages-landing-page-landing-page-module */
          "pages-landing-page-landing-page-module").then(__webpack_require__.bind(null,
          /*! ./pages/landing-page/landing-page.module */
          "./src/app/pages/landing-page/landing-page.module.ts")).then(function (m) {
            return m.LandingPagePageModule;
          });
        }
      }, {
        path: 'select-roles',
        loadChildren: function loadChildren() {
          return Promise.all(
          /*! import() | pages-select-roles-select-roles-module */
          [__webpack_require__.e("common"), __webpack_require__.e("pages-select-roles-select-roles-module")]).then(__webpack_require__.bind(null,
          /*! ./pages/select-roles/select-roles.module */
          "./src/app/pages/select-roles/select-roles.module.ts")).then(function (m) {
            return m.SelectRolesPageModule;
          });
        }
      }, {
        path: 'body-scan-steps',
        loadChildren: function loadChildren() {
          return Promise.all(
          /*! import() | pages-body-scan-steps-body-scan-steps-module */
          [__webpack_require__.e("common"), __webpack_require__.e("pages-body-scan-steps-body-scan-steps-module")]).then(__webpack_require__.bind(null,
          /*! ./pages/body-scan-steps/body-scan-steps.module */
          "./src/app/pages/body-scan-steps/body-scan-steps.module.ts")).then(function (m) {
            return m.BodyScanStepsPageModule;
          });
        }
      }, {
        path: 'tabs',
        loadChildren: function loadChildren() {
          return Promise.all(
          /*! import() | pages-tabs-tabs-module */
          [__webpack_require__.e("common"), __webpack_require__.e("pages-tabs-tabs-module")]).then(__webpack_require__.bind(null,
          /*! ./pages/tabs/tabs.module */
          "./src/app/pages/tabs/tabs.module.ts")).then(function (m) {
            return m.TabsPageModule;
          });
        }
      }, {
        path: 'auth',
        loadChildren: function loadChildren() {
          return Promise.all(
          /*! import() | pages-auth-auth-module */
          [__webpack_require__.e("common"), __webpack_require__.e("pages-auth-auth-module")]).then(__webpack_require__.bind(null,
          /*! ./pages/auth/auth.module */
          "./src/app/pages/auth/auth.module.ts")).then(function (m) {
            return m.AuthPageModule;
          });
        }
      }, {
        path: 'body-scan-arrival',
        loadChildren: function loadChildren() {
          return __webpack_require__.e(
          /*! import() | pages-body-scan-arrival-body-scan-arrival-module */
          "pages-body-scan-arrival-body-scan-arrival-module").then(__webpack_require__.bind(null,
          /*! ./pages/body-scan-arrival/body-scan-arrival.module */
          "./src/app/pages/body-scan-arrival/body-scan-arrival.module.ts")).then(function (m) {
            return m.BodyScanArrivalPageModule;
          });
        }
      }, {
        path: 'select-plan',
        loadChildren: function loadChildren() {
          return __webpack_require__.e(
          /*! import() | pages-select-plan-select-plan-module */
          "pages-select-plan-select-plan-module").then(__webpack_require__.bind(null,
          /*! ./pages/select-plan/select-plan.module */
          "./src/app/pages/select-plan/select-plan.module.ts")).then(function (m) {
            return m.SelectPlanPageModule;
          });
        }
      }, {
        path: 'hypertrophy-strengthening-program-creation',
        loadChildren: function loadChildren() {
          return __webpack_require__.e(
          /*! import() | pages-tabs-home-hypertrophy-strengthening-program-creation-hypertrophy-strengthening-program-creation-module */
          "pages-tabs-home-hypertrophy-strengthening-program-creation-hypertrophy-strengthening-program-creation-module").then(__webpack_require__.bind(null,
          /*! ./pages/tabs/home/hypertrophy-strengthening-program-creation/hypertrophy-strengthening-program-creation.module */
          "./src/app/pages/tabs/home/hypertrophy-strengthening-program-creation/hypertrophy-strengthening-program-creation.module.ts")).then(function (m) {
            return m.HypertrophyStrengtheningProgramCreationPageModule;
          });
        }
      }, {
        path: 'add-exercise',
        loadChildren: function loadChildren() {
          return __webpack_require__.e(
          /*! import() | pages-tabs-home-add-exercise-add-exercise-module */
          "pages-tabs-home-add-exercise-add-exercise-module").then(__webpack_require__.bind(null,
          /*! ./pages/tabs/home/add-exercise/add-exercise.module */
          "./src/app/pages/tabs/home/add-exercise/add-exercise.module.ts")).then(function (m) {
            return m.AddExercisePageModule;
          });
        }
      }, {
        path: 'select-body-groups',
        loadChildren: function loadChildren() {
          return __webpack_require__.e(
          /*! import() | pages-tabs-home-select-body-groups-select-body-groups-module */
          "pages-tabs-home-select-body-groups-select-body-groups-module").then(__webpack_require__.bind(null,
          /*! ./pages/tabs/home/select-body-groups/select-body-groups.module */
          "./src/app/pages/tabs/home/select-body-groups/select-body-groups.module.ts")).then(function (m) {
            return m.SelectBodyGroupsPageModule;
          });
        }
      }, {
        path: 'excercise-sheet',
        loadChildren: function loadChildren() {
          return Promise.all(
          /*! import() | pages-tabs-home-excercise-sheet-excercise-sheet-module */
          [__webpack_require__.e("common"), __webpack_require__.e("pages-tabs-home-excercise-sheet-excercise-sheet-module")]).then(__webpack_require__.bind(null,
          /*! ./pages/tabs/home/excercise-sheet/excercise-sheet.module */
          "./src/app/pages/tabs/home/excercise-sheet/excercise-sheet.module.ts")).then(function (m) {
            return m.ExcerciseSheetPageModule;
          });
        }
      }, {
        path: 'edit-physical-activity',
        loadChildren: function loadChildren() {
          return __webpack_require__.e(
          /*! import() | pages-tabs-home-edit-physical-activity-edit-physical-activity-module */
          "pages-tabs-home-edit-physical-activity-edit-physical-activity-module").then(__webpack_require__.bind(null,
          /*! ./pages/tabs/home/edit-physical-activity/edit-physical-activity.module */
          "./src/app/pages/tabs/home/edit-physical-activity/edit-physical-activity.module.ts")).then(function (m) {
            return m.EditPhysicalActivityPageModule;
          });
        }
      }, {
        path: 'ebook',
        loadChildren: function loadChildren() {
          return __webpack_require__.e(
          /*! import() | pages-tabs-cart-ebook-ebook-module */
          "pages-tabs-cart-ebook-ebook-module").then(__webpack_require__.bind(null,
          /*! ./pages/tabs/cart/ebook/ebook.module */
          "./src/app/pages/tabs/cart/ebook/ebook.module.ts")).then(function (m) {
            return m.EbookPageModule;
          });
        }
      }, {
        path: 'cloth-accessoires',
        loadChildren: function loadChildren() {
          return __webpack_require__.e(
          /*! import() | pages-tabs-cart-cloth-accessoires-cloth-accessoires-module */
          "pages-tabs-cart-cloth-accessoires-cloth-accessoires-module").then(__webpack_require__.bind(null,
          /*! ./pages/tabs/cart/cloth-accessoires/cloth-accessoires.module */
          "./src/app/pages/tabs/cart/cloth-accessoires/cloth-accessoires.module.ts")).then(function (m) {
            return m.ClothAccessoiresPageModule;
          });
        }
      }, {
        path: 'food-supplementation',
        loadChildren: function loadChildren() {
          return __webpack_require__.e(
          /*! import() | pages-tabs-cart-food-supplementation-food-supplementation-module */
          "pages-tabs-cart-food-supplementation-food-supplementation-module").then(__webpack_require__.bind(null,
          /*! ./pages/tabs/cart/food-supplementation/food-supplementation.module */
          "./src/app/pages/tabs/cart/food-supplementation/food-supplementation.module.ts")).then(function (m) {
            return m.FoodSupplementationPageModule;
          });
        }
      }, {
        path: 'sports-metrial',
        loadChildren: function loadChildren() {
          return __webpack_require__.e(
          /*! import() | pages-tabs-cart-sports-metrial-sports-metrial-module */
          "pages-tabs-cart-sports-metrial-sports-metrial-module").then(__webpack_require__.bind(null,
          /*! ./pages/tabs/cart/sports-metrial/sports-metrial.module */
          "./src/app/pages/tabs/cart/sports-metrial/sports-metrial.module.ts")).then(function (m) {
            return m.SportsMetrialPageModule;
          });
        }
      }, {
        path: 'ebook-details',
        loadChildren: function loadChildren() {
          return __webpack_require__.e(
          /*! import() | pages-tabs-cart-ebook-details-ebook-details-module */
          "pages-tabs-cart-ebook-details-ebook-details-module").then(__webpack_require__.bind(null,
          /*! ./pages/tabs/cart/ebook-details/ebook-details.module */
          "./src/app/pages/tabs/cart/ebook-details/ebook-details.module.ts")).then(function (m) {
            return m.EbookDetailsPageModule;
          });
        }
      }, {
        path: 'coaching',
        loadChildren: function loadChildren() {
          return __webpack_require__.e(
          /*! import() | pages-tabs-menu-coaching-coaching-module */
          "pages-tabs-menu-coaching-coaching-module").then(__webpack_require__.bind(null,
          /*! ./pages/tabs/menu/coaching/coaching.module */
          "./src/app/pages/tabs/menu/coaching/coaching.module.ts")).then(function (m) {
            return m.CoachingPageModule;
          });
        }
      }, {
        path: 'preparation-mental',
        loadChildren: function loadChildren() {
          return __webpack_require__.e(
          /*! import() | pages-tabs-menu-preparation-mental-preparation-mental-module */
          "pages-tabs-menu-preparation-mental-preparation-mental-module").then(__webpack_require__.bind(null,
          /*! ./pages/tabs/menu/preparation-mental/preparation-mental.module */
          "./src/app/pages/tabs/menu/preparation-mental/preparation-mental.module.ts")).then(function (m) {
            return m.PreparationMentalPageModule;
          });
        }
      }, {
        path: 'apprentissage',
        loadChildren: function loadChildren() {
          return __webpack_require__.e(
          /*! import() | pages-tabs-menu-apprentissage-apprentissage-module */
          "pages-tabs-menu-apprentissage-apprentissage-module").then(__webpack_require__.bind(null,
          /*! ./pages/tabs/menu/apprentissage/apprentissage.module */
          "./src/app/pages/tabs/menu/apprentissage/apprentissage.module.ts")).then(function (m) {
            return m.ApprentissagePageModule;
          });
        }
      }, {
        path: 'chat',
        loadChildren: function loadChildren() {
          return __webpack_require__.e(
          /*! import() | pages-tabs-menu-chat-chat-module */
          "pages-tabs-menu-chat-chat-module").then(__webpack_require__.bind(null,
          /*! ./pages/tabs/menu/chat/chat.module */
          "./src/app/pages/tabs/menu/chat/chat.module.ts")).then(function (m) {
            return m.ChatPageModule;
          });
        }
      }, {
        path: 'physical-activity-creation-menu',
        loadChildren: function loadChildren() {
          return __webpack_require__.e(
          /*! import() | pages-tabs-home-physical-activity-creation-menu-physical-activity-creation-menu-module */
          "pages-tabs-home-physical-activity-creation-menu-physical-activity-creation-menu-module").then(__webpack_require__.bind(null,
          /*! ./pages/tabs/home/physical-activity-creation-menu/physical-activity-creation-menu.module */
          "./src/app/pages/tabs/home/physical-activity-creation-menu/physical-activity-creation-menu.module.ts")).then(function (m) {
            return m.PhysicalActivityCreationMenuPageModule;
          });
        }
      }, {
        path: 'find-coach',
        loadChildren: function loadChildren() {
          return __webpack_require__.e(
          /*! import() | pages-tabs-menu-find-coach-find-coach-module */
          "pages-tabs-menu-find-coach-find-coach-module").then(__webpack_require__.bind(null,
          /*! ./pages/tabs/menu/find-coach/find-coach.module */
          "./src/app/pages/tabs/menu/find-coach/find-coach.module.ts")).then(function (m) {
            return m.FindCoachPageModule;
          });
        }
      }, {
        path: 'aliments',
        loadChildren: function loadChildren() {
          return __webpack_require__.e(
          /*! import() | pages-tabs-home-aliments-aliments-module */
          "pages-tabs-home-aliments-aliments-module").then(__webpack_require__.bind(null,
          /*! ./pages/tabs/home/aliments/aliments.module */
          "./src/app/pages/tabs/home/aliments/aliments.module.ts")).then(function (m) {
            return m.AlimentsPageModule;
          });
        }
      }, {
        path: 'aliments-details',
        loadChildren: function loadChildren() {
          return __webpack_require__.e(
          /*! import() | pages-tabs-home-aliments-details-aliments-details-module */
          "pages-tabs-home-aliments-details-aliments-details-module").then(__webpack_require__.bind(null,
          /*! ./pages/tabs/home/aliments-details/aliments-details.module */
          "./src/app/pages/tabs/home/aliments-details/aliments-details.module.ts")).then(function (m) {
            return m.AlimentsDetailsPageModule;
          });
        }
      }, {
        path: 'conseils-nutritionnels',
        loadChildren: function loadChildren() {
          return __webpack_require__.e(
          /*! import() | pages-tabs-home-conseils-nutritionnels-conseils-nutritionnels-module */
          "pages-tabs-home-conseils-nutritionnels-conseils-nutritionnels-module").then(__webpack_require__.bind(null,
          /*! ./pages/tabs/home/conseils-nutritionnels/conseils-nutritionnels.module */
          "./src/app/pages/tabs/home/conseils-nutritionnels/conseils-nutritionnels.module.ts")).then(function (m) {
            return m.ConseilsNutritionnelsPageModule;
          });
        }
      }, {
        path: 'conseils-nutritionnels-details',
        loadChildren: function loadChildren() {
          return __webpack_require__.e(
          /*! import() | pages-tabs-home-conseils-nutritionnels-details-conseils-nutritionnels-details-module */
          "pages-tabs-home-conseils-nutritionnels-details-conseils-nutritionnels-details-module").then(__webpack_require__.bind(null,
          /*! ./pages/tabs/home/conseils-nutritionnels-details/conseils-nutritionnels-details.module */
          "./src/app/pages/tabs/home/conseils-nutritionnels-details/conseils-nutritionnels-details.module.ts")).then(function (m) {
            return m.ConseilsNutritionnelsDetailsPageModule;
          });
        }
      }, {
        path: 'body-scan-arrival-coach',
        loadChildren: function loadChildren() {
          return __webpack_require__.e(
          /*! import() | pages-body-scan-arrival-coach-body-scan-arrival-coach-module */
          "pages-body-scan-arrival-coach-body-scan-arrival-coach-module").then(__webpack_require__.bind(null,
          /*! ./pages/body-scan-arrival-coach/body-scan-arrival-coach.module */
          "./src/app/pages/body-scan-arrival-coach/body-scan-arrival-coach.module.ts")).then(function (m) {
            return m.BodyScanArrivalCoachPageModule;
          });
        }
      }, {
        path: 'body-scan-step-coach',
        loadChildren: function loadChildren() {
          return __webpack_require__.e(
          /*! import() | pages-body-scan-step-coach-body-scan-step-coach-module */
          "pages-body-scan-step-coach-body-scan-step-coach-module").then(__webpack_require__.bind(null,
          /*! ./pages/body-scan-step-coach/body-scan-step-coach.module */
          "./src/app/pages/body-scan-step-coach/body-scan-step-coach.module.ts")).then(function (m) {
            return m.BodyScanStepCoachPageModule;
          });
        }
      }, {
        path: 'select-plan-coach',
        loadChildren: function loadChildren() {
          return __webpack_require__.e(
          /*! import() | pages-select-plan-coach-select-plan-coach-module */
          "pages-select-plan-coach-select-plan-coach-module").then(__webpack_require__.bind(null,
          /*! ./pages/select-plan-coach/select-plan-coach.module */
          "./src/app/pages/select-plan-coach/select-plan-coach.module.ts")).then(function (m) {
            return m.SelectPlanCoachPageModule;
          });
        }
      }, {
        path: 'recipe-details',
        loadChildren: function loadChildren() {
          return __webpack_require__.e(
          /*! import() | pages-tabs-home-recipe-details-recipe-details-module */
          "pages-tabs-home-recipe-details-recipe-details-module").then(__webpack_require__.bind(null,
          /*! ./pages/tabs/home/recipe-details/recipe-details.module */
          "./src/app/pages/tabs/home/recipe-details/recipe-details.module.ts")).then(function (m) {
            return m.RecipeDetailsPageModule;
          });
        }
      }, {
        path: 'coach-acivity',
        loadChildren: function loadChildren() {
          return __webpack_require__.e(
          /*! import() | pages-coach-coach-acivity-coach-acivity-module */
          "pages-coach-coach-acivity-coach-acivity-module").then(__webpack_require__.bind(null,
          /*! ./pages/coach/coach-acivity/coach-acivity.module */
          "./src/app/pages/coach/coach-acivity/coach-acivity.module.ts")).then(function (m) {
            return m.CoachAcivityPageModule;
          });
        }
      }, {
        path: 'musculation-auto-seances-programms/:page',
        loadChildren: function loadChildren() {
          return __webpack_require__.e(
          /*! import() | pages-musculation-auto-seances-programms-musculation-auto-seances-programms-module */
          "pages-musculation-auto-seances-programms-musculation-auto-seances-programms-module").then(__webpack_require__.bind(null,
          /*! ./pages/musculation-auto-seances-programms/musculation-auto-seances-programms.module */
          "./src/app/pages/musculation-auto-seances-programms/musculation-auto-seances-programms.module.ts")).then(function (m) {
            return m.MusculationAutoSeancesProgrammsPageModule;
          });
        }
      }, {
        path: 'musculation-auto-seances-programms-session',
        loadChildren: function loadChildren() {
          return __webpack_require__.e(
          /*! import() | pages-musculation-auto-seances-programms-session-musculation-auto-seances-programms-session-module */
          "pages-musculation-auto-seances-programms-session-musculation-auto-seances-programms-session-module").then(__webpack_require__.bind(null,
          /*! ./pages/musculation-auto-seances-programms-session/musculation-auto-seances-programms-session.module */
          "./src/app/pages/musculation-auto-seances-programms-session/musculation-auto-seances-programms-session.module.ts")).then(function (m) {
            return m.MusculationAutoSeancesProgrammsSessionPageModule;
          });
        }
      }, {
        path: 'musculation-auto-programms-session',
        loadChildren: function loadChildren() {
          return __webpack_require__.e(
          /*! import() | pages-musculation-auto-programms-session-musculation-auto-programms-session-module */
          "pages-musculation-auto-programms-session-musculation-auto-programms-session-module").then(__webpack_require__.bind(null,
          /*! ./pages/musculation-auto-programms-session/musculation-auto-programms-session.module */
          "./src/app/pages/musculation-auto-programms-session/musculation-auto-programms-session.module.ts")).then(function (m) {
            return m.MusculationAutoProgrammsSessionPageModule;
          });
        }
      }, {
        path: 'start-begin-the-session',
        loadChildren: function loadChildren() {
          return __webpack_require__.e(
          /*! import() | pages-start-begin-the-session-start-begin-the-session-module */
          "pages-start-begin-the-session-start-begin-the-session-module").then(__webpack_require__.bind(null,
          /*! ./pages/start-begin-the-session/start-begin-the-session.module */
          "./src/app/pages/start-begin-the-session/start-begin-the-session.module.ts")).then(function (m) {
            return m.StartBeginTheSessionPageModule;
          });
        }
      }, {
        path: 'after-excercise-selection',
        loadChildren: function loadChildren() {
          return __webpack_require__.e(
          /*! import() | pages-tabs-home-after-excercise-selection-after-excercise-selection-module */
          "pages-tabs-home-after-excercise-selection-after-excercise-selection-module").then(__webpack_require__.bind(null,
          /*! ./pages/tabs/home/after-excercise-selection/after-excercise-selection.module */
          "./src/app/pages/tabs/home/after-excercise-selection/after-excercise-selection.module.ts")).then(function (m) {
            return m.AfterExcerciseSelectionPageModule;
          });
        }
      }, {
        path: 'physical-activity-menu-empty',
        loadChildren: function loadChildren() {
          return __webpack_require__.e(
          /*! import() | pages-tabs-home-physical-activity-menu-empty-physical-activity-menu-empty-module */
          "pages-tabs-home-physical-activity-menu-empty-physical-activity-menu-empty-module").then(__webpack_require__.bind(null,
          /*! ./pages/tabs/home/physical-activity-menu-empty/physical-activity-menu-empty.module */
          "./src/app/pages/tabs/home/physical-activity-menu-empty/physical-activity-menu-empty.module.ts")).then(function (m) {
            return m.PhysicalActivityMenuEmptyPageModule;
          });
        }
      }, {
        path: 'pendant-la-seance',
        loadChildren: function loadChildren() {
          return __webpack_require__.e(
          /*! import() | pages-pendant-la-seance-pendant-la-seance-module */
          "pages-pendant-la-seance-pendant-la-seance-module").then(__webpack_require__.bind(null,
          /*! ./pages/pendant-la-seance/pendant-la-seance.module */
          "./src/app/pages/pendant-la-seance/pendant-la-seance.module.ts")).then(function (m) {
            return m.PendantLaSeancePageModule;
          });
        }
      }, {
        path: 'bilan-post-seance',
        loadChildren: function loadChildren() {
          return __webpack_require__.e(
          /*! import() | pages-bilan-post-seance-bilan-post-seance-module */
          "pages-bilan-post-seance-bilan-post-seance-module").then(__webpack_require__.bind(null,
          /*! ./pages/bilan-post-seance/bilan-post-seance.module */
          "./src/app/pages/bilan-post-seance/bilan-post-seance.module.ts")).then(function (m) {
            return m.BilanPostSeancePageModule;
          });
        }
      }, {
        path: 'add-excercise-for-menu',
        loadChildren: function loadChildren() {
          return __webpack_require__.e(
          /*! import() | pages-tabs-home-add-excercise-for-menu-add-excercise-for-menu-module */
          "pages-tabs-home-add-excercise-for-menu-add-excercise-for-menu-module").then(__webpack_require__.bind(null,
          /*! ./pages/tabs/home/add-excercise-for-menu/add-excercise-for-menu.module */
          "./src/app/pages/tabs/home/add-excercise-for-menu/add-excercise-for-menu.module.ts")).then(function (m) {
            return m.AddExcerciseForMenuPageModule;
          });
        }
      }];

      var AppRoutingModule = function AppRoutingModule() {
        _classCallCheck(this, AppRoutingModule);
      };

      AppRoutingModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forRoot(routes, {
          preloadingStrategy: _angular_router__WEBPACK_IMPORTED_MODULE_2__["PreloadAllModules"]
        })],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]]
      })], AppRoutingModule);
      /***/
    },

    /***/
    "./src/app/app.component.scss":
    /*!************************************!*\
      !*** ./src/app/app.component.scss ***!
      \************************************/

    /*! exports provided: default */

    /***/
    function srcAppAppComponentScss(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony default export */


      __webpack_exports__["default"] = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL2FwcC5jb21wb25lbnQuc2NzcyJ9 */";
      /***/
    },

    /***/
    "./src/app/app.component.ts":
    /*!**********************************!*\
      !*** ./src/app/app.component.ts ***!
      \**********************************/

    /*! exports provided: AppComponent */

    /***/
    function srcAppAppComponentTs(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "AppComponent", function () {
        return AppComponent;
      });
      /* harmony import */


      var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! tslib */
      "./node_modules/tslib/tslib.es6.js");
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! @angular/core */
      "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
      /* harmony import */


      var _ionic_angular__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! @ionic/angular */
      "./node_modules/@ionic/angular/__ivy_ngcc__/fesm2015/ionic-angular.js");
      /* harmony import */


      var _ionic_native_splash_screen_ngx__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
      /*! @ionic-native/splash-screen/ngx */
      "./node_modules/@ionic-native/splash-screen/__ivy_ngcc__/ngx/index.js");
      /* harmony import */


      var _ionic_native_status_bar_ngx__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
      /*! @ionic-native/status-bar/ngx */
      "./node_modules/@ionic-native/status-bar/__ivy_ngcc__/ngx/index.js");
      /* harmony import */


      var _ngx_translate_core__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(
      /*! @ngx-translate/core */
      "./node_modules/@ngx-translate/core/__ivy_ngcc__/fesm2015/ngx-translate-core.js");
      /* harmony import */


      var _services_network_service__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(
      /*! @services/network.service */
      "./src/app/services/network.service.ts");

      var AppComponent = /*#__PURE__*/function () {
        function AppComponent(platform, splashScreen, statusBar, networkSvc, translate) {
          _classCallCheck(this, AppComponent);

          this.platform = platform;
          this.splashScreen = splashScreen;
          this.statusBar = statusBar;
          this.networkSvc = networkSvc;
          this.translate = translate;
          this.initializeApp();
          console.log("Lang");
          translate.addLangs(["en", "fr"]);
          translate.use("fr");
          translate.setDefaultLang("fr");
        }

        _createClass(AppComponent, [{
          key: "initializeApp",
          value: function initializeApp() {
            return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, /*#__PURE__*/regeneratorRuntime.mark(function _callee2() {
              var _this = this;

              var platform;
              return regeneratorRuntime.wrap(function _callee2$(_context2) {
                while (1) {
                  switch (_context2.prev = _context2.next) {
                    case 0:
                      _context2.next = 2;
                      return this.platform.ready();

                    case 2:
                      platform = _context2.sent;
                      this.statusBar.styleDefault();
                      this.splashScreen.hide(); // Listen to the network interface

                      this.networkSvc.listenNetwork();

                      if (!this.platform.is("cordova")) {
                        this.networkSvc.online = true;
                      } // Subscribe for platform pause and resume events


                      this.platform.pause.subscribe(function (e) {
                        console.log("[INFO] Application paused....");
                      });
                      this.platform.resume.subscribe(function (e) {
                        return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(_this, void 0, void 0, /*#__PURE__*/regeneratorRuntime.mark(function _callee() {
                          return regeneratorRuntime.wrap(function _callee$(_context) {
                            while (1) {
                              switch (_context.prev = _context.next) {
                                case 0:
                                  console.log("[INFO] Application resumed....");

                                case 1:
                                case "end":
                                  return _context.stop();
                              }
                            }
                          }, _callee);
                        }));
                      });

                    case 9:
                    case "end":
                      return _context2.stop();
                  }
                }
              }, _callee2, this);
            }));
          }
        }]);

        return AppComponent;
      }();

      AppComponent.ctorParameters = function () {
        return [{
          type: _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["Platform"]
        }, {
          type: _ionic_native_splash_screen_ngx__WEBPACK_IMPORTED_MODULE_3__["SplashScreen"]
        }, {
          type: _ionic_native_status_bar_ngx__WEBPACK_IMPORTED_MODULE_4__["StatusBar"]
        }, {
          type: _services_network_service__WEBPACK_IMPORTED_MODULE_6__["NetworkService"]
        }, {
          type: _ngx_translate_core__WEBPACK_IMPORTED_MODULE_5__["TranslateService"]
        }];
      };

      AppComponent = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-root',
        template: Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(
        /*! raw-loader!./app.component.html */
        "./node_modules/raw-loader/dist/cjs.js!./src/app/app.component.html"))["default"],
        styles: [Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(
        /*! ./app.component.scss */
        "./src/app/app.component.scss"))["default"]]
      })], AppComponent);
      /***/
    },

    /***/
    "./src/app/app.module.ts":
    /*!*******************************!*\
      !*** ./src/app/app.module.ts ***!
      \*******************************/

    /*! exports provided: AppModule, HttpLoaderFactory */

    /***/
    function srcAppAppModuleTs(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "AppModule", function () {
        return AppModule;
      });
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "HttpLoaderFactory", function () {
        return HttpLoaderFactory;
      });
      /* harmony import */


      var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! tslib */
      "./node_modules/tslib/tslib.es6.js");
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! @angular/core */
      "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
      /* harmony import */


      var _angular_platform_browser__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! @angular/platform-browser */
      "./node_modules/@angular/platform-browser/__ivy_ngcc__/fesm2015/platform-browser.js");
      /* harmony import */


      var _angular_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
      /*! @angular/router */
      "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");
      /* harmony import */


      var _angular_common_http__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
      /*! @angular/common/http */
      "./node_modules/@angular/common/__ivy_ngcc__/fesm2015/http.js");
      /* harmony import */


      var _ionic_angular__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(
      /*! @ionic/angular */
      "./node_modules/@ionic/angular/__ivy_ngcc__/fesm2015/ionic-angular.js");
      /* harmony import */


      var _ionic_native_splash_screen_ngx__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(
      /*! @ionic-native/splash-screen/ngx */
      "./node_modules/@ionic-native/splash-screen/__ivy_ngcc__/ngx/index.js");
      /* harmony import */


      var _ionic_native_status_bar_ngx__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(
      /*! @ionic-native/status-bar/ngx */
      "./node_modules/@ionic-native/status-bar/__ivy_ngcc__/ngx/index.js");
      /* harmony import */


      var _ionic_storage__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(
      /*! @ionic/storage */
      "./node_modules/@ionic/storage/__ivy_ngcc__/fesm2015/ionic-storage.js");
      /* harmony import */


      var _app_component__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(
      /*! ./app.component */
      "./src/app/app.component.ts");
      /* harmony import */


      var _app_routing_module__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(
      /*! ./app-routing.module */
      "./src/app/app-routing.module.ts");
      /* harmony import */


      var _pipes_pipes_module__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(
      /*! @pipes/pipes.module */
      "./src/app/pipes/pipes.module.ts");
      /* harmony import */


      var ionic2_calendar__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(
      /*! ionic2-calendar */
      "./node_modules/ionic2-calendar/__ivy_ngcc__/fesm2015/ionic2-calendar.js");
      /* harmony import */


      var _ngx_translate_core__WEBPACK_IMPORTED_MODULE_13__ = __webpack_require__(
      /*! @ngx-translate/core */
      "./node_modules/@ngx-translate/core/__ivy_ngcc__/fesm2015/ngx-translate-core.js");
      /* harmony import */


      var _ngx_translate_http_loader__WEBPACK_IMPORTED_MODULE_14__ = __webpack_require__(
      /*! @ngx-translate/http-loader */
      "./node_modules/@ngx-translate/http-loader/__ivy_ngcc__/fesm2015/ngx-translate-http-loader.js");
      /* harmony import */


      var _components_shared_module__WEBPACK_IMPORTED_MODULE_15__ = __webpack_require__(
      /*! @components/shared.module */
      "./src/app/components/shared.module.ts");
      /* harmony import */


      var _angular_platform_browser_animations__WEBPACK_IMPORTED_MODULE_16__ = __webpack_require__(
      /*! @angular/platform-browser/animations */
      "./node_modules/@angular/platform-browser/__ivy_ngcc__/fesm2015/animations.js"); // import ngx-translate and the http loader


      var AppModule = function AppModule() {
        _classCallCheck(this, AppModule);
      };

      AppModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        declarations: [_app_component__WEBPACK_IMPORTED_MODULE_9__["AppComponent"]],
        entryComponents: [],
        schemas: [_angular_core__WEBPACK_IMPORTED_MODULE_1__["CUSTOM_ELEMENTS_SCHEMA"]],
        imports: [_angular_platform_browser__WEBPACK_IMPORTED_MODULE_2__["BrowserModule"], _ionic_angular__WEBPACK_IMPORTED_MODULE_5__["IonicModule"].forRoot({
          scrollPadding: false,
          scrollAssist: true
        }), ionic2_calendar__WEBPACK_IMPORTED_MODULE_12__["NgCalendarModule"], _app_routing_module__WEBPACK_IMPORTED_MODULE_10__["AppRoutingModule"], _angular_common_http__WEBPACK_IMPORTED_MODULE_4__["HttpClientModule"], _pipes_pipes_module__WEBPACK_IMPORTED_MODULE_11__["PipesModule"], _ionic_storage__WEBPACK_IMPORTED_MODULE_8__["IonicStorageModule"].forRoot(), _ngx_translate_core__WEBPACK_IMPORTED_MODULE_13__["TranslateModule"].forRoot({
          loader: {
            provide: _ngx_translate_core__WEBPACK_IMPORTED_MODULE_13__["TranslateLoader"],
            useFactory: HttpLoaderFactory,
            deps: [_angular_common_http__WEBPACK_IMPORTED_MODULE_4__["HttpClient"]]
          }
        }), _components_shared_module__WEBPACK_IMPORTED_MODULE_15__["SharedModule"], _angular_platform_browser_animations__WEBPACK_IMPORTED_MODULE_16__["NoopAnimationsModule"]],
        providers: [_ionic_native_status_bar_ngx__WEBPACK_IMPORTED_MODULE_7__["StatusBar"], _ionic_native_splash_screen_ngx__WEBPACK_IMPORTED_MODULE_6__["SplashScreen"], {
          provide: _angular_router__WEBPACK_IMPORTED_MODULE_3__["RouteReuseStrategy"],
          useClass: _ionic_angular__WEBPACK_IMPORTED_MODULE_5__["IonicRouteStrategy"]
        }],
        bootstrap: [_app_component__WEBPACK_IMPORTED_MODULE_9__["AppComponent"]]
      })], AppModule);

      function HttpLoaderFactory(http) {
        return new _ngx_translate_http_loader__WEBPACK_IMPORTED_MODULE_14__["TranslateHttpLoader"](http);
      }
      /***/

    },

    /***/
    "./src/app/components/body-scan-step-five/body-scan-step-five.component.scss":
    /*!***********************************************************************************!*\
      !*** ./src/app/components/body-scan-step-five/body-scan-step-five.component.scss ***!
      \***********************************************************************************/

    /*! exports provided: default */

    /***/
    function srcAppComponentsBodyScanStepFiveBodyScanStepFiveComponentScss(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony default export */


      __webpack_exports__["default"] = "ion-card.program {\n  width: 85%;\n  margin-inline: 5px;\n}\n\nion-card-content {\n  -webkit-padding-start: 0px;\n  padding-inline-start: 0px;\n  -webkit-padding-end: 0px;\n  padding-inline-end: 0px;\n}\n\n.swiper-container {\n  margin: 0 auto;\n  position: relative;\n  overflow: hidden;\n  list-style: none;\n  padding: 0;\n  z-index: 1;\n}\n\nion-card.program.active {\n  transform: scale(1.1, 1.1);\n  transition: all 0.5s ease;\n  border: 2px solid var(--ion-color-primary);\n  transition: all 0.5s ease;\n}\n\n.swiper-container {\n  overflow-x: visible;\n  overflow-y: visible;\n}\n\nion-card.program.active:before {\n  content: \"\";\n  background: url('checkmark-circle-outline.svg');\n  height: 25px;\n  width: 25px;\n  right: 5px;\n  top: 5px;\n  position: absolute;\n}\n\n.body-scan-slide2 .md {\n  padding-bottom: 20px;\n}\n\n.body-scan-slide2 .slides-md {\n  --bullet-background: #4e4d4d;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvY29tcG9uZW50cy9ib2R5LXNjYW4tc3RlcC1maXZlL2JvZHktc2Nhbi1zdGVwLWZpdmUuY29tcG9uZW50LnNjc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUE7RUFDSSxVQUFBO0VBQ0Esa0JBQUE7QUFDSjs7QUFFQTtFQUNJLDBCQUFBO0VBQ0EseUJBQUE7RUFDQSx3QkFBQTtFQUNBLHVCQUFBO0FBQ0o7O0FBRUE7RUFDSSxjQUFBO0VBQ0Esa0JBQUE7RUFDQSxnQkFBQTtFQUNBLGdCQUFBO0VBQ0EsVUFBQTtFQUNBLFVBQUE7QUFDSjs7QUFFQTtFQUNJLDBCQUFBO0VBQ0EseUJBQUE7RUFDQSwwQ0FBQTtFQUNBLHlCQUFBO0FBQ0o7O0FBRUE7RUFDSSxtQkFBQTtFQUNBLG1CQUFBO0FBQ0o7O0FBRUE7RUFDSSxXQUFBO0VBQ0EsK0NBQUE7RUFDQSxZQUFBO0VBQ0EsV0FBQTtFQUNBLFVBQUE7RUFDQSxRQUFBO0VBQ0Esa0JBQUE7QUFDSjs7QUFFQTtFQUNJLG9CQUFBO0FBQ0o7O0FBSUE7RUFDSSw0QkFBQTtBQURKIiwiZmlsZSI6InNyYy9hcHAvY29tcG9uZW50cy9ib2R5LXNjYW4tc3RlcC1maXZlL2JvZHktc2Nhbi1zdGVwLWZpdmUuY29tcG9uZW50LnNjc3MiLCJzb3VyY2VzQ29udGVudCI6WyJpb24tY2FyZC5wcm9ncmFtIHtcbiAgICB3aWR0aDogODUlO1xuICAgIG1hcmdpbi1pbmxpbmU6IDVweDtcbn1cblxuaW9uLWNhcmQtY29udGVudCB7XG4gICAgLXdlYmtpdC1wYWRkaW5nLXN0YXJ0OiAwcHg7XG4gICAgcGFkZGluZy1pbmxpbmUtc3RhcnQ6IDBweDtcbiAgICAtd2Via2l0LXBhZGRpbmctZW5kOiAwcHg7XG4gICAgcGFkZGluZy1pbmxpbmUtZW5kOiAwcHg7XG59XG5cbi5zd2lwZXItY29udGFpbmVyIHtcbiAgICBtYXJnaW46IDAgYXV0bztcbiAgICBwb3NpdGlvbjogcmVsYXRpdmU7XG4gICAgb3ZlcmZsb3c6IGhpZGRlbjtcbiAgICBsaXN0LXN0eWxlOiBub25lO1xuICAgIHBhZGRpbmc6IDA7XG4gICAgei1pbmRleDogMTtcbn1cblxuaW9uLWNhcmQucHJvZ3JhbS5hY3RpdmUge1xuICAgIHRyYW5zZm9ybTogc2NhbGUoMS4xLCAxLjEpO1xuICAgIHRyYW5zaXRpb246IGFsbCAwLjVzIGVhc2U7XG4gICAgYm9yZGVyOiAycHggc29saWQgdmFyKC0taW9uLWNvbG9yLXByaW1hcnkpO1xuICAgIHRyYW5zaXRpb246IGFsbCAwLjVzIGVhc2U7XG59XG5cbi5zd2lwZXItY29udGFpbmVyIHtcbiAgICBvdmVyZmxvdy14OiB2aXNpYmxlO1xuICAgIG92ZXJmbG93LXk6IHZpc2libGU7XG59XG5cbmlvbi1jYXJkLnByb2dyYW0uYWN0aXZlOmJlZm9yZSB7XG4gICAgY29udGVudDogXCJcIjtcbiAgICBiYWNrZ3JvdW5kOiB1cmwoLi4vLi4vLi4vYXNzZXRzL2ljb24vY2hlY2ttYXJrLWNpcmNsZS1vdXRsaW5lLnN2Zyk7XG4gICAgaGVpZ2h0OiAyNXB4O1xuICAgIHdpZHRoOiAyNXB4O1xuICAgIHJpZ2h0OiA1cHg7XG4gICAgdG9wOiA1cHg7XG4gICAgcG9zaXRpb246IGFic29sdXRlO1xufVxuXG4uYm9keS1zY2FuLXNsaWRlMiAubWQgeyBcbiAgICBwYWRkaW5nLWJvdHRvbTogMjBweDtcblxufVxuXG5cbi5ib2R5LXNjYW4tc2xpZGUyIC5zbGlkZXMtbWQgIHsgXG4gICAgLS1idWxsZXQtYmFja2dyb3VuZDogIzRlNGQ0ZDtcblxufVxuIl19 */";
      /***/
    },

    /***/
    "./src/app/components/body-scan-step-five/body-scan-step-five.component.ts":
    /*!*********************************************************************************!*\
      !*** ./src/app/components/body-scan-step-five/body-scan-step-five.component.ts ***!
      \*********************************************************************************/

    /*! exports provided: BodyScanStepFiveComponent */

    /***/
    function srcAppComponentsBodyScanStepFiveBodyScanStepFiveComponentTs(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "BodyScanStepFiveComponent", function () {
        return BodyScanStepFiveComponent;
      });
      /* harmony import */


      var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! tslib */
      "./node_modules/tslib/tslib.es6.js");
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! @angular/core */
      "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
      /* harmony import */


      var _angular_forms__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! @angular/forms */
      "./node_modules/@angular/forms/__ivy_ngcc__/fesm2015/forms.js");
      /* harmony import */


      var _ionic_angular__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
      /*! @ionic/angular */
      "./node_modules/@ionic/angular/__ivy_ngcc__/fesm2015/ionic-angular.js");

      var BodyScanStepFiveComponent = /*#__PURE__*/function () {
        function BodyScanStepFiveComponent() {
          _classCallCheck(this, BodyScanStepFiveComponent);

          this.stepFiveSelected = new _angular_core__WEBPACK_IMPORTED_MODULE_1__["EventEmitter"]();
          this.morphotypeArray = [{
            id: 1,
            src: "assets/images/Morphotype men-one.png",
            name: "First Slide",
            activeClass: false
          }, {
            id: 2,
            src: "assets/images/Morphotype men-two.png",
            name: "Second Slide",
            activeClass: false
          }, {
            id: 3,
            src: "assets/images/Morphotype men-three.png",
            name: "Third Slide",
            activeClass: false
          }];
          this.fatMassRateArray = [{
            id: 1,
            src: "assets/images/bodyfat-35.png",
            activeClass: false
          }, {
            id: 2,
            src: "assets/images/bodyfat-30.png",
            activeClass: false
          }, {
            id: 3,
            src: "assets/images/bodyfat-25.png",
            activeClass: false
          }, {
            id: 4,
            src: "assets/images/bodyfat-35.png",
            activeClass: false
          }, {
            id: 5,
            src: "assets/images/bodyfat-20.png",
            activeClass: false
          }, {
            id: 6,
            src: "assets/images/bodyfat-15.png",
            activeClass: false
          }, {
            id: 7,
            src: "assets/images/bodyfat-12.png",
            activeClass: false
          }, {
            id: 8,
            src: "assets/images/bodyfat-8.png",
            activeClass: false
          }];
          this.options = {
            centeredSlides: true,
            effect: 'flip',
            spaceBetween: 0
          };
          this.categories = {
            slidesPerView: 3,
            effect: 'flip',
            slidesPerColumn: 1,
            slidesPerGroup: 1,
            watchSlidesProgress: true
          };
        }

        _createClass(BodyScanStepFiveComponent, [{
          key: "ngOnInit",
          value: function ngOnInit() {
            this.initializeForm();
          }
        }, {
          key: "initializeForm",
          value: function initializeForm() {
            this.stepFiveForm = new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormGroup"]({
              field1: new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormControl"](),
              field2: new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormControl"](),
              field3: new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormControl"](),
              field4: new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormControl"](),
              field5: new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormControl"](),
              field6: new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormControl"]()
            });
            this.onChanges();
          }
        }, {
          key: "donsomething",
          value: function donsomething(event, src) {
            var _this2 = this;

            this.morphotypeArray.forEach(function (obj) {
              if (obj.id == src.id) {
                _this2.stepFiveForm.get('field1').setValue(obj);

                obj.activeClass = true;
              } else {
                obj.activeClass = false;
              }
            });
          }
        }, {
          key: "fatMassRateArraySelect",
          value: function fatMassRateArraySelect(obj) {
            var _this3 = this;

            this.fatMassRateArray.forEach(function (doc) {
              if (doc.id == obj.id) {
                _this3.stepFiveForm.get('field2').setValue(obj);

                doc.activeClass = true;
              } else {
                doc.activeClass = false;
              }
            });
          }
        }, {
          key: "onChanges",
          value: function onChanges() {
            var _this4 = this;

            this.stepFiveForm.valueChanges.subscribe(function (val) {
              _this4.emitChanges();
            });
          }
        }, {
          key: "emitChanges",
          value: function emitChanges() {
            this.stepFiveSelected.emit(this.stepFiveForm.value);
          }
        }]);

        return BodyScanStepFiveComponent;
      }();

      BodyScanStepFiveComponent.ctorParameters = function () {
        return [];
      };

      BodyScanStepFiveComponent.propDecorators = {
        stepFiveSelected: [{
          type: _angular_core__WEBPACK_IMPORTED_MODULE_1__["Output"]
        }],
        stepFiveForm: [{
          type: _angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"]
        }],
        slides: [{
          type: _angular_core__WEBPACK_IMPORTED_MODULE_1__["ViewChild"],
          args: [_ionic_angular__WEBPACK_IMPORTED_MODULE_3__["IonSlides"]]
        }]
      };
      BodyScanStepFiveComponent = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-body-scan-step-five',
        template: Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(
        /*! raw-loader!./body-scan-step-five.component.html */
        "./node_modules/raw-loader/dist/cjs.js!./src/app/components/body-scan-step-five/body-scan-step-five.component.html"))["default"],
        styles: [Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(
        /*! ./body-scan-step-five.component.scss */
        "./src/app/components/body-scan-step-five/body-scan-step-five.component.scss"))["default"]]
      })], BodyScanStepFiveComponent);
      /***/
    },

    /***/
    "./src/app/components/body-scan-step-four/body-scan-step-four.component.scss":
    /*!***********************************************************************************!*\
      !*** ./src/app/components/body-scan-step-four/body-scan-step-four.component.scss ***!
      \***********************************************************************************/

    /*! exports provided: default */

    /***/
    function srcAppComponentsBodyScanStepFourBodyScanStepFourComponentScss(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony default export */


      __webpack_exports__["default"] = ".my-checkbox {\n  display: flex;\n  align-items: center;\n  float: left;\n  margin-right: 15px;\n}\n\n.my-checkbox ion-label {\n  padding-right: 5px;\n}\n\nion-item ion-label {\n  white-space: normal;\n}\n\nion-item {\n  --inner-border-width: 0px 0px 0px 0px;\n  --min-height: 35px;\n  --padding-start: 0px;\n  --border-width: 0 0 0px 0;\n}\n\nion-list {\n  padding-top: 0px !important;\n  padding-bottom: 0px !important;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvY29tcG9uZW50cy9ib2R5LXNjYW4tc3RlcC1mb3VyL2JvZHktc2Nhbi1zdGVwLWZvdXIuY29tcG9uZW50LnNjc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUE7RUFDSSxhQUFBO0VBQ0EsbUJBQUE7RUFDQSxXQUFBO0VBQ0Esa0JBQUE7QUFDSjs7QUFFQTtFQUNJLGtCQUFBO0FBQ0o7O0FBRUE7RUFDSSxtQkFBQTtBQUNKOztBQUVBO0VBQ0kscUNBQUE7RUFDQSxrQkFBQTtFQUNBLG9CQUFBO0VBQ0EseUJBQUE7QUFDSjs7QUFFQTtFQUNJLDJCQUFBO0VBQ0EsOEJBQUE7QUFDSiIsImZpbGUiOiJzcmMvYXBwL2NvbXBvbmVudHMvYm9keS1zY2FuLXN0ZXAtZm91ci9ib2R5LXNjYW4tc3RlcC1mb3VyLmNvbXBvbmVudC5zY3NzIiwic291cmNlc0NvbnRlbnQiOlsiLm15LWNoZWNrYm94IHtcbiAgICBkaXNwbGF5OiBmbGV4O1xuICAgIGFsaWduLWl0ZW1zOiBjZW50ZXI7XG4gICAgZmxvYXQ6IGxlZnQ7XG4gICAgbWFyZ2luLXJpZ2h0OiAxNXB4O1xufVxuXG4ubXktY2hlY2tib3ggaW9uLWxhYmVsIHtcbiAgICBwYWRkaW5nLXJpZ2h0OiA1cHg7XG59XG5cbmlvbi1pdGVtIGlvbi1sYWJlbCB7XG4gICAgd2hpdGUtc3BhY2U6IG5vcm1hbDtcbn1cblxuaW9uLWl0ZW0ge1xuICAgIC0taW5uZXItYm9yZGVyLXdpZHRoOiAwcHggMHB4IDBweCAwcHg7XG4gICAgLS1taW4taGVpZ2h0OiAzNXB4O1xuICAgIC0tcGFkZGluZy1zdGFydDogMHB4O1xuICAgIC0tYm9yZGVyLXdpZHRoOiAwIDAgMHB4IDA7XG59XG5cbmlvbi1saXN0IHtcbiAgICBwYWRkaW5nLXRvcDogMHB4ICFpbXBvcnRhbnQ7XG4gICAgcGFkZGluZy1ib3R0b206IDBweCAhaW1wb3J0YW50O1xufSJdfQ== */";
      /***/
    },

    /***/
    "./src/app/components/body-scan-step-four/body-scan-step-four.component.ts":
    /*!*********************************************************************************!*\
      !*** ./src/app/components/body-scan-step-four/body-scan-step-four.component.ts ***!
      \*********************************************************************************/

    /*! exports provided: BodyScanStepFourComponent */

    /***/
    function srcAppComponentsBodyScanStepFourBodyScanStepFourComponentTs(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "BodyScanStepFourComponent", function () {
        return BodyScanStepFourComponent;
      });
      /* harmony import */


      var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! tslib */
      "./node_modules/tslib/tslib.es6.js");
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! @angular/core */
      "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
      /* harmony import */


      var _angular_forms__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! @angular/forms */
      "./node_modules/@angular/forms/__ivy_ngcc__/fesm2015/forms.js");

      var BodyScanStepFourComponent = /*#__PURE__*/function () {
        function BodyScanStepFourComponent() {
          _classCallCheck(this, BodyScanStepFourComponent);

          this.stepFourSelected = new _angular_core__WEBPACK_IMPORTED_MODULE_1__["EventEmitter"]();
          this.showMeds = false;
          this.showIntolerance = false;
        }

        _createClass(BodyScanStepFourComponent, [{
          key: "ngOnInit",
          value: function ngOnInit() {
            this.initializeForm();
          }
        }, {
          key: "initializeForm",
          value: function initializeForm() {
            this.stepFourForm = new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormGroup"]({
              field1: new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormControl"](),
              field2: new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormControl"](),
              field3: new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormControl"](),
              field4: new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormControl"](),
              field5: new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormControl"](),
              field6: new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormControl"]()
            });
            this.onChanges();
          }
        }, {
          key: "showMedicines",
          value: function showMedicines(event) {
            console.log(event);

            if (event.detail.value == 'yes' && event.detail.checked == true) {
              this.showMeds = true;
            } else {
              this.showMeds = false;
            }
          }
        }, {
          key: "hideMedicines",
          value: function hideMedicines(event) {
            console.log(event);

            if (event.detail.value == 'no' && event.detail.checked == true) {
              this.showMeds = false;
            }
          }
        }, {
          key: "showIntolenrances",
          value: function showIntolenrances(event) {
            console.log(event);

            if (event.detail.value == 'yes' && event.detail.checked == true) {
              this.showIntolerance = true;
            } else {
              this.showIntolerance = false;
            }
          }
        }, {
          key: "hideIntolenrances",
          value: function hideIntolenrances(event) {
            console.log(event);

            if (event.detail.value == 'no' && event.detail.checked == true) {
              this.showIntolerance = false;
            }
          }
        }, {
          key: "onChanges",
          value: function onChanges() {
            var _this5 = this;

            this.stepFourForm.valueChanges.subscribe(function (val) {
              _this5.emitChanges();
            });
          }
        }, {
          key: "emitChanges",
          value: function emitChanges() {
            this.stepFourSelected.emit(this.stepFourForm.value);
          }
        }, {
          key: "onMedicineSelection",
          value: function onMedicineSelection(event) {
            this.stepFourForm.get('field2').setValue(event.detail.value);
          }
        }, {
          key: "onIntolerenceSelection",
          value: function onIntolerenceSelection(event) {
            this.stepFourForm.get('field3').setValue(event.detail.value);
          }
        }]);

        return BodyScanStepFourComponent;
      }();

      BodyScanStepFourComponent.ctorParameters = function () {
        return [];
      };

      BodyScanStepFourComponent.propDecorators = {
        stepFourSelected: [{
          type: _angular_core__WEBPACK_IMPORTED_MODULE_1__["Output"]
        }],
        stepFourForm: [{
          type: _angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"]
        }]
      };
      BodyScanStepFourComponent = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-body-scan-step-four',
        template: Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(
        /*! raw-loader!./body-scan-step-four.component.html */
        "./node_modules/raw-loader/dist/cjs.js!./src/app/components/body-scan-step-four/body-scan-step-four.component.html"))["default"],
        styles: [Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(
        /*! ./body-scan-step-four.component.scss */
        "./src/app/components/body-scan-step-four/body-scan-step-four.component.scss"))["default"]]
      })], BodyScanStepFourComponent);
      /***/
    },

    /***/
    "./src/app/components/body-scan-step-one/body-scan-step-one.component.scss":
    /*!*********************************************************************************!*\
      !*** ./src/app/components/body-scan-step-one/body-scan-step-one.component.scss ***!
      \*********************************************************************************/

    /*! exports provided: default */

    /***/
    function srcAppComponentsBodyScanStepOneBodyScanStepOneComponentScss(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony default export */


      __webpack_exports__["default"] = ".input-price {\n  display: flex;\n  align-items: center;\n  justify-content: center;\n  font-family: Oswald, sans-serif;\n}\n\n.display-price {\n  padding: 5px;\n  font-weight: bold;\n  font-size: 14px;\n  font-family: Oswald, sans-serif;\n}\n\nion-item ion-label {\n  white-space: normal;\n  font-family: Oswald, sans-serif;\n}\n\nion-item {\n  --inner-border-width: 0px 0px 0px 0px;\n}\n\n.sports-box ion-button {\n  height: 80px;\n  --ion-button-round-border-radius: 5px;\n  --ion-button-border-radius: 5px;\n  --ion-border-radius: 5px;\n  --border-radius: 5px;\n}\n\n.sports-box ion-button ion-icon {\n  font-size: 50px;\n}\n\n.gender ion-button {\n  white-space: normal;\n  font-size: 12px;\n  --padding-start: 0;\n  --padding-end: 0;\n  height: 50px;\n  font-family: Oswald, sans-serif;\n}\n\n.calendar-icon {\n  position: absolute;\n  right: 5px;\n  bottom: 4px;\n}\n\nion-item {\n  --inner-border-width: 0px 0px 0px 0px;\n  --min-height: 35px;\n  --padding-start: 0px;\n  --border-width: 0 0 0px 0;\n}\n\nion-list {\n  padding-top: 0px !important;\n  padding-bottom: 0px !important;\n}\n\nion-range {\n  --knob-background: var(--ion-color-primary);\n  --bar-height: 8px;\n  --knob-size: 30px;\n  --height: 20px;\n  --knob-border-radius: 22%;\n  --bar-background-active: var(--ion-color-primary);\n}\n\n.dateDeNa {\n  padding: 10px 0px;\n}\n\n.activitePhysiqueExterne {\n  padding: 25px 0px;\n}\n\nion-card-content {\n  -webkit-padding-end: 0px !important;\n          padding-inline-end: 0px !important;\n  font-family: Oswald, sans-serif;\n}\n\nion-segment-button {\n  --ripple-color: transparent;\n  background: transparent;\n  --color-checked: white;\n  --color: #9c9c9c;\n  --indicator-color: var(--ion-color-primary);\n  --margin-bottom: 0px;\n  height: 40px;\n  --border-radius: 25px;\n}\n\nion-segment {\n  background: rgba(170, 170, 170, 0.11);\n  border-radius: 25px;\n}\n\n.segment-box {\n  margin-bottom: 25px;\n}\n\n.remove {\n  color: #000;\n}\n\n.add {\n  color: #000;\n}\n\n.sports-box ion-button {\n  --background: rgb(0 0 0 / 50%);\n}\n\n.sports-box ion-button.active {\n  --background: var(--ion-color-primary);\n}\n\n.steper-heading {\n  font-family: Oswald;\n}\n\n.steper-title {\n  font-family: Oswald, sans-serif;\n}\n\n.ion_img_icon {\n  padding: 15%;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvY29tcG9uZW50cy9ib2R5LXNjYW4tc3RlcC1vbmUvYm9keS1zY2FuLXN0ZXAtb25lLmNvbXBvbmVudC5zY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBO0VBQ0ksYUFBQTtFQUNBLG1CQUFBO0VBQ0EsdUJBQUE7RUFDQSwrQkFBQTtBQUNKOztBQUVBO0VBQ0ksWUFBQTtFQUNBLGlCQUFBO0VBQ0EsZUFBQTtFQUNBLCtCQUFBO0FBQ0o7O0FBRUE7RUFDSSxtQkFBQTtFQUNBLCtCQUFBO0FBQ0o7O0FBRUE7RUFDSSxxQ0FBQTtBQUNKOztBQUVBO0VBQ0ksWUFBQTtFQUNBLHFDQUFBO0VBQ0EsK0JBQUE7RUFDQSx3QkFBQTtFQUNBLG9CQUFBO0FBQ0o7O0FBRUE7RUFDSSxlQUFBO0FBQ0o7O0FBRUE7RUFDSSxtQkFBQTtFQUNBLGVBQUE7RUFDQSxrQkFBQTtFQUNBLGdCQUFBO0VBQ0EsWUFBQTtFQUNBLCtCQUFBO0FBQ0o7O0FBRUE7RUFDSSxrQkFBQTtFQUNBLFVBQUE7RUFDQSxXQUFBO0FBQ0o7O0FBRUE7RUFDSSxxQ0FBQTtFQUNBLGtCQUFBO0VBQ0Esb0JBQUE7RUFDQSx5QkFBQTtBQUNKOztBQUVBO0VBQ0ksMkJBQUE7RUFDQSw4QkFBQTtBQUNKOztBQUVBO0VBQ0ksMkNBQUE7RUFDQSxpQkFBQTtFQUNBLGlCQUFBO0VBQ0EsY0FBQTtFQUNBLHlCQUFBO0VBQ0EsaURBQUE7QUFDSjs7QUFFQTtFQUNJLGlCQUFBO0FBQ0o7O0FBRUE7RUFDSSxpQkFBQTtBQUNKOztBQUVBO0VBQ0ksbUNBQUE7VUFBQSxrQ0FBQTtFQUNBLCtCQUFBO0FBQ0o7O0FBRUE7RUFDSSwyQkFBQTtFQUNBLHVCQUFBO0VBQ0Esc0JBQUE7RUFDQSxnQkFBQTtFQUNBLDJDQUFBO0VBQ0Esb0JBQUE7RUFDQSxZQUFBO0VBQ0EscUJBQUE7QUFDSjs7QUFFQTtFQUNJLHFDQUFBO0VBQ0EsbUJBQUE7QUFDSjs7QUFFQTtFQUNJLG1CQUFBO0FBQ0o7O0FBRUE7RUFDSSxXQUFBO0FBQ0o7O0FBRUE7RUFDSSxXQUFBO0FBQ0o7O0FBRUE7RUFDSSw4QkFBQTtBQUNKOztBQUVBO0VBQ0ksc0NBQUE7QUFDSjs7QUFDQTtFQUNJLG1CQUFBO0FBRUo7O0FBQUE7RUFDSSwrQkFBQTtBQUdKOztBQUFBO0VBQ0ksWUFBQTtBQUdKIiwiZmlsZSI6InNyYy9hcHAvY29tcG9uZW50cy9ib2R5LXNjYW4tc3RlcC1vbmUvYm9keS1zY2FuLXN0ZXAtb25lLmNvbXBvbmVudC5zY3NzIiwic291cmNlc0NvbnRlbnQiOlsiLmlucHV0LXByaWNlIHtcbiAgICBkaXNwbGF5OiBmbGV4O1xuICAgIGFsaWduLWl0ZW1zOiBjZW50ZXI7XG4gICAganVzdGlmeS1jb250ZW50OiBjZW50ZXI7XG4gICAgZm9udC1mYW1pbHk6IE9zd2FsZCwgc2Fucy1zZXJpZjtcbn1cblxuLmRpc3BsYXktcHJpY2Uge1xuICAgIHBhZGRpbmc6IDVweDtcbiAgICBmb250LXdlaWdodDogYm9sZDtcbiAgICBmb250LXNpemU6IDE0cHg7XG4gICAgZm9udC1mYW1pbHk6IE9zd2FsZCwgc2Fucy1zZXJpZjtcbn1cblxuaW9uLWl0ZW0gaW9uLWxhYmVsIHtcbiAgICB3aGl0ZS1zcGFjZTogbm9ybWFsO1xuICAgIGZvbnQtZmFtaWx5OiBPc3dhbGQsIHNhbnMtc2VyaWY7XG59XG5cbmlvbi1pdGVtIHtcbiAgICAtLWlubmVyLWJvcmRlci13aWR0aDogMHB4IDBweCAwcHggMHB4O1xufVxuXG4uc3BvcnRzLWJveCBpb24tYnV0dG9uIHtcbiAgICBoZWlnaHQ6IDgwcHg7XG4gICAgLS1pb24tYnV0dG9uLXJvdW5kLWJvcmRlci1yYWRpdXM6IDVweDtcbiAgICAtLWlvbi1idXR0b24tYm9yZGVyLXJhZGl1czogNXB4O1xuICAgIC0taW9uLWJvcmRlci1yYWRpdXM6IDVweDtcbiAgICAtLWJvcmRlci1yYWRpdXM6IDVweDtcbn1cblxuLnNwb3J0cy1ib3ggaW9uLWJ1dHRvbiBpb24taWNvbiB7XG4gICAgZm9udC1zaXplOiA1MHB4O1xufVxuXG4uZ2VuZGVyIGlvbi1idXR0b24ge1xuICAgIHdoaXRlLXNwYWNlOiBub3JtYWw7XG4gICAgZm9udC1zaXplOiAxMnB4O1xuICAgIC0tcGFkZGluZy1zdGFydDogMDtcbiAgICAtLXBhZGRpbmctZW5kOiAwO1xuICAgIGhlaWdodDogNTBweDtcbiAgICBmb250LWZhbWlseTogT3N3YWxkLCBzYW5zLXNlcmlmO1xufVxuXG4uY2FsZW5kYXItaWNvbiB7XG4gICAgcG9zaXRpb246IGFic29sdXRlO1xuICAgIHJpZ2h0OiA1cHg7XG4gICAgYm90dG9tOiA0cHg7XG59XG5cbmlvbi1pdGVtIHtcbiAgICAtLWlubmVyLWJvcmRlci13aWR0aDogMHB4IDBweCAwcHggMHB4O1xuICAgIC0tbWluLWhlaWdodDogMzVweDtcbiAgICAtLXBhZGRpbmctc3RhcnQ6IDBweDtcbiAgICAtLWJvcmRlci13aWR0aDogMCAwIDBweCAwO1xufVxuXG5pb24tbGlzdCB7XG4gICAgcGFkZGluZy10b3A6IDBweCAhaW1wb3J0YW50O1xuICAgIHBhZGRpbmctYm90dG9tOiAwcHggIWltcG9ydGFudDtcbn1cblxuaW9uLXJhbmdlIHtcbiAgICAtLWtub2ItYmFja2dyb3VuZDogdmFyKC0taW9uLWNvbG9yLXByaW1hcnkpO1xuICAgIC0tYmFyLWhlaWdodDogOHB4O1xuICAgIC0ta25vYi1zaXplOiAzMHB4O1xuICAgIC0taGVpZ2h0OiAyMHB4O1xuICAgIC0ta25vYi1ib3JkZXItcmFkaXVzOiAyMiU7XG4gICAgLS1iYXItYmFja2dyb3VuZC1hY3RpdmU6IHZhcigtLWlvbi1jb2xvci1wcmltYXJ5KTtcbn1cblxuLmRhdGVEZU5hIHtcbiAgICBwYWRkaW5nOiAxMHB4IDBweDtcbn1cblxuLmFjdGl2aXRlUGh5c2lxdWVFeHRlcm5lIHtcbiAgICBwYWRkaW5nOiAyNXB4IDBweDtcbn1cblxuaW9uLWNhcmQtY29udGVudCB7XG4gICAgcGFkZGluZy1pbmxpbmUtZW5kOiAwcHggIWltcG9ydGFudDtcbiAgICBmb250LWZhbWlseTogT3N3YWxkLCBzYW5zLXNlcmlmO1xufVxuXG5pb24tc2VnbWVudC1idXR0b24ge1xuICAgIC0tcmlwcGxlLWNvbG9yOiB0cmFuc3BhcmVudDtcbiAgICBiYWNrZ3JvdW5kOiB0cmFuc3BhcmVudDtcbiAgICAtLWNvbG9yLWNoZWNrZWQ6IHdoaXRlO1xuICAgIC0tY29sb3I6ICM5YzljOWM7XG4gICAgLS1pbmRpY2F0b3ItY29sb3I6IHZhcigtLWlvbi1jb2xvci1wcmltYXJ5KTtcbiAgICAtLW1hcmdpbi1ib3R0b206IDBweDtcbiAgICBoZWlnaHQ6IDQwcHg7XG4gICAgLS1ib3JkZXItcmFkaXVzOiAyNXB4O1xufVxuXG5pb24tc2VnbWVudCB7XG4gICAgYmFja2dyb3VuZDogcmdiKDE3MCAxNzAgMTcwIC8gMTElKTtcbiAgICBib3JkZXItcmFkaXVzOiAyNXB4O1xufVxuXG4uc2VnbWVudC1ib3gge1xuICAgIG1hcmdpbi1ib3R0b206IDI1cHg7XG59XG5cbi5yZW1vdmUge1xuICAgIGNvbG9yOiAjMDAwO1xufVxuXG4uYWRkIHtcbiAgICBjb2xvcjogIzAwMDtcbn1cblxuLnNwb3J0cy1ib3ggaW9uLWJ1dHRvbiB7XG4gICAgLS1iYWNrZ3JvdW5kOiByZ2IoMCAwIDAgLyA1MCUpO1xufVxuXG4uc3BvcnRzLWJveCBpb24tYnV0dG9uLmFjdGl2ZSB7XG4gICAgLS1iYWNrZ3JvdW5kOiB2YXIoLS1pb24tY29sb3ItcHJpbWFyeSk7XG59XG4uc3RlcGVyLWhlYWRpbmcge1xuICAgIGZvbnQtZmFtaWx5OiBPc3dhbGQ7XG59XG4uc3RlcGVyLXRpdGxlIHtcbiAgICBmb250LWZhbWlseTogT3N3YWxkLCBzYW5zLXNlcmlmO1xufVxuXG4uaW9uX2ltZ19pY29uIHtcbiAgICBwYWRkaW5nOiAxNSU7XG59XG5cblxuXG4iXX0= */";
      /***/
    },

    /***/
    "./src/app/components/body-scan-step-one/body-scan-step-one.component.ts":
    /*!*******************************************************************************!*\
      !*** ./src/app/components/body-scan-step-one/body-scan-step-one.component.ts ***!
      \*******************************************************************************/

    /*! exports provided: BodyScanStepOneComponent */

    /***/
    function srcAppComponentsBodyScanStepOneBodyScanStepOneComponentTs(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "BodyScanStepOneComponent", function () {
        return BodyScanStepOneComponent;
      });
      /* harmony import */


      var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! tslib */
      "./node_modules/tslib/tslib.es6.js");
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! @angular/core */
      "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
      /* harmony import */


      var _angular_forms__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! @angular/forms */
      "./node_modules/@angular/forms/__ivy_ngcc__/fesm2015/forms.js");
      /* harmony import */


      var _services_ionic_toast_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
      /*! @services/ionic/toast.service */
      "./src/app/services/ionic/toast.service.ts");

      var BodyScanStepOneComponent = /*#__PURE__*/function () {
        function BodyScanStepOneComponent(toast) {
          _classCallCheck(this, BodyScanStepOneComponent);

          this.toast = toast;
          this.segmentModel = "favorites";
          this.stepOneSelected = new _angular_core__WEBPACK_IMPORTED_MODULE_1__["EventEmitter"]();
          this.gender = 'male';
          this.training_days = [{
            id: 1,
            name: 'Sommeil, position allongée',
            quantity: 0
          }, {
            id: 2,
            name: 'Position assise',
            quantity: 0
          }, {
            id: 3,
            name: 'Position debout, activité de faible intensité',
            quantity: 0
          }, {
            id: 4,
            name: 'Activité d’intensité moyenne (marche)',
            quantity: 0
          }, {
            id: 5,
            name: 'Activité à haute intensité (marche rapide, sport)',
            quantity: 0
          }, {
            id: 6,
            name: 'Sport, activité intense',
            quantity: 0
          }];
          this.days_off = [{
            id: 1,
            name: 'Sommeil, position allongée',
            quantity: 0
          }, {
            id: 2,
            name: 'Position assise',
            quantity: 0
          }, {
            id: 3,
            name: 'Position debout, activité de faible intensité',
            quantity: 0
          }, {
            id: 4,
            name: 'Activité d’intensité moyenne (marche)',
            quantity: 0
          }, {
            id: 5,
            name: 'Activité à haute intensité (marche rapide, sport)',
            quantity: 0
          }, {
            id: 6,
            name: 'Sport, activité intense',
            quantity: 0
          }];
          this.select_sports = [{
            id: 1,
            icon: 'assets/icon/sports/Football.png',
            name: 'Football',
            isActive: false
          }, {
            id: 2,
            icon: 'assets/icon/sports/Badminton.png',
            name: 'Badminton',
            isActive: false
          }, {
            id: 3,
            icon: 'assets/icon/sports/Basketball.png',
            name: 'Basketball',
            isActive: false
          }, {
            id: 4,
            icon: 'assets/icon/sports/Volleyball.png',
            name: 'Volleyball',
            isActive: false
          }, {
            id: 5,
            icon: 'assets/icon/sports/Tennis.png',
            name: 'Tennis',
            isActive: false
          }, {
            id: 6,
            icon: 'assets/icon/sports/Aquabike.png',
            name: 'Aquabike',
            isActive: false
          }, {
            id: 7,
            icon: 'assets/icon/sports/Aquagym.png',
            name: 'Aquagym',
            isActive: false
          }, {
            id: 8,
            icon: 'assets/icon/sports/Arts martiaux.png',
            name: 'Arts martiaux',
            isActive: false
          }, {
            id: 9,
            icon: 'assets/icon/sports/Aviron.png',
            name: 'Aviron',
            isActive: false
          }, {
            id: 10,
            icon: 'assets/icon/sports/Boxe.png',
            name: 'Boxe',
            isActive: false
          }, {
            id: 12,
            icon: 'assets/icon/sports/Cours collectifs.png',
            name: 'Cours collectifs',
            isActive: false
          }, {
            id: 13,
            icon: 'assets/icon/sports/Haltérophilie.png',
            name: 'Haltérophilie',
            isActive: false
          }, {
            id: 14,
            icon: 'assets/icon/sports/Handball.png',
            name: 'Handball',
            isActive: false
          }, {
            id: 15,
            icon: 'assets/icon/sports/Natation.png',
            name: 'Natation',
            isActive: false
          }, {
            id: 16,
            icon: 'assets/icon/sports/Planche à voile.png',
            name: 'Planche à voile',
            isActive: false
          }, {
            id: 17,
            icon: 'assets/icon/sports/Plongée.png',
            name: 'Plongée',
            isActive: false
          }, {
            id: 18,
            icon: 'assets/icon/sports/Rugby.png',
            name: 'Rugby',
            isActive: false
          }, {
            id: 19,
            icon: 'assets/icon/sports/Surf.png',
            name: 'Surf',
            isActive: false
          }, {
            id: 20,
            icon: 'assets/icon/sports/Water polo.png',
            name: 'Water polo',
            isActive: false
          }, {
            id: 21,
            icon: 'assets/icon/sports/Tennis de table.png',
            name: 'Tennis de table',
            isActive: false
          }, {
            id: 22,
            icon: 'assets/icon/sports/Yoga.png',
            name: 'Tennis de table',
            isActive: false
          }];
          this.pressedBtnArray = [];
          this.weightMinRange = 0;
          this.weightChangeRange = 200;
          this.heightMinRange = 0;
          this.heightChangeRange = 250;
          this.totalTrainingHours = 0;
          this.totalDaysHours = 0;
          this.selectedDate = "1994-12-15T13:47:20.789";
        }

        _createClass(BodyScanStepOneComponent, [{
          key: "ngOnInit",
          value: function ngOnInit() {
            this.initializeForm();
            this.total_trainings_quantity = this.training_days.reduce(function (a, b) {
              // return a.quantity + b;
              return a + b.quantity;
            }, 0);
            this.total_days_off_quantity = this.days_off.reduce(function (a, b) {
              // return a.quantity + b;
              return a + b.quantity;
            }, 0); // console.log("sum", sum)
            //reduce training

            this.checkTotalTrainingHours();
            this.checkDaysHours();
          }
        }, {
          key: "initializeForm",
          value: function initializeForm() {
            this.stepOneForm = new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormGroup"]({
              gender: new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormControl"]('male'),
              birthday_date: new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormControl"](this.selectedDate),
              weight: new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormControl"](''),
              height: new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormControl"](''),
              favorites: new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormControl"](),
              profile: new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormControl"](),
              sports: new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormControl"]()
            });
            this.onChanges();
          }
        }, {
          key: "changeGender",
          value: function changeGender(value) {
            this.gender = value;
            this.stepOneForm.get('gender').setValue(value); // this.stepOneSelected.emit(value);
          }
        }, {
          key: "checkTotalTrainingHours",
          value: function checkTotalTrainingHours() {
            this.totalTrainingHours = this.training_days.reduce(function (acc, obj) {
              return acc + obj.quantity;
            }, 0); // 7

            this.emitChanges();
            console.log("aaa", this.totalTrainingHours);
          }
        }, {
          key: "checkDaysHours",
          value: function checkDaysHours() {
            this.totalDaysHours = this.days_off.reduce(function (acc, obj) {
              return acc + obj.quantity;
            }, 0); // 7

            this.emitChanges();
            console.log("aaa", this.totalTrainingHours);
          }
        }, {
          key: "heightChange",
          value: function heightChange(event) {
            this.heightChangeRange = event.detail.value;
            this.stepOneForm.get('height').setValue(event.detail.value);
          }
        }, {
          key: "weightChange",
          value: function weightChange(event) {
            this.weightChangeRange = event.detail.value;
            this.stepOneForm.get('weight').setValue(event.detail.value);
          }
        }, {
          key: "segmentChanged",
          value: function segmentChanged(event) {
            console.log(this.segmentModel);
            console.log(event.detail.value);
            this.segmentModel = event.detail.value;
          }
        }, {
          key: "increase",
          value: function increase(id) {
            var _this6 = this;

            var filteredData = this.training_days.filter(function (item) {
              if (item.id === id) {
                if (_this6.totalTrainingHours != 24) {
                  if (item.quantity >= 24) {
                    return item;
                  } else {
                    item.quantity += 1;
                  }
                } else {
                  _this6.toast.present("Le nombre total d'heures ne doit pas dépasser 24");

                  return item;
                }
              }

              return item;
            });
            console.log(filteredData);
            this.total_trainings_quantity = filteredData.reduce(function (a, b) {
              // return a.quantity + b;
              return a + b.quantity;
            }, 0);
            this.training_days = filteredData;
            this.checkTotalTrainingHours();
          }
        }, {
          key: "decrease",
          value: function decrease(id) {
            var filteredData = this.training_days.filter(function (item) {
              if (item.id === id) {
                if (item.quantity <= 1) {
                  item.quantity = 1; // return;
                } else {
                  item.quantity -= 1;
                }
              }

              return item;
            });
            this.checkTotalTrainingHours(); // this.total_trainings_quantity = filteredData.reduce((a, b) => {
            //   // return a.quantity + b;
            //   return a + b.quantity; 
            // }, 0);

            this.training_days = filteredData;
          }
        }, {
          key: "increaseDaysOff",
          value: function increaseDaysOff(id) {
            var _this7 = this;

            var filteredData = this.days_off.filter(function (item) {
              if (item.id === id) {
                if (_this7.totalDaysHours != 24) {
                  if (item.quantity >= 24) {
                    return item;
                  } else {
                    item.quantity += 1;
                  }
                } else {
                  _this7.toast.present("Le nombre total d'heures ne doit pas dépasser 24");

                  return item;
                }
              }

              return item;
            });
            console.log(filteredData); // this.total_days_off_quantity = filteredData.reduce((a, b) => {
            //   // return a.quantity + b;
            //   return a + b.quantity; 
            // }, 0);

            this.days_off = filteredData;
            this.checkDaysHours();
          }
        }, {
          key: "decreaseDaysOff",
          value: function decreaseDaysOff(id) {
            var _this8 = this;

            var filteredData = this.days_off.filter(function (item) {
              if (item.id === id) {
                if (item.quantity <= 1) {
                  item.quantity = 1; // return;
                } else {
                  item.quantity -= 1;
                }
              }

              _this8.checkDaysHours();

              return item;
            });
            this.total_days_off_quantity = filteredData.reduce(function (a, b) {
              // return a.quantity + b;
              return a + b.quantity;
            }, 0);
            this.days_off = filteredData;
          }
        }, {
          key: "clickSportButton",
          value: function clickSportButton(btn) {
            console.log(btn);
            btn.isActive = !btn.isActive; // this.pressedBtnArray.push(btn)

            console.log(this.select_sports);
            var newString = JSON.stringify(this.select_sports.filter(function (item) {
              return item.isActive;
            }));
            newString = newString.replace('"', '\"');
            this.stepOneForm.get('sports').setValue(newString); // this.stepOneForm.get('sports').setValue(JSON.stringify(this.select_sports.filter((item) => { return item.isActive })));

            this.emitChanges(); // if(btn.isActive == 1) {
            //   btn.isActive = 0;
            // }
            // let filteredData = this.select_sports.filter((item) => {
            //   if (item.id == id) {
            //     item.isActive = 1;
            //   } else {
            //     item.isActive = 0;
            //   }
            //   return item;
            // })
            // this.select_sports = filteredData;
            // console.log(this.select_sports)
          }
        }, {
          key: "onChanges",
          value: function onChanges() {
            var _this9 = this;

            this.stepOneForm.valueChanges.subscribe(function (val) {
              // this.stepOneSelected.emit(JSON.stringify(val));
              _this9.emitChanges();
            });
          }
        }, {
          key: "emitChanges",
          value: function emitChanges() {
            // console.log();
            var favorites = JSON.stringify(this.stepOneForm.value.favorites);
            var profile = JSON.stringify(this.stepOneForm.value.profile); // this.stepOneForm.get('favorites').setValue(favorites.toString());
            // this.stepOneForm.get('profile').setValue(JSON.stringify(this.stepOneForm.value.profile));

            this.stepOneSelected.emit(this.stepOneForm.value);
          }
        }, {
          key: "changeDOB",
          value: function changeDOB(event) {
            this.stepOneForm.get('birthday_date').setValue(event.detail.value);
          }
        }]);

        return BodyScanStepOneComponent;
      }();

      BodyScanStepOneComponent.ctorParameters = function () {
        return [{
          type: _services_ionic_toast_service__WEBPACK_IMPORTED_MODULE_3__["ToasterService"]
        }];
      };

      BodyScanStepOneComponent.propDecorators = {
        stepOneSelected: [{
          type: _angular_core__WEBPACK_IMPORTED_MODULE_1__["Output"]
        }],
        stepOneForm: [{
          type: _angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"]
        }]
      };
      BodyScanStepOneComponent = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-body-scan-step-one',
        template: Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(
        /*! raw-loader!./body-scan-step-one.component.html */
        "./node_modules/raw-loader/dist/cjs.js!./src/app/components/body-scan-step-one/body-scan-step-one.component.html"))["default"],
        styles: [Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(
        /*! ./body-scan-step-one.component.scss */
        "./src/app/components/body-scan-step-one/body-scan-step-one.component.scss"))["default"]]
      })], BodyScanStepOneComponent);
      /***/
    },

    /***/
    "./src/app/components/body-scan-step-three/body-scan-step-three.component.scss":
    /*!*************************************************************************************!*\
      !*** ./src/app/components/body-scan-step-three/body-scan-step-three.component.scss ***!
      \*************************************************************************************/

    /*! exports provided: default */

    /***/
    function srcAppComponentsBodyScanStepThreeBodyScanStepThreeComponentScss(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony default export */


      __webpack_exports__["default"] = ".my-checkbox {\n  display: flex;\n  align-items: center;\n  float: left;\n  margin-right: 15px;\n}\n\n.my-checkbox ion-label {\n  padding-right: 5px;\n}\n\nion-item ion-label {\n  white-space: normal;\n  padding-left: 10px;\n}\n\nion-item {\n  --inner-border-width: 0px 0px 0px 0px;\n  --min-height: 35px;\n  --padding-start: 0px;\n}\n\nion-list {\n  padding-top: 0px !important;\n  padding-bottom: 0px !important;\n}\n\n.process-btn ion-button {\n  white-space: normal;\n  font-size: 12px;\n  --padding-start: 0;\n  --padding-end: 0;\n  height: 50px;\n}\n\n.vegiteble-box ion-button img {\n  border-radius: 50%;\n  width: 40px;\n  height: 40px;\n  -o-object-fit: cover;\n     object-fit: cover;\n}\n\n.vegiteble-box ion-button ion-label {\n  margin-left: 10px;\n}\n\n.vegiteble-box ion-button {\n  height: 80px;\n  --background: #fff;\n  border: 2px solid #c1c1c1;\n  color: #000;\n  border-radius: 5px;\n}\n\n.active ion-button ion-icon {\n  background: var(--ion-color-primary);\n}\n\n.active ion-button {\n  border: 2px solid var(--ion-color-primary) !important;\n  color: var(--ion-color-primary) !important;\n}\n\n.vegiteble-box ion-icon {\n  background: #c1c1c1;\n  padding: 5px;\n  border-radius: 50%;\n  color: #fff;\n}\n\n.vegiteble-box-title {\n  text-align: center;\n}\n\n@-webkit-keyframes fadeIn {\n  0% {\n    opacity: 0;\n  }\n  100% {\n    opacity: 1;\n  }\n}\n\n@keyframes fadeIn {\n  0% {\n    opacity: 0;\n  }\n  100% {\n    opacity: 1;\n  }\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvY29tcG9uZW50cy9ib2R5LXNjYW4tc3RlcC10aHJlZS9ib2R5LXNjYW4tc3RlcC10aHJlZS5jb21wb25lbnQuc2NzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQTtFQUNJLGFBQUE7RUFDQSxtQkFBQTtFQUNBLFdBQUE7RUFDQSxrQkFBQTtBQUNKOztBQUVBO0VBQ0ksa0JBQUE7QUFDSjs7QUFFQTtFQUNJLG1CQUFBO0VBQ0Esa0JBQUE7QUFDSjs7QUFFQTtFQUNJLHFDQUFBO0VBQ0Esa0JBQUE7RUFDQSxvQkFBQTtBQUNKOztBQUVBO0VBQ0ksMkJBQUE7RUFDQSw4QkFBQTtBQUNKOztBQUVBO0VBQ0ksbUJBQUE7RUFDQSxlQUFBO0VBQ0Esa0JBQUE7RUFDQSxnQkFBQTtFQUNBLFlBQUE7QUFDSjs7QUFJUTtFQUNJLGtCQUFBO0VBQ0EsV0FBQTtFQUNBLFlBQUE7RUFDQSxvQkFBQTtLQUFBLGlCQUFBO0FBRFo7O0FBSVE7RUFDSSxpQkFBQTtBQUZaOztBQU1BO0VBQ0ksWUFBQTtFQUNBLGtCQUFBO0VBQ0EseUJBQUE7RUFDQSxXQUFBO0VBQ0Esa0JBQUE7QUFISjs7QUFXQTtFQUNJLG9DQUFBO0FBUko7O0FBV0E7RUFDSSxxREFBQTtFQUNBLDBDQUFBO0FBUko7O0FBYUE7RUFDSSxtQkFBQTtFQUNBLFlBQUE7RUFDQSxrQkFBQTtFQUNBLFdBQUE7QUFWSjs7QUFhQTtFQUNJLGtCQUFBO0FBVko7O0FBY0k7RUFDSTtJQUFJLFVBQUE7RUFWVjtFQVdNO0lBQU0sVUFBQTtFQVJaO0FBQ0Y7O0FBS0k7RUFDSTtJQUFJLFVBQUE7RUFWVjtFQVdNO0lBQU0sVUFBQTtFQVJaO0FBQ0YiLCJmaWxlIjoic3JjL2FwcC9jb21wb25lbnRzL2JvZHktc2Nhbi1zdGVwLXRocmVlL2JvZHktc2Nhbi1zdGVwLXRocmVlLmNvbXBvbmVudC5zY3NzIiwic291cmNlc0NvbnRlbnQiOlsiLm15LWNoZWNrYm94IHtcbiAgICBkaXNwbGF5OiBmbGV4O1xuICAgIGFsaWduLWl0ZW1zOiBjZW50ZXI7XG4gICAgZmxvYXQ6IGxlZnQ7XG4gICAgbWFyZ2luLXJpZ2h0OiAxNXB4O1xufVxuXG4ubXktY2hlY2tib3ggaW9uLWxhYmVsIHtcbiAgICBwYWRkaW5nLXJpZ2h0OiA1cHg7XG59XG5cbmlvbi1pdGVtIGlvbi1sYWJlbCB7XG4gICAgd2hpdGUtc3BhY2U6IG5vcm1hbDtcbiAgICBwYWRkaW5nLWxlZnQ6IDEwcHg7XG59XG5cbmlvbi1pdGVtIHtcbiAgICAtLWlubmVyLWJvcmRlci13aWR0aDogMHB4IDBweCAwcHggMHB4O1xuICAgIC0tbWluLWhlaWdodDogMzVweDtcbiAgICAtLXBhZGRpbmctc3RhcnQ6IDBweDtcbn1cblxuaW9uLWxpc3Qge1xuICAgIHBhZGRpbmctdG9wOiAwcHggIWltcG9ydGFudDtcbiAgICBwYWRkaW5nLWJvdHRvbTogMHB4ICFpbXBvcnRhbnQ7XG59XG5cbi5wcm9jZXNzLWJ0biBpb24tYnV0dG9uIHtcbiAgICB3aGl0ZS1zcGFjZTogbm9ybWFsO1xuICAgIGZvbnQtc2l6ZTogMTJweDtcbiAgICAtLXBhZGRpbmctc3RhcnQ6IDA7XG4gICAgLS1wYWRkaW5nLWVuZDogMDtcbiAgICBoZWlnaHQ6IDUwcHg7XG59XG5cbi52ZWdpdGVibGUtYm94IHtcbiAgICBpb24tYnV0dG9uIHtcbiAgICAgICAgaW1nIHtcbiAgICAgICAgICAgIGJvcmRlci1yYWRpdXM6IDUwJTtcbiAgICAgICAgICAgIHdpZHRoOiA0MHB4O1xuICAgICAgICAgICAgaGVpZ2h0OiA0MHB4O1xuICAgICAgICAgICAgb2JqZWN0LWZpdDogY292ZXI7XG4gICAgICAgIH1cblxuICAgICAgICBpb24tbGFiZWwge1xuICAgICAgICAgICAgbWFyZ2luLWxlZnQ6IDEwcHg7XG4gICAgICAgIH1cbiAgICB9XG59XG4udmVnaXRlYmxlLWJveCBpb24tYnV0dG9uIHtcbiAgICBoZWlnaHQ6IDgwcHg7XG4gICAgLS1iYWNrZ3JvdW5kOiAjZmZmO1xuICAgIGJvcmRlcjogMnB4IHNvbGlkICNjMWMxYzE7XG4gICAgY29sb3I6ICMwMDA7XG4gICAgYm9yZGVyLXJhZGl1czogNXB4O1xufVxuXG4vLyAudmVnaXRlYmxlLWJveCBpb24tYnV0dG9uIHtcbi8vICAgICBib3JkZXI6IDJweCBzb2xpZCAjZjU3NzI4O1xuLy8gICAgIGNvbG9yOiAjZjU3NzI4O1xuLy8gfVxuXG4uYWN0aXZlIGlvbi1idXR0b24gaW9uLWljb24ge1xuICAgIGJhY2tncm91bmQ6IHZhcigtLWlvbi1jb2xvci1wcmltYXJ5KTtcbn1cblxuLmFjdGl2ZSBpb24tYnV0dG9uIHtcbiAgICBib3JkZXI6IDJweCBzb2xpZCB2YXIoLS1pb24tY29sb3ItcHJpbWFyeSkgIWltcG9ydGFudDtcbiAgICBjb2xvcjogdmFyKC0taW9uLWNvbG9yLXByaW1hcnkpICFpbXBvcnRhbnQ7XG5cblxufVxuXG4udmVnaXRlYmxlLWJveCBpb24taWNvbiB7XG4gICAgYmFja2dyb3VuZDogI2MxYzFjMTtcbiAgICBwYWRkaW5nOiA1cHg7XG4gICAgYm9yZGVyLXJhZGl1czogNTAlO1xuICAgIGNvbG9yOiAjZmZmO1xufVxuXG4udmVnaXRlYmxlLWJveC10aXRsZSB7XG4gICAgdGV4dC1hbGlnbjogY2VudGVyO1xufVxuXG4udHJhbiB7XG4gICAgQGtleWZyYW1lcyBmYWRlSW4ge1xuICAgICAgICAwJSB7b3BhY2l0eTogMDt9XG4gICAgICAgIDEwMCUge29wYWNpdHk6IDE7fVxuICAgICB9IFxufSJdfQ== */";
      /***/
    },

    /***/
    "./src/app/components/body-scan-step-three/body-scan-step-three.component.ts":
    /*!***********************************************************************************!*\
      !*** ./src/app/components/body-scan-step-three/body-scan-step-three.component.ts ***!
      \***********************************************************************************/

    /*! exports provided: BodyScanStepThreeComponent */

    /***/
    function srcAppComponentsBodyScanStepThreeBodyScanStepThreeComponentTs(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "BodyScanStepThreeComponent", function () {
        return BodyScanStepThreeComponent;
      });
      /* harmony import */


      var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! tslib */
      "./node_modules/tslib/tslib.es6.js");
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! @angular/core */
      "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
      /* harmony import */


      var _angular_forms__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! @angular/forms */
      "./node_modules/@angular/forms/__ivy_ngcc__/fesm2015/forms.js");

      var BodyScanStepThreeComponent = /*#__PURE__*/function () {
        function BodyScanStepThreeComponent() {
          _classCallCheck(this, BodyScanStepThreeComponent);

          this.showNutrition = false;
          this.stepThreeSelected = new _angular_core__WEBPACK_IMPORTED_MODULE_1__["EventEmitter"]();
          this.nutritionArr = [];
          this.calorieCycling = "Automatic management";
          this.vagetables = [{
            id: 1,
            text: 'Aubergine',
            icon: 'assets/icon/legumes/Aubergine.jpg',
            active: false
          }, {
            id: 2,
            text: 'Brocoli',
            icon: 'assets/icon/legumes/Brocoli.jpg',
            active: false
          }, {
            id: 3,
            text: 'Carotte',
            icon: 'assets/icon/legumes/Carotte.jpg',
            active: false
          }, {
            id: 4,
            text: 'Chou',
            icon: 'assets/icon/legumes/Chou.jpg',
            active: false
          }, {
            id: 5,
            text: 'Courgette',
            icon: 'assets/icon/legumes/Courgette.jpg',
            active: false
          }, {
            id: 6,
            text: 'Epinard',
            icon: 'assets/icon/legumes/Epinard.jpg',
            active: false
          }, {
            id: 7,
            text: 'Haricot',
            icon: 'assets/icon/legumes/Haricot.jpg',
            active: false
          }, {
            id: 8,
            text: 'Laitue',
            icon: 'assets/icon/legumes/Laitue.jpg',
            active: false
          }, {
            id: 9,
            text: 'Oignon',
            icon: 'assets/icon/legumes/Oignon.jpg',
            active: false
          }, {
            id: 10,
            text: 'Poireau',
            icon: 'assets/icon/legumes/Poireau.jpg',
            active: false
          }, {
            id: 11,
            text: 'Pomme de terre',
            icon: 'assets/icon/legumes/Pomme de terre.jpg',
            active: false
          }, {
            id: 12,
            text: 'Radis',
            icon: 'assets/icon/legumes/Radis.jpg',
            active: false
          }];
          this.fruits = [{
            id: 1,
            text: 'Abricot',
            icon: 'assets/icon/fruits/Abricot.jpg',
            active: false
          }, {
            id: 2,
            text: 'Ananas',
            icon: 'assets/icon/fruits/Ananas.jpg',
            active: false
          }, {
            id: 2,
            text: 'Banane',
            icon: 'assets/icon/fruits/Banane.jpg',
            active: false
          }, {
            id: 2,
            text: 'Fraise',
            icon: 'assets/icon/fruits/Fraise.jpg',
            active: false
          }, {
            id: 2,
            text: 'Framboise',
            icon: 'assets/icon/fruits/Framboise.jpg',
            active: false
          }, {
            id: 2,
            text: 'Pastèque',
            icon: 'assets/icon/fruits/Pastèque.jpg',
            active: false
          }, {
            id: 2,
            text: 'Poire',
            icon: 'assets/icon/fruits/Poire.jpg',
            active: false
          }, {
            id: 2,
            text: 'Pomme',
            icon: 'assets/icon/fruits/Pomme.jpg',
            active: false
          }];
          this.starchy = [{
            id: 1,
            text: 'Haricots verts',
            icon: 'assets/icon/feculents/Haricots verts.jpg',
            active: false
          }, {
            id: 2,
            text: 'Lentilles',
            icon: 'assets/icon/feculents/Lentilles.jpg',
            active: false
          }, {
            id: 3,
            text: 'Patate douce',
            icon: 'assets/icon/feculents/Patate douce.jpg',
            active: false
          }, {
            id: 4,
            text: 'Pâtes',
            icon: 'assets/icon/feculents/Pâtes.jpg',
            active: false
          }, {
            id: 5,
            text: 'Pomme de terre',
            icon: 'assets/icon/feculents/Pomme de terre.jpg',
            active: false
          }, {
            id: 6,
            text: 'Quinoa',
            icon: 'assets/icon/feculents/Quinoa.jpg',
            active: false
          }, {
            id: 7,
            text: 'Riz',
            icon: 'assets/icon/feculents/Riz.jpg',
            active: false
          }, {
            id: 8,
            text: 'Semoule',
            icon: 'assets/icon/feculents/Semoule.jpg',
            active: false
          }];
        }

        _createClass(BodyScanStepThreeComponent, [{
          key: "ngOnInit",
          value: function ngOnInit() {
            this.initializeForm();
          }
        }, {
          key: "initializeForm",
          value: function initializeForm() {
            this.stepThreeForm = new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormGroup"]({
              field1: new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormControl"](),
              field2: new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormControl"](),
              field3: new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormControl"](this.calorieCycling),
              field4: new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormControl"](),
              field5: new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormControl"](),
              field6: new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormControl"]()
            });
            this.onChanges();
          }
        }, {
          key: "selectStarchy",
          value: function selectStarchy(starchy) {
            starchy.active = !starchy.active;
            this.stepThreeForm.get('field6').setValue(JSON.stringify(this.starchy.filter(function (item) {
              return item.active;
            })));
            this.emitChanges();
          }
        }, {
          key: "selectFruits",
          value: function selectFruits(fruit) {
            fruit.active = !fruit.active;
            this.stepThreeForm.get('field5').setValue(JSON.stringify(this.fruits.filter(function (item) {
              return item.active;
            })));
            this.emitChanges();
          }
        }, {
          key: "selectVeg",
          value: function selectVeg(veg) {
            veg.active = !veg.active;
            this.stepThreeForm.get('field4').setValue(JSON.stringify(this.vagetables.filter(function (item) {
              return item.active;
            })));
            this.emitChanges();
          }
        }, {
          key: "checkValue1",
          value: function checkValue1(event) {
            console.log(event);

            if (event.detail.value == 'yes' && event.detail.checked == true) {
              this.showNutrition = true;
            } else {
              this.showNutrition = false;
            }

            this.stepThreeForm.get('field1').setValue(this.showNutrition);
          }
        }, {
          key: "checkValue2",
          value: function checkValue2(event) {
            console.log(event);

            if (event.detail.value == 'no' && event.detail.checked == true) {
              this.showNutrition = false;
            }

            this.stepThreeForm.get('field1').setValue(this.showNutrition);
          }
        }, {
          key: "onChanges",
          value: function onChanges() {
            var _this10 = this;

            this.stepThreeForm.valueChanges.subscribe(function (val) {
              _this10.emitChanges();
            });
          }
        }, {
          key: "emitChanges",
          value: function emitChanges() {
            this.stepThreeSelected.emit(this.stepThreeForm.value);
          }
        }, {
          key: "addRemoveNutrition",
          value: function addRemoveNutrition(event) {
            if (event.target.checked) {
              this.nutritionArr.push(event.target.value);
            } else {
              var index = this.nutritionArr.indexOf(event.target.value);
              this.nutritionArr.splice(index, 1);
            }

            if (this.nutritionArr.length > 0) {
              this.stepThreeForm.get('field2').setValue(JSON.stringify(this.nutritionArr));
            } else {
              this.stepThreeForm.get('field2').setValue('');
            }

            this.onChanges();
          }
        }, {
          key: "calorieCyclingSelection",
          value: function calorieCyclingSelection(value) {
            this.calorieCycling = value;
            this.stepThreeForm.get('field3').setValue(value);
            this.onChanges();
          }
        }]);

        return BodyScanStepThreeComponent;
      }();

      BodyScanStepThreeComponent.ctorParameters = function () {
        return [];
      };

      BodyScanStepThreeComponent.propDecorators = {
        stepThreeSelected: [{
          type: _angular_core__WEBPACK_IMPORTED_MODULE_1__["Output"]
        }],
        stepThreeForm: [{
          type: _angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"]
        }]
      };
      BodyScanStepThreeComponent = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-body-scan-step-three',
        template: Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(
        /*! raw-loader!./body-scan-step-three.component.html */
        "./node_modules/raw-loader/dist/cjs.js!./src/app/components/body-scan-step-three/body-scan-step-three.component.html"))["default"],
        styles: [Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(
        /*! ./body-scan-step-three.component.scss */
        "./src/app/components/body-scan-step-three/body-scan-step-three.component.scss"))["default"]]
      })], BodyScanStepThreeComponent);
      /***/
    },

    /***/
    "./src/app/components/body-scan-step-two/body-scan-step-two.component.scss":
    /*!*********************************************************************************!*\
      !*** ./src/app/components/body-scan-step-two/body-scan-step-two.component.scss ***!
      \*********************************************************************************/

    /*! exports provided: default */

    /***/
    function srcAppComponentsBodyScanStepTwoBodyScanStepTwoComponentScss(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony default export */


      __webpack_exports__["default"] = ".steptwofirstbtn ion-button {\n  white-space: normal;\n  font-size: 12px;\n  --padding-start: 0;\n  --padding-end: 0;\n  height: 50px;\n}\n\nion-item ion-label {\n  white-space: normal;\n}\n\nion-item {\n  --inner-border-width: 0px 0px 0px 0px;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvY29tcG9uZW50cy9ib2R5LXNjYW4tc3RlcC10d28vYm9keS1zY2FuLXN0ZXAtdHdvLmNvbXBvbmVudC5zY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBO0VBQ0ksbUJBQUE7RUFDQSxlQUFBO0VBQ0Esa0JBQUE7RUFDQSxnQkFBQTtFQUNBLFlBQUE7QUFDSjs7QUFFQTtFQUNJLG1CQUFBO0FBQ0o7O0FBRUE7RUFDSSxxQ0FBQTtBQUNKIiwiZmlsZSI6InNyYy9hcHAvY29tcG9uZW50cy9ib2R5LXNjYW4tc3RlcC10d28vYm9keS1zY2FuLXN0ZXAtdHdvLmNvbXBvbmVudC5zY3NzIiwic291cmNlc0NvbnRlbnQiOlsiLnN0ZXB0d29maXJzdGJ0biBpb24tYnV0dG9uIHtcbiAgICB3aGl0ZS1zcGFjZTogbm9ybWFsO1xuICAgIGZvbnQtc2l6ZTogMTJweDtcbiAgICAtLXBhZGRpbmctc3RhcnQ6IDA7XG4gICAgLS1wYWRkaW5nLWVuZDogMDtcbiAgICBoZWlnaHQ6IDUwcHg7XG59XG5cbmlvbi1pdGVtIGlvbi1sYWJlbCB7XG4gICAgd2hpdGUtc3BhY2U6IG5vcm1hbDtcbn1cblxuaW9uLWl0ZW0ge1xuICAgIC0taW5uZXItYm9yZGVyLXdpZHRoOiAwcHggMHB4IDBweCAwcHg7XG59Il19 */";
      /***/
    },

    /***/
    "./src/app/components/body-scan-step-two/body-scan-step-two.component.ts":
    /*!*******************************************************************************!*\
      !*** ./src/app/components/body-scan-step-two/body-scan-step-two.component.ts ***!
      \*******************************************************************************/

    /*! exports provided: BodyScanStepTwoComponent */

    /***/
    function srcAppComponentsBodyScanStepTwoBodyScanStepTwoComponentTs(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "BodyScanStepTwoComponent", function () {
        return BodyScanStepTwoComponent;
      });
      /* harmony import */


      var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! tslib */
      "./node_modules/tslib/tslib.es6.js");
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! @angular/core */
      "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
      /* harmony import */


      var _angular_forms__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! @angular/forms */
      "./node_modules/@angular/forms/__ivy_ngcc__/fesm2015/forms.js");

      var BodyScanStepTwoComponent = /*#__PURE__*/function () {
        function BodyScanStepTwoComponent() {
          _classCallCheck(this, BodyScanStepTwoComponent);

          this.stepTwoSelected = new _angular_core__WEBPACK_IMPORTED_MODULE_1__["EventEmitter"]();
          this.foodGoalsSelection = "Gain muscle";
          this.paceOfMuscleGain = "Soft";
        }

        _createClass(BodyScanStepTwoComponent, [{
          key: "ngOnInit",
          value: function ngOnInit() {
            this.initializeForm();
          }
        }, {
          key: "initializeForm",
          value: function initializeForm() {
            this.stepTwoForm = new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormGroup"]({
              food_pref: new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormControl"](this.foodGoalsSelection),
              food_intolerance: new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormControl"](this.paceOfMuscleGain)
            });
            this.onChanges();
          }
        }, {
          key: "onChanges",
          value: function onChanges() {
            var _this11 = this;

            this.stepTwoForm.valueChanges.subscribe(function (val) {
              _this11.emitChanges();
            });
          }
        }, {
          key: "emitChanges",
          value: function emitChanges() {
            this.stepTwoSelected.emit(this.stepTwoForm.value);
          }
        }, {
          key: "changeFoodGoalsSelection",
          value: function changeFoodGoalsSelection(value) {
            this.foodGoalsSelection = value;
            this.stepTwoForm.get('food_pref').setValue(value);
          }
        }, {
          key: "changePaceOfMuscleGain",
          value: function changePaceOfMuscleGain(value) {
            this.paceOfMuscleGain = value;
            this.stepTwoForm.get('food_intolerance').setValue(value);
          }
        }]);

        return BodyScanStepTwoComponent;
      }();

      BodyScanStepTwoComponent.ctorParameters = function () {
        return [];
      };

      BodyScanStepTwoComponent.propDecorators = {
        stepTwoSelected: [{
          type: _angular_core__WEBPACK_IMPORTED_MODULE_1__["Output"]
        }],
        stepTwoForm: [{
          type: _angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"]
        }]
      };
      BodyScanStepTwoComponent = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-body-scan-step-two',
        template: Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(
        /*! raw-loader!./body-scan-step-two.component.html */
        "./node_modules/raw-loader/dist/cjs.js!./src/app/components/body-scan-step-two/body-scan-step-two.component.html"))["default"],
        styles: [Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(
        /*! ./body-scan-step-two.component.scss */
        "./src/app/components/body-scan-step-two/body-scan-step-two.component.scss"))["default"]]
      })], BodyScanStepTwoComponent);
      /***/
    },

    /***/
    "./src/app/components/bodysvgback/bodysvgback.component.scss":
    /*!*******************************************************************!*\
      !*** ./src/app/components/bodysvgback/bodysvgback.component.scss ***!
      \*******************************************************************/

    /*! exports provided: default */

    /***/
    function srcAppComponentsBodysvgbackBodysvgbackComponentScss(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony default export */


      __webpack_exports__["default"] = "svg {\n  width: 135%;\n  position: absolute;\n  right: -17%;\n  top: -17%;\n  height: 130%;\n}\n\n.Pathselected {\n  fill: red !important;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvY29tcG9uZW50cy9ib2R5c3ZnYmFjay9ib2R5c3ZnYmFjay5jb21wb25lbnQuc2NzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQTtFQUNJLFdBQUE7RUFDQSxrQkFBQTtFQUNBLFdBQUE7RUFDQSxTQUFBO0VBQ0EsWUFBQTtBQUNKOztBQUVBO0VBQ0ksb0JBQUE7QUFDSiIsImZpbGUiOiJzcmMvYXBwL2NvbXBvbmVudHMvYm9keXN2Z2JhY2svYm9keXN2Z2JhY2suY29tcG9uZW50LnNjc3MiLCJzb3VyY2VzQ29udGVudCI6WyJzdmcge1xuICAgIHdpZHRoOiAxMzUlO1xuICAgIHBvc2l0aW9uOiBhYnNvbHV0ZTtcbiAgICByaWdodDogLTE3JTtcbiAgICB0b3A6IC0xNyU7XG4gICAgaGVpZ2h0OiAxMzAlO1xufVxuXG4uUGF0aHNlbGVjdGVkIHtcbiAgICBmaWxsOiByZWQgIWltcG9ydGFudDtcbn0iXX0= */";
      /***/
    },

    /***/
    "./src/app/components/bodysvgback/bodysvgback.component.ts":
    /*!*****************************************************************!*\
      !*** ./src/app/components/bodysvgback/bodysvgback.component.ts ***!
      \*****************************************************************/

    /*! exports provided: BodysvgbackComponent */

    /***/
    function srcAppComponentsBodysvgbackBodysvgbackComponentTs(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "BodysvgbackComponent", function () {
        return BodysvgbackComponent;
      });
      /* harmony import */


      var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! tslib */
      "./node_modules/tslib/tslib.es6.js");
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! @angular/core */
      "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");

      var BodysvgbackComponent = /*#__PURE__*/function () {
        function BodysvgbackComponent() {
          _classCallCheck(this, BodysvgbackComponent);
        }

        _createClass(BodysvgbackComponent, [{
          key: "ngOnInit",
          value: function ngOnInit() {}
        }, {
          key: "onClickPathBack",
          value: function onClickPathBack(event) {
            if (event) {
              if (event.target.classList && event.target.classList) {
                var cls = event.target.classList;

                if (cls && cls.length > 0) {
                  var className = "Pathselected";
                  var res = cls.value.includes(className);
                  console.log("res===", res);

                  if (!res) {
                    cls.add(className);
                  } else {
                    cls.remove(className);
                  }

                  console.log("cls", cls);
                }
              }
            }
          }
        }]);

        return BodysvgbackComponent;
      }();

      BodysvgbackComponent.ctorParameters = function () {
        return [];
      };

      BodysvgbackComponent = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-bodysvgback',
        template: Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(
        /*! raw-loader!./bodysvgback.component.html */
        "./node_modules/raw-loader/dist/cjs.js!./src/app/components/bodysvgback/bodysvgback.component.html"))["default"],
        styles: [Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(
        /*! ./bodysvgback.component.scss */
        "./src/app/components/bodysvgback/bodysvgback.component.scss"))["default"]]
      })], BodysvgbackComponent);
      /***/
    },

    /***/
    "./src/app/components/bodysvgfront/bodysvg.component.scss":
    /*!****************************************************************!*\
      !*** ./src/app/components/bodysvgfront/bodysvg.component.scss ***!
      \****************************************************************/

    /*! exports provided: default */

    /***/
    function srcAppComponentsBodysvgfrontBodysvgComponentScss(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony default export */


      __webpack_exports__["default"] = "svg {\n  width: 135%;\n  position: absolute;\n  right: -17%;\n  top: -17%;\n  height: 130%;\n}\n\n.Pathselected {\n  fill: red !important;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvY29tcG9uZW50cy9ib2R5c3ZnZnJvbnQvYm9keXN2Zy5jb21wb25lbnQuc2NzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQTtFQUNJLFdBQUE7RUFDQSxrQkFBQTtFQUNBLFdBQUE7RUFDQSxTQUFBO0VBQ0EsWUFBQTtBQUNKOztBQUVBO0VBQ0ksb0JBQUE7QUFDSiIsImZpbGUiOiJzcmMvYXBwL2NvbXBvbmVudHMvYm9keXN2Z2Zyb250L2JvZHlzdmcuY29tcG9uZW50LnNjc3MiLCJzb3VyY2VzQ29udGVudCI6WyJzdmcge1xuICAgIHdpZHRoOiAxMzUlO1xuICAgIHBvc2l0aW9uOiBhYnNvbHV0ZTtcbiAgICByaWdodDogLTE3JTtcbiAgICB0b3A6IC0xNyU7XG4gICAgaGVpZ2h0OiAxMzAlO1xufVxuXG4uUGF0aHNlbGVjdGVkIHtcbiAgICBmaWxsOiByZWQgIWltcG9ydGFudDtcbn0iXX0= */";
      /***/
    },

    /***/
    "./src/app/components/bodysvgfront/bodysvg.component.ts":
    /*!**************************************************************!*\
      !*** ./src/app/components/bodysvgfront/bodysvg.component.ts ***!
      \**************************************************************/

    /*! exports provided: BodysvgComponent */

    /***/
    function srcAppComponentsBodysvgfrontBodysvgComponentTs(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "BodysvgComponent", function () {
        return BodysvgComponent;
      });
      /* harmony import */


      var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! tslib */
      "./node_modules/tslib/tslib.es6.js");
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! @angular/core */
      "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");

      var BodysvgComponent = /*#__PURE__*/function () {
        function BodysvgComponent() {
          _classCallCheck(this, BodysvgComponent);
        }

        _createClass(BodysvgComponent, [{
          key: "ngOnInit",
          value: function ngOnInit() {}
        }, {
          key: "onClickPath",
          value: function onClickPath(event) {
            console.log("click", event);

            if (event) {
              if (event.target.classList && event.target.classList) {
                var cls = event.target.classList;

                if (cls && cls.length > 0) {
                  var className = "Pathselected";
                  var res = cls.value.includes(className);
                  console.log("res===", res);

                  if (!res) {
                    cls.add(className);
                  } else {
                    cls.remove(className);
                  }

                  console.log("cls", cls);
                }
              }
            }
          }
        }]);

        return BodysvgComponent;
      }();

      BodysvgComponent.ctorParameters = function () {
        return [];
      };

      BodysvgComponent = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-bodysvg',
        template: Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(
        /*! raw-loader!./bodysvg.component.html */
        "./node_modules/raw-loader/dist/cjs.js!./src/app/components/bodysvgfront/bodysvg.component.html"))["default"],
        styles: [Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(
        /*! ./bodysvg.component.scss */
        "./src/app/components/bodysvgfront/bodysvg.component.scss"))["default"]]
      })], BodysvgComponent);
      /***/
    },

    /***/
    "./src/app/components/book/book.component.scss":
    /*!*****************************************************!*\
      !*** ./src/app/components/book/book.component.scss ***!
      \*****************************************************/

    /*! exports provided: default */

    /***/
    function srcAppComponentsBookBookComponentScss(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony default export */


      __webpack_exports__["default"] = ".product-bg {\n  background: #fff;\n  border-radius: 5px;\n  padding: 15px;\n}\n\n.product-descrption {\n  text-align: center;\n}\n\n.product-descrption p {\n  margin: 0;\n}\n\n.product-descrption h4 {\n  margin: 0;\n  font-size: 14px;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvY29tcG9uZW50cy9ib29rL2Jvb2suY29tcG9uZW50LnNjc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUE7RUFDSSxnQkFBQTtFQUNBLGtCQUFBO0VBQ0EsYUFBQTtBQUNKOztBQUVBO0VBQ0ksa0JBQUE7QUFDSjs7QUFFQTtFQUNJLFNBQUE7QUFDSjs7QUFFQTtFQUNJLFNBQUE7RUFDQSxlQUFBO0FBQ0oiLCJmaWxlIjoic3JjL2FwcC9jb21wb25lbnRzL2Jvb2svYm9vay5jb21wb25lbnQuc2NzcyIsInNvdXJjZXNDb250ZW50IjpbIi5wcm9kdWN0LWJnIHtcbiAgICBiYWNrZ3JvdW5kOiAjZmZmO1xuICAgIGJvcmRlci1yYWRpdXM6IDVweDtcbiAgICBwYWRkaW5nOiAxNXB4O1xufVxuXG4ucHJvZHVjdC1kZXNjcnB0aW9uIHtcbiAgICB0ZXh0LWFsaWduOiBjZW50ZXI7XG59XG5cbi5wcm9kdWN0LWRlc2NycHRpb24gcCB7XG4gICAgbWFyZ2luOiAwO1xufVxuXG4ucHJvZHVjdC1kZXNjcnB0aW9uIGg0IHtcbiAgICBtYXJnaW46IDA7XG4gICAgZm9udC1zaXplOiAxNHB4O1xufSJdfQ== */";
      /***/
    },

    /***/
    "./src/app/components/book/book.component.ts":
    /*!***************************************************!*\
      !*** ./src/app/components/book/book.component.ts ***!
      \***************************************************/

    /*! exports provided: BookComponent */

    /***/
    function srcAppComponentsBookBookComponentTs(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "BookComponent", function () {
        return BookComponent;
      });
      /* harmony import */


      var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! tslib */
      "./node_modules/tslib/tslib.es6.js");
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! @angular/core */
      "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");

      var BookComponent = /*#__PURE__*/function () {
        function BookComponent() {
          _classCallCheck(this, BookComponent);
        }

        _createClass(BookComponent, [{
          key: "ngOnInit",
          value: function ngOnInit() {}
        }]);

        return BookComponent;
      }();

      BookComponent.ctorParameters = function () {
        return [];
      };

      BookComponent = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-book',
        template: Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(
        /*! raw-loader!./book.component.html */
        "./node_modules/raw-loader/dist/cjs.js!./src/app/components/book/book.component.html"))["default"],
        styles: [Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(
        /*! ./book.component.scss */
        "./src/app/components/book/book.component.scss"))["default"]]
      })], BookComponent);
      /***/
    },

    /***/
    "./src/app/components/chart-bar/chart-bar.component.scss":
    /*!***************************************************************!*\
      !*** ./src/app/components/chart-bar/chart-bar.component.scss ***!
      \***************************************************************/

    /*! exports provided: default */

    /***/
    function srcAppComponentsChartBarChartBarComponentScss(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony default export */


      __webpack_exports__["default"] = "#chart {\n  width: 100%;\n  height: 230px;\n  margin: 0px auto 0;\n  display: block;\n  overflow-x: scroll;\n  margin-top: 15px;\n}\n\n#chart #numbers li {\n  text-align: right;\n  padding-right: 1em;\n  list-style: none;\n  height: 29px;\n  border-bottom: 1px solid #444;\n  position: relative;\n  bottom: 30px;\n}\n\n#chart #bars {\n  display: flex;\n  background: rgba(0, 0, 0, 0.2);\n  width: 100%;\n  height: 230px;\n  max-width: 100%;\n  padding: 0;\n  margin: 0;\n  box-shadow: 0 0 0 1px #444;\n  overflow-x: scroll;\n}\n\n#chart #bars li {\n  display: inline-table;\n  width: 50px;\n  height: 130px;\n  margin: 15px 0px 0px 0px;\n  text-align: center;\n  position: relative;\n}\n\n#chart #bars li .bar {\n  display: block;\n  width: 40px;\n  margin-left: 15px;\n  background: #49E;\n  position: absolute;\n  bottom: 0;\n  border-radius: 10px 10px 0px 0px;\n}\n\n#chart #bars li span {\n  color: #fff;\n  width: 100%;\n  position: absolute;\n  bottom: -5em;\n  left: 21px;\n  text-align: center;\n  border-radius: 5px;\n  padding: 6px 0px;\n  width: 28px;\n  font-size: 12px;\n}\n\n#chart #bars li span.active {\n  color: #000;\n  width: 100%;\n  position: absolute;\n  bottom: -5em;\n  left: 21px;\n  text-align: center;\n  background: #FFF;\n  border-radius: 5px;\n  padding: 6px 0px;\n  width: 28px;\n  font-size: 12px;\n}\n\nspan.chartbar-velue {\n  position: relative !important;\n  left: 0px !important;\n  top: -25px;\n  font-size: 10px !important;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvY29tcG9uZW50cy9jaGFydC1iYXIvY2hhcnQtYmFyLmNvbXBvbmVudC5zY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBO0VBQ0ksV0FBQTtFQUNBLGFBQUE7RUFDQSxrQkFBQTtFQUNBLGNBQUE7RUFDQSxrQkFBQTtFQUNBLGdCQUFBO0FBQ0o7O0FBRUE7RUFDSSxpQkFBQTtFQUNBLGtCQUFBO0VBQ0EsZ0JBQUE7RUFDQSxZQUFBO0VBQ0EsNkJBQUE7RUFDQSxrQkFBQTtFQUNBLFlBQUE7QUFDSjs7QUFFQTtFQUNJLGFBQUE7RUFDQSw4QkFBQTtFQUNBLFdBQUE7RUFDQSxhQUFBO0VBQ0EsZUFBQTtFQUNBLFVBQUE7RUFDQSxTQUFBO0VBQ0EsMEJBQUE7RUFDQSxrQkFBQTtBQUNKOztBQUVBO0VBQ0kscUJBQUE7RUFDQSxXQUFBO0VBQ0EsYUFBQTtFQUNBLHdCQUFBO0VBQ0Esa0JBQUE7RUFDQSxrQkFBQTtBQUNKOztBQUVBO0VBQ0ksY0FBQTtFQUNBLFdBQUE7RUFDQSxpQkFBQTtFQUNBLGdCQUFBO0VBQ0Esa0JBQUE7RUFDQSxTQUFBO0VBQ0EsZ0NBQUE7QUFDSjs7QUFJQTtFQUNJLFdBQUE7RUFDQSxXQUFBO0VBQ0Esa0JBQUE7RUFDQSxZQUFBO0VBQ0EsVUFBQTtFQUNBLGtCQUFBO0VBQ0Esa0JBQUE7RUFDQSxnQkFBQTtFQUNBLFdBQUE7RUFDQSxlQUFBO0FBREo7O0FBSUE7RUFDSSxXQUFBO0VBQ0EsV0FBQTtFQUNBLGtCQUFBO0VBQ0EsWUFBQTtFQUNBLFVBQUE7RUFDQSxrQkFBQTtFQUNBLGdCQUFBO0VBQ0Esa0JBQUE7RUFDQSxnQkFBQTtFQUNBLFdBQUE7RUFDQSxlQUFBO0FBREo7O0FBSUE7RUFDSSw2QkFBQTtFQUNBLG9CQUFBO0VBQ0EsVUFBQTtFQUNBLDBCQUFBO0FBREoiLCJmaWxlIjoic3JjL2FwcC9jb21wb25lbnRzL2NoYXJ0LWJhci9jaGFydC1iYXIuY29tcG9uZW50LnNjc3MiLCJzb3VyY2VzQ29udGVudCI6WyIjY2hhcnQge1xuICAgIHdpZHRoOiAxMDAlO1xuICAgIGhlaWdodDogMjMwcHg7XG4gICAgbWFyZ2luOiAwcHggYXV0byAwO1xuICAgIGRpc3BsYXk6IGJsb2NrO1xuICAgIG92ZXJmbG93LXg6IHNjcm9sbDtcbiAgICBtYXJnaW4tdG9wOiAxNXB4O1xufVxuXG4jY2hhcnQgI251bWJlcnMgbGkge1xuICAgIHRleHQtYWxpZ246IHJpZ2h0O1xuICAgIHBhZGRpbmctcmlnaHQ6IDFlbTtcbiAgICBsaXN0LXN0eWxlOiBub25lO1xuICAgIGhlaWdodDogMjlweDtcbiAgICBib3JkZXItYm90dG9tOiAxcHggc29saWQgIzQ0NDtcbiAgICBwb3NpdGlvbjogcmVsYXRpdmU7XG4gICAgYm90dG9tOiAzMHB4O1xufVxuXG4jY2hhcnQgI2JhcnMge1xuICAgIGRpc3BsYXk6IGZsZXg7XG4gICAgYmFja2dyb3VuZDogcmdiYSgwLCAwLCAwLCAwLjIpO1xuICAgIHdpZHRoOiAxMDAlO1xuICAgIGhlaWdodDogMjMwcHg7XG4gICAgbWF4LXdpZHRoOiAxMDAlO1xuICAgIHBhZGRpbmc6IDA7XG4gICAgbWFyZ2luOiAwO1xuICAgIGJveC1zaGFkb3c6IDAgMCAwIDFweCAjNDQ0O1xuICAgIG92ZXJmbG93LXg6IHNjcm9sbFxufVxuXG4jY2hhcnQgI2JhcnMgbGkge1xuICAgIGRpc3BsYXk6IGlubGluZS10YWJsZTtcbiAgICB3aWR0aDogNTBweDtcbiAgICBoZWlnaHQ6IDEzMHB4O1xuICAgIG1hcmdpbjogMTVweCAwcHggMHB4IDBweDtcbiAgICB0ZXh0LWFsaWduOiBjZW50ZXI7XG4gICAgcG9zaXRpb246IHJlbGF0aXZlO1xufVxuXG4jY2hhcnQgI2JhcnMgbGkgLmJhciB7XG4gICAgZGlzcGxheTogYmxvY2s7XG4gICAgd2lkdGg6IDQwcHg7XG4gICAgbWFyZ2luLWxlZnQ6IDE1cHg7XG4gICAgYmFja2dyb3VuZDogIzQ5RTtcbiAgICBwb3NpdGlvbjogYWJzb2x1dGU7XG4gICAgYm90dG9tOiAwO1xuICAgIGJvcmRlci1yYWRpdXM6IDEwcHggMTBweCAwcHggMHB4O1xuICAgIC8vIGJhY2tncm91bmQtY29sb3I6ICNmZjk4MDA7XG4gICAgLy8gYmFja2dyb3VuZC1pbWFnZTogbGluZWFyLWdyYWRpZW50KCB0byB0b3AsICNmZmMxMDcsICNlNjhjMDkpO1xufVxuXG4jY2hhcnQgI2JhcnMgbGkgc3BhbiB7XG4gICAgY29sb3I6ICNmZmY7XG4gICAgd2lkdGg6IDEwMCU7XG4gICAgcG9zaXRpb246IGFic29sdXRlO1xuICAgIGJvdHRvbTogLTVlbTtcbiAgICBsZWZ0OiAyMXB4O1xuICAgIHRleHQtYWxpZ246IGNlbnRlcjtcbiAgICBib3JkZXItcmFkaXVzOiA1cHg7XG4gICAgcGFkZGluZzogNnB4IDBweDtcbiAgICB3aWR0aDogMjhweDtcbiAgICBmb250LXNpemU6IDEycHg7XG59XG5cbiNjaGFydCAjYmFycyBsaSBzcGFuLmFjdGl2ZSB7XG4gICAgY29sb3I6ICMwMDA7XG4gICAgd2lkdGg6IDEwMCU7XG4gICAgcG9zaXRpb246IGFic29sdXRlO1xuICAgIGJvdHRvbTogLTVlbTtcbiAgICBsZWZ0OiAyMXB4O1xuICAgIHRleHQtYWxpZ246IGNlbnRlcjtcbiAgICBiYWNrZ3JvdW5kOiAjRkZGO1xuICAgIGJvcmRlci1yYWRpdXM6IDVweDtcbiAgICBwYWRkaW5nOiA2cHggMHB4O1xuICAgIHdpZHRoOiAyOHB4O1xuICAgIGZvbnQtc2l6ZTogMTJweDtcbn1cblxuc3Bhbi5jaGFydGJhci12ZWx1ZSB7XG4gICAgcG9zaXRpb246IHJlbGF0aXZlICFpbXBvcnRhbnQ7XG4gICAgbGVmdDogMHB4ICFpbXBvcnRhbnQ7XG4gICAgdG9wOiAtMjVweDtcbiAgICBmb250LXNpemU6IDEwcHggIWltcG9ydGFudDtcbn0iXX0= */";
      /***/
    },

    /***/
    "./src/app/components/chart-bar/chart-bar.component.ts":
    /*!*************************************************************!*\
      !*** ./src/app/components/chart-bar/chart-bar.component.ts ***!
      \*************************************************************/

    /*! exports provided: ChartBarComponent */

    /***/
    function srcAppComponentsChartBarChartBarComponentTs(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "ChartBarComponent", function () {
        return ChartBarComponent;
      });
      /* harmony import */


      var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! tslib */
      "./node_modules/tslib/tslib.es6.js");
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! @angular/core */
      "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");

      var ChartBarComponent = /*#__PURE__*/function () {
        function ChartBarComponent() {
          _classCallCheck(this, ChartBarComponent);

          this.Charts = [];
          this.slideOpts = {
            initialSlide: 0,
            speed: 500,
            slidesPerView: 8,
            spaceBetween: 5
          };
        }

        _createClass(ChartBarComponent, [{
          key: "ngOnInit",
          value: function ngOnInit() {
            console.log(this.label);
            var date = new Date();
            this.Charts = this.getDaysInMonth(date.getMonth(), date.getFullYear());
          }
        }, {
          key: "ngAfterViewInit",
          value: function ngAfterViewInit() {
            this.goToSlide();
          }
          /**
           * @param {number} The month number, 0 based
           * @param {number} The year, not zero based, required to account for leap years
           * @return {Date[]} List with date objects for each day of the month
           */

        }, {
          key: "getDaysInMonth",
          value: function getDaysInMonth(month, year) {
            var _this12 = this;

            var date = new Date(year, month, 1);
            var days = [];
            var today = new Date();

            while (date.getMonth() === month) {
              days.push({
                date: new Date(date)
              });
              date.setDate(date.getDate() + 1);
            }

            days.forEach(function (day, index) {
              var date = new Date(day.date);
              days[index]["monthName"] = date.toLocaleString("default", {
                month: "short"
              });
              days[index]["dayName"] = date.toLocaleString("default", {
                weekday: "short"
              });
              days[index]["day"] = date.toLocaleString("default", {
                day: "2-digit"
              });
              days[index]["data"] = _this12.getRandomInt();
              days[index]["color"] = "#ff9900";

              if (date.getDate() === today.getDate()) {
                days[index]["isToday"] = true;
              } else {
                days[index]["isToday"] = false;
              }
            });
            console.log("day", days);
            return days;
          }
        }, {
          key: "getRandomInt",
          value: function getRandomInt() {
            var min = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : 0;
            var max = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : 100;
            min = Math.ceil(min);
            max = Math.floor(max);
            return Math.floor(Math.random() * (max - min + 1)) + min;
          }
        }, {
          key: "goToSlide",
          value: function goToSlide() {
            return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, /*#__PURE__*/regeneratorRuntime.mark(function _callee3() {
              var currentIndex, index;
              return regeneratorRuntime.wrap(function _callee3$(_context3) {
                while (1) {
                  switch (_context3.prev = _context3.next) {
                    case 0:
                      _context3.next = 2;
                      return this.slider.nativeElement.getActiveIndex();

                    case 2:
                      currentIndex = _context3.sent;
                      index = this.Charts.findIndex(function (x) {
                        return x.isToday === true;
                      });
                      this.slider.nativeElement.slideTo(index - 2, 1000);

                    case 5:
                    case "end":
                      return _context3.stop();
                  }
                }
              }, _callee3, this);
            }));
          }
        }]);

        return ChartBarComponent;
      }();

      ChartBarComponent.ctorParameters = function () {
        return [];
      };

      ChartBarComponent.propDecorators = {
        slider: [{
          type: _angular_core__WEBPACK_IMPORTED_MODULE_1__["ViewChild"],
          args: ['mySlider']
        }],
        label: [{
          type: _angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"],
          args: ['label']
        }]
      };
      ChartBarComponent = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: "app-chart-bar",
        template: Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(
        /*! raw-loader!./chart-bar.component.html */
        "./node_modules/raw-loader/dist/cjs.js!./src/app/components/chart-bar/chart-bar.component.html"))["default"],
        styles: [Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(
        /*! ./chart-bar.component.scss */
        "./src/app/components/chart-bar/chart-bar.component.scss"))["default"]]
      })], ChartBarComponent);
      /***/
    },

    /***/
    "./src/app/components/chart/chart.component.scss":
    /*!*******************************************************!*\
      !*** ./src/app/components/chart/chart.component.scss ***!
      \*******************************************************/

    /*! exports provided: default */

    /***/
    function srcAppComponentsChartChartComponentScss(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony default export */


      __webpack_exports__["default"] = "ion-card {\n  width: 94%;\n  margin-top: 20px;\n}\n\n.canvas {\n  display: block;\n  height: 182px;\n  width: 324px;\n}\n\nion-card-content {\n  padding: 0;\n  margin-top: 25px;\n  margin-left: 10px;\n}\n\n.p-disabled {\n  color: #bebcbc;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvY29tcG9uZW50cy9jaGFydC9jaGFydC5jb21wb25lbnQuc2NzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQTtFQUNJLFVBQUE7RUFDQSxnQkFBQTtBQUNKOztBQUVBO0VBQ0ksY0FBQTtFQUNBLGFBQUE7RUFDQSxZQUFBO0FBQ0o7O0FBRUE7RUFDSSxVQUFBO0VBQ0EsZ0JBQUE7RUFDQSxpQkFBQTtBQUNKOztBQWNBO0VBQ0ksY0FBQTtBQVhKIiwiZmlsZSI6InNyYy9hcHAvY29tcG9uZW50cy9jaGFydC9jaGFydC5jb21wb25lbnQuc2NzcyIsInNvdXJjZXNDb250ZW50IjpbImlvbi1jYXJkIHtcbiAgICB3aWR0aDogOTQlO1xuICAgIG1hcmdpbi10b3A6IDIwcHg7XG59XG5cbi5jYW52YXMge1xuICAgIGRpc3BsYXk6IGJsb2NrO1xuICAgIGhlaWdodDogMTgycHg7XG4gICAgd2lkdGg6IDMyNHB4O1xufVxuXG5pb24tY2FyZC1jb250ZW50IHtcbiAgICBwYWRkaW5nOiAwO1xuICAgIG1hcmdpbi10b3A6IDI1cHg7XG4gICAgbWFyZ2luLWxlZnQ6IDEwcHg7XG59XG5cbi8vIC5jYXJkX2hlYWRlciBoNHtcbi8vICAgICBtYXJnaW4tdG9wOiA0cHg7XG4vLyAgICAgbWFyZ2luLWJvdHRvbTogNHB4O1xuLy8gICAgIGZvbnQtc2l6ZTogMTZweDtcbi8vICAgICBjb2xvcjogIzAwMDtcbi8vIH1cbi8vIC5jYXJkX2hlYWRlciBwe1xuLy8gICAgIG1hcmdpbi1ibG9jay1zdGFydDogMGVtO1xuLy8gICAgIG1hcmdpbi1ibG9jay1lbmQ6IDBlbTtcbi8vIH1cblxuXG4ucC1kaXNhYmxlZCB7XG4gICAgY29sb3I6ICNiZWJjYmM7XG59Il19 */";
      /***/
    },

    /***/
    "./src/app/components/chart/chart.component.ts":
    /*!*****************************************************!*\
      !*** ./src/app/components/chart/chart.component.ts ***!
      \*****************************************************/

    /*! exports provided: ChartComponent */

    /***/
    function srcAppComponentsChartChartComponentTs(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "ChartComponent", function () {
        return ChartComponent;
      });
      /* harmony import */


      var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! tslib */
      "./node_modules/tslib/tslib.es6.js");
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! @angular/core */
      "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
      /* harmony import */


      var chart_js__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! chart.js */
      "./node_modules/chart.js/dist/Chart.js");
      /* harmony import */


      var chart_js__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(chart_js__WEBPACK_IMPORTED_MODULE_2__);

      var ChartComponent = /*#__PURE__*/function () {
        function ChartComponent() {
          _classCallCheck(this, ChartComponent);
        }

        _createClass(ChartComponent, [{
          key: "ngAfterViewInit",
          value: function ngAfterViewInit() {
            this.lineChartMethod();
          }
        }, {
          key: "ngOnInit",
          value: function ngOnInit() {}
        }, {
          key: "lineChartMethod",
          value: function lineChartMethod() {
            this.lineChart = new chart_js__WEBPACK_IMPORTED_MODULE_2__["Chart"](this.lineCanvas.nativeElement, {
              type: 'line',
              data: {
                labels: ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'November', 'December'],
                datasets: [{
                  label: '',
                  fill: true,
                  lineTension: 0.1,
                  backgroundColor: '#ffd1b3',
                  borderColor: '#F4B183',
                  borderCapStyle: 'butt',
                  borderDash: [],
                  borderDashOffset: 0.0,
                  borderJoinStyle: 'miter',
                  pointBorderColor: 'rgba(75,192,192,1)',
                  pointBackgroundColor: '#fff',
                  pointBorderWidth: 1,
                  pointHoverRadius: 5,
                  pointHoverBackgroundColor: 'rgba(75,192,192,1)',
                  pointHoverBorderColor: 'rgba(220,220,220,1)',
                  pointHoverBorderWidth: 2,
                  pointRadius: 1,
                  pointHitRadius: 10,
                  data: [65, 59, 80, 81, 56, 55, 40, 10, 5, 50, 10, 15],
                  spanGaps: false
                }]
              },
              options: {
                scales: {
                  xAxes: [{
                    display: false
                  }],
                  yAxes: [{
                    position: 'right'
                  }]
                },
                legend: {
                  display: false
                }
              }
            });
          }
        }]);

        return ChartComponent;
      }();

      ChartComponent.ctorParameters = function () {
        return [];
      };

      ChartComponent.propDecorators = {
        lineCanvas: [{
          type: _angular_core__WEBPACK_IMPORTED_MODULE_1__["ViewChild"],
          args: ['lineCanvas']
        }]
      };
      ChartComponent = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-chart',
        template: Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(
        /*! raw-loader!./chart.component.html */
        "./node_modules/raw-loader/dist/cjs.js!./src/app/components/chart/chart.component.html"))["default"],
        styles: [Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(
        /*! ./chart.component.scss */
        "./src/app/components/chart/chart.component.scss"))["default"]]
      })], ChartComponent);
      /***/
    },

    /***/
    "./src/app/components/charts-checkup/charts-checkup.component.scss":
    /*!*************************************************************************!*\
      !*** ./src/app/components/charts-checkup/charts-checkup.component.scss ***!
      \*************************************************************************/

    /*! exports provided: default */

    /***/
    function srcAppComponentsChartsCheckupChartsCheckupComponentScss(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony default export */


      __webpack_exports__["default"] = "ion-card {\n  width: 94%;\n  margin-top: 20px;\n}\n\n.canvas {\n  display: block;\n  height: 182px;\n  width: 324px;\n}\n\nion-card-content {\n  padding: 0;\n  margin-top: 25px;\n  margin-left: 10px;\n}\n\n.card_header {\n  padding-inline: 5px;\n}\n\n.card_header h4 {\n  margin-top: 4px;\n  margin-bottom: 4px;\n  font-size: 16px;\n  color: #000;\n}\n\n.card_header p {\n  -webkit-margin-before: 0em;\n          margin-block-start: 0em;\n  -webkit-margin-after: 0em;\n          margin-block-end: 0em;\n}\n\n.p-disabled {\n  color: #bebcbc;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvY29tcG9uZW50cy9jaGFydHMtY2hlY2t1cC9jaGFydHMtY2hlY2t1cC5jb21wb25lbnQuc2NzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQTtFQUNJLFVBQUE7RUFDQSxnQkFBQTtBQUNKOztBQUVBO0VBQ0ksY0FBQTtFQUNBLGFBQUE7RUFDQSxZQUFBO0FBQ0o7O0FBRUE7RUFDSSxVQUFBO0VBQ0EsZ0JBQUE7RUFDQSxpQkFBQTtBQUNKOztBQUNBO0VBQ0ksbUJBQUE7QUFFSjs7QUFBQTtFQUNJLGVBQUE7RUFDQSxrQkFBQTtFQUNBLGVBQUE7RUFDQSxXQUFBO0FBR0o7O0FBREE7RUFDSSwwQkFBQTtVQUFBLHVCQUFBO0VBQ0EseUJBQUE7VUFBQSxxQkFBQTtBQUlKOztBQUFBO0VBQ0ksY0FBQTtBQUdKIiwiZmlsZSI6InNyYy9hcHAvY29tcG9uZW50cy9jaGFydHMtY2hlY2t1cC9jaGFydHMtY2hlY2t1cC5jb21wb25lbnQuc2NzcyIsInNvdXJjZXNDb250ZW50IjpbImlvbi1jYXJkIHtcbiAgICB3aWR0aDogOTQlO1xuICAgIG1hcmdpbi10b3A6IDIwcHg7XG59XG5cbi5jYW52YXMge1xuICAgIGRpc3BsYXk6IGJsb2NrO1xuICAgIGhlaWdodDogMTgycHg7XG4gICAgd2lkdGg6IDMyNHB4O1xufVxuXG5pb24tY2FyZC1jb250ZW50IHtcbiAgICBwYWRkaW5nOiAwO1xuICAgIG1hcmdpbi10b3A6IDI1cHg7XG4gICAgbWFyZ2luLWxlZnQ6IDEwcHg7XG59XG4uY2FyZF9oZWFkZXIge1xuICAgIHBhZGRpbmctaW5saW5lOiA1cHg7XG59XG4uY2FyZF9oZWFkZXIgaDR7XG4gICAgbWFyZ2luLXRvcDogNHB4O1xuICAgIG1hcmdpbi1ib3R0b206IDRweDtcbiAgICBmb250LXNpemU6IDE2cHg7XG4gICAgY29sb3I6ICMwMDA7XG59XG4uY2FyZF9oZWFkZXIgcHtcbiAgICBtYXJnaW4tYmxvY2stc3RhcnQ6IDBlbTtcbiAgICBtYXJnaW4tYmxvY2stZW5kOiAwZW07XG59XG5cblxuLnAtZGlzYWJsZWQge1xuICAgIGNvbG9yOiAjYmViY2JjO1xufSJdfQ== */";
      /***/
    },

    /***/
    "./src/app/components/charts-checkup/charts-checkup.component.ts":
    /*!***********************************************************************!*\
      !*** ./src/app/components/charts-checkup/charts-checkup.component.ts ***!
      \***********************************************************************/

    /*! exports provided: ChartsCheckupComponent */

    /***/
    function srcAppComponentsChartsCheckupChartsCheckupComponentTs(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "ChartsCheckupComponent", function () {
        return ChartsCheckupComponent;
      });
      /* harmony import */


      var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! tslib */
      "./node_modules/tslib/tslib.es6.js");
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! @angular/core */
      "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
      /* harmony import */


      var chart_js__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! chart.js */
      "./node_modules/chart.js/dist/Chart.js");
      /* harmony import */


      var chart_js__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(chart_js__WEBPACK_IMPORTED_MODULE_2__);

      var ChartsCheckupComponent = /*#__PURE__*/function () {
        function ChartsCheckupComponent() {
          _classCallCheck(this, ChartsCheckupComponent);
        }

        _createClass(ChartsCheckupComponent, [{
          key: "ngAfterViewInit",
          value: function ngAfterViewInit() {
            this.lineChartMethod();
          }
        }, {
          key: "ngOnInit",
          value: function ngOnInit() {}
        }, {
          key: "lineChartMethod",
          value: function lineChartMethod() {
            this.lineChart = new chart_js__WEBPACK_IMPORTED_MODULE_2__["Chart"](this.lineCanvas.nativeElement, {
              type: 'line',
              data: {
                labels: ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'November', 'December'],
                datasets: [{
                  label: '',
                  fill: true,
                  lineTension: 0.1,
                  backgroundColor: '#ffd1b3',
                  borderColor: '#F4B183',
                  borderCapStyle: 'butt',
                  borderDash: [],
                  borderDashOffset: 0.0,
                  borderJoinStyle: 'miter',
                  pointBorderColor: 'rgba(75,192,192,1)',
                  pointBackgroundColor: '#fff',
                  pointBorderWidth: 1,
                  pointHoverRadius: 5,
                  pointHoverBackgroundColor: 'rgba(75,192,192,1)',
                  pointHoverBorderColor: 'rgba(220,220,220,1)',
                  pointHoverBorderWidth: 2,
                  pointRadius: 1,
                  pointHitRadius: 10,
                  data: [65, 59, 80, 81, 56, 55, 40, 10, 5, 50, 10, 15],
                  spanGaps: false
                }]
              },
              options: {
                scales: {
                  xAxes: [{
                    display: false
                  }],
                  yAxes: [{
                    position: 'right'
                  }]
                },
                legend: {
                  display: false
                }
              }
            });
          }
        }]);

        return ChartsCheckupComponent;
      }();

      ChartsCheckupComponent.ctorParameters = function () {
        return [];
      };

      ChartsCheckupComponent.propDecorators = {
        lineCanvas: [{
          type: _angular_core__WEBPACK_IMPORTED_MODULE_1__["ViewChild"],
          args: ['lineCanvas']
        }]
      };
      ChartsCheckupComponent = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-charts-checkup',
        template: Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(
        /*! raw-loader!./charts-checkup.component.html */
        "./node_modules/raw-loader/dist/cjs.js!./src/app/components/charts-checkup/charts-checkup.component.html"))["default"],
        styles: [Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(
        /*! ./charts-checkup.component.scss */
        "./src/app/components/charts-checkup/charts-checkup.component.scss"))["default"]]
      })], ChartsCheckupComponent);
      /***/
    },

    /***/
    "./src/app/components/check-ups-profile/check-ups-profile.component.scss":
    /*!*******************************************************************************!*\
      !*** ./src/app/components/check-ups-profile/check-ups-profile.component.scss ***!
      \*******************************************************************************/

    /*! exports provided: default */

    /***/
    function srcAppComponentsCheckUpsProfileCheckUpsProfileComponentScss(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony default export */


      __webpack_exports__["default"] = "ion-grid {\n  --padding-start: 10px;\n  --padding-end: 10px ;\n}\n\n.performance_text {\n  text-align: justify;\n  line-height: 1.1rem;\n  font-size: 13px;\n  font-weight: 100;\n}\n\n.main_container {\n  padding-top: 15px;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvY29tcG9uZW50cy9jaGVjay11cHMtcHJvZmlsZS9jaGVjay11cHMtcHJvZmlsZS5jb21wb25lbnQuc2NzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQTtFQUNJLHFCQUFBO0VBQ0Esb0JBQUE7QUFDSjs7QUFFQTtFQUNJLG1CQUFBO0VBQ0EsbUJBQUE7RUFDQSxlQUFBO0VBQ0EsZ0JBQUE7QUFDSjs7QUFHQTtFQUVJLGlCQUFBO0FBREoiLCJmaWxlIjoic3JjL2FwcC9jb21wb25lbnRzL2NoZWNrLXVwcy1wcm9maWxlL2NoZWNrLXVwcy1wcm9maWxlLmNvbXBvbmVudC5zY3NzIiwic291cmNlc0NvbnRlbnQiOlsiaW9uLWdyaWQge1xuICAgIC0tcGFkZGluZy1zdGFydDogMTBweDtcbiAgICAtLXBhZGRpbmctZW5kOiAxMHB4XG59XG5cbi5wZXJmb3JtYW5jZV90ZXh0IHtcbiAgICB0ZXh0LWFsaWduOiBqdXN0aWZ5O1xuICAgIGxpbmUtaGVpZ2h0OiAxLjFyZW07XG4gICAgZm9udC1zaXplOiAxM3B4O1xuICAgIGZvbnQtd2VpZ2h0OiAxMDA7XG5cbn1cblxuLm1haW5fY29udGFpbmVyIHtcbiAgICAvLyBwYWRkaW5nLWxlZnQ6IDEwcHg7XG4gICAgcGFkZGluZy10b3A6IDE1cHg7XG4gICAgLy8gcGFkZGluZy1yaWdodDogMTBweDtcbn1cblxuIl19 */";
      /***/
    },

    /***/
    "./src/app/components/check-ups-profile/check-ups-profile.component.ts":
    /*!*****************************************************************************!*\
      !*** ./src/app/components/check-ups-profile/check-ups-profile.component.ts ***!
      \*****************************************************************************/

    /*! exports provided: CheckUpsProfileComponent */

    /***/
    function srcAppComponentsCheckUpsProfileCheckUpsProfileComponentTs(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "CheckUpsProfileComponent", function () {
        return CheckUpsProfileComponent;
      });
      /* harmony import */


      var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! tslib */
      "./node_modules/tslib/tslib.es6.js");
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! @angular/core */
      "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");

      var CheckUpsProfileComponent = /*#__PURE__*/function () {
        function CheckUpsProfileComponent() {
          _classCallCheck(this, CheckUpsProfileComponent);
        }

        _createClass(CheckUpsProfileComponent, [{
          key: "ngOnInit",
          value: function ngOnInit() {}
        }]);

        return CheckUpsProfileComponent;
      }();

      CheckUpsProfileComponent.ctorParameters = function () {
        return [];
      };

      CheckUpsProfileComponent = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-check-ups-profile',
        template: Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(
        /*! raw-loader!./check-ups-profile.component.html */
        "./node_modules/raw-loader/dist/cjs.js!./src/app/components/check-ups-profile/check-ups-profile.component.html"))["default"],
        styles: [Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(
        /*! ./check-ups-profile.component.scss */
        "./src/app/components/check-ups-profile/check-ups-profile.component.scss"))["default"]]
      })], CheckUpsProfileComponent);
      /***/
    },

    /***/
    "./src/app/components/coach-payment-visibility/coach-payment-visibility.component.scss":
    /*!*********************************************************************************************!*\
      !*** ./src/app/components/coach-payment-visibility/coach-payment-visibility.component.scss ***!
      \*********************************************************************************************/

    /*! exports provided: default */

    /***/
    function srcAppComponentsCoachPaymentVisibilityCoachPaymentVisibilityComponentScss(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony default export */


      __webpack_exports__["default"] = "ion-grid {\n  --padding-start: 10px;\n  --padding-end: 10px ;\n}\n\n.padding-start-0 {\n  --padding-start: 0 !important;\n}\n\n.main_container {\n  padding-left: 10px;\n  padding-top: 15px;\n  padding-right: 10px;\n}\n\n.performance_text {\n  text-align: justify;\n  line-height: 1.1rem;\n  font-size: 12px;\n  font-weight: 100;\n}\n\nion-icon {\n  padding: 0 !important;\n  font-size: 14px;\n  margin: 0;\n  margin-right: 5px !important;\n}\n\nion-item {\n  height: 45px;\n}\n\nion-label {\n  padding-bottom: 0 !important;\n  padding-top: 0 !important;\n  margin-top: 0 !important;\n  margin-bottom: 0 !important;\n  white-space: unset !important;\n}\n\n.subscription td {\n  padding: 15px;\n}\n\n.subscription td {\n  border-right: solid 1px grey;\n}\n\n.subscription td:last-child {\n  border: none !important;\n}\n\n.subscription tr:nth-child(2) {\n  border-bottom: solid 1px grey;\n}\n\n.profile-viewall {\n  width: 100%;\n  position: relative;\n  margin-top: 15px;\n  margin-bottom: 15px;\n}\n\n.profile-viewall ion-button {\n  --border-radius: 25px;\n  display: table;\n  margin: auto;\n  font-size: 12px;\n}\n\n.profile-viewall:before {\n  content: \"\";\n  position: absolute;\n  background: #c1c1c1;\n  height: 1px;\n  width: 100%;\n  top: 19px;\n  left: 0;\n}\n\n.container_data {\n  position: relative;\n  display: block;\n  width: 100%;\n  margin-top: 15px;\n}\n\n.tag {\n  background: #f5f6f9;\n  padding: 8px;\n  border-radius: 10px;\n}\n\n.tag p {\n  padding: 0 !important;\n  margin: 0 !important;\n}\n\n.value {\n  background: #f5f6f9;\n  padding: 8px;\n  border-radius: 10px;\n}\n\n.value p {\n  padding: 0 !important;\n  margin: 0 !important;\n}\n\n.next {\n  --background: var(--ion-btn-custom-color);\n  font-size: 16px;\n}\n\n.history td {\n  font-size: 10px;\n  padding: 10px;\n  padding-right: 40px;\n  border-bottom: solid 1px grey;\n}\n\n.history td span {\n  background: #2dd36f;\n  font-size: 8px;\n  padding: 5px 10px 5px 10px;\n  margin-left: 5px;\n}\n\n.history td span:after {\n  content: url('tick (1).png');\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvY29tcG9uZW50cy9jb2FjaC1wYXltZW50LXZpc2liaWxpdHkvY29hY2gtcGF5bWVudC12aXNpYmlsaXR5LmNvbXBvbmVudC5zY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBO0VBQ0kscUJBQUE7RUFDQSxvQkFBQTtBQUNKOztBQUNBO0VBQ0ksNkJBQUE7QUFFSjs7QUFDQTtFQUNJLGtCQUFBO0VBQ0EsaUJBQUE7RUFDQSxtQkFBQTtBQUVKOztBQUFBO0VBQ0ksbUJBQUE7RUFDQSxtQkFBQTtFQUNBLGVBQUE7RUFDQSxnQkFBQTtBQUdKOztBQURBO0VBQ0kscUJBQUE7RUFDQSxlQUFBO0VBQ0EsU0FBQTtFQUNBLDRCQUFBO0FBSUo7O0FBRkE7RUFDSSxZQUFBO0FBS0o7O0FBSEE7RUFDSSw0QkFBQTtFQUNBLHlCQUFBO0VBQ0Esd0JBQUE7RUFDQSwyQkFBQTtFQUNBLDZCQUFBO0FBTUo7O0FBRkk7RUFDSSxhQUFBO0FBS1I7O0FBRkk7RUFDSSw0QkFBQTtBQUlSOztBQUZJO0VBQ0ksdUJBQUE7QUFJUjs7QUFESTtFQUNJLDZCQUFBO0FBR1I7O0FBRUE7RUFDSSxXQUFBO0VBQ0Esa0JBQUE7RUFDQSxnQkFBQTtFQUNBLG1CQUFBO0FBQ0o7O0FBRUE7RUFDSSxxQkFBQTtFQUNBLGNBQUE7RUFDQSxZQUFBO0VBQ0EsZUFBQTtBQUNKOztBQUVBO0VBQ0ksV0FBQTtFQUNBLGtCQUFBO0VBQ0EsbUJBQUE7RUFDQSxXQUFBO0VBQ0EsV0FBQTtFQUNBLFNBQUE7RUFDQSxPQUFBO0FBQ0o7O0FBQ0E7RUFDSSxrQkFBQTtFQUNBLGNBQUE7RUFDQSxXQUFBO0VBQ0EsZ0JBQUE7QUFFSjs7QUFBQTtFQUNJLG1CQUFBO0VBQ0EsWUFBQTtFQUNBLG1CQUFBO0FBR0o7O0FBRkk7RUFDSSxxQkFBQTtFQUNBLG9CQUFBO0FBSVI7O0FBREE7RUFDSSxtQkFBQTtFQUNBLFlBQUE7RUFDQSxtQkFBQTtBQUlKOztBQUhJO0VBQ0kscUJBQUE7RUFDQSxvQkFBQTtBQUtSOztBQUZBO0VBQ0kseUNBQUE7RUFDSSxlQUFBO0FBS1I7O0FBREk7RUFDSSxlQUFBO0VBQ0EsYUFBQTtFQUNBLG1CQUFBO0VBQ0EsNkJBQUE7QUFJUjs7QUFIUTtFQUNJLG1CQUFBO0VBQ0EsY0FBQTtFQUNBLDBCQUFBO0VBQ0EsZ0JBQUE7QUFLWjs7QUFIUTtFQUNJLDRCQUFBO0FBS1oiLCJmaWxlIjoic3JjL2FwcC9jb21wb25lbnRzL2NvYWNoLXBheW1lbnQtdmlzaWJpbGl0eS9jb2FjaC1wYXltZW50LXZpc2liaWxpdHkuY29tcG9uZW50LnNjc3MiLCJzb3VyY2VzQ29udGVudCI6WyJpb24tZ3JpZCB7XG4gICAgLS1wYWRkaW5nLXN0YXJ0OiAxMHB4O1xuICAgIC0tcGFkZGluZy1lbmQ6IDEwcHhcbn1cbi5wYWRkaW5nLXN0YXJ0LTAge1xuICAgIC0tcGFkZGluZy1zdGFydDogMCAhaW1wb3J0YW50O1xufVxuXG4ubWFpbl9jb250YWluZXIge1xuICAgIHBhZGRpbmctbGVmdDogMTBweDtcbiAgICBwYWRkaW5nLXRvcDogMTVweDtcbiAgICBwYWRkaW5nLXJpZ2h0OiAxMHB4O1xufVxuLnBlcmZvcm1hbmNlX3RleHQge1xuICAgIHRleHQtYWxpZ246IGp1c3RpZnk7XG4gICAgbGluZS1oZWlnaHQ6IDEuMXJlbTtcbiAgICBmb250LXNpemU6IDEycHg7XG4gICAgZm9udC13ZWlnaHQ6IDEwMDtcbn1cbmlvbi1pY29uIHtcbiAgICBwYWRkaW5nOiAwICFpbXBvcnRhbnQ7XG4gICAgZm9udC1zaXplOiAxNHB4O1xuICAgIG1hcmdpbjogMDtcbiAgICBtYXJnaW4tcmlnaHQ6IDVweCAhaW1wb3J0YW50O1xufVxuaW9uLWl0ZW0ge1xuICAgIGhlaWdodDogNDVweDtcbn1cbmlvbi1sYWJlbCB7XG4gICAgcGFkZGluZy1ib3R0b206IDAgIWltcG9ydGFudDtcbiAgICBwYWRkaW5nLXRvcDogMCAhaW1wb3J0YW50O1xuICAgIG1hcmdpbi10b3A6IDAgIWltcG9ydGFudDtcbiAgICBtYXJnaW4tYm90dG9tOiAwICFpbXBvcnRhbnQ7XG4gICAgd2hpdGUtc3BhY2U6IHVuc2V0ICFpbXBvcnRhbnQ7XG4gICAgXG59XG4uc3Vic2NyaXB0aW9uIHtcbiAgICB0ZCB7XG4gICAgICAgIHBhZGRpbmc6IDE1cHg7XG4gICAgICAgXG4gICAgfVxuICAgIHRke1xuICAgICAgICBib3JkZXItcmlnaHQ6IHNvbGlkIDFweCBncmV5O1xuICAgIH1cbiAgICB0ZDpsYXN0LWNoaWxkIHtcbiAgICAgICAgYm9yZGVyOiBub25lICFpbXBvcnRhbnQ7XG4gICAgICBcbiAgICB9XG4gICAgdHI6bnRoLWNoaWxkKDIpIHtcbiAgICAgICAgYm9yZGVyLWJvdHRvbTogc29saWQgMXB4IGdyZXk7XG4gICAgICAgXG4gICAgfVxufVxuXG4ucHJvZmlsZS12aWV3YWxsIHtcbiAgICB3aWR0aDogMTAwJTtcbiAgICBwb3NpdGlvbjogcmVsYXRpdmU7XG4gICAgbWFyZ2luLXRvcDogMTVweDtcbiAgICBtYXJnaW4tYm90dG9tOiAxNXB4O1xufVxuXG4ucHJvZmlsZS12aWV3YWxsIGlvbi1idXR0b24ge1xuICAgIC0tYm9yZGVyLXJhZGl1czogMjVweDtcbiAgICBkaXNwbGF5OiB0YWJsZTtcbiAgICBtYXJnaW46IGF1dG87XG4gICAgZm9udC1zaXplOiAxMnB4O1xufVxuXG4ucHJvZmlsZS12aWV3YWxsOmJlZm9yZSB7XG4gICAgY29udGVudDogXCJcIjtcbiAgICBwb3NpdGlvbjogYWJzb2x1dGU7XG4gICAgYmFja2dyb3VuZDogI2MxYzFjMTtcbiAgICBoZWlnaHQ6IDFweDtcbiAgICB3aWR0aDogMTAwJTtcbiAgICB0b3A6IDE5cHg7XG4gICAgbGVmdDogMDtcbn1cbi5jb250YWluZXJfZGF0YSB7XG4gICAgcG9zaXRpb246IHJlbGF0aXZlO1xuICAgIGRpc3BsYXk6IGJsb2NrO1xuICAgIHdpZHRoOiAxMDAlO1xuICAgIG1hcmdpbi10b3A6IDE1cHg7XG59XG4udGFnIHtcbiAgICBiYWNrZ3JvdW5kOiAjZjVmNmY5O1xuICAgIHBhZGRpbmc6IDhweDtcbiAgICBib3JkZXItcmFkaXVzOiAxMHB4O1xuICAgIHB7XG4gICAgICAgIHBhZGRpbmc6IDAgIWltcG9ydGFudDtcbiAgICAgICAgbWFyZ2luOiAwICFpbXBvcnRhbnQ7XG4gICAgfVxufVxuLnZhbHVlIHtcbiAgICBiYWNrZ3JvdW5kOiAjZjVmNmY5O1xuICAgIHBhZGRpbmc6IDhweDtcbiAgICBib3JkZXItcmFkaXVzOiAxMHB4O1xuICAgIHB7XG4gICAgICAgIHBhZGRpbmc6IDAgIWltcG9ydGFudDtcbiAgICAgICAgbWFyZ2luOiAwICFpbXBvcnRhbnQ7XG4gICAgfVxufVxuLm5leHQge1xuICAgIC0tYmFja2dyb3VuZDogdmFyKC0taW9uLWJ0bi1jdXN0b20tY29sb3IpO1xuICAgICAgICBmb250LXNpemU6IDE2cHg7XG4gICAgICAgIFxufVxuLmhpc3Rvcnkge1xuICAgIHRkIHtcbiAgICAgICAgZm9udC1zaXplOiAxMHB4O1xuICAgICAgICBwYWRkaW5nOiAxMHB4O1xuICAgICAgICBwYWRkaW5nLXJpZ2h0OiA0MHB4O1xuICAgICAgICBib3JkZXItYm90dG9tOiBzb2xpZCAxcHggZ3JleTtcbiAgICAgICAgc3BhbiB7XG4gICAgICAgICAgICBiYWNrZ3JvdW5kOiAjMmRkMzZmO1xuICAgICAgICAgICAgZm9udC1zaXplOiA4cHg7XG4gICAgICAgICAgICBwYWRkaW5nOiA1cHggMTBweCA1cHggMTBweDtcbiAgICAgICAgICAgIG1hcmdpbi1sZWZ0OiA1cHg7XG4gICAgICAgIH1cbiAgICAgICAgc3BhbjphZnRlciB7XG4gICAgICAgICAgICBjb250ZW50OiB1cmwoJy4uLy4uLy4uL2Fzc2V0cy9pY29uL3RpY2tcXCBcXCgxXFwpLnBuZycpO1xuICAgICAgICAgICAgXG4gICAgICAgIH1cbiAgICB9XG59Il19 */";
      /***/
    },

    /***/
    "./src/app/components/coach-payment-visibility/coach-payment-visibility.component.ts":
    /*!*******************************************************************************************!*\
      !*** ./src/app/components/coach-payment-visibility/coach-payment-visibility.component.ts ***!
      \*******************************************************************************************/

    /*! exports provided: CoachPaymentVisibilityComponent */

    /***/
    function srcAppComponentsCoachPaymentVisibilityCoachPaymentVisibilityComponentTs(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "CoachPaymentVisibilityComponent", function () {
        return CoachPaymentVisibilityComponent;
      });
      /* harmony import */


      var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! tslib */
      "./node_modules/tslib/tslib.es6.js");
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! @angular/core */
      "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
      /* harmony import */


      var _ionic_angular__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! @ionic/angular */
      "./node_modules/@ionic/angular/__ivy_ngcc__/fesm2015/ionic-angular.js");
      /* harmony import */


      var _components_modals_coach_chat_modal_coach_chat_modal_component__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
      /*! @components/modals/coach-chat-modal/coach-chat-modal.component */
      "./src/app/components/modals/coach-chat-modal/coach-chat-modal.component.ts");
      /* harmony import */


      var _components_modals_coach_new_request_coach_new_request_component__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
      /*! @components/modals/coach-new-request/coach-new-request.component */
      "./src/app/components/modals/coach-new-request/coach-new-request.component.ts");

      var CoachPaymentVisibilityComponent = /*#__PURE__*/function () {
        function CoachPaymentVisibilityComponent(modal) {
          _classCallCheck(this, CoachPaymentVisibilityComponent);

          this.modal = modal;
        }

        _createClass(CoachPaymentVisibilityComponent, [{
          key: "ngOnInit",
          value: function ngOnInit() {}
        }, {
          key: "openChat",
          value: function openChat() {
            return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, /*#__PURE__*/regeneratorRuntime.mark(function _callee4() {
              var modal;
              return regeneratorRuntime.wrap(function _callee4$(_context4) {
                while (1) {
                  switch (_context4.prev = _context4.next) {
                    case 0:
                      _context4.next = 2;
                      return this.modal.create({
                        component: _components_modals_coach_chat_modal_coach_chat_modal_component__WEBPACK_IMPORTED_MODULE_3__["CoachChatModalComponent"],
                        cssClass: 'coach-chat-modal',
                        showBackdrop: false
                      });

                    case 2:
                      modal = _context4.sent;
                      _context4.next = 5;
                      return modal.present();

                    case 5:
                      return _context4.abrupt("return", _context4.sent);

                    case 6:
                    case "end":
                      return _context4.stop();
                  }
                }
              }, _callee4, this);
            }));
          }
        }, {
          key: "openNewRequest",
          value: function openNewRequest() {
            return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, /*#__PURE__*/regeneratorRuntime.mark(function _callee5() {
              var modal;
              return regeneratorRuntime.wrap(function _callee5$(_context5) {
                while (1) {
                  switch (_context5.prev = _context5.next) {
                    case 0:
                      _context5.next = 2;
                      return this.modal.create({
                        component: _components_modals_coach_new_request_coach_new_request_component__WEBPACK_IMPORTED_MODULE_4__["CoachNewRequestComponent"],
                        cssClass: 'coach-new-request-modal',
                        showBackdrop: false
                      });

                    case 2:
                      modal = _context5.sent;
                      _context5.next = 5;
                      return modal.present();

                    case 5:
                      return _context5.abrupt("return", _context5.sent);

                    case 6:
                    case "end":
                      return _context5.stop();
                  }
                }
              }, _callee5, this);
            }));
          }
        }]);

        return CoachPaymentVisibilityComponent;
      }();

      CoachPaymentVisibilityComponent.ctorParameters = function () {
        return [{
          type: _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["ModalController"]
        }];
      };

      CoachPaymentVisibilityComponent = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-coach-payment-visibility',
        template: Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(
        /*! raw-loader!./coach-payment-visibility.component.html */
        "./node_modules/raw-loader/dist/cjs.js!./src/app/components/coach-payment-visibility/coach-payment-visibility.component.html"))["default"],
        styles: [Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(
        /*! ./coach-payment-visibility.component.scss */
        "./src/app/components/coach-payment-visibility/coach-payment-visibility.component.scss"))["default"]]
      })], CoachPaymentVisibilityComponent);
      /***/
    },

    /***/
    "./src/app/components/coach/chart-one/chart-one.component.scss":
    /*!*********************************************************************!*\
      !*** ./src/app/components/coach/chart-one/chart-one.component.scss ***!
      \*********************************************************************/

    /*! exports provided: default */

    /***/
    function srcAppComponentsCoachChartOneChartOneComponentScss(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony default export */


      __webpack_exports__["default"] = "ion-card {\n  width: 94%;\n  margin-top: 20px;\n}\n\n.canvas {\n  display: block;\n  height: 182px;\n  width: 324px;\n}\n\nion-card-content {\n  padding: 0;\n  margin-top: 25px;\n  margin-left: 10px;\n}\n\n.card_header {\n  padding-inline: 5px;\n}\n\n.card_header h4 {\n  margin-top: 4px;\n  margin-bottom: 4px;\n  font-size: 16px;\n  color: #000;\n}\n\n.card_header p {\n  -webkit-margin-before: 0em;\n          margin-block-start: 0em;\n  -webkit-margin-after: 0em;\n          margin-block-end: 0em;\n}\n\n.p-disabled {\n  color: #bebcbc;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvY29tcG9uZW50cy9jb2FjaC9jaGFydC1vbmUvY2hhcnQtb25lLmNvbXBvbmVudC5zY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBO0VBQ0ksVUFBQTtFQUNBLGdCQUFBO0FBQ0o7O0FBRUE7RUFDSSxjQUFBO0VBQ0EsYUFBQTtFQUNBLFlBQUE7QUFDSjs7QUFFQTtFQUNJLFVBQUE7RUFDQSxnQkFBQTtFQUNBLGlCQUFBO0FBQ0o7O0FBQ0E7RUFDSSxtQkFBQTtBQUVKOztBQUFBO0VBQ0ksZUFBQTtFQUNBLGtCQUFBO0VBQ0EsZUFBQTtFQUNBLFdBQUE7QUFHSjs7QUFEQTtFQUNJLDBCQUFBO1VBQUEsdUJBQUE7RUFDQSx5QkFBQTtVQUFBLHFCQUFBO0FBSUo7O0FBQUE7RUFDSSxjQUFBO0FBR0oiLCJmaWxlIjoic3JjL2FwcC9jb21wb25lbnRzL2NvYWNoL2NoYXJ0LW9uZS9jaGFydC1vbmUuY29tcG9uZW50LnNjc3MiLCJzb3VyY2VzQ29udGVudCI6WyJpb24tY2FyZCB7XG4gICAgd2lkdGg6IDk0JTtcbiAgICBtYXJnaW4tdG9wOiAyMHB4O1xufVxuXG4uY2FudmFzIHtcbiAgICBkaXNwbGF5OiBibG9jaztcbiAgICBoZWlnaHQ6IDE4MnB4O1xuICAgIHdpZHRoOiAzMjRweDtcbn1cblxuaW9uLWNhcmQtY29udGVudCB7XG4gICAgcGFkZGluZzogMDtcbiAgICBtYXJnaW4tdG9wOiAyNXB4O1xuICAgIG1hcmdpbi1sZWZ0OiAxMHB4O1xufVxuLmNhcmRfaGVhZGVyIHtcbiAgICBwYWRkaW5nLWlubGluZTogNXB4O1xufVxuLmNhcmRfaGVhZGVyIGg0e1xuICAgIG1hcmdpbi10b3A6IDRweDtcbiAgICBtYXJnaW4tYm90dG9tOiA0cHg7XG4gICAgZm9udC1zaXplOiAxNnB4O1xuICAgIGNvbG9yOiAjMDAwO1xufVxuLmNhcmRfaGVhZGVyIHB7XG4gICAgbWFyZ2luLWJsb2NrLXN0YXJ0OiAwZW07XG4gICAgbWFyZ2luLWJsb2NrLWVuZDogMGVtO1xufVxuXG5cbi5wLWRpc2FibGVkIHtcbiAgICBjb2xvcjogI2JlYmNiYztcbn0iXX0= */";
      /***/
    },

    /***/
    "./src/app/components/coach/chart-one/chart-one.component.ts":
    /*!*******************************************************************!*\
      !*** ./src/app/components/coach/chart-one/chart-one.component.ts ***!
      \*******************************************************************/

    /*! exports provided: ChartOneComponent */

    /***/
    function srcAppComponentsCoachChartOneChartOneComponentTs(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "ChartOneComponent", function () {
        return ChartOneComponent;
      });
      /* harmony import */


      var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! tslib */
      "./node_modules/tslib/tslib.es6.js");
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! @angular/core */
      "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
      /* harmony import */


      var chart_js__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! chart.js */
      "./node_modules/chart.js/dist/Chart.js");
      /* harmony import */


      var chart_js__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(chart_js__WEBPACK_IMPORTED_MODULE_2__);

      var ChartOneComponent = /*#__PURE__*/function () {
        function ChartOneComponent() {
          _classCallCheck(this, ChartOneComponent);
        }

        _createClass(ChartOneComponent, [{
          key: "ngAfterViewInit",
          value: function ngAfterViewInit() {
            this.lineChartMethod();
          }
        }, {
          key: "ngOnInit",
          value: function ngOnInit() {}
        }, {
          key: "lineChartMethod",
          value: function lineChartMethod() {
            this.lineChart = new chart_js__WEBPACK_IMPORTED_MODULE_2__["Chart"](this.lineCanvas.nativeElement, {
              type: 'line',
              data: {
                labels: ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'November', 'December'],
                datasets: [{
                  label: '',
                  fill: true,
                  lineTension: 0.1,
                  backgroundColor: '#ffd1b3',
                  borderColor: '#F4B183',
                  borderCapStyle: 'butt',
                  borderDash: [],
                  borderDashOffset: 0.0,
                  borderJoinStyle: 'miter',
                  pointBorderColor: 'rgba(75,192,192,1)',
                  pointBackgroundColor: '#fff',
                  pointBorderWidth: 1,
                  pointHoverRadius: 5,
                  pointHoverBackgroundColor: 'rgba(75,192,192,1)',
                  pointHoverBorderColor: 'rgba(220,220,220,1)',
                  pointHoverBorderWidth: 2,
                  pointRadius: 1,
                  pointHitRadius: 10,
                  data: [65, 59, 80, 81, 56, 55, 40, 10, 5, 50, 10, 15],
                  spanGaps: false
                }]
              },
              options: {
                scales: {
                  xAxes: [{
                    display: false
                  }],
                  yAxes: [{
                    position: 'right'
                  }]
                },
                legend: {
                  display: false
                }
              }
            });
          }
        }]);

        return ChartOneComponent;
      }();

      ChartOneComponent.ctorParameters = function () {
        return [];
      };

      ChartOneComponent.propDecorators = {
        lineCanvas: [{
          type: _angular_core__WEBPACK_IMPORTED_MODULE_1__["ViewChild"],
          args: ['lineCanvas']
        }]
      };
      ChartOneComponent = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-chart-one',
        template: Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(
        /*! raw-loader!./chart-one.component.html */
        "./node_modules/raw-loader/dist/cjs.js!./src/app/components/coach/chart-one/chart-one.component.html"))["default"],
        styles: [Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(
        /*! ./chart-one.component.scss */
        "./src/app/components/coach/chart-one/chart-one.component.scss"))["default"]]
      })], ChartOneComponent);
      /***/
    },

    /***/
    "./src/app/components/coach/chart-two/chart-two.component.scss":
    /*!*********************************************************************!*\
      !*** ./src/app/components/coach/chart-two/chart-two.component.scss ***!
      \*********************************************************************/

    /*! exports provided: default */

    /***/
    function srcAppComponentsCoachChartTwoChartTwoComponentScss(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony default export */


      __webpack_exports__["default"] = "ion-card {\n  width: 94%;\n  margin-top: 20px;\n}\n\n.canvas {\n  display: block;\n  height: 182px;\n  width: 324px;\n}\n\nion-card-content {\n  padding: 0;\n  margin-top: 25px;\n  margin-left: 10px;\n}\n\n.card_header {\n  padding-inline: 5px;\n}\n\n.card_header h4 {\n  margin-top: 4px;\n  margin-bottom: 4px;\n  font-size: 16px;\n  color: #000;\n}\n\n.card_header p {\n  -webkit-margin-before: 0em;\n          margin-block-start: 0em;\n  -webkit-margin-after: 0em;\n          margin-block-end: 0em;\n}\n\n.p-disabled {\n  color: #bebcbc;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvY29tcG9uZW50cy9jb2FjaC9jaGFydC10d28vY2hhcnQtdHdvLmNvbXBvbmVudC5zY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBO0VBQ0ksVUFBQTtFQUNBLGdCQUFBO0FBQ0o7O0FBRUE7RUFDSSxjQUFBO0VBQ0EsYUFBQTtFQUNBLFlBQUE7QUFDSjs7QUFFQTtFQUNJLFVBQUE7RUFDQSxnQkFBQTtFQUNBLGlCQUFBO0FBQ0o7O0FBQ0E7RUFDSSxtQkFBQTtBQUVKOztBQUFBO0VBQ0ksZUFBQTtFQUNBLGtCQUFBO0VBQ0EsZUFBQTtFQUNBLFdBQUE7QUFHSjs7QUFEQTtFQUNJLDBCQUFBO1VBQUEsdUJBQUE7RUFDQSx5QkFBQTtVQUFBLHFCQUFBO0FBSUo7O0FBQUE7RUFDSSxjQUFBO0FBR0oiLCJmaWxlIjoic3JjL2FwcC9jb21wb25lbnRzL2NvYWNoL2NoYXJ0LXR3by9jaGFydC10d28uY29tcG9uZW50LnNjc3MiLCJzb3VyY2VzQ29udGVudCI6WyJpb24tY2FyZCB7XG4gICAgd2lkdGg6IDk0JTtcbiAgICBtYXJnaW4tdG9wOiAyMHB4O1xufVxuXG4uY2FudmFzIHtcbiAgICBkaXNwbGF5OiBibG9jaztcbiAgICBoZWlnaHQ6IDE4MnB4O1xuICAgIHdpZHRoOiAzMjRweDtcbn1cblxuaW9uLWNhcmQtY29udGVudCB7XG4gICAgcGFkZGluZzogMDtcbiAgICBtYXJnaW4tdG9wOiAyNXB4O1xuICAgIG1hcmdpbi1sZWZ0OiAxMHB4O1xufVxuLmNhcmRfaGVhZGVyIHtcbiAgICBwYWRkaW5nLWlubGluZTogNXB4O1xufVxuLmNhcmRfaGVhZGVyIGg0e1xuICAgIG1hcmdpbi10b3A6IDRweDtcbiAgICBtYXJnaW4tYm90dG9tOiA0cHg7XG4gICAgZm9udC1zaXplOiAxNnB4O1xuICAgIGNvbG9yOiAjMDAwO1xufVxuLmNhcmRfaGVhZGVyIHB7XG4gICAgbWFyZ2luLWJsb2NrLXN0YXJ0OiAwZW07XG4gICAgbWFyZ2luLWJsb2NrLWVuZDogMGVtO1xufVxuXG5cbi5wLWRpc2FibGVkIHtcbiAgICBjb2xvcjogI2JlYmNiYztcbn0iXX0= */";
      /***/
    },

    /***/
    "./src/app/components/coach/chart-two/chart-two.component.ts":
    /*!*******************************************************************!*\
      !*** ./src/app/components/coach/chart-two/chart-two.component.ts ***!
      \*******************************************************************/

    /*! exports provided: ChartTwoComponent */

    /***/
    function srcAppComponentsCoachChartTwoChartTwoComponentTs(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "ChartTwoComponent", function () {
        return ChartTwoComponent;
      });
      /* harmony import */


      var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! tslib */
      "./node_modules/tslib/tslib.es6.js");
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! @angular/core */
      "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
      /* harmony import */


      var chart_js__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! chart.js */
      "./node_modules/chart.js/dist/Chart.js");
      /* harmony import */


      var chart_js__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(chart_js__WEBPACK_IMPORTED_MODULE_2__);

      var ChartTwoComponent = /*#__PURE__*/function () {
        function ChartTwoComponent() {
          _classCallCheck(this, ChartTwoComponent);
        }

        _createClass(ChartTwoComponent, [{
          key: "ngAfterViewInit",
          value: function ngAfterViewInit() {
            this.lineChartMethod();
          }
        }, {
          key: "ngOnInit",
          value: function ngOnInit() {}
        }, {
          key: "lineChartMethod",
          value: function lineChartMethod() {
            this.lineChart = new chart_js__WEBPACK_IMPORTED_MODULE_2__["Chart"](this.lineCanvas.nativeElement, {
              type: 'line',
              data: {
                labels: ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'November', 'December'],
                datasets: [{
                  label: '',
                  fill: true,
                  lineTension: 0.1,
                  backgroundColor: '#ffd1b3',
                  borderColor: '#F4B183',
                  borderCapStyle: 'butt',
                  borderDash: [],
                  borderDashOffset: 0.0,
                  borderJoinStyle: 'miter',
                  pointBorderColor: 'rgba(75,192,192,1)',
                  pointBackgroundColor: '#fff',
                  pointBorderWidth: 1,
                  pointHoverRadius: 5,
                  pointHoverBackgroundColor: 'rgba(75,192,192,1)',
                  pointHoverBorderColor: 'rgba(220,220,220,1)',
                  pointHoverBorderWidth: 2,
                  pointRadius: 1,
                  pointHitRadius: 10,
                  data: [65, 59, 80, 81, 56, 55, 40, 10, 5, 50, 10, 15],
                  spanGaps: false
                }]
              },
              options: {
                scales: {
                  xAxes: [{
                    display: false
                  }],
                  yAxes: [{
                    position: 'right'
                  }]
                },
                legend: {
                  display: false
                }
              }
            });
          }
        }]);

        return ChartTwoComponent;
      }();

      ChartTwoComponent.ctorParameters = function () {
        return [];
      };

      ChartTwoComponent.propDecorators = {
        lineCanvas: [{
          type: _angular_core__WEBPACK_IMPORTED_MODULE_1__["ViewChild"],
          args: ['lineCanvas']
        }]
      };
      ChartTwoComponent = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-chart-two',
        template: Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(
        /*! raw-loader!./chart-two.component.html */
        "./node_modules/raw-loader/dist/cjs.js!./src/app/components/coach/chart-two/chart-two.component.html"))["default"],
        styles: [Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(
        /*! ./chart-two.component.scss */
        "./src/app/components/coach/chart-two/chart-two.component.scss"))["default"]]
      })], ChartTwoComponent);
      /***/
    },

    /***/
    "./src/app/components/coach/coach-body-scan-step-one/coach-body-scan-step-one.component.scss":
    /*!***************************************************************************************************!*\
      !*** ./src/app/components/coach/coach-body-scan-step-one/coach-body-scan-step-one.component.scss ***!
      \***************************************************************************************************/

    /*! exports provided: default */

    /***/
    function srcAppComponentsCoachCoachBodyScanStepOneCoachBodyScanStepOneComponentScss(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony default export */


      __webpack_exports__["default"] = ".input-price {\n  display: flex;\n  align-items: center;\n  justify-content: center;\n  font-family: Oswald, sans-serif;\n}\n\n.display-price {\n  padding: 5px;\n  font-weight: bold;\n  font-size: 14px;\n  font-family: Oswald, sans-serif;\n}\n\nion-item ion-label {\n  white-space: normal;\n  font-family: Oswald, sans-serif;\n}\n\nion-item {\n  --inner-border-width: 0px 0px 0px 0px;\n}\n\n.sports-box ion-button {\n  height: 80px;\n  --ion-button-round-border-radius: 5px;\n  --ion-button-border-radius: 5px;\n  --ion-border-radius: 5px;\n  --border-radius: 5px;\n}\n\n.sports-box ion-button ion-icon {\n  font-size: 50px;\n}\n\n.gender ion-button {\n  white-space: normal;\n  font-size: 12px;\n  --padding-start: 0;\n  --padding-end: 0;\n  height: 50px;\n  font-family: Oswald, sans-serif;\n}\n\n.calendar-icon {\n  position: absolute;\n  right: 5px;\n  bottom: 4px;\n}\n\nion-item {\n  --inner-border-width: 0px 0px 0px 0px;\n  --min-height: 35px;\n  --padding-start: 0px;\n  --border-width: 0 0 0px 0;\n}\n\nion-list {\n  padding-top: 0px !important;\n  padding-bottom: 0px !important;\n}\n\nion-range {\n  --knob-background: var(--ion-color-primary);\n  --bar-height: 8px;\n  --knob-size: 30px;\n  --height: 20px;\n  --knob-border-radius: 22%;\n  --bar-background-active: var(--ion-color-primary);\n}\n\n.dateDeNa {\n  padding: 2px 0px;\n}\n\n.activitePhysiqueExterne {\n  padding: 25px 0px;\n}\n\nion-card-content {\n  -webkit-padding-end: 0px !important;\n          padding-inline-end: 0px !important;\n  font-family: Oswald, sans-serif;\n}\n\nion-segment-button {\n  --ripple-color: transparent;\n  background: transparent;\n  --color-checked: white;\n  --color: #9c9c9c;\n  --indicator-color: var(--ion-color-primary);\n  --margin-bottom: 0px;\n  height: 40px;\n  --border-radius: 25px;\n}\n\nion-segment {\n  background: rgba(170, 170, 170, 0.11);\n  border-radius: 25px;\n}\n\n.segment-box {\n  margin-bottom: 25px;\n}\n\n.remove {\n  color: #000;\n}\n\n.add {\n  color: #000;\n}\n\n.sports-box ion-button {\n  --background: rgb(0 0 0 / 50%);\n}\n\n.sports-box ion-button.active {\n  --background: var(--ion-color-primary);\n}\n\n.steper-heading {\n  font-family: Oswald;\n}\n\n.steper-title {\n  font-family: Oswald, sans-serif;\n}\n\n.numbered_data_section .text_desc {\n  text-align: left;\n}\n\n.text_desc ion-card {\n  margin: 0;\n  /* padding-left: 4%; */\n  /* padding-top: 3%; */\n  /* padding-bottom: 2%; */\n  padding: 4%;\n  height: 100%;\n  border-radius: 5px;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvY29tcG9uZW50cy9jb2FjaC9jb2FjaC1ib2R5LXNjYW4tc3RlcC1vbmUvY29hY2gtYm9keS1zY2FuLXN0ZXAtb25lLmNvbXBvbmVudC5zY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBO0VBQ0ksYUFBQTtFQUNBLG1CQUFBO0VBQ0EsdUJBQUE7RUFDQSwrQkFBQTtBQUNKOztBQUVBO0VBQ0ksWUFBQTtFQUNBLGlCQUFBO0VBQ0EsZUFBQTtFQUNBLCtCQUFBO0FBQ0o7O0FBRUE7RUFDSSxtQkFBQTtFQUNBLCtCQUFBO0FBQ0o7O0FBRUE7RUFDSSxxQ0FBQTtBQUNKOztBQUVBO0VBQ0ksWUFBQTtFQUNBLHFDQUFBO0VBQ0EsK0JBQUE7RUFDQSx3QkFBQTtFQUNBLG9CQUFBO0FBQ0o7O0FBRUE7RUFDSSxlQUFBO0FBQ0o7O0FBRUE7RUFDSSxtQkFBQTtFQUNBLGVBQUE7RUFDQSxrQkFBQTtFQUNBLGdCQUFBO0VBQ0EsWUFBQTtFQUNBLCtCQUFBO0FBQ0o7O0FBRUE7RUFDSSxrQkFBQTtFQUNBLFVBQUE7RUFDQSxXQUFBO0FBQ0o7O0FBRUE7RUFDSSxxQ0FBQTtFQUNBLGtCQUFBO0VBQ0Esb0JBQUE7RUFDQSx5QkFBQTtBQUNKOztBQUVBO0VBQ0ksMkJBQUE7RUFDQSw4QkFBQTtBQUNKOztBQUVBO0VBQ0ksMkNBQUE7RUFDQSxpQkFBQTtFQUNBLGlCQUFBO0VBQ0EsY0FBQTtFQUNBLHlCQUFBO0VBQ0EsaURBQUE7QUFDSjs7QUFFQTtFQUNJLGdCQUFBO0FBQ0o7O0FBRUE7RUFDSSxpQkFBQTtBQUNKOztBQUVBO0VBQ0ksbUNBQUE7VUFBQSxrQ0FBQTtFQUNBLCtCQUFBO0FBQ0o7O0FBRUE7RUFDSSwyQkFBQTtFQUNBLHVCQUFBO0VBQ0Esc0JBQUE7RUFDQSxnQkFBQTtFQUNBLDJDQUFBO0VBQ0Esb0JBQUE7RUFDQSxZQUFBO0VBQ0EscUJBQUE7QUFDSjs7QUFFQTtFQUNJLHFDQUFBO0VBQ0EsbUJBQUE7QUFDSjs7QUFFQTtFQUNJLG1CQUFBO0FBQ0o7O0FBRUE7RUFDSSxXQUFBO0FBQ0o7O0FBRUE7RUFDSSxXQUFBO0FBQ0o7O0FBRUE7RUFDSSw4QkFBQTtBQUNKOztBQUVBO0VBQ0ksc0NBQUE7QUFDSjs7QUFDQTtFQUNJLG1CQUFBO0FBRUo7O0FBQUE7RUFDSSwrQkFBQTtBQUdKOztBQUlJO0VBQ0ksZ0JBQUE7QUFEUjs7QUFNSTtFQUNJLFNBQUE7RUFDQSxzQkFBQTtFQUNBLHFCQUFBO0VBQ0Esd0JBQUE7RUFDQSxXQUFBO0VBQ0EsWUFBQTtFQUNBLGtCQUFBO0FBSFIiLCJmaWxlIjoic3JjL2FwcC9jb21wb25lbnRzL2NvYWNoL2NvYWNoLWJvZHktc2Nhbi1zdGVwLW9uZS9jb2FjaC1ib2R5LXNjYW4tc3RlcC1vbmUuY29tcG9uZW50LnNjc3MiLCJzb3VyY2VzQ29udGVudCI6WyIuaW5wdXQtcHJpY2Uge1xuICAgIGRpc3BsYXk6IGZsZXg7XG4gICAgYWxpZ24taXRlbXM6IGNlbnRlcjtcbiAgICBqdXN0aWZ5LWNvbnRlbnQ6IGNlbnRlcjtcbiAgICBmb250LWZhbWlseTogT3N3YWxkLCBzYW5zLXNlcmlmO1xufVxuXG4uZGlzcGxheS1wcmljZSB7XG4gICAgcGFkZGluZzogNXB4O1xuICAgIGZvbnQtd2VpZ2h0OiBib2xkO1xuICAgIGZvbnQtc2l6ZTogMTRweDtcbiAgICBmb250LWZhbWlseTogT3N3YWxkLCBzYW5zLXNlcmlmO1xufVxuXG5pb24taXRlbSBpb24tbGFiZWwge1xuICAgIHdoaXRlLXNwYWNlOiBub3JtYWw7XG4gICAgZm9udC1mYW1pbHk6IE9zd2FsZCwgc2Fucy1zZXJpZjtcbn1cblxuaW9uLWl0ZW0ge1xuICAgIC0taW5uZXItYm9yZGVyLXdpZHRoOiAwcHggMHB4IDBweCAwcHg7XG59XG5cbi5zcG9ydHMtYm94IGlvbi1idXR0b24ge1xuICAgIGhlaWdodDogODBweDtcbiAgICAtLWlvbi1idXR0b24tcm91bmQtYm9yZGVyLXJhZGl1czogNXB4O1xuICAgIC0taW9uLWJ1dHRvbi1ib3JkZXItcmFkaXVzOiA1cHg7XG4gICAgLS1pb24tYm9yZGVyLXJhZGl1czogNXB4O1xuICAgIC0tYm9yZGVyLXJhZGl1czogNXB4O1xufVxuXG4uc3BvcnRzLWJveCBpb24tYnV0dG9uIGlvbi1pY29uIHtcbiAgICBmb250LXNpemU6IDUwcHg7XG59XG5cbi5nZW5kZXIgaW9uLWJ1dHRvbiB7XG4gICAgd2hpdGUtc3BhY2U6IG5vcm1hbDtcbiAgICBmb250LXNpemU6IDEycHg7XG4gICAgLS1wYWRkaW5nLXN0YXJ0OiAwO1xuICAgIC0tcGFkZGluZy1lbmQ6IDA7XG4gICAgaGVpZ2h0OiA1MHB4O1xuICAgIGZvbnQtZmFtaWx5OiBPc3dhbGQsIHNhbnMtc2VyaWY7XG59XG5cbi5jYWxlbmRhci1pY29uIHtcbiAgICBwb3NpdGlvbjogYWJzb2x1dGU7XG4gICAgcmlnaHQ6IDVweDtcbiAgICBib3R0b206IDRweDtcbn1cblxuaW9uLWl0ZW0ge1xuICAgIC0taW5uZXItYm9yZGVyLXdpZHRoOiAwcHggMHB4IDBweCAwcHg7XG4gICAgLS1taW4taGVpZ2h0OiAzNXB4O1xuICAgIC0tcGFkZGluZy1zdGFydDogMHB4O1xuICAgIC0tYm9yZGVyLXdpZHRoOiAwIDAgMHB4IDA7XG59XG5cbmlvbi1saXN0IHtcbiAgICBwYWRkaW5nLXRvcDogMHB4ICFpbXBvcnRhbnQ7XG4gICAgcGFkZGluZy1ib3R0b206IDBweCAhaW1wb3J0YW50O1xufVxuXG5pb24tcmFuZ2Uge1xuICAgIC0ta25vYi1iYWNrZ3JvdW5kOiB2YXIoLS1pb24tY29sb3ItcHJpbWFyeSk7XG4gICAgLS1iYXItaGVpZ2h0OiA4cHg7XG4gICAgLS1rbm9iLXNpemU6IDMwcHg7XG4gICAgLS1oZWlnaHQ6IDIwcHg7XG4gICAgLS1rbm9iLWJvcmRlci1yYWRpdXM6IDIyJTtcbiAgICAtLWJhci1iYWNrZ3JvdW5kLWFjdGl2ZTogdmFyKC0taW9uLWNvbG9yLXByaW1hcnkpO1xufVxuXG4uZGF0ZURlTmEge1xuICAgIHBhZGRpbmc6IDJweCAwcHg7XG59XG5cbi5hY3Rpdml0ZVBoeXNpcXVlRXh0ZXJuZSB7XG4gICAgcGFkZGluZzogMjVweCAwcHg7XG59XG5cbmlvbi1jYXJkLWNvbnRlbnQge1xuICAgIHBhZGRpbmctaW5saW5lLWVuZDogMHB4ICFpbXBvcnRhbnQ7XG4gICAgZm9udC1mYW1pbHk6IE9zd2FsZCwgc2Fucy1zZXJpZjtcbn1cblxuaW9uLXNlZ21lbnQtYnV0dG9uIHtcbiAgICAtLXJpcHBsZS1jb2xvcjogdHJhbnNwYXJlbnQ7XG4gICAgYmFja2dyb3VuZDogdHJhbnNwYXJlbnQ7XG4gICAgLS1jb2xvci1jaGVja2VkOiB3aGl0ZTtcbiAgICAtLWNvbG9yOiAjOWM5YzljO1xuICAgIC0taW5kaWNhdG9yLWNvbG9yOiB2YXIoLS1pb24tY29sb3ItcHJpbWFyeSk7XG4gICAgLS1tYXJnaW4tYm90dG9tOiAwcHg7XG4gICAgaGVpZ2h0OiA0MHB4O1xuICAgIC0tYm9yZGVyLXJhZGl1czogMjVweDtcbn1cblxuaW9uLXNlZ21lbnQge1xuICAgIGJhY2tncm91bmQ6IHJnYigxNzAgMTcwIDE3MCAvIDExJSk7XG4gICAgYm9yZGVyLXJhZGl1czogMjVweDtcbn1cblxuLnNlZ21lbnQtYm94IHtcbiAgICBtYXJnaW4tYm90dG9tOiAyNXB4O1xufVxuXG4ucmVtb3ZlIHtcbiAgICBjb2xvcjogIzAwMDtcbn1cblxuLmFkZCB7XG4gICAgY29sb3I6ICMwMDA7XG59XG5cbi5zcG9ydHMtYm94IGlvbi1idXR0b24ge1xuICAgIC0tYmFja2dyb3VuZDogcmdiKDAgMCAwIC8gNTAlKTtcbn1cblxuLnNwb3J0cy1ib3ggaW9uLWJ1dHRvbi5hY3RpdmUge1xuICAgIC0tYmFja2dyb3VuZDogdmFyKC0taW9uLWNvbG9yLXByaW1hcnkpO1xufVxuLnN0ZXBlci1oZWFkaW5nIHtcbiAgICBmb250LWZhbWlseTogT3N3YWxkO1xufVxuLnN0ZXBlci10aXRsZSB7XG4gICAgZm9udC1mYW1pbHk6IE9zd2FsZCwgc2Fucy1zZXJpZjtcbn1cblxuXG5cblxuLm51bWJlcmVkX2RhdGFfc2VjdGlvbiB7XG4gICAgLnRleHRfZGVzYyB7XG4gICAgICAgIHRleHQtYWxpZ246IGxlZnQ7XG4gICAgfVxufVxuXG4udGV4dF9kZXNjIHtcbiAgICBpb24tY2FyZCB7XG4gICAgICAgIG1hcmdpbjogMDtcbiAgICAgICAgLyogcGFkZGluZy1sZWZ0OiA0JTsgKi9cbiAgICAgICAgLyogcGFkZGluZy10b3A6IDMlOyAqL1xuICAgICAgICAvKiBwYWRkaW5nLWJvdHRvbTogMiU7ICovXG4gICAgICAgIHBhZGRpbmc6IDQlO1xuICAgICAgICBoZWlnaHQ6IDEwMCU7XG4gICAgICAgIGJvcmRlci1yYWRpdXM6IDVweDtcbiAgICB9XG59Il19 */";
      /***/
    },

    /***/
    "./src/app/components/coach/coach-body-scan-step-one/coach-body-scan-step-one.component.ts":
    /*!*************************************************************************************************!*\
      !*** ./src/app/components/coach/coach-body-scan-step-one/coach-body-scan-step-one.component.ts ***!
      \*************************************************************************************************/

    /*! exports provided: CoachBodyScanStepOneComponent */

    /***/
    function srcAppComponentsCoachCoachBodyScanStepOneCoachBodyScanStepOneComponentTs(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "CoachBodyScanStepOneComponent", function () {
        return CoachBodyScanStepOneComponent;
      });
      /* harmony import */


      var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! tslib */
      "./node_modules/tslib/tslib.es6.js");
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! @angular/core */
      "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");

      var CoachBodyScanStepOneComponent = /*#__PURE__*/function () {
        function CoachBodyScanStepOneComponent() {
          _classCallCheck(this, CoachBodyScanStepOneComponent);

          this.gender = "male";
        }

        _createClass(CoachBodyScanStepOneComponent, [{
          key: "ngOnInit",
          value: function ngOnInit() {}
        }]);

        return CoachBodyScanStepOneComponent;
      }();

      CoachBodyScanStepOneComponent.ctorParameters = function () {
        return [];
      };

      CoachBodyScanStepOneComponent = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-coach-body-scan-step-one',
        template: Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(
        /*! raw-loader!./coach-body-scan-step-one.component.html */
        "./node_modules/raw-loader/dist/cjs.js!./src/app/components/coach/coach-body-scan-step-one/coach-body-scan-step-one.component.html"))["default"],
        styles: [Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(
        /*! ./coach-body-scan-step-one.component.scss */
        "./src/app/components/coach/coach-body-scan-step-one/coach-body-scan-step-one.component.scss"))["default"]]
      })], CoachBodyScanStepOneComponent);
      /***/
    },

    /***/
    "./src/app/components/coach/coach-body-scan-step-three/coach-body-scan-step-three.component.scss":
    /*!*******************************************************************************************************!*\
      !*** ./src/app/components/coach/coach-body-scan-step-three/coach-body-scan-step-three.component.scss ***!
      \*******************************************************************************************************/

    /*! exports provided: default */

    /***/
    function srcAppComponentsCoachCoachBodyScanStepThreeCoachBodyScanStepThreeComponentScss(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony default export */


      __webpack_exports__["default"] = ".input-price {\n  display: flex;\n  align-items: center;\n  justify-content: center;\n  font-family: Oswald, sans-serif;\n}\n\n.display-price {\n  padding: 5px;\n  font-weight: bold;\n  font-size: 14px;\n  font-family: Oswald, sans-serif;\n}\n\nion-item ion-label {\n  white-space: normal;\n  font-family: Oswald, sans-serif;\n}\n\nion-item {\n  --inner-border-width: 0px 0px 0px 0px;\n}\n\n.sports-box ion-button {\n  height: 80px;\n  --ion-button-round-border-radius: 5px;\n  --ion-button-border-radius: 5px;\n  --ion-border-radius: 5px;\n  --border-radius: 5px;\n}\n\n.sports-box ion-button ion-icon {\n  font-size: 50px;\n}\n\n.gender ion-button {\n  white-space: normal;\n  font-size: 12px;\n  --padding-start: 0;\n  --padding-end: 0;\n  height: 50px;\n  font-family: Oswald, sans-serif;\n}\n\n.calendar-icon {\n  position: absolute;\n  right: 5px;\n  bottom: 4px;\n}\n\nion-item {\n  --inner-border-width: 0px 0px 0px 0px;\n  --min-height: 35px;\n  --padding-start: 0px;\n  --border-width: 0 0 0px 0;\n}\n\nion-list {\n  padding-top: 0px !important;\n  padding-bottom: 0px !important;\n}\n\nion-range {\n  --knob-background: var(--ion-color-primary);\n  --bar-height: 8px;\n  --knob-size: 30px;\n  --height: 20px;\n  --knob-border-radius: 22%;\n  --bar-background-active: var(--ion-color-primary);\n}\n\n.dateDeNa {\n  padding: 2px 0px;\n}\n\n.activitePhysiqueExterne {\n  padding: 25px 0px;\n}\n\nion-card-content {\n  -webkit-padding-end: 0px !important;\n          padding-inline-end: 0px !important;\n  font-family: Oswald, sans-serif;\n}\n\nion-segment-button {\n  --ripple-color: transparent;\n  background: transparent;\n  --color-checked: white;\n  --color: #9c9c9c;\n  --indicator-color: var(--ion-color-primary);\n  --margin-bottom: 0px;\n  height: 40px;\n  --border-radius: 25px;\n}\n\nion-segment {\n  background: rgba(170, 170, 170, 0.11);\n  border-radius: 25px;\n}\n\n.segment-box {\n  margin-bottom: 25px;\n}\n\n.remove {\n  color: #000;\n}\n\n.add {\n  color: #000;\n}\n\n.sports-box ion-button {\n  --background: rgb(0 0 0 / 50%);\n}\n\n.sports-box ion-button.active {\n  --background: var(--ion-color-primary);\n}\n\n.steper-heading {\n  font-family: Oswald;\n}\n\n.steper-title {\n  font-family: Oswald, sans-serif;\n}\n\n.numbered_data_section .text_desc {\n  text-align: left;\n}\n\n.text_desc ion-card {\n  margin: 0;\n  /* padding-left: 4%; */\n  /* padding-top: 3%; */\n  /* padding-bottom: 2%; */\n  padding: 4%;\n  border-radius: 5px;\n}\n\n.card_item_section ion-col ion-card {\n  height: 85px;\n  margin: 0;\n  display: flex;\n  justify-content: center;\n}\n\n.card_item_section ion-col ion-card ion-card-content {\n  display: flex;\n  justify-content: center;\n  flex-direction: column;\n}\n\n.card_item_section ion-col ion-card ion-card-content ion-img {\n  width: 25px;\n  border-radius: 50%;\n  align-self: center;\n  margin-top: 5px;\n}\n\n.card_item_section ion-col ion-card ion-card-content p {\n  align-self: center;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvY29tcG9uZW50cy9jb2FjaC9jb2FjaC1ib2R5LXNjYW4tc3RlcC10aHJlZS9jb2FjaC1ib2R5LXNjYW4tc3RlcC10aHJlZS5jb21wb25lbnQuc2NzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQTtFQUNJLGFBQUE7RUFDQSxtQkFBQTtFQUNBLHVCQUFBO0VBQ0EsK0JBQUE7QUFDSjs7QUFFQTtFQUNJLFlBQUE7RUFDQSxpQkFBQTtFQUNBLGVBQUE7RUFDQSwrQkFBQTtBQUNKOztBQUVBO0VBQ0ksbUJBQUE7RUFDQSwrQkFBQTtBQUNKOztBQUVBO0VBQ0kscUNBQUE7QUFDSjs7QUFFQTtFQUNJLFlBQUE7RUFDQSxxQ0FBQTtFQUNBLCtCQUFBO0VBQ0Esd0JBQUE7RUFDQSxvQkFBQTtBQUNKOztBQUVBO0VBQ0ksZUFBQTtBQUNKOztBQUVBO0VBQ0ksbUJBQUE7RUFDQSxlQUFBO0VBQ0Esa0JBQUE7RUFDQSxnQkFBQTtFQUNBLFlBQUE7RUFDQSwrQkFBQTtBQUNKOztBQUVBO0VBQ0ksa0JBQUE7RUFDQSxVQUFBO0VBQ0EsV0FBQTtBQUNKOztBQUVBO0VBQ0kscUNBQUE7RUFDQSxrQkFBQTtFQUNBLG9CQUFBO0VBQ0EseUJBQUE7QUFDSjs7QUFFQTtFQUNJLDJCQUFBO0VBQ0EsOEJBQUE7QUFDSjs7QUFFQTtFQUNJLDJDQUFBO0VBQ0EsaUJBQUE7RUFDQSxpQkFBQTtFQUNBLGNBQUE7RUFDQSx5QkFBQTtFQUNBLGlEQUFBO0FBQ0o7O0FBRUE7RUFDSSxnQkFBQTtBQUNKOztBQUVBO0VBQ0ksaUJBQUE7QUFDSjs7QUFFQTtFQUNJLG1DQUFBO1VBQUEsa0NBQUE7RUFDQSwrQkFBQTtBQUNKOztBQUVBO0VBQ0ksMkJBQUE7RUFDQSx1QkFBQTtFQUNBLHNCQUFBO0VBQ0EsZ0JBQUE7RUFDQSwyQ0FBQTtFQUNBLG9CQUFBO0VBQ0EsWUFBQTtFQUNBLHFCQUFBO0FBQ0o7O0FBRUE7RUFDSSxxQ0FBQTtFQUNBLG1CQUFBO0FBQ0o7O0FBRUE7RUFDSSxtQkFBQTtBQUNKOztBQUVBO0VBQ0ksV0FBQTtBQUNKOztBQUVBO0VBQ0ksV0FBQTtBQUNKOztBQUVBO0VBQ0ksOEJBQUE7QUFDSjs7QUFFQTtFQUNJLHNDQUFBO0FBQ0o7O0FBQ0E7RUFDSSxtQkFBQTtBQUVKOztBQUFBO0VBQ0ksK0JBQUE7QUFHSjs7QUFJSTtFQUNJLGdCQUFBO0FBRFI7O0FBTUk7RUFDSSxTQUFBO0VBQ0Esc0JBQUE7RUFDQSxxQkFBQTtFQUNBLHdCQUFBO0VBQ0EsV0FBQTtFQUNBLGtCQUFBO0FBSFI7O0FBY1E7RUFDSSxZQUFBO0VBQ0EsU0FBQTtFQUNBLGFBQUE7RUFDQSx1QkFBQTtBQVhaOztBQWVZO0VBQ0ksYUFBQTtFQUNBLHVCQUFBO0VBQ0Esc0JBQUE7QUFiaEI7O0FBY2dCO0VBQ0ksV0FBQTtFQUNBLGtCQUFBO0VBQ0Esa0JBQUE7RUFDQSxlQUFBO0FBWnBCOztBQWVnQjtFQUNJLGtCQUFBO0FBYnBCIiwiZmlsZSI6InNyYy9hcHAvY29tcG9uZW50cy9jb2FjaC9jb2FjaC1ib2R5LXNjYW4tc3RlcC10aHJlZS9jb2FjaC1ib2R5LXNjYW4tc3RlcC10aHJlZS5jb21wb25lbnQuc2NzcyIsInNvdXJjZXNDb250ZW50IjpbIi5pbnB1dC1wcmljZSB7XG4gICAgZGlzcGxheTogZmxleDtcbiAgICBhbGlnbi1pdGVtczogY2VudGVyO1xuICAgIGp1c3RpZnktY29udGVudDogY2VudGVyO1xuICAgIGZvbnQtZmFtaWx5OiBPc3dhbGQsIHNhbnMtc2VyaWY7XG59XG5cbi5kaXNwbGF5LXByaWNlIHtcbiAgICBwYWRkaW5nOiA1cHg7XG4gICAgZm9udC13ZWlnaHQ6IGJvbGQ7XG4gICAgZm9udC1zaXplOiAxNHB4O1xuICAgIGZvbnQtZmFtaWx5OiBPc3dhbGQsIHNhbnMtc2VyaWY7XG59XG5cbmlvbi1pdGVtIGlvbi1sYWJlbCB7XG4gICAgd2hpdGUtc3BhY2U6IG5vcm1hbDtcbiAgICBmb250LWZhbWlseTogT3N3YWxkLCBzYW5zLXNlcmlmO1xufVxuXG5pb24taXRlbSB7XG4gICAgLS1pbm5lci1ib3JkZXItd2lkdGg6IDBweCAwcHggMHB4IDBweDtcbn1cblxuLnNwb3J0cy1ib3ggaW9uLWJ1dHRvbiB7XG4gICAgaGVpZ2h0OiA4MHB4O1xuICAgIC0taW9uLWJ1dHRvbi1yb3VuZC1ib3JkZXItcmFkaXVzOiA1cHg7XG4gICAgLS1pb24tYnV0dG9uLWJvcmRlci1yYWRpdXM6IDVweDtcbiAgICAtLWlvbi1ib3JkZXItcmFkaXVzOiA1cHg7XG4gICAgLS1ib3JkZXItcmFkaXVzOiA1cHg7XG59XG5cbi5zcG9ydHMtYm94IGlvbi1idXR0b24gaW9uLWljb24ge1xuICAgIGZvbnQtc2l6ZTogNTBweDtcbn1cblxuLmdlbmRlciBpb24tYnV0dG9uIHtcbiAgICB3aGl0ZS1zcGFjZTogbm9ybWFsO1xuICAgIGZvbnQtc2l6ZTogMTJweDtcbiAgICAtLXBhZGRpbmctc3RhcnQ6IDA7XG4gICAgLS1wYWRkaW5nLWVuZDogMDtcbiAgICBoZWlnaHQ6IDUwcHg7XG4gICAgZm9udC1mYW1pbHk6IE9zd2FsZCwgc2Fucy1zZXJpZjtcbn1cblxuLmNhbGVuZGFyLWljb24ge1xuICAgIHBvc2l0aW9uOiBhYnNvbHV0ZTtcbiAgICByaWdodDogNXB4O1xuICAgIGJvdHRvbTogNHB4O1xufVxuXG5pb24taXRlbSB7XG4gICAgLS1pbm5lci1ib3JkZXItd2lkdGg6IDBweCAwcHggMHB4IDBweDtcbiAgICAtLW1pbi1oZWlnaHQ6IDM1cHg7XG4gICAgLS1wYWRkaW5nLXN0YXJ0OiAwcHg7XG4gICAgLS1ib3JkZXItd2lkdGg6IDAgMCAwcHggMDtcbn1cblxuaW9uLWxpc3Qge1xuICAgIHBhZGRpbmctdG9wOiAwcHggIWltcG9ydGFudDtcbiAgICBwYWRkaW5nLWJvdHRvbTogMHB4ICFpbXBvcnRhbnQ7XG59XG5cbmlvbi1yYW5nZSB7XG4gICAgLS1rbm9iLWJhY2tncm91bmQ6IHZhcigtLWlvbi1jb2xvci1wcmltYXJ5KTtcbiAgICAtLWJhci1oZWlnaHQ6IDhweDtcbiAgICAtLWtub2Itc2l6ZTogMzBweDtcbiAgICAtLWhlaWdodDogMjBweDtcbiAgICAtLWtub2ItYm9yZGVyLXJhZGl1czogMjIlO1xuICAgIC0tYmFyLWJhY2tncm91bmQtYWN0aXZlOiB2YXIoLS1pb24tY29sb3ItcHJpbWFyeSk7XG59XG5cbi5kYXRlRGVOYSB7XG4gICAgcGFkZGluZzogMnB4IDBweDtcbn1cblxuLmFjdGl2aXRlUGh5c2lxdWVFeHRlcm5lIHtcbiAgICBwYWRkaW5nOiAyNXB4IDBweDtcbn1cblxuaW9uLWNhcmQtY29udGVudCB7XG4gICAgcGFkZGluZy1pbmxpbmUtZW5kOiAwcHggIWltcG9ydGFudDtcbiAgICBmb250LWZhbWlseTogT3N3YWxkLCBzYW5zLXNlcmlmO1xufVxuXG5pb24tc2VnbWVudC1idXR0b24ge1xuICAgIC0tcmlwcGxlLWNvbG9yOiB0cmFuc3BhcmVudDtcbiAgICBiYWNrZ3JvdW5kOiB0cmFuc3BhcmVudDtcbiAgICAtLWNvbG9yLWNoZWNrZWQ6IHdoaXRlO1xuICAgIC0tY29sb3I6ICM5YzljOWM7XG4gICAgLS1pbmRpY2F0b3ItY29sb3I6IHZhcigtLWlvbi1jb2xvci1wcmltYXJ5KTtcbiAgICAtLW1hcmdpbi1ib3R0b206IDBweDtcbiAgICBoZWlnaHQ6IDQwcHg7XG4gICAgLS1ib3JkZXItcmFkaXVzOiAyNXB4O1xufVxuXG5pb24tc2VnbWVudCB7XG4gICAgYmFja2dyb3VuZDogcmdiKDE3MCAxNzAgMTcwIC8gMTElKTtcbiAgICBib3JkZXItcmFkaXVzOiAyNXB4O1xufVxuXG4uc2VnbWVudC1ib3gge1xuICAgIG1hcmdpbi1ib3R0b206IDI1cHg7XG59XG5cbi5yZW1vdmUge1xuICAgIGNvbG9yOiAjMDAwO1xufVxuXG4uYWRkIHtcbiAgICBjb2xvcjogIzAwMDtcbn1cblxuLnNwb3J0cy1ib3ggaW9uLWJ1dHRvbiB7XG4gICAgLS1iYWNrZ3JvdW5kOiByZ2IoMCAwIDAgLyA1MCUpO1xufVxuXG4uc3BvcnRzLWJveCBpb24tYnV0dG9uLmFjdGl2ZSB7XG4gICAgLS1iYWNrZ3JvdW5kOiB2YXIoLS1pb24tY29sb3ItcHJpbWFyeSk7XG59XG4uc3RlcGVyLWhlYWRpbmcge1xuICAgIGZvbnQtZmFtaWx5OiBPc3dhbGQ7XG59XG4uc3RlcGVyLXRpdGxlIHtcbiAgICBmb250LWZhbWlseTogT3N3YWxkLCBzYW5zLXNlcmlmO1xufVxuXG5cblxuXG4ubnVtYmVyZWRfZGF0YV9zZWN0aW9uIHtcbiAgICAudGV4dF9kZXNjIHtcbiAgICAgICAgdGV4dC1hbGlnbjogbGVmdDtcbiAgICB9XG59XG5cbi50ZXh0X2Rlc2Mge1xuICAgIGlvbi1jYXJkIHtcbiAgICAgICAgbWFyZ2luOiAwO1xuICAgICAgICAvKiBwYWRkaW5nLWxlZnQ6IDQlOyAqL1xuICAgICAgICAvKiBwYWRkaW5nLXRvcDogMyU7ICovXG4gICAgICAgIC8qIHBhZGRpbmctYm90dG9tOiAyJTsgKi9cbiAgICAgICAgcGFkZGluZzogNCU7XG4gICAgICAgIGJvcmRlci1yYWRpdXM6IDVweDtcbiAgICB9XG59XG5cblxuXG5cblxuXG4uY2FyZF9pdGVtX3NlY3Rpb24ge1xuICAgIGlvbi1jb2wge1xuICAgICAgICBpb24tY2FyZCB7XG4gICAgICAgICAgICBoZWlnaHQ6IDg1cHg7XG4gICAgICAgICAgICBtYXJnaW46IDA7XG4gICAgICAgICAgICBkaXNwbGF5OiBmbGV4O1xuICAgICAgICAgICAganVzdGlmeS1jb250ZW50OiBjZW50ZXI7XG5cblxuXG4gICAgICAgICAgICBpb24tY2FyZC1jb250ZW50IHtcbiAgICAgICAgICAgICAgICBkaXNwbGF5OiBmbGV4O1xuICAgICAgICAgICAgICAgIGp1c3RpZnktY29udGVudDogY2VudGVyO1xuICAgICAgICAgICAgICAgIGZsZXgtZGlyZWN0aW9uOiBjb2x1bW47XG4gICAgICAgICAgICAgICAgaW9uLWltZyB7XG4gICAgICAgICAgICAgICAgICAgIHdpZHRoOiAyNXB4O1xuICAgICAgICAgICAgICAgICAgICBib3JkZXItcmFkaXVzOiA1MCU7XG4gICAgICAgICAgICAgICAgICAgIGFsaWduLXNlbGY6IGNlbnRlcjtcbiAgICAgICAgICAgICAgICAgICAgbWFyZ2luLXRvcDogNXB4O1xuICAgICAgICAgICAgICAgIH1cblxuICAgICAgICAgICAgICAgIHAge1xuICAgICAgICAgICAgICAgICAgICBhbGlnbi1zZWxmOiBjZW50ZXI7XG5cbiAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICB9XG4gICAgICAgIH1cbiAgICB9XG59XG4iXX0= */";
      /***/
    },

    /***/
    "./src/app/components/coach/coach-body-scan-step-three/coach-body-scan-step-three.component.ts":
    /*!*****************************************************************************************************!*\
      !*** ./src/app/components/coach/coach-body-scan-step-three/coach-body-scan-step-three.component.ts ***!
      \*****************************************************************************************************/

    /*! exports provided: CoachBodyScanStepThreeComponent */

    /***/
    function srcAppComponentsCoachCoachBodyScanStepThreeCoachBodyScanStepThreeComponentTs(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "CoachBodyScanStepThreeComponent", function () {
        return CoachBodyScanStepThreeComponent;
      });
      /* harmony import */


      var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! tslib */
      "./node_modules/tslib/tslib.es6.js");
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! @angular/core */
      "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");

      var CoachBodyScanStepThreeComponent = /*#__PURE__*/function () {
        function CoachBodyScanStepThreeComponent() {
          _classCallCheck(this, CoachBodyScanStepThreeComponent);
        }

        _createClass(CoachBodyScanStepThreeComponent, [{
          key: "ngOnInit",
          value: function ngOnInit() {}
        }]);

        return CoachBodyScanStepThreeComponent;
      }();

      CoachBodyScanStepThreeComponent.ctorParameters = function () {
        return [];
      };

      CoachBodyScanStepThreeComponent = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-coach-body-scan-step-three',
        template: Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(
        /*! raw-loader!./coach-body-scan-step-three.component.html */
        "./node_modules/raw-loader/dist/cjs.js!./src/app/components/coach/coach-body-scan-step-three/coach-body-scan-step-three.component.html"))["default"],
        styles: [Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(
        /*! ./coach-body-scan-step-three.component.scss */
        "./src/app/components/coach/coach-body-scan-step-three/coach-body-scan-step-three.component.scss"))["default"]]
      })], CoachBodyScanStepThreeComponent);
      /***/
    },

    /***/
    "./src/app/components/coach/coach-body-scan-step-two/coach-body-scan-step-two.component.scss":
    /*!***************************************************************************************************!*\
      !*** ./src/app/components/coach/coach-body-scan-step-two/coach-body-scan-step-two.component.scss ***!
      \***************************************************************************************************/

    /*! exports provided: default */

    /***/
    function srcAppComponentsCoachCoachBodyScanStepTwoCoachBodyScanStepTwoComponentScss(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony default export */


      __webpack_exports__["default"] = ".input-price {\n  display: flex;\n  align-items: center;\n  justify-content: center;\n  font-family: Oswald, sans-serif;\n}\n\n.display-price {\n  padding: 5px;\n  font-weight: bold;\n  font-size: 14px;\n  font-family: Oswald, sans-serif;\n}\n\nion-item ion-label {\n  white-space: normal;\n  font-family: Oswald, sans-serif;\n}\n\nion-item {\n  --inner-border-width: 0px 0px 0px 0px;\n}\n\n.sports-box ion-button {\n  height: 80px;\n  --ion-button-round-border-radius: 5px;\n  --ion-button-border-radius: 5px;\n  --ion-border-radius: 5px;\n  --border-radius: 5px;\n}\n\n.sports-box ion-button ion-icon {\n  font-size: 50px;\n}\n\n.gender ion-button {\n  white-space: normal;\n  font-size: 12px;\n  --padding-start: 0;\n  --padding-end: 0;\n  height: 50px;\n  font-family: Oswald, sans-serif;\n}\n\n.calendar-icon {\n  position: absolute;\n  right: 5px;\n  bottom: 4px;\n}\n\nion-item {\n  --inner-border-width: 0px 0px 0px 0px;\n  --min-height: 35px;\n  --padding-start: 0px;\n  --border-width: 0 0 0px 0;\n}\n\nion-list {\n  padding-top: 0px !important;\n  padding-bottom: 0px !important;\n}\n\nion-range {\n  --knob-background: var(--ion-color-primary);\n  --bar-height: 8px;\n  --knob-size: 30px;\n  --height: 20px;\n  --knob-border-radius: 22%;\n  --bar-background-active: var(--ion-color-primary);\n}\n\n.dateDeNa {\n  padding: 2px 0px;\n}\n\n.activitePhysiqueExterne {\n  padding: 25px 0px;\n}\n\nion-card-content {\n  -webkit-padding-end: 0px !important;\n          padding-inline-end: 0px !important;\n  font-family: Oswald, sans-serif;\n}\n\nion-segment-button {\n  --ripple-color: transparent;\n  background: transparent;\n  --color-checked: white;\n  --color: #9c9c9c;\n  --indicator-color: var(--ion-color-primary);\n  --margin-bottom: 0px;\n  height: 40px;\n  --border-radius: 25px;\n}\n\nion-segment {\n  background: rgba(170, 170, 170, 0.11);\n  border-radius: 25px;\n}\n\n.segment-box {\n  margin-bottom: 25px;\n}\n\n.remove {\n  color: #000;\n}\n\n.add {\n  color: #000;\n}\n\n.sports-box ion-button {\n  --background: rgb(0 0 0 / 50%);\n}\n\n.sports-box ion-button.active {\n  --background: var(--ion-color-primary);\n}\n\n.steper-heading {\n  font-family: Oswald;\n}\n\n.steper-title {\n  font-family: Oswald, sans-serif;\n}\n\n.numbered_data_section .text_desc {\n  text-align: left;\n}\n\n.text_desc ion-card {\n  margin: 0;\n  /* padding-left: 4%; */\n  /* padding-top: 3%; */\n  /* padding-bottom: 2%; */\n  padding: 4%;\n  height: 100%;\n  border-radius: 5px;\n}\n\n.excercise_input_section {\n  width: 100%;\n  margin-bottom: 2%;\n}\n\n.excercise_input_section ion-item {\n  width: 100%;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvY29tcG9uZW50cy9jb2FjaC9jb2FjaC1ib2R5LXNjYW4tc3RlcC10d28vY29hY2gtYm9keS1zY2FuLXN0ZXAtdHdvLmNvbXBvbmVudC5zY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBO0VBQ0ksYUFBQTtFQUNBLG1CQUFBO0VBQ0EsdUJBQUE7RUFDQSwrQkFBQTtBQUNKOztBQUVBO0VBQ0ksWUFBQTtFQUNBLGlCQUFBO0VBQ0EsZUFBQTtFQUNBLCtCQUFBO0FBQ0o7O0FBRUE7RUFDSSxtQkFBQTtFQUNBLCtCQUFBO0FBQ0o7O0FBRUE7RUFDSSxxQ0FBQTtBQUNKOztBQUVBO0VBQ0ksWUFBQTtFQUNBLHFDQUFBO0VBQ0EsK0JBQUE7RUFDQSx3QkFBQTtFQUNBLG9CQUFBO0FBQ0o7O0FBRUE7RUFDSSxlQUFBO0FBQ0o7O0FBRUE7RUFDSSxtQkFBQTtFQUNBLGVBQUE7RUFDQSxrQkFBQTtFQUNBLGdCQUFBO0VBQ0EsWUFBQTtFQUNBLCtCQUFBO0FBQ0o7O0FBRUE7RUFDSSxrQkFBQTtFQUNBLFVBQUE7RUFDQSxXQUFBO0FBQ0o7O0FBRUE7RUFDSSxxQ0FBQTtFQUNBLGtCQUFBO0VBQ0Esb0JBQUE7RUFDQSx5QkFBQTtBQUNKOztBQUVBO0VBQ0ksMkJBQUE7RUFDQSw4QkFBQTtBQUNKOztBQUVBO0VBQ0ksMkNBQUE7RUFDQSxpQkFBQTtFQUNBLGlCQUFBO0VBQ0EsY0FBQTtFQUNBLHlCQUFBO0VBQ0EsaURBQUE7QUFDSjs7QUFFQTtFQUNJLGdCQUFBO0FBQ0o7O0FBRUE7RUFDSSxpQkFBQTtBQUNKOztBQUVBO0VBQ0ksbUNBQUE7VUFBQSxrQ0FBQTtFQUNBLCtCQUFBO0FBQ0o7O0FBRUE7RUFDSSwyQkFBQTtFQUNBLHVCQUFBO0VBQ0Esc0JBQUE7RUFDQSxnQkFBQTtFQUNBLDJDQUFBO0VBQ0Esb0JBQUE7RUFDQSxZQUFBO0VBQ0EscUJBQUE7QUFDSjs7QUFFQTtFQUNJLHFDQUFBO0VBQ0EsbUJBQUE7QUFDSjs7QUFFQTtFQUNJLG1CQUFBO0FBQ0o7O0FBRUE7RUFDSSxXQUFBO0FBQ0o7O0FBRUE7RUFDSSxXQUFBO0FBQ0o7O0FBRUE7RUFDSSw4QkFBQTtBQUNKOztBQUVBO0VBQ0ksc0NBQUE7QUFDSjs7QUFDQTtFQUNJLG1CQUFBO0FBRUo7O0FBQUE7RUFDSSwrQkFBQTtBQUdKOztBQUlJO0VBQ0ksZ0JBQUE7QUFEUjs7QUFNSTtFQUNJLFNBQUE7RUFDQSxzQkFBQTtFQUNBLHFCQUFBO0VBQ0Esd0JBQUE7RUFDQSxXQUFBO0VBQ0EsWUFBQTtFQUNBLGtCQUFBO0FBSFI7O0FBUUE7RUFDSSxXQUFBO0VBQ0EsaUJBQUE7QUFMSjs7QUFNSTtFQUNJLFdBQUE7QUFKUiIsImZpbGUiOiJzcmMvYXBwL2NvbXBvbmVudHMvY29hY2gvY29hY2gtYm9keS1zY2FuLXN0ZXAtdHdvL2NvYWNoLWJvZHktc2Nhbi1zdGVwLXR3by5jb21wb25lbnQuc2NzcyIsInNvdXJjZXNDb250ZW50IjpbIi5pbnB1dC1wcmljZSB7XG4gICAgZGlzcGxheTogZmxleDtcbiAgICBhbGlnbi1pdGVtczogY2VudGVyO1xuICAgIGp1c3RpZnktY29udGVudDogY2VudGVyO1xuICAgIGZvbnQtZmFtaWx5OiBPc3dhbGQsIHNhbnMtc2VyaWY7XG59XG5cbi5kaXNwbGF5LXByaWNlIHtcbiAgICBwYWRkaW5nOiA1cHg7XG4gICAgZm9udC13ZWlnaHQ6IGJvbGQ7XG4gICAgZm9udC1zaXplOiAxNHB4O1xuICAgIGZvbnQtZmFtaWx5OiBPc3dhbGQsIHNhbnMtc2VyaWY7XG59XG5cbmlvbi1pdGVtIGlvbi1sYWJlbCB7XG4gICAgd2hpdGUtc3BhY2U6IG5vcm1hbDtcbiAgICBmb250LWZhbWlseTogT3N3YWxkLCBzYW5zLXNlcmlmO1xufVxuXG5pb24taXRlbSB7XG4gICAgLS1pbm5lci1ib3JkZXItd2lkdGg6IDBweCAwcHggMHB4IDBweDtcbn1cblxuLnNwb3J0cy1ib3ggaW9uLWJ1dHRvbiB7XG4gICAgaGVpZ2h0OiA4MHB4O1xuICAgIC0taW9uLWJ1dHRvbi1yb3VuZC1ib3JkZXItcmFkaXVzOiA1cHg7XG4gICAgLS1pb24tYnV0dG9uLWJvcmRlci1yYWRpdXM6IDVweDtcbiAgICAtLWlvbi1ib3JkZXItcmFkaXVzOiA1cHg7XG4gICAgLS1ib3JkZXItcmFkaXVzOiA1cHg7XG59XG5cbi5zcG9ydHMtYm94IGlvbi1idXR0b24gaW9uLWljb24ge1xuICAgIGZvbnQtc2l6ZTogNTBweDtcbn1cblxuLmdlbmRlciBpb24tYnV0dG9uIHtcbiAgICB3aGl0ZS1zcGFjZTogbm9ybWFsO1xuICAgIGZvbnQtc2l6ZTogMTJweDtcbiAgICAtLXBhZGRpbmctc3RhcnQ6IDA7XG4gICAgLS1wYWRkaW5nLWVuZDogMDtcbiAgICBoZWlnaHQ6IDUwcHg7XG4gICAgZm9udC1mYW1pbHk6IE9zd2FsZCwgc2Fucy1zZXJpZjtcbn1cblxuLmNhbGVuZGFyLWljb24ge1xuICAgIHBvc2l0aW9uOiBhYnNvbHV0ZTtcbiAgICByaWdodDogNXB4O1xuICAgIGJvdHRvbTogNHB4O1xufVxuXG5pb24taXRlbSB7XG4gICAgLS1pbm5lci1ib3JkZXItd2lkdGg6IDBweCAwcHggMHB4IDBweDtcbiAgICAtLW1pbi1oZWlnaHQ6IDM1cHg7XG4gICAgLS1wYWRkaW5nLXN0YXJ0OiAwcHg7XG4gICAgLS1ib3JkZXItd2lkdGg6IDAgMCAwcHggMDtcbn1cblxuaW9uLWxpc3Qge1xuICAgIHBhZGRpbmctdG9wOiAwcHggIWltcG9ydGFudDtcbiAgICBwYWRkaW5nLWJvdHRvbTogMHB4ICFpbXBvcnRhbnQ7XG59XG5cbmlvbi1yYW5nZSB7XG4gICAgLS1rbm9iLWJhY2tncm91bmQ6IHZhcigtLWlvbi1jb2xvci1wcmltYXJ5KTtcbiAgICAtLWJhci1oZWlnaHQ6IDhweDtcbiAgICAtLWtub2Itc2l6ZTogMzBweDtcbiAgICAtLWhlaWdodDogMjBweDtcbiAgICAtLWtub2ItYm9yZGVyLXJhZGl1czogMjIlO1xuICAgIC0tYmFyLWJhY2tncm91bmQtYWN0aXZlOiB2YXIoLS1pb24tY29sb3ItcHJpbWFyeSk7XG59XG5cbi5kYXRlRGVOYSB7XG4gICAgcGFkZGluZzogMnB4IDBweDtcbn1cblxuLmFjdGl2aXRlUGh5c2lxdWVFeHRlcm5lIHtcbiAgICBwYWRkaW5nOiAyNXB4IDBweDtcbn1cblxuaW9uLWNhcmQtY29udGVudCB7XG4gICAgcGFkZGluZy1pbmxpbmUtZW5kOiAwcHggIWltcG9ydGFudDtcbiAgICBmb250LWZhbWlseTogT3N3YWxkLCBzYW5zLXNlcmlmO1xufVxuXG5pb24tc2VnbWVudC1idXR0b24ge1xuICAgIC0tcmlwcGxlLWNvbG9yOiB0cmFuc3BhcmVudDtcbiAgICBiYWNrZ3JvdW5kOiB0cmFuc3BhcmVudDtcbiAgICAtLWNvbG9yLWNoZWNrZWQ6IHdoaXRlO1xuICAgIC0tY29sb3I6ICM5YzljOWM7XG4gICAgLS1pbmRpY2F0b3ItY29sb3I6IHZhcigtLWlvbi1jb2xvci1wcmltYXJ5KTtcbiAgICAtLW1hcmdpbi1ib3R0b206IDBweDtcbiAgICBoZWlnaHQ6IDQwcHg7XG4gICAgLS1ib3JkZXItcmFkaXVzOiAyNXB4O1xufVxuXG5pb24tc2VnbWVudCB7XG4gICAgYmFja2dyb3VuZDogcmdiKDE3MCAxNzAgMTcwIC8gMTElKTtcbiAgICBib3JkZXItcmFkaXVzOiAyNXB4O1xufVxuXG4uc2VnbWVudC1ib3gge1xuICAgIG1hcmdpbi1ib3R0b206IDI1cHg7XG59XG5cbi5yZW1vdmUge1xuICAgIGNvbG9yOiAjMDAwO1xufVxuXG4uYWRkIHtcbiAgICBjb2xvcjogIzAwMDtcbn1cblxuLnNwb3J0cy1ib3ggaW9uLWJ1dHRvbiB7XG4gICAgLS1iYWNrZ3JvdW5kOiByZ2IoMCAwIDAgLyA1MCUpO1xufVxuXG4uc3BvcnRzLWJveCBpb24tYnV0dG9uLmFjdGl2ZSB7XG4gICAgLS1iYWNrZ3JvdW5kOiB2YXIoLS1pb24tY29sb3ItcHJpbWFyeSk7XG59XG4uc3RlcGVyLWhlYWRpbmcge1xuICAgIGZvbnQtZmFtaWx5OiBPc3dhbGQ7XG59XG4uc3RlcGVyLXRpdGxlIHtcbiAgICBmb250LWZhbWlseTogT3N3YWxkLCBzYW5zLXNlcmlmO1xufVxuXG5cblxuXG4ubnVtYmVyZWRfZGF0YV9zZWN0aW9uIHtcbiAgICAudGV4dF9kZXNjIHtcbiAgICAgICAgdGV4dC1hbGlnbjogbGVmdDtcbiAgICB9XG59XG5cbi50ZXh0X2Rlc2Mge1xuICAgIGlvbi1jYXJkIHtcbiAgICAgICAgbWFyZ2luOiAwO1xuICAgICAgICAvKiBwYWRkaW5nLWxlZnQ6IDQlOyAqL1xuICAgICAgICAvKiBwYWRkaW5nLXRvcDogMyU7ICovXG4gICAgICAgIC8qIHBhZGRpbmctYm90dG9tOiAyJTsgKi9cbiAgICAgICAgcGFkZGluZzogNCU7XG4gICAgICAgIGhlaWdodDogMTAwJTtcbiAgICAgICAgYm9yZGVyLXJhZGl1czogNXB4O1xuICAgIH1cbn1cblxuXG4uZXhjZXJjaXNlX2lucHV0X3NlY3Rpb24ge1xuICAgIHdpZHRoOiAxMDAlO1xuICAgIG1hcmdpbi1ib3R0b206IDIlO1xuICAgIGlvbi1pdGVtIHtcbiAgICAgICAgd2lkdGg6IDEwMCU7XG4gICAgfVxufSJdfQ== */";
      /***/
    },

    /***/
    "./src/app/components/coach/coach-body-scan-step-two/coach-body-scan-step-two.component.ts":
    /*!*************************************************************************************************!*\
      !*** ./src/app/components/coach/coach-body-scan-step-two/coach-body-scan-step-two.component.ts ***!
      \*************************************************************************************************/

    /*! exports provided: CoachBodyScanStepTwoComponent */

    /***/
    function srcAppComponentsCoachCoachBodyScanStepTwoCoachBodyScanStepTwoComponentTs(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "CoachBodyScanStepTwoComponent", function () {
        return CoachBodyScanStepTwoComponent;
      });
      /* harmony import */


      var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! tslib */
      "./node_modules/tslib/tslib.es6.js");
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! @angular/core */
      "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");

      var CoachBodyScanStepTwoComponent = /*#__PURE__*/function () {
        function CoachBodyScanStepTwoComponent() {
          _classCallCheck(this, CoachBodyScanStepTwoComponent);
        }

        _createClass(CoachBodyScanStepTwoComponent, [{
          key: "ngOnInit",
          value: function ngOnInit() {}
        }]);

        return CoachBodyScanStepTwoComponent;
      }();

      CoachBodyScanStepTwoComponent.ctorParameters = function () {
        return [];
      };

      CoachBodyScanStepTwoComponent = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-coach-body-scan-step-two',
        template: Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(
        /*! raw-loader!./coach-body-scan-step-two.component.html */
        "./node_modules/raw-loader/dist/cjs.js!./src/app/components/coach/coach-body-scan-step-two/coach-body-scan-step-two.component.html"))["default"],
        styles: [Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(
        /*! ./coach-body-scan-step-two.component.scss */
        "./src/app/components/coach/coach-body-scan-step-two/coach-body-scan-step-two.component.scss"))["default"]]
      })], CoachBodyScanStepTwoComponent);
      /***/
    },

    /***/
    "./src/app/components/header/header.component.scss":
    /*!*********************************************************!*\
      !*** ./src/app/components/header/header.component.scss ***!
      \*********************************************************/

    /*! exports provided: default */

    /***/
    function srcAppComponentsHeaderHeaderComponentScss(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony default export */


      __webpack_exports__["default"] = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL2NvbXBvbmVudHMvaGVhZGVyL2hlYWRlci5jb21wb25lbnQuc2NzcyJ9 */";
      /***/
    },

    /***/
    "./src/app/components/header/header.component.ts":
    /*!*******************************************************!*\
      !*** ./src/app/components/header/header.component.ts ***!
      \*******************************************************/

    /*! exports provided: HeaderComponent */

    /***/
    function srcAppComponentsHeaderHeaderComponentTs(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "HeaderComponent", function () {
        return HeaderComponent;
      });
      /* harmony import */


      var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! tslib */
      "./node_modules/tslib/tslib.es6.js");
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! @angular/core */
      "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");

      var HeaderComponent = /*#__PURE__*/function () {
        function HeaderComponent() {
          _classCallCheck(this, HeaderComponent);
        }

        _createClass(HeaderComponent, [{
          key: "ngOnInit",
          value: function ngOnInit() {}
        }]);

        return HeaderComponent;
      }();

      HeaderComponent.ctorParameters = function () {
        return [];
      };

      HeaderComponent = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-header',
        template: Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(
        /*! raw-loader!./header.component.html */
        "./node_modules/raw-loader/dist/cjs.js!./src/app/components/header/header.component.html"))["default"],
        styles: [Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(
        /*! ./header.component.scss */
        "./src/app/components/header/header.component.scss"))["default"]]
      })], HeaderComponent);
      /***/
    },

    /***/
    "./src/app/components/modals/add-excercise-wood-modal/add-excercise-wood-modal.component.scss":
    /*!****************************************************************************************************!*\
      !*** ./src/app/components/modals/add-excercise-wood-modal/add-excercise-wood-modal.component.scss ***!
      \****************************************************************************************************/

    /*! exports provided: default */

    /***/
    function srcAppComponentsModalsAddExcerciseWoodModalAddExcerciseWoodModalComponentScss(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony default export */


      __webpack_exports__["default"] = "ion-content {\n  --padding-start: 2%;\n  --padding-end: 2%;\n}\n\nion-button {\n  text-transform: capitalize;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvY29tcG9uZW50cy9tb2RhbHMvYWRkLWV4Y2VyY2lzZS13b29kLW1vZGFsL2FkZC1leGNlcmNpc2Utd29vZC1tb2RhbC5jb21wb25lbnQuc2NzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQTtFQUNJLG1CQUFBO0VBQ0EsaUJBQUE7QUFDSjs7QUFHQTtFQUNJLDBCQUFBO0FBQUoiLCJmaWxlIjoic3JjL2FwcC9jb21wb25lbnRzL21vZGFscy9hZGQtZXhjZXJjaXNlLXdvb2QtbW9kYWwvYWRkLWV4Y2VyY2lzZS13b29kLW1vZGFsLmNvbXBvbmVudC5zY3NzIiwic291cmNlc0NvbnRlbnQiOlsiaW9uLWNvbnRlbnQge1xuICAgIC0tcGFkZGluZy1zdGFydDogMiU7XG4gICAgLS1wYWRkaW5nLWVuZDogMiU7XG59XG5cblxuaW9uLWJ1dHRvbiB7XG4gICAgdGV4dC10cmFuc2Zvcm06IGNhcGl0YWxpemU7XG59Il19 */";
      /***/
    },

    /***/
    "./src/app/components/modals/add-excercise-wood-modal/add-excercise-wood-modal.component.ts":
    /*!**************************************************************************************************!*\
      !*** ./src/app/components/modals/add-excercise-wood-modal/add-excercise-wood-modal.component.ts ***!
      \**************************************************************************************************/

    /*! exports provided: AddExcerciseWoodModalComponent */

    /***/
    function srcAppComponentsModalsAddExcerciseWoodModalAddExcerciseWoodModalComponentTs(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "AddExcerciseWoodModalComponent", function () {
        return AddExcerciseWoodModalComponent;
      });
      /* harmony import */


      var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! tslib */
      "./node_modules/tslib/tslib.es6.js");
      /* harmony import */


      var _ionic_angular__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! @ionic/angular */
      "./node_modules/@ionic/angular/__ivy_ngcc__/fesm2015/ionic-angular.js");
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! @angular/core */
      "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
      /* harmony import */


      var hammerjs__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
      /*! hammerjs */
      "./node_modules/hammerjs/hammer.js");
      /* harmony import */


      var hammerjs__WEBPACK_IMPORTED_MODULE_3___default = /*#__PURE__*/__webpack_require__.n(hammerjs__WEBPACK_IMPORTED_MODULE_3__);

      var AddExcerciseWoodModalComponent = /*#__PURE__*/function () {
        function AddExcerciseWoodModalComponent(modal, gestureCtrl, element, renderer) {
          _classCallCheck(this, AddExcerciseWoodModalComponent);

          this.modal = modal;
          this.gestureCtrl = gestureCtrl;
          this.element = element;
          this.renderer = renderer;
          this.categoryWood = "";
          this.typeWood = "";
          this.selectLocker = "";
          this.selectMiniWood = "";
          this.yesNoBtnClick = "";
        }

        _createClass(AddExcerciseWoodModalComponent, [{
          key: "ngOnInit",
          value: function ngOnInit() {}
        }, {
          key: "yesClick",
          value: function yesClick() {
            this.yesNoBtnClick = 'yes';
            this.modal.dismiss();
          }
        }, {
          key: "noClick",
          value: function noClick() {
            this.yesNoBtnClick = 'no';
            this.modal.dismiss();
          }
        }, {
          key: "ngAfterViewInit",
          value: function ngAfterViewInit() {
            return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, /*#__PURE__*/regeneratorRuntime.mark(function _callee6() {
              var _this13 = this;

              var d, options, gesture;
              return regeneratorRuntime.wrap(function _callee6$(_context6) {
                while (1) {
                  switch (_context6.prev = _context6.next) {
                    case 0:
                      d = document.querySelector('ion-modal');
                      options = {
                        el: d,
                        direction: "y",
                        gestureName: 'swipe',
                        onStart: function onStart() {
                          _this13.renderer.setStyle(d, 'transition', 'none');
                        },
                        onMove: function onMove(ev) {
                          if (ev.deltaY > 0) {
                            _this13.renderer.setStyle(d, 'transform', "translateY(".concat(ev.deltaY, "px)"));
                          }
                        },
                        onEnd: function onEnd(ev) {
                          _this13.renderer.setStyle(d, 'transition', '0.5s ease-out');

                          if (ev.deltaY > -200) {
                            _this13.renderer.setStyle(d, 'transform', 'translateY(500px)');

                            _this13.modal.dismiss();
                          } else {
                            _this13.renderer.setStyle(d, 'transform', 'translateY(0px)');
                          }
                        }
                      };
                      _context6.next = 4;
                      return this.gestureCtrl.create(options);

                    case 4:
                      gesture = _context6.sent;
                      gesture.enable();

                    case 6:
                    case "end":
                      return _context6.stop();
                  }
                }
              }, _callee6, this);
            }));
          }
        }]);

        return AddExcerciseWoodModalComponent;
      }();

      AddExcerciseWoodModalComponent.ctorParameters = function () {
        return [{
          type: _ionic_angular__WEBPACK_IMPORTED_MODULE_1__["ModalController"]
        }, {
          type: _ionic_angular__WEBPACK_IMPORTED_MODULE_1__["GestureController"]
        }, {
          type: _angular_core__WEBPACK_IMPORTED_MODULE_2__["ElementRef"]
        }, {
          type: _angular_core__WEBPACK_IMPORTED_MODULE_2__["Renderer2"]
        }];
      };

      AddExcerciseWoodModalComponent = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([Object(_angular_core__WEBPACK_IMPORTED_MODULE_2__["Component"])({
        selector: 'app-add-excercise-wood-modal',
        template: Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(
        /*! raw-loader!./add-excercise-wood-modal.component.html */
        "./node_modules/raw-loader/dist/cjs.js!./src/app/components/modals/add-excercise-wood-modal/add-excercise-wood-modal.component.html"))["default"],
        styles: [Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(
        /*! ./add-excercise-wood-modal.component.scss */
        "./src/app/components/modals/add-excercise-wood-modal/add-excercise-wood-modal.component.scss"))["default"]]
      })], AddExcerciseWoodModalComponent);
      /***/
    },

    /***/
    "./src/app/components/modals/before-aliments/before-aliments.component.scss":
    /*!**********************************************************************************!*\
      !*** ./src/app/components/modals/before-aliments/before-aliments.component.scss ***!
      \**********************************************************************************/

    /*! exports provided: default */

    /***/
    function srcAppComponentsModalsBeforeAlimentsBeforeAlimentsComponentScss(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony default export */


      __webpack_exports__["default"] = "ion-content {\n  --padding-start: 3%;\n  --padding-end: 3%;\n}\n\nion-toolbar {\n  border-bottom: 1px solid #f1f1f1;\n  height: 9%;\n}\n\nion-toolbar ion-text {\n  display: block;\n}\n\n.close-btn-icon {\n  font-size: 29px;\n  font-weight: 700;\n}\n\n.toggle_btn_section {\n  width: 100%;\n  --padding-start: 0;\n}\n\n.toggle_btn_section ion-toggle {\n  height: 25px;\n}\n\nion-avatar {\n  padding: 15px;\n}\n\n.header_row {\n  display: flex;\n  flex-direction: row;\n  justify-content: space-between;\n}\n\n.header_row ion-label {\n  align-self: center;\n}\n\n.header_row div {\n  width: 48%;\n  border-style: dashed;\n  border-color: gray;\n  height: 1px;\n  border-width: 1px;\n  align-self: center;\n  position: absolute;\n  right: 16%;\n}\n\n.main_line_section {\n  display: flex;\n  flex-direction: row;\n  height: 24px;\n}\n\n.main_line_section .yellow-back {\n  background: var(--ion-color-primary);\n  width: 20px;\n  height: 4px;\n  align-self: center;\n}\n\n.main_line_section .black-back {\n  background: #000;\n  width: 20px;\n  height: 4px;\n  align-self: center;\n}\n\n.card_item_section ion-col ion-card {\n  height: 150px;\n  margin: 0;\n}\n\n.card_item_section ion-col ion-card ion-card-header ion-button {\n  width: 100%;\n  --background: #2a2a2a;\n}\n\n.card_item_section ion-col ion-card ion-card-content {\n  display: flex;\n  justify-content: center;\n  flex-direction: column;\n}\n\n.card_item_section ion-col ion-card ion-card-content ion-img {\n  width: 25px;\n  background: #bebebe;\n  border-radius: 50%;\n  align-self: center;\n}\n\n.card_item_section ion-col ion-card ion-card-content p {\n  align-self: center;\n}\n\n.card_footer_data p {\n  margin: 0;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvY29tcG9uZW50cy9tb2RhbHMvYmVmb3JlLWFsaW1lbnRzL2JlZm9yZS1hbGltZW50cy5jb21wb25lbnQuc2NzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQTtFQUNJLG1CQUFBO0VBQ0EsaUJBQUE7QUFDSjs7QUFFQTtFQUNJLGdDQUFBO0VBQ0EsVUFBQTtBQUNKOztBQUNJO0VBQ0ksY0FBQTtBQUNSOztBQUlBO0VBQ0ksZUFBQTtFQUNBLGdCQUFBO0FBREo7O0FBSUE7RUFDSSxXQUFBO0VBQ0Esa0JBQUE7QUFESjs7QUFHSTtFQUNJLFlBQUE7QUFEUjs7QUFRQTtFQUNJLGFBQUE7QUFMSjs7QUFRQTtFQUNJLGFBQUE7RUFDQSxtQkFBQTtFQUNBLDhCQUFBO0FBTEo7O0FBT0k7RUFDSSxrQkFBQTtBQUxSOztBQVVJO0VBQ0ksVUFBQTtFQUNBLG9CQUFBO0VBQ0Esa0JBQUE7RUFDQSxXQUFBO0VBQ0EsaUJBQUE7RUFDQSxrQkFBQTtFQUNBLGtCQUFBO0VBQ0EsVUFBQTtBQVJSOztBQVlBO0VBQ0ksYUFBQTtFQUNBLG1CQUFBO0VBQ0EsWUFBQTtBQVRKOztBQVdJO0VBQ0ksb0NBQUE7RUFDQSxXQUFBO0VBQ0EsV0FBQTtFQUNBLGtCQUFBO0FBVFI7O0FBWUk7RUFDSSxnQkFBQTtFQUNBLFdBQUE7RUFDQSxXQUFBO0VBQ0Esa0JBQUE7QUFWUjs7QUFrQlE7RUFDSSxhQUFBO0VBQ0EsU0FBQTtBQWZaOztBQWtCZ0I7RUFDSSxXQUFBO0VBQ0EscUJBQUE7QUFoQnBCOztBQW9CWTtFQUNJLGFBQUE7RUFDQSx1QkFBQTtFQUNBLHNCQUFBO0FBbEJoQjs7QUFtQmdCO0VBQ0ksV0FBQTtFQUNBLG1CQUFBO0VBQ0Esa0JBQUE7RUFDQSxrQkFBQTtBQWpCcEI7O0FBb0JnQjtFQUNJLGtCQUFBO0FBbEJwQjs7QUE0Qkk7RUFDSSxTQUFBO0FBekJSIiwiZmlsZSI6InNyYy9hcHAvY29tcG9uZW50cy9tb2RhbHMvYmVmb3JlLWFsaW1lbnRzL2JlZm9yZS1hbGltZW50cy5jb21wb25lbnQuc2NzcyIsInNvdXJjZXNDb250ZW50IjpbImlvbi1jb250ZW50IHtcbiAgICAtLXBhZGRpbmctc3RhcnQ6IDMlO1xuICAgIC0tcGFkZGluZy1lbmQ6IDMlO1xufVxuXG5pb24tdG9vbGJhciB7XG4gICAgYm9yZGVyLWJvdHRvbTogMXB4IHNvbGlkICNmMWYxZjE7XG4gICAgaGVpZ2h0OiA5JTtcblxuICAgIGlvbi10ZXh0IHtcbiAgICAgICAgZGlzcGxheTogYmxvY2s7XG4gICAgfVxufVxuXG5cbi5jbG9zZS1idG4taWNvbiB7XG4gICAgZm9udC1zaXplOiAyOXB4O1xuICAgIGZvbnQtd2VpZ2h0OiA3MDA7XG59XG5cbi50b2dnbGVfYnRuX3NlY3Rpb24ge1xuICAgIHdpZHRoOiAxMDAlO1xuICAgIC0tcGFkZGluZy1zdGFydDogMDtcblxuICAgIGlvbi10b2dnbGUge1xuICAgICAgICBoZWlnaHQ6IDI1cHg7XG4gICAgfVxufVxuXG5cblxuXG5pb24tYXZhdGFyIHtcbiAgICBwYWRkaW5nOiAxNXB4O1xufVxuXG4uaGVhZGVyX3JvdyB7XG4gICAgZGlzcGxheTogZmxleDtcbiAgICBmbGV4LWRpcmVjdGlvbjogcm93O1xuICAgIGp1c3RpZnktY29udGVudDogc3BhY2UtYmV0d2VlbjtcblxuICAgIGlvbi1sYWJlbCB7XG4gICAgICAgIGFsaWduLXNlbGY6IGNlbnRlcjtcblxuICAgIH1cblxuXG4gICAgZGl2IHtcbiAgICAgICAgd2lkdGg6IDQ4JTtcbiAgICAgICAgYm9yZGVyLXN0eWxlOiBkYXNoZWQ7XG4gICAgICAgIGJvcmRlci1jb2xvcjogZ3JheTtcbiAgICAgICAgaGVpZ2h0OiAxcHg7XG4gICAgICAgIGJvcmRlci13aWR0aDogMXB4O1xuICAgICAgICBhbGlnbi1zZWxmOiBjZW50ZXI7XG4gICAgICAgIHBvc2l0aW9uOiBhYnNvbHV0ZTtcbiAgICAgICAgcmlnaHQ6IDE2JTtcbiAgICB9XG59XG5cbi5tYWluX2xpbmVfc2VjdGlvbiB7XG4gICAgZGlzcGxheTogZmxleDtcbiAgICBmbGV4LWRpcmVjdGlvbjogcm93O1xuICAgIGhlaWdodDogMjRweDtcblxuICAgIC55ZWxsb3ctYmFjayB7XG4gICAgICAgIGJhY2tncm91bmQ6IHZhcigtLWlvbi1jb2xvci1wcmltYXJ5KTtcbiAgICAgICAgd2lkdGg6IDIwcHg7XG4gICAgICAgIGhlaWdodDogNHB4O1xuICAgICAgICBhbGlnbi1zZWxmOiBjZW50ZXI7XG4gICAgfVxuXG4gICAgLmJsYWNrLWJhY2sge1xuICAgICAgICBiYWNrZ3JvdW5kOiAjMDAwO1xuICAgICAgICB3aWR0aDogMjBweDtcbiAgICAgICAgaGVpZ2h0OiA0cHg7XG4gICAgICAgIGFsaWduLXNlbGY6IGNlbnRlcjtcbiAgICB9XG59XG5cblxuXG4uY2FyZF9pdGVtX3NlY3Rpb24ge1xuICAgIGlvbi1jb2wge1xuICAgICAgICBpb24tY2FyZCB7XG4gICAgICAgICAgICBoZWlnaHQ6IDE1MHB4O1xuICAgICAgICAgICAgbWFyZ2luOiAwO1xuXG4gICAgICAgICAgICBpb24tY2FyZC1oZWFkZXIge1xuICAgICAgICAgICAgICAgIGlvbi1idXR0b24ge1xuICAgICAgICAgICAgICAgICAgICB3aWR0aDogMTAwJTtcbiAgICAgICAgICAgICAgICAgICAgLS1iYWNrZ3JvdW5kOiAjMmEyYTJhO1xuICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgIH1cblxuICAgICAgICAgICAgaW9uLWNhcmQtY29udGVudCB7XG4gICAgICAgICAgICAgICAgZGlzcGxheTogZmxleDtcbiAgICAgICAgICAgICAgICBqdXN0aWZ5LWNvbnRlbnQ6IGNlbnRlcjtcbiAgICAgICAgICAgICAgICBmbGV4LWRpcmVjdGlvbjogY29sdW1uO1xuICAgICAgICAgICAgICAgIGlvbi1pbWcge1xuICAgICAgICAgICAgICAgICAgICB3aWR0aDogMjVweDtcbiAgICAgICAgICAgICAgICAgICAgYmFja2dyb3VuZDogI2JlYmViZTtcbiAgICAgICAgICAgICAgICAgICAgYm9yZGVyLXJhZGl1czogNTAlO1xuICAgICAgICAgICAgICAgICAgICBhbGlnbi1zZWxmOiBjZW50ZXI7XG4gICAgICAgICAgICAgICAgfVxuXG4gICAgICAgICAgICAgICAgcCB7XG4gICAgICAgICAgICAgICAgICAgIGFsaWduLXNlbGY6IGNlbnRlcjtcblxuICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgIH1cbiAgICAgICAgfVxuICAgIH1cbn1cblxuXG4uY2FyZF9mb290ZXJfZGF0YSB7XG4gICAgcCB7XG4gICAgICAgIG1hcmdpbjogMDtcbiAgICB9XG59Il19 */";
      /***/
    },

    /***/
    "./src/app/components/modals/before-aliments/before-aliments.component.ts":
    /*!********************************************************************************!*\
      !*** ./src/app/components/modals/before-aliments/before-aliments.component.ts ***!
      \********************************************************************************/

    /*! exports provided: BeforeAlimentsComponent */

    /***/
    function srcAppComponentsModalsBeforeAlimentsBeforeAlimentsComponentTs(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "BeforeAlimentsComponent", function () {
        return BeforeAlimentsComponent;
      });
      /* harmony import */


      var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! tslib */
      "./node_modules/tslib/tslib.es6.js");
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! @angular/core */
      "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
      /* harmony import */


      var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! @angular/router */
      "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");
      /* harmony import */


      var _ionic_angular__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
      /*! @ionic/angular */
      "./node_modules/@ionic/angular/__ivy_ngcc__/fesm2015/ionic-angular.js");

      var BeforeAlimentsComponent = /*#__PURE__*/function () {
        function BeforeAlimentsComponent(modalCtrl, gestureCtrl, element, renderer, router) {
          _classCallCheck(this, BeforeAlimentsComponent);

          this.modalCtrl = modalCtrl;
          this.gestureCtrl = gestureCtrl;
          this.element = element;
          this.renderer = renderer;
          this.router = router;
        }

        _createClass(BeforeAlimentsComponent, [{
          key: "ngOnInit",
          value: function ngOnInit() {}
        }, {
          key: "ngAfterViewInit",
          value: function ngAfterViewInit() {
            return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, /*#__PURE__*/regeneratorRuntime.mark(function _callee7() {
              var _this14 = this;

              var d, options, gesture;
              return regeneratorRuntime.wrap(function _callee7$(_context7) {
                while (1) {
                  switch (_context7.prev = _context7.next) {
                    case 0:
                      d = document.querySelector('ion-modal');
                      options = {
                        el: d,
                        direction: "y",
                        gestureName: 'swipe',
                        onStart: function onStart() {
                          _this14.renderer.setStyle(d, 'transition', 'none');
                        },
                        onMove: function onMove(ev) {
                          if (ev.deltaY > 0) {
                            _this14.renderer.setStyle(d, 'transform', "translateY(".concat(ev.deltaY, "px)"));
                          }
                        },
                        onEnd: function onEnd(ev) {
                          _this14.renderer.setStyle(d, 'transition', '0.5s ease-out');

                          if (ev.deltaY > -200) {
                            _this14.renderer.setStyle(d, 'transform', 'translateY(500px)');

                            _this14.modalCtrl.dismiss();
                          } else {
                            _this14.renderer.setStyle(d, 'transform', 'translateY(0px)');
                          }
                        }
                      };
                      _context7.next = 4;
                      return this.gestureCtrl.create(options);

                    case 4:
                      gesture = _context7.sent;
                      gesture.enable();

                    case 6:
                    case "end":
                      return _context7.stop();
                  }
                }
              }, _callee7, this);
            }));
          }
        }, {
          key: "gotoAliments",
          value: function gotoAliments() {
            this.router.navigate(['/aliments']);
            this.modalCtrl.dismiss();
          }
        }, {
          key: "closeModal",
          value: function closeModal() {
            this.modalCtrl.dismiss();
          }
        }]);

        return BeforeAlimentsComponent;
      }();

      BeforeAlimentsComponent.ctorParameters = function () {
        return [{
          type: _ionic_angular__WEBPACK_IMPORTED_MODULE_3__["ModalController"]
        }, {
          type: _ionic_angular__WEBPACK_IMPORTED_MODULE_3__["GestureController"]
        }, {
          type: _angular_core__WEBPACK_IMPORTED_MODULE_1__["ElementRef"]
        }, {
          type: _angular_core__WEBPACK_IMPORTED_MODULE_1__["Renderer2"]
        }, {
          type: _angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"]
        }];
      };

      BeforeAlimentsComponent = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-before-aliments',
        template: Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(
        /*! raw-loader!./before-aliments.component.html */
        "./node_modules/raw-loader/dist/cjs.js!./src/app/components/modals/before-aliments/before-aliments.component.html"))["default"],
        styles: [Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(
        /*! ./before-aliments.component.scss */
        "./src/app/components/modals/before-aliments/before-aliments.component.scss"))["default"]]
      })], BeforeAlimentsComponent);
      /***/
    },

    /***/
    "./src/app/components/modals/calendar-modal/calendar-modal.scss":
    /*!**********************************************************************!*\
      !*** ./src/app/components/modals/calendar-modal/calendar-modal.scss ***!
      \**********************************************************************/

    /*! exports provided: default */

    /***/
    function srcAppComponentsModalsCalendarModalCalendarModalScss(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony default export */


      __webpack_exports__["default"] = ".daybox {\n  text-align: center;\n  color: #fff;\n  margin-top: 15px;\n}\n\n.daybox-title {\n  display: inline-block;\n  background: #626262;\n  padding: 3px 20px;\n  border-radius: 25px;\n  border: 2px solid #333;\n  z-index: 1;\n  position: relative;\n}\n\n.toggle-button {\n  position: absolute;\n  right: 15px;\n  top: 10px;\n}\n\n.detail-box {\n  padding-left: 20px;\n  padding-right: 20px;\n}\n\nion-item ion-label {\n  font-size: 12px;\n  font-family: \"Oswald\";\n}\n\n.next {\n  --background: var(--ion-btn-custom-color);\n  font-size: 10px;\n}\n\nion-toggle {\n  --handle-background: #fff url('female.png') no-repeat center / contain;\n  --handle-background-checked: #fff url('female.png') no-repeat center / contain;\n}\n\nion-datetime {\n  font-size: 12px;\n  font-weight: 100;\n}\n\nion-select {\n  font-size: 12px !important;\n  font-weight: 100;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvY29tcG9uZW50cy9tb2RhbHMvY2FsZW5kYXItbW9kYWwvY2FsZW5kYXItbW9kYWwuc2NzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQTtFQUNJLGtCQUFBO0VBQ0EsV0FBQTtFQUNBLGdCQUFBO0FBQ0o7O0FBRUE7RUFDSSxxQkFBQTtFQUNBLG1CQUFBO0VBQ0EsaUJBQUE7RUFDQSxtQkFBQTtFQUNBLHNCQUFBO0VBQ0EsVUFBQTtFQUNBLGtCQUFBO0FBQ0o7O0FBQ0E7RUFDSSxrQkFBQTtFQUNBLFdBQUE7RUFDQSxTQUFBO0FBRUo7O0FBQUE7RUFDSSxrQkFBQTtFQUNBLG1CQUFBO0FBR0o7O0FBQUk7RUFDSSxlQUFBO0VBQ0EscUJBQUE7QUFHUjs7QUFBQTtFQUNJLHlDQUFBO0VBQ0EsZUFBQTtBQUdKOztBQUNBO0VBQ0ksc0VBQUE7RUFDQSw4RUFBQTtBQUVKOztBQUFBO0VBQ0ksZUFBQTtFQUNBLGdCQUFBO0FBR0o7O0FBREE7RUFDSSwwQkFBQTtFQUNBLGdCQUFBO0FBSUoiLCJmaWxlIjoic3JjL2FwcC9jb21wb25lbnRzL21vZGFscy9jYWxlbmRhci1tb2RhbC9jYWxlbmRhci1tb2RhbC5zY3NzIiwic291cmNlc0NvbnRlbnQiOlsiLmRheWJveCB7XG4gICAgdGV4dC1hbGlnbjogY2VudGVyO1xuICAgIGNvbG9yOiAjZmZmO1xuICAgIG1hcmdpbi10b3A6IDE1cHg7XG59XG5cbi5kYXlib3gtdGl0bGUge1xuICAgIGRpc3BsYXk6IGlubGluZS1ibG9jaztcbiAgICBiYWNrZ3JvdW5kOiAjNjI2MjYyO1xuICAgIHBhZGRpbmc6IDNweCAyMHB4O1xuICAgIGJvcmRlci1yYWRpdXM6IDI1cHg7XG4gICAgYm9yZGVyOiAycHggc29saWQgIzMzMztcbiAgICB6LWluZGV4OiAxO1xuICAgIHBvc2l0aW9uOiByZWxhdGl2ZTtcbn1cbi50b2dnbGUtYnV0dG9uIHtcbiAgICBwb3NpdGlvbjogYWJzb2x1dGU7XG4gICAgcmlnaHQ6IDE1cHg7XG4gICAgdG9wOiAxMHB4O1xufVxuLmRldGFpbC1ib3gge1xuICAgIHBhZGRpbmctbGVmdDogMjBweDtcbiAgICBwYWRkaW5nLXJpZ2h0OiAyMHB4O1xufVxuaW9uLWl0ZW0ge1xuICAgIGlvbi1sYWJlbCB7XG4gICAgICAgIGZvbnQtc2l6ZTogMTJweDtcbiAgICAgICAgZm9udC1mYW1pbHk6ICdPc3dhbGQnO1xuICAgIH1cbn1cbi5uZXh0IHtcbiAgICAtLWJhY2tncm91bmQ6IHZhcigtLWlvbi1idG4tY3VzdG9tLWNvbG9yKTtcbiAgICBmb250LXNpemU6IDEwcHg7XG5cbn1cblxuaW9uLXRvZ2dsZSB7XG4gICAgLS1oYW5kbGUtYmFja2dyb3VuZDogICNmZmYgdXJsKCcuLi8uLi8uLi8uLi9hc3NldHMvL2ljb24vZmVtYWxlLnBuZycpIG5vLXJlcGVhdCBjZW50ZXIgLyBjb250YWluO1xuICAgIC0taGFuZGxlLWJhY2tncm91bmQtY2hlY2tlZDogI2ZmZiB1cmwoJy4uLy4uLy4uLy4uL2Fzc2V0cy8vaWNvbi9mZW1hbGUucG5nJykgbm8tcmVwZWF0IGNlbnRlciAvIGNvbnRhaW47XG59XG5pb24tZGF0ZXRpbWUge1xuICAgIGZvbnQtc2l6ZTogMTJweDtcbiAgICBmb250LXdlaWdodDogMTAwO1xufVxuaW9uLXNlbGVjdCB7XG4gICAgZm9udC1zaXplOiAxMnB4ICFpbXBvcnRhbnQ7XG4gICAgZm9udC13ZWlnaHQ6IDEwMDtcbn0iXX0= */";
      /***/
    },

    /***/
    "./src/app/components/modals/calendar-modal/calendar-modal.ts":
    /*!********************************************************************!*\
      !*** ./src/app/components/modals/calendar-modal/calendar-modal.ts ***!
      \********************************************************************/

    /*! exports provided: CalendarModal */

    /***/
    function srcAppComponentsModalsCalendarModalCalendarModalTs(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "CalendarModal", function () {
        return CalendarModal;
      });
      /* harmony import */


      var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! tslib */
      "./node_modules/tslib/tslib.es6.js");
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! @angular/core */
      "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
      /* harmony import */


      var _ionic_angular__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! @ionic/angular */
      "./node_modules/@ionic/angular/__ivy_ngcc__/fesm2015/ionic-angular.js");

      var CalendarModal = /*#__PURE__*/function () {
        function CalendarModal(modalCtrl) {
          _classCallCheck(this, CalendarModal);

          this.modalCtrl = modalCtrl;
          this.days = new Array(40);
          this.showDetails = false;
          this.options = {
            from: new Date(2000, 0, 1),
            pickMode: 'single',
            weekdays: ['Sun', 'Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat'],
            monthPickerFormat: ['Janvier', 'Février', 'Mars', 'Avril', 'Mai', 'Juin', 'Juillet', 'Août', 'Septembre', 'Octobre', 'Novembre', 'Décembre'],
            monthFormat: 'MMMM'
          };
        }

        _createClass(CalendarModal, [{
          key: "ngOnInit",
          value: function ngOnInit() {}
        }, {
          key: "onChange",
          value: function onChange(event) {
            console.log(event);
          }
        }, {
          key: "showDetail",
          value: function showDetail(event) {
            this.showDetails = !this.showDetails;
          }
        }, {
          key: "closeModal",
          value: function closeModal() {
            console.log("aa");
            this.modalCtrl.dismiss({
              componentProps: {}
            });
          }
        }]);

        return CalendarModal;
      }();

      CalendarModal.ctorParameters = function () {
        return [{
          type: _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["ModalController"]
        }];
      };

      CalendarModal = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-calendar-modal',
        template: Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(
        /*! raw-loader!./calendar-modal.html */
        "./node_modules/raw-loader/dist/cjs.js!./src/app/components/modals/calendar-modal/calendar-modal.html"))["default"],
        styles: [Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(
        /*! ./calendar-modal.scss */
        "./src/app/components/modals/calendar-modal/calendar-modal.scss"))["default"]]
      })], CalendarModal);
      /***/
    },

    /***/
    "./src/app/components/modals/check-up-modals/check-up-modals.component.scss":
    /*!**********************************************************************************!*\
      !*** ./src/app/components/modals/check-up-modals/check-up-modals.component.scss ***!
      \**********************************************************************************/

    /*! exports provided: default */

    /***/
    function srcAppComponentsModalsCheckUpModalsCheckUpModalsComponentScss(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony default export */


      __webpack_exports__["default"] = "ion-content {\n  --padding-start: 3%;\n  --padding-end: 3%;\n}\nion-content ion-text {\n  display: block;\n}\nion-toolbar {\n  border-bottom: 1px solid #f1f1f1;\n  height: 9%;\n}\n.close-btn-icon {\n  font-size: 29px;\n  font-weight: 700;\n}\nion-card {\n  margin: 0;\n}\n.check-out-big-cards {\n  display: flex;\n  flex-direction: column;\n  align-items: flex-end;\n}\n.check-out-big-cards p {\n  padding-bottom: 0;\n  margin-bottom: 0;\n  padding-right: 14%;\n}\n.check-out-big-cards div {\n  padding-bottom: 7px;\n  padding-left: 28%;\n  padding-right: 5%;\n}\n.check-out-big-cards ion-input {\n  margin-bottom: 0;\n  color: #000;\n  text-align: end;\n  font-weight: bold;\n}\n.check-out-big-cards h4 {\n  margin: 0;\n}\n.check-out-big-cards ion-label {\n  margin-top: 5px;\n  margin-left: 3px;\n  color: #353535;\n}\nion-card.program {\n  width: 100%;\n  margin-inline: 5px;\n  margin: 20px 0px;\n}\n.check-out-small-cards {\n  display: flex;\n  flex-direction: column;\n  align-items: flex-end;\n  width: 100%;\n  height: 70px;\n  justify-content: center;\n  padding-top: 5px;\n}\n.check-out-small-cards .small_card_title {\n  padding-right: 15%;\n}\n.check-out-small-cards .small_card_title .very_small_text {\n  color: #000 !important;\n}\n.check-out-small-cards .small_card_desc {\n  display: flex;\n  flex-direction: row;\n  align-self: center;\n}\n.check-out-small-cards .small_card_desc ion-item {\n  --min-height: 37px;\n  margin-left: 4px;\n  margin-bottom: 5px;\n}\n.check-out-small-cards .small_card_desc ion-item ion-input {\n  color: #000;\n  text-align: right;\n  --padding-top: 0;\n  --padding-bottom: 0;\n}\n.check-out-small-cards p {\n  margin-bottom: 0;\n  color: #353535;\n  margin-top: 0;\n  text-align: -webkit-center;\n  align-self: flex-end;\n  width: 100%;\n}\n.check-out-small-cards h4 {\n  margin: 0;\n}\n.check-out-small-cards ion-label {\n  margin-left: 3px;\n  color: #353535;\n}\n.check-out-small-cards .right-p {\n  align-self: flex-end;\n}\nion-card.program.active {\n  transform: scale(1.1, 1.1);\n  transition: all 0.5s ease;\n  border: 2px solid var(--ion-color-primary);\n  transition: all 0.5s ease;\n}\n.swiper-container {\n  overflow-x: visible;\n  overflow-y: visible;\n}\nion-card.program.active:before {\n  content: \"\";\n  background: url('checkmark-circle-outline.svg');\n  height: 25px;\n  width: 25px;\n  right: 5px;\n  top: 5px;\n  position: absolute;\n}\n.slide-img {\n  max-height: 100px !important;\n}\n.validate_btn {\n  width: 100%;\n  --background: var(--ion-btn-custom-color) ;\n}\nion-slide {\n  overflow-y: auto;\n}\nion-grid {\n  padding-top: 0;\n}\n.slide_card_content {\n  display: flex !important;\n  justify-content: center !important;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvY29tcG9uZW50cy9tb2RhbHMvY2hlY2stdXAtbW9kYWxzL2NoZWNrLXVwLW1vZGFscy5jb21wb25lbnQuc2NzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQTtFQUNJLG1CQUFBO0VBQ0EsaUJBQUE7QUFDSjtBQUNJO0VBQ0ksY0FBQTtBQUNSO0FBR0E7RUFDSSxnQ0FBQTtFQUNBLFVBQUE7QUFBSjtBQUdBO0VBQ0ksZUFBQTtFQUNBLGdCQUFBO0FBQUo7QUFHQTtFQUNJLFNBQUE7QUFBSjtBQUdBO0VBQ0ksYUFBQTtFQUNBLHNCQUFBO0VBQ0EscUJBQUE7QUFBSjtBQUVJO0VBQ0ksaUJBQUE7RUFDQSxnQkFBQTtFQUNBLGtCQUFBO0FBQVI7QUFHSTtFQUNJLG1CQUFBO0VBQ0EsaUJBQUE7RUFDQSxpQkFBQTtBQURSO0FBV0k7RUFDSSxnQkFBQTtFQUNBLFdBQUE7RUFDQSxlQUFBO0VBQ0EsaUJBQUE7QUFUUjtBQVlJO0VBQ0ksU0FBQTtBQVZSO0FBY0k7RUFDSSxlQUFBO0VBQ0EsZ0JBQUE7RUFDQSxjQUFBO0FBWlI7QUFnQkE7RUFDSSxXQUFBO0VBQ0Esa0JBQUE7RUFDQSxnQkFBQTtBQWJKO0FBZ0JBO0VBQ0ksYUFBQTtFQUNBLHNCQUFBO0VBQ0EscUJBQUE7RUFDQSxXQUFBO0VBQ0EsWUFBQTtFQUNBLHVCQUFBO0VBQ0EsZ0JBQUE7QUFiSjtBQWVJO0VBS0ksa0JBQUE7QUFqQlI7QUFtQlE7RUFDSSxzQkFBQTtBQWpCWjtBQXFCSTtFQUNJLGFBQUE7RUFDQSxtQkFBQTtFQUVBLGtCQUFBO0FBcEJSO0FBc0JRO0VBQ0ksa0JBQUE7RUFDQSxnQkFBQTtFQUVBLGtCQUFBO0FBckJaO0FBc0JZO0VBQ0ksV0FBQTtFQUNBLGlCQUFBO0VBRUEsZ0JBQUE7RUFDQSxtQkFBQTtBQXJCaEI7QUE2Qkk7RUFDSSxnQkFBQTtFQUNBLGNBQUE7RUFDQSxhQUFBO0VBQ0EsMEJBQUE7RUFDQSxvQkFBQTtFQUNBLFdBQUE7QUEzQlI7QUErQkk7RUFDSSxTQUFBO0FBN0JSO0FBaUNJO0VBRUksZ0JBQUE7RUFDQSxjQUFBO0FBaENSO0FBbUNJO0VBQ0ksb0JBQUE7QUFqQ1I7QUFzQ0E7RUFDSSwwQkFBQTtFQUNBLHlCQUFBO0VBQ0EsMENBQUE7RUFDQSx5QkFBQTtBQW5DSjtBQXNDQTtFQUNJLG1CQUFBO0VBQ0EsbUJBQUE7QUFuQ0o7QUFzQ0E7RUFDSSxXQUFBO0VBRUEsK0NBQUE7RUFFQSxZQUFBO0VBQ0EsV0FBQTtFQUNBLFVBQUE7RUFDQSxRQUFBO0VBQ0Esa0JBQUE7QUFyQ0o7QUEyQ0E7RUFDSSw0QkFBQTtBQXhDSjtBQTRDQTtFQUNJLFdBQUE7RUFDQSwwQ0FBQTtBQXpDSjtBQTZDQTtFQUNJLGdCQUFBO0FBMUNKO0FBOENBO0VBQ0ksY0FBQTtBQTNDSjtBQThDQTtFQUNJLHdCQUFBO0VBQ0Esa0NBQUE7QUEzQ0oiLCJmaWxlIjoic3JjL2FwcC9jb21wb25lbnRzL21vZGFscy9jaGVjay11cC1tb2RhbHMvY2hlY2stdXAtbW9kYWxzLmNvbXBvbmVudC5zY3NzIiwic291cmNlc0NvbnRlbnQiOlsiaW9uLWNvbnRlbnQge1xuICAgIC0tcGFkZGluZy1zdGFydDogMyU7XG4gICAgLS1wYWRkaW5nLWVuZDogMyU7XG5cbiAgICBpb24tdGV4dCB7XG4gICAgICAgIGRpc3BsYXk6IGJsb2NrO1xuICAgIH1cbn1cblxuaW9uLXRvb2xiYXIge1xuICAgIGJvcmRlci1ib3R0b206IDFweCBzb2xpZCAjZjFmMWYxO1xuICAgIGhlaWdodDogOSU7XG59XG5cbi5jbG9zZS1idG4taWNvbiB7XG4gICAgZm9udC1zaXplOiAyOXB4O1xuICAgIGZvbnQtd2VpZ2h0OiA3MDA7XG59XG5cbmlvbi1jYXJkIHtcbiAgICBtYXJnaW46IDA7XG59XG5cbi5jaGVjay1vdXQtYmlnLWNhcmRzIHtcbiAgICBkaXNwbGF5OiBmbGV4O1xuICAgIGZsZXgtZGlyZWN0aW9uOiBjb2x1bW47XG4gICAgYWxpZ24taXRlbXM6IGZsZXgtZW5kO1xuXG4gICAgcCB7XG4gICAgICAgIHBhZGRpbmctYm90dG9tOiAwO1xuICAgICAgICBtYXJnaW4tYm90dG9tOiAwO1xuICAgICAgICBwYWRkaW5nLXJpZ2h0OiAxNCU7XG4gICAgfVxuXG4gICAgZGl2IHtcbiAgICAgICAgcGFkZGluZy1ib3R0b206IDdweDtcbiAgICAgICAgcGFkZGluZy1sZWZ0OiAyOCU7XG4gICAgICAgIHBhZGRpbmctcmlnaHQ6IDUlO1xuICAgIH1cblxuICAgIC8vIGRpdiB7XG4gICAgLy8gICAgIGRpc3BsYXk6IGZsZXg7XG4gICAgLy8gICAgIGZsZXgtZGlyZWN0aW9uOiByb3c7XG4gICAgLy8gICAgIG1hcmdpbi10b3A6IDEwcHg7XG4gICAgLy8gICAgIG1hcmdpbi1ib3R0b206IDEwcHg7XG4gICAgLy8gfVxuXG4gICAgaW9uLWlucHV0IHtcbiAgICAgICAgbWFyZ2luLWJvdHRvbTogMDtcbiAgICAgICAgY29sb3I6ICMwMDA7XG4gICAgICAgIHRleHQtYWxpZ246IGVuZDtcbiAgICAgICAgZm9udC13ZWlnaHQ6IGJvbGQ7XG4gICAgfVxuXG4gICAgaDQge1xuICAgICAgICBtYXJnaW46IDA7XG5cbiAgICB9XG5cbiAgICBpb24tbGFiZWwge1xuICAgICAgICBtYXJnaW4tdG9wOiA1cHg7XG4gICAgICAgIG1hcmdpbi1sZWZ0OiAzcHg7XG4gICAgICAgIGNvbG9yOiAjMzUzNTM1O1xuICAgIH1cbn1cblxuaW9uLWNhcmQucHJvZ3JhbSB7XG4gICAgd2lkdGg6IDEwMCU7XG4gICAgbWFyZ2luLWlubGluZTogNXB4O1xuICAgIG1hcmdpbjogMjBweCAwcHg7XG59XG5cbi5jaGVjay1vdXQtc21hbGwtY2FyZHMge1xuICAgIGRpc3BsYXk6IGZsZXg7XG4gICAgZmxleC1kaXJlY3Rpb246IGNvbHVtbjtcbiAgICBhbGlnbi1pdGVtczogZmxleC1lbmQ7XG4gICAgd2lkdGg6IDEwMCU7XG4gICAgaGVpZ2h0OiA3MHB4O1xuICAgIGp1c3RpZnktY29udGVudDogY2VudGVyO1xuICAgIHBhZGRpbmctdG9wOiA1cHg7XG5cbiAgICAuc21hbGxfY2FyZF90aXRsZSB7XG4gICAgICAgIC8vIGRpc3BsYXk6IGZsZXg7XG4gICAgICAgIC8vIGZsZXgtZGlyZWN0aW9uOiBjb2x1bW47XG4gICAgICAgIC8vIG1hcmdpbjogMDtcbiAgICAgICAgLy8gYWxpZ24tc2VsZjogY2VudGVyO1xuICAgICAgICBwYWRkaW5nLXJpZ2h0OiAxNSU7XG5cbiAgICAgICAgLnZlcnlfc21hbGxfdGV4dCB7XG4gICAgICAgICAgICBjb2xvcjogIzAwMCAhaW1wb3J0YW50O1xuICAgICAgICB9XG4gICAgfVxuXG4gICAgLnNtYWxsX2NhcmRfZGVzYyB7XG4gICAgICAgIGRpc3BsYXk6IGZsZXg7XG4gICAgICAgIGZsZXgtZGlyZWN0aW9uOiByb3c7XG4gICAgICAgIC8vIG1hcmdpbi10b3A6IDEwcHg7XG4gICAgICAgIGFsaWduLXNlbGY6IGNlbnRlcjtcblxuICAgICAgICBpb24taXRlbSB7XG4gICAgICAgICAgICAtLW1pbi1oZWlnaHQ6IDM3cHg7XG4gICAgICAgICAgICBtYXJnaW4tbGVmdDogNHB4O1xuICAgICAgICAgICAgXG4gICAgICAgICAgICBtYXJnaW4tYm90dG9tOiA1cHg7XG4gICAgICAgICAgICBpb24taW5wdXQge1xuICAgICAgICAgICAgICAgIGNvbG9yOiAjMDAwO1xuICAgICAgICAgICAgICAgIHRleHQtYWxpZ246IHJpZ2h0O1xuXG4gICAgICAgICAgICAgICAgLS1wYWRkaW5nLXRvcDogMDtcbiAgICAgICAgICAgICAgICAtLXBhZGRpbmctYm90dG9tOiAwO1xuICAgICAgICAgICAgfVxuICAgICAgICB9XG5cblxuXG4gICAgfVxuXG4gICAgcCB7XG4gICAgICAgIG1hcmdpbi1ib3R0b206IDA7XG4gICAgICAgIGNvbG9yOiAjMzUzNTM1O1xuICAgICAgICBtYXJnaW4tdG9wOiAwO1xuICAgICAgICB0ZXh0LWFsaWduOiAtd2Via2l0LWNlbnRlcjtcbiAgICAgICAgYWxpZ24tc2VsZjogZmxleC1lbmQ7XG4gICAgICAgIHdpZHRoOiAxMDAlO1xuXG4gICAgfVxuXG4gICAgaDQge1xuICAgICAgICBtYXJnaW46IDA7XG5cbiAgICB9XG5cbiAgICBpb24tbGFiZWwge1xuICAgICAgICAvLyBtYXJnaW4tdG9wOiA1cHg7XG4gICAgICAgIG1hcmdpbi1sZWZ0OiAzcHg7XG4gICAgICAgIGNvbG9yOiAjMzUzNTM1O1xuICAgIH1cblxuICAgIC5yaWdodC1wIHtcbiAgICAgICAgYWxpZ24tc2VsZjogZmxleC1lbmQ7XG4gICAgfVxufVxuXG5cbmlvbi1jYXJkLnByb2dyYW0uYWN0aXZlIHtcbiAgICB0cmFuc2Zvcm06IHNjYWxlKDEuMSwgMS4xKTtcbiAgICB0cmFuc2l0aW9uOiBhbGwgMC41cyBlYXNlO1xuICAgIGJvcmRlcjogMnB4IHNvbGlkIHZhcigtLWlvbi1jb2xvci1wcmltYXJ5KTtcbiAgICB0cmFuc2l0aW9uOiBhbGwgMC41cyBlYXNlO1xufVxuXG4uc3dpcGVyLWNvbnRhaW5lciB7XG4gICAgb3ZlcmZsb3cteDogdmlzaWJsZTtcbiAgICBvdmVyZmxvdy15OiB2aXNpYmxlO1xufVxuXG5pb24tY2FyZC5wcm9ncmFtLmFjdGl2ZTpiZWZvcmUge1xuICAgIGNvbnRlbnQ6IFwiXCI7XG4gICAgLy8gYmFja2dyb3VuZDogdXJsKC4uLy4uLy4uL2Fzc2V0cy9pY29uL2NoZWNrbWFyay1jaXJjbGUtb3V0bGluZS5zdmcpO1xuICAgIGJhY2tncm91bmQ6IHVybCguLi8uLi8uLi8uLi9hc3NldHMvaWNvbi9jaGVja21hcmstY2lyY2xlLW91dGxpbmUuc3ZnKTtcblxuICAgIGhlaWdodDogMjVweDtcbiAgICB3aWR0aDogMjVweDtcbiAgICByaWdodDogNXB4O1xuICAgIHRvcDogNXB4O1xuICAgIHBvc2l0aW9uOiBhYnNvbHV0ZTtcbn1cblxuXG5cblxuLnNsaWRlLWltZyB7XG4gICAgbWF4LWhlaWdodDogMTAwcHggIWltcG9ydGFudDtcblxufVxuXG4udmFsaWRhdGVfYnRuIHtcbiAgICB3aWR0aDogMTAwJTtcbiAgICAtLWJhY2tncm91bmQ6IHZhcigtLWlvbi1idG4tY3VzdG9tLWNvbG9yKVxufVxuXG5cbmlvbi1zbGlkZSB7XG4gICAgb3ZlcmZsb3cteTogYXV0bztcbn1cblxuXG5pb24tZ3JpZCB7XG4gICAgcGFkZGluZy10b3A6IDA7XG59XG5cbi5zbGlkZV9jYXJkX2NvbnRlbnQge1xuICAgIGRpc3BsYXk6IGZsZXggIWltcG9ydGFudDtcbiAgICBqdXN0aWZ5LWNvbnRlbnQ6IGNlbnRlciAhaW1wb3J0YW50O1xufSJdfQ== */";
      /***/
    },

    /***/
    "./src/app/components/modals/check-up-modals/check-up-modals.component.ts":
    /*!********************************************************************************!*\
      !*** ./src/app/components/modals/check-up-modals/check-up-modals.component.ts ***!
      \********************************************************************************/

    /*! exports provided: CheckUpModalsComponent */

    /***/
    function srcAppComponentsModalsCheckUpModalsCheckUpModalsComponentTs(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "CheckUpModalsComponent", function () {
        return CheckUpModalsComponent;
      });
      /* harmony import */


      var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! tslib */
      "./node_modules/tslib/tslib.es6.js");
      /* harmony import */


      var _ionic_angular__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! @ionic/angular */
      "./node_modules/@ionic/angular/__ivy_ngcc__/fesm2015/ionic-angular.js");
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! @angular/core */
      "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
      /* harmony import */


      var hammerjs__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
      /*! hammerjs */
      "./node_modules/hammerjs/hammer.js");
      /* harmony import */


      var hammerjs__WEBPACK_IMPORTED_MODULE_3___default = /*#__PURE__*/__webpack_require__.n(hammerjs__WEBPACK_IMPORTED_MODULE_3__);

      var CheckUpModalsComponent = /*#__PURE__*/function () {
        function CheckUpModalsComponent(modalCtrl, gestureCtrl, element, renderer) {
          _classCallCheck(this, CheckUpModalsComponent);

          this.modalCtrl = modalCtrl;
          this.gestureCtrl = gestureCtrl;
          this.element = element;
          this.renderer = renderer;
          this.categories = {
            slidesPerView: 3,
            initialSlide: 0,
            effect: 'flip',
            slidesPerColumn: 1,
            slidesPerGroup: 1,
            watchSlidesProgress: true,
            loop: true
          };
          this.morphotypeArray = [{
            id: 1,
            src: "assets/icon/check-up-slide1.png",
            name: "First Slide",
            activeClass: false
          }, {
            id: 2,
            src: "assets/icon/check-up-slide2.png",
            name: "Second Slide",
            activeClass: false
          }, {
            id: 3,
            src: "assets/icon/check-up-slide3.png",
            name: "Third Slide",
            activeClass: false
          }];
        }

        _createClass(CheckUpModalsComponent, [{
          key: "ngOnInit",
          value: function ngOnInit() {}
        }, {
          key: "closeModal",
          value: function closeModal() {
            console.log("aa");
            this.modalCtrl.dismiss();
          }
        }, {
          key: "donsomething",
          value: function donsomething(event, src) {
            this.morphotypeArray.forEach(function (obj) {
              if (obj.id == src.id) {
                obj.activeClass = true;
              } else {
                obj.activeClass = false;
              }
            });
          }
        }, {
          key: "ngAfterViewInit",
          value: function ngAfterViewInit() {
            return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, /*#__PURE__*/regeneratorRuntime.mark(function _callee8() {
              var _this15 = this;

              var d, options, gesture;
              return regeneratorRuntime.wrap(function _callee8$(_context8) {
                while (1) {
                  switch (_context8.prev = _context8.next) {
                    case 0:
                      d = document.querySelector('ion-modal');
                      options = {
                        el: d,
                        direction: "y",
                        gestureName: 'swipe',
                        onStart: function onStart() {
                          _this15.renderer.setStyle(d, 'transition', 'none');
                        },
                        onMove: function onMove(ev) {
                          if (ev.deltaY > 0) {
                            _this15.renderer.setStyle(d, 'transform', "translateY(".concat(ev.deltaY, "px)"));
                          }
                        },
                        onEnd: function onEnd(ev) {
                          _this15.renderer.setStyle(d, 'transition', '0.5s ease-out');

                          if (ev.deltaY > -200) {
                            _this15.renderer.setStyle(d, 'transform', 'translateY(500px)');

                            _this15.modalCtrl.dismiss();
                          } else {
                            _this15.renderer.setStyle(d, 'transform', 'translateY(0px)');
                          }
                        }
                      };
                      _context8.next = 4;
                      return this.gestureCtrl.create(options);

                    case 4:
                      gesture = _context8.sent;
                      gesture.enable();

                    case 6:
                    case "end":
                      return _context8.stop();
                  }
                }
              }, _callee8, this);
            }));
          }
        }]);

        return CheckUpModalsComponent;
      }();

      CheckUpModalsComponent.ctorParameters = function () {
        return [{
          type: _ionic_angular__WEBPACK_IMPORTED_MODULE_1__["ModalController"]
        }, {
          type: _ionic_angular__WEBPACK_IMPORTED_MODULE_1__["GestureController"]
        }, {
          type: _angular_core__WEBPACK_IMPORTED_MODULE_2__["ElementRef"]
        }, {
          type: _angular_core__WEBPACK_IMPORTED_MODULE_2__["Renderer2"]
        }];
      };

      CheckUpModalsComponent = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([Object(_angular_core__WEBPACK_IMPORTED_MODULE_2__["Component"])({
        selector: 'app-check-up-modals',
        template: Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(
        /*! raw-loader!./check-up-modals.component.html */
        "./node_modules/raw-loader/dist/cjs.js!./src/app/components/modals/check-up-modals/check-up-modals.component.html"))["default"],
        styles: [Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(
        /*! ./check-up-modals.component.scss */
        "./src/app/components/modals/check-up-modals/check-up-modals.component.scss"))["default"]]
      })], CheckUpModalsComponent);
      /***/
    },

    /***/
    "./src/app/components/modals/click-add-excercise-modal/click-add-excercise-modal.component.scss":
    /*!******************************************************************************************************!*\
      !*** ./src/app/components/modals/click-add-excercise-modal/click-add-excercise-modal.component.scss ***!
      \******************************************************************************************************/

    /*! exports provided: default */

    /***/
    function srcAppComponentsModalsClickAddExcerciseModalClickAddExcerciseModalComponentScss(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony default export */


      __webpack_exports__["default"] = "ion-content {\n  --padding-start: 2%;\n  --padding-end: 2%;\n  --padding-top: 5%;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvY29tcG9uZW50cy9tb2RhbHMvY2xpY2stYWRkLWV4Y2VyY2lzZS1tb2RhbC9jbGljay1hZGQtZXhjZXJjaXNlLW1vZGFsLmNvbXBvbmVudC5zY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBO0VBQ0ksbUJBQUE7RUFDQSxpQkFBQTtFQUNBLGlCQUFBO0FBQ0oiLCJmaWxlIjoic3JjL2FwcC9jb21wb25lbnRzL21vZGFscy9jbGljay1hZGQtZXhjZXJjaXNlLW1vZGFsL2NsaWNrLWFkZC1leGNlcmNpc2UtbW9kYWwuY29tcG9uZW50LnNjc3MiLCJzb3VyY2VzQ29udGVudCI6WyJpb24tY29udGVudCB7XG4gICAgLS1wYWRkaW5nLXN0YXJ0OiAyJTtcbiAgICAtLXBhZGRpbmctZW5kOiAyJTtcbiAgICAtLXBhZGRpbmctdG9wOiA1JTtcbn0iXX0= */";
      /***/
    },

    /***/
    "./src/app/components/modals/click-add-excercise-modal/click-add-excercise-modal.component.ts":
    /*!****************************************************************************************************!*\
      !*** ./src/app/components/modals/click-add-excercise-modal/click-add-excercise-modal.component.ts ***!
      \****************************************************************************************************/

    /*! exports provided: ClickAddExcerciseModalComponent */

    /***/
    function srcAppComponentsModalsClickAddExcerciseModalClickAddExcerciseModalComponentTs(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "ClickAddExcerciseModalComponent", function () {
        return ClickAddExcerciseModalComponent;
      });
      /* harmony import */


      var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! tslib */
      "./node_modules/tslib/tslib.es6.js");
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! @angular/core */
      "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
      /* harmony import */


      var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! @angular/router */
      "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");
      /* harmony import */


      var _ionic_angular__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
      /*! @ionic/angular */
      "./node_modules/@ionic/angular/__ivy_ngcc__/fesm2015/ionic-angular.js");

      var ClickAddExcerciseModalComponent = /*#__PURE__*/function () {
        function ClickAddExcerciseModalComponent(modal, router) {
          _classCallCheck(this, ClickAddExcerciseModalComponent);

          this.modal = modal;
          this.router = router;
          this.typeOfSupersetBtnArray = "excercise_de_force";
          this.yesNoBtnClick = "";
        }

        _createClass(ClickAddExcerciseModalComponent, [{
          key: "ngOnInit",
          value: function ngOnInit() {}
        }, {
          key: "yesClick",
          value: function yesClick() {
            this.yesNoBtnClick = 'yes';
            this.modal.dismiss();
          }
        }, {
          key: "noClick",
          value: function noClick() {
            this.yesNoBtnClick = 'no';
            this.modal.dismiss();
          }
        }, {
          key: "goToNext",
          value: function goToNext() {
            this.router.navigate(['/add-exercise']);
            this.modal.dismiss();
          }
        }]);

        return ClickAddExcerciseModalComponent;
      }();

      ClickAddExcerciseModalComponent.ctorParameters = function () {
        return [{
          type: _ionic_angular__WEBPACK_IMPORTED_MODULE_3__["ModalController"]
        }, {
          type: _angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"]
        }];
      };

      ClickAddExcerciseModalComponent = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-click-add-excercise-modal',
        template: Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(
        /*! raw-loader!./click-add-excercise-modal.component.html */
        "./node_modules/raw-loader/dist/cjs.js!./src/app/components/modals/click-add-excercise-modal/click-add-excercise-modal.component.html"))["default"],
        styles: [Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(
        /*! ./click-add-excercise-modal.component.scss */
        "./src/app/components/modals/click-add-excercise-modal/click-add-excercise-modal.component.scss"))["default"]]
      })], ClickAddExcerciseModalComponent);
      /***/
    },

    /***/
    "./src/app/components/modals/coach-chat-modal/coach-chat-modal.component.scss":
    /*!************************************************************************************!*\
      !*** ./src/app/components/modals/coach-chat-modal/coach-chat-modal.component.scss ***!
      \************************************************************************************/

    /*! exports provided: default */

    /***/
    function srcAppComponentsModalsCoachChatModalCoachChatModalComponentScss(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony default export */


      __webpack_exports__["default"] = "ion-content {\n  --padding-start: 3%;\n  --padding-end: 3%;\n}\nion-content ion-text {\n  display: block;\n}\nion-toolbar {\n  border-bottom: 1px solid #f1f1f1;\n  height: 9%;\n}\n.close-btn-icon {\n  font-size: 29px;\n  font-weight: 700;\n}\n.next {\n  --background: var(--ion-btn-custom-color);\n  font-size: 7px;\n}\n.header-button-box {\n  display: flex;\n  z-index: 999;\n  margin-top: -20px;\n  color: #fff;\n  width: 100%;\n}\n.header-button-box-button-right {\n  right: 0;\n  top: -25px;\n  position: absolute;\n  z-index: 999;\n}\n.header-button-box-button-right .header-button-box-button {\n  display: inline-block;\n  margin-right: 15px;\n  height: 35px;\n  width: 35px;\n  background: #fff;\n  border-radius: 40px;\n  padding: 5px 5px 0px 6px;\n  border: 2px solid #ffd1b3;\n}\n.chat-profile ion-item {\n  --inner-border-width: 0 0 0px 0;\n}\n.chat-input {\n  --border-width: 0 0 0px 0;\n}\n.chat-screen-box ion-item {\n  --inner-border-width: 0 0 0px 0;\n  margin-bottom: 15px;\n}\n.chat-screen-box ion-label {\n  white-space: normal;\n}\n.chat-text-user {\n  background: #e8f8ff;\n  padding: 0px 15px;\n  border-radius: 5px;\n}\n.chat-text-custemer {\n  background: #adf;\n  padding: 0px 15px;\n  border-radius: 5px;\n}\n.seendate {\n  position: relative;\n  margin-bottom: 15px;\n}\n.seendate:before {\n  content: \"\";\n  position: absolute;\n  height: 1px;\n  background: #c1c1c1;\n  left: 0;\n  width: 100%;\n  top: 14px;\n  z-index: -1;\n}\n.seendate span {\n  background: #ffffff;\n  z-index: 2;\n  padding: 0px 15px;\n  font-size: 12px;\n}\n.startchartdate {\n  font-size: 12px;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvY29tcG9uZW50cy9tb2RhbHMvY29hY2gtY2hhdC1tb2RhbC9jb2FjaC1jaGF0LW1vZGFsLmNvbXBvbmVudC5zY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBO0VBQ0ksbUJBQUE7RUFDQSxpQkFBQTtBQUNKO0FBQ0k7RUFDSSxjQUFBO0FBQ1I7QUFHQTtFQUNJLGdDQUFBO0VBQ0EsVUFBQTtBQUFKO0FBR0E7RUFDSSxlQUFBO0VBQ0EsZ0JBQUE7QUFBSjtBQUVBO0VBQ0kseUNBQUE7RUFDSSxjQUFBO0FBQ1I7QUFDQTtFQUNJLGFBQUE7RUFDQSxZQUFBO0VBQ0EsaUJBQUE7RUFDQSxXQUFBO0VBQ0EsV0FBQTtBQUVKO0FBQ0E7RUFDSSxRQUFBO0VBQ0EsVUFBQTtFQUNBLGtCQUFBO0VBQ0EsWUFBQTtBQUVKO0FBQ0E7RUFDSSxxQkFBQTtFQUNBLGtCQUFBO0VBQ0EsWUFBQTtFQUNBLFdBQUE7RUFDQSxnQkFBQTtFQUNBLG1CQUFBO0VBQ0Esd0JBQUE7RUFDQSx5QkFBQTtBQUVKO0FBQ0E7RUFDSSwrQkFBQTtBQUVKO0FBQ0E7RUFDSSx5QkFBQTtBQUVKO0FBQ0E7RUFDSSwrQkFBQTtFQUNBLG1CQUFBO0FBRUo7QUFDQTtFQUNJLG1CQUFBO0FBRUo7QUFDQTtFQUNJLG1CQUFBO0VBQ0EsaUJBQUE7RUFDQSxrQkFBQTtBQUVKO0FBQ0E7RUFDSSxnQkFBQTtFQUNBLGlCQUFBO0VBQ0Esa0JBQUE7QUFFSjtBQUNBO0VBQ0ksa0JBQUE7RUFDQSxtQkFBQTtBQUVKO0FBQ0E7RUFDSSxXQUFBO0VBQ0Esa0JBQUE7RUFDQSxXQUFBO0VBQ0EsbUJBQUE7RUFDQSxPQUFBO0VBQ0EsV0FBQTtFQUNBLFNBQUE7RUFDQSxXQUFBO0FBRUo7QUFDQTtFQUNJLG1CQUFBO0VBQ0EsVUFBQTtFQUNBLGlCQUFBO0VBQ0EsZUFBQTtBQUVKO0FBQ0E7RUFDSSxlQUFBO0FBRUoiLCJmaWxlIjoic3JjL2FwcC9jb21wb25lbnRzL21vZGFscy9jb2FjaC1jaGF0LW1vZGFsL2NvYWNoLWNoYXQtbW9kYWwuY29tcG9uZW50LnNjc3MiLCJzb3VyY2VzQ29udGVudCI6WyJpb24tY29udGVudCB7XG4gICAgLS1wYWRkaW5nLXN0YXJ0OiAzJTtcbiAgICAtLXBhZGRpbmctZW5kOiAzJTsgIFxuXG4gICAgaW9uLXRleHQge1xuICAgICAgICBkaXNwbGF5OiBibG9jaztcbiAgICB9XG59XG5cbmlvbi10b29sYmFyIHtcbiAgICBib3JkZXItYm90dG9tOiAxcHggc29saWQgI2YxZjFmMTtcbiAgICBoZWlnaHQ6IDklO1xufVxuXG4uY2xvc2UtYnRuLWljb24ge1xuICAgIGZvbnQtc2l6ZTogMjlweDtcbiAgICBmb250LXdlaWdodDogNzAwO1xufVxuLm5leHQge1xuICAgIC0tYmFja2dyb3VuZDogdmFyKC0taW9uLWJ0bi1jdXN0b20tY29sb3IpO1xuICAgICAgICBmb250LXNpemU6IDdweDtcbn1cbi5oZWFkZXItYnV0dG9uLWJveCB7XG4gICAgZGlzcGxheTogZmxleDtcbiAgICB6LWluZGV4OiA5OTk7XG4gICAgbWFyZ2luLXRvcDogLTIwcHg7XG4gICAgY29sb3I6ICNmZmY7XG4gICAgd2lkdGg6IDEwMCU7XG59XG5cbi5oZWFkZXItYnV0dG9uLWJveC1idXR0b24tcmlnaHQge1xuICAgIHJpZ2h0OiAwO1xuICAgIHRvcDogLTI1cHg7XG4gICAgcG9zaXRpb246IGFic29sdXRlO1xuICAgIHotaW5kZXg6IDk5OTtcbn1cblxuLmhlYWRlci1idXR0b24tYm94LWJ1dHRvbi1yaWdodCAuaGVhZGVyLWJ1dHRvbi1ib3gtYnV0dG9uIHtcbiAgICBkaXNwbGF5OiBpbmxpbmUtYmxvY2s7XG4gICAgbWFyZ2luLXJpZ2h0OiAxNXB4O1xuICAgIGhlaWdodDogMzVweDtcbiAgICB3aWR0aDogMzVweDtcbiAgICBiYWNrZ3JvdW5kOiAjZmZmO1xuICAgIGJvcmRlci1yYWRpdXM6IDQwcHg7XG4gICAgcGFkZGluZzogNXB4IDVweCAwcHggNnB4O1xuICAgIGJvcmRlcjogMnB4IHNvbGlkICNmZmQxYjM7XG59XG5cbi5jaGF0LXByb2ZpbGUgaW9uLWl0ZW0ge1xuICAgIC0taW5uZXItYm9yZGVyLXdpZHRoOiAwIDAgMHB4IDA7XG59XG5cbi5jaGF0LWlucHV0IHtcbiAgICAtLWJvcmRlci13aWR0aDogMCAwIDBweCAwO1xufVxuXG4uY2hhdC1zY3JlZW4tYm94IGlvbi1pdGVtIHtcbiAgICAtLWlubmVyLWJvcmRlci13aWR0aDogMCAwIDBweCAwO1xuICAgIG1hcmdpbi1ib3R0b206IDE1cHg7XG59XG5cbi5jaGF0LXNjcmVlbi1ib3ggaW9uLWxhYmVsIHtcbiAgICB3aGl0ZS1zcGFjZTogbm9ybWFsO1xufVxuXG4uY2hhdC10ZXh0LXVzZXIge1xuICAgIGJhY2tncm91bmQ6ICNlOGY4ZmY7XG4gICAgcGFkZGluZzogMHB4IDE1cHg7XG4gICAgYm9yZGVyLXJhZGl1czogNXB4O1xufVxuXG4uY2hhdC10ZXh0LWN1c3RlbWVyIHtcbiAgICBiYWNrZ3JvdW5kOiAjYWRmO1xuICAgIHBhZGRpbmc6IDBweCAxNXB4O1xuICAgIGJvcmRlci1yYWRpdXM6IDVweDtcbn1cblxuLnNlZW5kYXRlIHtcbiAgICBwb3NpdGlvbjogcmVsYXRpdmU7XG4gICAgbWFyZ2luLWJvdHRvbTogMTVweDtcbn1cblxuLnNlZW5kYXRlOmJlZm9yZSB7XG4gICAgY29udGVudDogXCJcIjtcbiAgICBwb3NpdGlvbjogYWJzb2x1dGU7XG4gICAgaGVpZ2h0OiAxcHg7XG4gICAgYmFja2dyb3VuZDogI2MxYzFjMTtcbiAgICBsZWZ0OiAwO1xuICAgIHdpZHRoOiAxMDAlO1xuICAgIHRvcDogMTRweDtcbiAgICB6LWluZGV4OiAtMTtcbn1cblxuLnNlZW5kYXRlIHNwYW4ge1xuICAgIGJhY2tncm91bmQ6ICNmZmZmZmY7XG4gICAgei1pbmRleDogMjtcbiAgICBwYWRkaW5nOiAwcHggMTVweDtcbiAgICBmb250LXNpemU6IDEycHg7XG59XG5cbi5zdGFydGNoYXJ0ZGF0ZSB7XG4gICAgZm9udC1zaXplOiAxMnB4O1xufSJdfQ== */";
      /***/
    },

    /***/
    "./src/app/components/modals/coach-chat-modal/coach-chat-modal.component.ts":
    /*!**********************************************************************************!*\
      !*** ./src/app/components/modals/coach-chat-modal/coach-chat-modal.component.ts ***!
      \**********************************************************************************/

    /*! exports provided: CoachChatModalComponent */

    /***/
    function srcAppComponentsModalsCoachChatModalCoachChatModalComponentTs(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "CoachChatModalComponent", function () {
        return CoachChatModalComponent;
      });
      /* harmony import */


      var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! tslib */
      "./node_modules/tslib/tslib.es6.js");
      /* harmony import */


      var _ionic_angular__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! @ionic/angular */
      "./node_modules/@ionic/angular/__ivy_ngcc__/fesm2015/ionic-angular.js");
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! @angular/core */
      "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
      /* harmony import */


      var hammerjs__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
      /*! hammerjs */
      "./node_modules/hammerjs/hammer.js");
      /* harmony import */


      var hammerjs__WEBPACK_IMPORTED_MODULE_3___default = /*#__PURE__*/__webpack_require__.n(hammerjs__WEBPACK_IMPORTED_MODULE_3__);
      /* harmony import */


      var _components_modals_coach_checkup_modal_coach_checkup_modal_component__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
      /*! @components/modals/coach-checkup-modal/coach-checkup-modal.component */
      "./src/app/components/modals/coach-checkup-modal/coach-checkup-modal.component.ts");

      var CoachChatModalComponent = /*#__PURE__*/function () {
        function CoachChatModalComponent(modalCtrl, gestureCtrl, element, renderer) {
          _classCallCheck(this, CoachChatModalComponent);

          this.modalCtrl = modalCtrl;
          this.gestureCtrl = gestureCtrl;
          this.element = element;
          this.renderer = renderer;
        }

        _createClass(CoachChatModalComponent, [{
          key: "ngOnInit",
          value: function ngOnInit() {}
        }, {
          key: "closeModal",
          value: function closeModal() {
            this.modalCtrl.dismiss();
          }
        }, {
          key: "openCheckUp",
          value: function openCheckUp() {
            return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, /*#__PURE__*/regeneratorRuntime.mark(function _callee9() {
              var modal;
              return regeneratorRuntime.wrap(function _callee9$(_context9) {
                while (1) {
                  switch (_context9.prev = _context9.next) {
                    case 0:
                      _context9.next = 2;
                      return this.modalCtrl.create({
                        component: _components_modals_coach_checkup_modal_coach_checkup_modal_component__WEBPACK_IMPORTED_MODULE_4__["CoachCheckupModalComponent"],
                        cssClass: 'coach-checkup-modal',
                        showBackdrop: false
                      });

                    case 2:
                      modal = _context9.sent;
                      _context9.next = 5;
                      return modal.present();

                    case 5:
                      return _context9.abrupt("return", _context9.sent);

                    case 6:
                    case "end":
                      return _context9.stop();
                  }
                }
              }, _callee9, this);
            }));
          }
        }, {
          key: "ngAfterViewInit",
          value: function ngAfterViewInit() {
            return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, /*#__PURE__*/regeneratorRuntime.mark(function _callee10() {
              var _this16 = this;

              var d, options, gesture;
              return regeneratorRuntime.wrap(function _callee10$(_context10) {
                while (1) {
                  switch (_context10.prev = _context10.next) {
                    case 0:
                      d = document.querySelector('ion-modal');
                      options = {
                        el: d,
                        direction: "y",
                        gestureName: 'swipe',
                        onStart: function onStart() {
                          _this16.renderer.setStyle(d, 'transition', 'none');
                        },
                        onMove: function onMove(ev) {
                          if (ev.deltaY > 0) {
                            _this16.renderer.setStyle(d, 'transform', "translateY(".concat(ev.deltaY, "px)"));
                          }
                        },
                        onEnd: function onEnd(ev) {
                          _this16.renderer.setStyle(d, 'transition', '0.5s ease-out');

                          if (ev.deltaY > -200) {
                            _this16.renderer.setStyle(d, 'transform', 'translateY(500px)');

                            _this16.modalCtrl.dismiss();
                          } else {
                            _this16.renderer.setStyle(d, 'transform', 'translateY(0px)');
                          }
                        }
                      };
                      _context10.next = 4;
                      return this.gestureCtrl.create(options);

                    case 4:
                      gesture = _context10.sent;
                      gesture.enable();

                    case 6:
                    case "end":
                      return _context10.stop();
                  }
                }
              }, _callee10, this);
            }));
          }
        }]);

        return CoachChatModalComponent;
      }();

      CoachChatModalComponent.ctorParameters = function () {
        return [{
          type: _ionic_angular__WEBPACK_IMPORTED_MODULE_1__["ModalController"]
        }, {
          type: _ionic_angular__WEBPACK_IMPORTED_MODULE_1__["GestureController"]
        }, {
          type: _angular_core__WEBPACK_IMPORTED_MODULE_2__["ElementRef"]
        }, {
          type: _angular_core__WEBPACK_IMPORTED_MODULE_2__["Renderer2"]
        }];
      };

      CoachChatModalComponent = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([Object(_angular_core__WEBPACK_IMPORTED_MODULE_2__["Component"])({
        selector: 'app-coach-chat-modal',
        template: Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(
        /*! raw-loader!./coach-chat-modal.component.html */
        "./node_modules/raw-loader/dist/cjs.js!./src/app/components/modals/coach-chat-modal/coach-chat-modal.component.html"))["default"],
        styles: [Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(
        /*! ./coach-chat-modal.component.scss */
        "./src/app/components/modals/coach-chat-modal/coach-chat-modal.component.scss"))["default"]]
      })], CoachChatModalComponent);
      /***/
    },

    /***/
    "./src/app/components/modals/coach-checkup-modal/coach-checkup-modal.component.scss":
    /*!******************************************************************************************!*\
      !*** ./src/app/components/modals/coach-checkup-modal/coach-checkup-modal.component.scss ***!
      \******************************************************************************************/

    /*! exports provided: default */

    /***/
    function srcAppComponentsModalsCoachCheckupModalCoachCheckupModalComponentScss(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony default export */


      __webpack_exports__["default"] = "ion-content {\n  --padding-start: 3%;\n  --padding-end: 3%;\n}\nion-content ion-text {\n  display: block;\n}\nion-toolbar {\n  border-bottom: 1px solid #f1f1f1;\n  height: 9%;\n}\n.close-btn-icon {\n  font-size: 29px;\n  font-weight: 700;\n}\nion-card {\n  margin: 0;\n}\n.check-out-big-cards {\n  display: flex;\n  flex-direction: column;\n  align-items: flex-end;\n}\n.check-out-big-cards div {\n  display: flex;\n  flex-direction: row;\n  margin-top: 10px;\n  margin-bottom: 10px;\n}\n.check-out-big-cards p {\n  margin-bottom: 0;\n  color: #353535;\n}\n.check-out-big-cards h4 {\n  margin: 0;\n}\n.check-out-big-cards ion-label {\n  margin-top: 5px;\n  margin-left: 3px;\n  color: #353535;\n}\nion-card.program {\n  width: 100%;\n  margin-inline: 5px;\n  margin: 20px 0px;\n}\n.check-out-small-cards {\n  display: flex;\n  flex-direction: column;\n  align-items: flex-end;\n  width: 88%;\n  height: 70px;\n  justify-content: center;\n  padding-top: 5px;\n}\n.check-out-small-cards .small_card_title {\n  display: flex;\n  flex-direction: column;\n  margin: 0;\n  align-self: center;\n}\n.check-out-small-cards .small_card_desc {\n  display: flex;\n  flex-direction: row;\n  margin-bottom: 10px;\n  align-self: center;\n}\n.check-out-small-cards p {\n  margin-bottom: 0;\n  color: #353535;\n  margin-top: 0;\n  text-align: -webkit-center;\n  align-self: flex-end;\n  margin-right: 15%;\n  width: 55%;\n}\n.check-out-small-cards h4 {\n  margin: 0;\n}\n.check-out-small-cards ion-label {\n  margin-left: 3px;\n  color: #353535;\n}\n.check-out-small-cards .right-p {\n  align-self: flex-end;\n}\nion-card.program.active {\n  transform: scale(1.1, 1.1);\n  transition: all 0.5s ease;\n  border: 2px solid var(--ion-color-primary);\n  transition: all 0.5s ease;\n}\n.swiper-container {\n  overflow-x: visible;\n  overflow-y: visible;\n}\nion-card.program.active:before {\n  content: \"\";\n  background: url('checkmark-circle-outline.svg');\n  height: 25px;\n  width: 25px;\n  right: 5px;\n  top: 5px;\n  position: absolute;\n}\n.slide-img {\n  max-height: 100px !important;\n}\n.validate_btn {\n  width: 100%;\n  --background: var(--ion-btn-custom-color) ;\n}\nion-slide {\n  overflow-y: auto;\n}\n.daybox {\n  text-align: center;\n  color: #fff;\n  margin-top: 15px;\n}\n.daybox-title {\n  display: block;\n  min-width: 100px;\n  background: #626262;\n  padding: 3px 20px;\n  border-radius: 25px;\n  border: 2px solid #333;\n  z-index: 1;\n  position: relative;\n}\n.back-icon {\n  position: absolute;\n  top: 30px;\n  left: 95px;\n  z-index: 2;\n}\n.forward-icon {\n  position: absolute;\n  right: 95px;\n  top: 30px;\n  z-index: 2;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvY29tcG9uZW50cy9tb2RhbHMvY29hY2gtY2hlY2t1cC1tb2RhbC9jb2FjaC1jaGVja3VwLW1vZGFsLmNvbXBvbmVudC5zY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBO0VBQ0ksbUJBQUE7RUFDQSxpQkFBQTtBQUNKO0FBQ0k7RUFDSSxjQUFBO0FBQ1I7QUFHQTtFQUNJLGdDQUFBO0VBQ0EsVUFBQTtBQUFKO0FBR0E7RUFDSSxlQUFBO0VBQ0EsZ0JBQUE7QUFBSjtBQUVBO0VBQ0ksU0FBQTtBQUNKO0FBRUE7RUFDSSxhQUFBO0VBQ0Esc0JBQUE7RUFDQSxxQkFBQTtBQUNKO0FBQ0k7RUFDSSxhQUFBO0VBQ0EsbUJBQUE7RUFDQSxnQkFBQTtFQUNBLG1CQUFBO0FBQ1I7QUFFSTtFQUNJLGdCQUFBO0VBQ0EsY0FBQTtBQUFSO0FBR0k7RUFDSSxTQUFBO0FBRFI7QUFLSTtFQUNJLGVBQUE7RUFDQSxnQkFBQTtFQUNBLGNBQUE7QUFIUjtBQU9BO0VBQ0ksV0FBQTtFQUNBLGtCQUFBO0VBQ0EsZ0JBQUE7QUFKSjtBQU9BO0VBQ0ksYUFBQTtFQUNBLHNCQUFBO0VBQ0EscUJBQUE7RUFDQSxVQUFBO0VBQ0EsWUFBQTtFQUNBLHVCQUFBO0VBQ0EsZ0JBQUE7QUFKSjtBQU1JO0VBQ0ksYUFBQTtFQUNBLHNCQUFBO0VBQ0EsU0FBQTtFQUNBLGtCQUFBO0FBSlI7QUFNSTtFQUNJLGFBQUE7RUFDQSxtQkFBQTtFQUVBLG1CQUFBO0VBQ0Esa0JBQUE7QUFMUjtBQVFJO0VBQ0ksZ0JBQUE7RUFDQSxjQUFBO0VBQ0EsYUFBQTtFQUNBLDBCQUFBO0VBQ0Qsb0JBQUE7RUFDQSxpQkFBQTtFQUNBLFVBQUE7QUFOUDtBQVVJO0VBQ0ksU0FBQTtBQVJSO0FBWUk7RUFFSSxnQkFBQTtFQUNBLGNBQUE7QUFYUjtBQWNJO0VBQ0ksb0JBQUE7QUFaUjtBQWlCQTtFQUNJLDBCQUFBO0VBQ0EseUJBQUE7RUFDQSwwQ0FBQTtFQUNBLHlCQUFBO0FBZEo7QUFpQkE7RUFDSSxtQkFBQTtFQUNBLG1CQUFBO0FBZEo7QUFpQkE7RUFDSSxXQUFBO0VBRUEsK0NBQUE7RUFFQSxZQUFBO0VBQ0EsV0FBQTtFQUNBLFVBQUE7RUFDQSxRQUFBO0VBQ0Esa0JBQUE7QUFoQko7QUFzQkE7RUFDSSw0QkFBQTtBQW5CSjtBQXVCQTtFQUNJLFdBQUE7RUFDQSwwQ0FBQTtBQXBCSjtBQXlCQTtFQUNJLGdCQUFBO0FBdEJKO0FBd0JBO0VBQ0ksa0JBQUE7RUFDQSxXQUFBO0VBQ0EsZ0JBQUE7QUFyQko7QUF3QkE7RUFDSSxjQUFBO0VBQ0EsZ0JBQUE7RUFDQSxtQkFBQTtFQUNBLGlCQUFBO0VBQ0EsbUJBQUE7RUFDQSxzQkFBQTtFQUNBLFVBQUE7RUFDQSxrQkFBQTtBQXJCSjtBQXdCQTtFQUNJLGtCQUFBO0VBQ0EsU0FBQTtFQUNBLFVBQUE7RUFDQSxVQUFBO0FBckJKO0FBd0JBO0VBQ0ksa0JBQUE7RUFDQSxXQUFBO0VBQ0EsU0FBQTtFQUNBLFVBQUE7QUFyQkoiLCJmaWxlIjoic3JjL2FwcC9jb21wb25lbnRzL21vZGFscy9jb2FjaC1jaGVja3VwLW1vZGFsL2NvYWNoLWNoZWNrdXAtbW9kYWwuY29tcG9uZW50LnNjc3MiLCJzb3VyY2VzQ29udGVudCI6WyJpb24tY29udGVudCB7XG4gICAgLS1wYWRkaW5nLXN0YXJ0OiAzJTtcbiAgICAtLXBhZGRpbmctZW5kOiAzJTsgIFxuXG4gICAgaW9uLXRleHQge1xuICAgICAgICBkaXNwbGF5OiBibG9jaztcbiAgICB9XG59XG5cbmlvbi10b29sYmFyIHtcbiAgICBib3JkZXItYm90dG9tOiAxcHggc29saWQgI2YxZjFmMTtcbiAgICBoZWlnaHQ6IDklO1xufVxuXG4uY2xvc2UtYnRuLWljb24ge1xuICAgIGZvbnQtc2l6ZTogMjlweDtcbiAgICBmb250LXdlaWdodDogNzAwO1xufVxuaW9uLWNhcmQge1xuICAgIG1hcmdpbjogMDtcbn1cblxuLmNoZWNrLW91dC1iaWctY2FyZHMge1xuICAgIGRpc3BsYXk6IGZsZXg7XG4gICAgZmxleC1kaXJlY3Rpb246IGNvbHVtbjtcbiAgICBhbGlnbi1pdGVtczogZmxleC1lbmQ7XG5cbiAgICBkaXYge1xuICAgICAgICBkaXNwbGF5OiBmbGV4O1xuICAgICAgICBmbGV4LWRpcmVjdGlvbjogcm93O1xuICAgICAgICBtYXJnaW4tdG9wOiAxMHB4O1xuICAgICAgICBtYXJnaW4tYm90dG9tOiAxMHB4O1xuICAgIH1cblxuICAgIHAge1xuICAgICAgICBtYXJnaW4tYm90dG9tOiAwO1xuICAgICAgICBjb2xvcjogIzM1MzUzNTtcbiAgICB9XG5cbiAgICBoNCB7XG4gICAgICAgIG1hcmdpbjogMDtcblxuICAgIH1cblxuICAgIGlvbi1sYWJlbCB7XG4gICAgICAgIG1hcmdpbi10b3A6IDVweDtcbiAgICAgICAgbWFyZ2luLWxlZnQ6IDNweDtcbiAgICAgICAgY29sb3I6ICMzNTM1MzU7XG4gICAgfVxufVxuXG5pb24tY2FyZC5wcm9ncmFtIHtcbiAgICB3aWR0aDogMTAwJTtcbiAgICBtYXJnaW4taW5saW5lOiA1cHg7XG4gICAgbWFyZ2luOiAyMHB4IDBweDtcbn1cblxuLmNoZWNrLW91dC1zbWFsbC1jYXJkcyB7XG4gICAgZGlzcGxheTogZmxleDtcbiAgICBmbGV4LWRpcmVjdGlvbjogY29sdW1uO1xuICAgIGFsaWduLWl0ZW1zOiBmbGV4LWVuZDtcbiAgICB3aWR0aDogODglO1xuICAgIGhlaWdodDogNzBweDtcbiAgICBqdXN0aWZ5LWNvbnRlbnQ6IGNlbnRlcjtcbiAgICBwYWRkaW5nLXRvcDogNXB4O1xuXG4gICAgLnNtYWxsX2NhcmRfdGl0bGUge1xuICAgICAgICBkaXNwbGF5OiBmbGV4O1xuICAgICAgICBmbGV4LWRpcmVjdGlvbjogY29sdW1uO1xuICAgICAgICBtYXJnaW46IDA7XG4gICAgICAgIGFsaWduLXNlbGY6IGNlbnRlcjtcbiAgICB9XG4gICAgLnNtYWxsX2NhcmRfZGVzYyB7XG4gICAgICAgIGRpc3BsYXk6IGZsZXg7XG4gICAgICAgIGZsZXgtZGlyZWN0aW9uOiByb3c7XG4gICAgICAgIC8vIG1hcmdpbi10b3A6IDEwcHg7XG4gICAgICAgIG1hcmdpbi1ib3R0b206IDEwcHg7XG4gICAgICAgIGFsaWduLXNlbGY6IGNlbnRlcjtcbiAgICB9XG5cbiAgICBwIHtcbiAgICAgICAgbWFyZ2luLWJvdHRvbTogMDtcbiAgICAgICAgY29sb3I6ICMzNTM1MzU7XG4gICAgICAgIG1hcmdpbi10b3A6IDA7XG4gICAgICAgIHRleHQtYWxpZ246IC13ZWJraXQtY2VudGVyO1xuICAgICAgIGFsaWduLXNlbGY6IGZsZXgtZW5kO1xuICAgICAgIG1hcmdpbi1yaWdodDogMTUlO1xuICAgICAgIHdpZHRoOiA1NSU7XG5cbiAgICB9XG5cbiAgICBoNCB7XG4gICAgICAgIG1hcmdpbjogMDtcblxuICAgIH1cblxuICAgIGlvbi1sYWJlbCB7XG4gICAgICAgIC8vIG1hcmdpbi10b3A6IDVweDtcbiAgICAgICAgbWFyZ2luLWxlZnQ6IDNweDtcbiAgICAgICAgY29sb3I6ICMzNTM1MzU7XG4gICAgfVxuXG4gICAgLnJpZ2h0LXAge1xuICAgICAgICBhbGlnbi1zZWxmOiBmbGV4LWVuZDtcbiAgICB9XG59XG5cblxuaW9uLWNhcmQucHJvZ3JhbS5hY3RpdmUge1xuICAgIHRyYW5zZm9ybTogc2NhbGUoMS4xLCAxLjEpO1xuICAgIHRyYW5zaXRpb246IGFsbCAwLjVzIGVhc2U7XG4gICAgYm9yZGVyOiAycHggc29saWQgdmFyKC0taW9uLWNvbG9yLXByaW1hcnkpO1xuICAgIHRyYW5zaXRpb246IGFsbCAwLjVzIGVhc2U7XG59XG5cbi5zd2lwZXItY29udGFpbmVyIHtcbiAgICBvdmVyZmxvdy14OiB2aXNpYmxlO1xuICAgIG92ZXJmbG93LXk6IHZpc2libGU7XG59XG5cbmlvbi1jYXJkLnByb2dyYW0uYWN0aXZlOmJlZm9yZSB7XG4gICAgY29udGVudDogXCJcIjtcbiAgICAvLyBiYWNrZ3JvdW5kOiB1cmwoLi4vLi4vLi4vYXNzZXRzL2ljb24vY2hlY2ttYXJrLWNpcmNsZS1vdXRsaW5lLnN2Zyk7XG4gICAgYmFja2dyb3VuZDogdXJsKC4uLy4uLy4uLy4uL2Fzc2V0cy9pY29uL2NoZWNrbWFyay1jaXJjbGUtb3V0bGluZS5zdmcpO1xuXG4gICAgaGVpZ2h0OiAyNXB4O1xuICAgIHdpZHRoOiAyNXB4O1xuICAgIHJpZ2h0OiA1cHg7XG4gICAgdG9wOiA1cHg7XG4gICAgcG9zaXRpb246IGFic29sdXRlO1xufVxuXG5cblxuXG4uc2xpZGUtaW1nIHtcbiAgICBtYXgtaGVpZ2h0OiAxMDBweCAhaW1wb3J0YW50O1xuXG59XG5cbi52YWxpZGF0ZV9idG4ge1xuICAgIHdpZHRoOiAxMDAlO1xuICAgIC0tYmFja2dyb3VuZDogdmFyKC0taW9uLWJ0bi1jdXN0b20tY29sb3IpXG4gICAgXG59XG5cblxuaW9uLXNsaWRlIHtcbiAgICBvdmVyZmxvdy15OiBhdXRvO1xufVxuLmRheWJveCB7XG4gICAgdGV4dC1hbGlnbjogY2VudGVyO1xuICAgIGNvbG9yOiAjZmZmO1xuICAgIG1hcmdpbi10b3A6IDE1cHg7XG59XG5cbi5kYXlib3gtdGl0bGUge1xuICAgIGRpc3BsYXk6IGJsb2NrO1xuICAgIG1pbi13aWR0aDogMTAwcHg7XG4gICAgYmFja2dyb3VuZDogIzYyNjI2MjtcbiAgICBwYWRkaW5nOiAzcHggMjBweDtcbiAgICBib3JkZXItcmFkaXVzOiAyNXB4O1xuICAgIGJvcmRlcjogMnB4IHNvbGlkICMzMzM7XG4gICAgei1pbmRleDogMTtcbiAgICBwb3NpdGlvbjogcmVsYXRpdmU7XG59XG5cbi5iYWNrLWljb24ge1xuICAgIHBvc2l0aW9uOiBhYnNvbHV0ZTtcbiAgICB0b3A6IDMwcHg7XG4gICAgbGVmdDogOTVweDtcbiAgICB6LWluZGV4OiAyO1xufVxuXG4uZm9yd2FyZC1pY29uIHtcbiAgICBwb3NpdGlvbjogYWJzb2x1dGU7XG4gICAgcmlnaHQ6IDk1cHg7XG4gICAgdG9wOiAzMHB4O1xuICAgIHotaW5kZXg6IDI7XG59Il19 */";
      /***/
    },

    /***/
    "./src/app/components/modals/coach-checkup-modal/coach-checkup-modal.component.ts":
    /*!****************************************************************************************!*\
      !*** ./src/app/components/modals/coach-checkup-modal/coach-checkup-modal.component.ts ***!
      \****************************************************************************************/

    /*! exports provided: CoachCheckupModalComponent */

    /***/
    function srcAppComponentsModalsCoachCheckupModalCoachCheckupModalComponentTs(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "CoachCheckupModalComponent", function () {
        return CoachCheckupModalComponent;
      });
      /* harmony import */


      var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! tslib */
      "./node_modules/tslib/tslib.es6.js");
      /* harmony import */


      var _ionic_angular__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! @ionic/angular */
      "./node_modules/@ionic/angular/__ivy_ngcc__/fesm2015/ionic-angular.js");
      /* harmony import */


      var _components_modals_calendar_modal_calendar_modal__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! @components/modals/calendar-modal/calendar-modal */
      "./src/app/components/modals/calendar-modal/calendar-modal.ts");
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
      /*! @angular/core */
      "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
      /* harmony import */


      var hammerjs__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
      /*! hammerjs */
      "./node_modules/hammerjs/hammer.js");
      /* harmony import */


      var hammerjs__WEBPACK_IMPORTED_MODULE_4___default = /*#__PURE__*/__webpack_require__.n(hammerjs__WEBPACK_IMPORTED_MODULE_4__);

      var CoachCheckupModalComponent = /*#__PURE__*/function () {
        function CoachCheckupModalComponent(modalCtrl, gestureCtrl, element, renderer) {
          _classCallCheck(this, CoachCheckupModalComponent);

          this.modalCtrl = modalCtrl;
          this.gestureCtrl = gestureCtrl;
          this.element = element;
          this.renderer = renderer;
          this.days = [];
          this.categories = {
            slidesPerView: 2.5,
            initialSlide: 0,
            effect: 'flip',
            slidesPerColumn: 1,
            slidesPerGroup: 1,
            watchSlidesProgress: true,
            loop: true
          };
          this.options = {
            centeredSlides: true,
            slidesPerView: 1,
            spaceBetween: 0,
            initialSlide: 5,
            on: {
              beforeInit: function beforeInit() {
                var swiper = this;
                swiper.classNames.push("".concat(swiper.params.containerModifierClass, "fade"));
                var overwriteParams = {
                  slidesPerView: 1,
                  slidesPerColumn: 1,
                  slidesPerGroup: 1,
                  watchSlidesProgress: true,
                  spaceBetween: 0,
                  virtualTranslate: true
                };
                swiper.params = Object.assign(swiper.params, overwriteParams);
                swiper.params = Object.assign(swiper.originalParams, overwriteParams);
              },
              setTranslate: function setTranslate() {
                var swiper = this;
                var slides = swiper.slides;

                for (var i = 0; i < slides.length; i += 1) {
                  var $slideEl = swiper.slides.eq(i);
                  var offset$$1 = $slideEl[0].swiperSlideOffset;
                  var tx = -offset$$1;
                  if (!swiper.params.virtualTranslate) tx -= swiper.translate;
                  var ty = 0;

                  if (!swiper.isHorizontal()) {
                    ty = tx;
                    tx = 0;
                  }

                  var slideOpacity = swiper.params.fadeEffect.crossFade ? Math.max(1 - Math.abs($slideEl[0].progress), 0) : 1 + Math.min(Math.max($slideEl[0].progress, -1), 0);
                  $slideEl.css({
                    opacity: slideOpacity
                  }).transform("translate3d(".concat(tx, "px, ").concat(ty, "px, 0px)"));
                }
              },
              setTransition: function setTransition(duration) {
                var swiper = this;
                var slides = swiper.slides,
                    $wrapperEl = swiper.$wrapperEl;
                slides.transition(duration);

                if (swiper.params.virtualTranslate && duration !== 0) {
                  var eventTriggered = false;
                  slides.transitionEnd(function () {
                    if (eventTriggered) return;
                    if (!swiper || swiper.destroyed) return;
                    eventTriggered = true;
                    swiper.animating = false;
                    var triggerEvents = ['webkitTransitionEnd', 'transitionend'];

                    for (var i = 0; i < triggerEvents.length; i += 1) {
                      $wrapperEl.trigger(triggerEvents[i]);
                    }
                  });
                }
              }
            }
          };
          this.morphotypeArray = [{
            id: 1,
            src: "assets/icon/check-up-slide1.png",
            name: "First Slide",
            activeClass: false
          }, {
            id: 2,
            src: "assets/icon/check-up-slide2.png",
            name: "Second Slide",
            activeClass: false
          }, {
            id: 3,
            src: "assets/icon/check-up-slide3.png",
            name: "Third Slide",
            activeClass: false
          }];
        }

        _createClass(CoachCheckupModalComponent, [{
          key: "ngOnInit",
          value: function ngOnInit() {
            var date = new Date();
            this.days = this.getDaysInMonth(date.getMonth(), date.getFullYear());
            console.log(this.slider);
          }
        }, {
          key: "ngAfterViewInit",
          value: function ngAfterViewInit() {
            return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, /*#__PURE__*/regeneratorRuntime.mark(function _callee11() {
              var _this17 = this;

              var d, options, gesture;
              return regeneratorRuntime.wrap(function _callee11$(_context11) {
                while (1) {
                  switch (_context11.prev = _context11.next) {
                    case 0:
                      this.goToSlide();
                      d = document.querySelector('ion-modal');
                      options = {
                        el: d,
                        direction: "y",
                        gestureName: 'swipe',
                        onStart: function onStart() {
                          _this17.renderer.setStyle(d, 'transition', 'none');
                        },
                        onMove: function onMove(ev) {
                          if (ev.deltaY > 0) {
                            _this17.renderer.setStyle(d, 'transform', "translateY(".concat(ev.deltaY, "px)"));
                          }
                        },
                        onEnd: function onEnd(ev) {
                          _this17.renderer.setStyle(d, 'transition', '0.5s ease-out');

                          if (ev.deltaY > -200) {
                            _this17.renderer.setStyle(d, 'transform', 'translateY(500px)');

                            _this17.modalCtrl.dismiss();
                          } else {
                            _this17.renderer.setStyle(d, 'transform', 'translateY(0px)');
                          }
                        }
                      };
                      _context11.next = 5;
                      return this.gestureCtrl.create(options);

                    case 5:
                      gesture = _context11.sent;
                      gesture.enable();

                    case 7:
                    case "end":
                      return _context11.stop();
                  }
                }
              }, _callee11, this);
            }));
          }
        }, {
          key: "closeModal",
          value: function closeModal() {
            this.modalCtrl.dismiss();
          }
        }, {
          key: "donsomething",
          value: function donsomething(event, src) {
            this.morphotypeArray.forEach(function (obj) {
              if (obj.id == src.id) {
                obj.activeClass = true;
              } else {
                obj.activeClass = false;
              }
            });
          }
        }, {
          key: "openCalendar",
          value: function openCalendar() {
            return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, /*#__PURE__*/regeneratorRuntime.mark(function _callee12() {
              var modal;
              return regeneratorRuntime.wrap(function _callee12$(_context12) {
                while (1) {
                  switch (_context12.prev = _context12.next) {
                    case 0:
                      _context12.next = 2;
                      return this.modalCtrl.create({
                        component: _components_modals_calendar_modal_calendar_modal__WEBPACK_IMPORTED_MODULE_2__["CalendarModal"],
                        cssClass: 'calendar-style',
                        backdropDismiss: true,
                        showBackdrop: false,
                        componentProps: {
                          'startDate': new Date()
                        }
                      });

                    case 2:
                      modal = _context12.sent;
                      _context12.next = 5;
                      return modal.present();

                    case 5:
                      return _context12.abrupt("return", _context12.sent);

                    case 6:
                    case "end":
                      return _context12.stop();
                  }
                }
              }, _callee12, this);
            }));
          }
          /**
           * @param {number} The month number, 0 based
           * @param {number} The year, not zero based, required to account for leap years
           * @return {Date[]} List with date objects for each day of the month
           */

        }, {
          key: "getDaysInMonth",
          value: function getDaysInMonth(month, year) {
            var date = new Date(year, month, 1);
            var days = [];
            var today = new Date();

            while (date.getMonth() === month) {
              days.push({
                date: new Date(date)
              });
              date.setDate(date.getDate() + 1);
            }

            days.forEach(function (day, index) {
              var date = new Date(day.date); // days[index]["monthName"] = date.toLocaleString("default", {
              //   month: "short",
              // });

              days[index]["dayName"] = date.toLocaleString("default", {
                weekday: "short"
              });
              days[index]["day"] = date.toLocaleString("default", {
                day: "2-digit"
              }); // days[index]["data"] = this.getRandomInt();
              // days[index]["color"] = "#ff9900";

              if (date.getDate() === today.getDate()) {
                days[index]["isToday"] = true;
                days[index]["dayName"] = 'Today';
              } else {
                days[index]["isToday"] = false;
              }
            });
            console.log("days", days);
            return days;
          }
        }, {
          key: "goToSlide",
          value: function goToSlide() {
            return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, /*#__PURE__*/regeneratorRuntime.mark(function _callee13() {
              var currentIndex, index;
              return regeneratorRuntime.wrap(function _callee13$(_context13) {
                while (1) {
                  switch (_context13.prev = _context13.next) {
                    case 0:
                      _context13.next = 2;
                      return this.slider.nativeElement.getActiveIndex();

                    case 2:
                      currentIndex = _context13.sent;
                      index = this.days.findIndex(function (x) {
                        return x.isToday === true;
                      });
                      this.slider.nativeElement.slideTo(index, 1000);

                    case 5:
                    case "end":
                      return _context13.stop();
                  }
                }
              }, _callee13, this);
            }));
          }
        }, {
          key: "goBack",
          value: function goBack() {
            return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, /*#__PURE__*/regeneratorRuntime.mark(function _callee14() {
              var currentIndex;
              return regeneratorRuntime.wrap(function _callee14$(_context14) {
                while (1) {
                  switch (_context14.prev = _context14.next) {
                    case 0:
                      _context14.next = 2;
                      return this.slider.nativeElement.getActiveIndex();

                    case 2:
                      currentIndex = _context14.sent;
                      this.slider.nativeElement.slideTo(currentIndex - 1, 1000);

                    case 4:
                    case "end":
                      return _context14.stop();
                  }
                }
              }, _callee14, this);
            }));
          }
        }, {
          key: "goAhead",
          value: function goAhead() {
            return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, /*#__PURE__*/regeneratorRuntime.mark(function _callee15() {
              var currentIndex;
              return regeneratorRuntime.wrap(function _callee15$(_context15) {
                while (1) {
                  switch (_context15.prev = _context15.next) {
                    case 0:
                      _context15.next = 2;
                      return this.slider.nativeElement.getActiveIndex();

                    case 2:
                      currentIndex = _context15.sent;
                      this.slider.nativeElement.slideTo(currentIndex + 1, 1000);

                    case 4:
                    case "end":
                      return _context15.stop();
                  }
                }
              }, _callee15, this);
            }));
          }
        }]);

        return CoachCheckupModalComponent;
      }();

      CoachCheckupModalComponent.ctorParameters = function () {
        return [{
          type: _ionic_angular__WEBPACK_IMPORTED_MODULE_1__["ModalController"]
        }, {
          type: _ionic_angular__WEBPACK_IMPORTED_MODULE_1__["GestureController"]
        }, {
          type: _angular_core__WEBPACK_IMPORTED_MODULE_3__["ElementRef"]
        }, {
          type: _angular_core__WEBPACK_IMPORTED_MODULE_3__["Renderer2"]
        }];
      };

      CoachCheckupModalComponent.propDecorators = {
        slider: [{
          type: _angular_core__WEBPACK_IMPORTED_MODULE_3__["ViewChild"],
          args: ['myslide']
        }]
      };
      CoachCheckupModalComponent = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([Object(_angular_core__WEBPACK_IMPORTED_MODULE_3__["Component"])({
        selector: 'app-coach-checkup-modal',
        template: Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(
        /*! raw-loader!./coach-checkup-modal.component.html */
        "./node_modules/raw-loader/dist/cjs.js!./src/app/components/modals/coach-checkup-modal/coach-checkup-modal.component.html"))["default"],
        styles: [Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(
        /*! ./coach-checkup-modal.component.scss */
        "./src/app/components/modals/coach-checkup-modal/coach-checkup-modal.component.scss"))["default"]]
      })], CoachCheckupModalComponent);
      /***/
    },

    /***/
    "./src/app/components/modals/coach-new-request/coach-new-request.component.scss":
    /*!**************************************************************************************!*\
      !*** ./src/app/components/modals/coach-new-request/coach-new-request.component.scss ***!
      \**************************************************************************************/

    /*! exports provided: default */

    /***/
    function srcAppComponentsModalsCoachNewRequestCoachNewRequestComponentScss(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony default export */


      __webpack_exports__["default"] = "ion-content {\n  --padding-start: 3%;\n  --padding-end: 3%;\n}\nion-content ion-text {\n  display: block;\n}\nion-toolbar {\n  border-bottom: 1px solid #f1f1f1;\n  height: 9%;\n}\n.close-btn-icon {\n  font-size: 29px;\n  font-weight: 700;\n}\nion-card {\n  margin: 0;\n}\n.check-out-big-cards {\n  display: flex;\n  flex-direction: column;\n  align-items: flex-end;\n}\n.check-out-big-cards div {\n  display: flex;\n  flex-direction: row;\n  margin-top: 10px;\n  margin-bottom: 10px;\n}\n.check-out-big-cards p {\n  margin-bottom: 0;\n  color: #353535;\n}\n.check-out-big-cards h4 {\n  margin: 0;\n}\n.check-out-big-cards ion-label {\n  margin-top: 5px;\n  margin-left: 3px;\n  color: #353535;\n}\n.profile-viewall {\n  width: 100%;\n  position: relative;\n  margin-top: 15px;\n  margin-bottom: 15px;\n}\n.profile-viewall ion-button {\n  --border-radius: 25px;\n  display: table;\n  margin: auto;\n  font-size: 12px;\n}\n.profile-viewall:before {\n  content: \"\";\n  position: absolute;\n  background: #c1c1c1;\n  height: 1px;\n  width: 100%;\n  top: 19px;\n  left: 0;\n}\n.container_data {\n  position: relative;\n  display: block;\n  width: 100%;\n  margin-top: 15px;\n}\nion-item ion-label {\n  font-size: 14px;\n  font-family: \"Oswald\";\n}\nion-avatar {\n  padding: 5px;\n  -webkit-margin-end: 0px;\n          margin-inline-end: 0px;\n}\n.next {\n  --background: var(--ion-btn-custom-color);\n}\n.back {\n  --background: linear-gradient(to right, #161616e0, #c1c1c1);\n}\n.back,\n.next {\n  --ion-button-round-border-radius: 5px;\n  --ion-button-border-radius: 5px;\n  --ion-border-radius: 5px;\n  --border-radius: 5px;\n  height: 40px;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvY29tcG9uZW50cy9tb2RhbHMvY29hY2gtbmV3LXJlcXVlc3QvY29hY2gtbmV3LXJlcXVlc3QuY29tcG9uZW50LnNjc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUE7RUFDSSxtQkFBQTtFQUNBLGlCQUFBO0FBQ0o7QUFDSTtFQUNJLGNBQUE7QUFDUjtBQUdBO0VBQ0ksZ0NBQUE7RUFDQSxVQUFBO0FBQUo7QUFHQTtFQUNJLGVBQUE7RUFDQSxnQkFBQTtBQUFKO0FBR0E7RUFDSSxTQUFBO0FBQUo7QUFFQTtFQUNJLGFBQUE7RUFDQSxzQkFBQTtFQUNBLHFCQUFBO0FBQ0o7QUFDSTtFQUNJLGFBQUE7RUFDQSxtQkFBQTtFQUNBLGdCQUFBO0VBQ0EsbUJBQUE7QUFDUjtBQUVJO0VBQ0ksZ0JBQUE7RUFDQSxjQUFBO0FBQVI7QUFHSTtFQUNJLFNBQUE7QUFEUjtBQUtJO0VBQ0ksZUFBQTtFQUNBLGdCQUFBO0VBQ0EsY0FBQTtBQUhSO0FBT0E7RUFDSSxXQUFBO0VBQ0Esa0JBQUE7RUFDQSxnQkFBQTtFQUNBLG1CQUFBO0FBSko7QUFPQTtFQUNJLHFCQUFBO0VBQ0EsY0FBQTtFQUNBLFlBQUE7RUFDQSxlQUFBO0FBSko7QUFPQTtFQUNJLFdBQUE7RUFDQSxrQkFBQTtFQUNBLG1CQUFBO0VBQ0EsV0FBQTtFQUNBLFdBQUE7RUFDQSxTQUFBO0VBQ0EsT0FBQTtBQUpKO0FBTUE7RUFDSSxrQkFBQTtFQUNBLGNBQUE7RUFDQSxXQUFBO0VBQ0EsZ0JBQUE7QUFISjtBQU1JO0VBQ0ksZUFBQTtFQUNBLHFCQUFBO0FBSFI7QUFNQTtFQUNJLFlBQUE7RUFDQSx1QkFBQTtVQUFBLHNCQUFBO0FBSEo7QUFLQTtFQUNJLHlDQUFBO0FBRko7QUFNQTtFQUNJLDJEQUFBO0FBSEo7QUFLQTs7RUFFSSxxQ0FBQTtFQUNBLCtCQUFBO0VBQ0Esd0JBQUE7RUFDQSxvQkFBQTtFQUNBLFlBQUE7QUFGSiIsImZpbGUiOiJzcmMvYXBwL2NvbXBvbmVudHMvbW9kYWxzL2NvYWNoLW5ldy1yZXF1ZXN0L2NvYWNoLW5ldy1yZXF1ZXN0LmNvbXBvbmVudC5zY3NzIiwic291cmNlc0NvbnRlbnQiOlsiaW9uLWNvbnRlbnQge1xuICAgIC0tcGFkZGluZy1zdGFydDogMyU7XG4gICAgLS1wYWRkaW5nLWVuZDogMyU7ICBcblxuICAgIGlvbi10ZXh0IHtcbiAgICAgICAgZGlzcGxheTogYmxvY2s7XG4gICAgfVxufVxuXG5pb24tdG9vbGJhciB7XG4gICAgYm9yZGVyLWJvdHRvbTogMXB4IHNvbGlkICNmMWYxZjE7XG4gICAgaGVpZ2h0OiA5JTtcbn1cblxuLmNsb3NlLWJ0bi1pY29uIHtcbiAgICBmb250LXNpemU6IDI5cHg7XG4gICAgZm9udC13ZWlnaHQ6IDcwMDtcbn1cblxuaW9uLWNhcmQge1xuICAgIG1hcmdpbjogMDtcbn1cbi5jaGVjay1vdXQtYmlnLWNhcmRzIHtcbiAgICBkaXNwbGF5OiBmbGV4O1xuICAgIGZsZXgtZGlyZWN0aW9uOiBjb2x1bW47XG4gICAgYWxpZ24taXRlbXM6IGZsZXgtZW5kO1xuXG4gICAgZGl2IHtcbiAgICAgICAgZGlzcGxheTogZmxleDtcbiAgICAgICAgZmxleC1kaXJlY3Rpb246IHJvdztcbiAgICAgICAgbWFyZ2luLXRvcDogMTBweDtcbiAgICAgICAgbWFyZ2luLWJvdHRvbTogMTBweDtcbiAgICB9XG5cbiAgICBwIHtcbiAgICAgICAgbWFyZ2luLWJvdHRvbTogMDtcbiAgICAgICAgY29sb3I6ICMzNTM1MzU7XG4gICAgfVxuXG4gICAgaDQge1xuICAgICAgICBtYXJnaW46IDA7XG5cbiAgICB9XG5cbiAgICBpb24tbGFiZWwge1xuICAgICAgICBtYXJnaW4tdG9wOiA1cHg7XG4gICAgICAgIG1hcmdpbi1sZWZ0OiAzcHg7XG4gICAgICAgIGNvbG9yOiAjMzUzNTM1O1xuICAgIH1cbn1cblxuLnByb2ZpbGUtdmlld2FsbCB7XG4gICAgd2lkdGg6IDEwMCU7XG4gICAgcG9zaXRpb246IHJlbGF0aXZlO1xuICAgIG1hcmdpbi10b3A6IDE1cHg7XG4gICAgbWFyZ2luLWJvdHRvbTogMTVweDtcbn1cblxuLnByb2ZpbGUtdmlld2FsbCBpb24tYnV0dG9uIHtcbiAgICAtLWJvcmRlci1yYWRpdXM6IDI1cHg7XG4gICAgZGlzcGxheTogdGFibGU7XG4gICAgbWFyZ2luOiBhdXRvO1xuICAgIGZvbnQtc2l6ZTogMTJweDtcbn1cblxuLnByb2ZpbGUtdmlld2FsbDpiZWZvcmUge1xuICAgIGNvbnRlbnQ6IFwiXCI7XG4gICAgcG9zaXRpb246IGFic29sdXRlO1xuICAgIGJhY2tncm91bmQ6ICNjMWMxYzE7XG4gICAgaGVpZ2h0OiAxcHg7XG4gICAgd2lkdGg6IDEwMCU7XG4gICAgdG9wOiAxOXB4O1xuICAgIGxlZnQ6IDA7XG59XG4uY29udGFpbmVyX2RhdGEge1xuICAgIHBvc2l0aW9uOiByZWxhdGl2ZTtcbiAgICBkaXNwbGF5OiBibG9jaztcbiAgICB3aWR0aDogMTAwJTtcbiAgICBtYXJnaW4tdG9wOiAxNXB4O1xufVxuaW9uLWl0ZW0ge1xuICAgIGlvbi1sYWJlbCB7XG4gICAgICAgIGZvbnQtc2l6ZTogMTRweDtcbiAgICAgICAgZm9udC1mYW1pbHk6ICdPc3dhbGQnO1xuICAgIH1cbn1cbmlvbi1hdmF0YXIge1xuICAgIHBhZGRpbmc6IDVweDtcbiAgICBtYXJnaW4taW5saW5lLWVuZDogMHB4O1xufVxuLm5leHQge1xuICAgIC0tYmFja2dyb3VuZDogdmFyKC0taW9uLWJ0bi1jdXN0b20tY29sb3IpO1xuXG59XG5cbi5iYWNrIHtcbiAgICAtLWJhY2tncm91bmQ6IGxpbmVhci1ncmFkaWVudCh0byByaWdodCwgIzE2MTYxNmUwLCAjYzFjMWMxKTtcbn1cbi5iYWNrLFxuLm5leHQge1xuICAgIC0taW9uLWJ1dHRvbi1yb3VuZC1ib3JkZXItcmFkaXVzOiA1cHg7XG4gICAgLS1pb24tYnV0dG9uLWJvcmRlci1yYWRpdXM6IDVweDtcbiAgICAtLWlvbi1ib3JkZXItcmFkaXVzOiA1cHg7XG4gICAgLS1ib3JkZXItcmFkaXVzOiA1cHg7XG4gICAgaGVpZ2h0OiA0MHB4O1xufSJdfQ== */";
      /***/
    },

    /***/
    "./src/app/components/modals/coach-new-request/coach-new-request.component.ts":
    /*!************************************************************************************!*\
      !*** ./src/app/components/modals/coach-new-request/coach-new-request.component.ts ***!
      \************************************************************************************/

    /*! exports provided: CoachNewRequestComponent */

    /***/
    function srcAppComponentsModalsCoachNewRequestCoachNewRequestComponentTs(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "CoachNewRequestComponent", function () {
        return CoachNewRequestComponent;
      });
      /* harmony import */


      var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! tslib */
      "./node_modules/tslib/tslib.es6.js");
      /* harmony import */


      var _ionic_angular__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! @ionic/angular */
      "./node_modules/@ionic/angular/__ivy_ngcc__/fesm2015/ionic-angular.js");
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! @angular/core */
      "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
      /* harmony import */


      var hammerjs__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
      /*! hammerjs */
      "./node_modules/hammerjs/hammer.js");
      /* harmony import */


      var hammerjs__WEBPACK_IMPORTED_MODULE_3___default = /*#__PURE__*/__webpack_require__.n(hammerjs__WEBPACK_IMPORTED_MODULE_3__);

      var CoachNewRequestComponent = /*#__PURE__*/function () {
        function CoachNewRequestComponent(modalCtrl, gestureCtrl, element, renderer) {
          _classCallCheck(this, CoachNewRequestComponent);

          this.modalCtrl = modalCtrl;
          this.gestureCtrl = gestureCtrl;
          this.element = element;
          this.renderer = renderer;
        }

        _createClass(CoachNewRequestComponent, [{
          key: "ngOnInit",
          value: function ngOnInit() {}
        }, {
          key: "closeModal",
          value: function closeModal() {
            this.modalCtrl.dismiss();
          }
        }, {
          key: "ngAfterViewInit",
          value: function ngAfterViewInit() {
            return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, /*#__PURE__*/regeneratorRuntime.mark(function _callee16() {
              var _this18 = this;

              var d, options, gesture;
              return regeneratorRuntime.wrap(function _callee16$(_context16) {
                while (1) {
                  switch (_context16.prev = _context16.next) {
                    case 0:
                      d = document.querySelector('ion-modal');
                      options = {
                        el: d,
                        direction: "y",
                        gestureName: 'swipe',
                        onStart: function onStart() {
                          _this18.renderer.setStyle(d, 'transition', 'none');
                        },
                        onMove: function onMove(ev) {
                          if (ev.deltaY > 0) {
                            _this18.renderer.setStyle(d, 'transform', "translateY(".concat(ev.deltaY, "px)"));
                          }
                        },
                        onEnd: function onEnd(ev) {
                          _this18.renderer.setStyle(d, 'transition', '0.5s ease-out');

                          if (ev.deltaY > -200) {
                            _this18.renderer.setStyle(d, 'transform', 'translateY(500px)');

                            _this18.modalCtrl.dismiss();
                          } else {
                            _this18.renderer.setStyle(d, 'transform', 'translateY(0px)');
                          }
                        }
                      };
                      _context16.next = 4;
                      return this.gestureCtrl.create(options);

                    case 4:
                      gesture = _context16.sent;
                      gesture.enable();

                    case 6:
                    case "end":
                      return _context16.stop();
                  }
                }
              }, _callee16, this);
            }));
          }
        }]);

        return CoachNewRequestComponent;
      }();

      CoachNewRequestComponent.ctorParameters = function () {
        return [{
          type: _ionic_angular__WEBPACK_IMPORTED_MODULE_1__["ModalController"]
        }, {
          type: _ionic_angular__WEBPACK_IMPORTED_MODULE_1__["GestureController"]
        }, {
          type: _angular_core__WEBPACK_IMPORTED_MODULE_2__["ElementRef"]
        }, {
          type: _angular_core__WEBPACK_IMPORTED_MODULE_2__["Renderer2"]
        }];
      };

      CoachNewRequestComponent = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([Object(_angular_core__WEBPACK_IMPORTED_MODULE_2__["Component"])({
        selector: 'app-coach-new-request',
        template: Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(
        /*! raw-loader!./coach-new-request.component.html */
        "./node_modules/raw-loader/dist/cjs.js!./src/app/components/modals/coach-new-request/coach-new-request.component.html"))["default"],
        styles: [Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(
        /*! ./coach-new-request.component.scss */
        "./src/app/components/modals/coach-new-request/coach-new-request.component.scss"))["default"]]
      })], CoachNewRequestComponent);
      /***/
    },

    /***/
    "./src/app/components/modals/courses-modal/courses-modal.component.scss":
    /*!******************************************************************************!*\
      !*** ./src/app/components/modals/courses-modal/courses-modal.component.scss ***!
      \******************************************************************************/

    /*! exports provided: default */

    /***/
    function srcAppComponentsModalsCoursesModalCoursesModalComponentScss(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony default export */


      __webpack_exports__["default"] = "ion-content {\n  --padding-start: 3%;\n  --padding-end: 3%;\n}\nion-content ion-text {\n  display: block;\n}\nion-toolbar {\n  border-bottom: 1px solid #f1f1f1;\n  height: 9%;\n}\n.close-btn-icon {\n  font-size: 29px;\n  font-weight: 700;\n}\n.header_row {\n  display: flex;\n  flex-direction: row;\n  justify-content: space-between;\n}\n.header_row ion-label {\n  align-self: center;\n}\n.header_row div {\n  width: 48%;\n  border-style: dashed;\n  border-color: #dfdddd;\n  height: 1px;\n  border-width: 1px;\n  align-self: center;\n  position: absolute;\n  right: 14%;\n}\n.header_row ion-avatar {\n  padding: 8px;\n}\n.large_card_row ion-card {\n  overflow: inherit;\n  width: 100%;\n  padding: 10px;\n  margin: 0;\n}\n.small_card_row ion-card {\n  width: 100%;\n  margin: 0;\n  height: 50px;\n  display: flex;\n  padding-left: 5%;\n}\n.small_card_row ion-card ion-label {\n  align-self: center;\n}\n.first_side_section {\n  display: flex;\n  flex-direction: row;\n  position: relative;\n  top: -66%;\n  right: -19%;\n  justify-content: space-around;\n}\n.first_side_section .circle {\n  width: 30px;\n  height: 30px;\n  background: #d2d2d2;\n  border-radius: 50%;\n  align-self: center;\n}\n.first_side_section .second_circle {\n  width: 30px;\n  height: 30px;\n  background: var(--ion-btn-custom-color);\n  border-radius: 50%;\n  align-self: center;\n}\n.first_side_section ion-button {\n  padding: 2px;\n  --background: #d2d2d2;\n}\n.first_side_section .second_btn {\n  --background: var(--ion-btn-custom-color);\n}\n.second_side_section {\n  display: flex;\n  flex-direction: row;\n  position: relative;\n  top: -66%;\n  right: -21%;\n  justify-content: space-around;\n}\n.second_side_section .circle {\n  width: 30px;\n  height: 30px;\n  background: #f4f5f8;\n  border-radius: 50%;\n  align-self: center;\n}\n.second_side_section .second_circle {\n  width: 30px;\n  height: 30px;\n  background: var(--ion-btn-custom-color);\n  border-radius: 50%;\n  align-self: center;\n  padding: 7px;\n}\n.second_side_section ion-button {\n  padding: 2px;\n}\n.second_side_section .second_btn {\n  --background: var(--ion-btn-custom-color);\n}\nion-col {\n  padding: 0;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvY29tcG9uZW50cy9tb2RhbHMvY291cnNlcy1tb2RhbC9jb3Vyc2VzLW1vZGFsLmNvbXBvbmVudC5zY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBO0VBQ0ksbUJBQUE7RUFDQSxpQkFBQTtBQUNKO0FBQ0k7RUFDSSxjQUFBO0FBQ1I7QUFJQTtFQUNJLGdDQUFBO0VBQ0EsVUFBQTtBQURKO0FBS0E7RUFDSSxlQUFBO0VBQ0EsZ0JBQUE7QUFGSjtBQU1BO0VBQ0ksYUFBQTtFQUNBLG1CQUFBO0VBQ0EsOEJBQUE7QUFISjtBQUtJO0VBQ0ksa0JBQUE7QUFIUjtBQVFJO0VBQ0ksVUFBQTtFQUNBLG9CQUFBO0VBQ0EscUJBQUE7RUFDQSxXQUFBO0VBQ0EsaUJBQUE7RUFDQSxrQkFBQTtFQUNBLGtCQUFBO0VBQ0EsVUFBQTtBQU5SO0FBU0k7RUFDSSxZQUFBO0FBUFI7QUFtQkk7RUFDSSxpQkFBQTtFQUNBLFdBQUE7RUFFQSxhQUFBO0VBQ0EsU0FBQTtBQWpCUjtBQXlCSTtFQUNJLFdBQUE7RUFDQSxTQUFBO0VBRUEsWUFBQTtFQUNBLGFBQUE7RUFDQSxnQkFBQTtBQXZCUjtBQXlCUTtFQUNJLGtCQUFBO0FBdkJaO0FBNkJBO0VBQ0ksYUFBQTtFQUNBLG1CQUFBO0VBQ0Esa0JBQUE7RUFDQSxTQUFBO0VBQ0EsV0FBQTtFQUNBLDZCQUFBO0FBMUJKO0FBNEJJO0VBQ0ksV0FBQTtFQUNBLFlBQUE7RUFDQSxtQkFBQTtFQUNBLGtCQUFBO0VBQ0Esa0JBQUE7QUExQlI7QUE0Qkk7RUFDSSxXQUFBO0VBQ0EsWUFBQTtFQUNBLHVDQUFBO0VBQ0Esa0JBQUE7RUFDQSxrQkFBQTtBQTFCUjtBQStCSTtFQUNJLFlBQUE7RUFDQSxxQkFBQTtBQTdCUjtBQWlDSTtFQUNJLHlDQUFBO0FBL0JSO0FBb0NBO0VBRUksYUFBQTtFQUNBLG1CQUFBO0VBQ0Esa0JBQUE7RUFDQSxTQUFBO0VBQ0EsV0FBQTtFQUNBLDZCQUFBO0FBbENKO0FBb0NJO0VBQ0ksV0FBQTtFQUNBLFlBQUE7RUFDQSxtQkFBQTtFQUNBLGtCQUFBO0VBQ0Esa0JBQUE7QUFsQ1I7QUFvQ0k7RUFDSSxXQUFBO0VBQ0EsWUFBQTtFQUNBLHVDQUFBO0VBQ0Esa0JBQUE7RUFDQSxrQkFBQTtFQUNBLFlBQUE7QUFsQ1I7QUFzQ0k7RUFDSSxZQUFBO0FBcENSO0FBdUNJO0VBQ0kseUNBQUE7QUFyQ1I7QUE0Q0E7RUFDSSxVQUFBO0FBekNKIiwiZmlsZSI6InNyYy9hcHAvY29tcG9uZW50cy9tb2RhbHMvY291cnNlcy1tb2RhbC9jb3Vyc2VzLW1vZGFsLmNvbXBvbmVudC5zY3NzIiwic291cmNlc0NvbnRlbnQiOlsiaW9uLWNvbnRlbnQge1xuICAgIC0tcGFkZGluZy1zdGFydDogMyU7XG4gICAgLS1wYWRkaW5nLWVuZDogMyU7XG5cbiAgICBpb24tdGV4dCB7XG4gICAgICAgIGRpc3BsYXk6IGJsb2NrO1xuICAgIH1cblxufVxuXG5pb24tdG9vbGJhciB7XG4gICAgYm9yZGVyLWJvdHRvbTogMXB4IHNvbGlkICNmMWYxZjE7XG4gICAgaGVpZ2h0OiA5JTtcbn1cblxuXG4uY2xvc2UtYnRuLWljb24ge1xuICAgIGZvbnQtc2l6ZTogMjlweDtcbiAgICBmb250LXdlaWdodDogNzAwO1xufVxuXG5cbi5oZWFkZXJfcm93IHtcbiAgICBkaXNwbGF5OiBmbGV4O1xuICAgIGZsZXgtZGlyZWN0aW9uOiByb3c7XG4gICAganVzdGlmeS1jb250ZW50OiBzcGFjZS1iZXR3ZWVuO1xuXG4gICAgaW9uLWxhYmVsIHtcbiAgICAgICAgYWxpZ24tc2VsZjogY2VudGVyO1xuXG4gICAgfVxuXG5cbiAgICBkaXYge1xuICAgICAgICB3aWR0aDogNDglO1xuICAgICAgICBib3JkZXItc3R5bGU6IGRhc2hlZDtcbiAgICAgICAgYm9yZGVyLWNvbG9yOiAjZGZkZGRkO1xuICAgICAgICBoZWlnaHQ6IDFweDtcbiAgICAgICAgYm9yZGVyLXdpZHRoOiAxcHg7XG4gICAgICAgIGFsaWduLXNlbGY6IGNlbnRlcjtcbiAgICAgICAgcG9zaXRpb246IGFic29sdXRlO1xuICAgICAgICByaWdodDogMTQlO1xuICAgIH1cblxuICAgIGlvbi1hdmF0YXIge1xuICAgICAgICBwYWRkaW5nOiA4cHg7XG4gICAgfVxufVxuXG5cbi8vIGlvbi1jYXJkIHtcbi8vICAgICB3aWR0aDogMTAwJTtcbi8vIH1cblxuLmxhcmdlX2NhcmRfcm93IHtcblxuICAgIFxuICAgIGlvbi1jYXJkIHtcbiAgICAgICAgb3ZlcmZsb3c6IGluaGVyaXQ7XG4gICAgICAgIHdpZHRoOiAxMDAlO1xuICAgICAgICAvLyBoZWlnaHQ6IDEzMHB4O1xuICAgICAgICBwYWRkaW5nOiAxMHB4O1xuICAgICAgICBtYXJnaW46IDA7XG5cbiAgICB9XG59XG5cblxuLnNtYWxsX2NhcmRfcm93IHtcblxuICAgIGlvbi1jYXJkIHtcbiAgICAgICAgd2lkdGg6IDEwMCU7XG4gICAgICAgIG1hcmdpbjogMDtcblxuICAgICAgICBoZWlnaHQ6IDUwcHg7XG4gICAgICAgIGRpc3BsYXk6IGZsZXg7XG4gICAgICAgIHBhZGRpbmctbGVmdDogNSU7XG5cbiAgICAgICAgaW9uLWxhYmVsIHtcbiAgICAgICAgICAgIGFsaWduLXNlbGY6IGNlbnRlcjtcbiAgICAgICAgfVxuICAgIH1cbn1cblxuXG4uZmlyc3Rfc2lkZV9zZWN0aW9uIHtcbiAgICBkaXNwbGF5OiBmbGV4O1xuICAgIGZsZXgtZGlyZWN0aW9uOiByb3c7XG4gICAgcG9zaXRpb246IHJlbGF0aXZlO1xuICAgIHRvcDogLTY2JTtcbiAgICByaWdodDogLTE5JTtcbiAgICBqdXN0aWZ5LWNvbnRlbnQ6IHNwYWNlLWFyb3VuZDtcblxuICAgIC5jaXJjbGUge1xuICAgICAgICB3aWR0aDogMzBweDtcbiAgICAgICAgaGVpZ2h0OiAzMHB4O1xuICAgICAgICBiYWNrZ3JvdW5kOiAjZDJkMmQyO1xuICAgICAgICBib3JkZXItcmFkaXVzOiA1MCU7XG4gICAgICAgIGFsaWduLXNlbGY6IGNlbnRlcjtcbiAgICB9XG4gICAgLnNlY29uZF9jaXJjbGUge1xuICAgICAgICB3aWR0aDogMzBweDtcbiAgICAgICAgaGVpZ2h0OiAzMHB4O1xuICAgICAgICBiYWNrZ3JvdW5kOiAgdmFyKC0taW9uLWJ0bi1jdXN0b20tY29sb3IpO1xuICAgICAgICBib3JkZXItcmFkaXVzOiA1MCU7XG4gICAgICAgIGFsaWduLXNlbGY6IGNlbnRlcjtcblxuXG4gICAgfVxuXG4gICAgaW9uLWJ1dHRvbiB7XG4gICAgICAgIHBhZGRpbmc6IDJweDtcbiAgICAgICAgLS1iYWNrZ3JvdW5kOiAjZDJkMmQyO1xuXG4gICAgfVxuXG4gICAgLnNlY29uZF9idG4ge1xuICAgICAgICAtLWJhY2tncm91bmQ6IHZhcigtLWlvbi1idG4tY3VzdG9tLWNvbG9yKTtcbiAgICB9XG59XG5cblxuLnNlY29uZF9zaWRlX3NlY3Rpb24ge1xuXG4gICAgZGlzcGxheTogZmxleDtcbiAgICBmbGV4LWRpcmVjdGlvbjogcm93O1xuICAgIHBvc2l0aW9uOiByZWxhdGl2ZTtcbiAgICB0b3A6IC02NiU7XG4gICAgcmlnaHQ6IC0yMSU7XG4gICAganVzdGlmeS1jb250ZW50OiBzcGFjZS1hcm91bmQ7XG5cbiAgICAuY2lyY2xlIHtcbiAgICAgICAgd2lkdGg6IDMwcHg7XG4gICAgICAgIGhlaWdodDogMzBweDtcbiAgICAgICAgYmFja2dyb3VuZDogI2Y0ZjVmODtcbiAgICAgICAgYm9yZGVyLXJhZGl1czogNTAlO1xuICAgICAgICBhbGlnbi1zZWxmOiBjZW50ZXI7XG4gICAgfVxuICAgIC5zZWNvbmRfY2lyY2xlIHtcbiAgICAgICAgd2lkdGg6IDMwcHg7XG4gICAgICAgIGhlaWdodDogMzBweDtcbiAgICAgICAgYmFja2dyb3VuZDogIHZhcigtLWlvbi1idG4tY3VzdG9tLWNvbG9yKTtcbiAgICAgICAgYm9yZGVyLXJhZGl1czogNTAlO1xuICAgICAgICBhbGlnbi1zZWxmOiBjZW50ZXI7XG4gICAgICAgIHBhZGRpbmc6IDdweDtcblxuICAgIH1cblxuICAgIGlvbi1idXR0b24ge1xuICAgICAgICBwYWRkaW5nOiAycHg7XG4gICAgfVxuXG4gICAgLnNlY29uZF9idG4ge1xuICAgICAgICAtLWJhY2tncm91bmQ6IHZhcigtLWlvbi1idG4tY3VzdG9tLWNvbG9yKTtcbiAgICB9XG59XG5cblxuXG5cbmlvbi1jb2wge1xuICAgIHBhZGRpbmc6IDA7XG59Il19 */";
      /***/
    },

    /***/
    "./src/app/components/modals/courses-modal/courses-modal.component.ts":
    /*!****************************************************************************!*\
      !*** ./src/app/components/modals/courses-modal/courses-modal.component.ts ***!
      \****************************************************************************/

    /*! exports provided: CoursesModalComponent */

    /***/
    function srcAppComponentsModalsCoursesModalCoursesModalComponentTs(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "CoursesModalComponent", function () {
        return CoursesModalComponent;
      });
      /* harmony import */


      var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! tslib */
      "./node_modules/tslib/tslib.es6.js");
      /* harmony import */


      var _ionic_angular__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! @ionic/angular */
      "./node_modules/@ionic/angular/__ivy_ngcc__/fesm2015/ionic-angular.js");
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! @angular/core */
      "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
      /* harmony import */


      var hammerjs__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
      /*! hammerjs */
      "./node_modules/hammerjs/hammer.js");
      /* harmony import */


      var hammerjs__WEBPACK_IMPORTED_MODULE_3___default = /*#__PURE__*/__webpack_require__.n(hammerjs__WEBPACK_IMPORTED_MODULE_3__);

      var CoursesModalComponent = /*#__PURE__*/function () {
        function CoursesModalComponent(modalCtrl, gestureCtrl, element, renderer) {
          _classCallCheck(this, CoursesModalComponent);

          this.modalCtrl = modalCtrl;
          this.gestureCtrl = gestureCtrl;
          this.element = element;
          this.renderer = renderer;
        }

        _createClass(CoursesModalComponent, [{
          key: "ngOnInit",
          value: function ngOnInit() {}
        }, {
          key: "closeModal",
          value: function closeModal() {
            console.log("aa");
            this.modalCtrl.dismiss();
          }
        }, {
          key: "ngAfterViewInit",
          value: function ngAfterViewInit() {
            return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, /*#__PURE__*/regeneratorRuntime.mark(function _callee17() {
              var _this19 = this;

              var d, options, gesture;
              return regeneratorRuntime.wrap(function _callee17$(_context17) {
                while (1) {
                  switch (_context17.prev = _context17.next) {
                    case 0:
                      d = document.querySelector('ion-modal');
                      options = {
                        el: d,
                        direction: "y",
                        gestureName: 'swipe',
                        onStart: function onStart() {
                          _this19.renderer.setStyle(d, 'transition', 'none');
                        },
                        onMove: function onMove(ev) {
                          if (ev.deltaY > 0) {
                            _this19.renderer.setStyle(d, 'transform', "translateY(".concat(ev.deltaY, "px)"));
                          }
                        },
                        onEnd: function onEnd(ev) {
                          _this19.renderer.setStyle(d, 'transition', '0.5s ease-out');

                          if (ev.deltaY > -200) {
                            _this19.renderer.setStyle(d, 'transform', 'translateY(500px)');

                            _this19.modalCtrl.dismiss();
                          } else {
                            _this19.renderer.setStyle(d, 'transform', 'translateY(0px)');
                          }
                        }
                      };
                      _context17.next = 4;
                      return this.gestureCtrl.create(options);

                    case 4:
                      gesture = _context17.sent;
                      gesture.enable();

                    case 6:
                    case "end":
                      return _context17.stop();
                  }
                }
              }, _callee17, this);
            }));
          }
        }]);

        return CoursesModalComponent;
      }();

      CoursesModalComponent.ctorParameters = function () {
        return [{
          type: _ionic_angular__WEBPACK_IMPORTED_MODULE_1__["ModalController"]
        }, {
          type: _ionic_angular__WEBPACK_IMPORTED_MODULE_1__["GestureController"]
        }, {
          type: _angular_core__WEBPACK_IMPORTED_MODULE_2__["ElementRef"]
        }, {
          type: _angular_core__WEBPACK_IMPORTED_MODULE_2__["Renderer2"]
        }];
      };

      CoursesModalComponent = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([Object(_angular_core__WEBPACK_IMPORTED_MODULE_2__["Component"])({
        selector: 'app-courses-modal',
        template: Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(
        /*! raw-loader!./courses-modal.component.html */
        "./node_modules/raw-loader/dist/cjs.js!./src/app/components/modals/courses-modal/courses-modal.component.html"))["default"],
        styles: [Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(
        /*! ./courses-modal.component.scss */
        "./src/app/components/modals/courses-modal/courses-modal.component.scss"))["default"]]
      })], CoursesModalComponent);
      /***/
    },

    /***/
    "./src/app/components/modals/manual-registration/manual-registration.component.scss":
    /*!******************************************************************************************!*\
      !*** ./src/app/components/modals/manual-registration/manual-registration.component.scss ***!
      \******************************************************************************************/

    /*! exports provided: default */

    /***/
    function srcAppComponentsModalsManualRegistrationManualRegistrationComponentScss(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony default export */


      __webpack_exports__["default"] = "ion-content {\n  --padding-start: 3%;\n  --padding-end: 3%;\n}\nion-content ion-text {\n  display: block;\n}\nion-toolbar {\n  border-bottom: 1px solid #f1f1f1;\n  height: 9%;\n}\n.close-btn-icon {\n  font-size: 29px;\n  font-weight: 700;\n}\n.header_row {\n  display: flex;\n  flex-direction: row;\n  justify-content: space-between;\n}\n.header_row ion-label {\n  align-self: center;\n}\n.header_row div {\n  width: 48%;\n  border-style: dashed;\n  border-color: gray;\n  height: 1px;\n  border-width: 1px;\n  align-self: center;\n  position: absolute;\n  right: 16%;\n}\n.small_card_row ion-card {\n  width: 100%;\n  margin: 0;\n  height: 80px;\n  display: flex;\n  padding-left: 5%;\n}\n.small_card_row ion-card ion-card-content {\n  display: flex;\n  flex-direction: column;\n}\n.big_card_row ion-card {\n  width: 100%;\n  margin: 0;\n  height: 130px;\n  display: flex;\n  padding-left: 5%;\n}\n.big_card_row ion-card ion-card-content {\n  display: flex;\n  flex-direction: row;\n}\n.big_card_row ion-card ion-card-content ion-img {\n  width: 95px;\n}\n.big_card_row ion-card ion-card-content ion-label {\n  align-self: center;\n  padding-left: 5%;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvY29tcG9uZW50cy9tb2RhbHMvbWFudWFsLXJlZ2lzdHJhdGlvbi9tYW51YWwtcmVnaXN0cmF0aW9uLmNvbXBvbmVudC5zY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBO0VBQ0ksbUJBQUE7RUFDQSxpQkFBQTtBQUNKO0FBQ0k7RUFDSSxjQUFBO0FBQ1I7QUFJQTtFQUNJLGdDQUFBO0VBQ0EsVUFBQTtBQURKO0FBSUE7RUFDSSxlQUFBO0VBQ0EsZ0JBQUE7QUFESjtBQUlBO0VBQ0ksYUFBQTtFQUNBLG1CQUFBO0VBQ0EsOEJBQUE7QUFESjtBQUdJO0VBQ0ksa0JBQUE7QUFEUjtBQU1JO0VBQ0ksVUFBQTtFQUNBLG9CQUFBO0VBQ0Esa0JBQUE7RUFDQSxXQUFBO0VBQ0EsaUJBQUE7RUFDQSxrQkFBQTtFQUNBLGtCQUFBO0VBQ0EsVUFBQTtBQUpSO0FBVUk7RUFDSSxXQUFBO0VBQ0EsU0FBQTtFQUVBLFlBQUE7RUFDQSxhQUFBO0VBQ0EsZ0JBQUE7QUFSUjtBQVdRO0VBQ0ksYUFBQTtFQUNBLHNCQUFBO0FBVFo7QUFpQkk7RUFDSSxXQUFBO0VBQ0EsU0FBQTtFQUVBLGFBQUE7RUFDQSxhQUFBO0VBQ0EsZ0JBQUE7QUFmUjtBQWtCUTtFQUNJLGFBQUE7RUFDQSxtQkFBQTtBQWhCWjtBQWtCWTtFQUNJLFdBQUE7QUFoQmhCO0FBbUJZO0VBQ0ksa0JBQUE7RUFDQSxnQkFBQTtBQWpCaEIiLCJmaWxlIjoic3JjL2FwcC9jb21wb25lbnRzL21vZGFscy9tYW51YWwtcmVnaXN0cmF0aW9uL21hbnVhbC1yZWdpc3RyYXRpb24uY29tcG9uZW50LnNjc3MiLCJzb3VyY2VzQ29udGVudCI6WyJpb24tY29udGVudCB7XG4gICAgLS1wYWRkaW5nLXN0YXJ0OiAzJTtcbiAgICAtLXBhZGRpbmctZW5kOiAzJTtcblxuICAgIGlvbi10ZXh0IHtcbiAgICAgICAgZGlzcGxheTogYmxvY2s7XG4gICAgfVxuXG59XG5cbmlvbi10b29sYmFyIHtcbiAgICBib3JkZXItYm90dG9tOiAxcHggc29saWQgI2YxZjFmMTtcbiAgICBoZWlnaHQ6IDklO1xufVxuXG4uY2xvc2UtYnRuLWljb24ge1xuICAgIGZvbnQtc2l6ZTogMjlweDtcbiAgICBmb250LXdlaWdodDogNzAwO1xufVxuXG4uaGVhZGVyX3JvdyB7XG4gICAgZGlzcGxheTogZmxleDtcbiAgICBmbGV4LWRpcmVjdGlvbjogcm93O1xuICAgIGp1c3RpZnktY29udGVudDogc3BhY2UtYmV0d2VlbjtcblxuICAgIGlvbi1sYWJlbCB7XG4gICAgICAgIGFsaWduLXNlbGY6IGNlbnRlcjtcblxuICAgIH1cblxuXG4gICAgZGl2IHtcbiAgICAgICAgd2lkdGg6IDQ4JTtcbiAgICAgICAgYm9yZGVyLXN0eWxlOiBkYXNoZWQ7XG4gICAgICAgIGJvcmRlci1jb2xvcjogZ3JheTtcbiAgICAgICAgaGVpZ2h0OiAxcHg7XG4gICAgICAgIGJvcmRlci13aWR0aDogMXB4O1xuICAgICAgICBhbGlnbi1zZWxmOiBjZW50ZXI7XG4gICAgICAgIHBvc2l0aW9uOiBhYnNvbHV0ZTtcbiAgICAgICAgcmlnaHQ6IDE2JTtcbiAgICB9XG59XG5cbi5zbWFsbF9jYXJkX3JvdyB7XG5cbiAgICBpb24tY2FyZCB7XG4gICAgICAgIHdpZHRoOiAxMDAlO1xuICAgICAgICBtYXJnaW46IDA7XG5cbiAgICAgICAgaGVpZ2h0OiA4MHB4O1xuICAgICAgICBkaXNwbGF5OiBmbGV4O1xuICAgICAgICBwYWRkaW5nLWxlZnQ6IDUlO1xuXG4gXG4gICAgICAgIGlvbi1jYXJkLWNvbnRlbnQge1xuICAgICAgICAgICAgZGlzcGxheTogZmxleDtcbiAgICAgICAgICAgIGZsZXgtZGlyZWN0aW9uOiBjb2x1bW47ICAgIFxuICAgICAgICB9XG4gICAgfVxufVxuXG5cbi5iaWdfY2FyZF9yb3cge1xuXG4gICAgaW9uLWNhcmQge1xuICAgICAgICB3aWR0aDogMTAwJTtcbiAgICAgICAgbWFyZ2luOiAwO1xuXG4gICAgICAgIGhlaWdodDogMTMwcHg7XG4gICAgICAgIGRpc3BsYXk6IGZsZXg7XG4gICAgICAgIHBhZGRpbmctbGVmdDogNSU7XG5cbiBcbiAgICAgICAgaW9uLWNhcmQtY29udGVudCB7XG4gICAgICAgICAgICBkaXNwbGF5OiBmbGV4O1xuICAgICAgICAgICAgZmxleC1kaXJlY3Rpb246IHJvdzsgICAgXG5cbiAgICAgICAgICAgIGlvbi1pbWcge1xuICAgICAgICAgICAgICAgIHdpZHRoOiA5NXB4O1xuICAgICAgICAgICAgfVxuXG4gICAgICAgICAgICBpb24tbGFiZWwge1xuICAgICAgICAgICAgICAgIGFsaWduLXNlbGY6IGNlbnRlcjtcbiAgICAgICAgICAgICAgICBwYWRkaW5nLWxlZnQ6IDUlO1xuICAgICAgICAgICAgfVxuICAgICAgICB9XG4gICAgfVxufVxuIl19 */";
      /***/
    },

    /***/
    "./src/app/components/modals/manual-registration/manual-registration.component.ts":
    /*!****************************************************************************************!*\
      !*** ./src/app/components/modals/manual-registration/manual-registration.component.ts ***!
      \****************************************************************************************/

    /*! exports provided: ManualRegistrationComponent */

    /***/
    function srcAppComponentsModalsManualRegistrationManualRegistrationComponentTs(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "ManualRegistrationComponent", function () {
        return ManualRegistrationComponent;
      });
      /* harmony import */


      var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! tslib */
      "./node_modules/tslib/tslib.es6.js");
      /* harmony import */


      var _ionic_angular__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! @ionic/angular */
      "./node_modules/@ionic/angular/__ivy_ngcc__/fesm2015/ionic-angular.js");
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! @angular/core */
      "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
      /* harmony import */


      var hammerjs__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
      /*! hammerjs */
      "./node_modules/hammerjs/hammer.js");
      /* harmony import */


      var hammerjs__WEBPACK_IMPORTED_MODULE_3___default = /*#__PURE__*/__webpack_require__.n(hammerjs__WEBPACK_IMPORTED_MODULE_3__);

      var ManualRegistrationComponent = /*#__PURE__*/function () {
        function ManualRegistrationComponent(modalCtrl, gestureCtrl, element, renderer) {
          _classCallCheck(this, ManualRegistrationComponent);

          this.modalCtrl = modalCtrl;
          this.gestureCtrl = gestureCtrl;
          this.element = element;
          this.renderer = renderer;
        }

        _createClass(ManualRegistrationComponent, [{
          key: "ngOnInit",
          value: function ngOnInit() {}
        }, {
          key: "closeModal",
          value: function closeModal() {
            console.log("aa");
            this.modalCtrl.dismiss();
          }
        }, {
          key: "ngAfterViewInit",
          value: function ngAfterViewInit() {
            return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, /*#__PURE__*/regeneratorRuntime.mark(function _callee18() {
              var _this20 = this;

              var d, options, gesture;
              return regeneratorRuntime.wrap(function _callee18$(_context18) {
                while (1) {
                  switch (_context18.prev = _context18.next) {
                    case 0:
                      d = document.querySelector('ion-modal');
                      options = {
                        el: d,
                        direction: "y",
                        gestureName: 'swipe',
                        onStart: function onStart() {
                          _this20.renderer.setStyle(d, 'transition', 'none');
                        },
                        onMove: function onMove(ev) {
                          if (ev.deltaY > 0) {
                            _this20.renderer.setStyle(d, 'transform', "translateY(".concat(ev.deltaY, "px)"));
                          }
                        },
                        onEnd: function onEnd(ev) {
                          _this20.renderer.setStyle(d, 'transition', '0.5s ease-out');

                          if (ev.deltaY > -200) {
                            _this20.renderer.setStyle(d, 'transform', 'translateY(500px)');

                            _this20.modalCtrl.dismiss();
                          } else {
                            _this20.renderer.setStyle(d, 'transform', 'translateY(0px)');
                          }
                        }
                      };
                      _context18.next = 4;
                      return this.gestureCtrl.create(options);

                    case 4:
                      gesture = _context18.sent;
                      gesture.enable();

                    case 6:
                    case "end":
                      return _context18.stop();
                  }
                }
              }, _callee18, this);
            }));
          }
        }]);

        return ManualRegistrationComponent;
      }();

      ManualRegistrationComponent.ctorParameters = function () {
        return [{
          type: _ionic_angular__WEBPACK_IMPORTED_MODULE_1__["ModalController"]
        }, {
          type: _ionic_angular__WEBPACK_IMPORTED_MODULE_1__["GestureController"]
        }, {
          type: _angular_core__WEBPACK_IMPORTED_MODULE_2__["ElementRef"]
        }, {
          type: _angular_core__WEBPACK_IMPORTED_MODULE_2__["Renderer2"]
        }];
      };

      ManualRegistrationComponent = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([Object(_angular_core__WEBPACK_IMPORTED_MODULE_2__["Component"])({
        selector: 'app-manual-registration',
        template: Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(
        /*! raw-loader!./manual-registration.component.html */
        "./node_modules/raw-loader/dist/cjs.js!./src/app/components/modals/manual-registration/manual-registration.component.html"))["default"],
        styles: [Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(
        /*! ./manual-registration.component.scss */
        "./src/app/components/modals/manual-registration/manual-registration.component.scss"))["default"]]
      })], ManualRegistrationComponent);
      /***/
    },

    /***/
    "./src/app/components/modals/my-devices-modal/my-devices-modal.component.scss":
    /*!************************************************************************************!*\
      !*** ./src/app/components/modals/my-devices-modal/my-devices-modal.component.scss ***!
      \************************************************************************************/

    /*! exports provided: default */

    /***/
    function srcAppComponentsModalsMyDevicesModalMyDevicesModalComponentScss(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony default export */


      __webpack_exports__["default"] = "ion-toolbar {\n  border-bottom: 1px solid #c8c8c8;\n  padding-right: 10px;\n}\nion-toolbar ion-text {\n  display: block;\n}\nion-toolbar ion-buttons ion-button ion-img {\n  width: 30px;\n}\n.close-btn-icon {\n  font-size: 29px;\n  font-weight: 700;\n}\n.list_device_row ion-item {\n  width: 100%;\n}\n.list_device_row ion-item div ion-img {\n  width: 50px;\n}\n.list_device_row ion-item .list_section_title {\n  padding-left: 7%;\n}\n.list_device_row ion-item .menu-icon {\n  position: absolute;\n  right: 5px;\n  font-size: 25px;\n  color: gray;\n}\n.line {\n  width: 100px;\n  border-bottom: 2px solid black;\n  padding-bottom: 15px;\n  position: relative;\n}\n.line:before,\n.line:after {\n  position: absolute;\n  bottom: -6px;\n  left: 0;\n  height: 10px;\n  width: 10px;\n  background: black;\n  content: \"\";\n  border-radius: 5px;\n}\n.line:after {\n  right: 0;\n  left: auto;\n}\n.logo_compare_section ion-grid .device_section {\n  display: flex;\n  flex-direction: row;\n  justify-content: center;\n}\n.logo_compare_section ion-grid .device_section ion-img {\n  width: 50px;\n}\n.logo_compare_section ion-grid .device_section .logo_one {\n  margin-right: 10px;\n}\n.logo_compare_section ion-grid .device_section div {\n  align-self: center;\n  margin-right: 10px;\n  padding: 0;\n}\n.logo_compare_section ion-grid .description_section_one ion-label {\n  text-align: center;\n}\n.device_compare_body_section {\n  display: flex;\n  flex-direction: row;\n  justify-content: center;\n  padding-left: 25px;\n  padding-right: 25px;\n}\n.device_compare_body_section ion-label {\n  text-align: center;\n}\nion-footer ion-button {\n  width: 100%;\n  --background: var(--ion-btn-custom-color) ;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvY29tcG9uZW50cy9tb2RhbHMvbXktZGV2aWNlcy1tb2RhbC9teS1kZXZpY2VzLW1vZGFsLmNvbXBvbmVudC5zY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUVBO0VBQ0ksZ0NBQUE7RUFFQSxtQkFBQTtBQUZKO0FBSUk7RUFDSSxjQUFBO0FBRlI7QUFPWTtFQUNJLFdBQUE7QUFMaEI7QUFZQTtFQUNJLGVBQUE7RUFDQSxnQkFBQTtBQVRKO0FBYUk7RUFDSSxXQUFBO0FBVlI7QUFlWTtFQUNJLFdBQUE7QUFiaEI7QUFrQlE7RUFDSSxnQkFBQTtBQWhCWjtBQW1CUTtFQUNJLGtCQUFBO0VBQ0EsVUFBQTtFQUNBLGVBQUE7RUFDQSxXQUFBO0FBakJaO0FBd0JBO0VBQ0ksWUFBQTtFQUNBLDhCQUFBO0VBQ0Esb0JBQUE7RUFDQSxrQkFBQTtBQXJCSjtBQXdCQTs7RUFFSSxrQkFBQTtFQUNBLFlBQUE7RUFDQSxPQUFBO0VBQ0EsWUFBQTtFQUNBLFdBQUE7RUFDQSxpQkFBQTtFQUNBLFdBQUE7RUFDQSxrQkFBQTtBQXJCSjtBQXdCQTtFQUNJLFFBQUE7RUFDQSxVQUFBO0FBckJKO0FBMEJRO0VBQ0ksYUFBQTtFQUNBLG1CQUFBO0VBQ0EsdUJBQUE7QUF2Qlo7QUF3Qlk7RUFDSSxXQUFBO0FBdEJoQjtBQXlCWTtFQUNJLGtCQUFBO0FBdkJoQjtBQTJCWTtFQUNJLGtCQUFBO0VBQ0Esa0JBQUE7RUFDQSxVQUFBO0FBekJoQjtBQThCWTtFQUNJLGtCQUFBO0FBNUJoQjtBQWtDQTtFQUNJLGFBQUE7RUFDQSxtQkFBQTtFQUNBLHVCQUFBO0VBRUEsa0JBQUE7RUFDQSxtQkFBQTtBQWhDSjtBQW1DSTtFQUNJLGtCQUFBO0FBakNSO0FBd0NJO0VBQ0ksV0FBQTtFQUNBLDBDQUFBO0FBckNSIiwiZmlsZSI6InNyYy9hcHAvY29tcG9uZW50cy9tb2RhbHMvbXktZGV2aWNlcy1tb2RhbC9teS1kZXZpY2VzLW1vZGFsLmNvbXBvbmVudC5zY3NzIiwic291cmNlc0NvbnRlbnQiOlsiaW9uLWNvbnRlbnQge31cblxuaW9uLXRvb2xiYXIge1xuICAgIGJvcmRlci1ib3R0b206IDFweCBzb2xpZCAjYzhjOGM4O1xuICAgIC8vIGhlaWdodDogNyU7XG4gICAgcGFkZGluZy1yaWdodDogMTBweDtcblxuICAgIGlvbi10ZXh0IHtcbiAgICAgICAgZGlzcGxheTogYmxvY2s7XG4gICAgfVxuXG4gICAgaW9uLWJ1dHRvbnMge1xuICAgICAgICBpb24tYnV0dG9uIHtcbiAgICAgICAgICAgIGlvbi1pbWcge1xuICAgICAgICAgICAgICAgIHdpZHRoOiAzMHB4O1xuICAgICAgICAgICAgfVxuICAgICAgICB9XG4gICAgfVxufVxuXG5cbi5jbG9zZS1idG4taWNvbiB7XG4gICAgZm9udC1zaXplOiAyOXB4O1xuICAgIGZvbnQtd2VpZ2h0OiA3MDA7XG59XG5cbi5saXN0X2RldmljZV9yb3cge1xuICAgIGlvbi1pdGVtIHtcbiAgICAgICAgd2lkdGg6IDEwMCU7XG5cblxuXG4gICAgICAgIGRpdiB7XG4gICAgICAgICAgICBpb24taW1nIHtcbiAgICAgICAgICAgICAgICB3aWR0aDogNTBweDtcbiAgICAgICAgICAgIH1cbiAgICAgICAgfVxuXG5cbiAgICAgICAgLmxpc3Rfc2VjdGlvbl90aXRsZSB7XG4gICAgICAgICAgICBwYWRkaW5nLWxlZnQ6IDclO1xuICAgICAgICB9XG5cbiAgICAgICAgLm1lbnUtaWNvbiB7XG4gICAgICAgICAgICBwb3NpdGlvbjogYWJzb2x1dGU7XG4gICAgICAgICAgICByaWdodDogNXB4O1xuICAgICAgICAgICAgZm9udC1zaXplOiAyNXB4O1xuICAgICAgICAgICAgY29sb3I6IGdyYXk7XG4gICAgICAgIH1cbiAgICB9XG59XG5cblxuXG4ubGluZSB7XG4gICAgd2lkdGg6IDEwMHB4O1xuICAgIGJvcmRlci1ib3R0b206IDJweCBzb2xpZCBibGFjazsgXG4gICAgcGFkZGluZy1ib3R0b206IDE1cHg7IFxuICAgIHBvc2l0aW9uOiByZWxhdGl2ZTtcbn1cblxuLmxpbmU6YmVmb3JlLCBcbi5saW5lOmFmdGVyIHtcbiAgICBwb3NpdGlvbjogYWJzb2x1dGU7IFxuICAgIGJvdHRvbTogLTZweDsgXG4gICAgbGVmdDogMDsgXG4gICAgaGVpZ2h0OiAxMHB4OyBcbiAgICB3aWR0aDogMTBweDsgXG4gICAgYmFja2dyb3VuZDogYmxhY2s7IFxuICAgIGNvbnRlbnQ6IFwiXCI7IFxuICAgIGJvcmRlci1yYWRpdXM6IDVweDtcbn1cblxuLmxpbmU6YWZ0ZXIge1xuICAgIHJpZ2h0OiAwOyBcbiAgICBsZWZ0OiBhdXRvO1xufVxuXG4ubG9nb19jb21wYXJlX3NlY3Rpb24ge1xuICAgIGlvbi1ncmlkIHtcbiAgICAgICAgLmRldmljZV9zZWN0aW9uIHtcbiAgICAgICAgICAgIGRpc3BsYXk6IGZsZXg7XG4gICAgICAgICAgICBmbGV4LWRpcmVjdGlvbjogcm93O1xuICAgICAgICAgICAganVzdGlmeS1jb250ZW50OiBjZW50ZXI7XG4gICAgICAgICAgICBpb24taW1nIHtcbiAgICAgICAgICAgICAgICB3aWR0aDogNTBweDtcbiAgICAgICAgICAgIH1cblxuICAgICAgICAgICAgLmxvZ29fb25lIHtcbiAgICAgICAgICAgICAgICBtYXJnaW4tcmlnaHQ6IDEwcHg7XG4gICAgICAgICAgICB9XG5cblxuICAgICAgICAgICAgZGl2IHtcbiAgICAgICAgICAgICAgICBhbGlnbi1zZWxmOiBjZW50ZXI7XG4gICAgICAgICAgICAgICAgbWFyZ2luLXJpZ2h0OiAxMHB4O1xuICAgICAgICAgICAgICAgIHBhZGRpbmc6IDA7XG4gICAgICAgICAgICB9XG4gICAgICAgIH1cblxuICAgICAgICAuZGVzY3JpcHRpb25fc2VjdGlvbl9vbmUge1xuICAgICAgICAgICAgaW9uLWxhYmVsIHtcbiAgICAgICAgICAgICAgICB0ZXh0LWFsaWduOiBjZW50ZXI7XG4gICAgICAgICAgICB9XG4gICAgICAgIH1cbiAgICB9XG59XG5cbi5kZXZpY2VfY29tcGFyZV9ib2R5X3NlY3Rpb24ge1xuICAgIGRpc3BsYXk6IGZsZXg7XG4gICAgZmxleC1kaXJlY3Rpb246IHJvdztcbiAgICBqdXN0aWZ5LWNvbnRlbnQ6IGNlbnRlcjtcbiBcbiAgICBwYWRkaW5nLWxlZnQ6IDI1cHg7XG4gICAgcGFkZGluZy1yaWdodDogMjVweDtcblxuXG4gICAgaW9uLWxhYmVsIHtcbiAgICAgICAgdGV4dC1hbGlnbjogY2VudGVyO1xuICAgIH1cbn1cblxuXG5cbmlvbi1mb290ZXIge1xuICAgIGlvbi1idXR0b24ge1xuICAgICAgICB3aWR0aDogMTAwJTtcbiAgICAgICAgLS1iYWNrZ3JvdW5kOiB2YXIoLS1pb24tYnRuLWN1c3RvbS1jb2xvcilcbiAgICB9XG59Il19 */";
      /***/
    },

    /***/
    "./src/app/components/modals/my-devices-modal/my-devices-modal.component.ts":
    /*!**********************************************************************************!*\
      !*** ./src/app/components/modals/my-devices-modal/my-devices-modal.component.ts ***!
      \**********************************************************************************/

    /*! exports provided: MyDevicesModalComponent */

    /***/
    function srcAppComponentsModalsMyDevicesModalMyDevicesModalComponentTs(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "MyDevicesModalComponent", function () {
        return MyDevicesModalComponent;
      });
      /* harmony import */


      var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! tslib */
      "./node_modules/tslib/tslib.es6.js");
      /* harmony import */


      var _ionic_angular__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! @ionic/angular */
      "./node_modules/@ionic/angular/__ivy_ngcc__/fesm2015/ionic-angular.js");
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! @angular/core */
      "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
      /* harmony import */


      var hammerjs__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
      /*! hammerjs */
      "./node_modules/hammerjs/hammer.js");
      /* harmony import */


      var hammerjs__WEBPACK_IMPORTED_MODULE_3___default = /*#__PURE__*/__webpack_require__.n(hammerjs__WEBPACK_IMPORTED_MODULE_3__);

      var MyDevicesModalComponent = /*#__PURE__*/function () {
        function MyDevicesModalComponent(modalCtrl, gestureCtrl, element, renderer) {
          _classCallCheck(this, MyDevicesModalComponent);

          this.modalCtrl = modalCtrl;
          this.gestureCtrl = gestureCtrl;
          this.element = element;
          this.renderer = renderer;
          this.showRow = "first";
        }

        _createClass(MyDevicesModalComponent, [{
          key: "ngOnInit",
          value: function ngOnInit() {}
        }, {
          key: "closeModal",
          value: function closeModal() {
            // this.modalCtrl.dismiss()
            if (this.showRow == 'third') {
              this.modalCtrl.dismiss();
            } else {
              this.showRow = "second";
            }
          }
        }, {
          key: "ngAfterViewInit",
          value: function ngAfterViewInit() {
            return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, /*#__PURE__*/regeneratorRuntime.mark(function _callee19() {
              var _this21 = this;

              var d, options, gesture;
              return regeneratorRuntime.wrap(function _callee19$(_context19) {
                while (1) {
                  switch (_context19.prev = _context19.next) {
                    case 0:
                      d = document.querySelector('ion-modal');
                      options = {
                        el: d,
                        direction: "y",
                        gestureName: 'swipe',
                        onStart: function onStart() {
                          _this21.renderer.setStyle(d, 'transition', 'none');
                        },
                        onMove: function onMove(ev) {
                          if (ev.deltaY > 0) {
                            _this21.renderer.setStyle(d, 'transform', "translateY(".concat(ev.deltaY, "px)"));
                          }
                        },
                        onEnd: function onEnd(ev) {
                          _this21.renderer.setStyle(d, 'transition', '0.5s ease-out');

                          if (ev.deltaY > -200) {
                            _this21.renderer.setStyle(d, 'transform', 'translateY(500px)');

                            _this21.modalCtrl.dismiss();
                          } else {
                            _this21.renderer.setStyle(d, 'transform', 'translateY(0px)');
                          }
                        }
                      };
                      _context19.next = 4;
                      return this.gestureCtrl.create(options);

                    case 4:
                      gesture = _context19.sent;
                      gesture.enable();

                    case 6:
                    case "end":
                      return _context19.stop();
                  }
                }
              }, _callee19, this);
            }));
          }
        }]);

        return MyDevicesModalComponent;
      }();

      MyDevicesModalComponent.ctorParameters = function () {
        return [{
          type: _ionic_angular__WEBPACK_IMPORTED_MODULE_1__["ModalController"]
        }, {
          type: _ionic_angular__WEBPACK_IMPORTED_MODULE_1__["GestureController"]
        }, {
          type: _angular_core__WEBPACK_IMPORTED_MODULE_2__["ElementRef"]
        }, {
          type: _angular_core__WEBPACK_IMPORTED_MODULE_2__["Renderer2"]
        }];
      };

      MyDevicesModalComponent = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([Object(_angular_core__WEBPACK_IMPORTED_MODULE_2__["Component"])({
        selector: 'app-my-devices-modal',
        template: Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(
        /*! raw-loader!./my-devices-modal.component.html */
        "./node_modules/raw-loader/dist/cjs.js!./src/app/components/modals/my-devices-modal/my-devices-modal.component.html"))["default"],
        styles: [Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(
        /*! ./my-devices-modal.component.scss */
        "./src/app/components/modals/my-devices-modal/my-devices-modal.component.scss"))["default"]]
      })], MyDevicesModalComponent);
      /***/
    },

    /***/
    "./src/app/components/modals/my-diet-home-modal/my-diet-home-modal.component.scss":
    /*!****************************************************************************************!*\
      !*** ./src/app/components/modals/my-diet-home-modal/my-diet-home-modal.component.scss ***!
      \****************************************************************************************/

    /*! exports provided: default */

    /***/
    function srcAppComponentsModalsMyDietHomeModalMyDietHomeModalComponentScss(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony default export */


      __webpack_exports__["default"] = "ion-toolbar {\n  border-bottom: 1px solid #d0d0d0;\n  height: 9%;\n}\nion-toolbar ion-text {\n  display: block;\n}\n.steptwofirstbtn ion-button {\n  white-space: normal;\n  font-size: 12px;\n  --padding-start: 0;\n  --padding-end: 0;\n  height: 50px;\n}\n.plan_btn_section ion-col {\n  padding: 0;\n}\n.plan_btn_section ion-col .steptwofirstbtn ion-button {\n  margin: 0;\n}\n.select_box_section ion-col {\n  padding: 0;\n}\n.select_box_section ion-col ion-item {\n  --padding-start: 0;\n  --padding-end: 0;\n}\n.select_box_section ion-col ion-item ion-select {\n  position: absolute;\n  right: 0%;\n}\n.masculation_section ion-col {\n  padding: 0;\n}\n.coachingathlete {\n  margin-top: 5px;\n  --inner-border-width: 0 0 0px 0;\n  background: linear-gradient(to right, #ffece3ff, #ffc9a4ff, #ffece3ff);\n  display: flex;\n  justify-content: center;\n  height: 50px;\n}\n.coachingathlete ion-avatar {\n  padding: 18px;\n  align-self: center;\n}\n.coachingathlete ion-label {\n  align-self: center;\n}\nion-footer ion-button {\n  width: 100%;\n  --background: var(--ion-btn-custom-color);\n  font-size: 12px;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvY29tcG9uZW50cy9tb2RhbHMvbXktZGlldC1ob21lLW1vZGFsL215LWRpZXQtaG9tZS1tb2RhbC5jb21wb25lbnQuc2NzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFDQTtFQUNJLGdDQUFBO0VBQ0EsVUFBQTtBQUFKO0FBRUk7RUFDSSxjQUFBO0FBQVI7QUFLQTtFQUNJLG1CQUFBO0VBQ0EsZUFBQTtFQUNBLGtCQUFBO0VBQ0EsZ0JBQUE7RUFDQSxZQUFBO0FBRko7QUFNSTtFQUNJLFVBQUE7QUFIUjtBQU1ZO0VBRUksU0FBQTtBQUxoQjtBQVlJO0VBQ0ksVUFBQTtBQVRSO0FBV1E7RUFDSSxrQkFBQTtFQUNBLGdCQUFBO0FBVFo7QUFXWTtFQUNJLGtCQUFBO0VBQ0EsU0FBQTtBQVRoQjtBQWtCSTtFQUNJLFVBQUE7QUFmUjtBQWtCQTtFQUVJLGVBQUE7RUFDQSwrQkFBQTtFQUNBLHNFQUFBO0VBQ0EsYUFBQTtFQUNBLHVCQUFBO0VBQ0EsWUFBQTtBQWhCSjtBQWtCSTtFQUNJLGFBQUE7RUFDQSxrQkFBQTtBQWhCUjtBQWtCSTtFQUNJLGtCQUFBO0FBaEJSO0FBc0JJO0VBQ0ksV0FBQTtFQUNBLHlDQUFBO0VBQ0EsZUFBQTtBQW5CUiIsImZpbGUiOiJzcmMvYXBwL2NvbXBvbmVudHMvbW9kYWxzL215LWRpZXQtaG9tZS1tb2RhbC9teS1kaWV0LWhvbWUtbW9kYWwuY29tcG9uZW50LnNjc3MiLCJzb3VyY2VzQ29udGVudCI6WyJcbmlvbi10b29sYmFyIHtcbiAgICBib3JkZXItYm90dG9tOiAxcHggc29saWQgI2QwZDBkMDtcbiAgICBoZWlnaHQ6IDklO1xuXG4gICAgaW9uLXRleHQge1xuICAgICAgICBkaXNwbGF5OiBibG9jaztcbiAgICB9XG59XG5cblxuLnN0ZXB0d29maXJzdGJ0biBpb24tYnV0dG9uIHtcbiAgICB3aGl0ZS1zcGFjZTogbm9ybWFsO1xuICAgIGZvbnQtc2l6ZTogMTJweDtcbiAgICAtLXBhZGRpbmctc3RhcnQ6IDA7XG4gICAgLS1wYWRkaW5nLWVuZDogMDtcbiAgICBoZWlnaHQ6IDUwcHg7XG59XG5cbi5wbGFuX2J0bl9zZWN0aW9uIHtcbiAgICBpb24tY29sIHtcbiAgICAgICAgcGFkZGluZzogMDtcblxuICAgICAgICAuc3RlcHR3b2ZpcnN0YnRuIHtcbiAgICAgICAgICAgIGlvbi1idXR0b24ge1xuICAgICAgICAgICAgICAgIC8vIGhlaWdodDogNzJweDtcbiAgICAgICAgICAgICAgICBtYXJnaW46IDA7XG4gICAgICAgICAgICB9XG4gICAgICAgIH1cbiAgICB9XG59XG5cbi5zZWxlY3RfYm94X3NlY3Rpb24ge1xuICAgIGlvbi1jb2wge1xuICAgICAgICBwYWRkaW5nOiAwO1xuXG4gICAgICAgIGlvbi1pdGVtIHtcbiAgICAgICAgICAgIC0tcGFkZGluZy1zdGFydDogMDtcbiAgICAgICAgICAgIC0tcGFkZGluZy1lbmQ6IDA7XG5cbiAgICAgICAgICAgIGlvbi1zZWxlY3Qge1xuICAgICAgICAgICAgICAgIHBvc2l0aW9uOiBhYnNvbHV0ZTtcbiAgICAgICAgICAgICAgICByaWdodDogMCU7XG4gICAgICAgICAgICB9XG5cbiAgICAgICAgfVxuICAgIH1cbn1cblxuXG4ubWFzY3VsYXRpb25fc2VjdGlvbiB7XG4gICAgaW9uLWNvbCB7XG4gICAgICAgIHBhZGRpbmc6IDA7XG4gICAgfVxufVxuLmNvYWNoaW5nYXRobGV0ZSB7XG5cbiAgICBtYXJnaW4tdG9wOiA1cHg7XG4gICAgLS1pbm5lci1ib3JkZXItd2lkdGg6IDAgMCAwcHggMDtcbiAgICBiYWNrZ3JvdW5kOiBsaW5lYXItZ3JhZGllbnQodG8gcmlnaHQsI2ZmZWNlM2ZmLCAjZmZjOWE0ZmYsICNmZmVjZTNmZik7XG4gICAgZGlzcGxheTogZmxleDtcbiAgICBqdXN0aWZ5LWNvbnRlbnQ6IGNlbnRlcjtcbiAgICBoZWlnaHQ6IDUwcHg7XG5cbiAgICBpb24tYXZhdGFyIHtcbiAgICAgICAgcGFkZGluZzogMThweDtcbiAgICAgICAgYWxpZ24tc2VsZjogY2VudGVyO1xuICAgIH1cbiAgICBpb24tbGFiZWwge1xuICAgICAgICBhbGlnbi1zZWxmOiBjZW50ZXI7XG4gICAgfVxufVxuXG5cbmlvbi1mb290ZXIge1xuICAgIGlvbi1idXR0b24ge1xuICAgICAgICB3aWR0aDogMTAwJTtcbiAgICAgICAgLS1iYWNrZ3JvdW5kOiB2YXIoLS1pb24tYnRuLWN1c3RvbS1jb2xvcik7XG4gICAgICAgIGZvbnQtc2l6ZTogMTJweDtcbiAgICAgICAgXG4gICAgfVxufSJdfQ== */";
      /***/
    },

    /***/
    "./src/app/components/modals/my-diet-home-modal/my-diet-home-modal.component.ts":
    /*!**************************************************************************************!*\
      !*** ./src/app/components/modals/my-diet-home-modal/my-diet-home-modal.component.ts ***!
      \**************************************************************************************/

    /*! exports provided: MyDietHomeModalComponent */

    /***/
    function srcAppComponentsModalsMyDietHomeModalMyDietHomeModalComponentTs(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "MyDietHomeModalComponent", function () {
        return MyDietHomeModalComponent;
      });
      /* harmony import */


      var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! tslib */
      "./node_modules/tslib/tslib.es6.js");
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! @angular/core */
      "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
      /* harmony import */


      var _ionic_angular__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! @ionic/angular */
      "./node_modules/@ionic/angular/__ivy_ngcc__/fesm2015/ionic-angular.js");
      /* harmony import */


      var hammerjs__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
      /*! hammerjs */
      "./node_modules/hammerjs/hammer.js");
      /* harmony import */


      var hammerjs__WEBPACK_IMPORTED_MODULE_3___default = /*#__PURE__*/__webpack_require__.n(hammerjs__WEBPACK_IMPORTED_MODULE_3__);

      var MyDietHomeModalComponent = /*#__PURE__*/function () {
        function MyDietHomeModalComponent(gestureCtrl, element, renderer, modalCtrl) {
          _classCallCheck(this, MyDietHomeModalComponent);

          this.gestureCtrl = gestureCtrl;
          this.element = element;
          this.renderer = renderer;
          this.modalCtrl = modalCtrl;
          this.creationMode = "deux";
          this.plansBtn = "Gain muscle";
        }

        _createClass(MyDietHomeModalComponent, [{
          key: "ngOnInit",
          value: function ngOnInit() {}
        }, {
          key: "ngAfterViewInit",
          value: function ngAfterViewInit() {
            return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, /*#__PURE__*/regeneratorRuntime.mark(function _callee20() {
              var _this22 = this;

              var d, options, gesture;
              return regeneratorRuntime.wrap(function _callee20$(_context20) {
                while (1) {
                  switch (_context20.prev = _context20.next) {
                    case 0:
                      d = document.querySelector('ion-modal');
                      options = {
                        el: d,
                        direction: "y",
                        gestureName: 'swipe',
                        onStart: function onStart() {
                          _this22.renderer.setStyle(d, 'transition', 'none');
                        },
                        onMove: function onMove(ev) {
                          if (ev.deltaY > 0) {
                            _this22.renderer.setStyle(d, 'transform', "translateY(".concat(ev.deltaY, "px)"));
                          }
                        },
                        onEnd: function onEnd(ev) {
                          _this22.renderer.setStyle(d, 'transition', '0.5s ease-out');

                          if (ev.deltaY > -200) {
                            _this22.renderer.setStyle(d, 'transform', 'translateY(500px)');

                            _this22.modalCtrl.dismiss();
                          } else {
                            _this22.renderer.setStyle(d, 'transform', 'translateY(0px)');
                          }
                        }
                      };
                      _context20.next = 4;
                      return this.gestureCtrl.create(options);

                    case 4:
                      gesture = _context20.sent;
                      gesture.enable();

                    case 6:
                    case "end":
                      return _context20.stop();
                  }
                }
              }, _callee20, this);
            }));
          }
        }]);

        return MyDietHomeModalComponent;
      }();

      MyDietHomeModalComponent.ctorParameters = function () {
        return [{
          type: _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["GestureController"]
        }, {
          type: _angular_core__WEBPACK_IMPORTED_MODULE_1__["ElementRef"]
        }, {
          type: _angular_core__WEBPACK_IMPORTED_MODULE_1__["Renderer2"]
        }, {
          type: _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["ModalController"]
        }];
      };

      MyDietHomeModalComponent = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-my-diet-home-modal',
        template: Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(
        /*! raw-loader!./my-diet-home-modal.component.html */
        "./node_modules/raw-loader/dist/cjs.js!./src/app/components/modals/my-diet-home-modal/my-diet-home-modal.component.html"))["default"],
        styles: [Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(
        /*! ./my-diet-home-modal.component.scss */
        "./src/app/components/modals/my-diet-home-modal/my-diet-home-modal.component.scss"))["default"]]
      })], MyDietHomeModalComponent);
      /***/
    },

    /***/
    "./src/app/components/modals/my-diet-modal/my-diet-modal.component.scss":
    /*!******************************************************************************!*\
      !*** ./src/app/components/modals/my-diet-modal/my-diet-modal.component.scss ***!
      \******************************************************************************/

    /*! exports provided: default */

    /***/
    function srcAppComponentsModalsMyDietModalMyDietModalComponentScss(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony default export */


      __webpack_exports__["default"] = "ion-content {\n  --padding-start: 3%;\n  --padding-end: 3%;\n}\n\nion-toolbar {\n  border-bottom: 1px solid #f1f1f1;\n  height: 9%;\n}\n\nion-toolbar ion-text {\n  display: block;\n}\n\n.close-btn-icon {\n  font-size: 29px;\n  font-weight: 700;\n}\n\n.toggle_btn_section {\n  width: 100%;\n  --padding-start: 0;\n}\n\n.toggle_btn_section ion-toggle {\n  height: 25px;\n}\n\nion-avatar {\n  padding: 15px;\n}\n\n.header_row {\n  display: flex;\n  flex-direction: row;\n  justify-content: space-between;\n}\n\n.header_row ion-label {\n  align-self: center;\n}\n\n.header_row div {\n  width: 48%;\n  border-style: dashed;\n  border-color: gray;\n  height: 1px;\n  border-width: 1px;\n  align-self: center;\n  position: absolute;\n  right: 16%;\n}\n\n.main_line_section {\n  display: flex;\n  flex-direction: row;\n  height: 24px;\n}\n\n.main_line_section .yellow-back {\n  background: var(--ion-color-primary);\n  width: 20px;\n  height: 4px;\n  align-self: center;\n}\n\n.main_line_section .black-back {\n  background: #000;\n  width: 20px;\n  height: 4px;\n  align-self: center;\n}\n\n.card_item_section ion-col ion-card {\n  height: 150px;\n  margin: 0;\n}\n\n.card_item_section ion-col ion-card ion-card-header ion-button {\n  width: 100%;\n  --background: #2a2a2a;\n}\n\n.card_item_section ion-col ion-card ion-card-content {\n  display: flex;\n  justify-content: center;\n  flex-direction: column;\n}\n\n.card_item_section ion-col ion-card ion-card-content ion-img {\n  width: 25px;\n  background: #bebebe;\n  border-radius: 50%;\n  align-self: center;\n}\n\n.card_item_section ion-col ion-card ion-card-content p {\n  align-self: center;\n}\n\n.card_footer_data p {\n  margin: 0;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvY29tcG9uZW50cy9tb2RhbHMvbXktZGlldC1tb2RhbC9teS1kaWV0LW1vZGFsLmNvbXBvbmVudC5zY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBO0VBQ0ksbUJBQUE7RUFDQSxpQkFBQTtBQUNKOztBQUVBO0VBQ0ksZ0NBQUE7RUFDQSxVQUFBO0FBQ0o7O0FBQ0k7RUFDSSxjQUFBO0FBQ1I7O0FBSUE7RUFDSSxlQUFBO0VBQ0EsZ0JBQUE7QUFESjs7QUFJQTtFQUNJLFdBQUE7RUFDQSxrQkFBQTtBQURKOztBQUdJO0VBQ0ksWUFBQTtBQURSOztBQVFBO0VBQ0ksYUFBQTtBQUxKOztBQVFBO0VBQ0ksYUFBQTtFQUNBLG1CQUFBO0VBQ0EsOEJBQUE7QUFMSjs7QUFPSTtFQUNJLGtCQUFBO0FBTFI7O0FBVUk7RUFDSSxVQUFBO0VBQ0Esb0JBQUE7RUFDQSxrQkFBQTtFQUNBLFdBQUE7RUFDQSxpQkFBQTtFQUNBLGtCQUFBO0VBQ0Esa0JBQUE7RUFDQSxVQUFBO0FBUlI7O0FBWUE7RUFDSSxhQUFBO0VBQ0EsbUJBQUE7RUFDQSxZQUFBO0FBVEo7O0FBV0k7RUFDSSxvQ0FBQTtFQUNBLFdBQUE7RUFDQSxXQUFBO0VBQ0Esa0JBQUE7QUFUUjs7QUFZSTtFQUNJLGdCQUFBO0VBQ0EsV0FBQTtFQUNBLFdBQUE7RUFDQSxrQkFBQTtBQVZSOztBQWtCUTtFQUNJLGFBQUE7RUFDQSxTQUFBO0FBZlo7O0FBa0JnQjtFQUNJLFdBQUE7RUFDQSxxQkFBQTtBQWhCcEI7O0FBb0JZO0VBQ0ksYUFBQTtFQUNBLHVCQUFBO0VBQ0Esc0JBQUE7QUFsQmhCOztBQW1CZ0I7RUFDSSxXQUFBO0VBQ0EsbUJBQUE7RUFDQSxrQkFBQTtFQUNBLGtCQUFBO0FBakJwQjs7QUFvQmdCO0VBQ0ksa0JBQUE7QUFsQnBCOztBQTRCSTtFQUNJLFNBQUE7QUF6QlIiLCJmaWxlIjoic3JjL2FwcC9jb21wb25lbnRzL21vZGFscy9teS1kaWV0LW1vZGFsL215LWRpZXQtbW9kYWwuY29tcG9uZW50LnNjc3MiLCJzb3VyY2VzQ29udGVudCI6WyJpb24tY29udGVudCB7XG4gICAgLS1wYWRkaW5nLXN0YXJ0OiAzJTtcbiAgICAtLXBhZGRpbmctZW5kOiAzJTtcbn1cblxuaW9uLXRvb2xiYXIge1xuICAgIGJvcmRlci1ib3R0b206IDFweCBzb2xpZCAjZjFmMWYxO1xuICAgIGhlaWdodDogOSU7XG5cbiAgICBpb24tdGV4dCB7XG4gICAgICAgIGRpc3BsYXk6IGJsb2NrO1xuICAgIH1cbn1cblxuXG4uY2xvc2UtYnRuLWljb24ge1xuICAgIGZvbnQtc2l6ZTogMjlweDtcbiAgICBmb250LXdlaWdodDogNzAwO1xufVxuXG4udG9nZ2xlX2J0bl9zZWN0aW9uIHtcbiAgICB3aWR0aDogMTAwJTtcbiAgICAtLXBhZGRpbmctc3RhcnQ6IDA7XG5cbiAgICBpb24tdG9nZ2xlIHtcbiAgICAgICAgaGVpZ2h0OiAyNXB4O1xuICAgIH1cbn1cblxuXG5cblxuaW9uLWF2YXRhciB7XG4gICAgcGFkZGluZzogMTVweDtcbn1cblxuLmhlYWRlcl9yb3cge1xuICAgIGRpc3BsYXk6IGZsZXg7XG4gICAgZmxleC1kaXJlY3Rpb246IHJvdztcbiAgICBqdXN0aWZ5LWNvbnRlbnQ6IHNwYWNlLWJldHdlZW47XG5cbiAgICBpb24tbGFiZWwge1xuICAgICAgICBhbGlnbi1zZWxmOiBjZW50ZXI7XG5cbiAgICB9XG5cblxuICAgIGRpdiB7XG4gICAgICAgIHdpZHRoOiA0OCU7XG4gICAgICAgIGJvcmRlci1zdHlsZTogZGFzaGVkO1xuICAgICAgICBib3JkZXItY29sb3I6IGdyYXk7XG4gICAgICAgIGhlaWdodDogMXB4O1xuICAgICAgICBib3JkZXItd2lkdGg6IDFweDtcbiAgICAgICAgYWxpZ24tc2VsZjogY2VudGVyO1xuICAgICAgICBwb3NpdGlvbjogYWJzb2x1dGU7XG4gICAgICAgIHJpZ2h0OiAxNiU7XG4gICAgfVxufVxuXG4ubWFpbl9saW5lX3NlY3Rpb24ge1xuICAgIGRpc3BsYXk6IGZsZXg7XG4gICAgZmxleC1kaXJlY3Rpb246IHJvdztcbiAgICBoZWlnaHQ6IDI0cHg7XG5cbiAgICAueWVsbG93LWJhY2sge1xuICAgICAgICBiYWNrZ3JvdW5kOiB2YXIoLS1pb24tY29sb3ItcHJpbWFyeSk7XG4gICAgICAgIHdpZHRoOiAyMHB4O1xuICAgICAgICBoZWlnaHQ6IDRweDtcbiAgICAgICAgYWxpZ24tc2VsZjogY2VudGVyO1xuICAgIH1cblxuICAgIC5ibGFjay1iYWNrIHtcbiAgICAgICAgYmFja2dyb3VuZDogIzAwMDtcbiAgICAgICAgd2lkdGg6IDIwcHg7XG4gICAgICAgIGhlaWdodDogNHB4O1xuICAgICAgICBhbGlnbi1zZWxmOiBjZW50ZXI7XG4gICAgfVxufVxuXG5cblxuLmNhcmRfaXRlbV9zZWN0aW9uIHtcbiAgICBpb24tY29sIHtcbiAgICAgICAgaW9uLWNhcmQge1xuICAgICAgICAgICAgaGVpZ2h0OiAxNTBweDtcbiAgICAgICAgICAgIG1hcmdpbjogMDtcblxuICAgICAgICAgICAgaW9uLWNhcmQtaGVhZGVyIHtcbiAgICAgICAgICAgICAgICBpb24tYnV0dG9uIHtcbiAgICAgICAgICAgICAgICAgICAgd2lkdGg6IDEwMCU7XG4gICAgICAgICAgICAgICAgICAgIC0tYmFja2dyb3VuZDogIzJhMmEyYTtcbiAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICB9XG5cbiAgICAgICAgICAgIGlvbi1jYXJkLWNvbnRlbnQge1xuICAgICAgICAgICAgICAgIGRpc3BsYXk6IGZsZXg7XG4gICAgICAgICAgICAgICAganVzdGlmeS1jb250ZW50OiBjZW50ZXI7XG4gICAgICAgICAgICAgICAgZmxleC1kaXJlY3Rpb246IGNvbHVtbjtcbiAgICAgICAgICAgICAgICBpb24taW1nIHtcbiAgICAgICAgICAgICAgICAgICAgd2lkdGg6IDI1cHg7XG4gICAgICAgICAgICAgICAgICAgIGJhY2tncm91bmQ6ICNiZWJlYmU7XG4gICAgICAgICAgICAgICAgICAgIGJvcmRlci1yYWRpdXM6IDUwJTtcbiAgICAgICAgICAgICAgICAgICAgYWxpZ24tc2VsZjogY2VudGVyO1xuICAgICAgICAgICAgICAgIH1cblxuICAgICAgICAgICAgICAgIHAge1xuICAgICAgICAgICAgICAgICAgICBhbGlnbi1zZWxmOiBjZW50ZXI7XG5cbiAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICB9XG4gICAgICAgIH1cbiAgICB9XG59XG5cblxuLmNhcmRfZm9vdGVyX2RhdGEge1xuICAgIHAge1xuICAgICAgICBtYXJnaW46IDA7XG4gICAgfVxufSJdfQ== */";
      /***/
    },

    /***/
    "./src/app/components/modals/my-diet-modal/my-diet-modal.component.ts":
    /*!****************************************************************************!*\
      !*** ./src/app/components/modals/my-diet-modal/my-diet-modal.component.ts ***!
      \****************************************************************************/

    /*! exports provided: MyDietModalComponent */

    /***/
    function srcAppComponentsModalsMyDietModalMyDietModalComponentTs(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "MyDietModalComponent", function () {
        return MyDietModalComponent;
      });
      /* harmony import */


      var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! tslib */
      "./node_modules/tslib/tslib.es6.js");
      /* harmony import */


      var _ionic_angular__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! @ionic/angular */
      "./node_modules/@ionic/angular/__ivy_ngcc__/fesm2015/ionic-angular.js");
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! @angular/core */
      "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
      /* harmony import */


      var hammerjs__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
      /*! hammerjs */
      "./node_modules/hammerjs/hammer.js");
      /* harmony import */


      var hammerjs__WEBPACK_IMPORTED_MODULE_3___default = /*#__PURE__*/__webpack_require__.n(hammerjs__WEBPACK_IMPORTED_MODULE_3__);

      var MyDietModalComponent = /*#__PURE__*/function () {
        function MyDietModalComponent(modalCtrl, gestureCtrl, element, renderer) {
          _classCallCheck(this, MyDietModalComponent);

          this.modalCtrl = modalCtrl;
          this.gestureCtrl = gestureCtrl;
          this.element = element;
          this.renderer = renderer;
        }

        _createClass(MyDietModalComponent, [{
          key: "ngOnInit",
          value: function ngOnInit() {}
        }, {
          key: "closeModal",
          value: function closeModal() {
            console.log("aa");
            this.modalCtrl.dismiss();
          }
        }, {
          key: "ngAfterViewInit",
          value: function ngAfterViewInit() {
            return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, /*#__PURE__*/regeneratorRuntime.mark(function _callee21() {
              var _this23 = this;

              var d, options, gesture;
              return regeneratorRuntime.wrap(function _callee21$(_context21) {
                while (1) {
                  switch (_context21.prev = _context21.next) {
                    case 0:
                      d = document.querySelector('ion-modal');
                      options = {
                        el: d,
                        direction: "y",
                        gestureName: 'swipe',
                        onStart: function onStart() {
                          _this23.renderer.setStyle(d, 'transition', 'none');
                        },
                        onMove: function onMove(ev) {
                          if (ev.deltaY > 0) {
                            _this23.renderer.setStyle(d, 'transform', "translateY(".concat(ev.deltaY, "px)"));
                          }
                        },
                        onEnd: function onEnd(ev) {
                          _this23.renderer.setStyle(d, 'transition', '0.5s ease-out');

                          if (ev.deltaY > -200) {
                            _this23.renderer.setStyle(d, 'transform', 'translateY(500px)');

                            _this23.modalCtrl.dismiss();
                          } else {
                            _this23.renderer.setStyle(d, 'transform', 'translateY(0px)');
                          }
                        }
                      };
                      _context21.next = 4;
                      return this.gestureCtrl.create(options);

                    case 4:
                      gesture = _context21.sent;
                      gesture.enable();

                    case 6:
                    case "end":
                      return _context21.stop();
                  }
                }
              }, _callee21, this);
            }));
          }
        }]);

        return MyDietModalComponent;
      }();

      MyDietModalComponent.ctorParameters = function () {
        return [{
          type: _ionic_angular__WEBPACK_IMPORTED_MODULE_1__["ModalController"]
        }, {
          type: _ionic_angular__WEBPACK_IMPORTED_MODULE_1__["GestureController"]
        }, {
          type: _angular_core__WEBPACK_IMPORTED_MODULE_2__["ElementRef"]
        }, {
          type: _angular_core__WEBPACK_IMPORTED_MODULE_2__["Renderer2"]
        }];
      };

      MyDietModalComponent = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([Object(_angular_core__WEBPACK_IMPORTED_MODULE_2__["Component"])({
        selector: 'app-my-diet-modal',
        template: Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(
        /*! raw-loader!./my-diet-modal.component.html */
        "./node_modules/raw-loader/dist/cjs.js!./src/app/components/modals/my-diet-modal/my-diet-modal.component.html"))["default"],
        styles: [Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(
        /*! ./my-diet-modal.component.scss */
        "./src/app/components/modals/my-diet-modal/my-diet-modal.component.scss"))["default"]]
      })], MyDietModalComponent);
      /***/
    },

    /***/
    "./src/app/components/modals/notebook-modal/notebook-modal.component.scss":
    /*!********************************************************************************!*\
      !*** ./src/app/components/modals/notebook-modal/notebook-modal.component.scss ***!
      \********************************************************************************/

    /*! exports provided: default */

    /***/
    function srcAppComponentsModalsNotebookModalNotebookModalComponentScss(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony default export */


      __webpack_exports__["default"] = "ion-toolbar {\n  border-bottom: 1px solid #c8c8c8;\n  padding-right: 10px;\n}\nion-toolbar ion-text {\n  display: block;\n}\nion-toolbar ion-buttons ion-button ion-img {\n  width: 30px;\n}\n.close-btn-icon {\n  font-size: 29px;\n  font-weight: 700;\n}\n.toggle_btn_section {\n  width: 100%;\n  --padding-start: 0;\n}\n.toggle_btn_section ion-toggle {\n  height: 25px;\n}\n.header_row {\n  padding-left: 3%;\n  padding-right: 3%;\n}\n.note_book_list_row {\n  padding-left: 3%;\n  padding-right: 3%;\n}\n.note_book_list_row ion-col ion-item ion-avatar {\n  display: flex;\n  justify-content: center;\n}\n.note_book_list_row ion-col ion-item ion-avatar ion-img {\n  width: 23px;\n  height: 17px;\n  align-self: center;\n}\n.note_book_list_row ion-col ion-item ion-label {\n  padding-left: 7px;\n}\n.profile-viewall {\n  width: 100%;\n  position: relative;\n  margin-top: 15px;\n  margin-bottom: 15px;\n}\n.profile-viewall ion-button {\n  --border-radius: 25px;\n  display: table;\n  margin: auto;\n  font-size: 12px;\n}\n.profile-viewall:before {\n  content: \"\";\n  position: absolute;\n  background: #c1c1c1;\n  height: 1px;\n  width: 100%;\n  top: 19px;\n  left: 0;\n}\n.tick_col {\n  display: flex;\n  justify-content: center;\n}\n.list_section_row ion-col {\n  padding-top: 0;\n  padding-bottom: 0;\n}\n.tick_icon_background_add {\n  display: flex;\n  justify-content: center;\n  align-self: center;\n}\n.tick_icon_background_add ion-img {\n  width: 30px;\n}\n.tick_icon_background {\n  height: 25px;\n  background: #b2a847;\n  width: 25px;\n  border-radius: 50%;\n  display: flex;\n  justify-content: center;\n  align-self: center;\n}\n.tick_icon_background ion-icon {\n  align-self: center;\n  font-weight: bold;\n  color: #fff;\n}\n.card_data_section ion-card {\n  width: 100%;\n}\n.my_note_row_two_section ion-grid ion-row {\n  display: flex;\n  justify-content: center;\n}\n.my_note_row_two_section ion-grid ion-row p {\n  margin: 0;\n}\n.ribbon-wrapper-1 {\n  width: 51px;\n  height: 65px;\n  overflow: hidden;\n  position: absolute;\n  top: 0px;\n  right: 0px;\n}\n.ribbon-1 {\n  font: bold 15px Sans-Serif;\n  line-height: 18px;\n  color: #333;\n  text-align: center;\n  text-transform: uppercase;\n  -webkit-transform: rotate(47deg);\n  -moz-transform: rotate(45deg);\n  -ms-transform: rotate(45deg);\n  -o-transform: rotate(45deg);\n  position: relative;\n  padding: 7px 0;\n  left: 0px;\n  top: 16px;\n  height: 44px;\n  width: 150px;\n  background: var(--ion-btn-custom-color);\n  color: #fff;\n  box-shadow: 0 4px 6px rgba(0, 0, 0, 0.1);\n  letter-spacing: 0.5px;\n  z-index: 1;\n}\n.validate_btn {\n  width: 100%;\n  --background: var(--ion-btn-custom-color) ;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvY29tcG9uZW50cy9tb2RhbHMvbm90ZWJvb2stbW9kYWwvbm90ZWJvb2stbW9kYWwuY29tcG9uZW50LnNjc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBRUE7RUFDSSxnQ0FBQTtFQUVBLG1CQUFBO0FBRko7QUFJSTtFQUNJLGNBQUE7QUFGUjtBQU9ZO0VBQ0ksV0FBQTtBQUxoQjtBQVlBO0VBQ0ksZUFBQTtFQUNBLGdCQUFBO0FBVEo7QUFZQTtFQUNJLFdBQUE7RUFDQSxrQkFBQTtBQVRKO0FBV0k7RUFDSSxZQUFBO0FBVFI7QUFjQTtFQUVJLGdCQUFBO0VBQ0EsaUJBQUE7QUFaSjtBQWVBO0VBRUksZ0JBQUE7RUFDQSxpQkFBQTtBQWJKO0FBaUJZO0VBQ0ksYUFBQTtFQUNBLHVCQUFBO0FBZmhCO0FBaUJnQjtFQUNJLFdBQUE7RUFDQSxZQUFBO0VBQ0Esa0JBQUE7QUFmcEI7QUFtQlk7RUFDSSxpQkFBQTtBQWpCaEI7QUF5QkE7RUFDSSxXQUFBO0VBQ0Esa0JBQUE7RUFDQSxnQkFBQTtFQUNBLG1CQUFBO0FBdEJKO0FBeUJBO0VBQ0kscUJBQUE7RUFDQSxjQUFBO0VBQ0EsWUFBQTtFQUNBLGVBQUE7QUF0Qko7QUF5QkE7RUFDSSxXQUFBO0VBQ0Esa0JBQUE7RUFDQSxtQkFBQTtFQUNBLFdBQUE7RUFDQSxXQUFBO0VBQ0EsU0FBQTtFQUNBLE9BQUE7QUF0Qko7QUF5QkE7RUFDSSxhQUFBO0VBQ0EsdUJBQUE7QUF0Qko7QUEyQkk7RUFDSSxjQUFBO0VBQ0EsaUJBQUE7QUF4QlI7QUE0QkE7RUFLSSxhQUFBO0VBQ0EsdUJBQUE7RUFDQSxrQkFBQTtBQTdCSjtBQXVCSTtFQUNJLFdBQUE7QUFyQlI7QUE2QkE7RUFDSSxZQUFBO0VBQ0EsbUJBQUE7RUFDQSxXQUFBO0VBQ0Esa0JBQUE7RUFDQSxhQUFBO0VBQ0EsdUJBQUE7RUFDQSxrQkFBQTtBQTFCSjtBQTRCSTtFQUNJLGtCQUFBO0VBQ0EsaUJBQUE7RUFDQSxXQUFBO0FBMUJSO0FBZ0NJO0VBQ0ksV0FBQTtBQTdCUjtBQW9DUTtFQUVJLGFBQUE7RUFDQSx1QkFBQTtBQWxDWjtBQW9DWTtFQUNJLFNBQUE7QUFsQ2hCO0FBeURBO0VBQ0ksV0FBQTtFQUNBLFlBQUE7RUFDQSxnQkFBQTtFQUNBLGtCQUFBO0VBQ0EsUUFBQTtFQUNBLFVBQUE7QUF0REo7QUF5REE7RUFDSSwwQkFBQTtFQUNBLGlCQUFBO0VBQ0EsV0FBQTtFQUNBLGtCQUFBO0VBQ0EseUJBQUE7RUFDQSxnQ0FBQTtFQUNBLDZCQUFBO0VBQ0EsNEJBQUE7RUFDQSwyQkFBQTtFQUNBLGtCQUFBO0VBQ0EsY0FBQTtFQUNBLFNBQUE7RUFDQSxTQUFBO0VBQ0EsWUFBQTtFQUNBLFlBQUE7RUFDQSx1Q0FBQTtFQUNBLFdBQUE7RUFDQSx3Q0FBQTtFQUNBLHFCQUFBO0VBQ0EsVUFBQTtBQXRESjtBQXdEQTtFQUNJLFdBQUE7RUFDQSwwQ0FBQTtBQXJESiIsImZpbGUiOiJzcmMvYXBwL2NvbXBvbmVudHMvbW9kYWxzL25vdGVib29rLW1vZGFsL25vdGVib29rLW1vZGFsLmNvbXBvbmVudC5zY3NzIiwic291cmNlc0NvbnRlbnQiOlsiaW9uLWNvbnRlbnQge31cblxuaW9uLXRvb2xiYXIge1xuICAgIGJvcmRlci1ib3R0b206IDFweCBzb2xpZCAjYzhjOGM4O1xuICAgIC8vIGhlaWdodDogNyU7XG4gICAgcGFkZGluZy1yaWdodDogMTBweDtcblxuICAgIGlvbi10ZXh0IHtcbiAgICAgICAgZGlzcGxheTogYmxvY2s7XG4gICAgfVxuXG4gICAgaW9uLWJ1dHRvbnMge1xuICAgICAgICBpb24tYnV0dG9uIHtcbiAgICAgICAgICAgIGlvbi1pbWcge1xuICAgICAgICAgICAgICAgIHdpZHRoOiAzMHB4O1xuICAgICAgICAgICAgfVxuICAgICAgICB9XG4gICAgfVxufVxuXG5cbi5jbG9zZS1idG4taWNvbiB7XG4gICAgZm9udC1zaXplOiAyOXB4O1xuICAgIGZvbnQtd2VpZ2h0OiA3MDA7XG59XG5cbi50b2dnbGVfYnRuX3NlY3Rpb24ge1xuICAgIHdpZHRoOiAxMDAlO1xuICAgIC0tcGFkZGluZy1zdGFydDogMDtcblxuICAgIGlvbi10b2dnbGUge1xuICAgICAgICBoZWlnaHQ6IDI1cHg7XG4gICAgfVxufVxuXG5cbi5oZWFkZXJfcm93IHtcblxuICAgIHBhZGRpbmctbGVmdDogMyU7XG4gICAgcGFkZGluZy1yaWdodDogMyU7XG59XG5cbi5ub3RlX2Jvb2tfbGlzdF9yb3cge1xuXG4gICAgcGFkZGluZy1sZWZ0OiAzJTtcbiAgICBwYWRkaW5nLXJpZ2h0OiAzJTtcblxuICAgIGlvbi1jb2wge1xuICAgICAgICBpb24taXRlbSB7XG4gICAgICAgICAgICBpb24tYXZhdGFyIHtcbiAgICAgICAgICAgICAgICBkaXNwbGF5OiBmbGV4O1xuICAgICAgICAgICAgICAgIGp1c3RpZnktY29udGVudDogY2VudGVyO1xuXG4gICAgICAgICAgICAgICAgaW9uLWltZyB7XG4gICAgICAgICAgICAgICAgICAgIHdpZHRoOiAyM3B4O1xuICAgICAgICAgICAgICAgICAgICBoZWlnaHQ6IDE3cHg7XG4gICAgICAgICAgICAgICAgICAgIGFsaWduLXNlbGY6IGNlbnRlcjtcbiAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICB9XG5cbiAgICAgICAgICAgIGlvbi1sYWJlbCB7XG4gICAgICAgICAgICAgICAgcGFkZGluZy1sZWZ0OiA3cHg7XG4gICAgICAgICAgICB9XG4gICAgICAgIH1cbiAgICB9XG59XG5cblxuXG4ucHJvZmlsZS12aWV3YWxsIHtcbiAgICB3aWR0aDogMTAwJTtcbiAgICBwb3NpdGlvbjogcmVsYXRpdmU7XG4gICAgbWFyZ2luLXRvcDogMTVweDtcbiAgICBtYXJnaW4tYm90dG9tOiAxNXB4O1xufVxuXG4ucHJvZmlsZS12aWV3YWxsIGlvbi1idXR0b24ge1xuICAgIC0tYm9yZGVyLXJhZGl1czogMjVweDtcbiAgICBkaXNwbGF5OiB0YWJsZTtcbiAgICBtYXJnaW46IGF1dG87XG4gICAgZm9udC1zaXplOiAxMnB4O1xufVxuXG4ucHJvZmlsZS12aWV3YWxsOmJlZm9yZSB7XG4gICAgY29udGVudDogXCJcIjtcbiAgICBwb3NpdGlvbjogYWJzb2x1dGU7XG4gICAgYmFja2dyb3VuZDogI2MxYzFjMTtcbiAgICBoZWlnaHQ6IDFweDtcbiAgICB3aWR0aDogMTAwJTtcbiAgICB0b3A6IDE5cHg7XG4gICAgbGVmdDogMDtcbn1cblxuLnRpY2tfY29sIHtcbiAgICBkaXNwbGF5OiBmbGV4O1xuICAgIGp1c3RpZnktY29udGVudDogY2VudGVyO1xuXG59XG5cbi5saXN0X3NlY3Rpb25fcm93IHtcbiAgICBpb24tY29sIHtcbiAgICAgICAgcGFkZGluZy10b3A6IDA7XG4gICAgICAgIHBhZGRpbmctYm90dG9tOiAwO1xuICAgIH1cbn1cblxuLnRpY2tfaWNvbl9iYWNrZ3JvdW5kX2FkZCB7XG4gICAgaW9uLWltZyB7XG4gICAgICAgIHdpZHRoOiAzMHB4O1xuICAgIH1cblxuICAgIGRpc3BsYXk6IGZsZXg7XG4gICAganVzdGlmeS1jb250ZW50OiBjZW50ZXI7XG4gICAgYWxpZ24tc2VsZjogY2VudGVyO1xufVxuXG4udGlja19pY29uX2JhY2tncm91bmQge1xuICAgIGhlaWdodDogMjVweDtcbiAgICBiYWNrZ3JvdW5kOiAjYjJhODQ3O1xuICAgIHdpZHRoOiAyNXB4O1xuICAgIGJvcmRlci1yYWRpdXM6IDUwJTtcbiAgICBkaXNwbGF5OiBmbGV4O1xuICAgIGp1c3RpZnktY29udGVudDogY2VudGVyO1xuICAgIGFsaWduLXNlbGY6IGNlbnRlcjtcblxuICAgIGlvbi1pY29uIHtcbiAgICAgICAgYWxpZ24tc2VsZjogY2VudGVyO1xuICAgICAgICBmb250LXdlaWdodDogYm9sZDtcbiAgICAgICAgY29sb3I6ICNmZmY7XG4gICAgfVxufVxuXG5cbi5jYXJkX2RhdGFfc2VjdGlvbiB7XG4gICAgaW9uLWNhcmQge1xuICAgICAgICB3aWR0aDogMTAwJTtcbiAgICAgICAgLy8gaGVpZ2h0OiAyMTBweDtcbiAgICB9XG59XG5cbi5teV9ub3RlX3Jvd190d29fc2VjdGlvbiB7XG4gICAgaW9uLWdyaWQge1xuICAgICAgICBpb24tcm93IHtcbiAgICAgICAgICAgIC8vIHBhZGRpbmctcmlnaHQ6IDMlO1xuICAgICAgICAgICAgZGlzcGxheTogZmxleDtcbiAgICAgICAgICAgIGp1c3RpZnktY29udGVudDogY2VudGVyO1xuXG4gICAgICAgICAgICBwIHtcbiAgICAgICAgICAgICAgICBtYXJnaW46IDA7XG4gICAgICAgICAgICB9XG4gICAgICAgIH1cbiAgICB9XG59XG5cbi8vIC5jYXJkLWJveDpiZWZvcmUge1xuLy8gICAgIGNvbnRlbnQ6IFwiXCI7XG4vLyAgICAgYm9yZGVyLXJpZ2h0OiAyMHB4IHNvbGlkICNGNEIxODM7XG4vLyAgICAgYm9yZGVyLXRvcDogMjBweCBzb2xpZCAjRjRCMTgzO1xuLy8gICAgIGJvcmRlci1sZWZ0OiAyMHB4IHNvbGlkICNmZjAwMDAwMDtcbi8vICAgICBib3JkZXItYm90dG9tOiAyMHB4IHNvbGlkICNmZjAwMDAwMDtcbi8vICAgICBwb3NpdGlvbjogYWJzb2x1dGU7XG4vLyAgICAgdG9wOiAwO1xuLy8gICAgIHJpZ2h0OiAwO1xuLy8gICAgIHotaW5kZXg6IDk5OTtcbi8vIH1cblxuLy8gLmNhcmQtYm94IHtcbi8vICAgICBib3gtc2hhZG93OiByZ2IoMCAwIDAgLyAxMiUpIDBweCA0cHggMTZweDtcbi8vICAgICBwYWRkaW5nOiA1cHg7XG4vLyB9XG5cbi5yaWJib24td3JhcHBlci0xIHtcbiAgICB3aWR0aDogNTFweDtcbiAgICBoZWlnaHQ6IDY1cHg7XG4gICAgb3ZlcmZsb3c6IGhpZGRlbjtcbiAgICBwb3NpdGlvbjogYWJzb2x1dGU7XG4gICAgdG9wOiAwcHg7XG4gICAgcmlnaHQ6IDBweDtcbn1cblxuLnJpYmJvbi0xIHtcbiAgICBmb250OiBib2xkIDE1cHggU2Fucy1TZXJpZjtcbiAgICBsaW5lLWhlaWdodDogMThweDtcbiAgICBjb2xvcjogIzMzMztcbiAgICB0ZXh0LWFsaWduOiBjZW50ZXI7XG4gICAgdGV4dC10cmFuc2Zvcm06IHVwcGVyY2FzZTtcbiAgICAtd2Via2l0LXRyYW5zZm9ybTogcm90YXRlKCA0N2RlZyk7XG4gICAgLW1vei10cmFuc2Zvcm06IHJvdGF0ZSg0NWRlZyk7XG4gICAgLW1zLXRyYW5zZm9ybTogcm90YXRlKDQ1ZGVnKTtcbiAgICAtby10cmFuc2Zvcm06IHJvdGF0ZSg0NWRlZyk7XG4gICAgcG9zaXRpb246IHJlbGF0aXZlO1xuICAgIHBhZGRpbmc6IDdweCAwO1xuICAgIGxlZnQ6IDBweDtcbiAgICB0b3A6IDE2cHg7XG4gICAgaGVpZ2h0OiA0NHB4O1xuICAgIHdpZHRoOiAxNTBweDtcbiAgICBiYWNrZ3JvdW5kOiB2YXIoLS1pb24tYnRuLWN1c3RvbS1jb2xvcik7XG4gICAgY29sb3I6ICNmZmY7XG4gICAgYm94LXNoYWRvdzogMCA0cHggNnB4IHJnYigwIDAgMCAvIDEwJSk7XG4gICAgbGV0dGVyLXNwYWNpbmc6IDAuNXB4O1xuICAgIHotaW5kZXg6IDE7XG59XG4udmFsaWRhdGVfYnRuIHtcbiAgICB3aWR0aDogMTAwJTtcbiAgICAtLWJhY2tncm91bmQ6IHZhcigtLWlvbi1idG4tY3VzdG9tLWNvbG9yKVxufSJdfQ== */";
      /***/
    },

    /***/
    "./src/app/components/modals/notebook-modal/notebook-modal.component.ts":
    /*!******************************************************************************!*\
      !*** ./src/app/components/modals/notebook-modal/notebook-modal.component.ts ***!
      \******************************************************************************/

    /*! exports provided: NotebookModalComponent */

    /***/
    function srcAppComponentsModalsNotebookModalNotebookModalComponentTs(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "NotebookModalComponent", function () {
        return NotebookModalComponent;
      });
      /* harmony import */


      var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! tslib */
      "./node_modules/tslib/tslib.es6.js");
      /* harmony import */


      var _ionic_angular__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! @ionic/angular */
      "./node_modules/@ionic/angular/__ivy_ngcc__/fesm2015/ionic-angular.js");
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! @angular/core */
      "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
      /* harmony import */


      var hammerjs__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
      /*! hammerjs */
      "./node_modules/hammerjs/hammer.js");
      /* harmony import */


      var hammerjs__WEBPACK_IMPORTED_MODULE_3___default = /*#__PURE__*/__webpack_require__.n(hammerjs__WEBPACK_IMPORTED_MODULE_3__);

      var NotebookModalComponent = /*#__PURE__*/function () {
        function NotebookModalComponent(modalCtrl, gestureCtrl, element, renderer) {
          _classCallCheck(this, NotebookModalComponent);

          this.modalCtrl = modalCtrl;
          this.gestureCtrl = gestureCtrl;
          this.element = element;
          this.renderer = renderer;
          this.cards = [];
          this.showRow = "first";
        }

        _createClass(NotebookModalComponent, [{
          key: "ngOnInit",
          value: function ngOnInit() {
            this.loadTinderCards();
          }
        }, {
          key: "closeModal",
          value: function closeModal() {
            // this.modalCtrl.dismiss()
            if (this.showRow == 'third') {
              this.modalCtrl.dismiss();
            } else {
              this.showRow = "second";
            }
          }
        }, {
          key: "loadTinderCards",
          value: function loadTinderCards() {
            this.cards = [{
              title: "Use breathwork?",
              description: "If yes, for how long (minutes)?"
            }, {
              title: "Use breathwork?",
              description: "If yes, for how long (minutes)?"
            }, {
              title: "Use breathwork?",
              description: "If yes, for how long (minutes)?"
            }, {
              title: "Use breathwork?",
              description: "If yes, for how long (minutes)?"
            }, {
              title: "Use breathwork?",
              description: "If yes, for how long (minutes)?"
            }];
          }
        }, {
          key: "ngAfterViewInit",
          value: function ngAfterViewInit() {
            return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, /*#__PURE__*/regeneratorRuntime.mark(function _callee22() {
              var _this24 = this;

              var d, options, gesture;
              return regeneratorRuntime.wrap(function _callee22$(_context22) {
                while (1) {
                  switch (_context22.prev = _context22.next) {
                    case 0:
                      d = document.querySelector('ion-modal');
                      options = {
                        el: d,
                        direction: "y",
                        gestureName: 'swipe',
                        onStart: function onStart() {
                          _this24.renderer.setStyle(d, 'transition', 'none');
                        },
                        onMove: function onMove(ev) {
                          if (ev.deltaY > 0) {
                            _this24.renderer.setStyle(d, 'transform', "translateY(".concat(ev.deltaY, "px)"));
                          }
                        },
                        onEnd: function onEnd(ev) {
                          _this24.renderer.setStyle(d, 'transition', '0.5s ease-out');

                          if (ev.deltaY > -200) {
                            _this24.renderer.setStyle(d, 'transform', 'translateY(500px)');

                            _this24.modalCtrl.dismiss();
                          } else {
                            _this24.renderer.setStyle(d, 'transform', 'translateY(0px)');
                          }
                        }
                      };
                      _context22.next = 4;
                      return this.gestureCtrl.create(options);

                    case 4:
                      gesture = _context22.sent;
                      gesture.enable();

                    case 6:
                    case "end":
                      return _context22.stop();
                  }
                }
              }, _callee22, this);
            }));
          }
        }]);

        return NotebookModalComponent;
      }();

      NotebookModalComponent.ctorParameters = function () {
        return [{
          type: _ionic_angular__WEBPACK_IMPORTED_MODULE_1__["ModalController"]
        }, {
          type: _ionic_angular__WEBPACK_IMPORTED_MODULE_1__["GestureController"]
        }, {
          type: _angular_core__WEBPACK_IMPORTED_MODULE_2__["ElementRef"]
        }, {
          type: _angular_core__WEBPACK_IMPORTED_MODULE_2__["Renderer2"]
        }];
      };

      NotebookModalComponent = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([Object(_angular_core__WEBPACK_IMPORTED_MODULE_2__["Component"])({
        selector: 'app-notebook-modal',
        template: Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(
        /*! raw-loader!./notebook-modal.component.html */
        "./node_modules/raw-loader/dist/cjs.js!./src/app/components/modals/notebook-modal/notebook-modal.component.html"))["default"],
        styles: [Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(
        /*! ./notebook-modal.component.scss */
        "./src/app/components/modals/notebook-modal/notebook-modal.component.scss"))["default"]]
      })], NotebookModalComponent);
      /***/
    },

    /***/
    "./src/app/components/modals/payment-modal/payment-modal.component.scss":
    /*!******************************************************************************!*\
      !*** ./src/app/components/modals/payment-modal/payment-modal.component.scss ***!
      \******************************************************************************/

    /*! exports provided: default */

    /***/
    function srcAppComponentsModalsPaymentModalPaymentModalComponentScss(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony default export */


      __webpack_exports__["default"] = "ion-item {\n  --inner-border-width: 0 0 0px 0;\n}\n\n.timeline-box {\n  position: relative;\n  display: flex;\n  padding: 0px 0;\n  margin-left: 0px;\n  padding-bottom: 15px;\n}\n\n.timeline-box:before {\n  position: absolute;\n  top: 0;\n  bottom: 0;\n  left: 4px;\n  display: block;\n  width: 2px;\n  content: \"\";\n  background-color: #3f9c64;\n}\n\n.timeline-box:last-child:before {\n  display: none;\n}\n\n.timeline-badge {\n  position: relative;\n  z-index: 1;\n  display: flex;\n  width: 10px;\n  height: 10px;\n  margin-right: 8px;\n  margin-left: 0px;\n  color: #000;\n  align-items: center;\n  background-color: #3f9c64;\n  border-radius: 50%;\n  justify-content: center;\n  flex-shrink: 0;\n}\n\n.timeline-body {\n  min-width: 0;\n  max-width: 100%;\n  margin-top: -5px;\n  color: #000;\n  flex: auto;\n  display: flex;\n}\n\n.timeline-body div {\n  flex-grow: 1;\n  font-size: 12px;\n}\n\n.timeline-box:last-child .timeline-badge {\n  background-color: transparent;\n  border: 1px solid #3f9c64;\n}\n\n.payment-btn {\n  --background: #3f9c64;\n  --border-radius: 5px;\n}\n\nion-toolbar {\n  border-bottom: 1px solid #ccc;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvY29tcG9uZW50cy9tb2RhbHMvcGF5bWVudC1tb2RhbC9wYXltZW50LW1vZGFsLmNvbXBvbmVudC5zY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBO0VBQ0ksK0JBQUE7QUFDSjs7QUFFQTtFQUNJLGtCQUFBO0VBQ0EsYUFBQTtFQUNBLGNBQUE7RUFDQSxnQkFBQTtFQUNBLG9CQUFBO0FBQ0o7O0FBRUE7RUFDSSxrQkFBQTtFQUNBLE1BQUE7RUFDQSxTQUFBO0VBQ0EsU0FBQTtFQUNBLGNBQUE7RUFDQSxVQUFBO0VBQ0EsV0FBQTtFQUNBLHlCQUFBO0FBQ0o7O0FBRUE7RUFDSSxhQUFBO0FBQ0o7O0FBRUE7RUFDSSxrQkFBQTtFQUNBLFVBQUE7RUFDQSxhQUFBO0VBQ0EsV0FBQTtFQUNBLFlBQUE7RUFDQSxpQkFBQTtFQUNBLGdCQUFBO0VBQ0EsV0FBQTtFQUNBLG1CQUFBO0VBQ0EseUJBQUE7RUFDQSxrQkFBQTtFQUNBLHVCQUFBO0VBQ0EsY0FBQTtBQUNKOztBQUVBO0VBQ0ksWUFBQTtFQUNBLGVBQUE7RUFDQSxnQkFBQTtFQUNBLFdBQUE7RUFDQSxVQUFBO0VBQ0EsYUFBQTtBQUNKOztBQUVBO0VBQ0ksWUFBQTtFQUNBLGVBQUE7QUFDSjs7QUFFQTtFQUNJLDZCQUFBO0VBQ0EseUJBQUE7QUFDSjs7QUFFQTtFQUNJLHFCQUFBO0VBQ0Esb0JBQUE7QUFDSjs7QUFFQTtFQUNJLDZCQUFBO0FBQ0oiLCJmaWxlIjoic3JjL2FwcC9jb21wb25lbnRzL21vZGFscy9wYXltZW50LW1vZGFsL3BheW1lbnQtbW9kYWwuY29tcG9uZW50LnNjc3MiLCJzb3VyY2VzQ29udGVudCI6WyJpb24taXRlbSB7XG4gICAgLS1pbm5lci1ib3JkZXItd2lkdGg6IDAgMCAwcHggMDtcbn1cblxuLnRpbWVsaW5lLWJveCB7XG4gICAgcG9zaXRpb246IHJlbGF0aXZlO1xuICAgIGRpc3BsYXk6IGZsZXg7XG4gICAgcGFkZGluZzogMHB4IDA7XG4gICAgbWFyZ2luLWxlZnQ6IDBweDtcbiAgICBwYWRkaW5nLWJvdHRvbTogMTVweDtcbn1cblxuLnRpbWVsaW5lLWJveDpiZWZvcmUge1xuICAgIHBvc2l0aW9uOiBhYnNvbHV0ZTtcbiAgICB0b3A6IDA7XG4gICAgYm90dG9tOiAwO1xuICAgIGxlZnQ6IDRweDtcbiAgICBkaXNwbGF5OiBibG9jaztcbiAgICB3aWR0aDogMnB4O1xuICAgIGNvbnRlbnQ6IFwiXCI7XG4gICAgYmFja2dyb3VuZC1jb2xvcjogIzNmOWM2NDtcbn1cblxuLnRpbWVsaW5lLWJveDpsYXN0LWNoaWxkOmJlZm9yZSB7XG4gICAgZGlzcGxheTogbm9uZTtcbn1cblxuLnRpbWVsaW5lLWJhZGdlIHtcbiAgICBwb3NpdGlvbjogcmVsYXRpdmU7XG4gICAgei1pbmRleDogMTtcbiAgICBkaXNwbGF5OiBmbGV4O1xuICAgIHdpZHRoOiAxMHB4O1xuICAgIGhlaWdodDogMTBweDtcbiAgICBtYXJnaW4tcmlnaHQ6IDhweDtcbiAgICBtYXJnaW4tbGVmdDogMHB4O1xuICAgIGNvbG9yOiAjMDAwO1xuICAgIGFsaWduLWl0ZW1zOiBjZW50ZXI7XG4gICAgYmFja2dyb3VuZC1jb2xvcjogIzNmOWM2NDtcbiAgICBib3JkZXItcmFkaXVzOiA1MCU7XG4gICAganVzdGlmeS1jb250ZW50OiBjZW50ZXI7XG4gICAgZmxleC1zaHJpbms6IDA7XG59XG5cbi50aW1lbGluZS1ib2R5IHtcbiAgICBtaW4td2lkdGg6IDA7XG4gICAgbWF4LXdpZHRoOiAxMDAlO1xuICAgIG1hcmdpbi10b3A6IC01cHg7XG4gICAgY29sb3I6ICMwMDA7XG4gICAgZmxleDogYXV0bztcbiAgICBkaXNwbGF5OiBmbGV4O1xufVxuXG4udGltZWxpbmUtYm9keSBkaXYge1xuICAgIGZsZXgtZ3JvdzogMTtcbiAgICBmb250LXNpemU6IDEycHg7XG59XG5cbi50aW1lbGluZS1ib3g6bGFzdC1jaGlsZCAudGltZWxpbmUtYmFkZ2Uge1xuICAgIGJhY2tncm91bmQtY29sb3I6IHRyYW5zcGFyZW50O1xuICAgIGJvcmRlcjogMXB4IHNvbGlkICMzZjljNjQ7XG59XG5cbi5wYXltZW50LWJ0biB7XG4gICAgLS1iYWNrZ3JvdW5kOiAjM2Y5YzY0O1xuICAgIC0tYm9yZGVyLXJhZGl1czogNXB4O1xufVxuXG5pb24tdG9vbGJhciB7XG4gICAgYm9yZGVyLWJvdHRvbTogMXB4IHNvbGlkICNjY2M7XG59Il19 */";
      /***/
    },

    /***/
    "./src/app/components/modals/payment-modal/payment-modal.component.ts":
    /*!****************************************************************************!*\
      !*** ./src/app/components/modals/payment-modal/payment-modal.component.ts ***!
      \****************************************************************************/

    /*! exports provided: PaymentModalComponent */

    /***/
    function srcAppComponentsModalsPaymentModalPaymentModalComponentTs(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "PaymentModalComponent", function () {
        return PaymentModalComponent;
      });
      /* harmony import */


      var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! tslib */
      "./node_modules/tslib/tslib.es6.js");
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! @angular/core */
      "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
      /* harmony import */


      var _ionic_angular__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! @ionic/angular */
      "./node_modules/@ionic/angular/__ivy_ngcc__/fesm2015/ionic-angular.js");
      /* harmony import */


      var hammerjs__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
      /*! hammerjs */
      "./node_modules/hammerjs/hammer.js");
      /* harmony import */


      var hammerjs__WEBPACK_IMPORTED_MODULE_3___default = /*#__PURE__*/__webpack_require__.n(hammerjs__WEBPACK_IMPORTED_MODULE_3__);

      var PaymentModalComponent = /*#__PURE__*/function () {
        function PaymentModalComponent(gestureCtrl, element, renderer, modalCtrl) {
          _classCallCheck(this, PaymentModalComponent);

          this.gestureCtrl = gestureCtrl;
          this.element = element;
          this.renderer = renderer;
          this.modalCtrl = modalCtrl;
        }

        _createClass(PaymentModalComponent, [{
          key: "ngOnInit",
          value: function ngOnInit() {}
        }, {
          key: "ngAfterViewInit",
          value: function ngAfterViewInit() {
            return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, /*#__PURE__*/regeneratorRuntime.mark(function _callee23() {
              var _this25 = this;

              var d, options, gesture;
              return regeneratorRuntime.wrap(function _callee23$(_context23) {
                while (1) {
                  switch (_context23.prev = _context23.next) {
                    case 0:
                      d = document.querySelector('ion-modal');
                      options = {
                        el: d,
                        direction: "y",
                        gestureName: 'swipe',
                        onStart: function onStart() {
                          _this25.renderer.setStyle(d, 'transition', 'none');
                        },
                        onMove: function onMove(ev) {
                          if (ev.deltaY > 0) {
                            _this25.renderer.setStyle(d, 'transform', "translateY(".concat(ev.deltaY, "px)"));
                          }
                        },
                        onEnd: function onEnd(ev) {
                          _this25.renderer.setStyle(d, 'transition', '0.5s ease-out');

                          if (ev.deltaY > -200) {
                            _this25.renderer.setStyle(d, 'transform', 'translateY(500px)');

                            _this25.modalCtrl.dismiss();
                          } else {
                            _this25.renderer.setStyle(d, 'transform', 'translateY(0px)');
                          }
                        }
                      };
                      _context23.next = 4;
                      return this.gestureCtrl.create(options);

                    case 4:
                      gesture = _context23.sent;
                      gesture.enable();

                    case 6:
                    case "end":
                      return _context23.stop();
                  }
                }
              }, _callee23, this);
            }));
          }
        }]);

        return PaymentModalComponent;
      }();

      PaymentModalComponent.ctorParameters = function () {
        return [{
          type: _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["GestureController"]
        }, {
          type: _angular_core__WEBPACK_IMPORTED_MODULE_1__["ElementRef"]
        }, {
          type: _angular_core__WEBPACK_IMPORTED_MODULE_1__["Renderer2"]
        }, {
          type: _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["ModalController"]
        }];
      };

      PaymentModalComponent = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-payment-modal',
        template: Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(
        /*! raw-loader!./payment-modal.component.html */
        "./node_modules/raw-loader/dist/cjs.js!./src/app/components/modals/payment-modal/payment-modal.component.html"))["default"],
        styles: [Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(
        /*! ./payment-modal.component.scss */
        "./src/app/components/modals/payment-modal/payment-modal.component.scss"))["default"]]
      })], PaymentModalComponent);
      /***/
    },

    /***/
    "./src/app/components/modals/recipes-filters/recipes-filters.component.scss":
    /*!**********************************************************************************!*\
      !*** ./src/app/components/modals/recipes-filters/recipes-filters.component.scss ***!
      \**********************************************************************************/

    /*! exports provided: default */

    /***/
    function srcAppComponentsModalsRecipesFiltersRecipesFiltersComponentScss(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony default export */


      __webpack_exports__["default"] = "ion-item {\n  --inner-border-width: 0 0 0px 0;\n}\n\n.recipes div {\n  width: auto;\n  display: inline-block;\n  padding: 5px 15px;\n  margin: 0px 5px 5px 0px;\n  font-size: 12px;\n  background: #eaeaea;\n  border-radius: 5px;\n}\n\n.recipes {\n  position: relative;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvY29tcG9uZW50cy9tb2RhbHMvcmVjaXBlcy1maWx0ZXJzL3JlY2lwZXMtZmlsdGVycy5jb21wb25lbnQuc2NzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQTtFQUNJLCtCQUFBO0FBQ0o7O0FBRUE7RUFDSSxXQUFBO0VBQ0EscUJBQUE7RUFDQSxpQkFBQTtFQUNBLHVCQUFBO0VBQ0EsZUFBQTtFQUNBLG1CQUFBO0VBQ0Esa0JBQUE7QUFDSjs7QUFFQTtFQUNJLGtCQUFBO0FBQ0oiLCJmaWxlIjoic3JjL2FwcC9jb21wb25lbnRzL21vZGFscy9yZWNpcGVzLWZpbHRlcnMvcmVjaXBlcy1maWx0ZXJzLmNvbXBvbmVudC5zY3NzIiwic291cmNlc0NvbnRlbnQiOlsiaW9uLWl0ZW0ge1xuICAgIC0taW5uZXItYm9yZGVyLXdpZHRoOiAwIDAgMHB4IDA7XG59XG5cbi5yZWNpcGVzIGRpdiB7XG4gICAgd2lkdGg6IGF1dG87XG4gICAgZGlzcGxheTogaW5saW5lLWJsb2NrO1xuICAgIHBhZGRpbmc6IDVweCAxNXB4O1xuICAgIG1hcmdpbjogMHB4IDVweCA1cHggMHB4O1xuICAgIGZvbnQtc2l6ZTogMTJweDtcbiAgICBiYWNrZ3JvdW5kOiAjZWFlYWVhO1xuICAgIGJvcmRlci1yYWRpdXM6IDVweDtcbn1cblxuLnJlY2lwZXMge1xuICAgIHBvc2l0aW9uOiByZWxhdGl2ZTtcbn0iXX0= */";
      /***/
    },

    /***/
    "./src/app/components/modals/recipes-filters/recipes-filters.component.ts":
    /*!********************************************************************************!*\
      !*** ./src/app/components/modals/recipes-filters/recipes-filters.component.ts ***!
      \********************************************************************************/

    /*! exports provided: RecipesFiltersComponent */

    /***/
    function srcAppComponentsModalsRecipesFiltersRecipesFiltersComponentTs(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "RecipesFiltersComponent", function () {
        return RecipesFiltersComponent;
      });
      /* harmony import */


      var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! tslib */
      "./node_modules/tslib/tslib.es6.js");
      /* harmony import */


      var _angular_router__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! @angular/router */
      "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");
      /* harmony import */


      var _ionic_angular__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! @ionic/angular */
      "./node_modules/@ionic/angular/__ivy_ngcc__/fesm2015/ionic-angular.js");
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
      /*! @angular/core */
      "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");

      var RecipesFiltersComponent = /*#__PURE__*/function () {
        function RecipesFiltersComponent(modal, rouer) {
          _classCallCheck(this, RecipesFiltersComponent);

          this.modal = modal;
          this.rouer = rouer;
        }

        _createClass(RecipesFiltersComponent, [{
          key: "ngOnInit",
          value: function ngOnInit() {}
        }, {
          key: "closeModal",
          value: function closeModal() {
            this.modal.dismiss();
          }
        }]);

        return RecipesFiltersComponent;
      }();

      RecipesFiltersComponent.ctorParameters = function () {
        return [{
          type: _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["ModalController"]
        }, {
          type: _angular_router__WEBPACK_IMPORTED_MODULE_1__["Router"]
        }];
      };

      RecipesFiltersComponent = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([Object(_angular_core__WEBPACK_IMPORTED_MODULE_3__["Component"])({
        selector: 'app-recipes-filters',
        template: Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(
        /*! raw-loader!./recipes-filters.component.html */
        "./node_modules/raw-loader/dist/cjs.js!./src/app/components/modals/recipes-filters/recipes-filters.component.html"))["default"],
        styles: [Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(
        /*! ./recipes-filters.component.scss */
        "./src/app/components/modals/recipes-filters/recipes-filters.component.scss"))["default"]]
      })], RecipesFiltersComponent);
      /***/
    },

    /***/
    "./src/app/components/modals/sponsership-modal/sponsership-modal.component.scss":
    /*!**************************************************************************************!*\
      !*** ./src/app/components/modals/sponsership-modal/sponsership-modal.component.scss ***!
      \**************************************************************************************/

    /*! exports provided: default */

    /***/
    function srcAppComponentsModalsSponsershipModalSponsershipModalComponentScss(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony default export */


      __webpack_exports__["default"] = "ion-toolbar {\n  border-bottom: 1px solid #c8c8c8;\n  padding-right: 10px;\n}\nion-toolbar ion-text {\n  display: block;\n}\nion-row {\n  width: 100%;\n}\n.close-btn-icon {\n  font-size: 29px;\n  font-weight: 700;\n}\n.second-second-btn {\n  margin-left: 25%;\n}\n.principle_section {\n  display: flex;\n  flex-direction: column;\n  width: 100%;\n  text-align: center;\n}\n.principle_section ion-img {\n  width: 45%;\n  align-self: center;\n}\n.numbered_data_section .text_desc {\n  text-align: left;\n}\n.tick_icon_background {\n  height: 25px;\n  background: #b2a847;\n  width: 25px;\n  border-radius: 50%;\n  display: flex;\n  justify-content: center;\n  align-self: center;\n}\n.tick_icon_background ion-icon {\n  align-self: center;\n  font-weight: bold;\n  color: #fff;\n}\n.tick_icon_background_none {\n  height: 25px;\n  width: 25px;\n  border-radius: 50%;\n  display: flex;\n  justify-content: center;\n  align-self: center;\n}\n.tick_icon_background_none ion-icon {\n  align-self: center;\n  font-weight: bold;\n  color: #fff;\n}\n.tick_col {\n  display: flex;\n}\n.item_white_section ion-card {\n  width: 100%;\n  margin: 0;\n}\n.item_white_section ion-card ion-card-content {\n  display: flex;\n  justify-content: space-around;\n  padding-top: 10px;\n  padding-bottom: 10px;\n  padding-left: 5px;\n  padding-right: 5px;\n}\n.history_section {\n  width: 100%;\n}\n.main_tick_div {\n  display: flex;\n  flex-direction: row;\n}\n.main_tick_div ion-label {\n  margin-right: 5px;\n  font-size: 10px;\n  align-self: center;\n}\n.profile-viewall {\n  width: 100%;\n  position: relative;\n  margin-top: 15px;\n  margin-bottom: 15px;\n}\n.profile-viewall ion-button {\n  --border-radius: 25px;\n  display: table;\n  margin: auto;\n  font-size: 12px;\n}\n.profile-viewall:before {\n  content: \"\";\n  position: absolute;\n  background: #c1c1c1;\n  height: 1px;\n  width: 100%;\n  top: 19px;\n  left: 0;\n}\n.card_data_section ion-card {\n  width: 100%;\n  margin: 0;\n  padding: 5px;\n}\n.card_data_section ion-card ion-card-header {\n  padding: 5px;\n  display: flex;\n  flex-direction: column;\n}\n.card_data_section ion-card ion-card-content {\n  padding: 5px;\n  display: flex;\n  flex-direction: column;\n}\n.card-box {\n  box-shadow: rgba(0, 0, 0, 0.12) 0px 4px 16px;\n  padding: 5px;\n}\n.ribbon-wrapper-1 {\n  width: 51px;\n  height: 65px;\n  overflow: hidden;\n  position: absolute;\n  top: 0px;\n  right: 0px;\n}\n.ribbon-1 {\n  font: bold 15px Sans-Serif;\n  line-height: 18px;\n  color: #333;\n  text-align: center;\n  text-transform: uppercase;\n  -webkit-transform: rotate(47deg);\n  -moz-transform: rotate(45deg);\n  -ms-transform: rotate(45deg);\n  -o-transform: rotate(45deg);\n  position: relative;\n  padding: 7px 0;\n  left: 0px;\n  top: 16px;\n  height: 44px;\n  width: 150px;\n  background: var(--ion-btn-custom-color);\n  color: #fff;\n  box-shadow: 0 4px 6px rgba(0, 0, 0, 0.1);\n  letter-spacing: 0.5px;\n  z-index: 1;\n}\n.desc_text {\n  display: flex;\n}\n.close_icon {\n  align-self: center !important;\n  font-weight: bold !important;\n  color: #000 !important;\n  font-size: 23px !important;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvY29tcG9uZW50cy9tb2RhbHMvc3BvbnNlcnNoaXAtbW9kYWwvc3BvbnNlcnNoaXAtbW9kYWwuY29tcG9uZW50LnNjc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBRUE7RUFDSSxnQ0FBQTtFQUVBLG1CQUFBO0FBRko7QUFJSTtFQUNJLGNBQUE7QUFGUjtBQWNBO0VBQ0ksV0FBQTtBQVhKO0FBY0E7RUFDSSxlQUFBO0VBQ0EsZ0JBQUE7QUFYSjtBQWVBO0VBQ0ksZ0JBQUE7QUFaSjtBQWVBO0VBQ0ksYUFBQTtFQUNBLHNCQUFBO0VBQ0EsV0FBQTtFQUNBLGtCQUFBO0FBWko7QUFjSTtFQUNJLFVBQUE7RUFDQSxrQkFBQTtBQVpSO0FBa0JJO0VBQ0ksZ0JBQUE7QUFmUjtBQW1CQTtFQUNJLFlBQUE7RUFDQSxtQkFBQTtFQUNBLFdBQUE7RUFDQSxrQkFBQTtFQUNBLGFBQUE7RUFDQSx1QkFBQTtFQUNBLGtCQUFBO0FBaEJKO0FBa0JJO0VBQ0ksa0JBQUE7RUFDQSxpQkFBQTtFQUNBLFdBQUE7QUFoQlI7QUFvQkE7RUFDSSxZQUFBO0VBQ0EsV0FBQTtFQUNBLGtCQUFBO0VBQ0EsYUFBQTtFQUNBLHVCQUFBO0VBQ0Esa0JBQUE7QUFqQko7QUFtQkk7RUFDSSxrQkFBQTtFQUNBLGlCQUFBO0VBQ0EsV0FBQTtBQWpCUjtBQXFCQTtFQUNJLGFBQUE7QUFsQko7QUFzQkk7RUFDSSxXQUFBO0VBQ0EsU0FBQTtBQW5CUjtBQXFCUTtFQUVJLGFBQUE7RUFDQSw2QkFBQTtFQUVBLGlCQUFBO0VBQ0Esb0JBQUE7RUFDQSxpQkFBQTtFQUNBLGtCQUFBO0FBckJaO0FBMkJBO0VBQ0ksV0FBQTtBQXhCSjtBQTJCQTtFQUNJLGFBQUE7RUFDQSxtQkFBQTtBQXhCSjtBQTBCSTtFQUNJLGlCQUFBO0VBQ0EsZUFBQTtFQUNBLGtCQUFBO0FBeEJSO0FBOEJBO0VBQ0ksV0FBQTtFQUNBLGtCQUFBO0VBQ0EsZ0JBQUE7RUFDQSxtQkFBQTtBQTNCSjtBQThCQTtFQUNJLHFCQUFBO0VBQ0EsY0FBQTtFQUNBLFlBQUE7RUFDQSxlQUFBO0FBM0JKO0FBOEJBO0VBQ0ksV0FBQTtFQUNBLGtCQUFBO0VBQ0EsbUJBQUE7RUFDQSxXQUFBO0VBQ0EsV0FBQTtFQUNBLFNBQUE7RUFDQSxPQUFBO0FBM0JKO0FBZ0NJO0VBQ0ksV0FBQTtFQUVBLFNBQUE7RUFDQSxZQUFBO0FBOUJSO0FBZ0NRO0VBQ0ksWUFBQTtFQUNBLGFBQUE7RUFDQSxzQkFBQTtBQTlCWjtBQWlDUTtFQUNJLFlBQUE7RUFDQSxhQUFBO0VBQ0Esc0JBQUE7QUEvQlo7QUFpREE7RUFDSSw0Q0FBQTtFQUNBLFlBQUE7QUE5Q0o7QUFpREE7RUFDSSxXQUFBO0VBQ0EsWUFBQTtFQUNBLGdCQUFBO0VBQ0Esa0JBQUE7RUFDQSxRQUFBO0VBQ0EsVUFBQTtBQTlDSjtBQWlEQTtFQUNJLDBCQUFBO0VBQ0EsaUJBQUE7RUFDQSxXQUFBO0VBQ0Esa0JBQUE7RUFDQSx5QkFBQTtFQUNBLGdDQUFBO0VBQ0EsNkJBQUE7RUFDQSw0QkFBQTtFQUNBLDJCQUFBO0VBQ0Esa0JBQUE7RUFDQSxjQUFBO0VBQ0EsU0FBQTtFQUNBLFNBQUE7RUFDQSxZQUFBO0VBQ0EsWUFBQTtFQUNBLHVDQUFBO0VBQ0EsV0FBQTtFQUNBLHdDQUFBO0VBQ0EscUJBQUE7RUFDQSxVQUFBO0FBOUNKO0FBa0RBO0VBQ0ksYUFBQTtBQS9DSjtBQWtEQTtFQUNJLDZCQUFBO0VBQ0EsNEJBQUE7RUFDQSxzQkFBQTtFQUNBLDBCQUFBO0FBL0NKIiwiZmlsZSI6InNyYy9hcHAvY29tcG9uZW50cy9tb2RhbHMvc3BvbnNlcnNoaXAtbW9kYWwvc3BvbnNlcnNoaXAtbW9kYWwuY29tcG9uZW50LnNjc3MiLCJzb3VyY2VzQ29udGVudCI6WyJpb24tY29udGVudCB7fVxuXG5pb24tdG9vbGJhciB7XG4gICAgYm9yZGVyLWJvdHRvbTogMXB4IHNvbGlkICNjOGM4Yzg7XG4gICAgLy8gaGVpZ2h0OiA3JTtcbiAgICBwYWRkaW5nLXJpZ2h0OiAxMHB4O1xuXG4gICAgaW9uLXRleHQge1xuICAgICAgICBkaXNwbGF5OiBibG9jaztcbiAgICB9XG5cbiAgICAvLyBpb24tYnV0dG9ucyB7XG4gICAgLy8gICAgIGlvbi1idXR0b24ge1xuICAgIC8vICAgICAgICAgaW9uLWltZyB7XG4gICAgLy8gICAgICAgICAgICAgd2lkdGg6IDMwcHg7XG4gICAgLy8gICAgICAgICB9XG4gICAgLy8gICAgIH1cbiAgICAvLyB9XG59XG5cbmlvbi1yb3cge1xuICAgIHdpZHRoOiAxMDAlO1xufVxuXG4uY2xvc2UtYnRuLWljb24ge1xuICAgIGZvbnQtc2l6ZTogMjlweDtcbiAgICBmb250LXdlaWdodDogNzAwO1xufVxuXG5cbi5zZWNvbmQtc2Vjb25kLWJ0biB7XG4gICAgbWFyZ2luLWxlZnQ6IDI1JTtcbn1cblxuLnByaW5jaXBsZV9zZWN0aW9uIHtcbiAgICBkaXNwbGF5OiBmbGV4O1xuICAgIGZsZXgtZGlyZWN0aW9uOiBjb2x1bW47XG4gICAgd2lkdGg6IDEwMCU7XG4gICAgdGV4dC1hbGlnbjogY2VudGVyO1xuXG4gICAgaW9uLWltZyB7XG4gICAgICAgIHdpZHRoOiA0NSU7XG4gICAgICAgIGFsaWduLXNlbGY6IGNlbnRlcjtcbiAgICB9XG5cbn1cblxuLm51bWJlcmVkX2RhdGFfc2VjdGlvbiB7XG4gICAgLnRleHRfZGVzYyB7XG4gICAgICAgIHRleHQtYWxpZ246IGxlZnQ7XG4gICAgfVxufVxuXG4udGlja19pY29uX2JhY2tncm91bmQge1xuICAgIGhlaWdodDogMjVweDtcbiAgICBiYWNrZ3JvdW5kOiAjYjJhODQ3O1xuICAgIHdpZHRoOiAyNXB4O1xuICAgIGJvcmRlci1yYWRpdXM6IDUwJTtcbiAgICBkaXNwbGF5OiBmbGV4O1xuICAgIGp1c3RpZnktY29udGVudDogY2VudGVyO1xuICAgIGFsaWduLXNlbGY6IGNlbnRlcjtcblxuICAgIGlvbi1pY29uIHtcbiAgICAgICAgYWxpZ24tc2VsZjogY2VudGVyO1xuICAgICAgICBmb250LXdlaWdodDogYm9sZDtcbiAgICAgICAgY29sb3I6ICNmZmY7XG4gICAgfVxufVxuXG4udGlja19pY29uX2JhY2tncm91bmRfbm9uZSB7XG4gICAgaGVpZ2h0OiAyNXB4O1xuICAgIHdpZHRoOiAyNXB4O1xuICAgIGJvcmRlci1yYWRpdXM6IDUwJTtcbiAgICBkaXNwbGF5OiBmbGV4O1xuICAgIGp1c3RpZnktY29udGVudDogY2VudGVyO1xuICAgIGFsaWduLXNlbGY6IGNlbnRlcjtcblxuICAgIGlvbi1pY29uIHtcbiAgICAgICAgYWxpZ24tc2VsZjogY2VudGVyO1xuICAgICAgICBmb250LXdlaWdodDogYm9sZDtcbiAgICAgICAgY29sb3I6ICNmZmY7XG4gICAgfVxufVxuXG4udGlja19jb2wge1xuICAgIGRpc3BsYXk6IGZsZXg7XG59XG5cbi5pdGVtX3doaXRlX3NlY3Rpb24ge1xuICAgIGlvbi1jYXJkIHtcbiAgICAgICAgd2lkdGg6IDEwMCU7XG4gICAgICAgIG1hcmdpbjogMDtcblxuICAgICAgICBpb24tY2FyZC1jb250ZW50IHtcblxuICAgICAgICAgICAgZGlzcGxheTogZmxleDtcbiAgICAgICAgICAgIGp1c3RpZnktY29udGVudDogc3BhY2UtYXJvdW5kO1xuXG4gICAgICAgICAgICBwYWRkaW5nLXRvcDogMTBweDtcbiAgICAgICAgICAgIHBhZGRpbmctYm90dG9tOiAxMHB4O1xuICAgICAgICAgICAgcGFkZGluZy1sZWZ0OiA1cHg7XG4gICAgICAgICAgICBwYWRkaW5nLXJpZ2h0OiA1cHg7XG4gICAgICAgIH1cbiAgICB9XG59XG5cblxuLmhpc3Rvcnlfc2VjdGlvbiB7XG4gICAgd2lkdGg6IDEwMCU7XG59XG5cbi5tYWluX3RpY2tfZGl2IHtcbiAgICBkaXNwbGF5OiBmbGV4O1xuICAgIGZsZXgtZGlyZWN0aW9uOiByb3c7XG5cbiAgICBpb24tbGFiZWwge1xuICAgICAgICBtYXJnaW4tcmlnaHQ6IDVweDtcbiAgICAgICAgZm9udC1zaXplOiAxMHB4O1xuICAgICAgICBhbGlnbi1zZWxmOiBjZW50ZXI7XG4gICAgfVxufVxuXG5cblxuLnByb2ZpbGUtdmlld2FsbCB7XG4gICAgd2lkdGg6IDEwMCU7XG4gICAgcG9zaXRpb246IHJlbGF0aXZlO1xuICAgIG1hcmdpbi10b3A6IDE1cHg7XG4gICAgbWFyZ2luLWJvdHRvbTogMTVweDtcbn1cblxuLnByb2ZpbGUtdmlld2FsbCBpb24tYnV0dG9uIHtcbiAgICAtLWJvcmRlci1yYWRpdXM6IDI1cHg7XG4gICAgZGlzcGxheTogdGFibGU7XG4gICAgbWFyZ2luOiBhdXRvO1xuICAgIGZvbnQtc2l6ZTogMTJweDtcbn1cblxuLnByb2ZpbGUtdmlld2FsbDpiZWZvcmUge1xuICAgIGNvbnRlbnQ6IFwiXCI7XG4gICAgcG9zaXRpb246IGFic29sdXRlO1xuICAgIGJhY2tncm91bmQ6ICNjMWMxYzE7XG4gICAgaGVpZ2h0OiAxcHg7XG4gICAgd2lkdGg6IDEwMCU7XG4gICAgdG9wOiAxOXB4O1xuICAgIGxlZnQ6IDA7XG59XG5cblxuLmNhcmRfZGF0YV9zZWN0aW9uIHtcbiAgICBpb24tY2FyZCB7XG4gICAgICAgIHdpZHRoOiAxMDAlO1xuICAgICAgICAvLyBoZWlnaHQ6IDIxMHB4O1xuICAgICAgICBtYXJnaW46IDA7XG4gICAgICAgIHBhZGRpbmc6IDVweDtcbiAgICAgICAgXG4gICAgICAgIGlvbi1jYXJkLWhlYWRlciB7XG4gICAgICAgICAgICBwYWRkaW5nOiA1cHg7XG4gICAgICAgICAgICBkaXNwbGF5OiBmbGV4O1xuICAgICAgICAgICAgZmxleC1kaXJlY3Rpb246IGNvbHVtbjtcbiAgICAgICAgfVxuXG4gICAgICAgIGlvbi1jYXJkLWNvbnRlbnQge1xuICAgICAgICAgICAgcGFkZGluZzogNXB4O1xuICAgICAgICAgICAgZGlzcGxheTogZmxleDtcbiAgICAgICAgICAgIGZsZXgtZGlyZWN0aW9uOiBjb2x1bW47XG4gICAgICAgIH1cblxuICAgIH1cbn1cblxuLy8gLmNhcmQtYm94OmJlZm9yZSB7XG4vLyAgICAgY29udGVudDogXCJcIjtcbi8vICAgICBib3JkZXItcmlnaHQ6IDIwcHggc29saWQgI0Y0QjE4Mztcbi8vICAgICBib3JkZXItdG9wOiAyMHB4IHNvbGlkICNGNEIxODM7XG4vLyAgICAgYm9yZGVyLWxlZnQ6IDIwcHggc29saWQgI2ZmMDAwMDAwO1xuLy8gICAgIGJvcmRlci1ib3R0b206IDIwcHggc29saWQgI2ZmMDAwMDAwO1xuLy8gICAgIHBvc2l0aW9uOiBhYnNvbHV0ZTtcbi8vICAgICB0b3A6IDA7XG4vLyAgICAgcmlnaHQ6IDA7XG4vLyAgICAgei1pbmRleDogOTk5O1xuLy8gfVxuXG4uY2FyZC1ib3gge1xuICAgIGJveC1zaGFkb3c6IHJnYigwIDAgMCAvIDEyJSkgMHB4IDRweCAxNnB4O1xuICAgIHBhZGRpbmc6IDVweDtcbn1cblxuLnJpYmJvbi13cmFwcGVyLTEge1xuICAgIHdpZHRoOiA1MXB4O1xuICAgIGhlaWdodDogNjVweDtcbiAgICBvdmVyZmxvdzogaGlkZGVuO1xuICAgIHBvc2l0aW9uOiBhYnNvbHV0ZTtcbiAgICB0b3A6IDBweDtcbiAgICByaWdodDogMHB4O1xufVxuXG4ucmliYm9uLTEge1xuICAgIGZvbnQ6IGJvbGQgMTVweCBTYW5zLVNlcmlmO1xuICAgIGxpbmUtaGVpZ2h0OiAxOHB4O1xuICAgIGNvbG9yOiAjMzMzO1xuICAgIHRleHQtYWxpZ246IGNlbnRlcjtcbiAgICB0ZXh0LXRyYW5zZm9ybTogdXBwZXJjYXNlO1xuICAgIC13ZWJraXQtdHJhbnNmb3JtOiByb3RhdGUoIDQ3ZGVnKTtcbiAgICAtbW96LXRyYW5zZm9ybTogcm90YXRlKDQ1ZGVnKTtcbiAgICAtbXMtdHJhbnNmb3JtOiByb3RhdGUoNDVkZWcpO1xuICAgIC1vLXRyYW5zZm9ybTogcm90YXRlKDQ1ZGVnKTtcbiAgICBwb3NpdGlvbjogcmVsYXRpdmU7XG4gICAgcGFkZGluZzogN3B4IDA7XG4gICAgbGVmdDogMHB4O1xuICAgIHRvcDogMTZweDtcbiAgICBoZWlnaHQ6IDQ0cHg7XG4gICAgd2lkdGg6IDE1MHB4O1xuICAgIGJhY2tncm91bmQ6IHZhcigtLWlvbi1idG4tY3VzdG9tLWNvbG9yKTtcbiAgICBjb2xvcjogI2ZmZjtcbiAgICBib3gtc2hhZG93OiAwIDRweCA2cHggcmdiKDAgMCAwIC8gMTAlKTtcbiAgICBsZXR0ZXItc3BhY2luZzogMC41cHg7XG4gICAgei1pbmRleDogMTtcbn1cblxuXG4uZGVzY190ZXh0IHtcbiAgICBkaXNwbGF5OiBmbGV4O1xufVxuXG4uY2xvc2VfaWNvbiB7XG4gICAgYWxpZ24tc2VsZjogY2VudGVyICFpbXBvcnRhbnQ7XG4gICAgZm9udC13ZWlnaHQ6IGJvbGQgIWltcG9ydGFudDtcbiAgICBjb2xvcjogIzAwMCAhaW1wb3J0YW50O1xuICAgIGZvbnQtc2l6ZTogMjNweCAhaW1wb3J0YW50O1xufSJdfQ== */";
      /***/
    },

    /***/
    "./src/app/components/modals/sponsership-modal/sponsership-modal.component.ts":
    /*!************************************************************************************!*\
      !*** ./src/app/components/modals/sponsership-modal/sponsership-modal.component.ts ***!
      \************************************************************************************/

    /*! exports provided: SponsershipModalComponent */

    /***/
    function srcAppComponentsModalsSponsershipModalSponsershipModalComponentTs(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "SponsershipModalComponent", function () {
        return SponsershipModalComponent;
      });
      /* harmony import */


      var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! tslib */
      "./node_modules/tslib/tslib.es6.js");
      /* harmony import */


      var _ionic_angular__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! @ionic/angular */
      "./node_modules/@ionic/angular/__ivy_ngcc__/fesm2015/ionic-angular.js");
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! @angular/core */
      "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
      /* harmony import */


      var hammerjs__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
      /*! hammerjs */
      "./node_modules/hammerjs/hammer.js");
      /* harmony import */


      var hammerjs__WEBPACK_IMPORTED_MODULE_3___default = /*#__PURE__*/__webpack_require__.n(hammerjs__WEBPACK_IMPORTED_MODULE_3__);

      var SponsershipModalComponent = /*#__PURE__*/function () {
        function SponsershipModalComponent(modalCtrl, gestureCtrl, element, renderer) {
          _classCallCheck(this, SponsershipModalComponent);

          this.modalCtrl = modalCtrl;
          this.gestureCtrl = gestureCtrl;
          this.element = element;
          this.renderer = renderer;
          this.segmentModel = "principle";
          this.showRow = "first";
        }

        _createClass(SponsershipModalComponent, [{
          key: "segmentChanged",
          value: function segmentChanged(event) {
            console.log(this.segmentModel);
            console.log(event);
            this.segmentModel = event.detail.value;
          }
        }, {
          key: "ngOnInit",
          value: function ngOnInit() {}
        }, {
          key: "closeModal",
          value: function closeModal() {
            // this.modalCtrl.dismiss()
            if (this.showRow == 'third') {
              this.modalCtrl.dismiss();
            } else {
              this.showRow = "second";
            }
          }
        }, {
          key: "ngAfterViewInit",
          value: function ngAfterViewInit() {
            return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, /*#__PURE__*/regeneratorRuntime.mark(function _callee24() {
              var _this26 = this;

              var d, options, gesture;
              return regeneratorRuntime.wrap(function _callee24$(_context24) {
                while (1) {
                  switch (_context24.prev = _context24.next) {
                    case 0:
                      d = document.querySelector('ion-modal');
                      options = {
                        el: d,
                        direction: "y",
                        gestureName: 'swipe',
                        onStart: function onStart() {
                          _this26.renderer.setStyle(d, 'transition', 'none');
                        },
                        onMove: function onMove(ev) {
                          if (ev.deltaY > 0) {
                            _this26.renderer.setStyle(d, 'transform', "translateY(".concat(ev.deltaY, "px)"));
                          }
                        },
                        onEnd: function onEnd(ev) {
                          _this26.renderer.setStyle(d, 'transition', '0.5s ease-out');

                          if (ev.deltaY > -200) {
                            _this26.renderer.setStyle(d, 'transform', 'translateY(500px)');

                            _this26.modalCtrl.dismiss();
                          } else {
                            _this26.renderer.setStyle(d, 'transform', 'translateY(0px)');
                          }
                        }
                      };
                      _context24.next = 4;
                      return this.gestureCtrl.create(options);

                    case 4:
                      gesture = _context24.sent;
                      gesture.enable();

                    case 6:
                    case "end":
                      return _context24.stop();
                  }
                }
              }, _callee24, this);
            }));
          }
        }]);

        return SponsershipModalComponent;
      }();

      SponsershipModalComponent.ctorParameters = function () {
        return [{
          type: _ionic_angular__WEBPACK_IMPORTED_MODULE_1__["ModalController"]
        }, {
          type: _ionic_angular__WEBPACK_IMPORTED_MODULE_1__["GestureController"]
        }, {
          type: _angular_core__WEBPACK_IMPORTED_MODULE_2__["ElementRef"]
        }, {
          type: _angular_core__WEBPACK_IMPORTED_MODULE_2__["Renderer2"]
        }];
      };

      SponsershipModalComponent = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([Object(_angular_core__WEBPACK_IMPORTED_MODULE_2__["Component"])({
        selector: 'app-sponsership-modal',
        template: Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(
        /*! raw-loader!./sponsership-modal.component.html */
        "./node_modules/raw-loader/dist/cjs.js!./src/app/components/modals/sponsership-modal/sponsership-modal.component.html"))["default"],
        styles: [Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(
        /*! ./sponsership-modal.component.scss */
        "./src/app/components/modals/sponsership-modal/sponsership-modal.component.scss"))["default"]]
      })], SponsershipModalComponent);
      /***/
    },

    /***/
    "./src/app/components/musculation-auto-seances-session/musculation-auto-seances-session.component.scss":
    /*!*************************************************************************************************************!*\
      !*** ./src/app/components/musculation-auto-seances-session/musculation-auto-seances-session.component.scss ***!
      \*************************************************************************************************************/

    /*! exports provided: default */

    /***/
    function srcAppComponentsMusculationAutoSeancesSessionMusculationAutoSeancesSessionComponentScss(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony default export */


      __webpack_exports__["default"] = ".titlebox {\n  position: relative;\n  color: #fff;\n  overflow: hidden;\n  height: 57%;\n}\n\n.titlebox-text {\n  position: absolute;\n  bottom: 5%;\n  left: 5%;\n}\n\n.title-img {\n  background: #000;\n  height: 100%;\n}\n\n.title-img img {\n  height: 100%;\n  -o-object-fit: cover;\n     object-fit: cover;\n  width: 100%;\n  -o-object-position: top;\n     object-position: top;\n}\n\n.titlebox-text h5 {\n  font-size: 12px;\n  margin: 0;\n}\n\n.musculetion-vid h4 {\n  font-size: 12px;\n  text-align: center;\n  margin: 0;\n  margin-top: 5px;\n}\n\n.musculetion-vid {\n  width: 100%;\n}\n\n.musculetion-vid-img img {\n  -o-object-fit: cover;\n     object-fit: cover;\n  -o-object-position: top;\n     object-position: top;\n  height: 90px;\n  width: 90px;\n  border-radius: 50%;\n}\n\nion-item {\n  --inner-border-width: 0 0 0px 0;\n  --border-width: 0 0 0px 0;\n}\n\n.musculetion-vid-title h6 {\n  margin: 0;\n  font-size: 10px;\n  margin-top: 5px;\n}\n\n.titlebox-add {\n  position: relative;\n}\n\n.titlebox-add ion-icon {\n  position: absolute;\n  display: flex;\n  top: -26px;\n  right: 3%;\n  color: #fff;\n  border-radius: 25px;\n  background: var(--ion-btn-custom-color);\n  padding: 4px;\n  font-size: 35px;\n}\n\n.musculetion-vid-img {\n  position: relative;\n}\n\n.musculetion-vid-img ion-icon {\n  position: absolute;\n  left: 30px;\n  top: 30px;\n  background: #fffafab3;\n  border-radius: 25px;\n  font-size: 31px;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvY29tcG9uZW50cy9tdXNjdWxhdGlvbi1hdXRvLXNlYW5jZXMtc2Vzc2lvbi9tdXNjdWxhdGlvbi1hdXRvLXNlYW5jZXMtc2Vzc2lvbi5jb21wb25lbnQuc2NzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQTtFQUNJLGtCQUFBO0VBQ0EsV0FBQTtFQUVBLGdCQUFBO0VBRUEsV0FBQTtBQURKOztBQUtBO0VBQ0ksa0JBQUE7RUFDQSxVQUFBO0VBQ0EsUUFBQTtBQUZKOztBQUtBO0VBQ0ksZ0JBQUE7RUFDQSxZQUFBO0FBRko7O0FBS0E7RUFDSSxZQUFBO0VBQ0Esb0JBQUE7S0FBQSxpQkFBQTtFQUNBLFdBQUE7RUFDQSx1QkFBQTtLQUFBLG9CQUFBO0FBRko7O0FBS0E7RUFDSSxlQUFBO0VBQ0EsU0FBQTtBQUZKOztBQUtBO0VBQ0ksZUFBQTtFQUNBLGtCQUFBO0VBQ0EsU0FBQTtFQUNBLGVBQUE7QUFGSjs7QUFLQTtFQUNJLFdBQUE7QUFGSjs7QUFLQTtFQUNJLG9CQUFBO0tBQUEsaUJBQUE7RUFDQSx1QkFBQTtLQUFBLG9CQUFBO0VBQ0EsWUFBQTtFQUNBLFdBQUE7RUFDQSxrQkFBQTtBQUZKOztBQUtBO0VBQ0ksK0JBQUE7RUFDQSx5QkFBQTtBQUZKOztBQUtBO0VBQ0ksU0FBQTtFQUNBLGVBQUE7RUFDQSxlQUFBO0FBRko7O0FBS0E7RUFDSSxrQkFBQTtBQUZKOztBQUtBO0VBQ0ksa0JBQUE7RUFDQSxhQUFBO0VBQ0EsVUFBQTtFQUNBLFNBQUE7RUFDQSxXQUFBO0VBQ0EsbUJBQUE7RUFDQSx1Q0FBQTtFQUNBLFlBQUE7RUFDQSxlQUFBO0FBRko7O0FBS0E7RUFDSSxrQkFBQTtBQUZKOztBQUtBO0VBQ0ksa0JBQUE7RUFDQSxVQUFBO0VBQ0EsU0FBQTtFQUNBLHFCQUFBO0VBQ0EsbUJBQUE7RUFDQSxlQUFBO0FBRkoiLCJmaWxlIjoic3JjL2FwcC9jb21wb25lbnRzL211c2N1bGF0aW9uLWF1dG8tc2VhbmNlcy1zZXNzaW9uL211c2N1bGF0aW9uLWF1dG8tc2VhbmNlcy1zZXNzaW9uLmNvbXBvbmVudC5zY3NzIiwic291cmNlc0NvbnRlbnQiOlsiLnRpdGxlYm94IHtcbiAgICBwb3NpdGlvbjogcmVsYXRpdmU7XG4gICAgY29sb3I6ICNmZmY7XG4gICAgLy8gaGVpZ2h0OiAyNTBweDtcbiAgICBvdmVyZmxvdzogaGlkZGVuO1xuXG4gICAgaGVpZ2h0OiA1NyU7XG5cbn1cblxuLnRpdGxlYm94LXRleHQge1xuICAgIHBvc2l0aW9uOiBhYnNvbHV0ZTtcbiAgICBib3R0b206IDUlO1xuICAgIGxlZnQ6IDUlO1xufVxuXG4udGl0bGUtaW1nIHtcbiAgICBiYWNrZ3JvdW5kOiAjMDAwO1xuICAgIGhlaWdodDogMTAwJTtcbn1cblxuLnRpdGxlLWltZyBpbWcge1xuICAgIGhlaWdodDogMTAwJTtcbiAgICBvYmplY3QtZml0OiBjb3ZlcjtcbiAgICB3aWR0aDogMTAwJTtcbiAgICBvYmplY3QtcG9zaXRpb246IHRvcDtcbn1cblxuLnRpdGxlYm94LXRleHQgaDUge1xuICAgIGZvbnQtc2l6ZTogMTJweDtcbiAgICBtYXJnaW46IDA7XG59XG5cbi5tdXNjdWxldGlvbi12aWQgaDQge1xuICAgIGZvbnQtc2l6ZTogMTJweDtcbiAgICB0ZXh0LWFsaWduOiBjZW50ZXI7XG4gICAgbWFyZ2luOiAwO1xuICAgIG1hcmdpbi10b3A6IDVweDtcbn1cblxuLm11c2N1bGV0aW9uLXZpZCB7XG4gICAgd2lkdGg6IDEwMCU7XG59XG5cbi5tdXNjdWxldGlvbi12aWQtaW1nIGltZyB7XG4gICAgb2JqZWN0LWZpdDogY292ZXI7XG4gICAgb2JqZWN0LXBvc2l0aW9uOiB0b3A7XG4gICAgaGVpZ2h0OiA5MHB4O1xuICAgIHdpZHRoOiA5MHB4O1xuICAgIGJvcmRlci1yYWRpdXM6IDUwJTtcbn1cblxuaW9uLWl0ZW0ge1xuICAgIC0taW5uZXItYm9yZGVyLXdpZHRoOiAwIDAgMHB4IDA7XG4gICAgLS1ib3JkZXItd2lkdGg6IDAgMCAwcHggMDtcbn1cblxuLm11c2N1bGV0aW9uLXZpZC10aXRsZSBoNiB7XG4gICAgbWFyZ2luOiAwO1xuICAgIGZvbnQtc2l6ZTogMTBweDtcbiAgICBtYXJnaW4tdG9wOiA1cHg7XG59XG5cbi50aXRsZWJveC1hZGQge1xuICAgIHBvc2l0aW9uOiByZWxhdGl2ZTtcbn1cblxuLnRpdGxlYm94LWFkZCBpb24taWNvbiB7XG4gICAgcG9zaXRpb246IGFic29sdXRlO1xuICAgIGRpc3BsYXk6IGZsZXg7XG4gICAgdG9wOiAtMjZweDtcbiAgICByaWdodDogMyU7XG4gICAgY29sb3I6ICNmZmY7XG4gICAgYm9yZGVyLXJhZGl1czogMjVweDtcbiAgICBiYWNrZ3JvdW5kOiB2YXIoLS1pb24tYnRuLWN1c3RvbS1jb2xvcik7XG4gICAgcGFkZGluZzogNHB4O1xuICAgIGZvbnQtc2l6ZTogMzVweDtcbn1cblxuLm11c2N1bGV0aW9uLXZpZC1pbWcge1xuICAgIHBvc2l0aW9uOiByZWxhdGl2ZTtcbn1cblxuLm11c2N1bGV0aW9uLXZpZC1pbWcgaW9uLWljb24ge1xuICAgIHBvc2l0aW9uOiBhYnNvbHV0ZTtcbiAgICBsZWZ0OiAzMHB4O1xuICAgIHRvcDogMzBweDtcbiAgICBiYWNrZ3JvdW5kOiAjZmZmYWZhYjM7XG4gICAgYm9yZGVyLXJhZGl1czogMjVweDtcbiAgICBmb250LXNpemU6IDMxcHg7XG59Il19 */";
      /***/
    },

    /***/
    "./src/app/components/musculation-auto-seances-session/musculation-auto-seances-session.component.ts":
    /*!***********************************************************************************************************!*\
      !*** ./src/app/components/musculation-auto-seances-session/musculation-auto-seances-session.component.ts ***!
      \***********************************************************************************************************/

    /*! exports provided: MusculationAutoSeancesSessionComponent */

    /***/
    function srcAppComponentsMusculationAutoSeancesSessionMusculationAutoSeancesSessionComponentTs(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "MusculationAutoSeancesSessionComponent", function () {
        return MusculationAutoSeancesSessionComponent;
      });
      /* harmony import */


      var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! tslib */
      "./node_modules/tslib/tslib.es6.js");
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! @angular/core */
      "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");

      var MusculationAutoSeancesSessionComponent = /*#__PURE__*/function () {
        function MusculationAutoSeancesSessionComponent() {
          _classCallCheck(this, MusculationAutoSeancesSessionComponent);

          this.categories = {
            slidesPerView: 3.5,
            effect: 'flip',
            slidesPerColumn: 1,
            slidesPerGroup: 1,
            watchSlidesProgress: false,
            spaceBetween: 10
          };
        }

        _createClass(MusculationAutoSeancesSessionComponent, [{
          key: "ngOnInit",
          value: function ngOnInit() {}
        }]);

        return MusculationAutoSeancesSessionComponent;
      }();

      MusculationAutoSeancesSessionComponent.ctorParameters = function () {
        return [];
      };

      MusculationAutoSeancesSessionComponent = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-musculation-auto-seances-session',
        template: Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(
        /*! raw-loader!./musculation-auto-seances-session.component.html */
        "./node_modules/raw-loader/dist/cjs.js!./src/app/components/musculation-auto-seances-session/musculation-auto-seances-session.component.html"))["default"],
        styles: [Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(
        /*! ./musculation-auto-seances-session.component.scss */
        "./src/app/components/musculation-auto-seances-session/musculation-auto-seances-session.component.scss"))["default"]]
      })], MusculationAutoSeancesSessionComponent);
      /***/
    },

    /***/
    "./src/app/components/musculation-auto-seances/musculation-auto-seances.component.scss":
    /*!*********************************************************************************************!*\
      !*** ./src/app/components/musculation-auto-seances/musculation-auto-seances.component.scss ***!
      \*********************************************************************************************/

    /*! exports provided: default */

    /***/
    function srcAppComponentsMusculationAutoSeancesMusculationAutoSeancesComponentScss(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony default export */


      __webpack_exports__["default"] = ".titlebox {\n  position: relative;\n  color: #fff;\n  height: 57%;\n  overflow: hidden;\n}\n\n.titlebox-arrow {\n  display: flex;\n  position: absolute;\n  width: 100%;\n  top: 5%;\n  padding: 0px 5%;\n  transform: translateY(-5%);\n}\n\n.titlebox-arrow div {\n  flex-grow: 1;\n  font-size: 30px;\n}\n\n.titlebox-text {\n  position: absolute;\n  bottom: 5%;\n  left: 5%;\n}\n\n.title-img {\n  background: #000;\n  height: 100%;\n}\n\n.title-img img {\n  height: 100%;\n  -o-object-fit: cover;\n     object-fit: cover;\n  width: 100%;\n  -o-object-position: top;\n     object-position: top;\n}\n\n.titlebox-text h4 {\n  margin: 0;\n}\n\n.titlebox-text h5 {\n  font-size: 12px;\n}\n\n.musculetion-vid h4 {\n  font-size: 12px;\n  text-align: left;\n  margin: 0;\n  margin-top: 5px;\n}\n\n.musculetion-vid {\n  width: 100%;\n}\n\n.musculetion-vid-img img {\n  width: 100%;\n  -o-object-fit: cover;\n     object-fit: cover;\n  height: 150px;\n  -o-object-position: top;\n     object-position: top;\n}\n\nion-item {\n  --inner-border-width: 0 0 0px 0;\n  --border-width: 0 0 0px 0;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvY29tcG9uZW50cy9tdXNjdWxhdGlvbi1hdXRvLXNlYW5jZXMvbXVzY3VsYXRpb24tYXV0by1zZWFuY2VzLmNvbXBvbmVudC5zY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBO0VBQ0ksa0JBQUE7RUFDQSxXQUFBO0VBQ0EsV0FBQTtFQUNBLGdCQUFBO0FBQ0o7O0FBRUE7RUFDSSxhQUFBO0VBQ0Esa0JBQUE7RUFDQSxXQUFBO0VBQ0EsT0FBQTtFQUNBLGVBQUE7RUFDQSwwQkFBQTtBQUNKOztBQUVBO0VBQ0ksWUFBQTtFQUNBLGVBQUE7QUFDSjs7QUFFQTtFQUNJLGtCQUFBO0VBQ0EsVUFBQTtFQUNBLFFBQUE7QUFDSjs7QUFFQTtFQUNJLGdCQUFBO0VBQ0EsWUFBQTtBQUNKOztBQUVBO0VBQ0ksWUFBQTtFQUNBLG9CQUFBO0tBQUEsaUJBQUE7RUFDQSxXQUFBO0VBQ0EsdUJBQUE7S0FBQSxvQkFBQTtBQUNKOztBQUVBO0VBQ0ksU0FBQTtBQUNKOztBQUVBO0VBQ0ksZUFBQTtBQUNKOztBQUVBO0VBQ0ksZUFBQTtFQUNBLGdCQUFBO0VBQ0EsU0FBQTtFQUNBLGVBQUE7QUFDSjs7QUFFQTtFQUNJLFdBQUE7QUFDSjs7QUFFQTtFQUNJLFdBQUE7RUFDQSxvQkFBQTtLQUFBLGlCQUFBO0VBQ0EsYUFBQTtFQUNBLHVCQUFBO0tBQUEsb0JBQUE7QUFDSjs7QUFFQTtFQUNJLCtCQUFBO0VBQ0EseUJBQUE7QUFDSiIsImZpbGUiOiJzcmMvYXBwL2NvbXBvbmVudHMvbXVzY3VsYXRpb24tYXV0by1zZWFuY2VzL211c2N1bGF0aW9uLWF1dG8tc2VhbmNlcy5jb21wb25lbnQuc2NzcyIsInNvdXJjZXNDb250ZW50IjpbIi50aXRsZWJveCB7XG4gICAgcG9zaXRpb246IHJlbGF0aXZlO1xuICAgIGNvbG9yOiAjZmZmO1xuICAgIGhlaWdodDogNTclO1xuICAgIG92ZXJmbG93OiBoaWRkZW47XG59XG5cbi50aXRsZWJveC1hcnJvdyB7XG4gICAgZGlzcGxheTogZmxleDtcbiAgICBwb3NpdGlvbjogYWJzb2x1dGU7XG4gICAgd2lkdGg6IDEwMCU7XG4gICAgdG9wOiA1JTtcbiAgICBwYWRkaW5nOiAwcHggNSU7XG4gICAgdHJhbnNmb3JtOiB0cmFuc2xhdGVZKC01JSk7XG59XG5cbi50aXRsZWJveC1hcnJvdyBkaXYge1xuICAgIGZsZXgtZ3JvdzogMTtcbiAgICBmb250LXNpemU6IDMwcHg7XG59XG5cbi50aXRsZWJveC10ZXh0IHtcbiAgICBwb3NpdGlvbjogYWJzb2x1dGU7XG4gICAgYm90dG9tOiA1JTtcbiAgICBsZWZ0OiA1JTtcbn1cblxuLnRpdGxlLWltZyB7XG4gICAgYmFja2dyb3VuZDogIzAwMDtcbiAgICBoZWlnaHQ6IDEwMCU7XG59XG5cbi50aXRsZS1pbWcgaW1nIHtcbiAgICBoZWlnaHQ6IDEwMCU7XG4gICAgb2JqZWN0LWZpdDogY292ZXI7XG4gICAgd2lkdGg6IDEwMCU7XG4gICAgb2JqZWN0LXBvc2l0aW9uOiB0b3A7XG59XG5cbi50aXRsZWJveC10ZXh0IGg0IHtcbiAgICBtYXJnaW46IDA7XG59XG5cbi50aXRsZWJveC10ZXh0IGg1IHtcbiAgICBmb250LXNpemU6IDEycHg7XG59XG5cbi5tdXNjdWxldGlvbi12aWQgaDQge1xuICAgIGZvbnQtc2l6ZTogMTJweDtcbiAgICB0ZXh0LWFsaWduOiBsZWZ0O1xuICAgIG1hcmdpbjogMDtcbiAgICBtYXJnaW4tdG9wOiA1cHg7XG59XG5cbi5tdXNjdWxldGlvbi12aWQge1xuICAgIHdpZHRoOiAxMDAlO1xufVxuXG4ubXVzY3VsZXRpb24tdmlkLWltZyBpbWcge1xuICAgIHdpZHRoOiAxMDAlO1xuICAgIG9iamVjdC1maXQ6IGNvdmVyO1xuICAgIGhlaWdodDogMTUwcHg7XG4gICAgb2JqZWN0LXBvc2l0aW9uOiB0b3A7XG59XG5cbmlvbi1pdGVtIHtcbiAgICAtLWlubmVyLWJvcmRlci13aWR0aDogMCAwIDBweCAwO1xuICAgIC0tYm9yZGVyLXdpZHRoOiAwIDAgMHB4IDA7XG59Il19 */";
      /***/
    },

    /***/
    "./src/app/components/musculation-auto-seances/musculation-auto-seances.component.ts":
    /*!*******************************************************************************************!*\
      !*** ./src/app/components/musculation-auto-seances/musculation-auto-seances.component.ts ***!
      \*******************************************************************************************/

    /*! exports provided: MusculationAutoSeancesComponent */

    /***/
    function srcAppComponentsMusculationAutoSeancesMusculationAutoSeancesComponentTs(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "MusculationAutoSeancesComponent", function () {
        return MusculationAutoSeancesComponent;
      });
      /* harmony import */


      var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! tslib */
      "./node_modules/tslib/tslib.es6.js");
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! @angular/core */
      "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
      /* harmony import */


      var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! @angular/router */
      "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");

      var MusculationAutoSeancesComponent = /*#__PURE__*/function () {
        function MusculationAutoSeancesComponent(route) {
          _classCallCheck(this, MusculationAutoSeancesComponent);

          this.route = route;
          this.backpage = "";
          this.categories = {
            slidesPerView: 1.8,
            effect: 'flip',
            slidesPerColumn: 1,
            slidesPerGroup: 1,
            watchSlidesProgress: false,
            spaceBetween: 10
          };
          this.backpage = "/" + this.route.snapshot.paramMap.get('page');
          console.log(this.backpage);
        }

        _createClass(MusculationAutoSeancesComponent, [{
          key: "ngOnInit",
          value: function ngOnInit() {}
        }]);

        return MusculationAutoSeancesComponent;
      }();

      MusculationAutoSeancesComponent.ctorParameters = function () {
        return [{
          type: _angular_router__WEBPACK_IMPORTED_MODULE_2__["ActivatedRoute"]
        }];
      };

      MusculationAutoSeancesComponent = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-musculation-auto-seances',
        template: Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(
        /*! raw-loader!./musculation-auto-seances.component.html */
        "./node_modules/raw-loader/dist/cjs.js!./src/app/components/musculation-auto-seances/musculation-auto-seances.component.html"))["default"],
        styles: [Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(
        /*! ./musculation-auto-seances.component.scss */
        "./src/app/components/musculation-auto-seances/musculation-auto-seances.component.scss"))["default"]]
      })], MusculationAutoSeancesComponent);
      /***/
    },

    /***/
    "./src/app/components/performance-profile/performance-profile.component.scss":
    /*!***********************************************************************************!*\
      !*** ./src/app/components/performance-profile/performance-profile.component.scss ***!
      \***********************************************************************************/

    /*! exports provided: default */

    /***/
    function srcAppComponentsPerformanceProfilePerformanceProfileComponentScss(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony default export */


      __webpack_exports__["default"] = "ion-grid {\n  --padding-start: 10px;\n  --padding-end: 10px ;\n}\n\n.performance_text {\n  text-align: justify;\n  line-height: 1.1rem;\n  font-size: 13px;\n  font-weight: 100;\n}\n\n.main_container {\n  padding-top: 15px;\n  padding-right: 10px;\n}\n\n.filter-text {\n  font-family: \"Oswald\";\n  font-size: 13px;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvY29tcG9uZW50cy9wZXJmb3JtYW5jZS1wcm9maWxlL3BlcmZvcm1hbmNlLXByb2ZpbGUuY29tcG9uZW50LnNjc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUE7RUFDSSxxQkFBQTtFQUNBLG9CQUFBO0FBQ0o7O0FBRUE7RUFDSSxtQkFBQTtFQUNBLG1CQUFBO0VBQ0EsZUFBQTtFQUNBLGdCQUFBO0FBQ0o7O0FBR0E7RUFFSSxpQkFBQTtFQUNBLG1CQUFBO0FBREo7O0FBSUE7RUFDSSxxQkFBQTtFQUNBLGVBQUE7QUFESiIsImZpbGUiOiJzcmMvYXBwL2NvbXBvbmVudHMvcGVyZm9ybWFuY2UtcHJvZmlsZS9wZXJmb3JtYW5jZS1wcm9maWxlLmNvbXBvbmVudC5zY3NzIiwic291cmNlc0NvbnRlbnQiOlsiaW9uLWdyaWQge1xuICAgIC0tcGFkZGluZy1zdGFydDogMTBweDtcbiAgICAtLXBhZGRpbmctZW5kOiAxMHB4XG59XG5cbi5wZXJmb3JtYW5jZV90ZXh0IHtcbiAgICB0ZXh0LWFsaWduOiBqdXN0aWZ5O1xuICAgIGxpbmUtaGVpZ2h0OiAxLjFyZW07XG4gICAgZm9udC1zaXplOiAxM3B4O1xuICAgIGZvbnQtd2VpZ2h0OiAxMDA7XG5cbn1cblxuLm1haW5fY29udGFpbmVyIHtcbiAgICAvLyBwYWRkaW5nLWxlZnQ6IDEwcHg7XG4gICAgcGFkZGluZy10b3A6IDE1cHg7XG4gICAgcGFkZGluZy1yaWdodDogMTBweDtcbn1cblxuLmZpbHRlci10ZXh0IHtcbiAgICBmb250LWZhbWlseTogJ09zd2FsZCc7XG4gICAgZm9udC1zaXplOiAxM3B4O1xufSJdfQ== */";
      /***/
    },

    /***/
    "./src/app/components/performance-profile/performance-profile.component.ts":
    /*!*********************************************************************************!*\
      !*** ./src/app/components/performance-profile/performance-profile.component.ts ***!
      \*********************************************************************************/

    /*! exports provided: PerformanceProfileComponent */

    /***/
    function srcAppComponentsPerformanceProfilePerformanceProfileComponentTs(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "PerformanceProfileComponent", function () {
        return PerformanceProfileComponent;
      });
      /* harmony import */


      var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! tslib */
      "./node_modules/tslib/tslib.es6.js");
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! @angular/core */
      "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");

      var PerformanceProfileComponent = /*#__PURE__*/function () {
        function PerformanceProfileComponent() {
          _classCallCheck(this, PerformanceProfileComponent);
        }

        _createClass(PerformanceProfileComponent, [{
          key: "ngOnInit",
          value: function ngOnInit() {}
        }]);

        return PerformanceProfileComponent;
      }();

      PerformanceProfileComponent.ctorParameters = function () {
        return [];
      };

      PerformanceProfileComponent = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-performance-profile',
        template: Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(
        /*! raw-loader!./performance-profile.component.html */
        "./node_modules/raw-loader/dist/cjs.js!./src/app/components/performance-profile/performance-profile.component.html"))["default"],
        styles: [Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(
        /*! ./performance-profile.component.scss */
        "./src/app/components/performance-profile/performance-profile.component.scss"))["default"]]
      })], PerformanceProfileComponent);
      /***/
    },

    /***/
    "./src/app/components/physiology-profile/physiology-profile.component.scss":
    /*!*********************************************************************************!*\
      !*** ./src/app/components/physiology-profile/physiology-profile.component.scss ***!
      \*********************************************************************************/

    /*! exports provided: default */

    /***/
    function srcAppComponentsPhysiologyProfilePhysiologyProfileComponentScss(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony default export */


      __webpack_exports__["default"] = "ion-grid {\n  --padding-start: 10px;\n  --padding-end: 10px ;\n}\n\n.performance_text {\n  text-align: justify;\n  line-height: 1.1rem;\n  font-size: 13px;\n  font-weight: 100;\n}\n\n.padding-start-0 {\n  --padding-start: 0 !important;\n}\n\n.main_container {\n  padding-left: 10px;\n  padding-top: 15px;\n  padding-right: 10px;\n}\n\n.date_label {\n  text-align: justify;\n  line-height: 1.1rem;\n  font-size: 13px;\n  font-weight: 800;\n  font-family: \"Oswald-SemiBold\";\n}\n\nion-datetime {\n  font-size: 12px;\n  font-weight: 100;\n}\n\n.image-compair {\n  width: 300px !important;\n  height: 350px !important;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvY29tcG9uZW50cy9waHlzaW9sb2d5LXByb2ZpbGUvcGh5c2lvbG9neS1wcm9maWxlLmNvbXBvbmVudC5zY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBO0VBQ0kscUJBQUE7RUFDQSxvQkFBQTtBQUNKOztBQUVBO0VBQ0ksbUJBQUE7RUFDQSxtQkFBQTtFQUNBLGVBQUE7RUFDQSxnQkFBQTtBQUNKOztBQUVBO0VBQ0ksNkJBQUE7QUFDSjs7QUFFQTtFQUNJLGtCQUFBO0VBQ0EsaUJBQUE7RUFDQSxtQkFBQTtBQUNKOztBQUVBO0VBQ0ksbUJBQUE7RUFDQSxtQkFBQTtFQUNBLGVBQUE7RUFDQSxnQkFBQTtFQUNBLDhCQUFBO0FBQ0o7O0FBRUE7RUFDSSxlQUFBO0VBQ0EsZ0JBQUE7QUFDSjs7QUFLQTtFQUNJLHVCQUFBO0VBQ0Esd0JBQUE7QUFGSiIsImZpbGUiOiJzcmMvYXBwL2NvbXBvbmVudHMvcGh5c2lvbG9neS1wcm9maWxlL3BoeXNpb2xvZ3ktcHJvZmlsZS5jb21wb25lbnQuc2NzcyIsInNvdXJjZXNDb250ZW50IjpbImlvbi1ncmlkIHtcbiAgICAtLXBhZGRpbmctc3RhcnQ6IDEwcHg7XG4gICAgLS1wYWRkaW5nLWVuZDogMTBweFxufVxuXG4ucGVyZm9ybWFuY2VfdGV4dCB7XG4gICAgdGV4dC1hbGlnbjoganVzdGlmeTtcbiAgICBsaW5lLWhlaWdodDogMS4xcmVtO1xuICAgIGZvbnQtc2l6ZTogMTNweDtcbiAgICBmb250LXdlaWdodDogMTAwO1xufVxuXG4ucGFkZGluZy1zdGFydC0wIHtcbiAgICAtLXBhZGRpbmctc3RhcnQ6IDAgIWltcG9ydGFudDtcbn1cblxuLm1haW5fY29udGFpbmVyIHtcbiAgICBwYWRkaW5nLWxlZnQ6IDEwcHg7XG4gICAgcGFkZGluZy10b3A6IDE1cHg7XG4gICAgcGFkZGluZy1yaWdodDogMTBweDtcbn1cblxuLmRhdGVfbGFiZWwge1xuICAgIHRleHQtYWxpZ246IGp1c3RpZnk7XG4gICAgbGluZS1oZWlnaHQ6IDEuMXJlbTtcbiAgICBmb250LXNpemU6IDEzcHg7XG4gICAgZm9udC13ZWlnaHQ6IDgwMDtcbiAgICBmb250LWZhbWlseTogJ09zd2FsZC1TZW1pQm9sZCc7XG59XG5cbmlvbi1kYXRldGltZSB7XG4gICAgZm9udC1zaXplOiAxMnB4O1xuICAgIGZvbnQtd2VpZ2h0OiAxMDA7XG59XG5cbi8vIC5zZWdtZW50X2NvbnRhaW5lcl9kYXRhIHtcbi8vICAgICBtYXJnaW4tYm90dG9tOiAyNSU7XG4vLyB9XG4uaW1hZ2UtY29tcGFpciB7XG4gICAgd2lkdGg6IDMwMHB4ICFpbXBvcnRhbnQ7XG4gICAgaGVpZ2h0OiAzNTBweCAhaW1wb3J0YW50O1xufSJdfQ== */";
      /***/
    },

    /***/
    "./src/app/components/physiology-profile/physiology-profile.component.ts":
    /*!*******************************************************************************!*\
      !*** ./src/app/components/physiology-profile/physiology-profile.component.ts ***!
      \*******************************************************************************/

    /*! exports provided: PhysiologyProfileComponent */

    /***/
    function srcAppComponentsPhysiologyProfilePhysiologyProfileComponentTs(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "PhysiologyProfileComponent", function () {
        return PhysiologyProfileComponent;
      });
      /* harmony import */


      var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! tslib */
      "./node_modules/tslib/tslib.es6.js");
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! @angular/core */
      "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");

      var PhysiologyProfileComponent = /*#__PURE__*/function () {
        function PhysiologyProfileComponent() {
          _classCallCheck(this, PhysiologyProfileComponent);

          this.physiologySegmentModel = "phote-de-face";
        }

        _createClass(PhysiologyProfileComponent, [{
          key: "ngOnInit",
          value: function ngOnInit() {}
        }, {
          key: "physiologySegmentChanged",
          value: function physiologySegmentChanged(event) {
            console.log(this.physiologySegmentModel);
            console.log(this.physiologySegmentModel);
            console.log(event);
            this.physiologySegmentModel = event.detail.value;
          }
        }]);

        return PhysiologyProfileComponent;
      }();

      PhysiologyProfileComponent.ctorParameters = function () {
        return [];
      };

      PhysiologyProfileComponent = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-physiology-profile',
        template: Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(
        /*! raw-loader!./physiology-profile.component.html */
        "./node_modules/raw-loader/dist/cjs.js!./src/app/components/physiology-profile/physiology-profile.component.html"))["default"],
        styles: [Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(
        /*! ./physiology-profile.component.scss */
        "./src/app/components/physiology-profile/physiology-profile.component.scss"))["default"]]
      })], PhysiologyProfileComponent);
      /***/
    },

    /***/
    "./src/app/components/records-profile/records-profile.component.scss":
    /*!***************************************************************************!*\
      !*** ./src/app/components/records-profile/records-profile.component.scss ***!
      \***************************************************************************/

    /*! exports provided: default */

    /***/
    function srcAppComponentsRecordsProfileRecordsProfileComponentScss(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony default export */


      __webpack_exports__["default"] = "ion-grid {\n  --padding-start: 10px;\n  --padding-end: 10px ;\n}\n\n.performance_text {\n  text-align: justify;\n  line-height: 1.1rem;\n  font-size: 13px;\n  font-weight: 100;\n}\n\n.main_container {\n  padding-left: 10px;\n  padding-top: 15px;\n  padding-right: 10px;\n}\n\n.records-velue {\n  position: relative;\n  border-radius: 8px;\n  font-size: 14px;\n  box-shadow: rgba(0, 0, 0, 0.12) 0px 4px 16px;\n  padding: 10px;\n}\n\n.segmentOne-button {\n  --ripple-color: transparent;\n  background: transparent;\n  --color-checked: white;\n  --color: #9c9c9c;\n  --indicator-color: var(--ion-color-primary);\n  --margin-bottom: 0px;\n  height: 40px;\n  --border-radius: 25px;\n}\n\n.segmentOne {\n  background: rgba(170, 170, 170, 0.11);\n  border-radius: 25px;\n  margin-bottom: 15px;\n}\n\nion-item ion-label {\n  white-space: normal;\n}\n\nion-item {\n  --inner-border-width: 0px 0px 0px 0px;\n  --min-height: 35px;\n  --padding-start: 0px;\n  --border-width: 0 0 0px 0;\n}\n\nion-list {\n  padding-top: 0px !important;\n  padding-bottom: 0px !important;\n}\n\nion-avatar {\n  --border-radius: 0%;\n}\n\nion-label p {\n  color: #000;\n}\n\n.p-t-15 {\n  padding-top: 15px;\n}\n\n.add {\n  font-size: 25px;\n  padding-right: 10px;\n  padding-top: 15px;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvY29tcG9uZW50cy9yZWNvcmRzLXByb2ZpbGUvcmVjb3Jkcy1wcm9maWxlLmNvbXBvbmVudC5zY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBO0VBQ0kscUJBQUE7RUFDQSxvQkFBQTtBQUNKOztBQUVBO0VBQ0ksbUJBQUE7RUFDQSxtQkFBQTtFQUNBLGVBQUE7RUFDQSxnQkFBQTtBQUNKOztBQUdBO0VBQ0ksa0JBQUE7RUFDQSxpQkFBQTtFQUNBLG1CQUFBO0FBQUo7O0FBR0E7RUFDSSxrQkFBQTtFQUNBLGtCQUFBO0VBQ0EsZUFBQTtFQUNBLDRDQUFBO0VBQ0EsYUFBQTtBQUFKOztBQUdBO0VBQ0ksMkJBQUE7RUFDQSx1QkFBQTtFQUNBLHNCQUFBO0VBQ0EsZ0JBQUE7RUFDQSwyQ0FBQTtFQUNBLG9CQUFBO0VBQ0EsWUFBQTtFQUNBLHFCQUFBO0FBQUo7O0FBR0E7RUFDSSxxQ0FBQTtFQUNBLG1CQUFBO0VBQ0EsbUJBQUE7QUFBSjs7QUFHQTtFQUNJLG1CQUFBO0FBQUo7O0FBR0E7RUFDSSxxQ0FBQTtFQUNBLGtCQUFBO0VBQ0Esb0JBQUE7RUFDQSx5QkFBQTtBQUFKOztBQUdBO0VBQ0ksMkJBQUE7RUFDQSw4QkFBQTtBQUFKOztBQUdBO0VBQ0ksbUJBQUE7QUFBSjs7QUFFQTtFQUNJLFdBQUE7QUFDSjs7QUFDQTtFQUNJLGlCQUFBO0FBRUo7O0FBQUE7RUFDSSxlQUFBO0VBQ0EsbUJBQUE7RUFDQSxpQkFBQTtBQUdKIiwiZmlsZSI6InNyYy9hcHAvY29tcG9uZW50cy9yZWNvcmRzLXByb2ZpbGUvcmVjb3Jkcy1wcm9maWxlLmNvbXBvbmVudC5zY3NzIiwic291cmNlc0NvbnRlbnQiOlsiaW9uLWdyaWQge1xuICAgIC0tcGFkZGluZy1zdGFydDogMTBweDtcbiAgICAtLXBhZGRpbmctZW5kOiAxMHB4XG59XG5cbi5wZXJmb3JtYW5jZV90ZXh0IHtcbiAgICB0ZXh0LWFsaWduOiBqdXN0aWZ5O1xuICAgIGxpbmUtaGVpZ2h0OiAxLjFyZW07XG4gICAgZm9udC1zaXplOiAxM3B4O1xuICAgIGZvbnQtd2VpZ2h0OiAxMDA7XG5cbn1cblxuLm1haW5fY29udGFpbmVyIHtcbiAgICBwYWRkaW5nLWxlZnQ6IDEwcHg7XG4gICAgcGFkZGluZy10b3A6IDE1cHg7XG4gICAgcGFkZGluZy1yaWdodDogMTBweDtcbn1cblxuLnJlY29yZHMtdmVsdWUge1xuICAgIHBvc2l0aW9uOiByZWxhdGl2ZTtcbiAgICBib3JkZXItcmFkaXVzOiA4cHg7XG4gICAgZm9udC1zaXplOiAxNHB4O1xuICAgIGJveC1zaGFkb3c6IHJnYigwIDAgMCAvIDEyJSkgMHB4IDRweCAxNnB4O1xuICAgIHBhZGRpbmc6IDEwcHg7XG59XG5cbi5zZWdtZW50T25lLWJ1dHRvbiB7XG4gICAgLS1yaXBwbGUtY29sb3I6IHRyYW5zcGFyZW50O1xuICAgIGJhY2tncm91bmQ6IHRyYW5zcGFyZW50O1xuICAgIC0tY29sb3ItY2hlY2tlZDogd2hpdGU7XG4gICAgLS1jb2xvcjogIzljOWM5YztcbiAgICAtLWluZGljYXRvci1jb2xvcjogdmFyKC0taW9uLWNvbG9yLXByaW1hcnkpO1xuICAgIC0tbWFyZ2luLWJvdHRvbTogMHB4O1xuICAgIGhlaWdodDogNDBweDtcbiAgICAtLWJvcmRlci1yYWRpdXM6IDI1cHg7XG59XG5cbi5zZWdtZW50T25lIHtcbiAgICBiYWNrZ3JvdW5kOiByZ2IoMTcwIDE3MCAxNzAgLyAxMSUpO1xuICAgIGJvcmRlci1yYWRpdXM6IDI1cHg7XG4gICAgbWFyZ2luLWJvdHRvbTogMTVweDtcbn1cblxuaW9uLWl0ZW0gaW9uLWxhYmVsIHtcbiAgICB3aGl0ZS1zcGFjZTogbm9ybWFsO1xufVxuXG5pb24taXRlbSB7XG4gICAgLS1pbm5lci1ib3JkZXItd2lkdGg6IDBweCAwcHggMHB4IDBweDtcbiAgICAtLW1pbi1oZWlnaHQ6IDM1cHg7XG4gICAgLS1wYWRkaW5nLXN0YXJ0OiAwcHg7XG4gICAgLS1ib3JkZXItd2lkdGg6IDAgMCAwcHggMDtcbn1cblxuaW9uLWxpc3Qge1xuICAgIHBhZGRpbmctdG9wOiAwcHggIWltcG9ydGFudDtcbiAgICBwYWRkaW5nLWJvdHRvbTogMHB4ICFpbXBvcnRhbnQ7XG59XG5cbmlvbi1hdmF0YXIge1xuICAgIC0tYm9yZGVyLXJhZGl1czogMCU7XG59XG5pb24tbGFiZWwgcHtcbiAgICBjb2xvcjogIzAwMDtcbn1cbi5wLXQtMTUge1xuICAgIHBhZGRpbmctdG9wOiAxNXB4O1xufVxuLmFkZCB7XG4gICAgZm9udC1zaXplOiAyNXB4O1xuICAgIHBhZGRpbmctcmlnaHQ6IDEwcHg7XG4gICAgcGFkZGluZy10b3A6IDE1cHg7XG59Il19 */";
      /***/
    },

    /***/
    "./src/app/components/records-profile/records-profile.component.ts":
    /*!*************************************************************************!*\
      !*** ./src/app/components/records-profile/records-profile.component.ts ***!
      \*************************************************************************/

    /*! exports provided: RecordsProfileComponent */

    /***/
    function srcAppComponentsRecordsProfileRecordsProfileComponentTs(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "RecordsProfileComponent", function () {
        return RecordsProfileComponent;
      });
      /* harmony import */


      var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! tslib */
      "./node_modules/tslib/tslib.es6.js");
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! @angular/core */
      "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");

      var RecordsProfileComponent = /*#__PURE__*/function () {
        function RecordsProfileComponent() {
          _classCallCheck(this, RecordsProfileComponent);

          this.segmentRMModel = "1rm";
        }

        _createClass(RecordsProfileComponent, [{
          key: "ngOnInit",
          value: function ngOnInit() {}
        }, {
          key: "segmentChanged",
          value: function segmentChanged(event) {
            this.segmentRMModel = event.detail.value;
            console.log("segmentRMModel", event);
          }
        }]);

        return RecordsProfileComponent;
      }();

      RecordsProfileComponent.ctorParameters = function () {
        return [];
      };

      RecordsProfileComponent = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-records-profile',
        template: Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(
        /*! raw-loader!./records-profile.component.html */
        "./node_modules/raw-loader/dist/cjs.js!./src/app/components/records-profile/records-profile.component.html"))["default"],
        styles: [Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(
        /*! ./records-profile.component.scss */
        "./src/app/components/records-profile/records-profile.component.scss"))["default"]]
      })], RecordsProfileComponent);
      /***/
    },

    /***/
    "./src/app/components/shared.module.ts":
    /*!*********************************************!*\
      !*** ./src/app/components/shared.module.ts ***!
      \*********************************************/

    /*! exports provided: SharedModule */

    /***/
    function srcAppComponentsSharedModuleTs(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "SharedModule", function () {
        return SharedModule;
      });
      /* harmony import */


      var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! tslib */
      "./node_modules/tslib/tslib.es6.js");
      /* harmony import */


      var _modals_payment_modal_payment_modal_component__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! ./modals/payment-modal/payment-modal.component */
      "./src/app/components/modals/payment-modal/payment-modal.component.ts");
      /* harmony import */


      var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! @angular/router */
      "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");
      /* harmony import */


      var _check_ups_profile_check_ups_profile_component__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
      /*! ./check-ups-profile/check-ups-profile.component */
      "./src/app/components/check-ups-profile/check-ups-profile.component.ts");
      /* harmony import */


      var _physiology_profile_physiology_profile_component__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
      /*! ./physiology-profile/physiology-profile.component */
      "./src/app/components/physiology-profile/physiology-profile.component.ts");
      /* harmony import */


      var _chart_bar_chart_bar_component__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(
      /*! ./chart-bar/chart-bar.component */
      "./src/app/components/chart-bar/chart-bar.component.ts");
      /* harmony import */


      var _body_scan_step_five_body_scan_step_five_component__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(
      /*! ./body-scan-step-five/body-scan-step-five.component */
      "./src/app/components/body-scan-step-five/body-scan-step-five.component.ts");
      /* harmony import */


      var _body_scan_step_four_body_scan_step_four_component__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(
      /*! ./body-scan-step-four/body-scan-step-four.component */
      "./src/app/components/body-scan-step-four/body-scan-step-four.component.ts");
      /* harmony import */


      var _body_scan_step_three_body_scan_step_three_component__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(
      /*! ./body-scan-step-three/body-scan-step-three.component */
      "./src/app/components/body-scan-step-three/body-scan-step-three.component.ts");
      /* harmony import */


      var _body_scan_step_two_body_scan_step_two_component__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(
      /*! ./body-scan-step-two/body-scan-step-two.component */
      "./src/app/components/body-scan-step-two/body-scan-step-two.component.ts");
      /* harmony import */


      var _body_scan_step_one_body_scan_step_one_component__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(
      /*! ./body-scan-step-one/body-scan-step-one.component */
      "./src/app/components/body-scan-step-one/body-scan-step-one.component.ts");
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(
      /*! @angular/core */
      "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
      /* harmony import */


      var _angular_common__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(
      /*! @angular/common */
      "./node_modules/@angular/common/__ivy_ngcc__/fesm2015/common.js");
      /* harmony import */


      var _header_header_component__WEBPACK_IMPORTED_MODULE_13__ = __webpack_require__(
      /*! ./header/header.component */
      "./src/app/components/header/header.component.ts");
      /* harmony import */


      var _ngx_translate_core__WEBPACK_IMPORTED_MODULE_14__ = __webpack_require__(
      /*! @ngx-translate/core */
      "./node_modules/@ngx-translate/core/__ivy_ngcc__/fesm2015/ngx-translate-core.js");
      /* harmony import */


      var _performance_profile_performance_profile_component__WEBPACK_IMPORTED_MODULE_15__ = __webpack_require__(
      /*! ./performance-profile/performance-profile.component */
      "./src/app/components/performance-profile/performance-profile.component.ts");
      /* harmony import */


      var _records_profile_records_profile_component__WEBPACK_IMPORTED_MODULE_16__ = __webpack_require__(
      /*! ./records-profile/records-profile.component */
      "./src/app/components/records-profile/records-profile.component.ts");
      /* harmony import */


      var _chart_chart_component__WEBPACK_IMPORTED_MODULE_17__ = __webpack_require__(
      /*! ./chart/chart.component */
      "./src/app/components/chart/chart.component.ts");
      /* harmony import */


      var _charts_checkup_charts_checkup_component__WEBPACK_IMPORTED_MODULE_18__ = __webpack_require__(
      /*! ./charts-checkup/charts-checkup.component */
      "./src/app/components/charts-checkup/charts-checkup.component.ts");
      /* harmony import */


      var _book_book_component__WEBPACK_IMPORTED_MODULE_19__ = __webpack_require__(
      /*! ./book/book.component */
      "./src/app/components/book/book.component.ts");
      /* harmony import */


      var _modals_check_up_modals_check_up_modals_component__WEBPACK_IMPORTED_MODULE_20__ = __webpack_require__(
      /*! ./modals/check-up-modals/check-up-modals.component */
      "./src/app/components/modals/check-up-modals/check-up-modals.component.ts");
      /* harmony import */


      var _modals_courses_modal_courses_modal_component__WEBPACK_IMPORTED_MODULE_21__ = __webpack_require__(
      /*! ./modals/courses-modal/courses-modal.component */
      "./src/app/components/modals/courses-modal/courses-modal.component.ts");
      /* harmony import */


      var _modals_my_diet_modal_my_diet_modal_component__WEBPACK_IMPORTED_MODULE_22__ = __webpack_require__(
      /*! ./modals/my-diet-modal/my-diet-modal.component */
      "./src/app/components/modals/my-diet-modal/my-diet-modal.component.ts");
      /* harmony import */


      var _modals_manual_registration_manual_registration_component__WEBPACK_IMPORTED_MODULE_23__ = __webpack_require__(
      /*! ./modals/manual-registration/manual-registration.component */
      "./src/app/components/modals/manual-registration/manual-registration.component.ts");
      /* harmony import */


      var _modals_calendar_modal_calendar_modal__WEBPACK_IMPORTED_MODULE_24__ = __webpack_require__(
      /*! ./modals/calendar-modal/calendar-modal */
      "./src/app/components/modals/calendar-modal/calendar-modal.ts");
      /* harmony import */


      var ionic2_calendar__WEBPACK_IMPORTED_MODULE_25__ = __webpack_require__(
      /*! ionic2-calendar */
      "./node_modules/ionic2-calendar/__ivy_ngcc__/fesm2015/ionic2-calendar.js");
      /* harmony import */


      var ion2_calendar__WEBPACK_IMPORTED_MODULE_26__ = __webpack_require__(
      /*! ion2-calendar */
      "./node_modules/ion2-calendar/__ivy_ngcc__/dist/index.js");
      /* harmony import */


      var ion2_calendar__WEBPACK_IMPORTED_MODULE_26___default = /*#__PURE__*/__webpack_require__.n(ion2_calendar__WEBPACK_IMPORTED_MODULE_26__);
      /* harmony import */


      var _bodysvgfront_bodysvg_component__WEBPACK_IMPORTED_MODULE_27__ = __webpack_require__(
      /*! ./bodysvgfront/bodysvg.component */
      "./src/app/components/bodysvgfront/bodysvg.component.ts");
      /* harmony import */


      var _bodysvgback_bodysvgback_component__WEBPACK_IMPORTED_MODULE_28__ = __webpack_require__(
      /*! ./bodysvgback/bodysvgback.component */
      "./src/app/components/bodysvgback/bodysvgback.component.ts");
      /* harmony import */


      var _modals_notebook_modal_notebook_modal_component__WEBPACK_IMPORTED_MODULE_29__ = __webpack_require__(
      /*! ./modals/notebook-modal/notebook-modal.component */
      "./src/app/components/modals/notebook-modal/notebook-modal.component.ts");
      /* harmony import */


      var _modals_my_devices_modal_my_devices_modal_component__WEBPACK_IMPORTED_MODULE_30__ = __webpack_require__(
      /*! ./modals/my-devices-modal/my-devices-modal.component */
      "./src/app/components/modals/my-devices-modal/my-devices-modal.component.ts");
      /* harmony import */


      var _modals_sponsership_modal_sponsership_modal_component__WEBPACK_IMPORTED_MODULE_31__ = __webpack_require__(
      /*! ./modals/sponsership-modal/sponsership-modal.component */
      "./src/app/components/modals/sponsership-modal/sponsership-modal.component.ts");
      /* harmony import */


      var _swiper_card_swiper_card_component__WEBPACK_IMPORTED_MODULE_32__ = __webpack_require__(
      /*! ./swiper-card/swiper-card.component */
      "./src/app/components/swiper-card/swiper-card.component.ts");
      /* harmony import */


      var hammerjs__WEBPACK_IMPORTED_MODULE_33__ = __webpack_require__(
      /*! hammerjs */
      "./node_modules/hammerjs/hammer.js");
      /* harmony import */


      var hammerjs__WEBPACK_IMPORTED_MODULE_33___default = /*#__PURE__*/__webpack_require__.n(hammerjs__WEBPACK_IMPORTED_MODULE_33__);
      /* harmony import */


      var _modals_my_diet_home_modal_my_diet_home_modal_component__WEBPACK_IMPORTED_MODULE_34__ = __webpack_require__(
      /*! ./modals/my-diet-home-modal/my-diet-home-modal.component */
      "./src/app/components/modals/my-diet-home-modal/my-diet-home-modal.component.ts");
      /* harmony import */


      var _coach_payment_visibility_coach_payment_visibility_component__WEBPACK_IMPORTED_MODULE_35__ = __webpack_require__(
      /*! ./coach-payment-visibility/coach-payment-visibility.component */
      "./src/app/components/coach-payment-visibility/coach-payment-visibility.component.ts");
      /* harmony import */


      var _components_modals_coach_chat_modal_coach_chat_modal_component__WEBPACK_IMPORTED_MODULE_36__ = __webpack_require__(
      /*! @components/modals/coach-chat-modal/coach-chat-modal.component */
      "./src/app/components/modals/coach-chat-modal/coach-chat-modal.component.ts");
      /* harmony import */


      var _components_modals_coach_checkup_modal_coach_checkup_modal_component__WEBPACK_IMPORTED_MODULE_37__ = __webpack_require__(
      /*! @components/modals/coach-checkup-modal/coach-checkup-modal.component */
      "./src/app/components/modals/coach-checkup-modal/coach-checkup-modal.component.ts");
      /* harmony import */


      var _components_modals_coach_new_request_coach_new_request_component__WEBPACK_IMPORTED_MODULE_38__ = __webpack_require__(
      /*! @components/modals/coach-new-request/coach-new-request.component */
      "./src/app/components/modals/coach-new-request/coach-new-request.component.ts");
      /* harmony import */


      var _coach_coach_body_scan_step_one_coach_body_scan_step_one_component__WEBPACK_IMPORTED_MODULE_39__ = __webpack_require__(
      /*! ./coach/coach-body-scan-step-one/coach-body-scan-step-one.component */
      "./src/app/components/coach/coach-body-scan-step-one/coach-body-scan-step-one.component.ts");
      /* harmony import */


      var _coach_coach_body_scan_step_two_coach_body_scan_step_two_component__WEBPACK_IMPORTED_MODULE_40__ = __webpack_require__(
      /*! ./coach/coach-body-scan-step-two/coach-body-scan-step-two.component */
      "./src/app/components/coach/coach-body-scan-step-two/coach-body-scan-step-two.component.ts");
      /* harmony import */


      var _coach_coach_body_scan_step_three_coach_body_scan_step_three_component__WEBPACK_IMPORTED_MODULE_41__ = __webpack_require__(
      /*! ./coach/coach-body-scan-step-three/coach-body-scan-step-three.component */
      "./src/app/components/coach/coach-body-scan-step-three/coach-body-scan-step-three.component.ts");
      /* harmony import */


      var _modals_recipes_filters_recipes_filters_component__WEBPACK_IMPORTED_MODULE_42__ = __webpack_require__(
      /*! ./modals/recipes-filters/recipes-filters.component */
      "./src/app/components/modals/recipes-filters/recipes-filters.component.ts");
      /* harmony import */


      var _musculation_auto_seances_musculation_auto_seances_component__WEBPACK_IMPORTED_MODULE_43__ = __webpack_require__(
      /*! ./musculation-auto-seances/musculation-auto-seances.component */
      "./src/app/components/musculation-auto-seances/musculation-auto-seances.component.ts");
      /* harmony import */


      var _musculation_auto_seances_session_musculation_auto_seances_session_component__WEBPACK_IMPORTED_MODULE_44__ = __webpack_require__(
      /*! ./musculation-auto-seances-session/musculation-auto-seances-session.component */
      "./src/app/components/musculation-auto-seances-session/musculation-auto-seances-session.component.ts");
      /* harmony import */


      var _coach_chart_one_chart_one_component__WEBPACK_IMPORTED_MODULE_45__ = __webpack_require__(
      /*! ./coach/chart-one/chart-one.component */
      "./src/app/components/coach/chart-one/chart-one.component.ts");
      /* harmony import */


      var _coach_chart_two_chart_two_component__WEBPACK_IMPORTED_MODULE_46__ = __webpack_require__(
      /*! ./coach/chart-two/chart-two.component */
      "./src/app/components/coach/chart-two/chart-two.component.ts");
      /* harmony import */


      var _modals_add_excercise_wood_modal_add_excercise_wood_modal_component__WEBPACK_IMPORTED_MODULE_47__ = __webpack_require__(
      /*! ./modals/add-excercise-wood-modal/add-excercise-wood-modal.component */
      "./src/app/components/modals/add-excercise-wood-modal/add-excercise-wood-modal.component.ts");
      /* harmony import */


      var _modals_click_add_excercise_modal_click_add_excercise_modal_component__WEBPACK_IMPORTED_MODULE_48__ = __webpack_require__(
      /*! ./modals/click-add-excercise-modal/click-add-excercise-modal.component */
      "./src/app/components/modals/click-add-excercise-modal/click-add-excercise-modal.component.ts");
      /* harmony import */


      var _modals_before_aliments_before_aliments_component__WEBPACK_IMPORTED_MODULE_49__ = __webpack_require__(
      /*! ./modals/before-aliments/before-aliments.component */
      "./src/app/components/modals/before-aliments/before-aliments.component.ts");

      var SharedModule = function SharedModule() {
        _classCallCheck(this, SharedModule);
      };

      SharedModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([Object(_angular_core__WEBPACK_IMPORTED_MODULE_11__["NgModule"])({
        schemas: [_angular_core__WEBPACK_IMPORTED_MODULE_11__["CUSTOM_ELEMENTS_SCHEMA"]],
        declarations: [_header_header_component__WEBPACK_IMPORTED_MODULE_13__["HeaderComponent"], _body_scan_step_one_body_scan_step_one_component__WEBPACK_IMPORTED_MODULE_10__["BodyScanStepOneComponent"], _body_scan_step_two_body_scan_step_two_component__WEBPACK_IMPORTED_MODULE_9__["BodyScanStepTwoComponent"], _body_scan_step_three_body_scan_step_three_component__WEBPACK_IMPORTED_MODULE_8__["BodyScanStepThreeComponent"], _body_scan_step_four_body_scan_step_four_component__WEBPACK_IMPORTED_MODULE_7__["BodyScanStepFourComponent"], _body_scan_step_five_body_scan_step_five_component__WEBPACK_IMPORTED_MODULE_6__["BodyScanStepFiveComponent"], _chart_bar_chart_bar_component__WEBPACK_IMPORTED_MODULE_5__["ChartBarComponent"], _performance_profile_performance_profile_component__WEBPACK_IMPORTED_MODULE_15__["PerformanceProfileComponent"], _physiology_profile_physiology_profile_component__WEBPACK_IMPORTED_MODULE_4__["PhysiologyProfileComponent"], _records_profile_records_profile_component__WEBPACK_IMPORTED_MODULE_16__["RecordsProfileComponent"], _check_ups_profile_check_ups_profile_component__WEBPACK_IMPORTED_MODULE_3__["CheckUpsProfileComponent"], _chart_chart_component__WEBPACK_IMPORTED_MODULE_17__["ChartComponent"], _charts_checkup_charts_checkup_component__WEBPACK_IMPORTED_MODULE_18__["ChartsCheckupComponent"], _book_book_component__WEBPACK_IMPORTED_MODULE_19__["BookComponent"], _modals_calendar_modal_calendar_modal__WEBPACK_IMPORTED_MODULE_24__["CalendarModal"], _modals_check_up_modals_check_up_modals_component__WEBPACK_IMPORTED_MODULE_20__["CheckUpModalsComponent"], _modals_courses_modal_courses_modal_component__WEBPACK_IMPORTED_MODULE_21__["CoursesModalComponent"], _modals_my_diet_modal_my_diet_modal_component__WEBPACK_IMPORTED_MODULE_22__["MyDietModalComponent"], _modals_manual_registration_manual_registration_component__WEBPACK_IMPORTED_MODULE_23__["ManualRegistrationComponent"], _bodysvgfront_bodysvg_component__WEBPACK_IMPORTED_MODULE_27__["BodysvgComponent"], _bodysvgback_bodysvgback_component__WEBPACK_IMPORTED_MODULE_28__["BodysvgbackComponent"], _modals_notebook_modal_notebook_modal_component__WEBPACK_IMPORTED_MODULE_29__["NotebookModalComponent"], _modals_my_devices_modal_my_devices_modal_component__WEBPACK_IMPORTED_MODULE_30__["MyDevicesModalComponent"], _modals_sponsership_modal_sponsership_modal_component__WEBPACK_IMPORTED_MODULE_31__["SponsershipModalComponent"], _swiper_card_swiper_card_component__WEBPACK_IMPORTED_MODULE_32__["SwiperCardComponent"], _modals_my_diet_home_modal_my_diet_home_modal_component__WEBPACK_IMPORTED_MODULE_34__["MyDietHomeModalComponent"], _modals_my_diet_home_modal_my_diet_home_modal_component__WEBPACK_IMPORTED_MODULE_34__["MyDietHomeModalComponent"], _modals_payment_modal_payment_modal_component__WEBPACK_IMPORTED_MODULE_1__["PaymentModalComponent"], _coach_payment_visibility_coach_payment_visibility_component__WEBPACK_IMPORTED_MODULE_35__["CoachPaymentVisibilityComponent"], _components_modals_coach_chat_modal_coach_chat_modal_component__WEBPACK_IMPORTED_MODULE_36__["CoachChatModalComponent"], _components_modals_coach_checkup_modal_coach_checkup_modal_component__WEBPACK_IMPORTED_MODULE_37__["CoachCheckupModalComponent"], _components_modals_coach_new_request_coach_new_request_component__WEBPACK_IMPORTED_MODULE_38__["CoachNewRequestComponent"], _coach_coach_body_scan_step_one_coach_body_scan_step_one_component__WEBPACK_IMPORTED_MODULE_39__["CoachBodyScanStepOneComponent"], _coach_coach_body_scan_step_two_coach_body_scan_step_two_component__WEBPACK_IMPORTED_MODULE_40__["CoachBodyScanStepTwoComponent"], _coach_coach_body_scan_step_three_coach_body_scan_step_three_component__WEBPACK_IMPORTED_MODULE_41__["CoachBodyScanStepThreeComponent"], _modals_recipes_filters_recipes_filters_component__WEBPACK_IMPORTED_MODULE_42__["RecipesFiltersComponent"], _musculation_auto_seances_musculation_auto_seances_component__WEBPACK_IMPORTED_MODULE_43__["MusculationAutoSeancesComponent"], _musculation_auto_seances_session_musculation_auto_seances_session_component__WEBPACK_IMPORTED_MODULE_44__["MusculationAutoSeancesSessionComponent"], _coach_chart_one_chart_one_component__WEBPACK_IMPORTED_MODULE_45__["ChartOneComponent"], _coach_chart_two_chart_two_component__WEBPACK_IMPORTED_MODULE_46__["ChartTwoComponent"], _modals_add_excercise_wood_modal_add_excercise_wood_modal_component__WEBPACK_IMPORTED_MODULE_47__["AddExcerciseWoodModalComponent"], _modals_click_add_excercise_modal_click_add_excercise_modal_component__WEBPACK_IMPORTED_MODULE_48__["ClickAddExcerciseModalComponent"], _modals_before_aliments_before_aliments_component__WEBPACK_IMPORTED_MODULE_49__["BeforeAlimentsComponent"]],
        imports: [_angular_common__WEBPACK_IMPORTED_MODULE_12__["CommonModule"], _ngx_translate_core__WEBPACK_IMPORTED_MODULE_14__["TranslateModule"], ionic2_calendar__WEBPACK_IMPORTED_MODULE_25__["NgCalendarModule"], ion2_calendar__WEBPACK_IMPORTED_MODULE_26__["CalendarModule"], _angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]],
        exports: [_header_header_component__WEBPACK_IMPORTED_MODULE_13__["HeaderComponent"], _body_scan_step_one_body_scan_step_one_component__WEBPACK_IMPORTED_MODULE_10__["BodyScanStepOneComponent"], _body_scan_step_two_body_scan_step_two_component__WEBPACK_IMPORTED_MODULE_9__["BodyScanStepTwoComponent"], _body_scan_step_three_body_scan_step_three_component__WEBPACK_IMPORTED_MODULE_8__["BodyScanStepThreeComponent"], _body_scan_step_four_body_scan_step_four_component__WEBPACK_IMPORTED_MODULE_7__["BodyScanStepFourComponent"], _body_scan_step_five_body_scan_step_five_component__WEBPACK_IMPORTED_MODULE_6__["BodyScanStepFiveComponent"], _chart_bar_chart_bar_component__WEBPACK_IMPORTED_MODULE_5__["ChartBarComponent"], _performance_profile_performance_profile_component__WEBPACK_IMPORTED_MODULE_15__["PerformanceProfileComponent"], _physiology_profile_physiology_profile_component__WEBPACK_IMPORTED_MODULE_4__["PhysiologyProfileComponent"], _records_profile_records_profile_component__WEBPACK_IMPORTED_MODULE_16__["RecordsProfileComponent"], _check_ups_profile_check_ups_profile_component__WEBPACK_IMPORTED_MODULE_3__["CheckUpsProfileComponent"], _chart_chart_component__WEBPACK_IMPORTED_MODULE_17__["ChartComponent"], _charts_checkup_charts_checkup_component__WEBPACK_IMPORTED_MODULE_18__["ChartsCheckupComponent"], _book_book_component__WEBPACK_IMPORTED_MODULE_19__["BookComponent"], _modals_check_up_modals_check_up_modals_component__WEBPACK_IMPORTED_MODULE_20__["CheckUpModalsComponent"], _modals_courses_modal_courses_modal_component__WEBPACK_IMPORTED_MODULE_21__["CoursesModalComponent"], _modals_my_diet_modal_my_diet_modal_component__WEBPACK_IMPORTED_MODULE_22__["MyDietModalComponent"], _modals_manual_registration_manual_registration_component__WEBPACK_IMPORTED_MODULE_23__["ManualRegistrationComponent"], _modals_calendar_modal_calendar_modal__WEBPACK_IMPORTED_MODULE_24__["CalendarModal"], _bodysvgfront_bodysvg_component__WEBPACK_IMPORTED_MODULE_27__["BodysvgComponent"], _bodysvgback_bodysvgback_component__WEBPACK_IMPORTED_MODULE_28__["BodysvgbackComponent"], _modals_notebook_modal_notebook_modal_component__WEBPACK_IMPORTED_MODULE_29__["NotebookModalComponent"], _modals_my_devices_modal_my_devices_modal_component__WEBPACK_IMPORTED_MODULE_30__["MyDevicesModalComponent"], _modals_sponsership_modal_sponsership_modal_component__WEBPACK_IMPORTED_MODULE_31__["SponsershipModalComponent"], _swiper_card_swiper_card_component__WEBPACK_IMPORTED_MODULE_32__["SwiperCardComponent"], _modals_my_diet_home_modal_my_diet_home_modal_component__WEBPACK_IMPORTED_MODULE_34__["MyDietHomeModalComponent"], _modals_payment_modal_payment_modal_component__WEBPACK_IMPORTED_MODULE_1__["PaymentModalComponent"], _coach_payment_visibility_coach_payment_visibility_component__WEBPACK_IMPORTED_MODULE_35__["CoachPaymentVisibilityComponent"], _components_modals_coach_chat_modal_coach_chat_modal_component__WEBPACK_IMPORTED_MODULE_36__["CoachChatModalComponent"], _components_modals_coach_checkup_modal_coach_checkup_modal_component__WEBPACK_IMPORTED_MODULE_37__["CoachCheckupModalComponent"], _components_modals_coach_new_request_coach_new_request_component__WEBPACK_IMPORTED_MODULE_38__["CoachNewRequestComponent"], _modals_payment_modal_payment_modal_component__WEBPACK_IMPORTED_MODULE_1__["PaymentModalComponent"], _coach_coach_body_scan_step_one_coach_body_scan_step_one_component__WEBPACK_IMPORTED_MODULE_39__["CoachBodyScanStepOneComponent"], _coach_coach_body_scan_step_two_coach_body_scan_step_two_component__WEBPACK_IMPORTED_MODULE_40__["CoachBodyScanStepTwoComponent"], _coach_coach_body_scan_step_three_coach_body_scan_step_three_component__WEBPACK_IMPORTED_MODULE_41__["CoachBodyScanStepThreeComponent"], _modals_payment_modal_payment_modal_component__WEBPACK_IMPORTED_MODULE_1__["PaymentModalComponent"], _modals_recipes_filters_recipes_filters_component__WEBPACK_IMPORTED_MODULE_42__["RecipesFiltersComponent"], _musculation_auto_seances_musculation_auto_seances_component__WEBPACK_IMPORTED_MODULE_43__["MusculationAutoSeancesComponent"], _musculation_auto_seances_session_musculation_auto_seances_session_component__WEBPACK_IMPORTED_MODULE_44__["MusculationAutoSeancesSessionComponent"], _coach_chart_one_chart_one_component__WEBPACK_IMPORTED_MODULE_45__["ChartOneComponent"], _coach_chart_two_chart_two_component__WEBPACK_IMPORTED_MODULE_46__["ChartTwoComponent"], _modals_add_excercise_wood_modal_add_excercise_wood_modal_component__WEBPACK_IMPORTED_MODULE_47__["AddExcerciseWoodModalComponent"], _modals_click_add_excercise_modal_click_add_excercise_modal_component__WEBPACK_IMPORTED_MODULE_48__["ClickAddExcerciseModalComponent"], _modals_before_aliments_before_aliments_component__WEBPACK_IMPORTED_MODULE_49__["BeforeAlimentsComponent"]],
        providers: [_body_scan_step_one_body_scan_step_one_component__WEBPACK_IMPORTED_MODULE_10__["BodyScanStepOneComponent"]]
      })], SharedModule);
      /***/
    },

    /***/
    "./src/app/components/swiper-card/swiper-card.component.scss":
    /*!*******************************************************************!*\
      !*** ./src/app/components/swiper-card/swiper-card.component.scss ***!
      \*******************************************************************/

    /*! exports provided: default */

    /***/
    function srcAppComponentsSwiperCardSwiperCardComponentScss(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony default export */


      __webpack_exports__["default"] = ".swiper {\n  width: 100%;\n  height: 100%;\n  overflow: hidden;\n  position: absolute;\n  left: 0;\n  top: -40px;\n}\n\n.swiper--status {\n  position: absolute;\n  top: 50%;\n  margin-top: -30px;\n  z-index: 2;\n  width: 100%;\n  text-align: center;\n  pointer-events: none;\n}\n\n.swiper--cards {\n  text-align: center;\n  display: flex;\n  flex-direction: column;\n  position: fixed;\n  justify-content: center;\n  align-items: center;\n  width: 100%;\n  height: 100%;\n  overflow: hidden;\n}\n\n.swiper--card {\n  display: inline-block;\n  width: 80%;\n  overflow: hidden;\n  position: absolute;\n  will-change: transform;\n  transition: all 0.3s ease-in-out;\n  cursor: -webkit-grab;\n  cursor: grab;\n}\n\nion-card {\n  box-shadow: rgba(0, 0, 0, 0.12) 0px 4px 16px;\n}\n\nion-card-content {\n  text-align: left;\n}\n\nion-card-content h3 {\n  padding-top: 10px;\n  margin-bottom: 10px;\n  font-size: 16px !important;\n}\n\nion-card-content p {\n  color: #ccc;\n  padding-top: 10px;\n  margin-bottom: 10px;\n}\n\n.swiper--buttons .left {\n  float: left;\n  margin: 10px;\n  background: transparent !important;\n}\n\n.swiper--buttons .left ion-icon {\n  color: #f00;\n}\n\n.swiper--buttons .right {\n  float: right;\n  margin: 10px;\n  background: transparent !important;\n}\n\n.swiper--buttons .right ion-icon {\n  color: #f00;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvY29tcG9uZW50cy9zd2lwZXItY2FyZC9zd2lwZXItY2FyZC5jb21wb25lbnQuc2NzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQTtFQUNJLFdBQUE7RUFDQSxZQUFBO0VBQ0EsZ0JBQUE7RUFDQSxrQkFBQTtFQUNBLE9BQUE7RUFDQSxVQUFBO0FBQ0o7O0FBRUU7RUFDRSxrQkFBQTtFQUNBLFFBQUE7RUFDQSxpQkFBQTtFQUNBLFVBQUE7RUFDQSxXQUFBO0VBQ0Esa0JBQUE7RUFDQSxvQkFBQTtBQUNKOztBQUVFO0VBQ0Usa0JBQUE7RUFDQSxhQUFBO0VBQ0Esc0JBQUE7RUFDQSxlQUFBO0VBQ0EsdUJBQUE7RUFDQSxtQkFBQTtFQUNBLFdBQUE7RUFDQSxZQUFBO0VBQ0EsZ0JBQUE7QUFDSjs7QUFFRTtFQUNFLHFCQUFBO0VBQ0EsVUFBQTtFQU1BLGdCQUFBO0VBQ0Esa0JBQUE7RUFDQSxzQkFBQTtFQUNBLGdDQUFBO0VBQ0Esb0JBQUE7RUFFQSxZQUFBO0FBSko7O0FBZ0JFO0VBQ0UsNENBQUE7QUFiSjs7QUFlRTtFQUNJLGdCQUFBO0FBWk47O0FBYU07RUFDTSxpQkFBQTtFQUNBLG1CQUFBO0VBQ0EsMEJBQUE7QUFYWjs7QUFhTTtFQUNJLFdBQUE7RUFDQSxpQkFBQTtFQUNBLG1CQUFBO0FBWFY7O0FBY0U7RUFDRSxXQUFBO0VBQ0EsWUFBQTtFQUNBLGtDQUFBO0FBWEo7O0FBWUk7RUFDSSxXQUFBO0FBVlI7O0FBZ0JFO0VBQ0UsWUFBQTtFQUNBLFlBQUE7RUFDQSxrQ0FBQTtBQWJKOztBQWNJO0VBQ0ksV0FBQTtBQVpSIiwiZmlsZSI6InNyYy9hcHAvY29tcG9uZW50cy9zd2lwZXItY2FyZC9zd2lwZXItY2FyZC5jb21wb25lbnQuc2NzcyIsInNvdXJjZXNDb250ZW50IjpbIi5zd2lwZXIge1xuICAgIHdpZHRoOiAxMDAlO1xuICAgIGhlaWdodDogMTAwJTtcbiAgICBvdmVyZmxvdzogaGlkZGVuO1xuICAgIHBvc2l0aW9uOiBhYnNvbHV0ZTtcbiAgICBsZWZ0OiAwO1xuICAgIHRvcDogLTQwcHg7XG4gIH1cbiAgXG4gIC5zd2lwZXItLXN0YXR1cyB7XG4gICAgcG9zaXRpb246IGFic29sdXRlO1xuICAgIHRvcDogNTAlO1xuICAgIG1hcmdpbi10b3A6IC0zMHB4O1xuICAgIHotaW5kZXg6IDI7XG4gICAgd2lkdGg6IDEwMCU7XG4gICAgdGV4dC1hbGlnbjogY2VudGVyO1xuICAgIHBvaW50ZXItZXZlbnRzOiBub25lO1xuICB9XG4gIFxuICAuc3dpcGVyLS1jYXJkcyB7XG4gICAgdGV4dC1hbGlnbjogY2VudGVyO1xuICAgIGRpc3BsYXk6IGZsZXg7XG4gICAgZmxleC1kaXJlY3Rpb246IGNvbHVtbjtcbiAgICBwb3NpdGlvbjogZml4ZWQ7XG4gICAganVzdGlmeS1jb250ZW50OiBjZW50ZXI7XG4gICAgYWxpZ24taXRlbXM6IGNlbnRlcjtcbiAgICB3aWR0aDogMTAwJTtcbiAgICBoZWlnaHQ6IDEwMCU7XG4gICAgb3ZlcmZsb3c6IGhpZGRlbjtcbiAgfVxuICBcbiAgLnN3aXBlci0tY2FyZCB7XG4gICAgZGlzcGxheTogaW5saW5lLWJsb2NrO1xuICAgIHdpZHRoOiA4MCU7XG4gICAgLy8gaGVpZ2h0OiA2MCU7XG4gICAgLy8gYmFja2dyb3VuZDogI0ZGRkZGRjtcbiAgICAvLyBwYWRkaW5nLWJvdHRvbTogNDBweDtcbiAgICAvLyBib3JkZXItcmFkaXVzOiA4cHg7XG4gICBcbiAgICBvdmVyZmxvdzogaGlkZGVuO1xuICAgIHBvc2l0aW9uOiBhYnNvbHV0ZTtcbiAgICB3aWxsLWNoYW5nZTogdHJhbnNmb3JtO1xuICAgIHRyYW5zaXRpb246IGFsbCAwLjNzIGVhc2UtaW4tb3V0O1xuICAgIGN1cnNvcjogLXdlYmtpdC1ncmFiO1xuICAgIGN1cnNvcjogLW1vei1ncmFiO1xuICAgIGN1cnNvcjogZ3JhYjtcblxuICB9XG4gIFxuLy8gICAuc3dpcGVyLS1idXR0b25zIHtcbi8vICAgICBwb3NpdGlvbjogYWJzb2x1dGU7XG4vLyAgICAgZmxleDogMCAwIDEwMHB4O1xuLy8gICAgIHRleHQtYWxpZ246IGNlbnRlcjtcbi8vICAgICBib3R0b206IDIwcHg7XG4vLyAgICAgbGVmdDogMDtcbi8vICAgICByaWdodDogMDtcbi8vICAgfVxuICBpb24tY2FyZCB7XG4gICAgYm94LXNoYWRvdzogcmdiKDAgMCAwIC8gMTIlKSAwcHggNHB4IDE2cHg7XG4gIH1cbiAgaW9uLWNhcmQtY29udGVudCB7XG4gICAgICB0ZXh0LWFsaWduOiBsZWZ0O1xuICAgICAgaDMge1xuICAgICAgICAgICAgcGFkZGluZy10b3A6IDEwcHg7XG4gICAgICAgICAgICBtYXJnaW4tYm90dG9tOiAxMHB4O1xuICAgICAgICAgICAgZm9udC1zaXplOiAxNnB4ICAhaW1wb3J0YW50O1xuICAgICAgfVxuICAgICAgcHtcbiAgICAgICAgICBjb2xvcjogI2NjYztcbiAgICAgICAgICBwYWRkaW5nLXRvcDogMTBweDtcbiAgICAgICAgICBtYXJnaW4tYm90dG9tOiAxMHB4O1xuICAgICAgfVxuICB9XG4gIC5zd2lwZXItLWJ1dHRvbnMgLmxlZnQge1xuICAgIGZsb2F0OiBsZWZ0O1xuICAgIG1hcmdpbjogMTBweDtcbiAgICBiYWNrZ3JvdW5kOiB0cmFuc3BhcmVudCAhaW1wb3J0YW50O1xuICAgIGlvbi1pY29uIHtcbiAgICAgICAgY29sb3I6ICNmMDA7XG4gICAgfVxuICAgIFxuICBcbiAgfVxuXG4gIC5zd2lwZXItLWJ1dHRvbnMgLnJpZ2h0IHtcbiAgICBmbG9hdDogcmlnaHQ7XG4gICAgbWFyZ2luOiAxMHB4O1xuICAgIGJhY2tncm91bmQ6IHRyYW5zcGFyZW50ICFpbXBvcnRhbnQ7XG4gICAgaW9uLWljb24ge1xuICAgICAgICBjb2xvcjogI2YwMDtcbiAgICB9XG4gIFxufVxuICJdfQ== */";
      /***/
    },

    /***/
    "./src/app/components/swiper-card/swiper-card.component.ts":
    /*!*****************************************************************!*\
      !*** ./src/app/components/swiper-card/swiper-card.component.ts ***!
      \*****************************************************************/

    /*! exports provided: SwiperCardComponent */

    /***/
    function srcAppComponentsSwiperCardSwiperCardComponentTs(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "SwiperCardComponent", function () {
        return SwiperCardComponent;
      });
      /* harmony import */


      var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! tslib */
      "./node_modules/tslib/tslib.es6.js");
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! @angular/core */
      "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
      /* harmony import */


      var _ionic_angular__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! @ionic/angular */
      "./node_modules/@ionic/angular/__ivy_ngcc__/fesm2015/ionic-angular.js");
      /* harmony import */


      var hammerjs__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
      /*! hammerjs */
      "./node_modules/hammerjs/hammer.js");
      /* harmony import */


      var hammerjs__WEBPACK_IMPORTED_MODULE_3___default = /*#__PURE__*/__webpack_require__.n(hammerjs__WEBPACK_IMPORTED_MODULE_3__);

      var SwiperCardComponent = /*#__PURE__*/function () {
        function SwiperCardComponent(gestureCtrl, platform) {
          _classCallCheck(this, SwiperCardComponent);

          this.gestureCtrl = gestureCtrl;
          this.platform = platform;
          this.choiceMade = new _angular_core__WEBPACK_IMPORTED_MODULE_1__["EventEmitter"]();
          this.minutesMinRange = 0;
          this.minutesChangeRange = 60;
        }

        _createClass(SwiperCardComponent, [{
          key: "ngOnInit",
          value: function ngOnInit() {}
        }, {
          key: "minuteChange",
          value: function minuteChange(event) {
            this.minutesChangeRange = event.detail.value;
          }
        }, {
          key: "userClickedButton",
          value: function userClickedButton(event, heart) {
            event.preventDefault();
            if (!this.cards.length) return false;

            if (heart) {
              this.swiperCardsArray[0].nativeElement.style.transform = 'translate(' + this.moveOutWidth + 'px, -100px) rotate(-30deg)';
            } else {
              this.swiperCardsArray[0].nativeElement.style.transform = 'translate(-' + this.moveOutWidth + 'px, -100px) rotate(30deg)';
            }

            ;
            this.shiftRequired = true;
            this.transitionInProgress = true;
          }
        }, {
          key: "handleShift",
          value: function handleShift() {
            this.transitionInProgress = false;

            if (this.shiftRequired) {
              this.shiftRequired = false;
              this.cards.shift();
            }

            ;
          }
        }, {
          key: "ngAfterViewInit",
          value: function ngAfterViewInit() {
            var _this27 = this;

            var cardArray = this.swipercards.toArray();
            this.useSwiperGesture(cardArray);
            this.moveOutWidth = document.documentElement.clientWidth * 1.5;
            this.swiperCardsArray = this.swipercards.toArray();
            this.swipercards.changes.subscribe(function () {
              _this27.swiperCardsArray = _this27.swipercards.toArray();
            });
          }
        }, {
          key: "useSwiperGesture",
          value: function useSwiperGesture(cardArray) {
            var _this28 = this;

            var _loop = function _loop(i) {
              var card = cardArray[i]; // console.log("card", card);

              var gesture = _this28.gestureCtrl.create({
                el: card.nativeElement,
                threshold: 15,
                gestureName: "swipte",
                onStart: function onStart(ev) {},
                onMove: function onMove(ev) {
                  // console.log("ev : ", ev);
                  card.nativeElement.style.transform = "translateX(".concat(ev.deltaX, "px) rotate(").concat(ev.deltaX / 10, "deg)"); //TO SET COLOR ON SWIPE
                  // this.setCardColor(ev.deltaX, card.nativeElement);
                },
                onEnd: function onEnd(ev) {
                  card.nativeElement.style.transition = ".5s ease-out"; //Right side Move

                  if (ev.deltaX > 150) {
                    card.nativeElement.style.transform = "translateX(".concat(+_this28.platform.width() * 2, "px) rotate(").concat(ev.deltaX / 2, "deg)");
                  } // Left Side Move
                  else if (ev.deltaX < -150) {
                      card.nativeElement.style.transform = "translateX(-".concat(+_this28.platform.width() * 2, "px) rotate(").concat(ev.deltaX / 2, "deg)");
                    } // When No move or if small move back to original
                    else {
                        card.nativeElement.style.transform = "";
                      }

                  _this28.shiftRequired = true;
                  _this28.transitionInProgress = true;
                }
              });

              gesture.enable(true);
            };

            for (var i = 0; i < cardArray.length; i++) {
              _loop(i);
            }
          } // STYLE OF CARD WHEN GESTURE START

        }, {
          key: "setCardColor",
          value: function setCardColor(x, element) {
            var color = "";
            var abs = Math.abs(x);
            var min = Math.trunc(Math.min(16 * 16 - abs, 16 * 16));
            var hexCode = this.decimalToHex(min, 2);

            if (x < 0) {
              color = "#FF" + hexCode + "FF" + hexCode;
            } else {
              color = "#" + hexCode + "FF" + hexCode;
            }

            element.style.background = color;
          }
        }, {
          key: "decimalToHex",
          value: function decimalToHex(d, padding) {
            var hex = Number(d).toString(16);
            padding = typeof padding === "undefined" || padding === null ? padding = 2 : padding;

            while (hex.length < padding) {
              hex = "0" + hex;
            }

            return hex;
          }
        }]);

        return SwiperCardComponent;
      }();

      SwiperCardComponent.ctorParameters = function () {
        return [{
          type: _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["GestureController"]
        }, {
          type: _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["Platform"]
        }];
      };

      SwiperCardComponent.propDecorators = {
        cards: [{
          type: _angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"],
          args: ['cards']
        }],
        choiceMade: [{
          type: _angular_core__WEBPACK_IMPORTED_MODULE_1__["Output"]
        }],
        swipercards: [{
          type: _angular_core__WEBPACK_IMPORTED_MODULE_1__["ViewChildren"],
          args: ['swiperCard']
        }]
      };
      SwiperCardComponent = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-swiper-card',
        template: Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(
        /*! raw-loader!./swiper-card.component.html */
        "./node_modules/raw-loader/dist/cjs.js!./src/app/components/swiper-card/swiper-card.component.html"))["default"],
        styles: [Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(
        /*! ./swiper-card.component.scss */
        "./src/app/components/swiper-card/swiper-card.component.scss"))["default"]]
      })], SwiperCardComponent);
      /***/
    },

    /***/
    "./src/app/pipes/jsonpipe.pipe.ts":
    /*!****************************************!*\
      !*** ./src/app/pipes/jsonpipe.pipe.ts ***!
      \****************************************/

    /*! exports provided: JsonPipe */

    /***/
    function srcAppPipesJsonpipePipeTs(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "JsonPipe", function () {
        return JsonPipe;
      });
      /* harmony import */


      var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! tslib */
      "./node_modules/tslib/tslib.es6.js");
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! @angular/core */
      "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");

      var JsonPipe = /*#__PURE__*/function () {
        function JsonPipe() {
          _classCallCheck(this, JsonPipe);
        }

        _createClass(JsonPipe, [{
          key: "transform",
          value: function transform(value, args) {
            return null;
          }
        }]);

        return JsonPipe;
      }();

      JsonPipe = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Pipe"])({
        name: 'json'
      })], JsonPipe);
      /***/
    },

    /***/
    "./src/app/pipes/pipes.module.ts":
    /*!***************************************!*\
      !*** ./src/app/pipes/pipes.module.ts ***!
      \***************************************/

    /*! exports provided: PipesModule */

    /***/
    function srcAppPipesPipesModuleTs(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "PipesModule", function () {
        return PipesModule;
      });
      /* harmony import */


      var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! tslib */
      "./node_modules/tslib/tslib.es6.js");
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! @angular/core */
      "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
      /* harmony import */


      var _pipes_safepipe_pipe__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! @pipes/safepipe.pipe */
      "./src/app/pipes/safepipe.pipe.ts");
      /* harmony import */


      var _pipes_jsonpipe_pipe__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
      /*! @pipes/jsonpipe.pipe */
      "./src/app/pipes/jsonpipe.pipe.ts");

      var PipesModule = function PipesModule() {
        _classCallCheck(this, PipesModule);
      };

      PipesModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        declarations: [_pipes_safepipe_pipe__WEBPACK_IMPORTED_MODULE_2__["SafePipe"], _pipes_jsonpipe_pipe__WEBPACK_IMPORTED_MODULE_3__["JsonPipe"]],
        exports: [_pipes_safepipe_pipe__WEBPACK_IMPORTED_MODULE_2__["SafePipe"], _pipes_jsonpipe_pipe__WEBPACK_IMPORTED_MODULE_3__["JsonPipe"]]
      })], PipesModule);
      /***/
    },

    /***/
    "./src/app/pipes/safepipe.pipe.ts":
    /*!****************************************!*\
      !*** ./src/app/pipes/safepipe.pipe.ts ***!
      \****************************************/

    /*! exports provided: SafePipe */

    /***/
    function srcAppPipesSafepipePipeTs(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "SafePipe", function () {
        return SafePipe;
      });
      /* harmony import */


      var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! tslib */
      "./node_modules/tslib/tslib.es6.js");
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! @angular/core */
      "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
      /* harmony import */


      var _angular_platform_browser__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! @angular/platform-browser */
      "./node_modules/@angular/platform-browser/__ivy_ngcc__/fesm2015/platform-browser.js");

      var SafePipe = /*#__PURE__*/function () {
        function SafePipe(sanitizer) {
          _classCallCheck(this, SafePipe);

          this.sanitizer = sanitizer;
        }

        _createClass(SafePipe, [{
          key: "transform",
          value: function transform(url) {
            return this.sanitizer.bypassSecurityTrustResourceUrl(url);
          }
        }]);

        return SafePipe;
      }();

      SafePipe.ctorParameters = function () {
        return [{
          type: _angular_platform_browser__WEBPACK_IMPORTED_MODULE_2__["DomSanitizer"]
        }];
      };

      SafePipe = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Pipe"])({
        name: 'safe'
      })], SafePipe);
      /***/
    },

    /***/
    "./src/app/services/ionic/toast.service.ts":
    /*!*************************************************!*\
      !*** ./src/app/services/ionic/toast.service.ts ***!
      \*************************************************/

    /*! exports provided: ToasterService */

    /***/
    function srcAppServicesIonicToastServiceTs(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "ToasterService", function () {
        return ToasterService;
      });
      /* harmony import */


      var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! tslib */
      "./node_modules/tslib/tslib.es6.js");
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! @angular/core */
      "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
      /* harmony import */


      var _ionic_angular__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! @ionic/angular */
      "./node_modules/@ionic/angular/__ivy_ngcc__/fesm2015/ionic-angular.js");

      var ToasterService = /*#__PURE__*/function () {
        function ToasterService(toastCtrl) {
          _classCallCheck(this, ToasterService);

          this.toastCtrl = toastCtrl;
        }

        _createClass(ToasterService, [{
          key: "present",
          value: function present(message) {
            var duration = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : 2000;
            var color = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : 'primary';
            return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, /*#__PURE__*/regeneratorRuntime.mark(function _callee25() {
              return regeneratorRuntime.wrap(function _callee25$(_context25) {
                while (1) {
                  switch (_context25.prev = _context25.next) {
                    case 0:
                      _context25.next = 2;
                      return this.toastCtrl.create({
                        message: message,
                        duration: duration,
                        color: color
                      });

                    case 2:
                      this.toaster = _context25.sent;
                      _context25.next = 5;
                      return this.toaster.present();

                    case 5:
                    case "end":
                      return _context25.stop();
                  }
                }
              }, _callee25, this);
            }));
          }
        }]);

        return ToasterService;
      }();

      ToasterService.ctorParameters = function () {
        return [{
          type: _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["ToastController"]
        }];
      };

      ToasterService = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])({
        providedIn: 'root'
      })], ToasterService);
      /***/
    },

    /***/
    "./src/app/services/network.service.ts":
    /*!*********************************************!*\
      !*** ./src/app/services/network.service.ts ***!
      \*********************************************/

    /*! exports provided: NetworkService */

    /***/
    function srcAppServicesNetworkServiceTs(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "NetworkService", function () {
        return NetworkService;
      });
      /* harmony import */


      var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! tslib */
      "./node_modules/tslib/tslib.es6.js");
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! @angular/core */
      "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
      /* harmony import */


      var _ionic_native_network_ngx__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! @ionic-native/network/ngx */
      "./node_modules/@ionic-native/network/__ivy_ngcc__/ngx/index.js");

      var NetworkService = /*#__PURE__*/function (_ionic_native_network) {
        _inherits(NetworkService, _ionic_native_network);

        var _super = _createSuper(NetworkService);

        function NetworkService() {
          _classCallCheck(this, NetworkService);

          return _super.call(this);
        }

        _createClass(NetworkService, [{
          key: "listenNetwork",
          value: function listenNetwork() {
            var _this29 = this;

            this.onDisconnect().subscribe(function () {
              console.log('network was disconnected');
              console.log(_this29.type);
              _this29.online = false;
            });
            this.onConnect().subscribe(function () {
              console.log('network connected');
              console.log(_this29.type);
              _this29.online = true;
            });
            this.online = this.check_internet();
          }
        }, {
          key: "check_internet",
          value: function check_internet() {
            if (this.type == "ethernet" || this.type == "wifi" || this.type == "2g" || this.type == "3g" || this.type == "4g" || this.type == "cellular") {
              console.log("device is online");
              return true;
            } else {
              console.log("device is offline");
              return false;
            }
          }
        }]);

        return NetworkService;
      }(_ionic_native_network_ngx__WEBPACK_IMPORTED_MODULE_2__["Network"]);

      NetworkService.ctorParameters = function () {
        return [];
      };

      NetworkService = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])({
        providedIn: 'root'
      })], NetworkService);
      /***/
    },

    /***/
    "./src/environments/environment.ts":
    /*!*****************************************!*\
      !*** ./src/environments/environment.ts ***!
      \*****************************************/

    /*! exports provided: environment */

    /***/
    function srcEnvironmentsEnvironmentTs(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "environment", function () {
        return environment;
      }); // This file can be replaced during build by using the `fileReplacements` array.
      // `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
      // The list of file replacements can be found in `angular.json`.


      var environment = {
        production: false,
        server_url: "dev.blvckpixel.com/transformaticsbb",
        port: 443
      };
      /*
       * For easier debugging in development mode, you can import the following file
       * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
       *
       * This import should be commented out in production mode because it will have a negative impact
       * on performance if an error is thrown.
       */
      // import 'zone.js/dist/zone-error';  // Included with Angular CLI.

      /***/
    },

    /***/
    "./src/main.ts":
    /*!*********************!*\
      !*** ./src/main.ts ***!
      \*********************/

    /*! no exports provided */

    /***/
    function srcMainTs(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! @angular/core */
      "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
      /* harmony import */


      var _angular_platform_browser_dynamic__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! @angular/platform-browser-dynamic */
      "./node_modules/@angular/platform-browser-dynamic/__ivy_ngcc__/fesm2015/platform-browser-dynamic.js");
      /* harmony import */


      var _app_app_module__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! ./app/app.module */
      "./src/app/app.module.ts");
      /* harmony import */


      var _environments_environment__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
      /*! ./environments/environment */
      "./src/environments/environment.ts");

      if (_environments_environment__WEBPACK_IMPORTED_MODULE_3__["environment"].production) {
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["enableProdMode"])();
      }

      Object(_angular_platform_browser_dynamic__WEBPACK_IMPORTED_MODULE_1__["platformBrowserDynamic"])().bootstrapModule(_app_app_module__WEBPACK_IMPORTED_MODULE_2__["AppModule"])["catch"](function (err) {
        return console.log(err);
      });
      /***/
    },

    /***/
    0:
    /*!***************************!*\
      !*** multi ./src/main.ts ***!
      \***************************/

    /*! no static exports found */

    /***/
    function _(module, exports, __webpack_require__) {
      module.exports = __webpack_require__(
      /*! D:\Project\blvck pixies\Transformation\TransformaticsMobileCodebase\src\main.ts */
      "./src/main.ts");
      /***/
    }
  }, [[0, "runtime", "vendor"]]]);
})();
//# sourceMappingURL=main-es5.js.map