(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["pages-body-scan-arrival-body-scan-arrival-module"],{

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/pages/body-scan-arrival/body-scan-arrival.page.html":
/*!***********************************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/pages/body-scan-arrival/body-scan-arrival.page.html ***!
  \***********************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<ion-content>\n\n  <div class=\"main_container\">\n    <div class=\"description_section\">\n      <div class=\"header\">\n        <p>\n          Bienvenue dans Transformatics \n        </p>\n      </div>\n  \n      <div class=\"body\">\n        <p class=\"p1\">\n          Il s’agit de votre première connexion. Afin de permettre au système de s’individualiser à votre personne, il vous faut premièrement répondre à une série de questions pour que nous sachions tout de votre passé sportif, pour mieux créer votre futur.\n        </p>\n        <p class=\"p2\">\n          Le <strong>Bodyscan</strong> dure environ 7 minutes. Cliquez sur le bouton ci-dessous pour commencer. \n        </p>\n        <p class=\"p3\">\n          Note : il vous faut impérativement renseigner le Bodyscan dans son intégralité pour accéder à votre espace personnel. \n        </p>\n      </div>\n    </div>\n\n    <div class=\"footer\">\n      <ion-button class=\"commencer\" routerLink=\"/select-plan\">\n        Commencer\n      </ion-button>\n    </div>\n  </div>\n\n</ion-content>\n");

/***/ }),

/***/ "./src/app/pages/body-scan-arrival/body-scan-arrival-routing.module.ts":
/*!*****************************************************************************!*\
  !*** ./src/app/pages/body-scan-arrival/body-scan-arrival-routing.module.ts ***!
  \*****************************************************************************/
/*! exports provided: BodyScanArrivalPageRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "BodyScanArrivalPageRoutingModule", function() { return BodyScanArrivalPageRoutingModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");
/* harmony import */ var _body_scan_arrival_page__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./body-scan-arrival.page */ "./src/app/pages/body-scan-arrival/body-scan-arrival.page.ts");




const routes = [
    {
        path: '',
        component: _body_scan_arrival_page__WEBPACK_IMPORTED_MODULE_3__["BodyScanArrivalPage"]
    }
];
let BodyScanArrivalPageRoutingModule = class BodyScanArrivalPageRoutingModule {
};
BodyScanArrivalPageRoutingModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]],
    })
], BodyScanArrivalPageRoutingModule);



/***/ }),

/***/ "./src/app/pages/body-scan-arrival/body-scan-arrival.module.ts":
/*!*********************************************************************!*\
  !*** ./src/app/pages/body-scan-arrival/body-scan-arrival.module.ts ***!
  \*********************************************************************/
/*! exports provided: BodyScanArrivalPageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "BodyScanArrivalPageModule", function() { return BodyScanArrivalPageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/__ivy_ngcc__/fesm2015/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/__ivy_ngcc__/fesm2015/forms.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/__ivy_ngcc__/fesm2015/ionic-angular.js");
/* harmony import */ var _body_scan_arrival_routing_module__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./body-scan-arrival-routing.module */ "./src/app/pages/body-scan-arrival/body-scan-arrival-routing.module.ts");
/* harmony import */ var _body_scan_arrival_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./body-scan-arrival.page */ "./src/app/pages/body-scan-arrival/body-scan-arrival.page.ts");







let BodyScanArrivalPageModule = class BodyScanArrivalPageModule {
};
BodyScanArrivalPageModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [
            _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonicModule"],
            _body_scan_arrival_routing_module__WEBPACK_IMPORTED_MODULE_5__["BodyScanArrivalPageRoutingModule"]
        ],
        declarations: [_body_scan_arrival_page__WEBPACK_IMPORTED_MODULE_6__["BodyScanArrivalPage"]]
    })
], BodyScanArrivalPageModule);



/***/ }),

/***/ "./src/app/pages/body-scan-arrival/body-scan-arrival.page.scss":
/*!*********************************************************************!*\
  !*** ./src/app/pages/body-scan-arrival/body-scan-arrival.page.scss ***!
  \*********************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("ion-content {\n  --background: url('landing_page_background_1.png')0 18%/100% no-repeat;\n  background-size: cover;\n  --overflow: hidden;\n}\n\n.body {\n  color: #fff;\n}\n\n.body .p1,\n.body .p2 {\n  font-family: Oswald, serif;\n  text-align: justify;\n  font-size: 0.9rem;\n  /* letter-spacing: 1px; */\n  margin-top: 5%;\n  line-height: 1rem;\n}\n\n.body .p3 {\n  font-family: Oswald, serif;\n  text-align: justify;\n  font-style: italic;\n  margin-top: 5%;\n  line-height: 0.9rem;\n  font-size: 0.7rem;\n}\n\n.main_container {\n  height: 100%;\n  display: flex;\n  flex-direction: column;\n  justify-content: space-between;\n  padding-bottom: 10%;\n  background-color: rgba(0, 0, 0, 0.7);\n  padding-left: 5%;\n  padding-right: 5%;\n}\n\n.header p {\n  color: var(--ion-color-primary);\n  font-family: Oswald, serif;\n  font-size: 18px;\n  text-align: left;\n}\n\n.footer ion-button {\n  width: 100%;\n  border-radius: 10px;\n  height: 40px;\n}\n\n.description_section {\n  padding-top: 50%;\n}\n\n.commencer {\n  --background: var(--ion-btn-custom-color);\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvcGFnZXMvYm9keS1zY2FuLWFycml2YWwvYm9keS1zY2FuLWFycml2YWwucGFnZS5zY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBO0VBQ0ksc0VBQUE7RUFHQSxzQkFBQTtFQUNBLGtCQUFBO0FBQ0o7O0FBR0E7RUFDSSxXQUFBO0FBQUo7O0FBRUk7O0VBRUksMEJBQUE7RUFDQSxtQkFBQTtFQUNBLGlCQUFBO0VBQ0EseUJBQUE7RUFDQSxjQUFBO0VBQ0EsaUJBQUE7QUFBUjs7QUFHSTtFQUNJLDBCQUFBO0VBQ0EsbUJBQUE7RUFDQSxrQkFBQTtFQUNBLGNBQUE7RUFDQSxtQkFBQTtFQUNBLGlCQUFBO0FBRFI7O0FBT0E7RUFDSSxZQUFBO0VBQ0EsYUFBQTtFQUNBLHNCQUFBO0VBQ0EsOEJBQUE7RUFDQSxtQkFBQTtFQUNBLG9DQUFBO0VBQ0EsZ0JBQUE7RUFDQSxpQkFBQTtBQUpKOztBQVFJO0VBQ0ksK0JBQUE7RUFDQSwwQkFBQTtFQUNBLGVBQUE7RUFDQSxnQkFBQTtBQUxSOztBQVlJO0VBQ0ksV0FBQTtFQUNBLG1CQUFBO0VBQ0EsWUFBQTtBQVRSOztBQWFBO0VBQ0ksZ0JBQUE7QUFWSjs7QUFhQTtFQUNJLHlDQUFBO0FBVkoiLCJmaWxlIjoic3JjL2FwcC9wYWdlcy9ib2R5LXNjYW4tYXJyaXZhbC9ib2R5LXNjYW4tYXJyaXZhbC5wYWdlLnNjc3MiLCJzb3VyY2VzQ29udGVudCI6WyJpb24tY29udGVudCB7XG4gICAgLS1iYWNrZ3JvdW5kOiB1cmwoLi4vLi4vLi4vYXNzZXRzL2ltYWdlcy9sYW5kaW5nX3BhZ2VfYmFja2dyb3VuZF8xLnBuZykwIDE4JS8xMDAlIG5vLXJlcGVhdDtcbiAgICAtd2Via2l0LWJhY2tncm91bmQtc2l6ZTogY292ZXI7XG4gICAgLW1vei1iYWNrZ3JvdW5kLXNpemU6IGNvdmVyO1xuICAgIGJhY2tncm91bmQtc2l6ZTogY292ZXI7XG4gICAgLS1vdmVyZmxvdzogaGlkZGVuO1xuXG59XG5cbi5ib2R5IHtcbiAgICBjb2xvcjogI2ZmZjtcblxuICAgIC5wMSxcbiAgICAucDIge1xuICAgICAgICBmb250LWZhbWlseTogT3N3YWxkLCBzZXJpZjtcbiAgICAgICAgdGV4dC1hbGlnbjoganVzdGlmeTtcbiAgICAgICAgZm9udC1zaXplOiAwLjlyZW07XG4gICAgICAgIC8qIGxldHRlci1zcGFjaW5nOiAxcHg7ICovXG4gICAgICAgIG1hcmdpbi10b3A6IDUlO1xuICAgICAgICBsaW5lLWhlaWdodDogMXJlbTtcbiAgICB9XG5cbiAgICAucDMge1xuICAgICAgICBmb250LWZhbWlseTogT3N3YWxkLCBzZXJpZjtcbiAgICAgICAgdGV4dC1hbGlnbjoganVzdGlmeTtcbiAgICAgICAgZm9udC1zdHlsZTogaXRhbGljO1xuICAgICAgICBtYXJnaW4tdG9wOiA1JTtcbiAgICAgICAgbGluZS1oZWlnaHQ6IDAuOXJlbTtcbiAgICAgICAgZm9udC1zaXplOiAwLjdyZW07XG4gICAgfVxuXG59XG5cblxuLm1haW5fY29udGFpbmVyIHtcbiAgICBoZWlnaHQ6IDEwMCU7XG4gICAgZGlzcGxheTogZmxleDtcbiAgICBmbGV4LWRpcmVjdGlvbjogY29sdW1uO1xuICAgIGp1c3RpZnktY29udGVudDogc3BhY2UtYmV0d2VlbjtcbiAgICBwYWRkaW5nLWJvdHRvbTogMTAlO1xuICAgIGJhY2tncm91bmQtY29sb3I6IHJnYmEoMCwgMCwgMCwgMC43KTtcbiAgICBwYWRkaW5nLWxlZnQ6IDUlO1xuICAgIHBhZGRpbmctcmlnaHQ6IDUlO1xufVxuXG4uaGVhZGVyIHtcbiAgICBwIHtcbiAgICAgICAgY29sb3I6IHZhcigtLWlvbi1jb2xvci1wcmltYXJ5KTtcbiAgICAgICAgZm9udC1mYW1pbHk6IE9zd2FsZCwgc2VyaWY7XG4gICAgICAgIGZvbnQtc2l6ZTogMThweDtcbiAgICAgICAgdGV4dC1hbGlnbjogbGVmdDtcbiAgICB9XG59XG5cblxuLmZvb3RlciB7XG5cbiAgICBpb24tYnV0dG9uIHtcbiAgICAgICAgd2lkdGg6IDEwMCU7XG4gICAgICAgIGJvcmRlci1yYWRpdXM6IDEwcHg7XG4gICAgICAgIGhlaWdodDogNDBweDtcbiAgICB9XG59XG5cbi5kZXNjcmlwdGlvbl9zZWN0aW9uIHtcbiAgICBwYWRkaW5nLXRvcDogNTAlO1xufVxuXG4uY29tbWVuY2VyIHtcbiAgICAtLWJhY2tncm91bmQ6IHZhcigtLWlvbi1idG4tY3VzdG9tLWNvbG9yKTtcbn0iXX0= */");

/***/ }),

/***/ "./src/app/pages/body-scan-arrival/body-scan-arrival.page.ts":
/*!*******************************************************************!*\
  !*** ./src/app/pages/body-scan-arrival/body-scan-arrival.page.ts ***!
  \*******************************************************************/
/*! exports provided: BodyScanArrivalPage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "BodyScanArrivalPage", function() { return BodyScanArrivalPage; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");


let BodyScanArrivalPage = class BodyScanArrivalPage {
    constructor() { }
    ngOnInit() {
    }
};
BodyScanArrivalPage.ctorParameters = () => [];
BodyScanArrivalPage = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-body-scan-arrival',
        template: Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(/*! raw-loader!./body-scan-arrival.page.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/pages/body-scan-arrival/body-scan-arrival.page.html")).default,
        styles: [Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(/*! ./body-scan-arrival.page.scss */ "./src/app/pages/body-scan-arrival/body-scan-arrival.page.scss")).default]
    })
], BodyScanArrivalPage);



/***/ })

}]);
//# sourceMappingURL=pages-body-scan-arrival-body-scan-arrival-module-es2015.js.map