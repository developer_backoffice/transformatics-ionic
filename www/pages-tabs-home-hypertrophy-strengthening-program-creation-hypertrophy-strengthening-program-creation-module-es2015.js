(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["pages-tabs-home-hypertrophy-strengthening-program-creation-hypertrophy-strengthening-program-creation-module"],{

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/pages/tabs/home/hypertrophy-strengthening-program-creation/hypertrophy-strengthening-program-creation.page.html":
/*!***********************************************************************************************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/pages/tabs/home/hypertrophy-strengthening-program-creation/hypertrophy-strengthening-program-creation.page.html ***!
  \***********************************************************************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<!-- <ion-header>\n  <ion-toolbar>\n    <ion-title>hypertrophy-strengthening-program-creation</ion-title>\n  </ion-toolbar>\n</ion-header> -->\n\n<ion-content>\n  <ion-grid>\n    <ion-row>\n      <ion-text class=\"small_text\"> Image associée au programme </ion-text>\n    </ion-row>\n\n    <ion-row class=\"ion-margin-top\">\n      <ion-slides\n        pager=\"false\"\n        class=\"plan-slider\"\n        [options]=\"slideOptions\"\n        #slides\n        class=\"select-plan\"\n      >\n        <ion-slide>\n          <div class=\"slide-image\">\n            <img src=\"assets/images/payment_slide_1.png\" class=\"slide_image\" />\n          </div>\n        </ion-slide>\n\n        <ion-slide>\n          <div class=\"slide-image\">\n            <img src=\"assets/images/payment_slide_2.png\" class=\"slide_image\" />\n          </div>\n        </ion-slide>\n\n        <ion-slide>\n          <div class=\"slide-image\">\n            <img src=\"assets/images/payment_slide_3.png\" class=\"slide_image\" />\n          </div>\n        </ion-slide>\n      </ion-slides>\n    </ion-row>\n\n    <ion-row class=\"ion-margin-top\">\n      <ion-text class=\"small_text\"> Nom de votre programme </ion-text>\n    </ion-row>\n\n    <ion-row>\n      <ion-item class=\"row-width-underline ion-no-padding\">\n        <ion-input class=\"medium_text_bold\" placeholder=\"Nouveau programme\" style=\"--placeholder-color: #000;\"></ion-input>\n      </ion-item>\n\n      <ion-item class=\"row-width-underline ion-no-padding\">\n        <ion-input placeholder=\"Description\" class=\"medium_text_bold_light\"></ion-input>\n      </ion-item>\n    </ion-row>\n\n    <ion-row class=\"select_row_section\">\n      <ion-item lines=\"none\" class=\"ion-no-padding row-width-underline\">\n        <ion-label class=\"small_text\">Objectif</ion-label>\n        <ion-select\n          interface=\"action-sheet\"\n          class=\"custom-options\"\n          [(ngModel)]=\"objectif\"\n        >\n          <ion-select-option value=\"Hypertrophie\" class=\"small_text\">\n            Hypertrophie\n          </ion-select-option>\n          <ion-select-option value=\"Hypertrophie\" class=\"small_text\">\n            Force\n          </ion-select-option>\n          <ion-select-option value=\"Hypertrophie\" class=\"small_text\">\n            Aucun \n          </ion-select-option>\n        </ion-select>\n      </ion-item>\n\n      <ion-item lines=\"none\" class=\"ion-no-padding row-width-underline\">\n        <ion-label class=\"small_text\">Format d’entraînement </ion-label>\n        <ion-select\n          interface=\"action-sheet\"\n          class=\"custom-options\"\n          [(ngModel)]=\"format_dentrainement\" \n        >\n          <ion-select-option value=\"SPLIT\" class=\"small_text\"> SPLIT </ion-select-option>\n          <ion-select-option value=\"SPLIT\" class=\"small_text\"> Fullbody </ion-select-option>\n          <ion-select-option value=\"SPLIT\" class=\"small_text\"> Halfbody </ion-select-option>\n          <ion-select-option value=\"SPLIT\" class=\"small_text\"> Manuel </ion-select-option>\n        </ion-select>\n      </ion-item>\n\n      <ion-item lines=\"none\" class=\"ion-no-padding row-width-underline\">\n        <ion-label class=\"small_text\"\n          >Jours d’entraînement/semaine\n        </ion-label>\n        <ion-select\n          interface=\"action-sheet\"\n          class=\"custom-options\"\n          [(ngModel)]=\"training_days\"\n        >\n          <ion-select-option value=\"3\" class=\"small_text\"> 3 </ion-select-option>\n        </ion-select>\n      </ion-item>\n\n      <ion-item lines=\"none\" class=\"ion-no-padding row-width-underline\">\n        <ion-label class=\"small_text\">Méthode de progression </ion-label>\n        <ion-select\n          interface=\"action-sheet\"\n          class=\"custom-options\"\n          [(ngModel)]=\"progression_method\"\n        >\n          <ion-select-option value=\"Aucune\" class=\"small_text\"> Aucune </ion-select-option>\n        </ion-select>\n      </ion-item>\n\n      <ion-item lines=\"none\" class=\"ion-no-padding row-width-underline\">\n        <ion-label class=\"small_text\"\n          >Commencer le programme à partir de\n        </ion-label>\n        <ion-select\n          interface=\"action-sheet\"\n          class=\"custom-options\"\n          [(ngModel)]=\"start_the_program\"\n        >\n          <ion-select-option value=\"Aujourd’hui\">\n            Aujourd’hui\n          </ion-select-option>\n        </ion-select>\n      </ion-item>\n\n      <!-- <ion-item lines=\"none\" class=\"ion-no-padding row-width-underline\">\n        <ion-label class=\"small_text_italic\"\n          >Je veux inclure un Deload à chaque cycle\n        </ion-label>\n        <ion-checkbox></ion-checkbox>\n      </ion-item> -->\n\n      <ion-row class=\"select_body_section\" routerLink=\"/select-body-groups\">\n        <ion-label class=\"small_text_bold\">\n          Ajouter les exercices de force\n\n        </ion-label>\n      </ion-row>\n    </ion-row>\n\n    <ion-row class=\"btn_section\">\n      <ion-label class=\"medium_text_header\">Jours d’entraînement </ion-label>\n    </ion-row>\n\n\n    <ion-row class=\"ion-margin-top row-padding\" style=\"margin-right: 5%;\">\n      <ion-col size=\"8\" >\n        <ion-slides  [options]=\"btnSlideOptions\" class=\"btnSlides\" [pager]=\"false\"  #theSlides>\n          <ion-slide *ngFor=\"let item of btnArray\">\n            <ion-card [class]=\"item.active ? 'active_btn card_btn' : 'card_btn'\" (click)=\"makeActive(item)\">\n              <ion-label class=\"medium_text_bold\"> {{item.text}} </ion-label>\n            </ion-card>\n          </ion-slide>\n        </ion-slides>\n      </ion-col>\n      <ion-col size=\"4\" class=\"add_btn_col\">\n        <ion-card class=\"card_btn\" (click)=\"addBtn(theSlides)\">\n          <ion-label class=\"medium_text_bold\"> + </ion-label>\n        </ion-card>\n      </ion-col>\n    </ion-row>\n\n\n    <ion-row class=\"ion-margin-top\">\n      <ion-item class=\"row-width-underline ion-no-padding\">\n        <ion-label>\n          Jour {{filteredData.text}}\n        </ion-label>\n      </ion-item>\n    </ion-row>\n<!-- \n    <ion-row class=\"ion-margin-top\">\n      <ion-col size=\"9\" >\n        <ion-slides  [options]=\"btnSlideOptions\" class=\"btnSlides\" [pager]=\"false\"  #theSlides>\n          <ion-slide *ngFor=\"let item of btnArray\">\n            <ion-card [class]=\"item.text == 1 ? 'active_btn card_btn' : 'card_btn' \">\n              <ion-label class=\"medium_text_bold\"> {{item.text}} </ion-label>\n            </ion-card>\n          </ion-slide>\n        </ion-slides>\n      </ion-col>\n      <ion-col size=\"3\" class=\"add_btn_col\">\n        <ion-card class=\"card_btn\" (click)=\"addBtn(theSlides)\">\n          <ion-label class=\"medium_text_bold\"> + </ion-label>\n        </ion-card>\n      </ion-col>\n    </ion-row> -->\n\n    <!-- <div class=\"ion-margin-top\" class=\"btn_underline\">\n      <div class=\"profile-viewall\">\n        <ion-button\n          >Jour 1 -->\n          <!-- <ion-icon name=\"chevron-forward-outline\"></ion-icon> -->\n        <!-- </ion-button>\n      </div>\n    </div> -->\n\n    <!-- <ion-row class=\"ion-margin-top\">\n      <ion-text class=\"small_text\"> Aucun exercice pour le moment </ion-text>\n    </ion-row> -->\n\n    <!-- <ion-row class=\"ion-margin-top\">\n      <ion-item class=\"ion-no-padding\" lines=\"none\">\n        <img class=\"jour_img\" src=\"assets/icon/2body.png\" />\n\n        <ion-label>\n          <ion-label class=\"medium_text_bold\">\n            Groupes musculaires sollicités :\n          </ion-label>\n\n          <ion-label class=\"small_text\"> Pectoraux </ion-label>\n\n          <ion-label>\n            <ion-row class=\"ion-no-padding exercise-details-section\">\n              <ion-col size=\"8\" class=\"detail_excercise_col\">\n                <ion-label class=\"small_text\">\n                  Détails des exercices\n                </ion-label>\n              </ion-col>\n\n              <ion-col size=\"4\" class=\"toogle_col_section\">\n                <ion-toggle (ionChange)=\"toggleChange($event)\"></ion-toggle>\n              </ion-col>\n            </ion-row>\n          </ion-label>\n        </ion-label>\n      </ion-item>\n    </ion-row> -->\n\n    <!-- <ion-row class=\"ion-margin-top\" *ngIf=\"!showListItem\">\n      <ion-reorder-group disabled=\"false\"  reorder='true' (ionItemReorder)=\"onItemReorder($event)\">\n        \n        <ion-item>\n          <div class=\"reorder_list_div_section\"></div>\n\n          <ion-label class=\"reorder_list_title\">\n            <ion-label class=\"medium_text_bold\"> Nom de l’exercice </ion-label>\n            <ion-label class=\"medium_text_bold\">\n              Sous nom de l’exercice\n            </ion-label>\n          </ion-label>\n          <ion-reorder slot=\"end\"></ion-reorder>\n        </ion-item>\n\n        <ion-item>\n          <div class=\"reorder_list_div_section\"></div>\n\n          <ion-label class=\"reorder_list_title\">\n            <ion-label class=\"medium_text_bold\"> Nom de l’exercice </ion-label>\n            <ion-label class=\"medium_text_bold\">\n              Sous nom de l’exercice\n            </ion-label>\n          </ion-label>\n          <ion-reorder slot=\"end\"></ion-reorder>\n        </ion-item>\n\n        <ion-list-header>BISET</ion-list-header>\n        <ion-item>\n          <div class=\"reorder_list_div_section\"></div>\n\n          <ion-label class=\"reorder_list_title\">\n            <ion-label class=\"medium_text_bold\"> Nom de l’exercice </ion-label>\n            <ion-label class=\"medium_text_bold\">\n              Sous nom de l’exercice\n            </ion-label>\n          </ion-label>\n          <ion-reorder slot=\"end\"></ion-reorder>\n        </ion-item>\n\n        <ion-item>\n          <div class=\"reorder_list_div_section\"></div>\n\n          <ion-label class=\"reorder_list_title\">\n            <ion-label class=\"medium_text_bold\"> Nom de l’exercice </ion-label>\n            <ion-label class=\"medium_text_bold\">\n              Sous nom de l’exercice\n            </ion-label>\n          </ion-label>\n          <ion-reorder slot=\"end\"></ion-reorder>\n        </ion-item>\n      </ion-reorder-group>\n    </ion-row> -->\n\n    <!-- <ion-row class=\"ion-margin-top\" *ngIf=\"showListItem\">\n      <ion-row>\n        <ion-row class=\"biset_section_big\">\n          <h6>BISET – ENCHAINEMENT DE 2 EXERCICES</h6>\n        </ion-row>\n\n        <ion-row class=\"biset_section_first_row\">\n          <ion-item\n            class=\"ion-no-padding\"\n            lines=\"none\"\n            class=\"biset_section_ion_item\"\n          >\n            <div class=\"reorder_list_div_section\"></div>\n\n            <ion-label class=\"reorder_list_title\">\n              <ion-label class=\"medium_text_bold\">\n                Nom de l’exercice\n              </ion-label>\n\n              <ion-label class=\"small_text\"> Sous nom de l’exercice </ion-label>\n            </ion-label>\n          </ion-item>\n\n          <ion-row class=\"biset_section_second_row\">\n            <ion-col class=\"ion-no-padding\" size=\"6\">\n              Séries effectives : 4\n            </ion-col>\n\n            <ion-col class=\"ion-no-padding\" size=\"6\" style=\"align-self: center\">\n              <ion-button class=\"biset_section_btn_one\">\n                Ajouter une série\n              </ion-button>\n            </ion-col>\n          </ion-row>\n        </ion-row>\n\n        <ion-row class=\"biset_section_btn_row\">\n          <ion-button fill=\"flat\" style=\"--background: transparent\">\n            + Créer un triset\n          </ion-button>\n        </ion-row>\n      </ion-row>\n    </ion-row> -->\n  </ion-grid>\n</ion-content>\n\n<ion-footer class=\"ion-no-border footer_section\">\n  <ion-row>\n    <ion-button routerLink=\"/add-exercise\" class=\"next footer_section_btn\" expand=\"full\">\n      Ajouter des exercices\n    </ion-button>\n  </ion-row>\n</ion-footer>\n");

/***/ }),

/***/ "./src/app/pages/tabs/home/hypertrophy-strengthening-program-creation/hypertrophy-strengthening-program-creation-routing.module.ts":
/*!*****************************************************************************************************************************************!*\
  !*** ./src/app/pages/tabs/home/hypertrophy-strengthening-program-creation/hypertrophy-strengthening-program-creation-routing.module.ts ***!
  \*****************************************************************************************************************************************/
/*! exports provided: HypertrophyStrengtheningProgramCreationPageRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "HypertrophyStrengtheningProgramCreationPageRoutingModule", function() { return HypertrophyStrengtheningProgramCreationPageRoutingModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");
/* harmony import */ var _hypertrophy_strengthening_program_creation_page__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./hypertrophy-strengthening-program-creation.page */ "./src/app/pages/tabs/home/hypertrophy-strengthening-program-creation/hypertrophy-strengthening-program-creation.page.ts");




const routes = [
    {
        path: '',
        component: _hypertrophy_strengthening_program_creation_page__WEBPACK_IMPORTED_MODULE_3__["HypertrophyStrengtheningProgramCreationPage"]
    }
];
let HypertrophyStrengtheningProgramCreationPageRoutingModule = class HypertrophyStrengtheningProgramCreationPageRoutingModule {
};
HypertrophyStrengtheningProgramCreationPageRoutingModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]],
    })
], HypertrophyStrengtheningProgramCreationPageRoutingModule);



/***/ }),

/***/ "./src/app/pages/tabs/home/hypertrophy-strengthening-program-creation/hypertrophy-strengthening-program-creation.module.ts":
/*!*********************************************************************************************************************************!*\
  !*** ./src/app/pages/tabs/home/hypertrophy-strengthening-program-creation/hypertrophy-strengthening-program-creation.module.ts ***!
  \*********************************************************************************************************************************/
/*! exports provided: HypertrophyStrengtheningProgramCreationPageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "HypertrophyStrengtheningProgramCreationPageModule", function() { return HypertrophyStrengtheningProgramCreationPageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/__ivy_ngcc__/fesm2015/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/__ivy_ngcc__/fesm2015/forms.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/__ivy_ngcc__/fesm2015/ionic-angular.js");
/* harmony import */ var _hypertrophy_strengthening_program_creation_routing_module__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./hypertrophy-strengthening-program-creation-routing.module */ "./src/app/pages/tabs/home/hypertrophy-strengthening-program-creation/hypertrophy-strengthening-program-creation-routing.module.ts");
/* harmony import */ var _hypertrophy_strengthening_program_creation_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./hypertrophy-strengthening-program-creation.page */ "./src/app/pages/tabs/home/hypertrophy-strengthening-program-creation/hypertrophy-strengthening-program-creation.page.ts");







let HypertrophyStrengtheningProgramCreationPageModule = class HypertrophyStrengtheningProgramCreationPageModule {
};
HypertrophyStrengtheningProgramCreationPageModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [
            _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonicModule"],
            _hypertrophy_strengthening_program_creation_routing_module__WEBPACK_IMPORTED_MODULE_5__["HypertrophyStrengtheningProgramCreationPageRoutingModule"]
        ],
        declarations: [_hypertrophy_strengthening_program_creation_page__WEBPACK_IMPORTED_MODULE_6__["HypertrophyStrengtheningProgramCreationPage"]]
    })
], HypertrophyStrengtheningProgramCreationPageModule);



/***/ }),

/***/ "./src/app/pages/tabs/home/hypertrophy-strengthening-program-creation/hypertrophy-strengthening-program-creation.page.scss":
/*!*********************************************************************************************************************************!*\
  !*** ./src/app/pages/tabs/home/hypertrophy-strengthening-program-creation/hypertrophy-strengthening-program-creation.page.scss ***!
  \*********************************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = (".row-width-underline {\n  width: 100%;\n  border-bottom: 1px solid gray !important;\n}\n\nion-content {\n  --padding-top: 10%;\n  --padding-start: 3%;\n  --padding-bottom: 20%;\n}\n\n.slide-image img {\n  height: 127px;\n}\n\n.slide_section {\n  margin-top: 3%;\n}\n\n.select_row_section {\n  margin-top: 10%;\n}\n\n.btn_section {\n  margin-top: 10%;\n}\n\n.card_btn {\n  height: 70px;\n  width: 70px;\n  border: 2px solid #565656;\n  margin: 0;\n  display: flex;\n  justify-content: center;\n}\n\n.card_btn ion-label {\n  align-self: center;\n  font-size: 1.2rem;\n  font-weight: 600;\n  color: #000;\n}\n\n.btnSlides ion-slide {\n  width: 80px;\n  margin-right: 10px;\n}\n\n.profile-viewall {\n  width: 100%;\n  position: relative;\n  margin-top: 15px;\n  margin-bottom: 15px;\n}\n\n.profile-viewall ion-button {\n  --border-radius: 25px;\n  display: table;\n  margin: auto;\n  font-size: 12px;\n}\n\n.profile-viewall:before {\n  content: \"\";\n  position: absolute;\n  background: #c1c1c1;\n  height: 1px;\n  width: 100%;\n  top: 19px;\n  left: 0;\n}\n\n.btn_underline {\n  margin-top: 10%;\n}\n\n.add_btn_col {\n  display: flex;\n  justify-content: center;\n}\n\nion-item {\n  width: 100%;\n  height: 40px;\n  display: flex;\n}\n\nion-reorder-group {\n  width: 100%;\n}\n\n.jour_img {\n  width: 100px;\n}\n\n.exercise-details-section {\n  height: 35px;\n  margin-top: -8px;\n}\n\n.detail_excercise_col {\n  align-self: center;\n  padding: 0;\n}\n\n.toogle_col_section {\n  display: flex;\n  justify-content: flex-end;\n}\n\n.reorder_list_div_section {\n  width: 50px;\n  background: #000;\n  height: 35px;\n}\n\n.reorder_list_title {\n  padding-left: 10px;\n}\n\n.biset_section_big {\n  width: 100%;\n  justify-content: center;\n  background: #e7e6e6;\n  border-top-left-radius: 7px;\n  border-top-right-radius: 7px;\n  margin-bottom: 2px;\n}\n\n.biset_section_first_row {\n  background: #e7e6e6;\n  width: 100%;\n  padding: 5px;\n}\n\n.biset_section_ion_item {\n  --background: transparent ;\n}\n\n.biset_section_second_row {\n  width: 100%;\n}\n\n.biset_section_btn_one {\n  height: 27px;\n  --border-radius: 15px ;\n}\n\n.biset_section_btn_row {\n  width: 100%;\n  justify-content: center;\n  background: #e7e6e6;\n  margin-top: 5px;\n  border-bottom-left-radius: 7px;\n  border-bottom-right-radius: 7px;\n}\n\n.footer_section {\n  width: 100%;\n  position: fixed;\n  bottom: 10px;\n}\n\n.footer_section_btn {\n  width: 100%;\n  padding-left: 10px;\n  padding-right: 10px;\n  margin-top: 32px;\n}\n\n.active_btn {\n  background: var(--ion-btn-custom-color);\n}\n\nion-select::part(text) {\n  font-size: 0.8rem !important;\n  font-family: \"Oswald\" !important;\n}\n\n.next {\n  --background: var(--ion-btn-custom-color);\n  --ion-button-round-border-radius: 5px;\n  --ion-button-border-radius: 5px;\n  --ion-border-radius: 5px;\n  --border-radius: 5px;\n}\n\n.select_body_section {\n  margin-top: 5%;\n  margin-right: 4%;\n  border-style: dotted;\n  border-color: #696969;\n  padding: 5px;\n  justify-content: center;\n  border-width: 2px;\n  border-radius: 10px;\n  width: 100%;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvcGFnZXMvdGFicy9ob21lL2h5cGVydHJvcGh5LXN0cmVuZ3RoZW5pbmctcHJvZ3JhbS1jcmVhdGlvbi9oeXBlcnRyb3BoeS1zdHJlbmd0aGVuaW5nLXByb2dyYW0tY3JlYXRpb24ucGFnZS5zY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUVBO0VBQ0ksV0FBQTtFQUNBLHdDQUFBO0FBREo7O0FBSUE7RUFDSSxrQkFBQTtFQUNBLG1CQUFBO0VBQ0EscUJBQUE7QUFESjs7QUFJQTtFQUVJLGFBQUE7QUFGSjs7QUFNQTtFQUNJLGNBQUE7QUFISjs7QUFRQTtFQUNJLGVBQUE7QUFMSjs7QUFRQTtFQUNJLGVBQUE7QUFMSjs7QUFRQTtFQUNJLFlBQUE7RUFDQSxXQUFBO0VBQ0EseUJBQUE7RUFDQSxTQUFBO0VBQ0EsYUFBQTtFQUNBLHVCQUFBO0FBTEo7O0FBT0k7RUFDSSxrQkFBQTtFQUNBLGlCQUFBO0VBQ0EsZ0JBQUE7RUFDQSxXQUFBO0FBTFI7O0FBV0k7RUFDSSxXQUFBO0VBQ0Esa0JBQUE7QUFSUjs7QUFhQTtFQUNJLFdBQUE7RUFDQSxrQkFBQTtFQUNBLGdCQUFBO0VBQ0EsbUJBQUE7QUFWSjs7QUFhQTtFQUNJLHFCQUFBO0VBQ0EsY0FBQTtFQUNBLFlBQUE7RUFDQSxlQUFBO0FBVko7O0FBYUE7RUFDSSxXQUFBO0VBQ0Esa0JBQUE7RUFDQSxtQkFBQTtFQUNBLFdBQUE7RUFDQSxXQUFBO0VBQ0EsU0FBQTtFQUNBLE9BQUE7QUFWSjs7QUFtQkE7RUFDSSxlQUFBO0FBaEJKOztBQW9CQTtFQUVJLGFBQUE7RUFDQSx1QkFBQTtBQWxCSjs7QUFvQkE7RUFDSSxXQUFBO0VBQ0EsWUFBQTtFQUNBLGFBQUE7QUFqQko7O0FBb0JBO0VBQ0ksV0FBQTtBQWpCSjs7QUFvQkE7RUFDSSxZQUFBO0FBakJKOztBQW9CQTtFQUNJLFlBQUE7RUFDQSxnQkFBQTtBQWpCSjs7QUFxQkE7RUFDSSxrQkFBQTtFQUNBLFVBQUE7QUFsQko7O0FBc0JBO0VBQ0ksYUFBQTtFQUNBLHlCQUFBO0FBbkJKOztBQXVCQTtFQUNJLFdBQUE7RUFDQSxnQkFBQTtFQUNBLFlBQUE7QUFwQko7O0FBd0JBO0VBQ0ksa0JBQUE7QUFyQko7O0FBd0JBO0VBQ0ksV0FBQTtFQUNBLHVCQUFBO0VBQ0EsbUJBQUE7RUFDQSwyQkFBQTtFQUNBLDRCQUFBO0VBQ0Esa0JBQUE7QUFyQko7O0FBd0JBO0VBQ0ksbUJBQUE7RUFDQSxXQUFBO0VBQ0EsWUFBQTtBQXJCSjs7QUF3QkE7RUFDSSwwQkFBQTtBQXJCSjs7QUF3QkE7RUFDSSxXQUFBO0FBckJKOztBQXdCQTtFQUNJLFlBQUE7RUFDQSxzQkFBQTtBQXJCSjs7QUF5QkE7RUFDSSxXQUFBO0VBQ0EsdUJBQUE7RUFDQSxtQkFBQTtFQUNBLGVBQUE7RUFDQSw4QkFBQTtFQUNBLCtCQUFBO0FBdEJKOztBQXlCQTtFQUNJLFdBQUE7RUFDQSxlQUFBO0VBQ0EsWUFBQTtBQXRCSjs7QUEyQkE7RUFFSSxXQUFBO0VBQ0Esa0JBQUE7RUFDQSxtQkFBQTtFQUNBLGdCQUFBO0FBekJKOztBQStCQTtFQUNJLHVDQUFBO0FBNUJKOztBQStCQTtFQUNJLDRCQUFBO0VBQ0EsZ0NBQUE7QUE1Qko7O0FBK0JFO0VBQ0UseUNBQUE7RUFDQSxxQ0FBQTtFQUNBLCtCQUFBO0VBQ0Esd0JBQUE7RUFDQSxvQkFBQTtBQTVCSjs7QUErQkU7RUFFRSxjQUFBO0VBQ0EsZ0JBQUE7RUFDQSxvQkFBQTtFQUNBLHFCQUFBO0VBQ0EsWUFBQTtFQUNBLHVCQUFBO0VBQ0EsaUJBQUE7RUFDQSxtQkFBQTtFQUNBLFdBQUE7QUE3QkoiLCJmaWxlIjoic3JjL2FwcC9wYWdlcy90YWJzL2hvbWUvaHlwZXJ0cm9waHktc3RyZW5ndGhlbmluZy1wcm9ncmFtLWNyZWF0aW9uL2h5cGVydHJvcGh5LXN0cmVuZ3RoZW5pbmctcHJvZ3JhbS1jcmVhdGlvbi5wYWdlLnNjc3MiLCJzb3VyY2VzQ29udGVudCI6WyJcblxuLnJvdy13aWR0aC11bmRlcmxpbmUge1xuICAgIHdpZHRoOiAxMDAlO1xuICAgIGJvcmRlci1ib3R0b206IDFweCBzb2xpZCBncmF5ICFpbXBvcnRhbnQ7XG59XG5cbmlvbi1jb250ZW50IHtcbiAgICAtLXBhZGRpbmctdG9wOiAxMCU7XG4gICAgLS1wYWRkaW5nLXN0YXJ0OiAzJTtcbiAgICAtLXBhZGRpbmctYm90dG9tOiAyMCU7XG59XG5cbi5zbGlkZS1pbWFnZSBpbWcge1xuXG4gICAgaGVpZ2h0OiAxMjdweDtcblxufVxuXG4uc2xpZGVfc2VjdGlvbiB7XG4gICAgbWFyZ2luLXRvcDogMyU7XG59XG5cblxuXG4uc2VsZWN0X3Jvd19zZWN0aW9uIHtcbiAgICBtYXJnaW4tdG9wOiAxMCU7XG59XG5cbi5idG5fc2VjdGlvbiB7XG4gICAgbWFyZ2luLXRvcDogMTAlO1xufVxuXG4uY2FyZF9idG4ge1xuICAgIGhlaWdodDogNzBweDtcbiAgICB3aWR0aDogNzBweDtcbiAgICBib3JkZXI6IDJweCBzb2xpZCAjNTY1NjU2O1xuICAgIG1hcmdpbjogMDtcbiAgICBkaXNwbGF5OiBmbGV4O1xuICAgIGp1c3RpZnktY29udGVudDogY2VudGVyO1xuXG4gICAgaW9uLWxhYmVsIHtcbiAgICAgICAgYWxpZ24tc2VsZjogY2VudGVyO1xuICAgICAgICBmb250LXNpemU6IDEuMnJlbTtcbiAgICAgICAgZm9udC13ZWlnaHQ6IDYwMDtcbiAgICAgICAgY29sb3I6ICMwMDA7XG4gICAgfVxufVxuXG5cbi5idG5TbGlkZXMge1xuICAgIGlvbi1zbGlkZSB7XG4gICAgICAgIHdpZHRoOiA4MHB4O1xuICAgICAgICBtYXJnaW4tcmlnaHQ6IDEwcHg7XG4gICAgfVxufVxuXG5cbi5wcm9maWxlLXZpZXdhbGwge1xuICAgIHdpZHRoOiAxMDAlO1xuICAgIHBvc2l0aW9uOiByZWxhdGl2ZTtcbiAgICBtYXJnaW4tdG9wOiAxNXB4O1xuICAgIG1hcmdpbi1ib3R0b206IDE1cHg7XG59XG5cbi5wcm9maWxlLXZpZXdhbGwgaW9uLWJ1dHRvbiB7XG4gICAgLS1ib3JkZXItcmFkaXVzOiAyNXB4O1xuICAgIGRpc3BsYXk6IHRhYmxlO1xuICAgIG1hcmdpbjogYXV0bztcbiAgICBmb250LXNpemU6IDEycHg7XG59XG5cbi5wcm9maWxlLXZpZXdhbGw6YmVmb3JlIHtcbiAgICBjb250ZW50OiBcIlwiO1xuICAgIHBvc2l0aW9uOiBhYnNvbHV0ZTtcbiAgICBiYWNrZ3JvdW5kOiAjYzFjMWMxO1xuICAgIGhlaWdodDogMXB4O1xuICAgIHdpZHRoOiAxMDAlO1xuICAgIHRvcDogMTlweDtcbiAgICBsZWZ0OiAwO1xufVxuXG5cbmlvbi1yb3cge1xuICAgIC8vIHBhZGRpbmctbGVmdDogNSU7XG59XG5cblxuLmJ0bl91bmRlcmxpbmUge1xuICAgIG1hcmdpbi10b3A6IDEwJTtcbn1cblxuXG4uYWRkX2J0bl9jb2wge1xuXG4gICAgZGlzcGxheTogZmxleDtcbiAgICBqdXN0aWZ5LWNvbnRlbnQ6IGNlbnRlcjtcbn1cbmlvbi1pdGVtIHtcbiAgICB3aWR0aDogMTAwJTtcbiAgICBoZWlnaHQ6IDQwcHg7XG4gICAgZGlzcGxheTogZmxleDtcbn1cblxuaW9uLXJlb3JkZXItZ3JvdXAge1xuICAgIHdpZHRoOiAxMDAlO1xufVxuXG4uam91cl9pbWcge1xuICAgIHdpZHRoOiAxMDBweDtcbn1cblxuLmV4ZXJjaXNlLWRldGFpbHMtc2VjdGlvbiB7XG4gICAgaGVpZ2h0OiAzNXB4O1xuICAgIG1hcmdpbi10b3A6IC04cHhcbn1cblxuXG4uZGV0YWlsX2V4Y2VyY2lzZV9jb2wge1xuICAgIGFsaWduLXNlbGY6IGNlbnRlcjtcbiAgICBwYWRkaW5nOiAwXG59XG5cblxuLnRvb2dsZV9jb2xfc2VjdGlvbiB7XG4gICAgZGlzcGxheTogZmxleDtcbiAgICBqdXN0aWZ5LWNvbnRlbnQ6IGZsZXgtZW5kXG59XG5cblxuLnJlb3JkZXJfbGlzdF9kaXZfc2VjdGlvbiB7XG4gICAgd2lkdGg6IDUwcHg7XG4gICAgYmFja2dyb3VuZDogIzAwMDtcbiAgICBoZWlnaHQ6IDM1cHhcbn1cblxuXG4ucmVvcmRlcl9saXN0X3RpdGxlIHtcbiAgICBwYWRkaW5nLWxlZnQ6IDEwcHhcbn1cblxuLmJpc2V0X3NlY3Rpb25fYmlnIHtcbiAgICB3aWR0aDogMTAwJTtcbiAgICBqdXN0aWZ5LWNvbnRlbnQ6IGNlbnRlcjtcbiAgICBiYWNrZ3JvdW5kOiByZ2IoMjMxLCAyMzAsIDIzMCk7XG4gICAgYm9yZGVyLXRvcC1sZWZ0LXJhZGl1czogN3B4O1xuICAgIGJvcmRlci10b3AtcmlnaHQtcmFkaXVzOiA3cHg7XG4gICAgbWFyZ2luLWJvdHRvbTogMnB4O1xufVxuXG4uYmlzZXRfc2VjdGlvbl9maXJzdF9yb3cge1xuICAgIGJhY2tncm91bmQ6IHJnYigyMzEsIDIzMCwgMjMwKTtcbiAgICB3aWR0aDogMTAwJTtcbiAgICBwYWRkaW5nOiA1cHhcbn1cblxuLmJpc2V0X3NlY3Rpb25faW9uX2l0ZW0ge1xuICAgIC0tYmFja2dyb3VuZDogdHJhbnNwYXJlbnRcbn1cblxuLmJpc2V0X3NlY3Rpb25fc2Vjb25kX3JvdyB7XG4gICAgd2lkdGg6IDEwMCVcbn1cblxuLmJpc2V0X3NlY3Rpb25fYnRuX29uZSB7XG4gICAgaGVpZ2h0OiAyN3B4O1xuICAgIC0tYm9yZGVyLXJhZGl1czogMTVweFxufVxuXG5cbi5iaXNldF9zZWN0aW9uX2J0bl9yb3cge1xuICAgIHdpZHRoOiAxMDAlO1xuICAgIGp1c3RpZnktY29udGVudDogY2VudGVyO1xuICAgIGJhY2tncm91bmQ6IHJnYigyMzEsIDIzMCwgMjMwKTtcbiAgICBtYXJnaW4tdG9wOiA1cHg7XG4gICAgYm9yZGVyLWJvdHRvbS1sZWZ0LXJhZGl1czogN3B4O1xuICAgIGJvcmRlci1ib3R0b20tcmlnaHQtcmFkaXVzOiA3cHg7XG59XG5cbi5mb290ZXJfc2VjdGlvbiB7XG4gICAgd2lkdGg6IDEwMCU7XG4gICAgcG9zaXRpb246IGZpeGVkO1xuICAgIGJvdHRvbTogMTBweFxuICBcbn1cblxuXG4uZm9vdGVyX3NlY3Rpb25fYnRuIHtcbiAgICBcbiAgICB3aWR0aDogMTAwJTtcbiAgICBwYWRkaW5nLWxlZnQ6IDEwcHg7XG4gICAgcGFkZGluZy1yaWdodDogMTBweDtcbiAgICBtYXJnaW4tdG9wOiAzMnB4O1xuICBcbn1cblxuXG5cbi5hY3RpdmVfYnRuIHtcbiAgICBiYWNrZ3JvdW5kOiB2YXIoLS1pb24tYnRuLWN1c3RvbS1jb2xvcik7XG59XG5cbmlvbi1zZWxlY3Q6OnBhcnQodGV4dCkge1xuICAgIGZvbnQtc2l6ZTogMC44cmVtICFpbXBvcnRhbnQ7XG4gICAgZm9udC1mYW1pbHk6ICdPc3dhbGQnICFpbXBvcnRhbnQ7XG4gIH1cblxuICAubmV4dCB7XG4gICAgLS1iYWNrZ3JvdW5kOiB2YXIoLS1pb24tYnRuLWN1c3RvbS1jb2xvcik7XG4gICAgLS1pb24tYnV0dG9uLXJvdW5kLWJvcmRlci1yYWRpdXM6IDVweDtcbiAgICAtLWlvbi1idXR0b24tYm9yZGVyLXJhZGl1czogNXB4O1xuICAgIC0taW9uLWJvcmRlci1yYWRpdXM6IDVweDtcbiAgICAtLWJvcmRlci1yYWRpdXM6IDVweDtcbiAgfVxuXG4gIC5zZWxlY3RfYm9keV9zZWN0aW9uIHtcbiAgICAgIFxuICAgIG1hcmdpbi10b3A6IDUlO1xuICAgIG1hcmdpbi1yaWdodDogNCU7XG4gICAgYm9yZGVyLXN0eWxlOiBkb3R0ZWQ7XG4gICAgYm9yZGVyLWNvbG9yOiAjNjk2OTY5O1xuICAgIHBhZGRpbmc6IDVweDtcbiAgICBqdXN0aWZ5LWNvbnRlbnQ6IGNlbnRlcjtcbiAgICBib3JkZXItd2lkdGg6IDJweDtcbiAgICBib3JkZXItcmFkaXVzOiAxMHB4O1xuICAgIHdpZHRoOiAxMDAlO1xufSJdfQ== */");

/***/ }),

/***/ "./src/app/pages/tabs/home/hypertrophy-strengthening-program-creation/hypertrophy-strengthening-program-creation.page.ts":
/*!*******************************************************************************************************************************!*\
  !*** ./src/app/pages/tabs/home/hypertrophy-strengthening-program-creation/hypertrophy-strengthening-program-creation.page.ts ***!
  \*******************************************************************************************************************************/
/*! exports provided: HypertrophyStrengtheningProgramCreationPage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "HypertrophyStrengtheningProgramCreationPage", function() { return HypertrophyStrengtheningProgramCreationPage; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");


let HypertrophyStrengtheningProgramCreationPage = class HypertrophyStrengtheningProgramCreationPage {
    constructor() {
        this.showListItem = false;
        this.slideOptions = {
            // centeredSlides: true,
            slidesPerView: 2.5,
            spaceBetween: 10,
        };
        this.btnSlideOptions = {
            // centeredSlides: true,
            slidesPerView: 3,
            spaceBetween: 5,
        };
        this.objectif = "Hypertrophie";
        this.format_dentrainement = "SPLIT";
        this.training_days = "3";
        this.progression_method = "Aucune";
        this.start_the_program = "Aujourd’hui";
        this.btnArray = [
            {
                text: 1,
                active: true
            },
            {
                text: 2,
                active: false
            },
            {
                text: 3,
                active: false
            },
        ];
        this.filteredData = {};
    }
    ngOnInit() {
        this.btnArray.map((doc) => {
            if (doc.active) {
                this.filteredData = doc;
            }
        });
        console.log(this.filteredData);
    }
    toggleChange(event) {
        console.log(event);
        this.showListItem = event.detail.checked;
    }
    addBtn(slides) {
        if (!(this.btnArray.length >= 7)) {
            let text = 2;
            let arr = this.btnArray[this.btnArray.length - 1];
            let obj = { text: arr.text + 1, active: false };
            this.btnArray.push(obj);
            slides.update();
            slides.slideTo(this.btnArray.length + 1);
            setTimeout(() => {
                slides.slideTo(this.btnArray.length + 2);
            });
        }
    }
    onItemReorder({ detail }) {
        detail.complete(true);
    }
    makeActive(item) {
        console.log(item);
        item.active = true;
        let filteredData = [];
        this.btnArray.forEach((doc) => {
            if (item.text != doc.text) {
                doc.active = false;
            }
            filteredData.push(doc);
        });
        this.btnArray = filteredData;
        this.btnArray.map((doc) => {
            if (doc.active) {
                this.filteredData = doc;
            }
        });
    }
};
HypertrophyStrengtheningProgramCreationPage.ctorParameters = () => [];
HypertrophyStrengtheningProgramCreationPage = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: "app-hypertrophy-strengthening-program-creation",
        template: Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(/*! raw-loader!./hypertrophy-strengthening-program-creation.page.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/pages/tabs/home/hypertrophy-strengthening-program-creation/hypertrophy-strengthening-program-creation.page.html")).default,
        styles: [Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(/*! ./hypertrophy-strengthening-program-creation.page.scss */ "./src/app/pages/tabs/home/hypertrophy-strengthening-program-creation/hypertrophy-strengthening-program-creation.page.scss")).default]
    })
], HypertrophyStrengtheningProgramCreationPage);



/***/ })

}]);
//# sourceMappingURL=pages-tabs-home-hypertrophy-strengthening-program-creation-hypertrophy-strengthening-program-creation-module-es2015.js.map