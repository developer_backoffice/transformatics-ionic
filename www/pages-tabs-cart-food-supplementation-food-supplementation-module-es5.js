(function () {
  function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

  function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

  function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

  (window["webpackJsonp"] = window["webpackJsonp"] || []).push([["pages-tabs-cart-food-supplementation-food-supplementation-module"], {
    /***/
    "./node_modules/raw-loader/dist/cjs.js!./src/app/pages/tabs/cart/food-supplementation/food-supplementation.page.html":
    /*!***************************************************************************************************************************!*\
      !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/pages/tabs/cart/food-supplementation/food-supplementation.page.html ***!
      \***************************************************************************************************************************/

    /*! exports provided: default */

    /***/
    function node_modulesRawLoaderDistCjsJsSrcAppPagesTabsCartFoodSupplementationFoodSupplementationPageHtml(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony default export */


      __webpack_exports__["default"] = "<ion-content>\n    <div class=\"banner\">\n        <img src=\"assets/images/metrialstatus.png\">\n    </div>\n    <ion-grid>\n        <ion-row>\n            <ion-col size=12>\n                <div class=\"ebook-title\">\n                    <h4>ALIMENTATION & COMPLEMENTATION</h4>\n                </div>\n            </ion-col>\n\n            <ion-col size=\"4\">\n                <div class=\"brand-logo\">\n                    <img src=\"assets/icon/brand/logo6.jpg\">\n                </div>\n            </ion-col>\n            <ion-col size=\"4\">\n                <div class=\"brand-logo\">\n                    <img src=\"assets/icon/brand/logo7.png\">\n                </div>\n            </ion-col>\n            <ion-col size=\"4\">\n                <div class=\"brand-logo\">\n                    <img src=\"assets/icon/brand/logo8.png\">\n                </div>\n            </ion-col>\n        </ion-row>\n    </ion-grid>\n</ion-content>";
      /***/
    },

    /***/
    "./src/app/pages/tabs/cart/food-supplementation/food-supplementation-routing.module.ts":
    /*!*********************************************************************************************!*\
      !*** ./src/app/pages/tabs/cart/food-supplementation/food-supplementation-routing.module.ts ***!
      \*********************************************************************************************/

    /*! exports provided: FoodSupplementationPageRoutingModule */

    /***/
    function srcAppPagesTabsCartFoodSupplementationFoodSupplementationRoutingModuleTs(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "FoodSupplementationPageRoutingModule", function () {
        return FoodSupplementationPageRoutingModule;
      });
      /* harmony import */


      var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! tslib */
      "./node_modules/tslib/tslib.es6.js");
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! @angular/core */
      "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
      /* harmony import */


      var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! @angular/router */
      "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");
      /* harmony import */


      var _food_supplementation_page__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
      /*! ./food-supplementation.page */
      "./src/app/pages/tabs/cart/food-supplementation/food-supplementation.page.ts");

      var routes = [{
        path: '',
        component: _food_supplementation_page__WEBPACK_IMPORTED_MODULE_3__["FoodSupplementationPage"]
      }];

      var FoodSupplementationPageRoutingModule = function FoodSupplementationPageRoutingModule() {
        _classCallCheck(this, FoodSupplementationPageRoutingModule);
      };

      FoodSupplementationPageRoutingModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]]
      })], FoodSupplementationPageRoutingModule);
      /***/
    },

    /***/
    "./src/app/pages/tabs/cart/food-supplementation/food-supplementation.module.ts":
    /*!*************************************************************************************!*\
      !*** ./src/app/pages/tabs/cart/food-supplementation/food-supplementation.module.ts ***!
      \*************************************************************************************/

    /*! exports provided: FoodSupplementationPageModule */

    /***/
    function srcAppPagesTabsCartFoodSupplementationFoodSupplementationModuleTs(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "FoodSupplementationPageModule", function () {
        return FoodSupplementationPageModule;
      });
      /* harmony import */


      var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! tslib */
      "./node_modules/tslib/tslib.es6.js");
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! @angular/core */
      "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
      /* harmony import */


      var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! @angular/common */
      "./node_modules/@angular/common/__ivy_ngcc__/fesm2015/common.js");
      /* harmony import */


      var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
      /*! @angular/forms */
      "./node_modules/@angular/forms/__ivy_ngcc__/fesm2015/forms.js");
      /* harmony import */


      var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
      /*! @ionic/angular */
      "./node_modules/@ionic/angular/__ivy_ngcc__/fesm2015/ionic-angular.js");
      /* harmony import */


      var _food_supplementation_routing_module__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(
      /*! ./food-supplementation-routing.module */
      "./src/app/pages/tabs/cart/food-supplementation/food-supplementation-routing.module.ts");
      /* harmony import */


      var _food_supplementation_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(
      /*! ./food-supplementation.page */
      "./src/app/pages/tabs/cart/food-supplementation/food-supplementation.page.ts");

      var FoodSupplementationPageModule = function FoodSupplementationPageModule() {
        _classCallCheck(this, FoodSupplementationPageModule);
      };

      FoodSupplementationPageModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"], _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"], _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonicModule"], _food_supplementation_routing_module__WEBPACK_IMPORTED_MODULE_5__["FoodSupplementationPageRoutingModule"]],
        declarations: [_food_supplementation_page__WEBPACK_IMPORTED_MODULE_6__["FoodSupplementationPage"]]
      })], FoodSupplementationPageModule);
      /***/
    },

    /***/
    "./src/app/pages/tabs/cart/food-supplementation/food-supplementation.page.scss":
    /*!*************************************************************************************!*\
      !*** ./src/app/pages/tabs/cart/food-supplementation/food-supplementation.page.scss ***!
      \*************************************************************************************/

    /*! exports provided: default */

    /***/
    function srcAppPagesTabsCartFoodSupplementationFoodSupplementationPageScss(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony default export */


      __webpack_exports__["default"] = "ion-content {\n  --background: #000;\n}\n\n.ebook-title h4 {\n  color: #fff;\n  text-align: center;\n  font-size: 15px;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvcGFnZXMvdGFicy9jYXJ0L2Zvb2Qtc3VwcGxlbWVudGF0aW9uL2Zvb2Qtc3VwcGxlbWVudGF0aW9uLnBhZ2Uuc2NzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQTtFQUNJLGtCQUFBO0FBQ0o7O0FBRUE7RUFDSSxXQUFBO0VBQ0Esa0JBQUE7RUFDQSxlQUFBO0FBQ0oiLCJmaWxlIjoic3JjL2FwcC9wYWdlcy90YWJzL2NhcnQvZm9vZC1zdXBwbGVtZW50YXRpb24vZm9vZC1zdXBwbGVtZW50YXRpb24ucGFnZS5zY3NzIiwic291cmNlc0NvbnRlbnQiOlsiaW9uLWNvbnRlbnQge1xuICAgIC0tYmFja2dyb3VuZDogIzAwMDtcbn1cblxuLmVib29rLXRpdGxlIGg0IHtcbiAgICBjb2xvcjogI2ZmZjtcbiAgICB0ZXh0LWFsaWduOiBjZW50ZXI7XG4gICAgZm9udC1zaXplOiAxNXB4O1xufSJdfQ== */";
      /***/
    },

    /***/
    "./src/app/pages/tabs/cart/food-supplementation/food-supplementation.page.ts":
    /*!***********************************************************************************!*\
      !*** ./src/app/pages/tabs/cart/food-supplementation/food-supplementation.page.ts ***!
      \***********************************************************************************/

    /*! exports provided: FoodSupplementationPage */

    /***/
    function srcAppPagesTabsCartFoodSupplementationFoodSupplementationPageTs(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "FoodSupplementationPage", function () {
        return FoodSupplementationPage;
      });
      /* harmony import */


      var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! tslib */
      "./node_modules/tslib/tslib.es6.js");
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! @angular/core */
      "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");

      var FoodSupplementationPage = /*#__PURE__*/function () {
        function FoodSupplementationPage() {
          _classCallCheck(this, FoodSupplementationPage);
        }

        _createClass(FoodSupplementationPage, [{
          key: "ngOnInit",
          value: function ngOnInit() {}
        }]);

        return FoodSupplementationPage;
      }();

      FoodSupplementationPage.ctorParameters = function () {
        return [];
      };

      FoodSupplementationPage = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-food-supplementation',
        template: Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(
        /*! raw-loader!./food-supplementation.page.html */
        "./node_modules/raw-loader/dist/cjs.js!./src/app/pages/tabs/cart/food-supplementation/food-supplementation.page.html"))["default"],
        styles: [Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(
        /*! ./food-supplementation.page.scss */
        "./src/app/pages/tabs/cart/food-supplementation/food-supplementation.page.scss"))["default"]]
      })], FoodSupplementationPage);
      /***/
    }
  }]);
})();
//# sourceMappingURL=pages-tabs-cart-food-supplementation-food-supplementation-module-es5.js.map