(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["pages-tabs-home-conseils-nutritionnels-details-conseils-nutritionnels-details-module"],{

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/pages/tabs/home/conseils-nutritionnels-details/conseils-nutritionnels-details.page.html":
/*!***********************************************************************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/pages/tabs/home/conseils-nutritionnels-details/conseils-nutritionnels-details.page.html ***!
  \***********************************************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<ion-content>\n    <div class=\"titlebox\">\n        <div class=\"title-img\">\n            <img src=\"assets/images/food.png\">\n        </div>\n        <div class=\"titlebox-text\">\n            <h4>Le gingembre</h4>\n            <h5>et son influence sur la santé globale des individus</h5>\n        </div>\n    </div>\n    <ion-grid>\n        <ion-row>\n            <ion-col size=\"12\">\n                <p>\n                    Le gingembre est une espèce de plantes originaire d'Inde, du genre Zingiber et de la famille des Zingiberaceae dont on utilise le rhizome en cuisine et en médecine traditionnelle.\n                </p>\n                <p>\n                    Ce rhizome est une épice très employée dans un grand nombre de cuisines asiatiques, et en particulier dans la cuisine indienne. Il est aussi utilisé en Occident dans la confection du ginger ale et de desserts comme le pain d'épices.\n                </p>\n                <p>\n                    L'huile essentielle de gingembre est obtenue par distillation à la vapeur d'eau des rhizomes. Il faut environ 50 kg de rhizomes secs pour obtenir 1 kg d'huile essentielle. L'huile essentielle de gingembre est notamment réputée pour ses vertus digestives.\n                    Elle est supposée « stimuler et accélérer le passage du bol alimentaire »\n                </p>\n            </ion-col>\n        </ion-row>\n    </ion-grid>\n</ion-content>");

/***/ }),

/***/ "./src/app/pages/tabs/home/conseils-nutritionnels-details/conseils-nutritionnels-details-routing.module.ts":
/*!*****************************************************************************************************************!*\
  !*** ./src/app/pages/tabs/home/conseils-nutritionnels-details/conseils-nutritionnels-details-routing.module.ts ***!
  \*****************************************************************************************************************/
/*! exports provided: ConseilsNutritionnelsDetailsPageRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ConseilsNutritionnelsDetailsPageRoutingModule", function() { return ConseilsNutritionnelsDetailsPageRoutingModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");
/* harmony import */ var _conseils_nutritionnels_details_page__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./conseils-nutritionnels-details.page */ "./src/app/pages/tabs/home/conseils-nutritionnels-details/conseils-nutritionnels-details.page.ts");




const routes = [
    {
        path: '',
        component: _conseils_nutritionnels_details_page__WEBPACK_IMPORTED_MODULE_3__["ConseilsNutritionnelsDetailsPage"]
    }
];
let ConseilsNutritionnelsDetailsPageRoutingModule = class ConseilsNutritionnelsDetailsPageRoutingModule {
};
ConseilsNutritionnelsDetailsPageRoutingModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]],
    })
], ConseilsNutritionnelsDetailsPageRoutingModule);



/***/ }),

/***/ "./src/app/pages/tabs/home/conseils-nutritionnels-details/conseils-nutritionnels-details.module.ts":
/*!*********************************************************************************************************!*\
  !*** ./src/app/pages/tabs/home/conseils-nutritionnels-details/conseils-nutritionnels-details.module.ts ***!
  \*********************************************************************************************************/
/*! exports provided: ConseilsNutritionnelsDetailsPageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ConseilsNutritionnelsDetailsPageModule", function() { return ConseilsNutritionnelsDetailsPageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/__ivy_ngcc__/fesm2015/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/__ivy_ngcc__/fesm2015/forms.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/__ivy_ngcc__/fesm2015/ionic-angular.js");
/* harmony import */ var _conseils_nutritionnels_details_routing_module__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./conseils-nutritionnels-details-routing.module */ "./src/app/pages/tabs/home/conseils-nutritionnels-details/conseils-nutritionnels-details-routing.module.ts");
/* harmony import */ var _conseils_nutritionnels_details_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./conseils-nutritionnels-details.page */ "./src/app/pages/tabs/home/conseils-nutritionnels-details/conseils-nutritionnels-details.page.ts");







let ConseilsNutritionnelsDetailsPageModule = class ConseilsNutritionnelsDetailsPageModule {
};
ConseilsNutritionnelsDetailsPageModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [
            _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonicModule"],
            _conseils_nutritionnels_details_routing_module__WEBPACK_IMPORTED_MODULE_5__["ConseilsNutritionnelsDetailsPageRoutingModule"]
        ],
        declarations: [_conseils_nutritionnels_details_page__WEBPACK_IMPORTED_MODULE_6__["ConseilsNutritionnelsDetailsPage"]]
    })
], ConseilsNutritionnelsDetailsPageModule);



/***/ }),

/***/ "./src/app/pages/tabs/home/conseils-nutritionnels-details/conseils-nutritionnels-details.page.scss":
/*!*********************************************************************************************************!*\
  !*** ./src/app/pages/tabs/home/conseils-nutritionnels-details/conseils-nutritionnels-details.page.scss ***!
  \*********************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = (".titlebox {\n  position: relative;\n  color: #fff;\n}\n\n.titlebox-text {\n  position: absolute;\n  bottom: 5%;\n  left: 5%;\n}\n\n.title-img {\n  background: #000;\n}\n\n.titlebox-text h5 {\n  font-size: 12px;\n  margin: 0;\n}\n\n.titlebox {\n  position: relative;\n  color: #fff;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvcGFnZXMvdGFicy9ob21lL2NvbnNlaWxzLW51dHJpdGlvbm5lbHMtZGV0YWlscy9jb25zZWlscy1udXRyaXRpb25uZWxzLWRldGFpbHMucGFnZS5zY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBO0VBQ0ksa0JBQUE7RUFDQSxXQUFBO0FBQ0o7O0FBRUE7RUFDSSxrQkFBQTtFQUNBLFVBQUE7RUFDQSxRQUFBO0FBQ0o7O0FBRUE7RUFDSSxnQkFBQTtBQUNKOztBQUVBO0VBQ0ksZUFBQTtFQUNBLFNBQUE7QUFDSjs7QUFFQTtFQUNJLGtCQUFBO0VBQ0EsV0FBQTtBQUNKIiwiZmlsZSI6InNyYy9hcHAvcGFnZXMvdGFicy9ob21lL2NvbnNlaWxzLW51dHJpdGlvbm5lbHMtZGV0YWlscy9jb25zZWlscy1udXRyaXRpb25uZWxzLWRldGFpbHMucGFnZS5zY3NzIiwic291cmNlc0NvbnRlbnQiOlsiLnRpdGxlYm94IHtcbiAgICBwb3NpdGlvbjogcmVsYXRpdmU7XG4gICAgY29sb3I6ICNmZmY7XG59XG5cbi50aXRsZWJveC10ZXh0IHtcbiAgICBwb3NpdGlvbjogYWJzb2x1dGU7XG4gICAgYm90dG9tOiA1JTtcbiAgICBsZWZ0OiA1JTtcbn1cblxuLnRpdGxlLWltZyB7XG4gICAgYmFja2dyb3VuZDogIzAwMDtcbn1cblxuLnRpdGxlYm94LXRleHQgaDUge1xuICAgIGZvbnQtc2l6ZTogMTJweDtcbiAgICBtYXJnaW46IDA7XG59XG5cbi50aXRsZWJveCB7XG4gICAgcG9zaXRpb246IHJlbGF0aXZlO1xuICAgIGNvbG9yOiAjZmZmO1xufSJdfQ== */");

/***/ }),

/***/ "./src/app/pages/tabs/home/conseils-nutritionnels-details/conseils-nutritionnels-details.page.ts":
/*!*******************************************************************************************************!*\
  !*** ./src/app/pages/tabs/home/conseils-nutritionnels-details/conseils-nutritionnels-details.page.ts ***!
  \*******************************************************************************************************/
/*! exports provided: ConseilsNutritionnelsDetailsPage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ConseilsNutritionnelsDetailsPage", function() { return ConseilsNutritionnelsDetailsPage; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");


let ConseilsNutritionnelsDetailsPage = class ConseilsNutritionnelsDetailsPage {
    constructor() { }
    ngOnInit() {
    }
};
ConseilsNutritionnelsDetailsPage.ctorParameters = () => [];
ConseilsNutritionnelsDetailsPage = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-conseils-nutritionnels-details',
        template: Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(/*! raw-loader!./conseils-nutritionnels-details.page.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/pages/tabs/home/conseils-nutritionnels-details/conseils-nutritionnels-details.page.html")).default,
        styles: [Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(/*! ./conseils-nutritionnels-details.page.scss */ "./src/app/pages/tabs/home/conseils-nutritionnels-details/conseils-nutritionnels-details.page.scss")).default]
    })
], ConseilsNutritionnelsDetailsPage);



/***/ })

}]);
//# sourceMappingURL=pages-tabs-home-conseils-nutritionnels-details-conseils-nutritionnels-details-module-es2015.js.map