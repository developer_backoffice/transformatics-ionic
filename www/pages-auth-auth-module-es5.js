(function () {
  function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

  function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

  function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

  (window["webpackJsonp"] = window["webpackJsonp"] || []).push([["pages-auth-auth-module"], {
    /***/
    "./node_modules/raw-loader/dist/cjs.js!./src/app/pages/auth/auth.page.html":
    /*!*********************************************************************************!*\
      !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/pages/auth/auth.page.html ***!
      \*********************************************************************************/

    /*! exports provided: default */

    /***/
    function node_modulesRawLoaderDistCjsJsSrcAppPagesAuthAuthPageHtml(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony default export */


      __webpack_exports__["default"] = "<ion-header class=\"ion-no-border\">\n  <!-- <ion-toolbar mode=\"ios\">\n    <ion-buttons slot=\"start\" (click)=\"goBack()\">\n      <ion-button>\n        <ion-icon class=\"back-icon\" name=\"chevron-back-outline\"></ion-icon>\n      </ion-button>\n    </ion-buttons>\n    <ion-title>Transformatics</ion-title>\n  </ion-toolbar> -->\n\n  <div class=\"header-img\">\n    <ion-buttons slot=\"start\" (click)=\"goBack()\">\n      <ion-button>\n        <ion-icon class=\"back-icon\" name=\"chevron-back-outline\"></ion-icon>\n      </ion-button>\n    </ion-buttons>\n    <img src=\"assets/icon/header.png\" />\n  </div>\n\n\n</ion-header>\n\n<ion-content>\n  <div class=\"main-container\">\n    <div class=\"segment_container\">\n      <ion-segment value=\"login\" color=\"tertiary\" scrollable=\"true\" [(ngModel)]=\"segmentModel\"\n        (ionChange)=\"segmentChanged($event)\">\n        <ion-segment-button value=\"login\">\n          <ion-label class=\"segment-btn-text\">CONNEXION</ion-label>\n        </ion-segment-button>\n\n        <ion-segment-button value=\"register\">\n          <ion-label class=\"segment-btn-text\">INSCRIPTION</ion-label>\n        </ion-segment-button>\n      </ion-segment>\n    </div>\n  </div>\n\n  <div class=\"segment_container_data\">\n    <div *ngIf=\"segmentModel === 'login'\">\n      <form [formGroup]=\"loginForm\">\n\n        <ion-item class=\"input_container\">\n          <ion-icon class=\"input-icon\" name=\"mail-outline\"></ion-icon>\n          <ion-input placeholder=\"Adresse mail\" formControlName=\"email\"></ion-input>\n        </ion-item>\n\n        <ion-item class=\"input_container\">\n          <ion-icon class=\"input-icon\" name=\"key-outline\"></ion-icon>\n\n          <ion-input type=\"password\" placeholder=\"Mot de passe\" formControlName=\"password\"></ion-input>\n        </ion-item>\n\n\n        <ion-button class=\"submit-btn\" (click)=\"goToScanArrival()\"> S’IDENTIFIER </ion-button>\n      </form>\n    </div>\n\n    <div *ngIf=\"segmentModel === 'register'\">\n      <form [formGroup]=\"registrationForm\">\n        <!-- Input with placeholder -->\n        <ion-item class=\"input_container\">\n          <ion-input placeholder=\"Nom\" formControlName=\"last_name\"></ion-input>\n        </ion-item>\n\n        <ion-item class=\"input_container\">\n          <ion-input placeholder=\"Prénom\" formControlName=\"first_name\"></ion-input>\n        </ion-item>\n\n        <ion-item class=\"input_container\">\n          <ion-icon class=\"input-icon\" name=\"mail-outline\"></ion-icon>\n          <ion-input placeholder=\"Adresse mail\" formControlName=\"email\"></ion-input>\n        </ion-item>\n\n        <ion-item class=\"input_container\">\n          <ion-icon class=\"input-icon\" name=\"key-outline\"></ion-icon>\n\n          <ion-input type=\"password\" placeholder=\"Mot de passe\" formControlName=\"password\"></ion-input>\n        </ion-item>\n\n        <ion-item class=\"input_container\">\n          <ion-icon class=\"input-icon\" name=\"key-outline\"></ion-icon>\n\n          <ion-input type=\"password\" placeholder=\"Répétez le mot de passe\" formControlName=\"confirm_password\">\n          </ion-input>\n        </ion-item>\n\n        <ion-button class=\"submit-btn\" (click)=\"registerAndRedirect()\"> S’INSCRIRE </ion-button>\n      </form>\n    </div>\n  </div>\n</ion-content>";
      /***/
    },

    /***/
    "./src/app/pages/auth/auth-routing.module.ts":
    /*!***************************************************!*\
      !*** ./src/app/pages/auth/auth-routing.module.ts ***!
      \***************************************************/

    /*! exports provided: AuthPageRoutingModule */

    /***/
    function srcAppPagesAuthAuthRoutingModuleTs(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "AuthPageRoutingModule", function () {
        return AuthPageRoutingModule;
      });
      /* harmony import */


      var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! tslib */
      "./node_modules/tslib/tslib.es6.js");
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! @angular/core */
      "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
      /* harmony import */


      var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! @angular/router */
      "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");
      /* harmony import */


      var _auth_page__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
      /*! ./auth.page */
      "./src/app/pages/auth/auth.page.ts");

      var routes = [{
        path: '',
        component: _auth_page__WEBPACK_IMPORTED_MODULE_3__["AuthPage"]
      }];

      var AuthPageRoutingModule = function AuthPageRoutingModule() {
        _classCallCheck(this, AuthPageRoutingModule);
      };

      AuthPageRoutingModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]]
      })], AuthPageRoutingModule);
      /***/
    },

    /***/
    "./src/app/pages/auth/auth.module.ts":
    /*!*******************************************!*\
      !*** ./src/app/pages/auth/auth.module.ts ***!
      \*******************************************/

    /*! exports provided: AuthPageModule */

    /***/
    function srcAppPagesAuthAuthModuleTs(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "AuthPageModule", function () {
        return AuthPageModule;
      });
      /* harmony import */


      var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! tslib */
      "./node_modules/tslib/tslib.es6.js");
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! @angular/core */
      "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
      /* harmony import */


      var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! @angular/common */
      "./node_modules/@angular/common/__ivy_ngcc__/fesm2015/common.js");
      /* harmony import */


      var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
      /*! @angular/forms */
      "./node_modules/@angular/forms/__ivy_ngcc__/fesm2015/forms.js");
      /* harmony import */


      var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
      /*! @ionic/angular */
      "./node_modules/@ionic/angular/__ivy_ngcc__/fesm2015/ionic-angular.js");
      /* harmony import */


      var _auth_routing_module__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(
      /*! ./auth-routing.module */
      "./src/app/pages/auth/auth-routing.module.ts");
      /* harmony import */


      var _auth_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(
      /*! ./auth.page */
      "./src/app/pages/auth/auth.page.ts");

      var AuthPageModule = function AuthPageModule() {
        _classCallCheck(this, AuthPageModule);
      };

      AuthPageModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"], _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"], _angular_forms__WEBPACK_IMPORTED_MODULE_3__["ReactiveFormsModule"], _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonicModule"], _auth_routing_module__WEBPACK_IMPORTED_MODULE_5__["AuthPageRoutingModule"]],
        declarations: [_auth_page__WEBPACK_IMPORTED_MODULE_6__["AuthPage"]]
      })], AuthPageModule);
      /***/
    },

    /***/
    "./src/app/pages/auth/auth.page.scss":
    /*!*******************************************!*\
      !*** ./src/app/pages/auth/auth.page.scss ***!
      \*******************************************/

    /*! exports provided: default */

    /***/
    function srcAppPagesAuthAuthPageScss(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony default export */


      __webpack_exports__["default"] = "ion-content {\n  --background: #000;\n}\n\nion-header {\n  background: #000;\n}\n\nion-title {\n  color: #fff;\n  font-family: \"Anton\", serif;\n  font-size: 17px;\n  letter-spacing: 4px;\n}\n\n.header-img {\n  display: flex;\n  /* flex-direction: column; */\n  /* text-align: center; */\n}\n\n.header-img img {\n  height: 50px;\n  width: 190px;\n}\n\n.toolbar-background {\n  border-color: transparent !important;\n}\n\n.back-icon {\n  color: #fff;\n}\n\nion-segment {\n  background: transparent;\n}\n\nion-segment-button {\n  background: transparent;\n  --background-checked: transparent;\n  --indicator-height: 0px;\n}\n\n.segment_container {\n  padding-top: 15%;\n  padding-left: 15%;\n  padding-right: 15%;\n}\n\n.segment-btn-text {\n  color: #fff;\n  font-weight: 600;\n  font-size: 13px;\n  font-family: Oswald, serif;\n}\n\n.segment_container_data {\n  padding-top: 10%;\n  padding-left: 8%;\n  padding-right: 8%;\n}\n\n.input_container {\n  margin-bottom: 10px;\n  width: 100%;\n  --background: transparent;\n  --color: #fff;\n  --border-color: #fff;\n  height: 50px;\n}\n\n.submit-btn {\n  width: 100%;\n  margin-top: 10%;\n  height: 39px;\n  font-weight: 500;\n  font-family: Oswald, sans-serif;\n  --color: #fff;\n  --background: var(--ion-btn-custom-color);\n}\n\n.input-icon {\n  color: gray;\n  padding-right: 10px;\n  font-size: 20px;\n}\n\nion-input {\n  font-family: Oswald, serif;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvcGFnZXMvYXV0aC9hdXRoLnBhZ2Uuc2NzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFJQTtFQUNJLGtCQUFBO0FBSEo7O0FBTUE7RUFDSSxnQkFBQTtBQUhKOztBQVFBO0VBQ0ksV0FBQTtFQUNBLDJCQUFBO0VBRUEsZUFBQTtFQUNBLG1CQUFBO0FBTko7O0FBVUE7RUFDSSxhQUFBO0VBQ0EsNEJBQUE7RUFDQSx3QkFBQTtBQVBKOztBQVVJO0VBQ0ksWUFBQTtFQUNBLFlBQUE7QUFSUjs7QUFnQkE7RUFDSSxvQ0FBQTtBQWJKOztBQWdCQTtFQUNJLFdBQUE7QUFiSjs7QUFlQTtFQUNJLHVCQUFBO0FBWko7O0FBZ0JBO0VBRUksdUJBQUE7RUFDQSxpQ0FBQTtFQUNBLHVCQUFBO0FBZEo7O0FBbUJBO0VBQ0ksZ0JBQUE7RUFDQSxpQkFBQTtFQUNBLGtCQUFBO0FBaEJKOztBQW1CQTtFQUNJLFdBQUE7RUFDQSxnQkFBQTtFQUNBLGVBQUE7RUFDQSwwQkFBQTtBQWhCSjs7QUFtQkE7RUFDSSxnQkFBQTtFQUNBLGdCQUFBO0VBQ0EsaUJBQUE7QUFoQko7O0FBcUJBO0VBQ0ksbUJBQUE7RUFDQSxXQUFBO0VBQ0EseUJBQUE7RUFDQSxhQUFBO0VBQ0Esb0JBQUE7RUFDQSxZQUFBO0FBbEJKOztBQXFCQTtFQUNJLFdBQUE7RUFDQSxlQUFBO0VBQ0EsWUFBQTtFQUNBLGdCQUFBO0VBQ0EsK0JBQUE7RUFDQSxhQUFBO0VBQ0EseUNBQUE7QUFsQko7O0FBcUJBO0VBQ0ksV0FBQTtFQUNBLG1CQUFBO0VBQ0EsZUFBQTtBQWxCSjs7QUFxQkE7RUFDSSwwQkFBQTtBQWxCSiIsImZpbGUiOiJzcmMvYXBwL3BhZ2VzL2F1dGgvYXV0aC5wYWdlLnNjc3MiLCJzb3VyY2VzQ29udGVudCI6WyIvLyBpb24tdG9vbGJhciB7XG4vLyAgICAgLS1iYWNrZ3JvdW5kOiAjMDAwO1xuLy8gfVxuXG5pb24tY29udGVudCB7XG4gICAgLS1iYWNrZ3JvdW5kOiAjMDAwO1xufVxuXG5pb24taGVhZGVyIHtcbiAgICBiYWNrZ3JvdW5kOiAjMDAwO1xufVxuXG5cblxuaW9uLXRpdGxlIHtcbiAgICBjb2xvcjogI2ZmZjtcbiAgICBmb250LWZhbWlseTogJ0FudG9uJywgc2VyaWY7XG5cbiAgICBmb250LXNpemU6IDE3cHg7XG4gICAgbGV0dGVyLXNwYWNpbmc6IDRweDtcblxufVxuXG4uaGVhZGVyLWltZyB7XG4gICAgZGlzcGxheTogZmxleDtcbiAgICAvKiBmbGV4LWRpcmVjdGlvbjogY29sdW1uOyAqL1xuICAgIC8qIHRleHQtYWxpZ246IGNlbnRlcjsgKi9cbiAgICAvLyBtYXJnaW46IDAgYXV0bztcblxuICAgIGltZyB7XG4gICAgICAgIGhlaWdodDogNTBweDtcbiAgICAgICAgd2lkdGg6IDE5MHB4O1xuICAgICAgICBcbiAgICB9XG59XG5cblxuXG5cbi50b29sYmFyLWJhY2tncm91bmQge1xuICAgIGJvcmRlci1jb2xvcjogdHJhbnNwYXJlbnQgIWltcG9ydGFudDtcbn1cblxuLmJhY2staWNvbiB7XG4gICAgY29sb3I6ICNmZmY7XG59XG5pb24tc2VnbWVudCB7XG4gICAgYmFja2dyb3VuZDogdHJhbnNwYXJlbnQ7XG4gICAgXG4gICBcbn1cbmlvbi1zZWdtZW50LWJ1dHRvbiB7XG5cbiAgICBiYWNrZ3JvdW5kOiB0cmFuc3BhcmVudDtcbiAgICAtLWJhY2tncm91bmQtY2hlY2tlZDogdHJhbnNwYXJlbnQ7XG4gICAgLS1pbmRpY2F0b3ItaGVpZ2h0OiAwcHg7XG4gICBcbiAgICBcbn1cblxuLnNlZ21lbnRfY29udGFpbmVyIHtcbiAgICBwYWRkaW5nLXRvcDogMTUlO1xuICAgIHBhZGRpbmctbGVmdDogMTUlO1xuICAgIHBhZGRpbmctcmlnaHQ6IDE1JTtcbn1cblxuLnNlZ21lbnQtYnRuLXRleHQge1xuICAgIGNvbG9yOiAjZmZmO1xuICAgIGZvbnQtd2VpZ2h0OiA2MDA7XG4gICAgZm9udC1zaXplOiAxM3B4O1xuICAgIGZvbnQtZmFtaWx5OiBPc3dhbGQsIHNlcmlmO1xufVxuXG4uc2VnbWVudF9jb250YWluZXJfZGF0YSB7XG4gICAgcGFkZGluZy10b3A6IDEwJTtcbiAgICBwYWRkaW5nLWxlZnQ6IDglO1xuICAgIHBhZGRpbmctcmlnaHQ6IDglO1xufVxuXG5cblxuLmlucHV0X2NvbnRhaW5lciB7XG4gICAgbWFyZ2luLWJvdHRvbTogMTBweDtcbiAgICB3aWR0aDogMTAwJTtcbiAgICAtLWJhY2tncm91bmQ6IHRyYW5zcGFyZW50O1xuICAgIC0tY29sb3I6ICNmZmY7XG4gICAgLS1ib3JkZXItY29sb3I6ICNmZmY7XG4gICAgaGVpZ2h0OiA1MHB4O1xufVxuXG4uc3VibWl0LWJ0biB7XG4gICAgd2lkdGg6IDEwMCU7XG4gICAgbWFyZ2luLXRvcDogMTAlO1xuICAgIGhlaWdodDogMzlweDtcbiAgICBmb250LXdlaWdodDogNTAwO1xuICAgIGZvbnQtZmFtaWx5OiBPc3dhbGQsIHNhbnMtc2VyaWY7XG4gICAgLS1jb2xvcjogI2ZmZjtcbiAgICAtLWJhY2tncm91bmQ6IHZhcigtLWlvbi1idG4tY3VzdG9tLWNvbG9yKTtcbn1cblxuLmlucHV0LWljb24ge1xuICAgIGNvbG9yOiBncmF5O1xuICAgIHBhZGRpbmctcmlnaHQ6IDEwcHg7XG4gICAgZm9udC1zaXplOiAyMHB4O1xufVxuXG5pb24taW5wdXQge1xuICAgIGZvbnQtZmFtaWx5OiBPc3dhbGQsIHNlcmlmO1xuXG59Il19 */";
      /***/
    },

    /***/
    "./src/app/pages/auth/auth.page.ts":
    /*!*****************************************!*\
      !*** ./src/app/pages/auth/auth.page.ts ***!
      \*****************************************/

    /*! exports provided: AuthPage */

    /***/
    function srcAppPagesAuthAuthPageTs(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "AuthPage", function () {
        return AuthPage;
      });
      /* harmony import */


      var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! tslib */
      "./node_modules/tslib/tslib.es6.js");
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! @angular/core */
      "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
      /* harmony import */


      var _angular_forms__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! @angular/forms */
      "./node_modules/@angular/forms/__ivy_ngcc__/fesm2015/forms.js");
      /* harmony import */


      var _angular_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
      /*! @angular/router */
      "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");
      /* harmony import */


      var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
      /*! @ionic/angular */
      "./node_modules/@ionic/angular/__ivy_ngcc__/fesm2015/ionic-angular.js");
      /* harmony import */


      var _services_auth_auth_service__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(
      /*! @services/auth/auth.service */
      "./src/app/services/auth/auth.service.ts");
      /* harmony import */


      var _services_shared_localstroage_localstorage_service__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(
      /*! @services/shared/localstroage/localstorage.service */
      "./src/app/services/shared/localstroage/localstorage.service.ts");
      /* harmony import */


      var _services_userdata_service__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(
      /*! @services/userdata.service */
      "./src/app/services/userdata.service.ts");

      var AuthPage = /*#__PURE__*/function () {
        function AuthPage(router, userData, authService, alertCtrl, localStorage) {
          _classCallCheck(this, AuthPage);

          this.router = router;
          this.userData = userData;
          this.authService = authService;
          this.alertCtrl = alertCtrl;
          this.localStorage = localStorage;
          this.segmentModel = "login";
        }

        _createClass(AuthPage, [{
          key: "ngOnInit",
          value: function ngOnInit() {
            this.loginFormInitialization();
            this.registrationFormInitialization();
            this.userRole = this.localStorage.getItem('registrationRole');
          }
        }, {
          key: "loginFormInitialization",
          value: function loginFormInitialization() {
            this.loginForm = new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormGroup"]({
              email: new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormControl"]('', [_angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required, _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].email]),
              password: new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormControl"]('', _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required)
            });
          }
        }, {
          key: "goToScanArrival",
          value: function goToScanArrival() {
            return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, /*#__PURE__*/regeneratorRuntime.mark(function _callee() {
              var newData, res;
              return regeneratorRuntime.wrap(function _callee$(_context) {
                while (1) {
                  switch (_context.prev = _context.next) {
                    case 0:
                      if (!this.loginForm.valid) {
                        _context.next = 8;
                        break;
                      }

                      newData = {
                        "email": this.loginForm.value.email,
                        "password": this.loginForm.value.password
                      };
                      _context.next = 4;
                      return this.authService.login(newData);

                    case 4:
                      res = _context.sent;

                      if (res) {
                        if (res['error']) {
                          this.showAlert('Error', JSON.stringify(res['error'].message));
                        } else {
                          if (res['user']) {
                            this.localStorage.setItem('user', JSON.stringify(res['user']));
                          }

                          this.localStorage.setItem('userLoginDetails', JSON.stringify(res));

                          if (this.userRole != undefined && this.userRole != null) {
                            if (this.userRole == 'coach') {
                              this.router.navigate(['/body-scan-arrival-coach']);
                            } else {
                              this.router.navigate(['/body-scan-arrival']);
                            }
                          }
                        }
                      }

                      _context.next = 8;
                      break;

                    case 8:
                    case "end":
                      return _context.stop();
                  }
                }
              }, _callee, this);
            }));
          }
        }, {
          key: "registrationFormInitialization",
          value: function registrationFormInitialization() {
            this.registrationForm = new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormGroup"]({
              email: new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormControl"]('', [_angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required, _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].email]),
              password: new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormControl"]('', _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required),
              confirm_password: new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormControl"]('', [_angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required, this.equalto('password')]),
              first_name: new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormControl"]('', _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required),
              last_name: new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormControl"]('', _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required)
            });
          }
        }, {
          key: "registerAndRedirect",
          value: function registerAndRedirect() {
            return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, /*#__PURE__*/regeneratorRuntime.mark(function _callee2() {
              var newData, res;
              return regeneratorRuntime.wrap(function _callee2$(_context2) {
                while (1) {
                  switch (_context2.prev = _context2.next) {
                    case 0:
                      if (!this.registrationForm.valid) {
                        _context2.next = 8;
                        break;
                      }

                      newData = {
                        "email": this.registrationForm.value.email,
                        "password": this.registrationForm.value.password,
                        "role": this.userRole != undefined && this.userRole != null ? this.userRole : '',
                        "name": this.registrationForm.value.last_name + ' ' + this.registrationForm.value.first_name
                      };
                      _context2.next = 4;
                      return this.authService.register(newData);

                    case 4:
                      res = _context2.sent;

                      if (res) {
                        if (res['error']) {
                          this.showAlert('Error', res['error'].message);
                        } else {
                          this.showAlert('Success', JSON.stringify(res['message']));
                          this.localStorage.removeItem('registrationRole');
                          this.router.navigate(['/body-scan-arrival']);
                        }
                      }

                      _context2.next = 8;
                      break;

                    case 8:
                    case "end":
                      return _context2.stop();
                  }
                }
              }, _callee2, this);
            }));
          }
        }, {
          key: "equalto",
          value: function equalto(field_name) {
            return function (control) {
              var input = control.value;
              var isValid = control.root.value[field_name] == input;
              if (!isValid) return {
                'equalTo': {
                  isValid: isValid
                }
              };else return null;
            };
          }
        }, {
          key: "showAlert",
          value: function showAlert() {
            var status = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : '';
            var message = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : '';
            return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, /*#__PURE__*/regeneratorRuntime.mark(function _callee3() {
              var alert;
              return regeneratorRuntime.wrap(function _callee3$(_context3) {
                while (1) {
                  switch (_context3.prev = _context3.next) {
                    case 0:
                      alert = this.alertCtrl.create({
                        header: status,
                        message: message,
                        buttons: ['Dismiss']
                      });
                      _context3.next = 3;
                      return alert;

                    case 3:
                      _context3.sent.present();

                    case 4:
                    case "end":
                      return _context3.stop();
                  }
                }
              }, _callee3, this);
            }));
          }
        }, {
          key: "segmentChanged",
          value: function segmentChanged(event) {
            console.log(this.segmentModel);
            console.log(event);
          }
        }, {
          key: "goBack",
          value: function goBack() {
            this.router.navigate(['/select-roles']);
          }
        }]);

        return AuthPage;
      }();

      AuthPage.ctorParameters = function () {
        return [{
          type: _angular_router__WEBPACK_IMPORTED_MODULE_3__["Router"]
        }, {
          type: _services_userdata_service__WEBPACK_IMPORTED_MODULE_7__["UserdataService"]
        }, {
          type: _services_auth_auth_service__WEBPACK_IMPORTED_MODULE_5__["AuthService"]
        }, {
          type: _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["AlertController"]
        }, {
          type: _services_shared_localstroage_localstorage_service__WEBPACK_IMPORTED_MODULE_6__["LocalstorageService"]
        }];
      };

      AuthPage = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-auth',
        template: Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(
        /*! raw-loader!./auth.page.html */
        "./node_modules/raw-loader/dist/cjs.js!./src/app/pages/auth/auth.page.html"))["default"],
        styles: [Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(
        /*! ./auth.page.scss */
        "./src/app/pages/auth/auth.page.scss"))["default"]]
      })], AuthPage);
      /***/
    },

    /***/
    "./src/app/services/auth/auth.service.ts":
    /*!***********************************************!*\
      !*** ./src/app/services/auth/auth.service.ts ***!
      \***********************************************/

    /*! exports provided: AuthService */

    /***/
    function srcAppServicesAuthAuthServiceTs(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "AuthService", function () {
        return AuthService;
      });
      /* harmony import */


      var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! tslib */
      "./node_modules/tslib/tslib.es6.js");
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! @angular/core */
      "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
      /* harmony import */


      var _services_http_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! @services/http.service */
      "./src/app/services/http.service.ts");

      var AuthService = /*#__PURE__*/function () {
        function AuthService(httpService) {
          _classCallCheck(this, AuthService);

          this.httpService = httpService;
        }

        _createClass(AuthService, [{
          key: "login",
          value: function login(details) {
            return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, /*#__PURE__*/regeneratorRuntime.mark(function _callee4() {
              return regeneratorRuntime.wrap(function _callee4$(_context4) {
                while (1) {
                  switch (_context4.prev = _context4.next) {
                    case 0:
                      _context4.next = 2;
                      return this.httpService.callApi('POST', details, 'api/login');

                    case 2:
                      return _context4.abrupt("return", _context4.sent);

                    case 3:
                    case "end":
                      return _context4.stop();
                  }
                }
              }, _callee4, this);
            }));
          }
        }, {
          key: "register",
          value: function register(details) {
            return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, /*#__PURE__*/regeneratorRuntime.mark(function _callee5() {
              return regeneratorRuntime.wrap(function _callee5$(_context5) {
                while (1) {
                  switch (_context5.prev = _context5.next) {
                    case 0:
                      _context5.next = 2;
                      return this.httpService.callApi('POST', details, 'api/register');

                    case 2:
                      return _context5.abrupt("return", _context5.sent);

                    case 3:
                    case "end":
                      return _context5.stop();
                  }
                }
              }, _callee5, this);
            }));
          }
        }]);

        return AuthService;
      }();

      AuthService.ctorParameters = function () {
        return [{
          type: _services_http_service__WEBPACK_IMPORTED_MODULE_2__["HttpService"]
        }];
      };

      AuthService = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])({
        providedIn: 'root'
      })], AuthService);
      /***/
    }
  }]);
})();
//# sourceMappingURL=pages-auth-auth-module-es5.js.map