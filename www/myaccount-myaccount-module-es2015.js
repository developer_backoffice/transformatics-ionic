(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["myaccount-myaccount-module"],{

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/pages/tabs/myaccount/myaccount.page.html":
/*!************************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/pages/tabs/myaccount/myaccount.page.html ***!
  \************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<ion-content (ionScroll)=\"scrollHandler($event)\" scrollEvents=\"true\">\n    <div class=\"card\">\n        <div class=\"header\">\n            <div class=\"avatar\">\n                <img src=\"assets/icon/man.png\" alt=\"\" />\n                <div class=\"profile-edit\">\n                    <ion-icon name=\"pencil-outline\"></ion-icon>\n                </div>\n            </div>\n        </div>\n        <div class=\"card-body\">\n            <div class=\"user-meta ion-text-center\">\n                <h6 class=\"username\">@THOMAS</h6>\n                <h3 class=\"playername\">THOMAS CLARK</h3>\n                <h6 class=\"username\">23 - FRANCE</h6>\n            </div>\n            <ion-row>\n                <ion-col size=\"6\">\n                    <div class=\"profile-member\">\n                        <p>CONNEXION CONTINUE DEPUIS</p>\n                        <h4 class=\"activity_title\">42 JOURS</h4>\n                    </div>\n                </ion-col>\n                <ion-col size=\"6\">\n                    <div class=\"profile-member\">\n                        <p>MEMBRE DEPUIS LE</p>\n                        <h4 class=\"activity_title\">12 Janvier 2020</h4>\n                    </div>\n                </ion-col>\n            </ion-row>\n        </div>\n    </div>\n    <ion-grid>\n        <ion-row>\n            <ion-col size=\"12\">\n                <!-- ======================================= -->\n                <ion-segment class=\"segmentOne\" mode=\"ios\" value=\"profile\" color=\"tertiary\" scrollable=\"true\" [(ngModel)]=\"parentSegmentModel\" (ionChange)=\"segmentChanged($event)\">\n                    <ion-segment-button class=\"segmentOne-button\" value=\"profile\">\n                        <ion-label class=\"segment-btn-text\">MON PROFIL </ion-label>\n                    </ion-segment-button>\n\n                    <ion-segment-button class=\"segmentOne-button\" value=\"parameter\">\n                        <ion-label class=\"segment-btn-text\">PARAMETRES</ion-label>\n                    </ion-segment-button>\n                </ion-segment>\n\n                <div class=\"segment_container_data\">\n                    <div *ngIf=\"parentSegmentModel === 'profile'\">\n                        <ion-row>\n                            <ion-col size=\"4\">\n                                <div class=\"profile-segment-box\" routerLink=\"/edit-physical-activity\">\n                                    <img src=\"assets/icon/seance.png\" />\n                                    <p>NOMBRE TOTAL DE SEANCES</p>\n                                    <h4>655</h4>\n                                </div>\n                            </ion-col>\n                            <ion-col size=\"4\">\n                                <div class=\"profile-segment-box\">\n                                    <img src=\"assets/icon/pratiques.png\" />\n                                    <p>NOMBRE TOTAL DE SEANCES</p>\n                                    <h4>655</h4>\n                                </div>\n                            </ion-col>\n                            <ion-col size=\"4\">\n                                <div class=\"profile-segment-box\">\n                                    <img src=\"assets/icon/brules.png\" />\n                                    <p>NOMBRE TOTAL DE SEANCES</p>\n                                    <h4>655</h4>\n                                </div>\n                            </ion-col>\n                        </ion-row>\n                        <ion-row>\n                            <ion-col size=\"6\">\n                                <div>\n                                    <p class=\"p-b-0 m-b-0\">ACTIVITES</p>\n                                </div>\n                            </ion-col>\n                            <ion-col size=\"6\">\n                                <div class=\"ion-text-right\">\n                                    <p class=\"p-b-0 m-b-0\">NB DE SEANCES</p>\n                                </div>\n                            </ion-col>\n                            <ion-col size=\"8\" class=\"p-b-0 m-b-0\">\n                                <ion-item class=\"height-40\">\n                                    <ion-avatar slot=\"start\">\n                                        <img src=\"assets/icon/masculation.png\" />\n                                    </ion-avatar>\n                                    <ion-label class=\"activity_title\">MUSCULATION</ion-label>\n                                </ion-item>\n                            </ion-col>\n                            <ion-col size=\"4\" class=\"activities_count p-b-0 m-b-0\">\n                                <div class=\"ion-text-right\">x 32</div>\n                            </ion-col>\n                            <ion-col size=\"8\" class=\"p-b-0 m-b-0\">\n                                <ion-item class=\"height-40\">\n                                    <ion-avatar slot=\"start\">\n                                        <img src=\"assets/icon/natation.png\" />\n                                    </ion-avatar>\n                                    <ion-label class=\"activity_title \">NATATION</ion-label>\n                                </ion-item>\n                            </ion-col>\n                            <ion-col size=\"4\" class=\"activities_count p-b-0 m-b-0\">\n                                <div class=\"ion-text-right\">x 12</div>\n                            </ion-col>\n                            <ion-col size=\"8\" class=\"p-b-0 m-b-0\">\n                                <ion-item class=\"height-40\">\n                                    <ion-avatar slot=\"start\">\n                                        <img src=\"assets/icon/football.png\" />\n                                    </ion-avatar>\n                                    <ion-label class=\"activity_title\">FOOTBALL </ion-label>\n                                </ion-item>\n                            </ion-col>\n                            <ion-col size=\"4\" class=\"activities_count p-b-0 m-b-0\">\n                                <div class=\"ion-text-right\">x 3</div>\n                            </ion-col>\n                        </ion-row>\n                        <ion-row>\n                            <div class=\"profile-viewall\">\n                                <ion-button>VOIR TOUTES LES ACTIVITES\n                                    <ion-icon name=\"chevron-forward-outline\"></ion-icon>\n                                </ion-button>\n                            </div>\n                        </ion-row>\n                        <ion-row>\n                            <ion-segment value=\"performance\" color=\"tertiary\" scrollable=\"false\" [(ngModel)]=\"segmentModel\" (ionChange)=\"segmentChanged($event)\">\n                                <ion-segment-button value=\"performance\" style=\"padding-left: 5px\">\n                                    <ion-label class=\"segment-btn-text\">PERFORMANCE</ion-label>\n                                </ion-segment-button>\n\n                                <ion-segment-button value=\"records\">\n                                    <ion-label class=\"segment-btn-text\">RECORDS</ion-label>\n                                </ion-segment-button>\n                                <ion-segment-button value=\"physiologie\">\n                                    <ion-label class=\"segment-btn-text\">PHYSIOLOGIE</ion-label>\n                                </ion-segment-button>\n\n                                <ion-segment-button value=\"check-ups\">\n                                    <ion-label class=\"segment-btn-text\">CHECK-UPS</ion-label>\n                                </ion-segment-button>\n                            </ion-segment>\n\n                            <div class=\"segment_container_data\">\n                                <div *ngIf=\"segmentModel === 'performance'\">\n                                    <app-performance-profile></app-performance-profile>\n                                </div>\n\n                                <div *ngIf=\"segmentModel === 'records'\">\n                                    <app-records-profile></app-records-profile>\n                                </div>\n                                <div *ngIf=\"segmentModel === 'physiologie'\">\n                                    <app-physiology-profile> </app-physiology-profile>\n                                </div>\n                                <div *ngIf=\"segmentModel === 'check-ups'\">\n                                    <app-check-ups-profile></app-check-ups-profile>\n                                </div>\n                            </div>\n                        </ion-row>\n                    </div>\n                    <!-- ============================== -->\n                    <div *ngIf=\"parentSegmentModel === 'parameter'\">\n                        <ion-segment value=\"autorisations\" color=\"tertiary\" [(ngModel)]=\"parameterSegmentModel\" (ionChange)=\"parameterSegmentChanged($event)\">\n                            <ion-segment-button value=\"autorisations\">\n                                <ion-label class=\"segment-btn-text\">AUTORISATIONS</ion-label>\n                            </ion-segment-button>\n\n                            <ion-segment-button value=\"language\">\n                                <ion-label class=\"segment-btn-text\">LANGUE</ion-label>\n                            </ion-segment-button>\n                            <ion-segment-button value=\"notifications\">\n                                <ion-label class=\"segment-btn-text\">NOTIFICATIONS</ion-label>\n                            </ion-segment-button>\n                            <ion-segment-button value=\"general\">\n                                <ion-label class=\"segment-btn-text\">GENERAL</ion-label>\n                            </ion-segment-button>\n                        </ion-segment>\n\n                        <div class=\"segment_container_data\">\n                            <div *ngIf=\"parameterSegmentModel === 'autorisations'\" style=\"margin-bottom: 15%;\">\n                                <ion-list>\n                                    <ion-item>\n                                        <ion-label>Autoriser l’application à utiliser le GPS</ion-label>\n                                        <ion-toggle></ion-toggle>\n                                    </ion-item>\n                                    <ion-item>\n                                        <ion-label>Autoriser l’application à utiliser l’appareil photo\n                                        </ion-label>\n                                        <ion-toggle></ion-toggle>\n                                    </ion-item>\n                                    <ion-item>\n                                        <ion-label>Autoriser l’application à évoluer en arrière plan\n                                        </ion-label>\n                                        <ion-toggle></ion-toggle>\n                                    </ion-item>\n                                    <ion-item>\n                                        <ion-label>Autoriser la synchronisation avec appareils</ion-label>\n                                        <ion-toggle></ion-toggle>\n                                    </ion-item>\n                                </ion-list>\n                            </div>\n\n                            <div *ngIf=\"parameterSegmentModel === 'language'\">\n                                <ion-list>\n                                    <ion-item>\n                                        <ion-label>\n                                            Langue : <span class=\"value-text\">français</span>\n                                        </ion-label>\n                                    </ion-item>\n                                </ion-list>\n\n                            </div>\n                            <div *ngIf=\"parameterSegmentModel === 'notifications'\"  style=\"margin-bottom: 25%;\">\n\n                                <p style=\"font-size: 14px;\">Autoriser l'application à émettre des notifications pour les faits suivants:</p>\n                                <ion-list>\n                                    <ion-item>\n                                        <ion-label class=\"text-list\">\n                                            <p>- Check-up</p>\n                                            <p>- Connexion journalière</p>\n                                            <p>- Entraînement du jour</p>\n                                        </ion-label>\n                                        <ion-toggle></ion-toggle>\n                                    </ion-item>\n                                </ion-list>\n\n                            </div>\n                            <div *ngIf=\"parameterSegmentModel === 'general'\">\n                                <ion-list>\n                                    <ion-item>\n                                        <ion-label>\n                                            <span class=\"property-text\">Date d'inscription au système:</span> <span class=\"value-text\"> 02/02/2021</span>\n                                        </ion-label>\n                                    </ion-item>\n                                    <ion-item>\n                                        <ion-label>\n                                            <span class=\"property-text\">Id utilisateur:</span> <span class=\"value-text\"> 1234-abcd-4567-zxcv</span>\n                                        </ion-label>\n                                    </ion-item>\n                                    <ion-item>\n                                        <ion-label>\n                                            <span class=\"property-text\"> Nom et prénom:</span> <span class=\"value-text\"> Martin Nicolas</span>\n                                        </ion-label>\n                                    </ion-item>\n                                    <ion-item>\n                                        <ion-label>\n                                            <span class=\"property-text\">Poids:</span> <span class=\"value-text\"> 97kg</span>\n                                        </ion-label>\n                                    </ion-item>\n                                    <ion-item>\n                                        <ion-label>\n                                            <span class=\"property-text\">Taille:</span> <span class=\"value-text\"> 184cm</span>\n                                        </ion-label>\n                                    </ion-item>\n                                    <ion-item>\n                                        <ion-label>\n                                            <span class=\"property-text\">Sexe:</span> <span class=\"value-text\"> masculin</span>\n                                        </ion-label>\n                                    </ion-item>\n                                    <ion-item>\n                                        <ion-label>\n                                            <span class=\"property-text\">Date de naissance:</span> <span class=\"value-text\"> 08/12/1997</span>\n                                        </ion-label>\n                                    </ion-item>\n                                    <ion-item>\n                                        <ion-label>\n                                            <span class=\"property-text\">Email:</span> <span class=\"value-text\"> nicolas.martin1997@yahoo.fr</span>\n                                        </ion-label>\n                                    </ion-item>\n                                    <ion-item>\n                                        <ion-label>\n                                            <span class=\"change-pass\">Modifier le mot de passe</span>\n                                        </ion-label>\n                                    </ion-item>\n                                    <ion-item>\n                                        <ion-label>\n                                            <span class=\"property-text\">Objectif actuel: Perte de poids</span>\n                                        </ion-label>\n                                    </ion-item>\n                                </ion-list>\n\n                                <div class=\"buttons\">\n                                    <ion-button class=\"next\" expand=\"full\">MODIFIER MON OBJECTIF</ion-button>\n                                    <ion-button class=\"next\" expand=\"full\">VOIR LES RESULTATS DE MON BODYSCAN</ion-button>\n                                    <ion-button class=\"next\" expand=\"full\">RESILIER L'ABONNEMENT</ion-button>\n                                    <ion-button class=\"next\" expand=\"full\">EXPORTER MES DONNEES PERSONNELLES</ion-button>\n                                </div>\n\n\n                            </div>\n                        </div>\n                    </div>\n                </div>\n            </ion-col>\n        </ion-row>\n    </ion-grid>\n</ion-content>");

/***/ }),

/***/ "./src/app/pages/tabs/myaccount/myaccount-routing.module.ts":
/*!******************************************************************!*\
  !*** ./src/app/pages/tabs/myaccount/myaccount-routing.module.ts ***!
  \******************************************************************/
/*! exports provided: MyaccountPageRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "MyaccountPageRoutingModule", function() { return MyaccountPageRoutingModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");
/* harmony import */ var _myaccount_page__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./myaccount.page */ "./src/app/pages/tabs/myaccount/myaccount.page.ts");




const routes = [
    {
        path: '',
        component: _myaccount_page__WEBPACK_IMPORTED_MODULE_3__["MyaccountPage"]
    }
];
let MyaccountPageRoutingModule = class MyaccountPageRoutingModule {
};
MyaccountPageRoutingModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]],
    })
], MyaccountPageRoutingModule);



/***/ }),

/***/ "./src/app/pages/tabs/myaccount/myaccount.module.ts":
/*!**********************************************************!*\
  !*** ./src/app/pages/tabs/myaccount/myaccount.module.ts ***!
  \**********************************************************/
/*! exports provided: MyaccountPageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "MyaccountPageModule", function() { return MyaccountPageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/__ivy_ngcc__/fesm2015/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/__ivy_ngcc__/fesm2015/forms.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/__ivy_ngcc__/fesm2015/ionic-angular.js");
/* harmony import */ var _myaccount_routing_module__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./myaccount-routing.module */ "./src/app/pages/tabs/myaccount/myaccount-routing.module.ts");
/* harmony import */ var _myaccount_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./myaccount.page */ "./src/app/pages/tabs/myaccount/myaccount.page.ts");
/* harmony import */ var _components_shared_module__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! @components/shared.module */ "./src/app/components/shared.module.ts");








let MyaccountPageModule = class MyaccountPageModule {
};
MyaccountPageModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [
            _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonicModule"],
            _myaccount_routing_module__WEBPACK_IMPORTED_MODULE_5__["MyaccountPageRoutingModule"],
            _components_shared_module__WEBPACK_IMPORTED_MODULE_7__["SharedModule"]
        ],
        declarations: [_myaccount_page__WEBPACK_IMPORTED_MODULE_6__["MyaccountPage"]]
    })
], MyaccountPageModule);



/***/ }),

/***/ "./src/app/pages/tabs/myaccount/myaccount.page.scss":
/*!**********************************************************!*\
  !*** ./src/app/pages/tabs/myaccount/myaccount.page.scss ***!
  \**********************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("@charset \"UTF-8\";\nion-toolbar {\n  -–background: transparent;\n}\n.card {\n  margin: 0 auto;\n  background: url('landing_page_background_1.png') 0 0/100% no-repeat;\n  background-size: cover;\n  --overflow: hidden;\n}\n.header {\n  height: 160px;\n  position: relative;\n}\n.header:before {\n  content: \"\";\n  background: #00000094;\n  height: 100%;\n  width: 100%;\n  position: absolute;\n  left: 0;\n  right: 0;\n  top: 0;\n}\n.avatar {\n  width: 110px;\n  height: 110px;\n  position: relative;\n  margin: 0 auto;\n  bottom: calc(-1*(100px + 4px));\n}\n.avatar img {\n  display: block;\n  border-radius: 50%;\n  position: absolute;\n  border: 5px solid #c1c1c1;\n  background-color: #fff;\n}\n.card-body {\n  background-color: #ffffff;\n  padding: 50px 10px 15px 10px;\n  height: calc(100vh – (200px + 56px));\n  overflow: hidden;\n}\n.profile-member p {\n  color: #a8a7a7;\n  font-size: 12px;\n}\n.profile-edit {\n  color: #000;\n  font-size: 21px;\n  top: 55px;\n  right: -12px;\n  position: absolute;\n  background: #fff;\n  width: 35px;\n  height: 35px;\n  border-radius: 50%;\n  text-align: center;\n  border: 2px solid #000;\n}\n.user-meta {\n  padding-top: 15px;\n  padding-bottom: 5px;\n}\n.playername {\n  font-size: 14px;\n  font-weight: 600;\n  color: #303940;\n  margin-top: 5px;\n  margin-bottom: 5px;\n}\n.username {\n  font-size: 75%;\n  color: #949ea6;\n  text-transform: uppercase;\n  margin: 0 auto;\n}\n.segmentOne-button {\n  --ripple-color: transparent;\n  background: transparent;\n  --color-checked: var(--ion-color-primary);\n  --color: #9c9c9c;\n  --indicator-color: var(--ion-color-primary);\n  --margin-bottom: 0px;\n  height: 40px;\n  --border-radius: 25px;\n}\n.segmentOne {\n  background: rgba(170, 170, 170, 0.11);\n  border-radius: 25px;\n  margin-bottom: 15px;\n}\n.profile-segment-box img {\n  width: 40px;\n}\n.profile-segment-box {\n  position: relative;\n  border-radius: 8px;\n  font-size: 14px;\n  box-shadow: rgba(0, 0, 0, 0.12) 0px 4px 16px;\n  padding: 10px;\n  border: 1px solid #000;\n}\n.profile-segment-box p {\n  color: #7f7f7f;\n  font-size: 12px;\n  -webkit-margin-before: 0em;\n          margin-block-start: 0em;\n  -webkit-margin-after: 0em;\n          margin-block-end: 0em;\n}\n.profile-segment-box h4 {\n  margin-top: 5px;\n  margin-bottom: 0px;\n}\n.segment-btn-text {\n  font-family: \"Oswald-Medium\";\n}\nion-item ion-label {\n  white-space: normal;\n}\nion-item {\n  --inner-border-width: 0px 0px 0px 0px;\n  --min-height: 35px;\n  --padding-start: 0px;\n  --border-width: 0 0 0px 0;\n}\n.height-40 {\n  height: 40px !important;\n}\nion-list {\n  padding-top: 0px !important;\n  padding-bottom: 0px !important;\n}\n.profile-viewall {\n  width: 100%;\n  position: relative;\n  margin-top: 15px;\n  margin-bottom: 15px;\n}\n.profile-viewall ion-button {\n  --border-radius: 25px;\n  display: table;\n  margin: auto;\n  font-size: 12px;\n}\n.profile-viewall:before {\n  content: \"\";\n  position: absolute;\n  background: #c1c1c1;\n  height: 1px;\n  width: 100%;\n  top: 19px;\n  left: 0;\n}\nion-toggle {\n  --background: #926444;\n  --background-checked: #F4B183;\n  --handle-background: #F4B183;\n  --handle-background-checked: #926444;\n}\n.activity_title {\n  text-align: justify;\n  margin: 0;\n  font-size: 13px;\n  font-weight: 600;\n}\nion-avatar {\n  padding: 5px;\n  -webkit-margin-end: 0px;\n          margin-inline-end: 0px;\n}\n.activities_count {\n  padding-top: 10px;\n}\n.segment_container_data {\n  position: relative;\n  display: block;\n  width: 100%;\n  margin-top: 15px;\n}\n.value-text {\n  color: #383a3e9c;\n  font-size: 14px;\n}\nion-label p {\n  color: #000;\n}\n.property-text {\n  font-family: \"Oswald-SemiBold\";\n  font-size: 14px;\n}\n.change-pass {\n  font-family: \"Oswald-SemiBold\";\n  color: #f00;\n  font-size: 14px;\n}\nion-item ion-label {\n  font-size: 14px;\n  font-family: \"Oswald\";\n}\n.next {\n  --background: var(--ion-btn-custom-color);\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvcGFnZXMvdGFicy9teWFjY291bnQvbXlhY2NvdW50LnBhZ2Uuc2NzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQSxnQkFBZ0I7QUFBaEI7RUFDSSx5QkFBQTtBQUVKO0FBQ0E7RUFDSSxjQUFBO0VBQ0EsbUVBQUE7RUFHQSxzQkFBQTtFQUNBLGtCQUFBO0FBRUo7QUFDQTtFQUNJLGFBQUE7RUFDQSxrQkFBQTtBQUVKO0FBQ0E7RUFDSSxXQUFBO0VBQ0EscUJBQUE7RUFDQSxZQUFBO0VBQ0EsV0FBQTtFQUNBLGtCQUFBO0VBQ0EsT0FBQTtFQUNBLFFBQUE7RUFDQSxNQUFBO0FBRUo7QUFDQTtFQUNJLFlBQUE7RUFDQSxhQUFBO0VBQ0Esa0JBQUE7RUFDQSxjQUFBO0VBQ0EsOEJBQUE7QUFFSjtBQUNBO0VBQ0ksY0FBQTtFQUNBLGtCQUFBO0VBQ0Esa0JBQUE7RUFDQSx5QkFBQTtFQUNBLHNCQUFBO0FBRUo7QUFDQTtFQUNJLHlCQUFBO0VBQ0EsNEJBQUE7RUFDQSxvQ0FBQTtFQUNBLGdCQUFBO0FBRUo7QUFDQTtFQUNJLGNBQUE7RUFDQSxlQUFBO0FBRUo7QUFDQTtFQUNJLFdBQUE7RUFDQSxlQUFBO0VBQ0EsU0FBQTtFQUNBLFlBQUE7RUFDQSxrQkFBQTtFQUNBLGdCQUFBO0VBQ0EsV0FBQTtFQUNBLFlBQUE7RUFDQSxrQkFBQTtFQUNBLGtCQUFBO0VBQ0Esc0JBQUE7QUFFSjtBQUNBO0VBQ0ksaUJBQUE7RUFDQSxtQkFBQTtBQUVKO0FBQ0E7RUFDSSxlQUFBO0VBQ0EsZ0JBQUE7RUFDQSxjQUFBO0VBQ0EsZUFBQTtFQUNBLGtCQUFBO0FBRUo7QUFDQTtFQUNJLGNBQUE7RUFDQSxjQUFBO0VBQ0EseUJBQUE7RUFDQSxjQUFBO0FBRUo7QUFFQTtFQUNJLDJCQUFBO0VBQ0EsdUJBQUE7RUFDQSx5Q0FBQTtFQUNBLGdCQUFBO0VBQ0EsMkNBQUE7RUFDQSxvQkFBQTtFQUNBLFlBQUE7RUFDQSxxQkFBQTtBQUNKO0FBRUE7RUFDSSxxQ0FBQTtFQUNBLG1CQUFBO0VBQ0EsbUJBQUE7QUFDSjtBQUVBO0VBQ0ksV0FBQTtBQUNKO0FBRUE7RUFDSSxrQkFBQTtFQUNBLGtCQUFBO0VBQ0EsZUFBQTtFQUNBLDRDQUFBO0VBQ0EsYUFBQTtFQUNBLHNCQUFBO0FBQ0o7QUFFQTtFQUNJLGNBQUE7RUFDQSxlQUFBO0VBQ0EsMEJBQUE7VUFBQSx1QkFBQTtFQUNBLHlCQUFBO1VBQUEscUJBQUE7QUFDSjtBQUVBO0VBQ0ksZUFBQTtFQUNBLGtCQUFBO0FBQ0o7QUFDQTtFQUNJLDRCQUFBO0FBRUo7QUFBQTtFQUNJLG1CQUFBO0FBR0o7QUFBQTtFQUNJLHFDQUFBO0VBQ0Esa0JBQUE7RUFDQSxvQkFBQTtFQUNBLHlCQUFBO0FBR0o7QUFBQTtFQUNJLHVCQUFBO0FBR0o7QUFEQTtFQUNJLDJCQUFBO0VBQ0EsOEJBQUE7QUFJSjtBQURBO0VBQ0ksV0FBQTtFQUNBLGtCQUFBO0VBQ0EsZ0JBQUE7RUFDQSxtQkFBQTtBQUlKO0FBREE7RUFDSSxxQkFBQTtFQUNBLGNBQUE7RUFDQSxZQUFBO0VBQ0EsZUFBQTtBQUlKO0FBREE7RUFDSSxXQUFBO0VBQ0Esa0JBQUE7RUFDQSxtQkFBQTtFQUNBLFdBQUE7RUFDQSxXQUFBO0VBQ0EsU0FBQTtFQUNBLE9BQUE7QUFJSjtBQURBO0VBQ0kscUJBQUE7RUFDQSw2QkFBQTtFQUNBLDRCQUFBO0VBQ0Esb0NBQUE7QUFJSjtBQURBO0VBQ0ksbUJBQUE7RUFFQSxTQUFBO0VBQ0EsZUFBQTtFQUNBLGdCQUFBO0FBR0o7QUFDQTtFQUNJLFlBQUE7RUFDQSx1QkFBQTtVQUFBLHNCQUFBO0FBRUo7QUFDQTtFQUNJLGlCQUFBO0FBRUo7QUFDQTtFQUNJLGtCQUFBO0VBQ0EsY0FBQTtFQUNBLFdBQUE7RUFDQSxnQkFBQTtBQUVKO0FBQUE7RUFDSSxnQkFBQTtFQUNBLGVBQUE7QUFHSjtBQURBO0VBQ1ksV0FBQTtBQUlaO0FBRkE7RUFDSSw4QkFBQTtFQUNBLGVBQUE7QUFLSjtBQUhBO0VBQ0ksOEJBQUE7RUFDQSxXQUFBO0VBQ0EsZUFBQTtBQU1KO0FBSEk7RUFDSSxlQUFBO0VBQ0EscUJBQUE7QUFNUjtBQUhBO0VBQ0kseUNBQUE7QUFNSiIsImZpbGUiOiJzcmMvYXBwL3BhZ2VzL3RhYnMvbXlhY2NvdW50L215YWNjb3VudC5wYWdlLnNjc3MiLCJzb3VyY2VzQ29udGVudCI6WyJAY2hhcnNldCBcIlVURi04XCI7XG5pb24tdG9vbGJhciB7XG4gIC3igJNiYWNrZ3JvdW5kOiB0cmFuc3BhcmVudDtcbn1cblxuLmNhcmQge1xuICBtYXJnaW46IDAgYXV0bztcbiAgYmFja2dyb3VuZDogdXJsKFwiLi4vLi4vLi4vLi4vYXNzZXRzL2ltYWdlcy9sYW5kaW5nX3BhZ2VfYmFja2dyb3VuZF8xLnBuZ1wiKSAwIDAvMTAwJSBuby1yZXBlYXQ7XG4gIC13ZWJraXQtYmFja2dyb3VuZC1zaXplOiBjb3ZlcjtcbiAgLW1vei1iYWNrZ3JvdW5kLXNpemU6IGNvdmVyO1xuICBiYWNrZ3JvdW5kLXNpemU6IGNvdmVyO1xuICAtLW92ZXJmbG93OiBoaWRkZW47XG59XG5cbi5oZWFkZXIge1xuICBoZWlnaHQ6IDE2MHB4O1xuICBwb3NpdGlvbjogcmVsYXRpdmU7XG59XG5cbi5oZWFkZXI6YmVmb3JlIHtcbiAgY29udGVudDogXCJcIjtcbiAgYmFja2dyb3VuZDogIzAwMDAwMDk0O1xuICBoZWlnaHQ6IDEwMCU7XG4gIHdpZHRoOiAxMDAlO1xuICBwb3NpdGlvbjogYWJzb2x1dGU7XG4gIGxlZnQ6IDA7XG4gIHJpZ2h0OiAwO1xuICB0b3A6IDA7XG59XG5cbi5hdmF0YXIge1xuICB3aWR0aDogMTEwcHg7XG4gIGhlaWdodDogMTEwcHg7XG4gIHBvc2l0aW9uOiByZWxhdGl2ZTtcbiAgbWFyZ2luOiAwIGF1dG87XG4gIGJvdHRvbTogY2FsYygtMSooMTAwcHggKyA0cHgpKTtcbn1cblxuLmF2YXRhciBpbWcge1xuICBkaXNwbGF5OiBibG9jaztcbiAgYm9yZGVyLXJhZGl1czogNTAlO1xuICBwb3NpdGlvbjogYWJzb2x1dGU7XG4gIGJvcmRlcjogNXB4IHNvbGlkICNjMWMxYzE7XG4gIGJhY2tncm91bmQtY29sb3I6ICNmZmY7XG59XG5cbi5jYXJkLWJvZHkge1xuICBiYWNrZ3JvdW5kLWNvbG9yOiAjZmZmZmZmO1xuICBwYWRkaW5nOiA1MHB4IDEwcHggMTVweCAxMHB4O1xuICBoZWlnaHQ6IGNhbGMoMTAwdmgg4oCTICgyMDBweCArIDU2cHgpKTtcbiAgb3ZlcmZsb3c6IGhpZGRlbjtcbn1cblxuLnByb2ZpbGUtbWVtYmVyIHAge1xuICBjb2xvcjogI2E4YTdhNztcbiAgZm9udC1zaXplOiAxMnB4O1xufVxuXG4ucHJvZmlsZS1lZGl0IHtcbiAgY29sb3I6ICMwMDA7XG4gIGZvbnQtc2l6ZTogMjFweDtcbiAgdG9wOiA1NXB4O1xuICByaWdodDogLTEycHg7XG4gIHBvc2l0aW9uOiBhYnNvbHV0ZTtcbiAgYmFja2dyb3VuZDogI2ZmZjtcbiAgd2lkdGg6IDM1cHg7XG4gIGhlaWdodDogMzVweDtcbiAgYm9yZGVyLXJhZGl1czogNTAlO1xuICB0ZXh0LWFsaWduOiBjZW50ZXI7XG4gIGJvcmRlcjogMnB4IHNvbGlkICMwMDA7XG59XG5cbi51c2VyLW1ldGEge1xuICBwYWRkaW5nLXRvcDogMTVweDtcbiAgcGFkZGluZy1ib3R0b206IDVweDtcbn1cblxuLnBsYXllcm5hbWUge1xuICBmb250LXNpemU6IDE0cHg7XG4gIGZvbnQtd2VpZ2h0OiA2MDA7XG4gIGNvbG9yOiAjMzAzOTQwO1xuICBtYXJnaW4tdG9wOiA1cHg7XG4gIG1hcmdpbi1ib3R0b206IDVweDtcbn1cblxuLnVzZXJuYW1lIHtcbiAgZm9udC1zaXplOiA3NSU7XG4gIGNvbG9yOiAjOTQ5ZWE2O1xuICB0ZXh0LXRyYW5zZm9ybTogdXBwZXJjYXNlO1xuICBtYXJnaW46IDAgYXV0bztcbn1cblxuLnNlZ21lbnRPbmUtYnV0dG9uIHtcbiAgLS1yaXBwbGUtY29sb3I6IHRyYW5zcGFyZW50O1xuICBiYWNrZ3JvdW5kOiB0cmFuc3BhcmVudDtcbiAgLS1jb2xvci1jaGVja2VkOiB2YXIoLS1pb24tY29sb3ItcHJpbWFyeSk7XG4gIC0tY29sb3I6ICM5YzljOWM7XG4gIC0taW5kaWNhdG9yLWNvbG9yOiB2YXIoLS1pb24tY29sb3ItcHJpbWFyeSk7XG4gIC0tbWFyZ2luLWJvdHRvbTogMHB4O1xuICBoZWlnaHQ6IDQwcHg7XG4gIC0tYm9yZGVyLXJhZGl1czogMjVweDtcbn1cblxuLnNlZ21lbnRPbmUge1xuICBiYWNrZ3JvdW5kOiByZ2JhKDE3MCwgMTcwLCAxNzAsIDAuMTEpO1xuICBib3JkZXItcmFkaXVzOiAyNXB4O1xuICBtYXJnaW4tYm90dG9tOiAxNXB4O1xufVxuXG4ucHJvZmlsZS1zZWdtZW50LWJveCBpbWcge1xuICB3aWR0aDogNDBweDtcbn1cblxuLnByb2ZpbGUtc2VnbWVudC1ib3gge1xuICBwb3NpdGlvbjogcmVsYXRpdmU7XG4gIGJvcmRlci1yYWRpdXM6IDhweDtcbiAgZm9udC1zaXplOiAxNHB4O1xuICBib3gtc2hhZG93OiByZ2JhKDAsIDAsIDAsIDAuMTIpIDBweCA0cHggMTZweDtcbiAgcGFkZGluZzogMTBweDtcbiAgYm9yZGVyOiAxcHggc29saWQgIzAwMDtcbn1cblxuLnByb2ZpbGUtc2VnbWVudC1ib3ggcCB7XG4gIGNvbG9yOiAjN2Y3ZjdmO1xuICBmb250LXNpemU6IDEycHg7XG4gIG1hcmdpbi1ibG9jay1zdGFydDogMGVtO1xuICBtYXJnaW4tYmxvY2stZW5kOiAwZW07XG59XG5cbi5wcm9maWxlLXNlZ21lbnQtYm94IGg0IHtcbiAgbWFyZ2luLXRvcDogNXB4O1xuICBtYXJnaW4tYm90dG9tOiAwcHg7XG59XG5cbi5zZWdtZW50LWJ0bi10ZXh0IHtcbiAgZm9udC1mYW1pbHk6IFwiT3N3YWxkLU1lZGl1bVwiO1xufVxuXG5pb24taXRlbSBpb24tbGFiZWwge1xuICB3aGl0ZS1zcGFjZTogbm9ybWFsO1xufVxuXG5pb24taXRlbSB7XG4gIC0taW5uZXItYm9yZGVyLXdpZHRoOiAwcHggMHB4IDBweCAwcHg7XG4gIC0tbWluLWhlaWdodDogMzVweDtcbiAgLS1wYWRkaW5nLXN0YXJ0OiAwcHg7XG4gIC0tYm9yZGVyLXdpZHRoOiAwIDAgMHB4IDA7XG59XG5cbi5oZWlnaHQtNDAge1xuICBoZWlnaHQ6IDQwcHggIWltcG9ydGFudDtcbn1cblxuaW9uLWxpc3Qge1xuICBwYWRkaW5nLXRvcDogMHB4ICFpbXBvcnRhbnQ7XG4gIHBhZGRpbmctYm90dG9tOiAwcHggIWltcG9ydGFudDtcbn1cblxuLnByb2ZpbGUtdmlld2FsbCB7XG4gIHdpZHRoOiAxMDAlO1xuICBwb3NpdGlvbjogcmVsYXRpdmU7XG4gIG1hcmdpbi10b3A6IDE1cHg7XG4gIG1hcmdpbi1ib3R0b206IDE1cHg7XG59XG5cbi5wcm9maWxlLXZpZXdhbGwgaW9uLWJ1dHRvbiB7XG4gIC0tYm9yZGVyLXJhZGl1czogMjVweDtcbiAgZGlzcGxheTogdGFibGU7XG4gIG1hcmdpbjogYXV0bztcbiAgZm9udC1zaXplOiAxMnB4O1xufVxuXG4ucHJvZmlsZS12aWV3YWxsOmJlZm9yZSB7XG4gIGNvbnRlbnQ6IFwiXCI7XG4gIHBvc2l0aW9uOiBhYnNvbHV0ZTtcbiAgYmFja2dyb3VuZDogI2MxYzFjMTtcbiAgaGVpZ2h0OiAxcHg7XG4gIHdpZHRoOiAxMDAlO1xuICB0b3A6IDE5cHg7XG4gIGxlZnQ6IDA7XG59XG5cbmlvbi10b2dnbGUge1xuICAtLWJhY2tncm91bmQ6ICM5MjY0NDQ7XG4gIC0tYmFja2dyb3VuZC1jaGVja2VkOiAjRjRCMTgzO1xuICAtLWhhbmRsZS1iYWNrZ3JvdW5kOiAjRjRCMTgzO1xuICAtLWhhbmRsZS1iYWNrZ3JvdW5kLWNoZWNrZWQ6ICM5MjY0NDQ7XG59XG5cbi5hY3Rpdml0eV90aXRsZSB7XG4gIHRleHQtYWxpZ246IGp1c3RpZnk7XG4gIG1hcmdpbjogMDtcbiAgZm9udC1zaXplOiAxM3B4O1xuICBmb250LXdlaWdodDogNjAwO1xufVxuXG5pb24tYXZhdGFyIHtcbiAgcGFkZGluZzogNXB4O1xuICBtYXJnaW4taW5saW5lLWVuZDogMHB4O1xufVxuXG4uYWN0aXZpdGllc19jb3VudCB7XG4gIHBhZGRpbmctdG9wOiAxMHB4O1xufVxuXG4uc2VnbWVudF9jb250YWluZXJfZGF0YSB7XG4gIHBvc2l0aW9uOiByZWxhdGl2ZTtcbiAgZGlzcGxheTogYmxvY2s7XG4gIHdpZHRoOiAxMDAlO1xuICBtYXJnaW4tdG9wOiAxNXB4O1xufVxuXG4udmFsdWUtdGV4dCB7XG4gIGNvbG9yOiAjMzgzYTNlOWM7XG4gIGZvbnQtc2l6ZTogMTRweDtcbn1cblxuaW9uLWxhYmVsIHAge1xuICBjb2xvcjogIzAwMDtcbn1cblxuLnByb3BlcnR5LXRleHQge1xuICBmb250LWZhbWlseTogXCJPc3dhbGQtU2VtaUJvbGRcIjtcbiAgZm9udC1zaXplOiAxNHB4O1xufVxuXG4uY2hhbmdlLXBhc3Mge1xuICBmb250LWZhbWlseTogXCJPc3dhbGQtU2VtaUJvbGRcIjtcbiAgY29sb3I6ICNmMDA7XG4gIGZvbnQtc2l6ZTogMTRweDtcbn1cblxuaW9uLWl0ZW0gaW9uLWxhYmVsIHtcbiAgZm9udC1zaXplOiAxNHB4O1xuICBmb250LWZhbWlseTogXCJPc3dhbGRcIjtcbn1cblxuLm5leHQge1xuICAtLWJhY2tncm91bmQ6IHZhcigtLWlvbi1idG4tY3VzdG9tLWNvbG9yKTtcbn0iXX0= */");

/***/ }),

/***/ "./src/app/pages/tabs/myaccount/myaccount.page.ts":
/*!********************************************************!*\
  !*** ./src/app/pages/tabs/myaccount/myaccount.page.ts ***!
  \********************************************************/
/*! exports provided: MyaccountPage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "MyaccountPage", function() { return MyaccountPage; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");
/* harmony import */ var _services_userdata_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @services/userdata.service */ "./src/app/services/userdata.service.ts");




let MyaccountPage = class MyaccountPage {
    constructor(router, userData, zone) {
        this.router = router;
        this.userData = userData;
        this.zone = zone;
        this.segmentModel = "performance";
        this.parentSegmentModel = "profile";
        this.parameterSegmentModel = "autorisations";
        this.lastY = 0;
        this.routerWatch();
    }
    ngOnInit() {
    }
    ionViewDidEnter() {
    }
    segmentChanged(event) {
        console.log(this.segmentModel);
        console.log(event);
    }
    parameterSegmentChanged(event) {
        this.parameterSegmentModel = event.detail.value;
        console.log(event);
    }
    routerWatch() {
        this.routerSubscription = this.router.events.subscribe((event) => {
            if (event instanceof _angular_router__WEBPACK_IMPORTED_MODULE_2__["NavigationEnd"]) {
                if (event.url == '/tabs/account') {
                    console.log(111);
                    this.userData.changeColor = true;
                }
            }
        });
    }
    ionPageWillLeave() {
        this.routerSubscription.unsubscribe();
    }
    scrollHandler(event) {
        this.zone.run(() => {
            this.lastY = 5;
            if (event.detail.scrollTop > this.lastY) {
                let elem = document.querySelector('ion-tab-bar');
                elem.style.setProperty('margin-bottom', "-100px");
                elem.style.setProperty('transition', "0.5s");
            }
            else {
                let elem = document.querySelector('ion-tab-bar');
                elem.style.setProperty('margin-bottom', "10px");
                elem.style.setProperty('transition', "0.5s");
            }
            this.lastY = event.detail.scrollTop;
        });
    }
};
MyaccountPage.ctorParameters = () => [
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"] },
    { type: _services_userdata_service__WEBPACK_IMPORTED_MODULE_3__["UserdataService"] },
    { type: _angular_core__WEBPACK_IMPORTED_MODULE_1__["NgZone"] }
];
MyaccountPage.propDecorators = {
    contentHandle: [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_1__["ViewChild"], args: ["contentRef",] }]
};
MyaccountPage = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-myaccount',
        template: Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(/*! raw-loader!./myaccount.page.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/pages/tabs/myaccount/myaccount.page.html")).default,
        styles: [Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(/*! ./myaccount.page.scss */ "./src/app/pages/tabs/myaccount/myaccount.page.scss")).default]
    })
], MyaccountPage);



/***/ })

}]);
//# sourceMappingURL=myaccount-myaccount-module-es2015.js.map