(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["pages-body-scan-step-coach-body-scan-step-coach-module"],{

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/pages/body-scan-step-coach/body-scan-step-coach.page.html":
/*!*****************************************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/pages/body-scan-step-coach/body-scan-step-coach.page.html ***!
  \*****************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<ion-header>\n  <ion-grid>\n    <ion-row>\n      <ion-col>\n        <div [class]=\"activepage == 1 ? 'steps active' : 'steps' \"></div>\n      </ion-col>\n      <ion-col>\n        <!-- <div ng-class=\"(activepage == 2) ? 'active' : 'steps'\"></div> -->\n        <div [class]=\"activepage == 2 ? 'steps active' : 'steps' \"></div>\n      </ion-col>\n      <ion-col>\n        <div [class]=\"activepage == 3 ? 'steps active' : 'steps' \"></div>\n      </ion-col>\n\n    </ion-row>\n  </ion-grid>\n</ion-header>\n<!--=================================-->\n<!--=================================-->\n<ion-content>\n  <!--=================================-->\n  <app-coach-body-scan-step-one *ngIf=\"activepage == 1\"> </app-coach-body-scan-step-one>\n  <!--=================================-->\n  <!--=================================-->\n  <app-coach-body-scan-step-two *ngIf=\"activepage == 2\"> </app-coach-body-scan-step-two>\n  <!--=================================-->\n  <!--=================================-->\n  <app-coach-body-scan-step-three *ngIf=\"activepage == 3\"> </app-coach-body-scan-step-three>\n  <!--=================================-->\n\n</ion-content>\n<!--=================================-->\n<!--=================================-->\n<ion-footer>\n  <ion-toolbar>\n    <ion-grid>\n      <ion-row>\n        <ion-col>\n          <ion-button\n            class=\"back\"\n            expand=\"full\"\n            shape=\"round\"\n            (click)=\"previousPage()\"\n            >Retour\n          </ion-button>\n        </ion-col>\n        <ion-col>\n          <ion-button\n            class=\"next\"\n            expand=\"full\"\n            shape=\"round\"\n            (click)=\"nextPage()\"\n            >Etape suivante\n          </ion-button>\n        </ion-col>\n      </ion-row>\n    </ion-grid>\n  </ion-toolbar>\n</ion-footer>\n");

/***/ }),

/***/ "./src/app/pages/body-scan-step-coach/body-scan-step-coach-routing.module.ts":
/*!***********************************************************************************!*\
  !*** ./src/app/pages/body-scan-step-coach/body-scan-step-coach-routing.module.ts ***!
  \***********************************************************************************/
/*! exports provided: BodyScanStepCoachPageRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "BodyScanStepCoachPageRoutingModule", function() { return BodyScanStepCoachPageRoutingModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");
/* harmony import */ var _body_scan_step_coach_page__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./body-scan-step-coach.page */ "./src/app/pages/body-scan-step-coach/body-scan-step-coach.page.ts");




const routes = [
    {
        path: '',
        component: _body_scan_step_coach_page__WEBPACK_IMPORTED_MODULE_3__["BodyScanStepCoachPage"]
    }
];
let BodyScanStepCoachPageRoutingModule = class BodyScanStepCoachPageRoutingModule {
};
BodyScanStepCoachPageRoutingModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]],
    })
], BodyScanStepCoachPageRoutingModule);



/***/ }),

/***/ "./src/app/pages/body-scan-step-coach/body-scan-step-coach.module.ts":
/*!***************************************************************************!*\
  !*** ./src/app/pages/body-scan-step-coach/body-scan-step-coach.module.ts ***!
  \***************************************************************************/
/*! exports provided: BodyScanStepCoachPageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "BodyScanStepCoachPageModule", function() { return BodyScanStepCoachPageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/__ivy_ngcc__/fesm2015/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/__ivy_ngcc__/fesm2015/forms.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/__ivy_ngcc__/fesm2015/ionic-angular.js");
/* harmony import */ var _body_scan_step_coach_routing_module__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./body-scan-step-coach-routing.module */ "./src/app/pages/body-scan-step-coach/body-scan-step-coach-routing.module.ts");
/* harmony import */ var _body_scan_step_coach_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./body-scan-step-coach.page */ "./src/app/pages/body-scan-step-coach/body-scan-step-coach.page.ts");
/* harmony import */ var _components_shared_module__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! @components/shared.module */ "./src/app/components/shared.module.ts");








let BodyScanStepCoachPageModule = class BodyScanStepCoachPageModule {
};
BodyScanStepCoachPageModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [
            _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonicModule"],
            _body_scan_step_coach_routing_module__WEBPACK_IMPORTED_MODULE_5__["BodyScanStepCoachPageRoutingModule"],
            _components_shared_module__WEBPACK_IMPORTED_MODULE_7__["SharedModule"]
        ],
        declarations: [_body_scan_step_coach_page__WEBPACK_IMPORTED_MODULE_6__["BodyScanStepCoachPage"]]
    })
], BodyScanStepCoachPageModule);



/***/ }),

/***/ "./src/app/pages/body-scan-step-coach/body-scan-step-coach.page.scss":
/*!***************************************************************************!*\
  !*** ./src/app/pages/body-scan-step-coach/body-scan-step-coach.page.scss ***!
  \***************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = (".steps {\n  height: 5px;\n  background: #000;\n  border-radius: 10px;\n}\n\n.steps.active {\n  background: #c1c1c1;\n}\n\n.back,\n.next {\n  --ion-button-round-border-radius: 5px;\n  --ion-button-border-radius: 5px;\n  --ion-border-radius: 5px;\n  --border-radius: 5px;\n  height: 40px;\n}\n\n.header-md::after {\n  background-image: none;\n}\n\n.next {\n  --background: var(--ion-btn-custom-color);\n}\n\n.back {\n  --background: linear-gradient(to right, #161616e0, #c1c1c1);\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvcGFnZXMvYm9keS1zY2FuLXN0ZXAtY29hY2gvYm9keS1zY2FuLXN0ZXAtY29hY2gucGFnZS5zY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBO0VBQ0ksV0FBQTtFQUNBLGdCQUFBO0VBQ0EsbUJBQUE7QUFDSjs7QUFFQTtFQUNJLG1CQUFBO0FBQ0o7O0FBRUE7O0VBRUkscUNBQUE7RUFDQSwrQkFBQTtFQUNBLHdCQUFBO0VBQ0Esb0JBQUE7RUFDQSxZQUFBO0FBQ0o7O0FBRUE7RUFDSSxzQkFBQTtBQUNKOztBQUVBO0VBQ0kseUNBQUE7QUFDSjs7QUFHQTtFQUNJLDJEQUFBO0FBQUoiLCJmaWxlIjoic3JjL2FwcC9wYWdlcy9ib2R5LXNjYW4tc3RlcC1jb2FjaC9ib2R5LXNjYW4tc3RlcC1jb2FjaC5wYWdlLnNjc3MiLCJzb3VyY2VzQ29udGVudCI6WyIuc3RlcHMge1xuICAgIGhlaWdodDogNXB4O1xuICAgIGJhY2tncm91bmQ6ICMwMDA7XG4gICAgYm9yZGVyLXJhZGl1czogMTBweDtcbn1cblxuLnN0ZXBzLmFjdGl2ZSB7XG4gICAgYmFja2dyb3VuZDogI2MxYzFjMTtcbn1cblxuLmJhY2ssXG4ubmV4dCB7XG4gICAgLS1pb24tYnV0dG9uLXJvdW5kLWJvcmRlci1yYWRpdXM6IDVweDtcbiAgICAtLWlvbi1idXR0b24tYm9yZGVyLXJhZGl1czogNXB4O1xuICAgIC0taW9uLWJvcmRlci1yYWRpdXM6IDVweDtcbiAgICAtLWJvcmRlci1yYWRpdXM6IDVweDtcbiAgICBoZWlnaHQ6IDQwcHg7XG59XG5cbi5oZWFkZXItbWQ6OmFmdGVyIHtcbiAgICBiYWNrZ3JvdW5kLWltYWdlOiBub25lO1xufVxuXG4ubmV4dCB7XG4gICAgLS1iYWNrZ3JvdW5kOiB2YXIoLS1pb24tYnRuLWN1c3RvbS1jb2xvcik7XG5cbn1cblxuLmJhY2sge1xuICAgIC0tYmFja2dyb3VuZDogbGluZWFyLWdyYWRpZW50KHRvIHJpZ2h0LCAjMTYxNjE2ZTAsICNjMWMxYzEpO1xufVxuXG5cbiJdfQ== */");

/***/ }),

/***/ "./src/app/pages/body-scan-step-coach/body-scan-step-coach.page.ts":
/*!*************************************************************************!*\
  !*** ./src/app/pages/body-scan-step-coach/body-scan-step-coach.page.ts ***!
  \*************************************************************************/
/*! exports provided: BodyScanStepCoachPage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "BodyScanStepCoachPage", function() { return BodyScanStepCoachPage; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");



let BodyScanStepCoachPage = class BodyScanStepCoachPage {
    constructor(router, route) {
        this.router = router;
        this.route = route;
        this.activepage = 1;
    }
    ngOnInit() {
        console.log(this.activepage);
    }
    nextPage() {
        if (this.activepage >= 3) {
            this.router.navigate(['/coach-acivity']);
            this.activepage = 1;
            return;
        }
        this.activepage += 1;
        console.log("next", this.activepage);
    }
    previousPage() {
        if (this.activepage <= 1) {
            this.activepage = 1;
            this.router.navigate(['body-scan-arrival']);
            return;
        }
        this.activepage -= 1;
        console.log("next", this.activepage);
    }
};
BodyScanStepCoachPage.ctorParameters = () => [
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"] },
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_2__["ActivatedRoute"] }
];
BodyScanStepCoachPage = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-body-scan-step-coach',
        template: Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(/*! raw-loader!./body-scan-step-coach.page.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/pages/body-scan-step-coach/body-scan-step-coach.page.html")).default,
        styles: [Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(/*! ./body-scan-step-coach.page.scss */ "./src/app/pages/body-scan-step-coach/body-scan-step-coach.page.scss")).default]
    })
], BodyScanStepCoachPage);



/***/ })

}]);
//# sourceMappingURL=pages-body-scan-step-coach-body-scan-step-coach-module-es2015.js.map