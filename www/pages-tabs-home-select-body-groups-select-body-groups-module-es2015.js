(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["pages-tabs-home-select-body-groups-select-body-groups-module"],{

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/pages/tabs/home/select-body-groups/select-body-groups.page.html":
/*!***********************************************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/pages/tabs/home/select-body-groups/select-body-groups.page.html ***!
  \***********************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<ion-content>\n    <app-bodysvg *ngIf=\"face === 'front'\"></app-bodysvg>\n    <app-bodysvgback *ngIf=\"face === 'back'\"></app-bodysvgback>\n    <ion-grid>\n        <ion-row style=\"justify-content: center;\">\n            <ion-img (click)=\"flipImage()\" class=\"back_icon\" src=\"assets/icon/switch_body.png\"></ion-img>\n        </ion-row>\n    </ion-grid>\n</ion-content>\n\n<ion-footer>\n    <ion-toolbar>\n        <ion-grid>\n            <ion-row>\n                <ion-col size=\"3\">\n                    <ion-button class=\"back\" expand=\"full\" shape=\"round\" routerLink=\"/add-exercise\">\n                        <img src=\"assets/icon/back-triangle.png\" alt=\"\" class=\"\">\n                    </ion-button>\n                </ion-col>\n                <ion-col>\n                    <ion-button routerLink=\"/add-exercise\" class=\"next\" expand=\"full\" shape=\"round\">Ajouter [x] exercices\n                    </ion-button>\n                </ion-col>\n            </ion-row>\n        </ion-grid>\n    </ion-toolbar>\n</ion-footer>");

/***/ }),

/***/ "./src/app/pages/tabs/home/select-body-groups/select-body-groups-routing.module.ts":
/*!*****************************************************************************************!*\
  !*** ./src/app/pages/tabs/home/select-body-groups/select-body-groups-routing.module.ts ***!
  \*****************************************************************************************/
/*! exports provided: SelectBodyGroupsPageRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SelectBodyGroupsPageRoutingModule", function() { return SelectBodyGroupsPageRoutingModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");
/* harmony import */ var _select_body_groups_page__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./select-body-groups.page */ "./src/app/pages/tabs/home/select-body-groups/select-body-groups.page.ts");




const routes = [
    {
        path: '',
        component: _select_body_groups_page__WEBPACK_IMPORTED_MODULE_3__["SelectBodyGroupsPage"]
    }
];
let SelectBodyGroupsPageRoutingModule = class SelectBodyGroupsPageRoutingModule {
};
SelectBodyGroupsPageRoutingModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]],
    })
], SelectBodyGroupsPageRoutingModule);



/***/ }),

/***/ "./src/app/pages/tabs/home/select-body-groups/select-body-groups.module.ts":
/*!*********************************************************************************!*\
  !*** ./src/app/pages/tabs/home/select-body-groups/select-body-groups.module.ts ***!
  \*********************************************************************************/
/*! exports provided: SelectBodyGroupsPageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SelectBodyGroupsPageModule", function() { return SelectBodyGroupsPageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/__ivy_ngcc__/fesm2015/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/__ivy_ngcc__/fesm2015/forms.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/__ivy_ngcc__/fesm2015/ionic-angular.js");
/* harmony import */ var _select_body_groups_routing_module__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./select-body-groups-routing.module */ "./src/app/pages/tabs/home/select-body-groups/select-body-groups-routing.module.ts");
/* harmony import */ var _select_body_groups_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./select-body-groups.page */ "./src/app/pages/tabs/home/select-body-groups/select-body-groups.page.ts");
/* harmony import */ var _components_shared_module__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! @components/shared.module */ "./src/app/components/shared.module.ts");








let SelectBodyGroupsPageModule = class SelectBodyGroupsPageModule {
};
SelectBodyGroupsPageModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [
            _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonicModule"],
            _select_body_groups_routing_module__WEBPACK_IMPORTED_MODULE_5__["SelectBodyGroupsPageRoutingModule"],
            _components_shared_module__WEBPACK_IMPORTED_MODULE_7__["SharedModule"]
        ],
        declarations: [_select_body_groups_page__WEBPACK_IMPORTED_MODULE_6__["SelectBodyGroupsPage"]]
    })
], SelectBodyGroupsPageModule);



/***/ }),

/***/ "./src/app/pages/tabs/home/select-body-groups/select-body-groups.page.scss":
/*!*********************************************************************************!*\
  !*** ./src/app/pages/tabs/home/select-body-groups/select-body-groups.page.scss ***!
  \*********************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("ion-content {\n  --padding-top: 10%;\n  --padding-start: 3%;\n  --padding-bottom: 5%;\n}\n\nion-item {\n  width: 100%;\n}\n\n.search_section {\n  padding-left: 20px;\n  padding-right: 20px;\n}\n\n.search_section ion-input {\n  background: #f1f0f0;\n  border-radius: 12px;\n  --padding-start: 10px;\n}\n\n.select_body_section {\n  margin-left: 20px;\n  margin-top: 10px;\n  margin-right: 20px;\n  border-style: dotted;\n  border-color: #696969;\n  padding: 5px;\n  justify-content: center;\n  border-width: 2px;\n  border-radius: 10px;\n}\n\n.black_box {\n  width: 50px;\n  background: #000;\n  height: 35px;\n}\n\n.list_section_title {\n  padding-left: 10px;\n}\n\n.footer_section_btn {\n  width: 100%;\n  padding-left: 10px;\n  padding-right: 10px;\n  margin-top: 32px;\n  --background: var(--ion-btn-custom-color);\n}\n\n.back,\n.next {\n  --ion-button-round-border-radius: 5px;\n  --ion-button-border-radius: 5px;\n  --ion-border-radius: 5px;\n  --border-radius: 5px;\n  height: 40px;\n}\n\n.next {\n  --background: var(--ion-btn-custom-color);\n}\n\n.back {\n  --background: linear-gradient(to right, #161616e0, #c1c1c1);\n}\n\n.back img {\n  width: 70%;\n}\n\nimg {\n  max-width: 65%;\n}\n\n.back_icon {\n  position: absolute;\n  width: 7%;\n  right: 10%;\n  top: 10%;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvcGFnZXMvdGFicy9ob21lL3NlbGVjdC1ib2R5LWdyb3Vwcy9zZWxlY3QtYm9keS1ncm91cHMucGFnZS5zY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBO0VBQ0ksa0JBQUE7RUFDQSxtQkFBQTtFQUNBLG9CQUFBO0FBQ0o7O0FBSUE7RUFDSSxXQUFBO0FBREo7O0FBSUE7RUFDSSxrQkFBQTtFQUNBLG1CQUFBO0FBREo7O0FBRUk7RUFDSSxtQkFBQTtFQUNBLG1CQUFBO0VBQ0EscUJBQUE7QUFBUjs7QUFLQTtFQUNJLGlCQUFBO0VBQ0EsZ0JBQUE7RUFDQSxrQkFBQTtFQUNBLG9CQUFBO0VBQ0EscUJBQUE7RUFDQSxZQUFBO0VBQ0EsdUJBQUE7RUFDQSxpQkFBQTtFQUNBLG1CQUFBO0FBRko7O0FBWUE7RUFDSSxXQUFBO0VBQ0EsZ0JBQUE7RUFDQSxZQUFBO0FBVEo7O0FBWUE7RUFDSSxrQkFBQTtBQVRKOztBQWNBO0VBRUksV0FBQTtFQUNBLGtCQUFBO0VBQ0EsbUJBQUE7RUFDQSxnQkFBQTtFQUNBLHlDQUFBO0FBWko7O0FBZ0JBOztFQUVJLHFDQUFBO0VBQ0EsK0JBQUE7RUFDQSx3QkFBQTtFQUNBLG9CQUFBO0VBQ0EsWUFBQTtBQWJKOztBQWlCQTtFQUNJLHlDQUFBO0FBZEo7O0FBa0JBO0VBQ0ksMkRBQUE7QUFmSjs7QUFnQkk7RUFDSSxVQUFBO0FBZFI7O0FBbUJBO0VBQ0ksY0FBQTtBQWhCSjs7QUFtQkE7RUFDSSxrQkFBQTtFQUNBLFNBQUE7RUFDQSxVQUFBO0VBQ0EsUUFBQTtBQWhCSiIsImZpbGUiOiJzcmMvYXBwL3BhZ2VzL3RhYnMvaG9tZS9zZWxlY3QtYm9keS1ncm91cHMvc2VsZWN0LWJvZHktZ3JvdXBzLnBhZ2Uuc2NzcyIsInNvdXJjZXNDb250ZW50IjpbImlvbi1jb250ZW50IHtcbiAgICAtLXBhZGRpbmctdG9wOiAxMCU7XG4gICAgLS1wYWRkaW5nLXN0YXJ0OiAzJTtcbiAgICAtLXBhZGRpbmctYm90dG9tOiA1JTtcbn1cblxuXG5cbmlvbi1pdGVtIHtcbiAgICB3aWR0aDogMTAwJTtcbn1cblxuLnNlYXJjaF9zZWN0aW9uIHtcbiAgICBwYWRkaW5nLWxlZnQ6IDIwcHg7XG4gICAgcGFkZGluZy1yaWdodDogMjBweDtcbiAgICBpb24taW5wdXQge1xuICAgICAgICBiYWNrZ3JvdW5kOiAjZjFmMGYwO1xuICAgICAgICBib3JkZXItcmFkaXVzOiAxMnB4O1xuICAgICAgICAtLXBhZGRpbmctc3RhcnQ6IDEwcHg7XG4gICAgfVxufVxuXG5cbi5zZWxlY3RfYm9keV9zZWN0aW9uIHtcbiAgICBtYXJnaW4tbGVmdDogMjBweDtcbiAgICBtYXJnaW4tdG9wOiAxMHB4O1xuICAgIG1hcmdpbi1yaWdodDogMjBweDtcbiAgICBib3JkZXItc3R5bGU6IGRvdHRlZDtcbiAgICBib3JkZXItY29sb3I6ICM2OTY5Njk7XG4gICAgcGFkZGluZzogNXB4O1xuICAgIGp1c3RpZnktY29udGVudDogY2VudGVyO1xuICAgIGJvcmRlci13aWR0aDogMnB4O1xuICAgIGJvcmRlci1yYWRpdXM6IDEwcHg7XG4gICAgXG59XG5cblxuLmxpc3Rfc2VjdGlvbiB7XG4gICAgXG59XG5cblxuLmJsYWNrX2JveCB7XG4gICAgd2lkdGg6IDUwcHg7XG4gICAgYmFja2dyb3VuZDogIzAwMDtcbiAgICBoZWlnaHQ6IDM1cHhcbn1cblxuLmxpc3Rfc2VjdGlvbl90aXRsZSB7XG4gICAgcGFkZGluZy1sZWZ0OiAxMHB4O1xufVxuXG5cblxuLmZvb3Rlcl9zZWN0aW9uX2J0biB7XG5cbiAgICB3aWR0aDogMTAwJTtcbiAgICBwYWRkaW5nLWxlZnQ6IDEwcHg7XG4gICAgcGFkZGluZy1yaWdodDogMTBweDtcbiAgICBtYXJnaW4tdG9wOiAzMnB4O1xuICAgIC0tYmFja2dyb3VuZDogdmFyKC0taW9uLWJ0bi1jdXN0b20tY29sb3IpO1xufVxuXG5cbi5iYWNrLFxuLm5leHQge1xuICAgIC0taW9uLWJ1dHRvbi1yb3VuZC1ib3JkZXItcmFkaXVzOiA1cHg7XG4gICAgLS1pb24tYnV0dG9uLWJvcmRlci1yYWRpdXM6IDVweDtcbiAgICAtLWlvbi1ib3JkZXItcmFkaXVzOiA1cHg7XG4gICAgLS1ib3JkZXItcmFkaXVzOiA1cHg7XG4gICAgaGVpZ2h0OiA0MHB4O1xufVxuXG5cbi5uZXh0IHtcbiAgICAtLWJhY2tncm91bmQ6IHZhcigtLWlvbi1idG4tY3VzdG9tLWNvbG9yKTtcblxufVxuXG4uYmFjayB7XG4gICAgLS1iYWNrZ3JvdW5kOiBsaW5lYXItZ3JhZGllbnQodG8gcmlnaHQsICMxNjE2MTZlMCwgI2MxYzFjMSk7XG4gICAgaW1nIHtcbiAgICAgICAgd2lkdGg6IDcwJTtcbiAgICB9XG59XG5cblxuaW1nIHtcbiAgICBtYXgtd2lkdGg6IDY1JTtcbn1cblxuLmJhY2tfaWNvbiB7XG4gICAgcG9zaXRpb246IGFic29sdXRlO1xuICAgIHdpZHRoOiA3JTtcbiAgICByaWdodDogMTAlO1xuICAgIHRvcDogMTAlO1xuXG59Il19 */");

/***/ }),

/***/ "./src/app/pages/tabs/home/select-body-groups/select-body-groups.page.ts":
/*!*******************************************************************************!*\
  !*** ./src/app/pages/tabs/home/select-body-groups/select-body-groups.page.ts ***!
  \*******************************************************************************/
/*! exports provided: SelectBodyGroupsPage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SelectBodyGroupsPage", function() { return SelectBodyGroupsPage; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");


let SelectBodyGroupsPage = class SelectBodyGroupsPage {
    constructor() {
        this.face = "front";
    }
    ngOnInit() {
    }
    flipImage() {
        console.log("clicked");
        if (this.face === "front") {
            this.face = "back";
        }
        else {
            this.face = "front";
        }
    }
};
SelectBodyGroupsPage.ctorParameters = () => [];
SelectBodyGroupsPage = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-select-body-groups',
        template: Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(/*! raw-loader!./select-body-groups.page.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/pages/tabs/home/select-body-groups/select-body-groups.page.html")).default,
        styles: [Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(/*! ./select-body-groups.page.scss */ "./src/app/pages/tabs/home/select-body-groups/select-body-groups.page.scss")).default]
    })
], SelectBodyGroupsPage);



/***/ })

}]);
//# sourceMappingURL=pages-tabs-home-select-body-groups-select-body-groups-module-es2015.js.map