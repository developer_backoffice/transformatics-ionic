(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["pages-start-begin-the-session-start-begin-the-session-module"],{

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/pages/start-begin-the-session/start-begin-the-session.page.html":
/*!***********************************************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/pages/start-begin-the-session/start-begin-the-session.page.html ***!
  \***********************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<ion-content>\n    <div class=\"titlebox\">\n        <div class=\"titlebox-arrow\">\n            <div>\n                <ion-icon name=\"chevron-back-outline\"></ion-icon>\n            </div>\n        </div>\n        <div class=\"title-img\">\n            <img src=\"assets/images/edit_physical_activity.png\">\n        </div>\n        <div class=\"titlebox-text\">\n            <h5>DAY 2</h5>\n            <h4>PULL</h4>\n\n        </div>\n\n    </div>\n\n    <ion-grid>\n        <div class=\"titlebox-add\" routerLink=\"/pendant-la-seance\">\n            <ion-icon name=\"play-outline\"></ion-icon>\n        </div>\n        <ion-row>\n            <ion-col size=\"12\">\n                <h4 class=\"f14\">\n                    SEANCES D’ENTRAINEMENT TRANSFORMATICS\n                </h4>\n            </ion-col>\n            <ion-col size=\"12\">\n                <ion-item>\n                    <img slot=\"start\" src=\"assets/images/MODELE 2D TRAINING.png\" style=\"width: 100px;\">\n                    <ion-label>\n                        <h4>Groupes musculaires sollicités : </h4>\n                        <p>Pectoraux</p>\n                    </ion-label>\n                </ion-item>\n            </ion-col>\n        </ion-row>\n        <ion-row>\n            <ion-col size=\"12\"></ion-col>\n            <h4 class=\"f14\">\n                EXERCISE PREVIEWS\n            </h4>\n            <ion-col size=\"12\">\n                <ion-slides [options]=\"categories\" pager=\"false\">\n                    <ion-slide>\n                        <div class=\"musculetion-vid\">\n                            <div class=\"musculetion-vid-img\">\n                                <img src=\"assets/images/musculetion-auto.png\">\n                                <ion-icon name=\"play-circle-outline\"></ion-icon>\n                            </div>\n                            <div class=\"musculetion-vid-title\">\n                                <h4>[Nom de l’exercice]</h4>\n                                <h6>3 séries</h6>\n                            </div>\n                        </div>\n                    </ion-slide>\n                    <ion-slide>\n                        <div class=\"musculetion-vid\">\n                            <div class=\"musculetion-vid-img\">\n                                <img src=\"assets/images/payment_slide_3.png\">\n                                <ion-icon name=\"play-circle-outline\"></ion-icon>\n                            </div>\n                            <div class=\"musculetion-vid-title\">\n                                <h4>[Nom de l’exercice]</h4>\n                                <h6>3 séries</h6>\n                            </div>\n                        </div>\n                    </ion-slide>\n                    <ion-slide>\n                        <div class=\"musculetion-vid\">\n                            <div class=\"musculetion-vid-img\">\n                                <img src=\"assets/images/payment_slide_2.png\">\n                                <ion-icon name=\"play-circle-outline\"></ion-icon>\n                            </div>\n                            <div class=\"musculetion-vid-title\">\n                                <h4>[Nom de l’exercice]</h4>\n                                <h6>3 séries</h6>\n                            </div>\n                        </div>\n                    </ion-slide>\n                    <ion-slide>\n                        <div class=\"musculetion-vid\">\n                            <div class=\"musculetion-vid-img\">\n                                <img src=\"assets/images/musculetion-auto.png\">\n                                <ion-icon name=\"play-circle-outline\"></ion-icon>\n                            </div>\n                            <div class=\"musculetion-vid-title\">\n                                <h4>[Nom de l’exercice]</h4>\n                                <h6>3 séries</h6>\n                            </div>\n                        </div>\n                    </ion-slide>\n                    <ion-slide>\n                        <div class=\"musculetion-vid\">\n                            <div class=\"musculetion-vid-img\">\n                                <img src=\"assets/images/musculetion-auto.png\">\n                                <ion-icon name=\"play-circle-outline\"></ion-icon>\n                            </div>\n                            <div class=\"musculetion-vid-title\">\n                                <h4>[Nom de l’exercice]</h4>\n                                <h6>3 séries</h6>\n                            </div>\n                        </div>\n                    </ion-slide>\n                </ion-slides>\n            </ion-col>\n        </ion-row>\n    </ion-grid>\n</ion-content>");

/***/ }),

/***/ "./src/app/pages/start-begin-the-session/start-begin-the-session-routing.module.ts":
/*!*****************************************************************************************!*\
  !*** ./src/app/pages/start-begin-the-session/start-begin-the-session-routing.module.ts ***!
  \*****************************************************************************************/
/*! exports provided: StartBeginTheSessionPageRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "StartBeginTheSessionPageRoutingModule", function() { return StartBeginTheSessionPageRoutingModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");
/* harmony import */ var _start_begin_the_session_page__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./start-begin-the-session.page */ "./src/app/pages/start-begin-the-session/start-begin-the-session.page.ts");




const routes = [
    {
        path: '',
        component: _start_begin_the_session_page__WEBPACK_IMPORTED_MODULE_3__["StartBeginTheSessionPage"]
    }
];
let StartBeginTheSessionPageRoutingModule = class StartBeginTheSessionPageRoutingModule {
};
StartBeginTheSessionPageRoutingModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]],
    })
], StartBeginTheSessionPageRoutingModule);



/***/ }),

/***/ "./src/app/pages/start-begin-the-session/start-begin-the-session.module.ts":
/*!*********************************************************************************!*\
  !*** ./src/app/pages/start-begin-the-session/start-begin-the-session.module.ts ***!
  \*********************************************************************************/
/*! exports provided: StartBeginTheSessionPageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "StartBeginTheSessionPageModule", function() { return StartBeginTheSessionPageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/__ivy_ngcc__/fesm2015/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/__ivy_ngcc__/fesm2015/forms.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/__ivy_ngcc__/fesm2015/ionic-angular.js");
/* harmony import */ var _start_begin_the_session_routing_module__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./start-begin-the-session-routing.module */ "./src/app/pages/start-begin-the-session/start-begin-the-session-routing.module.ts");
/* harmony import */ var _start_begin_the_session_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./start-begin-the-session.page */ "./src/app/pages/start-begin-the-session/start-begin-the-session.page.ts");







let StartBeginTheSessionPageModule = class StartBeginTheSessionPageModule {
};
StartBeginTheSessionPageModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [
            _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonicModule"],
            _start_begin_the_session_routing_module__WEBPACK_IMPORTED_MODULE_5__["StartBeginTheSessionPageRoutingModule"]
        ],
        declarations: [_start_begin_the_session_page__WEBPACK_IMPORTED_MODULE_6__["StartBeginTheSessionPage"]]
    })
], StartBeginTheSessionPageModule);



/***/ }),

/***/ "./src/app/pages/start-begin-the-session/start-begin-the-session.page.scss":
/*!*********************************************************************************!*\
  !*** ./src/app/pages/start-begin-the-session/start-begin-the-session.page.scss ***!
  \*********************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = (".titlebox {\n  position: relative;\n  color: #fff;\n  height: 250px;\n  overflow: hidden;\n}\n\n.titlebox-arrow {\n  display: flex;\n  position: absolute;\n  width: 100%;\n  top: 5%;\n  padding: 0px 5%;\n  transform: translateY(-5%);\n}\n\n.titlebox-arrow div {\n  flex-grow: 1;\n  font-size: 30px;\n}\n\n.titlebox-text {\n  position: absolute;\n  bottom: 5%;\n  left: 5%;\n}\n\n.title-img {\n  background: #000;\n  height: 100%;\n}\n\n.title-img img {\n  height: 100%;\n  -o-object-fit: cover;\n     object-fit: cover;\n  width: 100%;\n  -o-object-position: top;\n     object-position: top;\n}\n\n.titlebox-text h4 {\n  margin: 0;\n}\n\n.titlebox-text h5 {\n  font-size: 12px;\n}\n\n.musculetion-vid h4 {\n  font-size: 12px;\n  text-align: center;\n  margin: 0;\n  margin-top: 5px;\n}\n\n.musculetion-vid {\n  width: 100%;\n}\n\n.musculetion-vid-img img {\n  -o-object-fit: cover;\n     object-fit: cover;\n  -o-object-position: top;\n     object-position: top;\n  height: 90px;\n  width: 90px;\n  border-radius: 50%;\n}\n\nion-item {\n  --inner-border-width: 0 0 0px 0;\n  --border-width: 0 0 0px 0;\n}\n\n.musculetion-vid-title h6 {\n  margin: 0;\n  font-size: 10px;\n  margin-top: 5px;\n}\n\n.titlebox-add {\n  position: relative;\n}\n\n.titlebox-add ion-icon {\n  position: absolute;\n  display: flex;\n  top: -26px;\n  right: 3%;\n  color: #fff;\n  border-radius: 25px;\n  background: var(--ion-btn-custom-color);\n  padding: 4px;\n  font-size: 35px;\n}\n\n.musculetion-vid-img {\n  position: relative;\n}\n\n.musculetion-vid-img ion-icon {\n  position: absolute;\n  left: 30px;\n  top: 30px;\n  background: #fffafab3;\n  border-radius: 25px;\n  font-size: 31px;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvcGFnZXMvc3RhcnQtYmVnaW4tdGhlLXNlc3Npb24vc3RhcnQtYmVnaW4tdGhlLXNlc3Npb24ucGFnZS5zY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBO0VBQ0ksa0JBQUE7RUFDQSxXQUFBO0VBQ0EsYUFBQTtFQUNBLGdCQUFBO0FBQ0o7O0FBRUE7RUFDSSxhQUFBO0VBQ0Esa0JBQUE7RUFDQSxXQUFBO0VBQ0EsT0FBQTtFQUNBLGVBQUE7RUFDQSwwQkFBQTtBQUNKOztBQUVBO0VBQ0ksWUFBQTtFQUNBLGVBQUE7QUFDSjs7QUFFQTtFQUNJLGtCQUFBO0VBQ0EsVUFBQTtFQUNBLFFBQUE7QUFDSjs7QUFFQTtFQUNJLGdCQUFBO0VBQ0EsWUFBQTtBQUNKOztBQUVBO0VBQ0ksWUFBQTtFQUNBLG9CQUFBO0tBQUEsaUJBQUE7RUFDQSxXQUFBO0VBQ0EsdUJBQUE7S0FBQSxvQkFBQTtBQUNKOztBQUVBO0VBQ0ksU0FBQTtBQUNKOztBQUVBO0VBQ0ksZUFBQTtBQUNKOztBQUVBO0VBQ0ksZUFBQTtFQUNBLGtCQUFBO0VBQ0EsU0FBQTtFQUNBLGVBQUE7QUFDSjs7QUFFQTtFQUNJLFdBQUE7QUFDSjs7QUFFQTtFQUNJLG9CQUFBO0tBQUEsaUJBQUE7RUFDQSx1QkFBQTtLQUFBLG9CQUFBO0VBQ0EsWUFBQTtFQUNBLFdBQUE7RUFDQSxrQkFBQTtBQUNKOztBQUVBO0VBQ0ksK0JBQUE7RUFDQSx5QkFBQTtBQUNKOztBQUVBO0VBQ0ksU0FBQTtFQUNBLGVBQUE7RUFDQSxlQUFBO0FBQ0o7O0FBRUE7RUFDSSxrQkFBQTtBQUNKOztBQUdBO0VBQ0ksa0JBQUE7RUFDQSxhQUFBO0VBQ0EsVUFBQTtFQUNBLFNBQUE7RUFDQSxXQUFBO0VBQ0EsbUJBQUE7RUFDQSx1Q0FBQTtFQUNBLFlBQUE7RUFDQSxlQUFBO0FBQUo7O0FBSUE7RUFDSSxrQkFBQTtBQURKOztBQUlBO0VBQ0ksa0JBQUE7RUFDQSxVQUFBO0VBQ0EsU0FBQTtFQUNBLHFCQUFBO0VBQ0EsbUJBQUE7RUFDQSxlQUFBO0FBREoiLCJmaWxlIjoic3JjL2FwcC9wYWdlcy9zdGFydC1iZWdpbi10aGUtc2Vzc2lvbi9zdGFydC1iZWdpbi10aGUtc2Vzc2lvbi5wYWdlLnNjc3MiLCJzb3VyY2VzQ29udGVudCI6WyIudGl0bGVib3gge1xuICAgIHBvc2l0aW9uOiByZWxhdGl2ZTtcbiAgICBjb2xvcjogI2ZmZjtcbiAgICBoZWlnaHQ6IDI1MHB4O1xuICAgIG92ZXJmbG93OiBoaWRkZW47XG59XG5cbi50aXRsZWJveC1hcnJvdyB7XG4gICAgZGlzcGxheTogZmxleDtcbiAgICBwb3NpdGlvbjogYWJzb2x1dGU7XG4gICAgd2lkdGg6IDEwMCU7XG4gICAgdG9wOiA1JTtcbiAgICBwYWRkaW5nOiAwcHggNSU7XG4gICAgdHJhbnNmb3JtOiB0cmFuc2xhdGVZKC01JSk7XG59XG5cbi50aXRsZWJveC1hcnJvdyBkaXYge1xuICAgIGZsZXgtZ3JvdzogMTtcbiAgICBmb250LXNpemU6IDMwcHg7XG59XG5cbi50aXRsZWJveC10ZXh0IHtcbiAgICBwb3NpdGlvbjogYWJzb2x1dGU7XG4gICAgYm90dG9tOiA1JTtcbiAgICBsZWZ0OiA1JTtcbn1cblxuLnRpdGxlLWltZyB7XG4gICAgYmFja2dyb3VuZDogIzAwMDtcbiAgICBoZWlnaHQ6IDEwMCU7XG59XG5cbi50aXRsZS1pbWcgaW1nIHtcbiAgICBoZWlnaHQ6IDEwMCU7XG4gICAgb2JqZWN0LWZpdDogY292ZXI7XG4gICAgd2lkdGg6IDEwMCU7XG4gICAgb2JqZWN0LXBvc2l0aW9uOiB0b3A7XG59XG5cbi50aXRsZWJveC10ZXh0IGg0IHtcbiAgICBtYXJnaW46IDA7XG59XG5cbi50aXRsZWJveC10ZXh0IGg1IHtcbiAgICBmb250LXNpemU6IDEycHg7XG59XG5cbi5tdXNjdWxldGlvbi12aWQgaDQge1xuICAgIGZvbnQtc2l6ZTogMTJweDtcbiAgICB0ZXh0LWFsaWduOiBjZW50ZXI7XG4gICAgbWFyZ2luOiAwO1xuICAgIG1hcmdpbi10b3A6IDVweDtcbn1cblxuLm11c2N1bGV0aW9uLXZpZCB7XG4gICAgd2lkdGg6IDEwMCU7XG59XG5cbi5tdXNjdWxldGlvbi12aWQtaW1nIGltZyB7XG4gICAgb2JqZWN0LWZpdDogY292ZXI7XG4gICAgb2JqZWN0LXBvc2l0aW9uOiB0b3A7XG4gICAgaGVpZ2h0OiA5MHB4O1xuICAgIHdpZHRoOiA5MHB4O1xuICAgIGJvcmRlci1yYWRpdXM6IDUwJTtcbn1cblxuaW9uLWl0ZW0ge1xuICAgIC0taW5uZXItYm9yZGVyLXdpZHRoOiAwIDAgMHB4IDA7XG4gICAgLS1ib3JkZXItd2lkdGg6IDAgMCAwcHggMDtcbn1cblxuLm11c2N1bGV0aW9uLXZpZC10aXRsZSBoNiB7XG4gICAgbWFyZ2luOiAwO1xuICAgIGZvbnQtc2l6ZTogMTBweDtcbiAgICBtYXJnaW4tdG9wOiA1cHg7XG59XG5cbi50aXRsZWJveC1hZGQge1xuICAgIHBvc2l0aW9uOiByZWxhdGl2ZTtcbiAgICBcbn1cblxuLnRpdGxlYm94LWFkZCBpb24taWNvbiB7XG4gICAgcG9zaXRpb246IGFic29sdXRlO1xuICAgIGRpc3BsYXk6IGZsZXg7XG4gICAgdG9wOiAtMjZweDtcbiAgICByaWdodDogMyU7XG4gICAgY29sb3I6ICNmZmY7XG4gICAgYm9yZGVyLXJhZGl1czogMjVweDtcbiAgICBiYWNrZ3JvdW5kOiB2YXIoLS1pb24tYnRuLWN1c3RvbS1jb2xvcik7XG4gICAgcGFkZGluZzogNHB4O1xuICAgIGZvbnQtc2l6ZTogMzVweDtcblxufVxuXG4ubXVzY3VsZXRpb24tdmlkLWltZyB7XG4gICAgcG9zaXRpb246IHJlbGF0aXZlO1xufVxuXG4ubXVzY3VsZXRpb24tdmlkLWltZyBpb24taWNvbiB7XG4gICAgcG9zaXRpb246IGFic29sdXRlO1xuICAgIGxlZnQ6IDMwcHg7XG4gICAgdG9wOiAzMHB4O1xuICAgIGJhY2tncm91bmQ6ICNmZmZhZmFiMztcbiAgICBib3JkZXItcmFkaXVzOiAyNXB4O1xuICAgIGZvbnQtc2l6ZTogMzFweDtcbn0iXX0= */");

/***/ }),

/***/ "./src/app/pages/start-begin-the-session/start-begin-the-session.page.ts":
/*!*******************************************************************************!*\
  !*** ./src/app/pages/start-begin-the-session/start-begin-the-session.page.ts ***!
  \*******************************************************************************/
/*! exports provided: StartBeginTheSessionPage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "StartBeginTheSessionPage", function() { return StartBeginTheSessionPage; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");


let StartBeginTheSessionPage = class StartBeginTheSessionPage {
    constructor() {
        this.categories = {
            slidesPerView: 3.5,
            effect: 'flip',
            slidesPerColumn: 1,
            slidesPerGroup: 1,
            watchSlidesProgress: false,
            spaceBetween: 10,
        };
    }
    ngOnInit() {
    }
};
StartBeginTheSessionPage.ctorParameters = () => [];
StartBeginTheSessionPage = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-start-begin-the-session',
        template: Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(/*! raw-loader!./start-begin-the-session.page.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/pages/start-begin-the-session/start-begin-the-session.page.html")).default,
        styles: [Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(/*! ./start-begin-the-session.page.scss */ "./src/app/pages/start-begin-the-session/start-begin-the-session.page.scss")).default]
    })
], StartBeginTheSessionPage);



/***/ })

}]);
//# sourceMappingURL=pages-start-begin-the-session-start-begin-the-session-module-es2015.js.map