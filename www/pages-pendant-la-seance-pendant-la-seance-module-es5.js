(function () {
  function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

  function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

  function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

  (window["webpackJsonp"] = window["webpackJsonp"] || []).push([["pages-pendant-la-seance-pendant-la-seance-module"], {
    /***/
    "./node_modules/raw-loader/dist/cjs.js!./src/app/pages/pendant-la-seance/pendant-la-seance.page.html":
    /*!***********************************************************************************************************!*\
      !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/pages/pendant-la-seance/pendant-la-seance.page.html ***!
      \***********************************************************************************************************/

    /*! exports provided: default */

    /***/
    function node_modulesRawLoaderDistCjsJsSrcAppPagesPendantLaSeancePendantLaSeancePageHtml(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony default export */


      __webpack_exports__["default"] = "<ion-content>\n    <ion-item>\n        <ion-label class=\"ion-text-center f12\">\n            Avatar Start, Button Item\n        </ion-label>\n        <ion-button class=\"f10 ion-no-margin header-right\" shape=\"round\" expand=\"full\" slot=\"end\">Ecourter </ion-button>\n    </ion-item>\n    <ion-grid class=\"ion-no-padding\">\n        <ion-row class=\"ion-no-padding\">\n            <ion-col size=\"9\" class=\"ion-no-padding\">\n                <div class=\"recovery\">Récupération : 00:17 restantes </div>\n            </ion-col>\n            <ion-col size=\"3\" class=\"ion-no-padding ion-text-right\">\n                <div class=\"pass\"> <span>Passer</span>\n                    <ion-icon name=\"close-outline\"></ion-icon>\n                </div>\n            </ion-col>\n        </ion-row>\n    </ion-grid>\n    <ion-grid>\n        <ion-row>\n            <ion-col size=\"12\">\n                <h4 class=\"f12 text-bold\">BISET – [NOM DE L’EXERCICE]\n                </h4>\n            </ion-col>\n            <ion-col size=\"12\">\n                <ul class=\"serie-no\">\n                    <li>n°1\n                    </li>\n                    <li>n°2\n                    </li>\n                    <li>Série n°3\n                    </li>\n                    <li>n°4\n                    </li>\n                </ul>\n            </ion-col>\n        </ion-row>\n    </ion-grid>\n    <ion-grid class=\"ion-no-padding\">\n        <ion-row class=\"ion-no-padding\">\n            <ion-col size=\"12\" class=\"ion-no-padding\">\n                <div class=\"videobox\">\n                    <div class=\"videobox-img\">\n                        <img src=\"assets/images/edit_physical_activity.png\"> </div>\n                    <div class=\"videobox-icon\">\n                        <div class=\"close\">\n                            <ion-icon name=\"close-outline\"></ion-icon>\n                        </div>\n                        <div class=\"right\">\n                            <ion-icon name=\"checkmark-outline\"></ion-icon>\n                        </div>\n                    </div>\n                </div>\n            </ion-col>\n        </ion-row>\n    </ion-grid>\n    <ion-grid>\n        <ion-row>\n            <ion-col size=\"12\">\n                <div class=\"f12\"><strong>Technique d’intensification </strong>: aucune </div>\n                <div class=\"d-fleex\">\n                    <div class=\"d-fleex-box f12\">\n                        Série suivante : BISET -<strong> [nom de l’exercice]</strong>\n\n                    </div>\n                    <div class=\"d-fleex-box f12 ion-text-right\">\n                        <strong>50</strong>kg x <strong>8</strong>réps\n                    </div>\n                </div>\n            </ion-col>\n            <ion-col size=\"12\">\n                <ion-slides [options]=\"categories\" pager=\"true\">\n                    <ion-slide>\n                        <ion-grid>\n                            <ion-row class=\"ion-justify-content-center\">\n                                <ion-col>\n                                    <div class=\"slider-box\">\n                                        <h4>40</h4>\n                                        <p>kilos</p>\n                                    </div>\n                                </ion-col>\n                                <ion-col size=\"auto\">\n                                    <h4>x</h4>\n                                </ion-col>\n                                <ion-col>\n                                    <div class=\"slider-box\">\n                                        <h4>12</h4>\n                                        <p>répétitions</p>\n                                    </div>\n                                </ion-col>\n                            </ion-row>\n                        </ion-grid>\n                    </ion-slide>\n                    <ion-slide>\n                        <ion-grid>\n                            <ion-row class=\"ion-justify-content-center\">\n                                <ion-col>\n                                    <div class=\"slider-box\">\n                                        <h4>40</h4>\n                                        <p>kilos</p>\n                                    </div>\n                                </ion-col>\n                                <ion-col size=\"auto\">\n                                    <h4>x</h4>\n                                </ion-col>\n                                <ion-col>\n                                    <div class=\"slider-box\">\n                                        <h4>12</h4>\n                                        <p>répétitions</p>\n                                    </div>\n                                </ion-col>\n                            </ion-row>\n                        </ion-grid>\n                    </ion-slide>\n                </ion-slides>\n            </ion-col>\n            <ion-col size=12>\n                <div class=\"doted-box\">\n                    <p>Informations complémentaires sur la série :\n                        <br> 90%1RM --- RPE 9 --- RIR 1 --- TEMPO 4122\n                    </p>\n                </div>\n            </ion-col>\n        </ion-row>\n        <ion-row>\n            <ion-col size=\"2\">\n                <div class=\"number-box ion-text-center\">\n                    10\n                </div>\n            </ion-col>\n            <ion-col size=\"1\">\n                <div class=\"number-box-border\" style=\"background: #ff0000ff;\"></div>\n            </ion-col>\n            <ion-col size=\"9\">\n                <div class=\"number-box-text\">\n                    <strong>Effort maximal</strong>\n                    <br> Je n’aurais pas pu faire 1 répétition de plus\n\n                </div>\n            </ion-col>\n        </ion-row>\n        <ion-row>\n            <ion-col size=\"2\">\n                <div class=\"number-box ion-text-center\">\n                    9\n                </div>\n            </ion-col>\n            <ion-col size=\"1\">\n                <div class=\"number-box-border\" style=\"background: #833c0bff;\"></div>\n            </ion-col>\n            <ion-col size=\"9\">\n                <div class=\"number-box-text\">\n                    <strong>Effort maximal</strong>\n                    <br> Je n’aurais pas pu faire 1 répétition de plus\n\n                </div>\n            </ion-col>\n        </ion-row>\n        <ion-row>\n            <ion-col size=\"2\">\n                <div class=\"number-box ion-text-center\">\n                    8\n                </div>\n            </ion-col>\n            <ion-col size=\"1\">\n                <div class=\"number-box-border\" style=\"background: #ed7d31ff;\"></div>\n            </ion-col>\n            <ion-col size=\"9\">\n                <div class=\"number-box-text\">\n                    <strong>Effort maximal</strong>\n                    <br> Je n’aurais pas pu faire 1 répétition de plus\n\n                </div>\n            </ion-col>\n        </ion-row>\n        <ion-row>\n            <ion-col size=\"2\">\n                <div class=\"number-box ion-text-center\">\n                    7\n                </div>\n            </ion-col>\n            <ion-col size=\"1\">\n                <div class=\"number-box-border\" style=\"background: #ffc000ff;\"></div>\n            </ion-col>\n            <ion-col size=\"9\">\n                <div class=\"number-box-text\">\n                    <strong>Effort maximal</strong>\n                    <br> Je n’aurais pas pu faire 1 répétition de plus\n\n                </div>\n            </ion-col>\n        </ion-row>\n        <ion-row>\n            <ion-col size=\"2\">\n                <div class=\"number-box ion-text-center\">\n                    6\n                </div>\n            </ion-col>\n            <ion-col size=\"1\">\n                <div class=\"number-box-border\" style=\"background: #ffff00ff;\"></div>\n            </ion-col>\n            <ion-col size=\"9\">\n                <div class=\"number-box-text\">\n                    <strong> Effort maximal</strong>\n                    <br> Je n’aurais pas pu faire 1 répétition de plus\n\n                </div>\n            </ion-col>\n        </ion-row>\n        <ion-row>\n            <ion-col size=\"2\">\n                <div class=\"number-box ion-text-center\">\n                    5\n                </div>\n            </ion-col>\n            <ion-col size=\"1\">\n                <div class=\"number-box-border\" style=\"background: #92d050ff;\"></div>\n            </ion-col>\n            <ion-col size=\"9\">\n                <div class=\"number-box-text\">\n                    <strong>   Effort maximal</strong>\n                    <br> Je n’aurais pas pu faire 1 répétition de plus\n\n                </div>\n            </ion-col>\n        </ion-row>\n        <ion-row>\n            <ion-col size=\"2\">\n                <div class=\"number-box ion-text-center\">\n                    1 - 4\n                </div>\n            </ion-col>\n            <ion-col size=\"1\">\n                <div class=\"number-box-border\" style=\"background: #b3c6e7ff;\"></div>\n            </ion-col>\n            <ion-col size=\"9\">\n                <div class=\"number-box-text\">\n                    <strong>Effort maximal</strong>\n                    <br> Je n’aurais pas pu faire 1 répétition de plus\n\n                </div>\n            </ion-col>\n        </ion-row>\n\n    </ion-grid>\n    <ion-grid>\n        <ion-row>\n            <ion-col size=\"3\">\n                <ion-button class=\"back\" expand=\"full\" shape=\"round\">\n                    <ion-icon name=\"close-outline\"></ion-icon>\n                </ion-button>\n            </ion-col>\n            <ion-col>\n                <ion-button class=\"next\" expand=\"full\" shape=\"round\">Enregistrer</ion-button>\n            </ion-col>\n        </ion-row>\n    </ion-grid>\n</ion-content>";
      /***/
    },

    /***/
    "./src/app/pages/pendant-la-seance/pendant-la-seance-routing.module.ts":
    /*!*****************************************************************************!*\
      !*** ./src/app/pages/pendant-la-seance/pendant-la-seance-routing.module.ts ***!
      \*****************************************************************************/

    /*! exports provided: PendantLaSeancePageRoutingModule */

    /***/
    function srcAppPagesPendantLaSeancePendantLaSeanceRoutingModuleTs(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "PendantLaSeancePageRoutingModule", function () {
        return PendantLaSeancePageRoutingModule;
      });
      /* harmony import */


      var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! tslib */
      "./node_modules/tslib/tslib.es6.js");
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! @angular/core */
      "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
      /* harmony import */


      var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! @angular/router */
      "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");
      /* harmony import */


      var _pendant_la_seance_page__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
      /*! ./pendant-la-seance.page */
      "./src/app/pages/pendant-la-seance/pendant-la-seance.page.ts");

      var routes = [{
        path: '',
        component: _pendant_la_seance_page__WEBPACK_IMPORTED_MODULE_3__["PendantLaSeancePage"]
      }];

      var PendantLaSeancePageRoutingModule = function PendantLaSeancePageRoutingModule() {
        _classCallCheck(this, PendantLaSeancePageRoutingModule);
      };

      PendantLaSeancePageRoutingModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]]
      })], PendantLaSeancePageRoutingModule);
      /***/
    },

    /***/
    "./src/app/pages/pendant-la-seance/pendant-la-seance.module.ts":
    /*!*********************************************************************!*\
      !*** ./src/app/pages/pendant-la-seance/pendant-la-seance.module.ts ***!
      \*********************************************************************/

    /*! exports provided: PendantLaSeancePageModule */

    /***/
    function srcAppPagesPendantLaSeancePendantLaSeanceModuleTs(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "PendantLaSeancePageModule", function () {
        return PendantLaSeancePageModule;
      });
      /* harmony import */


      var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! tslib */
      "./node_modules/tslib/tslib.es6.js");
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! @angular/core */
      "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
      /* harmony import */


      var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! @angular/common */
      "./node_modules/@angular/common/__ivy_ngcc__/fesm2015/common.js");
      /* harmony import */


      var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
      /*! @angular/forms */
      "./node_modules/@angular/forms/__ivy_ngcc__/fesm2015/forms.js");
      /* harmony import */


      var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
      /*! @ionic/angular */
      "./node_modules/@ionic/angular/__ivy_ngcc__/fesm2015/ionic-angular.js");
      /* harmony import */


      var _pendant_la_seance_routing_module__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(
      /*! ./pendant-la-seance-routing.module */
      "./src/app/pages/pendant-la-seance/pendant-la-seance-routing.module.ts");
      /* harmony import */


      var _pendant_la_seance_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(
      /*! ./pendant-la-seance.page */
      "./src/app/pages/pendant-la-seance/pendant-la-seance.page.ts");

      var PendantLaSeancePageModule = function PendantLaSeancePageModule() {
        _classCallCheck(this, PendantLaSeancePageModule);
      };

      PendantLaSeancePageModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"], _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"], _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonicModule"], _pendant_la_seance_routing_module__WEBPACK_IMPORTED_MODULE_5__["PendantLaSeancePageRoutingModule"]],
        declarations: [_pendant_la_seance_page__WEBPACK_IMPORTED_MODULE_6__["PendantLaSeancePage"]]
      })], PendantLaSeancePageModule);
      /***/
    },

    /***/
    "./src/app/pages/pendant-la-seance/pendant-la-seance.page.scss":
    /*!*********************************************************************!*\
      !*** ./src/app/pages/pendant-la-seance/pendant-la-seance.page.scss ***!
      \*********************************************************************/

    /*! exports provided: default */

    /***/
    function srcAppPagesPendantLaSeancePendantLaSeancePageScss(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony default export */


      __webpack_exports__["default"] = "ion-item {\n  --inner-border-width: 0 0 0px 0;\n  --border-width: 0 0 0px 0;\n}\n\n.header-right {\n  --border-radius: 5px;\n  --background: #ccc;\n  --color: #000;\n}\n\n.recovery {\n  background: var(--ion-btn-custom-color);\n  color: #fff;\n  font-size: 10px;\n  padding: 5px 10px;\n}\n\n.pass {\n  background: #ccc;\n  color: #000;\n  font-size: 10px;\n  padding: 5px 10px;\n}\n\n.serie-no {\n  display: block;\n  position: relative;\n  list-style: none;\n  padding: 0;\n  margin: 0;\n}\n\n.serie-no li {\n  display: inline-block;\n  padding: 3px 15px;\n  background: #c1c1c1;\n  margin-right: 5px;\n  font-size: 10px;\n  border-radius: 25px;\n}\n\n.videobox-icon {\n  display: flex;\n  position: absolute;\n  bottom: -13px;\n  right: 5%;\n}\n\n.close {\n  background: #000;\n  height: 30px;\n  width: 30px;\n  font-size: 19px;\n  padding: 2px 5px;\n  border-radius: 50%;\n  color: #fff;\n}\n\n.right {\n  height: 35px;\n  width: 35px;\n  font-size: 19px;\n  padding: 4px 9px;\n  border-radius: 50%;\n  color: #fff;\n  background: var(--ion-btn-custom-color);\n  margin-left: 10px;\n}\n\n.videobox-img img {\n  height: 225px;\n  width: 100%;\n  -o-object-fit: cover;\n     object-fit: cover;\n  -o-object-position: center;\n     object-position: center;\n}\n\n.videobox {\n  position: relative;\n}\n\n.d-fleex {\n  display: flex;\n  border-top: 1px solid #ccc;\n  padding-top: 5px;\n  margin-top: 5px;\n}\n\n.d-fleex-box {\n  flex-grow: 1;\n}\n\n.slider-box h4 {\n  font-size: 30px;\n  font-weight: bold;\n  margin: 0;\n  color: #9e9e9e;\n}\n\n.slider-box p {\n  margin: 0px;\n  font-size: 12px;\n  color: #9e9e9e;\n}\n\nion-slides {\n  height: 130px;\n}\n\n.doted-box {\n  font-size: 12px;\n  text-align: center;\n  background: #f2f2f2;\n  border: 1px dashed #c1c1c1;\n  border-radius: 10px;\n}\n\n.number-box {\n  top: 50%;\n  transform: translateY(-50%);\n  position: relative;\n}\n\n.number-box-border {\n  top: 50%;\n  transform: translateY(-50%);\n  position: relative;\n  margin: 0 auto;\n  height: 15px;\n  width: 5px;\n  background: #000;\n  border-radius: 25px;\n}\n\n.number-box-text {\n  font-size: 12px;\n}\n\n.back,\n.next {\n  --ion-button-round-border-radius: 5px;\n  --ion-button-border-radius: 5px;\n  --ion-border-radius: 5px;\n  --border-radius: 5px;\n  height: 40px;\n}\n\n.next {\n  --background: var(--ion-btn-custom-color);\n}\n\n.back {\n  --background: linear-gradient(to right, #161616e0, #c1c1c1);\n}\n\n.back img {\n  width: 70%;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvcGFnZXMvcGVuZGFudC1sYS1zZWFuY2UvcGVuZGFudC1sYS1zZWFuY2UucGFnZS5zY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBO0VBQ0ksK0JBQUE7RUFDQSx5QkFBQTtBQUNKOztBQUVBO0VBQ0ksb0JBQUE7RUFDQSxrQkFBQTtFQUNBLGFBQUE7QUFDSjs7QUFFQTtFQUNJLHVDQUFBO0VBQ0EsV0FBQTtFQUNBLGVBQUE7RUFDQSxpQkFBQTtBQUNKOztBQUVBO0VBQ0ksZ0JBQUE7RUFDQSxXQUFBO0VBQ0EsZUFBQTtFQUNBLGlCQUFBO0FBQ0o7O0FBRUE7RUFDSSxjQUFBO0VBQ0Esa0JBQUE7RUFDQSxnQkFBQTtFQUNBLFVBQUE7RUFDQSxTQUFBO0FBQ0o7O0FBRUE7RUFDSSxxQkFBQTtFQUNBLGlCQUFBO0VBQ0EsbUJBQUE7RUFDQSxpQkFBQTtFQUNBLGVBQUE7RUFDQSxtQkFBQTtBQUNKOztBQUVBO0VBQ0ksYUFBQTtFQUNBLGtCQUFBO0VBQ0EsYUFBQTtFQUNBLFNBQUE7QUFDSjs7QUFFQTtFQUNJLGdCQUFBO0VBQ0EsWUFBQTtFQUNBLFdBQUE7RUFDQSxlQUFBO0VBQ0EsZ0JBQUE7RUFDQSxrQkFBQTtFQUNBLFdBQUE7QUFDSjs7QUFFQTtFQUNJLFlBQUE7RUFDQSxXQUFBO0VBQ0EsZUFBQTtFQUNBLGdCQUFBO0VBQ0Esa0JBQUE7RUFDQSxXQUFBO0VBQ0EsdUNBQUE7RUFDQSxpQkFBQTtBQUNKOztBQUVBO0VBQ0ksYUFBQTtFQUNBLFdBQUE7RUFDQSxvQkFBQTtLQUFBLGlCQUFBO0VBQ0EsMEJBQUE7S0FBQSx1QkFBQTtBQUNKOztBQUVBO0VBQ0ksa0JBQUE7QUFDSjs7QUFFQTtFQUNJLGFBQUE7RUFDQSwwQkFBQTtFQUNBLGdCQUFBO0VBQ0EsZUFBQTtBQUNKOztBQUVBO0VBQ0ksWUFBQTtBQUNKOztBQUVBO0VBQ0ksZUFBQTtFQUNBLGlCQUFBO0VBQ0EsU0FBQTtFQUNBLGNBQUE7QUFDSjs7QUFFQTtFQUNJLFdBQUE7RUFDQSxlQUFBO0VBQ0EsY0FBQTtBQUNKOztBQUVBO0VBQ0ksYUFBQTtBQUNKOztBQUVBO0VBQ0ksZUFBQTtFQUNBLGtCQUFBO0VBQ0EsbUJBQUE7RUFDQSwwQkFBQTtFQUNBLG1CQUFBO0FBQ0o7O0FBRUE7RUFDSSxRQUFBO0VBQ0EsMkJBQUE7RUFDQSxrQkFBQTtBQUNKOztBQUVBO0VBQ0ksUUFBQTtFQUNBLDJCQUFBO0VBQ0Esa0JBQUE7RUFDQSxjQUFBO0VBQ0EsWUFBQTtFQUNBLFVBQUE7RUFDQSxnQkFBQTtFQUNBLG1CQUFBO0FBQ0o7O0FBRUE7RUFDSSxlQUFBO0FBQ0o7O0FBRUE7O0VBRUkscUNBQUE7RUFDQSwrQkFBQTtFQUNBLHdCQUFBO0VBQ0Esb0JBQUE7RUFDQSxZQUFBO0FBQ0o7O0FBRUE7RUFDSSx5Q0FBQTtBQUNKOztBQUVBO0VBQ0ksMkRBQUE7QUFDSjs7QUFBSTtFQUNJLFVBQUE7QUFFUiIsImZpbGUiOiJzcmMvYXBwL3BhZ2VzL3BlbmRhbnQtbGEtc2VhbmNlL3BlbmRhbnQtbGEtc2VhbmNlLnBhZ2Uuc2NzcyIsInNvdXJjZXNDb250ZW50IjpbImlvbi1pdGVtIHtcbiAgICAtLWlubmVyLWJvcmRlci13aWR0aDogMCAwIDBweCAwO1xuICAgIC0tYm9yZGVyLXdpZHRoOiAwIDAgMHB4IDA7XG59XG5cbi5oZWFkZXItcmlnaHQge1xuICAgIC0tYm9yZGVyLXJhZGl1czogNXB4O1xuICAgIC0tYmFja2dyb3VuZDogI2NjYztcbiAgICAtLWNvbG9yOiAjMDAwO1xufVxuXG4ucmVjb3Zlcnkge1xuICAgIGJhY2tncm91bmQ6IHZhcigtLWlvbi1idG4tY3VzdG9tLWNvbG9yKTtcbiAgICBjb2xvcjogI2ZmZjtcbiAgICBmb250LXNpemU6IDEwcHg7XG4gICAgcGFkZGluZzogNXB4IDEwcHg7XG59XG5cbi5wYXNzIHtcbiAgICBiYWNrZ3JvdW5kOiAjY2NjO1xuICAgIGNvbG9yOiAjMDAwO1xuICAgIGZvbnQtc2l6ZTogMTBweDtcbiAgICBwYWRkaW5nOiA1cHggMTBweDtcbn1cblxuLnNlcmllLW5vIHtcbiAgICBkaXNwbGF5OiBibG9jaztcbiAgICBwb3NpdGlvbjogcmVsYXRpdmU7XG4gICAgbGlzdC1zdHlsZTogbm9uZTtcbiAgICBwYWRkaW5nOiAwO1xuICAgIG1hcmdpbjogMDtcbn1cblxuLnNlcmllLW5vIGxpIHtcbiAgICBkaXNwbGF5OiBpbmxpbmUtYmxvY2s7XG4gICAgcGFkZGluZzogM3B4IDE1cHg7XG4gICAgYmFja2dyb3VuZDogI2MxYzFjMTtcbiAgICBtYXJnaW4tcmlnaHQ6IDVweDtcbiAgICBmb250LXNpemU6IDEwcHg7XG4gICAgYm9yZGVyLXJhZGl1czogMjVweDtcbn1cblxuLnZpZGVvYm94LWljb24ge1xuICAgIGRpc3BsYXk6IGZsZXg7XG4gICAgcG9zaXRpb246IGFic29sdXRlO1xuICAgIGJvdHRvbTogLTEzcHg7XG4gICAgcmlnaHQ6IDUlO1xufVxuXG4uY2xvc2Uge1xuICAgIGJhY2tncm91bmQ6ICMwMDA7XG4gICAgaGVpZ2h0OiAzMHB4O1xuICAgIHdpZHRoOiAzMHB4O1xuICAgIGZvbnQtc2l6ZTogMTlweDtcbiAgICBwYWRkaW5nOiAycHggNXB4O1xuICAgIGJvcmRlci1yYWRpdXM6IDUwJTtcbiAgICBjb2xvcjogI2ZmZjtcbn1cblxuLnJpZ2h0IHtcbiAgICBoZWlnaHQ6IDM1cHg7XG4gICAgd2lkdGg6IDM1cHg7XG4gICAgZm9udC1zaXplOiAxOXB4O1xuICAgIHBhZGRpbmc6IDRweCA5cHg7XG4gICAgYm9yZGVyLXJhZGl1czogNTAlO1xuICAgIGNvbG9yOiAjZmZmO1xuICAgIGJhY2tncm91bmQ6IHZhcigtLWlvbi1idG4tY3VzdG9tLWNvbG9yKTtcbiAgICBtYXJnaW4tbGVmdDogMTBweDtcbn1cblxuLnZpZGVvYm94LWltZyBpbWcge1xuICAgIGhlaWdodDogMjI1cHg7XG4gICAgd2lkdGg6IDEwMCU7XG4gICAgb2JqZWN0LWZpdDogY292ZXI7XG4gICAgb2JqZWN0LXBvc2l0aW9uOiBjZW50ZXI7XG59XG5cbi52aWRlb2JveCB7XG4gICAgcG9zaXRpb246IHJlbGF0aXZlO1xufVxuXG4uZC1mbGVleCB7XG4gICAgZGlzcGxheTogZmxleDtcbiAgICBib3JkZXItdG9wOiAxcHggc29saWQgI2NjYztcbiAgICBwYWRkaW5nLXRvcDogNXB4O1xuICAgIG1hcmdpbi10b3A6IDVweDtcbn1cblxuLmQtZmxlZXgtYm94IHtcbiAgICBmbGV4LWdyb3c6IDE7XG59XG5cbi5zbGlkZXItYm94IGg0IHtcbiAgICBmb250LXNpemU6IDMwcHg7XG4gICAgZm9udC13ZWlnaHQ6IGJvbGQ7XG4gICAgbWFyZ2luOiAwO1xuICAgIGNvbG9yOiAjOWU5ZTllO1xufVxuXG4uc2xpZGVyLWJveCBwIHtcbiAgICBtYXJnaW46IDBweDtcbiAgICBmb250LXNpemU6IDEycHg7XG4gICAgY29sb3I6ICM5ZTllOWU7XG59XG5cbmlvbi1zbGlkZXMge1xuICAgIGhlaWdodDogMTMwcHg7XG59XG5cbi5kb3RlZC1ib3gge1xuICAgIGZvbnQtc2l6ZTogMTJweDtcbiAgICB0ZXh0LWFsaWduOiBjZW50ZXI7XG4gICAgYmFja2dyb3VuZDogI2YyZjJmMjtcbiAgICBib3JkZXI6IDFweCBkYXNoZWQgI2MxYzFjMTtcbiAgICBib3JkZXItcmFkaXVzOiAxMHB4O1xufVxuXG4ubnVtYmVyLWJveCB7XG4gICAgdG9wOiA1MCU7XG4gICAgdHJhbnNmb3JtOiB0cmFuc2xhdGVZKC01MCUpO1xuICAgIHBvc2l0aW9uOiByZWxhdGl2ZTtcbn1cblxuLm51bWJlci1ib3gtYm9yZGVyIHtcbiAgICB0b3A6IDUwJTtcbiAgICB0cmFuc2Zvcm06IHRyYW5zbGF0ZVkoLTUwJSk7XG4gICAgcG9zaXRpb246IHJlbGF0aXZlO1xuICAgIG1hcmdpbjogMCBhdXRvO1xuICAgIGhlaWdodDogMTVweDtcbiAgICB3aWR0aDogNXB4O1xuICAgIGJhY2tncm91bmQ6ICMwMDA7XG4gICAgYm9yZGVyLXJhZGl1czogMjVweDtcbn1cblxuLm51bWJlci1ib3gtdGV4dCB7XG4gICAgZm9udC1zaXplOiAxMnB4O1xufVxuXG4uYmFjayxcbi5uZXh0IHtcbiAgICAtLWlvbi1idXR0b24tcm91bmQtYm9yZGVyLXJhZGl1czogNXB4O1xuICAgIC0taW9uLWJ1dHRvbi1ib3JkZXItcmFkaXVzOiA1cHg7XG4gICAgLS1pb24tYm9yZGVyLXJhZGl1czogNXB4O1xuICAgIC0tYm9yZGVyLXJhZGl1czogNXB4O1xuICAgIGhlaWdodDogNDBweDtcbn1cblxuLm5leHQge1xuICAgIC0tYmFja2dyb3VuZDogdmFyKC0taW9uLWJ0bi1jdXN0b20tY29sb3IpO1xufVxuXG4uYmFjayB7XG4gICAgLS1iYWNrZ3JvdW5kOiBsaW5lYXItZ3JhZGllbnQodG8gcmlnaHQsICMxNjE2MTZlMCwgI2MxYzFjMSk7XG4gICAgaW1nIHtcbiAgICAgICAgd2lkdGg6IDcwJTtcbiAgICB9XG59Il19 */";
      /***/
    },

    /***/
    "./src/app/pages/pendant-la-seance/pendant-la-seance.page.ts":
    /*!*******************************************************************!*\
      !*** ./src/app/pages/pendant-la-seance/pendant-la-seance.page.ts ***!
      \*******************************************************************/

    /*! exports provided: PendantLaSeancePage */

    /***/
    function srcAppPagesPendantLaSeancePendantLaSeancePageTs(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "PendantLaSeancePage", function () {
        return PendantLaSeancePage;
      });
      /* harmony import */


      var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! tslib */
      "./node_modules/tslib/tslib.es6.js");
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! @angular/core */
      "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");

      var PendantLaSeancePage = /*#__PURE__*/function () {
        function PendantLaSeancePage() {
          _classCallCheck(this, PendantLaSeancePage);

          this.categories = {
            slidesPerView: 1,
            effect: 'flip',
            slidesPerColumn: 1,
            slidesPerGroup: 1,
            watchSlidesProgress: true
          };
        }

        _createClass(PendantLaSeancePage, [{
          key: "ngOnInit",
          value: function ngOnInit() {}
        }]);

        return PendantLaSeancePage;
      }();

      PendantLaSeancePage.ctorParameters = function () {
        return [];
      };

      PendantLaSeancePage = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-pendant-la-seance',
        template: Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(
        /*! raw-loader!./pendant-la-seance.page.html */
        "./node_modules/raw-loader/dist/cjs.js!./src/app/pages/pendant-la-seance/pendant-la-seance.page.html"))["default"],
        styles: [Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(
        /*! ./pendant-la-seance.page.scss */
        "./src/app/pages/pendant-la-seance/pendant-la-seance.page.scss"))["default"]]
      })], PendantLaSeancePage);
      /***/
    }
  }]);
})();
//# sourceMappingURL=pages-pendant-la-seance-pendant-la-seance-module-es5.js.map