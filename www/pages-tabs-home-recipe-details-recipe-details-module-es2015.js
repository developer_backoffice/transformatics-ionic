(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["pages-tabs-home-recipe-details-recipe-details-module"],{

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/pages/tabs/home/recipe-details/recipe-details.page.html":
/*!***************************************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/pages/tabs/home/recipe-details/recipe-details.page.html ***!
  \***************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<ion-content>\n    <div class=\"titlebox\">\n        <div class=\"titlebox-arrow\">\n            <div routerLink=\"/aliments\">\n                <ion-icon name=\"chevron-back-outline\"></ion-icon>\n            </div>\n            <div class=\"ion-text-right\">\n                <ion-icon name=\"heart-outline\"></ion-icon>\n                <ion-icon name=\"ellipsis-vertical-outline\"></ion-icon>\n            </div>\n        </div>\n        <div class=\"title-img\">\n            <img src=\"assets/images/recipedetails.png\">\n        </div>\n        <div class=\"titlebox-text\">\n            <h4>BOULETTES DE COCO</h4>\n            <h5>Protéinées</h5>\n        </div>\n    </div>\n\n    <div class=\"kcal-box-bg\">\n        <ion-grid>\n            <ion-row>\n                <ion-col size=\"7\">\n                    <div>Catégories nutritionnelles</div>\n                </ion-col>\n                <ion-col size=\"5\">\n                    <div class=\"flex-box\">\n\n                        <div class=\"icons\">\n                            <img src=\"assets/icon/cart.png\">\n\n                        </div>\n\n\n                        <div class=\"icons\">\n                            <img src=\"assets/icon/usergroup.png\">\n                            <ion-badge color=\"primary\">1</ion-badge>\n                        </div>\n\n\n                        <div class=\"icons\">\n                            <img src=\"assets/icon/network.png\">\n                            <ion-badge color=\"primary\">2</ion-badge>\n                        </div>\n\n\n                        <div class=\"icons\">\n                            <img src=\"assets/icon/clock.png\">\n                            <ion-badge color=\"primary\">20</ion-badge>\n                        </div>\n\n                    </div>\n                </ion-col>\n            </ion-row>\n        </ion-grid>\n    </div>\n    <ion-grid>\n        <ion-row>\n            <ion-col size=\"2\">\n                <div class=\"remove-box\">\n                    <ion-icon name=\"remove-outline\" style=\"font-size: 2.0rem;\"></ion-icon>\n                </div>\n            </ion-col>\n            <ion-col size=\"8\">\n                <div class=\"content-box\">\n                    <h4 class=\"large_text_bold\">2 personnes</h4>\n                    <p>6 boulettes</p>\n                </div>\n            </ion-col>\n            <ion-col size=\"2\">\n                <div class=\"add-box\">\n                    <ion-icon name=\"add-outline\" style=\"font-size: 2.0rem;\"></ion-icon>\n                </div>\n            </ion-col>\n        </ion-row>\n        <ion-row>\n            <ion-col size=\"3\">\n                <div class=\"content-box b-l-r\">\n                    <h4 class=\"large_text_bold\">277</h4>\n                    <p>Kcalories</p>\n                </div>\n            </ion-col>\n            <ion-col size=\"3\">\n                <div class=\"content-box\">\n                    <h4 class=\"large_text_bold\">18</h4>\n                    <p>g de protéines</p>\n                </div>\n            </ion-col>\n            <ion-col size=\"3\">\n                <div class=\"content-box\">\n                    <h4 class=\"large_text_bold\">6</h4>\n                    <p>g de glucides </p>\n                </div>\n            </ion-col>\n            <ion-col size=\"3\">\n                <div class=\"content-box b-r-r\">\n                    <h4 class=\"large_text_bold\">27</h4>\n                    <p>g de lipides</p>\n                </div>\n            </ion-col>\n            <ion-col size=\"12\">\n                <h4>INGREDIENTS </h4>\n            </ion-col>\n            <ion-col size=\"6\">\n                <p class=\"ion-no-margin f12 \">40g <span class=\"text-bold\">Framboises</span></p>\n            </ion-col>\n            <ion-col size=\"6\">\n                <p class=\"ion-no-margin f12\">40g <span class=\"text-bold\"> Framboises</span></p>\n            </ion-col>\n            <ion-col size=\"6\">\n                <p class=\"ion-no-margin f12\">30g <span class=\"text-bold\"> Whey coco</span></p>\n            </ion-col>\n            <ion-col size=\"6\">\n                <p class=\"ion-no-margin f12\">30g <span class=\"text-bold\">Whey coco</span></p>\n            </ion-col>\n            <ion-col size=\"6\">\n                <p class=\"ion-no-margin f12\">10g <span class=\"text-bold\">Amandes effilées</span></p>\n            </ion-col>\n            <ion-col size=\"6\">\n                <p class=\"ion-no-margin f12\">10g <span class=\"text-bold\"> Amandes effilées</span></p>\n            </ion-col>\n            <ion-col size=\"6\">\n                <p class=\"ion-no-margin f12\">40g <span class=\"text-bold\"> Flocons d’avoine</span> </p>\n            </ion-col>\n            <ion-col size=\"6\">\n                <p class=\"ion-no-margin f12\">40g <span class=\"text-bold\">40g Flocons d’avoine </span></p>\n            </ion-col>\n        </ion-row>\n    </ion-grid>\n    <ion-row>\n        <div class=\"viewall\">\n            <ion-button>VOIR TOUTES LES ETAPES DE REALISATION\n                <ion-icon name=\"chevron-forward-outline\"></ion-icon>\n            </ion-button>\n        </div>\n    </ion-row>\n    <ion-grid>\n        <ion-row>\n            <ion-col size=\"12\">\n                <div class=\"timeline-box-group\">\n                    <div class=\"timeline-box\">\n                        <div class=\"timeline-badge \"></div>\n                        <div class=\"timeline-body\">\n                            <div>\n                                <strong>ETAPE N°1</strong> : mélangez tous les ingrédients sauf les flocons dans un blender.\n                            </div>\n                        </div>\n                    </div>\n                    <div class=\"timeline-box\">\n                        <div class=\"timeline-badge \"></div>\n                        <div class=\"timeline-body\">\n                            <div>\n                                <strong>ETAPE N°2</strong> : réalisez des boulettes avec le mélange, puis laissez-les reposer au réfrigérateur pendant 1h.\n                            </div>\n                        </div>\n                    </div>\n                    <div class=\"timeline-box\">\n                        <div class=\"timeline-badge \"></div>\n                        <div class=\"timeline-body\">\n                            <div>\n                                <strong>ETAPE N°3</strong> : réalisez des boulettes avec le mélange, puis laissez-les reposer au réfrigérateur pendant 1h.\n                            </div>\n                        </div>\n                    </div>\n                    <div class=\"timeline-box\">\n                        <div class=\"timeline-badge \"></div>\n                        <div class=\"timeline-body\">\n                            <div>\n                                <strong>ETAPE N°4</strong> : réalisez des boulettes avec le mélange, puis laissez-les reposer au réfrigérateur pendant 1h.\n                            </div>\n                        </div>\n                    </div>\n                    <div class=\"timeline-box\">\n                        <div class=\"timeline-badge \"></div>\n                        <div class=\"timeline-body\">\n                            <div>\n                                <strong>ETAPE N°5</strong> : réalisez des boulettes avec le mélange, puis laissez-les reposer au réfrigérateur pendant 1h.\n                            </div>\n                        </div>\n                    </div>\n                    <div class=\"timeline-box\">\n                        <div class=\"timeline-badge \"></div>\n                        <div class=\"timeline-body\">\n                            <div>\n                                <strong>ETAPE N°6</strong> : réalisez des boulettes avec le mélange, puis laissez-les reposer au réfrigérateur pendant 1h.\n                            </div>\n                        </div>\n                    </div>\n                </div>\n            </ion-col>\n        </ion-row>\n    </ion-grid>\n</ion-content>\n<ion-footer>\n    <ion-toolbar>\n        <div class=\"connect\">\n            <ion-button shape=\"round\" expand=\"full\">Ajouter la recette\n            </ion-button>\n        </div>\n    </ion-toolbar>\n</ion-footer>");

/***/ }),

/***/ "./src/app/pages/tabs/home/recipe-details/recipe-details-routing.module.ts":
/*!*********************************************************************************!*\
  !*** ./src/app/pages/tabs/home/recipe-details/recipe-details-routing.module.ts ***!
  \*********************************************************************************/
/*! exports provided: RecipeDetailsPageRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "RecipeDetailsPageRoutingModule", function() { return RecipeDetailsPageRoutingModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");
/* harmony import */ var _recipe_details_page__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./recipe-details.page */ "./src/app/pages/tabs/home/recipe-details/recipe-details.page.ts");




const routes = [
    {
        path: '',
        component: _recipe_details_page__WEBPACK_IMPORTED_MODULE_3__["RecipeDetailsPage"]
    }
];
let RecipeDetailsPageRoutingModule = class RecipeDetailsPageRoutingModule {
};
RecipeDetailsPageRoutingModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]],
    })
], RecipeDetailsPageRoutingModule);



/***/ }),

/***/ "./src/app/pages/tabs/home/recipe-details/recipe-details.module.ts":
/*!*************************************************************************!*\
  !*** ./src/app/pages/tabs/home/recipe-details/recipe-details.module.ts ***!
  \*************************************************************************/
/*! exports provided: RecipeDetailsPageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "RecipeDetailsPageModule", function() { return RecipeDetailsPageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/__ivy_ngcc__/fesm2015/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/__ivy_ngcc__/fesm2015/forms.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/__ivy_ngcc__/fesm2015/ionic-angular.js");
/* harmony import */ var _recipe_details_routing_module__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./recipe-details-routing.module */ "./src/app/pages/tabs/home/recipe-details/recipe-details-routing.module.ts");
/* harmony import */ var _recipe_details_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./recipe-details.page */ "./src/app/pages/tabs/home/recipe-details/recipe-details.page.ts");







let RecipeDetailsPageModule = class RecipeDetailsPageModule {
};
RecipeDetailsPageModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [
            _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonicModule"],
            _recipe_details_routing_module__WEBPACK_IMPORTED_MODULE_5__["RecipeDetailsPageRoutingModule"]
        ],
        declarations: [_recipe_details_page__WEBPACK_IMPORTED_MODULE_6__["RecipeDetailsPage"]]
    })
], RecipeDetailsPageModule);



/***/ }),

/***/ "./src/app/pages/tabs/home/recipe-details/recipe-details.page.scss":
/*!*************************************************************************!*\
  !*** ./src/app/pages/tabs/home/recipe-details/recipe-details.page.scss ***!
  \*************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = (".titlebox {\n  position: relative;\n  color: #fff;\n  height: 250px;\n  overflow: hidden;\n}\n\n.titlebox-arrow {\n  display: flex;\n  position: absolute;\n  width: 100%;\n  top: 10%;\n  padding: 0px 5%;\n  transform: translateY(-10%);\n}\n\n.titlebox-arrow div {\n  flex-grow: 1;\n  font-size: 30px;\n}\n\n.titlebox-text {\n  position: absolute;\n  bottom: 5%;\n  left: 5%;\n}\n\n.title-img {\n  background: #000;\n  height: 100%;\n}\n\n.title-img img {\n  height: 100%;\n  -o-object-fit: cover;\n     object-fit: cover;\n  width: 100%;\n}\n\n.titlebox-text h5 {\n  font-size: 12px;\n  margin: 0;\n}\n\n.titlebox {\n  position: relative;\n  color: #fff;\n}\n\n.kcal-box-bg {\n  background: #eee;\n  border-radius: 16px;\n  margin: 5px;\n  margin-top: 15px;\n  padding-top: 5px;\n}\n\n.viewall {\n  width: 100%;\n  position: relative;\n  margin-top: 15px;\n  margin-bottom: 15px;\n}\n\n.viewall ion-button {\n  --border-radius: 25px;\n  display: table;\n  margin: auto;\n  font-size: 10px;\n  height: 25px;\n  --background: #fff;\n  color: #000;\n  border: 1px solid #000;\n  border-radius: 25px;\n}\n\n.viewall:before {\n  content: \"\";\n  position: absolute;\n  background: #000;\n  height: 1px;\n  width: 100%;\n  top: 13px;\n  left: 0;\n}\n\nion-item {\n  --inner-border-width: 0 0 0px 0;\n  --border-width: 0 0 0px 0;\n}\n\n.footer-md::before {\n  display: none;\n}\n\n.icons img {\n  background: #ffeb3b;\n  padding: 4px;\n  border-radius: 31px;\n  border: 1px solid #000;\n  width: 25px;\n  height: 25px;\n}\n\n.flex-box {\n  text-align: right;\n}\n\n.icons {\n  display: inline-block;\n  margin-right: 5px;\n  position: relative;\n}\n\nion-badge {\n  position: absolute;\n  font-size: 6px;\n  top: -8px;\n  left: 0px;\n  border: 0.5px solid #000;\n  border-radius: 25px;\n  background: #fff;\n  color: #000;\n  height: 12px;\n  width: 12px;\n  padding: 2px;\n}\n\n.remove-box {\n  text-align: center;\n  background: #eee;\n  padding: 5px 0px;\n  font-size: 19px;\n  border-radius: 5px 0px 0px 5px;\n}\n\n.content-box {\n  text-align: center;\n  background: #eee;\n  padding: 5px 0px;\n}\n\n.content-box h4 {\n  margin: 0px;\n}\n\n.content-box p {\n  font-size: 10px;\n  margin: 0px;\n}\n\n.add-box {\n  text-align: center;\n  background: #eee;\n  padding: 5px 0px;\n  font-size: 19px;\n  border-radius: 0px 5px 5px 0px;\n}\n\n.b-l-r {\n  border-radius: 5px 0px 0px 5px;\n}\n\n.b-r-r {\n  border-radius: 0px 5px 5px 0px;\n}\n\n.timeline-box {\n  position: relative;\n  display: flex;\n  padding: 0px 0;\n  margin-left: 0px;\n  padding-bottom: 15px;\n}\n\n.timeline-box:before {\n  position: absolute;\n  top: 0;\n  bottom: 0;\n  left: 6px;\n  display: block;\n  width: 2px;\n  content: \"\";\n  background-color: #000;\n}\n\n.timeline-box:last-child:before {\n  display: none;\n}\n\n.timeline-badge {\n  position: relative;\n  z-index: 1;\n  display: flex;\n  width: 13px;\n  height: 13px;\n  margin-right: 8px;\n  margin-left: 0px;\n  color: #000;\n  align-items: center;\n  background-color: #000;\n  border-radius: 50%;\n  justify-content: center;\n  flex-shrink: 0;\n}\n\n.timeline-body {\n  min-width: 0;\n  max-width: 100%;\n  margin-top: -5px;\n  color: #000;\n  flex: auto;\n  display: flex;\n}\n\n.timeline-body div {\n  flex-grow: 1;\n  font-size: 12px;\n}\n\n.timeline-box .timeline-badge {\n  background-color: #fff;\n  border: 1px solid #000;\n}\n\n.connect ion-button {\n  --background: var(--ion-btn-custom-color);\n  --border-radius: 5px;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvcGFnZXMvdGFicy9ob21lL3JlY2lwZS1kZXRhaWxzL3JlY2lwZS1kZXRhaWxzLnBhZ2Uuc2NzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQTtFQUNJLGtCQUFBO0VBQ0EsV0FBQTtFQUNBLGFBQUE7RUFDQSxnQkFBQTtBQUNKOztBQUVBO0VBQ0ksYUFBQTtFQUNBLGtCQUFBO0VBQ0EsV0FBQTtFQUNBLFFBQUE7RUFDQSxlQUFBO0VBQ0EsMkJBQUE7QUFDSjs7QUFFQTtFQUNJLFlBQUE7RUFDQSxlQUFBO0FBQ0o7O0FBRUE7RUFDSSxrQkFBQTtFQUNBLFVBQUE7RUFDQSxRQUFBO0FBQ0o7O0FBRUE7RUFDSSxnQkFBQTtFQUNBLFlBQUE7QUFDSjs7QUFFQTtFQUNJLFlBQUE7RUFDQSxvQkFBQTtLQUFBLGlCQUFBO0VBQ0EsV0FBQTtBQUNKOztBQUVBO0VBQ0ksZUFBQTtFQUNBLFNBQUE7QUFDSjs7QUFFQTtFQUNJLGtCQUFBO0VBQ0EsV0FBQTtBQUNKOztBQUVBO0VBQ0ksZ0JBQUE7RUFDQSxtQkFBQTtFQUNBLFdBQUE7RUFDQSxnQkFBQTtFQUNBLGdCQUFBO0FBQ0o7O0FBRUE7RUFDSSxXQUFBO0VBQ0Esa0JBQUE7RUFDQSxnQkFBQTtFQUNBLG1CQUFBO0FBQ0o7O0FBRUE7RUFDSSxxQkFBQTtFQUNBLGNBQUE7RUFDQSxZQUFBO0VBQ0EsZUFBQTtFQUNBLFlBQUE7RUFDQSxrQkFBQTtFQUNBLFdBQUE7RUFDQSxzQkFBQTtFQUNBLG1CQUFBO0FBQ0o7O0FBRUE7RUFDSSxXQUFBO0VBQ0Esa0JBQUE7RUFDQSxnQkFBQTtFQUNBLFdBQUE7RUFDQSxXQUFBO0VBQ0EsU0FBQTtFQUNBLE9BQUE7QUFDSjs7QUFFQTtFQUNJLCtCQUFBO0VBQ0EseUJBQUE7QUFDSjs7QUFFQTtFQUNJLGFBQUE7QUFDSjs7QUFFQTtFQUNJLG1CQUFBO0VBQ0EsWUFBQTtFQUNBLG1CQUFBO0VBQ0Esc0JBQUE7RUFDQSxXQUFBO0VBQ0EsWUFBQTtBQUNKOztBQUVBO0VBQ0ksaUJBQUE7QUFDSjs7QUFFQTtFQUNJLHFCQUFBO0VBQ0EsaUJBQUE7RUFDQSxrQkFBQTtBQUNKOztBQUVBO0VBQ0ksa0JBQUE7RUFDQSxjQUFBO0VBQ0EsU0FBQTtFQUNBLFNBQUE7RUFDQSx3QkFBQTtFQUNBLG1CQUFBO0VBQ0EsZ0JBQUE7RUFDQSxXQUFBO0VBQ0EsWUFBQTtFQUNBLFdBQUE7RUFDQSxZQUFBO0FBQ0o7O0FBRUE7RUFDSSxrQkFBQTtFQUNBLGdCQUFBO0VBQ0EsZ0JBQUE7RUFDQSxlQUFBO0VBQ0EsOEJBQUE7QUFDSjs7QUFFQTtFQUNJLGtCQUFBO0VBQ0EsZ0JBQUE7RUFDQSxnQkFBQTtBQUNKOztBQUVBO0VBQ0ksV0FBQTtBQUNKOztBQUdBO0VBQ0ksZUFBQTtFQUNBLFdBQUE7QUFBSjs7QUFHQTtFQUNJLGtCQUFBO0VBQ0EsZ0JBQUE7RUFDQSxnQkFBQTtFQUNBLGVBQUE7RUFDQSw4QkFBQTtBQUFKOztBQUdBO0VBQ0ksOEJBQUE7QUFBSjs7QUFHQTtFQUNJLDhCQUFBO0FBQUo7O0FBR0E7RUFDSSxrQkFBQTtFQUNBLGFBQUE7RUFDQSxjQUFBO0VBQ0EsZ0JBQUE7RUFDQSxvQkFBQTtBQUFKOztBQUdBO0VBQ0ksa0JBQUE7RUFDQSxNQUFBO0VBQ0EsU0FBQTtFQUNBLFNBQUE7RUFDQSxjQUFBO0VBQ0EsVUFBQTtFQUNBLFdBQUE7RUFDQSxzQkFBQTtBQUFKOztBQUdBO0VBQ0ksYUFBQTtBQUFKOztBQUdBO0VBQ0ksa0JBQUE7RUFDQSxVQUFBO0VBQ0EsYUFBQTtFQUNBLFdBQUE7RUFDQSxZQUFBO0VBQ0EsaUJBQUE7RUFDQSxnQkFBQTtFQUNBLFdBQUE7RUFDQSxtQkFBQTtFQUNBLHNCQUFBO0VBQ0Esa0JBQUE7RUFDQSx1QkFBQTtFQUNBLGNBQUE7QUFBSjs7QUFHQTtFQUNJLFlBQUE7RUFDQSxlQUFBO0VBQ0EsZ0JBQUE7RUFDQSxXQUFBO0VBQ0EsVUFBQTtFQUNBLGFBQUE7QUFBSjs7QUFHQTtFQUNJLFlBQUE7RUFDQSxlQUFBO0FBQUo7O0FBR0E7RUFDSSxzQkFBQTtFQUNBLHNCQUFBO0FBQUo7O0FBR0E7RUFDSSx5Q0FBQTtFQUNBLG9CQUFBO0FBQUoiLCJmaWxlIjoic3JjL2FwcC9wYWdlcy90YWJzL2hvbWUvcmVjaXBlLWRldGFpbHMvcmVjaXBlLWRldGFpbHMucGFnZS5zY3NzIiwic291cmNlc0NvbnRlbnQiOlsiLnRpdGxlYm94IHtcbiAgICBwb3NpdGlvbjogcmVsYXRpdmU7XG4gICAgY29sb3I6ICNmZmY7XG4gICAgaGVpZ2h0OiAyNTBweDtcbiAgICBvdmVyZmxvdzogaGlkZGVuO1xufVxuXG4udGl0bGVib3gtYXJyb3cge1xuICAgIGRpc3BsYXk6IGZsZXg7XG4gICAgcG9zaXRpb246IGFic29sdXRlO1xuICAgIHdpZHRoOiAxMDAlO1xuICAgIHRvcDogMTAlO1xuICAgIHBhZGRpbmc6IDBweCA1JTtcbiAgICB0cmFuc2Zvcm06IHRyYW5zbGF0ZVkoLTEwJSk7XG59XG5cbi50aXRsZWJveC1hcnJvdyBkaXYge1xuICAgIGZsZXgtZ3JvdzogMTtcbiAgICBmb250LXNpemU6IDMwcHg7XG59XG5cbi50aXRsZWJveC10ZXh0IHtcbiAgICBwb3NpdGlvbjogYWJzb2x1dGU7XG4gICAgYm90dG9tOiA1JTtcbiAgICBsZWZ0OiA1JTtcbn1cblxuLnRpdGxlLWltZyB7XG4gICAgYmFja2dyb3VuZDogIzAwMDtcbiAgICBoZWlnaHQ6IDEwMCU7XG59XG5cbi50aXRsZS1pbWcgaW1nIHtcbiAgICBoZWlnaHQ6IDEwMCU7XG4gICAgb2JqZWN0LWZpdDogY292ZXI7XG4gICAgd2lkdGg6IDEwMCU7XG59XG5cbi50aXRsZWJveC10ZXh0IGg1IHtcbiAgICBmb250LXNpemU6IDEycHg7XG4gICAgbWFyZ2luOiAwO1xufVxuXG4udGl0bGVib3gge1xuICAgIHBvc2l0aW9uOiByZWxhdGl2ZTtcbiAgICBjb2xvcjogI2ZmZjtcbn1cblxuLmtjYWwtYm94LWJnIHtcbiAgICBiYWNrZ3JvdW5kOiAjZWVlO1xuICAgIGJvcmRlci1yYWRpdXM6IDE2cHg7XG4gICAgbWFyZ2luOiA1cHg7XG4gICAgbWFyZ2luLXRvcDogMTVweDtcbiAgICBwYWRkaW5nLXRvcDogNXB4O1xufVxuXG4udmlld2FsbCB7XG4gICAgd2lkdGg6IDEwMCU7XG4gICAgcG9zaXRpb246IHJlbGF0aXZlO1xuICAgIG1hcmdpbi10b3A6IDE1cHg7XG4gICAgbWFyZ2luLWJvdHRvbTogMTVweDtcbn1cblxuLnZpZXdhbGwgaW9uLWJ1dHRvbiB7XG4gICAgLS1ib3JkZXItcmFkaXVzOiAyNXB4O1xuICAgIGRpc3BsYXk6IHRhYmxlO1xuICAgIG1hcmdpbjogYXV0bztcbiAgICBmb250LXNpemU6IDEwcHg7XG4gICAgaGVpZ2h0OiAyNXB4O1xuICAgIC0tYmFja2dyb3VuZDogI2ZmZjtcbiAgICBjb2xvcjogIzAwMDtcbiAgICBib3JkZXI6IDFweCBzb2xpZCAjMDAwO1xuICAgIGJvcmRlci1yYWRpdXM6IDI1cHg7XG59XG5cbi52aWV3YWxsOmJlZm9yZSB7XG4gICAgY29udGVudDogXCJcIjtcbiAgICBwb3NpdGlvbjogYWJzb2x1dGU7XG4gICAgYmFja2dyb3VuZDogIzAwMDtcbiAgICBoZWlnaHQ6IDFweDtcbiAgICB3aWR0aDogMTAwJTtcbiAgICB0b3A6IDEzcHg7XG4gICAgbGVmdDogMDtcbn1cblxuaW9uLWl0ZW0ge1xuICAgIC0taW5uZXItYm9yZGVyLXdpZHRoOiAwIDAgMHB4IDA7XG4gICAgLS1ib3JkZXItd2lkdGg6IDAgMCAwcHggMDtcbn1cblxuLmZvb3Rlci1tZDo6YmVmb3JlIHtcbiAgICBkaXNwbGF5OiBub25lO1xufVxuXG4uaWNvbnMgaW1nIHtcbiAgICBiYWNrZ3JvdW5kOiAjZmZlYjNiO1xuICAgIHBhZGRpbmc6IDRweDtcbiAgICBib3JkZXItcmFkaXVzOiAzMXB4O1xuICAgIGJvcmRlcjogMXB4IHNvbGlkICMwMDA7XG4gICAgd2lkdGg6IDI1cHg7XG4gICAgaGVpZ2h0OiAyNXB4O1xufVxuXG4uZmxleC1ib3gge1xuICAgIHRleHQtYWxpZ246IHJpZ2h0O1xufVxuXG4uaWNvbnMge1xuICAgIGRpc3BsYXk6IGlubGluZS1ibG9jaztcbiAgICBtYXJnaW4tcmlnaHQ6IDVweDtcbiAgICBwb3NpdGlvbjogcmVsYXRpdmU7XG59XG5cbmlvbi1iYWRnZSB7XG4gICAgcG9zaXRpb246IGFic29sdXRlO1xuICAgIGZvbnQtc2l6ZTogNnB4O1xuICAgIHRvcDogLThweDtcbiAgICBsZWZ0OiAwcHg7XG4gICAgYm9yZGVyOiAwLjVweCBzb2xpZCAjMDAwO1xuICAgIGJvcmRlci1yYWRpdXM6IDI1cHg7XG4gICAgYmFja2dyb3VuZDogI2ZmZjtcbiAgICBjb2xvcjogIzAwMDtcbiAgICBoZWlnaHQ6IDEycHg7XG4gICAgd2lkdGg6IDEycHg7XG4gICAgcGFkZGluZzogMnB4O1xufVxuXG4ucmVtb3ZlLWJveCB7XG4gICAgdGV4dC1hbGlnbjogY2VudGVyO1xuICAgIGJhY2tncm91bmQ6ICNlZWU7XG4gICAgcGFkZGluZzogNXB4IDBweDtcbiAgICBmb250LXNpemU6IDE5cHg7XG4gICAgYm9yZGVyLXJhZGl1czogNXB4IDBweCAwcHggNXB4O1xufVxuXG4uY29udGVudC1ib3gge1xuICAgIHRleHQtYWxpZ246IGNlbnRlcjtcbiAgICBiYWNrZ3JvdW5kOiAjZWVlO1xuICAgIHBhZGRpbmc6IDVweCAwcHg7XG59XG5cbi5jb250ZW50LWJveCBoNCB7XG4gICAgbWFyZ2luOiAwcHg7XG4gICAgLy8gZm9udC1zaXplOiAxMnB4O1xufVxuXG4uY29udGVudC1ib3ggcCB7XG4gICAgZm9udC1zaXplOiAxMHB4O1xuICAgIG1hcmdpbjogMHB4O1xufVxuXG4uYWRkLWJveCB7XG4gICAgdGV4dC1hbGlnbjogY2VudGVyO1xuICAgIGJhY2tncm91bmQ6ICNlZWU7XG4gICAgcGFkZGluZzogNXB4IDBweDtcbiAgICBmb250LXNpemU6IDE5cHg7XG4gICAgYm9yZGVyLXJhZGl1czogMHB4IDVweCA1cHggMHB4O1xufVxuXG4uYi1sLXIge1xuICAgIGJvcmRlci1yYWRpdXM6IDVweCAwcHggMHB4IDVweDtcbn1cblxuLmItci1yIHtcbiAgICBib3JkZXItcmFkaXVzOiAwcHggNXB4IDVweCAwcHg7XG59XG5cbi50aW1lbGluZS1ib3gge1xuICAgIHBvc2l0aW9uOiByZWxhdGl2ZTtcbiAgICBkaXNwbGF5OiBmbGV4O1xuICAgIHBhZGRpbmc6IDBweCAwO1xuICAgIG1hcmdpbi1sZWZ0OiAwcHg7XG4gICAgcGFkZGluZy1ib3R0b206IDE1cHg7XG59XG5cbi50aW1lbGluZS1ib3g6YmVmb3JlIHtcbiAgICBwb3NpdGlvbjogYWJzb2x1dGU7XG4gICAgdG9wOiAwO1xuICAgIGJvdHRvbTogMDtcbiAgICBsZWZ0OiA2cHg7XG4gICAgZGlzcGxheTogYmxvY2s7XG4gICAgd2lkdGg6IDJweDtcbiAgICBjb250ZW50OiBcIlwiO1xuICAgIGJhY2tncm91bmQtY29sb3I6ICMwMDA7XG59XG5cbi50aW1lbGluZS1ib3g6bGFzdC1jaGlsZDpiZWZvcmUge1xuICAgIGRpc3BsYXk6IG5vbmU7XG59XG5cbi50aW1lbGluZS1iYWRnZSB7XG4gICAgcG9zaXRpb246IHJlbGF0aXZlO1xuICAgIHotaW5kZXg6IDE7XG4gICAgZGlzcGxheTogZmxleDtcbiAgICB3aWR0aDogMTNweDtcbiAgICBoZWlnaHQ6IDEzcHg7XG4gICAgbWFyZ2luLXJpZ2h0OiA4cHg7XG4gICAgbWFyZ2luLWxlZnQ6IDBweDtcbiAgICBjb2xvcjogIzAwMDtcbiAgICBhbGlnbi1pdGVtczogY2VudGVyO1xuICAgIGJhY2tncm91bmQtY29sb3I6ICMwMDA7XG4gICAgYm9yZGVyLXJhZGl1czogNTAlO1xuICAgIGp1c3RpZnktY29udGVudDogY2VudGVyO1xuICAgIGZsZXgtc2hyaW5rOiAwO1xufVxuXG4udGltZWxpbmUtYm9keSB7XG4gICAgbWluLXdpZHRoOiAwO1xuICAgIG1heC13aWR0aDogMTAwJTtcbiAgICBtYXJnaW4tdG9wOiAtNXB4O1xuICAgIGNvbG9yOiAjMDAwO1xuICAgIGZsZXg6IGF1dG87XG4gICAgZGlzcGxheTogZmxleDtcbn1cblxuLnRpbWVsaW5lLWJvZHkgZGl2IHtcbiAgICBmbGV4LWdyb3c6IDE7XG4gICAgZm9udC1zaXplOiAxMnB4O1xufVxuXG4udGltZWxpbmUtYm94IC50aW1lbGluZS1iYWRnZSB7XG4gICAgYmFja2dyb3VuZC1jb2xvcjogI2ZmZjtcbiAgICBib3JkZXI6IDFweCBzb2xpZCAjMDAwO1xufVxuXG4uY29ubmVjdCBpb24tYnV0dG9uIHtcbiAgICAtLWJhY2tncm91bmQ6IHZhcigtLWlvbi1idG4tY3VzdG9tLWNvbG9yKTtcbiAgICAtLWJvcmRlci1yYWRpdXM6IDVweDtcbn0iXX0= */");

/***/ }),

/***/ "./src/app/pages/tabs/home/recipe-details/recipe-details.page.ts":
/*!***********************************************************************!*\
  !*** ./src/app/pages/tabs/home/recipe-details/recipe-details.page.ts ***!
  \***********************************************************************/
/*! exports provided: RecipeDetailsPage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "RecipeDetailsPage", function() { return RecipeDetailsPage; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");


let RecipeDetailsPage = class RecipeDetailsPage {
    constructor() { }
    ngOnInit() {
    }
};
RecipeDetailsPage.ctorParameters = () => [];
RecipeDetailsPage = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-recipe-details',
        template: Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(/*! raw-loader!./recipe-details.page.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/pages/tabs/home/recipe-details/recipe-details.page.html")).default,
        styles: [Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(/*! ./recipe-details.page.scss */ "./src/app/pages/tabs/home/recipe-details/recipe-details.page.scss")).default]
    })
], RecipeDetailsPage);



/***/ })

}]);
//# sourceMappingURL=pages-tabs-home-recipe-details-recipe-details-module-es2015.js.map