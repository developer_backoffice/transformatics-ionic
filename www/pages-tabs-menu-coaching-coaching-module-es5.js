(function () {
  function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

  function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

  function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

  (window["webpackJsonp"] = window["webpackJsonp"] || []).push([["pages-tabs-menu-coaching-coaching-module"], {
    /***/
    "./node_modules/raw-loader/dist/cjs.js!./src/app/pages/tabs/menu/coaching/coaching.page.html":
    /*!***************************************************************************************************!*\
      !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/pages/tabs/menu/coaching/coaching.page.html ***!
      \***************************************************************************************************/

    /*! exports provided: default */

    /***/
    function node_modulesRawLoaderDistCjsJsSrcAppPagesTabsMenuCoachingCoachingPageHtml(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony default export */


      __webpack_exports__["default"] = "<!-- Header without a border -->\n<ion-header>\n    <ion-toolbar class=\"ion-text-center\">\n        <ion-title>LE COACHING TRANSFORMATICS</ion-title>\n    </ion-toolbar>\n</ion-header>\n\n<ion-content>\n    <ion-grid>\n        <ion-row>\n            <ion-col size=\"12\">\n                <div class=\"f14 ion-text-justify\">\n                    <p class=\"small_text\">\n                        Vous faire coacher, c’est entreprendre un long voyage avec celui qui s’engage corps et âme à vous aider à attendre vos objectifs. Le système de coaching de Transformatics vous permet de sélectionner un de nos préparateurs physiques certifiés afin d’entamer\n                        une collaboration sur une durée variable. Cette sélection se réalise en fonction de votre demande, à savoir le type de préparation que vous souhaitez suivre : un coaching standard, ou en vue d’une compétition ; ou même des préparations\n                        dans d’autres sports (natation, football, rugby, …). Vous aurez également accès aux diplômes de nos coachs et une brève biographie qui vous permettront de faire votre choix plus facilement.\n                    </p>\n                    <h4 class=\"f16 ion-text-center medium_text_bold\">POUR TOUT COACHING, TRANSFORMATICS VOUS OFFRE VOTRE ABONNEMENT ! </h4>\n                </div>\n            </ion-col>\n            <ion-col size=\"12\">\n                <ion-card class=\"card-box ion-no-margin\">\n                    <ion-item>\n                        <img class=\"logo\" style=\"width: 40px;\" slot=\"start\" src=\"assets/icon/traficationlogo.png\">\n                        <ion-label>TARIFICATION & ENGAGEMENT\n                        </ion-label>\n                    </ion-item>\n\n                    <ion-card-content class=\"ion-text-justify small_text_light\">\n                        Tous les coachings du système Transformatics reposent sur un engagement de 6 mois ou 1 an. En fonction de la prestation choisie, vous bénéficierez d’une réduction sur le tarif mensuel initial. Cependant, quelle que soit la prestation sélectionnée, votre\n                        abonnement au système vous sera offert pendant toute la durée du coaching ! La première étape pour vous lancer dans un coaching personnalisé, c’est de vous inscrire !\n                    </ion-card-content>\n                    <div class=\"ribbon-wrapper-1\">\n                        <div class=\"ribbon-1\">Ribbon</div>\n                    </div>\n                </ion-card>\n            </ion-col>\n            <ion-col size=\"6\">\n                <ion-button class=\"leftbtn border-5\" shape=\"round\" expand=\"full\" routerLink=\"/find-coach\">Choisir son coach</ion-button>\n            </ion-col>\n            <ion-col size=\"6 \">\n                <ion-button class=\"border-5\" color=\"dark \" shape=\"round\" expand=\"full \">Voir le classement</ion-button>\n            </ion-col>\n        </ion-row>\n    </ion-grid>\n</ion-content>";
      /***/
    },

    /***/
    "./src/app/pages/tabs/menu/coaching/coaching-routing.module.ts":
    /*!*********************************************************************!*\
      !*** ./src/app/pages/tabs/menu/coaching/coaching-routing.module.ts ***!
      \*********************************************************************/

    /*! exports provided: CoachingPageRoutingModule */

    /***/
    function srcAppPagesTabsMenuCoachingCoachingRoutingModuleTs(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "CoachingPageRoutingModule", function () {
        return CoachingPageRoutingModule;
      });
      /* harmony import */


      var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! tslib */
      "./node_modules/tslib/tslib.es6.js");
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! @angular/core */
      "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
      /* harmony import */


      var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! @angular/router */
      "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");
      /* harmony import */


      var _coaching_page__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
      /*! ./coaching.page */
      "./src/app/pages/tabs/menu/coaching/coaching.page.ts");

      var routes = [{
        path: '',
        component: _coaching_page__WEBPACK_IMPORTED_MODULE_3__["CoachingPage"]
      }];

      var CoachingPageRoutingModule = function CoachingPageRoutingModule() {
        _classCallCheck(this, CoachingPageRoutingModule);
      };

      CoachingPageRoutingModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]]
      })], CoachingPageRoutingModule);
      /***/
    },

    /***/
    "./src/app/pages/tabs/menu/coaching/coaching.module.ts":
    /*!*************************************************************!*\
      !*** ./src/app/pages/tabs/menu/coaching/coaching.module.ts ***!
      \*************************************************************/

    /*! exports provided: CoachingPageModule */

    /***/
    function srcAppPagesTabsMenuCoachingCoachingModuleTs(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "CoachingPageModule", function () {
        return CoachingPageModule;
      });
      /* harmony import */


      var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! tslib */
      "./node_modules/tslib/tslib.es6.js");
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! @angular/core */
      "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
      /* harmony import */


      var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! @angular/common */
      "./node_modules/@angular/common/__ivy_ngcc__/fesm2015/common.js");
      /* harmony import */


      var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
      /*! @angular/forms */
      "./node_modules/@angular/forms/__ivy_ngcc__/fesm2015/forms.js");
      /* harmony import */


      var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
      /*! @ionic/angular */
      "./node_modules/@ionic/angular/__ivy_ngcc__/fesm2015/ionic-angular.js");
      /* harmony import */


      var _coaching_routing_module__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(
      /*! ./coaching-routing.module */
      "./src/app/pages/tabs/menu/coaching/coaching-routing.module.ts");
      /* harmony import */


      var _coaching_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(
      /*! ./coaching.page */
      "./src/app/pages/tabs/menu/coaching/coaching.page.ts");

      var CoachingPageModule = function CoachingPageModule() {
        _classCallCheck(this, CoachingPageModule);
      };

      CoachingPageModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"], _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"], _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonicModule"], _coaching_routing_module__WEBPACK_IMPORTED_MODULE_5__["CoachingPageRoutingModule"]],
        declarations: [_coaching_page__WEBPACK_IMPORTED_MODULE_6__["CoachingPage"]]
      })], CoachingPageModule);
      /***/
    },

    /***/
    "./src/app/pages/tabs/menu/coaching/coaching.page.scss":
    /*!*************************************************************!*\
      !*** ./src/app/pages/tabs/menu/coaching/coaching.page.scss ***!
      \*************************************************************/

    /*! exports provided: default */

    /***/
    function srcAppPagesTabsMenuCoachingCoachingPageScss(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony default export */


      __webpack_exports__["default"] = ".card-box {\n  box-shadow: rgba(0, 0, 0, 0.12) 0px 4px 16px;\n  padding: 5px;\n}\n\n.ribbon-wrapper-1 {\n  width: 51px;\n  height: 65px;\n  overflow: hidden;\n  position: absolute;\n  top: 0px;\n  right: 0px;\n}\n\n.ribbon-1 {\n  font: bold 15px Sans-Serif;\n  line-height: 18px;\n  color: #333;\n  text-align: center;\n  text-transform: uppercase;\n  -webkit-transform: rotate(47deg);\n  -moz-transform: rotate(45deg);\n  -ms-transform: rotate(45deg);\n  -o-transform: rotate(45deg);\n  position: relative;\n  padding: 7px 0;\n  left: 0px;\n  top: 16px;\n  height: 44px;\n  width: 150px;\n  background: var(--ion-btn-custom-color);\n  color: #fff;\n  box-shadow: 0 4px 6px rgba(0, 0, 0, 0.1);\n  letter-spacing: 0.5px;\n  z-index: 1;\n}\n\n.card-box ion-item {\n  --inner-border-width: 0 0 0px 0;\n}\n\n.logo {\n  width: 40px;\n  border: 1px solid #000;\n  border-radius: 25px;\n  height: 40px;\n  padding: 0px;\n}\n\n.leftbtn {\n  --background: var(--ion-btn-custom-color) ;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvcGFnZXMvdGFicy9tZW51L2NvYWNoaW5nL2NvYWNoaW5nLnBhZ2Uuc2NzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFXQTtFQUNJLDRDQUFBO0VBQ0EsWUFBQTtBQVZKOztBQWFBO0VBQ0ksV0FBQTtFQUNBLFlBQUE7RUFDQSxnQkFBQTtFQUNBLGtCQUFBO0VBQ0EsUUFBQTtFQUNBLFVBQUE7QUFWSjs7QUFhQTtFQUNJLDBCQUFBO0VBQ0EsaUJBQUE7RUFDQSxXQUFBO0VBQ0Esa0JBQUE7RUFDQSx5QkFBQTtFQUNBLGdDQUFBO0VBQ0EsNkJBQUE7RUFDQSw0QkFBQTtFQUNBLDJCQUFBO0VBQ0Esa0JBQUE7RUFDQSxjQUFBO0VBQ0EsU0FBQTtFQUNBLFNBQUE7RUFDQSxZQUFBO0VBQ0EsWUFBQTtFQUNBLHVDQUFBO0VBQ0EsV0FBQTtFQUNBLHdDQUFBO0VBQ0EscUJBQUE7RUFDQSxVQUFBO0FBVko7O0FBY0E7RUFDSSwrQkFBQTtBQVhKOztBQWNBO0VBQ0ksV0FBQTtFQUNBLHNCQUFBO0VBQ0EsbUJBQUE7RUFDQSxZQUFBO0VBQ0EsWUFBQTtBQVhKOztBQWNBO0VBQ0ksMENBQUE7QUFYSiIsImZpbGUiOiJzcmMvYXBwL3BhZ2VzL3RhYnMvbWVudS9jb2FjaGluZy9jb2FjaGluZy5wYWdlLnNjc3MiLCJzb3VyY2VzQ29udGVudCI6WyIvLyAuY2FyZC1ib3g6YmVmb3JlIHtcbi8vICAgICBjb250ZW50OiBcIlwiO1xuLy8gICAgIGJvcmRlci1yaWdodDogMjBweCBzb2xpZCAjRjRCMTgzO1xuLy8gICAgIGJvcmRlci10b3A6IDIwcHggc29saWQgI0Y0QjE4Mztcbi8vICAgICBib3JkZXItbGVmdDogMjBweCBzb2xpZCAjZmYwMDAwMDA7XG4vLyAgICAgYm9yZGVyLWJvdHRvbTogMjBweCBzb2xpZCAjZmYwMDAwMDA7XG4vLyAgICAgcG9zaXRpb246IGFic29sdXRlO1xuLy8gICAgIHRvcDogMDtcbi8vICAgICByaWdodDogMDtcbi8vICAgICB6LWluZGV4OiA5OTk7XG4vLyB9XG4uY2FyZC1ib3gge1xuICAgIGJveC1zaGFkb3c6IHJnYigwIDAgMCAvIDEyJSkgMHB4IDRweCAxNnB4O1xuICAgIHBhZGRpbmc6IDVweDtcbn1cblxuLnJpYmJvbi13cmFwcGVyLTEge1xuICAgIHdpZHRoOiA1MXB4O1xuICAgIGhlaWdodDogNjVweDtcbiAgICBvdmVyZmxvdzogaGlkZGVuO1xuICAgIHBvc2l0aW9uOiBhYnNvbHV0ZTtcbiAgICB0b3A6IDBweDtcbiAgICByaWdodDogMHB4O1xufVxuXG4ucmliYm9uLTEge1xuICAgIGZvbnQ6IGJvbGQgMTVweCBTYW5zLVNlcmlmO1xuICAgIGxpbmUtaGVpZ2h0OiAxOHB4O1xuICAgIGNvbG9yOiAjMzMzO1xuICAgIHRleHQtYWxpZ246IGNlbnRlcjtcbiAgICB0ZXh0LXRyYW5zZm9ybTogdXBwZXJjYXNlO1xuICAgIC13ZWJraXQtdHJhbnNmb3JtOiByb3RhdGUoIDQ3ZGVnKTtcbiAgICAtbW96LXRyYW5zZm9ybTogcm90YXRlKDQ1ZGVnKTtcbiAgICAtbXMtdHJhbnNmb3JtOiByb3RhdGUoNDVkZWcpO1xuICAgIC1vLXRyYW5zZm9ybTogcm90YXRlKDQ1ZGVnKTtcbiAgICBwb3NpdGlvbjogcmVsYXRpdmU7XG4gICAgcGFkZGluZzogN3B4IDA7XG4gICAgbGVmdDogMHB4O1xuICAgIHRvcDogMTZweDtcbiAgICBoZWlnaHQ6IDQ0cHg7XG4gICAgd2lkdGg6IDE1MHB4O1xuICAgIGJhY2tncm91bmQ6IHZhcigtLWlvbi1idG4tY3VzdG9tLWNvbG9yKTtcbiAgICBjb2xvcjogI2ZmZjtcbiAgICBib3gtc2hhZG93OiAwIDRweCA2cHggcmdiKDAgMCAwIC8gMTAlKTtcbiAgICBsZXR0ZXItc3BhY2luZzogMC41cHg7XG4gICAgei1pbmRleDogMTtcbn1cblxuLy8gPT09PT09PT09PT09PT09PT1yaWJib249PT09PT09PT09PT09PT09PT09PT1cbi5jYXJkLWJveCBpb24taXRlbSB7XG4gICAgLS1pbm5lci1ib3JkZXItd2lkdGg6IDAgMCAwcHggMDtcbn1cblxuLmxvZ28ge1xuICAgIHdpZHRoOiA0MHB4O1xuICAgIGJvcmRlcjogMXB4IHNvbGlkICMwMDA7XG4gICAgYm9yZGVyLXJhZGl1czogMjVweDtcbiAgICBoZWlnaHQ6IDQwcHg7XG4gICAgcGFkZGluZzogMHB4O1xufVxuXG4ubGVmdGJ0biB7XG4gICAgLS1iYWNrZ3JvdW5kOiB2YXIoLS1pb24tYnRuLWN1c3RvbS1jb2xvcilcbn0iXX0= */";
      /***/
    },

    /***/
    "./src/app/pages/tabs/menu/coaching/coaching.page.ts":
    /*!***********************************************************!*\
      !*** ./src/app/pages/tabs/menu/coaching/coaching.page.ts ***!
      \***********************************************************/

    /*! exports provided: CoachingPage */

    /***/
    function srcAppPagesTabsMenuCoachingCoachingPageTs(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "CoachingPage", function () {
        return CoachingPage;
      });
      /* harmony import */


      var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! tslib */
      "./node_modules/tslib/tslib.es6.js");
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! @angular/core */
      "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");

      var CoachingPage = /*#__PURE__*/function () {
        function CoachingPage() {
          _classCallCheck(this, CoachingPage);

          this.categories = {
            slidesPerView: 1.8,
            effect: 'flip',
            slidesPerColumn: 1,
            slidesPerGroup: 1,
            watchSlidesProgress: true
          };
        }

        _createClass(CoachingPage, [{
          key: "ngOnInit",
          value: function ngOnInit() {}
        }]);

        return CoachingPage;
      }();

      CoachingPage.ctorParameters = function () {
        return [];
      };

      CoachingPage = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-coaching',
        template: Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(
        /*! raw-loader!./coaching.page.html */
        "./node_modules/raw-loader/dist/cjs.js!./src/app/pages/tabs/menu/coaching/coaching.page.html"))["default"],
        styles: [Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(
        /*! ./coaching.page.scss */
        "./src/app/pages/tabs/menu/coaching/coaching.page.scss"))["default"]]
      })], CoachingPage);
      /***/
    }
  }]);
})();
//# sourceMappingURL=pages-tabs-menu-coaching-coaching-module-es5.js.map