(function () {
  function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

  function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

  function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

  (window["webpackJsonp"] = window["webpackJsonp"] || []).push([["pages-select-roles-select-roles-module"], {
    /***/
    "./node_modules/raw-loader/dist/cjs.js!./src/app/pages/select-roles/select-roles.page.html":
    /*!*************************************************************************************************!*\
      !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/pages/select-roles/select-roles.page.html ***!
      \*************************************************************************************************/

    /*! exports provided: default */

    /***/
    function node_modulesRawLoaderDistCjsJsSrcAppPagesSelectRolesSelectRolesPageHtml(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony default export */


      __webpack_exports__["default"] = "<!-- <ion-header class=\"ion-no-border\">\n  <ion-toolbar mode=\"ios\">\n    <ion-title>Transformatics</ion-title>\n  </ion-toolbar>\n</ion-header> -->\n\n<ion-content>\n  <div class=\"main-container\">\n    <div class=\"inner-container\">\n      <div class=\"circle-container\" (click)=\"goToAuth(false)\">\n        <img src=\"assets/icon/athlete.png\" />\n      </div>\n      <h4>JE SUIS UN ATHLETE</h4>\n      <p>Je me connecte en tant que sportif membre du système Transformatics</p>\n    </div>\n\n    <div class=\"inner-container\">\n      <div class=\"circle-container\" (click)=\"goToAuth(true)\">\n        <img src=\"assets/icon/coach.png\" />\n      </div>\n      <h4>JE SUIS UN COACH</h4>\n      <p>Je me connecte en tant que coach certifié du système Transformatics</p>\n    </div>\n\n  </div>\n</ion-content>\n";
      /***/
    },

    /***/
    "./src/app/pages/select-roles/select-roles-routing.module.ts":
    /*!*******************************************************************!*\
      !*** ./src/app/pages/select-roles/select-roles-routing.module.ts ***!
      \*******************************************************************/

    /*! exports provided: SelectRolesPageRoutingModule */

    /***/
    function srcAppPagesSelectRolesSelectRolesRoutingModuleTs(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "SelectRolesPageRoutingModule", function () {
        return SelectRolesPageRoutingModule;
      });
      /* harmony import */


      var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! tslib */
      "./node_modules/tslib/tslib.es6.js");
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! @angular/core */
      "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
      /* harmony import */


      var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! @angular/router */
      "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");
      /* harmony import */


      var _select_roles_page__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
      /*! ./select-roles.page */
      "./src/app/pages/select-roles/select-roles.page.ts");

      var routes = [{
        path: '',
        component: _select_roles_page__WEBPACK_IMPORTED_MODULE_3__["SelectRolesPage"]
      }];

      var SelectRolesPageRoutingModule = function SelectRolesPageRoutingModule() {
        _classCallCheck(this, SelectRolesPageRoutingModule);
      };

      SelectRolesPageRoutingModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]]
      })], SelectRolesPageRoutingModule);
      /***/
    },

    /***/
    "./src/app/pages/select-roles/select-roles.module.ts":
    /*!***********************************************************!*\
      !*** ./src/app/pages/select-roles/select-roles.module.ts ***!
      \***********************************************************/

    /*! exports provided: SelectRolesPageModule */

    /***/
    function srcAppPagesSelectRolesSelectRolesModuleTs(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "SelectRolesPageModule", function () {
        return SelectRolesPageModule;
      });
      /* harmony import */


      var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! tslib */
      "./node_modules/tslib/tslib.es6.js");
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! @angular/core */
      "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
      /* harmony import */


      var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! @angular/common */
      "./node_modules/@angular/common/__ivy_ngcc__/fesm2015/common.js");
      /* harmony import */


      var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
      /*! @angular/forms */
      "./node_modules/@angular/forms/__ivy_ngcc__/fesm2015/forms.js");
      /* harmony import */


      var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
      /*! @ionic/angular */
      "./node_modules/@ionic/angular/__ivy_ngcc__/fesm2015/ionic-angular.js");
      /* harmony import */


      var _select_roles_routing_module__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(
      /*! ./select-roles-routing.module */
      "./src/app/pages/select-roles/select-roles-routing.module.ts");
      /* harmony import */


      var _select_roles_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(
      /*! ./select-roles.page */
      "./src/app/pages/select-roles/select-roles.page.ts");

      var SelectRolesPageModule = function SelectRolesPageModule() {
        _classCallCheck(this, SelectRolesPageModule);
      };

      SelectRolesPageModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"], _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"], _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonicModule"], _select_roles_routing_module__WEBPACK_IMPORTED_MODULE_5__["SelectRolesPageRoutingModule"]],
        declarations: [_select_roles_page__WEBPACK_IMPORTED_MODULE_6__["SelectRolesPage"]]
      })], SelectRolesPageModule);
      /***/
    },

    /***/
    "./src/app/pages/select-roles/select-roles.page.scss":
    /*!***********************************************************!*\
      !*** ./src/app/pages/select-roles/select-roles.page.scss ***!
      \***********************************************************/

    /*! exports provided: default */

    /***/
    function srcAppPagesSelectRolesSelectRolesPageScss(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony default export */


      __webpack_exports__["default"] = "ion-toolbar {\n  --background: #000;\n}\n\nion-content {\n  --background: url('role.png') 0 0/100% no-repeat;\n  background-size: cover;\n  --overflow: hidden;\n}\n\nion-title {\n  color: #fff;\n  font-family: Anton, sans-serif;\n  font-size: 17px;\n  letter-spacing: 4px;\n}\n\nh4 {\n  color: #fff;\n  font-family: Oswald-SemiBold, serif;\n  margin: 0;\n  margin-top: 20px;\n  font-size: 15px;\n  font-weight: bold;\n  letter-spacing: 1px;\n  color: var(--ion-color-primary);\n}\n\n.main-container {\n  height: 100%;\n  text-align: center;\n  display: flex;\n  flex-direction: column;\n  justify-content: center;\n  padding-left: 32%;\n  padding-right: 32%;\n}\n\np {\n  color: #b9b8b8;\n  margin: 0;\n  margin-top: 5px;\n  font-size: 12px;\n  font-family: Oswald, serif;\n  line-height: 1rem;\n}\n\n.circle-container {\n  border: 3px solid var(--ion-color-primary);\n  padding: 5px;\n  border-radius: 50%;\n  height: 90px;\n  width: 90px;\n}\n\n.circle-container img {\n  padding-top: 12%;\n}\n\n.inner-container {\n  display: flex;\n  flex-direction: column;\n  align-items: center;\n  margin-bottom: 40px;\n}\n\n.toolbar-background {\n  border-color: transparent !important;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvcGFnZXMvc2VsZWN0LXJvbGVzL3NlbGVjdC1yb2xlcy5wYWdlLnNjc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUE7RUFDSSxrQkFBQTtBQUNKOztBQUVBO0VBQ0ksZ0RBQUE7RUFHQSxzQkFBQTtFQUNBLGtCQUFBO0FBQ0o7O0FBR0E7RUFDSSxXQUFBO0VBQ0EsOEJBQUE7RUFDQSxlQUFBO0VBQ0EsbUJBQUE7QUFBSjs7QUFJQTtFQUNJLFdBQUE7RUFDQSxtQ0FBQTtFQUNBLFNBQUE7RUFDQSxnQkFBQTtFQUNBLGVBQUE7RUFDQSxpQkFBQTtFQUNBLG1CQUFBO0VBQ0EsK0JBQUE7QUFESjs7QUFJQTtFQUNJLFlBQUE7RUFDQSxrQkFBQTtFQUNBLGFBQUE7RUFDQSxzQkFBQTtFQUNBLHVCQUFBO0VBQ0EsaUJBQUE7RUFDQSxrQkFBQTtBQURKOztBQUlBO0VBQ0ksY0FBQTtFQUNBLFNBQUE7RUFDQSxlQUFBO0VBQ0EsZUFBQTtFQUNBLDBCQUFBO0VBQ0EsaUJBQUE7QUFESjs7QUFJQTtFQUNJLDBDQUFBO0VBQ0EsWUFBQTtFQUNBLGtCQUFBO0VBQ0EsWUFBQTtFQUNBLFdBQUE7QUFESjs7QUFFSTtFQUNJLGdCQUFBO0FBQVI7O0FBSUE7RUFDSSxhQUFBO0VBQ0Esc0JBQUE7RUFDQSxtQkFBQTtFQUNBLG1CQUFBO0FBREo7O0FBSUE7RUFDSSxvQ0FBQTtBQURKIiwiZmlsZSI6InNyYy9hcHAvcGFnZXMvc2VsZWN0LXJvbGVzL3NlbGVjdC1yb2xlcy5wYWdlLnNjc3MiLCJzb3VyY2VzQ29udGVudCI6WyJpb24tdG9vbGJhciB7XG4gICAgLS1iYWNrZ3JvdW5kOiAjMDAwO1xufVxuXG5pb24tY29udGVudCB7XG4gICAgLS1iYWNrZ3JvdW5kOiB1cmwoLi4vLi4vLi4vYXNzZXRzL2ltYWdlcy9yb2xlLnBuZykgMCAwLzEwMCUgbm8tcmVwZWF0O1xuICAgIC13ZWJraXQtYmFja2dyb3VuZC1zaXplOiBjb3ZlcjtcbiAgICAtbW96LWJhY2tncm91bmQtc2l6ZTogY292ZXI7XG4gICAgYmFja2dyb3VuZC1zaXplOiBjb3ZlcjtcbiAgICAtLW92ZXJmbG93OiBoaWRkZW47XG5cbn1cblxuaW9uLXRpdGxlIHtcbiAgICBjb2xvcjogI2ZmZjtcbiAgICBmb250LWZhbWlseTogQW50b24sIHNhbnMtc2VyaWY7XG4gICAgZm9udC1zaXplOiAxN3B4O1xuICAgIGxldHRlci1zcGFjaW5nOiA0cHg7XG5cbn1cblxuaDQge1xuICAgIGNvbG9yOiAjZmZmO1xuICAgIGZvbnQtZmFtaWx5OiBPc3dhbGQtU2VtaUJvbGQsIHNlcmlmO1xuICAgIG1hcmdpbjogMDtcbiAgICBtYXJnaW4tdG9wOiAyMHB4O1xuICAgIGZvbnQtc2l6ZTogMTVweDtcbiAgICBmb250LXdlaWdodDogYm9sZDtcbiAgICBsZXR0ZXItc3BhY2luZzogMXB4O1xuICAgIGNvbG9yOiB2YXIoLS1pb24tY29sb3ItcHJpbWFyeSk7XG59XG5cbi5tYWluLWNvbnRhaW5lciB7XG4gICAgaGVpZ2h0OiAxMDAlO1xuICAgIHRleHQtYWxpZ246IGNlbnRlcjtcbiAgICBkaXNwbGF5OiBmbGV4O1xuICAgIGZsZXgtZGlyZWN0aW9uOiBjb2x1bW47XG4gICAganVzdGlmeS1jb250ZW50OiBjZW50ZXI7XG4gICAgcGFkZGluZy1sZWZ0OiAzMiU7XG4gICAgcGFkZGluZy1yaWdodDogMzIlO1xufVxuXG5wIHtcbiAgICBjb2xvcjogI2I5YjhiODtcbiAgICBtYXJnaW46IDA7XG4gICAgbWFyZ2luLXRvcDogNXB4O1xuICAgIGZvbnQtc2l6ZTogMTJweDtcbiAgICBmb250LWZhbWlseTogT3N3YWxkLCBzZXJpZjtcbiAgICBsaW5lLWhlaWdodDogMXJlbTtcbn1cblxuLmNpcmNsZS1jb250YWluZXIge1xuICAgIGJvcmRlcjogM3B4IHNvbGlkIHZhcigtLWlvbi1jb2xvci1wcmltYXJ5KTtcbiAgICBwYWRkaW5nOiA1cHg7XG4gICAgYm9yZGVyLXJhZGl1czogNTAlO1xuICAgIGhlaWdodDogOTBweDtcbiAgICB3aWR0aDogOTBweDtcbiAgICBpbWcge1xuICAgICAgICBwYWRkaW5nLXRvcDogMTIlO1xuICAgIH1cbn1cblxuLmlubmVyLWNvbnRhaW5lciB7XG4gICAgZGlzcGxheTogZmxleDtcbiAgICBmbGV4LWRpcmVjdGlvbjogY29sdW1uO1xuICAgIGFsaWduLWl0ZW1zOiBjZW50ZXI7XG4gICAgbWFyZ2luLWJvdHRvbTogNDBweDtcbn1cblxuLnRvb2xiYXItYmFja2dyb3VuZCB7XG4gICAgYm9yZGVyLWNvbG9yOiB0cmFuc3BhcmVudCAhaW1wb3J0YW50O1xufSJdfQ== */";
      /***/
    },

    /***/
    "./src/app/pages/select-roles/select-roles.page.ts":
    /*!*********************************************************!*\
      !*** ./src/app/pages/select-roles/select-roles.page.ts ***!
      \*********************************************************/

    /*! exports provided: SelectRolesPage */

    /***/
    function srcAppPagesSelectRolesSelectRolesPageTs(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "SelectRolesPage", function () {
        return SelectRolesPage;
      });
      /* harmony import */


      var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! tslib */
      "./node_modules/tslib/tslib.es6.js");
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! @angular/core */
      "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
      /* harmony import */


      var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! @angular/router */
      "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");
      /* harmony import */


      var _services_shared_localstroage_localstorage_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
      /*! @services/shared/localstroage/localstorage.service */
      "./src/app/services/shared/localstroage/localstorage.service.ts");
      /* harmony import */


      var _services_userdata_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
      /*! @services/userdata.service */
      "./src/app/services/userdata.service.ts");

      var SelectRolesPage = /*#__PURE__*/function () {
        function SelectRolesPage(router, userData, localStorage) {
          _classCallCheck(this, SelectRolesPage);

          this.router = router;
          this.userData = userData;
          this.localStorage = localStorage;
        }

        _createClass(SelectRolesPage, [{
          key: "ngOnInit",
          value: function ngOnInit() {}
        }, {
          key: "goToAuth",
          value: function goToAuth(role) {
            return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, /*#__PURE__*/regeneratorRuntime.mark(function _callee() {
              return regeneratorRuntime.wrap(function _callee$(_context) {
                while (1) {
                  switch (_context.prev = _context.next) {
                    case 0:
                      this.localStorage.setItem('registrationRole', role ? 'coach' : 'athlete');

                      if (!role) {
                        _context.next = 4;
                        break;
                      }

                      this.router.navigate(['/auth']);
                      return _context.abrupt("return", this.userData.isCoach = true);

                    case 4:
                      this.router.navigate(['/auth']);
                      return _context.abrupt("return", this.userData.isCoach = false);

                    case 6:
                    case "end":
                      return _context.stop();
                  }
                }
              }, _callee, this);
            }));
          }
        }]);

        return SelectRolesPage;
      }();

      SelectRolesPage.ctorParameters = function () {
        return [{
          type: _angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"]
        }, {
          type: _services_userdata_service__WEBPACK_IMPORTED_MODULE_4__["UserdataService"]
        }, {
          type: _services_shared_localstroage_localstorage_service__WEBPACK_IMPORTED_MODULE_3__["LocalstorageService"]
        }];
      };

      SelectRolesPage = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-select-roles',
        template: Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(
        /*! raw-loader!./select-roles.page.html */
        "./node_modules/raw-loader/dist/cjs.js!./src/app/pages/select-roles/select-roles.page.html"))["default"],
        styles: [Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(
        /*! ./select-roles.page.scss */
        "./src/app/pages/select-roles/select-roles.page.scss"))["default"]]
      })], SelectRolesPage);
      /***/
    }
  }]);
})();
//# sourceMappingURL=pages-select-roles-select-roles-module-es5.js.map