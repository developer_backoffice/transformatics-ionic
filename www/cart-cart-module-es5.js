(function () {
  function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

  function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

  function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

  (window["webpackJsonp"] = window["webpackJsonp"] || []).push([["cart-cart-module"], {
    /***/
    "./node_modules/raw-loader/dist/cjs.js!./src/app/pages/tabs/cart/cart.page.html":
    /*!**************************************************************************************!*\
      !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/pages/tabs/cart/cart.page.html ***!
      \**************************************************************************************/

    /*! exports provided: default */

    /***/
    function node_modulesRawLoaderDistCjsJsSrcAppPagesTabsCartCartPageHtml(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony default export */


      __webpack_exports__["default"] = "<ion-content (ionScroll)=\"scrollHandler($event)\" scrollEvents=\"true\">\n\n    <div class=\"markts-menu\" routerLink=\"/ebook\">\n        <img src=\"assets/images/market1.jpg\">\n        <h4>E-BOOKS TRANSFORMATICS</h4>\n    </div>\n\n    <div class=\"markts-menu\" routerLink=\"/cloth-accessoires\">\n        <img src=\"assets/images/market2.jpg\">\n        <h4>VÊTEMENTS ET ACCESSOIRES</h4>\n    </div>\n\n    <div class=\"markts-menu\" routerLink=\"/food-supplementation\">\n        <img src=\"assets/images/market3.jpg\">\n        <h4>ALIMENTATION ET COMPLÉMENTATION</h4>\n    </div>\n\n    <div class=\"markts-menu\" routerLink=\"/sports-metrial\">\n        <img src=\"assets/images/market4.jpg\">\n        <h4>MATÉRIEL SPORTIF</h4>\n    </div>\n\n</ion-content>";
      /***/
    },

    /***/
    "./src/app/pages/tabs/cart/cart-routing.module.ts":
    /*!********************************************************!*\
      !*** ./src/app/pages/tabs/cart/cart-routing.module.ts ***!
      \********************************************************/

    /*! exports provided: CartPageRoutingModule */

    /***/
    function srcAppPagesTabsCartCartRoutingModuleTs(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "CartPageRoutingModule", function () {
        return CartPageRoutingModule;
      });
      /* harmony import */


      var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! tslib */
      "./node_modules/tslib/tslib.es6.js");
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! @angular/core */
      "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
      /* harmony import */


      var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! @angular/router */
      "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");
      /* harmony import */


      var _cart_page__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
      /*! ./cart.page */
      "./src/app/pages/tabs/cart/cart.page.ts");

      var routes = [{
        path: '',
        component: _cart_page__WEBPACK_IMPORTED_MODULE_3__["CartPage"]
      }];

      var CartPageRoutingModule = function CartPageRoutingModule() {
        _classCallCheck(this, CartPageRoutingModule);
      };

      CartPageRoutingModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]]
      })], CartPageRoutingModule);
      /***/
    },

    /***/
    "./src/app/pages/tabs/cart/cart.module.ts":
    /*!************************************************!*\
      !*** ./src/app/pages/tabs/cart/cart.module.ts ***!
      \************************************************/

    /*! exports provided: CartPageModule */

    /***/
    function srcAppPagesTabsCartCartModuleTs(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "CartPageModule", function () {
        return CartPageModule;
      });
      /* harmony import */


      var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! tslib */
      "./node_modules/tslib/tslib.es6.js");
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! @angular/core */
      "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
      /* harmony import */


      var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! @angular/common */
      "./node_modules/@angular/common/__ivy_ngcc__/fesm2015/common.js");
      /* harmony import */


      var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
      /*! @angular/forms */
      "./node_modules/@angular/forms/__ivy_ngcc__/fesm2015/forms.js");
      /* harmony import */


      var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
      /*! @ionic/angular */
      "./node_modules/@ionic/angular/__ivy_ngcc__/fesm2015/ionic-angular.js");
      /* harmony import */


      var _cart_routing_module__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(
      /*! ./cart-routing.module */
      "./src/app/pages/tabs/cart/cart-routing.module.ts");
      /* harmony import */


      var _cart_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(
      /*! ./cart.page */
      "./src/app/pages/tabs/cart/cart.page.ts");

      var CartPageModule = function CartPageModule() {
        _classCallCheck(this, CartPageModule);
      };

      CartPageModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"], _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"], _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonicModule"], _cart_routing_module__WEBPACK_IMPORTED_MODULE_5__["CartPageRoutingModule"]],
        declarations: [_cart_page__WEBPACK_IMPORTED_MODULE_6__["CartPage"]]
      })], CartPageModule);
      /***/
    },

    /***/
    "./src/app/pages/tabs/cart/cart.page.scss":
    /*!************************************************!*\
      !*** ./src/app/pages/tabs/cart/cart.page.scss ***!
      \************************************************/

    /*! exports provided: default */

    /***/
    function srcAppPagesTabsCartCartPageScss(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony default export */


      __webpack_exports__["default"] = ".markts-menu h4 {\n  position: absolute;\n  color: #fff;\n  z-index: 1;\n  top: 50%;\n  transform: translateY(-50%);\n  margin: 0;\n  width: 100%;\n  text-align: center;\n}\n\n.markts-menu {\n  position: relative;\n  height: 180px;\n}\n\n.markts-menu img {\n  width: 100%;\n  height: 100%;\n  -o-object-fit: cover;\n     object-fit: cover;\n}\n\nion-content {\n  --padding-bottom: 20% !important;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvcGFnZXMvdGFicy9jYXJ0L2NhcnQucGFnZS5zY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBO0VBQ0ksa0JBQUE7RUFDQSxXQUFBO0VBQ0EsVUFBQTtFQUNBLFFBQUE7RUFDQSwyQkFBQTtFQUNBLFNBQUE7RUFDQSxXQUFBO0VBQ0Esa0JBQUE7QUFDSjs7QUFFQTtFQUNJLGtCQUFBO0VBQ0EsYUFBQTtBQUNKOztBQUVBO0VBQ0ksV0FBQTtFQUNBLFlBQUE7RUFDQSxvQkFBQTtLQUFBLGlCQUFBO0FBQ0o7O0FBR0E7RUFDSSxnQ0FBQTtBQUFKIiwiZmlsZSI6InNyYy9hcHAvcGFnZXMvdGFicy9jYXJ0L2NhcnQucGFnZS5zY3NzIiwic291cmNlc0NvbnRlbnQiOlsiLm1hcmt0cy1tZW51IGg0IHtcbiAgICBwb3NpdGlvbjogYWJzb2x1dGU7XG4gICAgY29sb3I6ICNmZmY7XG4gICAgei1pbmRleDogMTtcbiAgICB0b3A6IDUwJTtcbiAgICB0cmFuc2Zvcm06IHRyYW5zbGF0ZVkoLTUwJSk7XG4gICAgbWFyZ2luOiAwO1xuICAgIHdpZHRoOiAxMDAlO1xuICAgIHRleHQtYWxpZ246IGNlbnRlcjtcbn1cblxuLm1hcmt0cy1tZW51IHtcbiAgICBwb3NpdGlvbjogcmVsYXRpdmU7XG4gICAgaGVpZ2h0OiAxODBweDtcbn1cblxuLm1hcmt0cy1tZW51IGltZyB7XG4gICAgd2lkdGg6IDEwMCU7XG4gICAgaGVpZ2h0OiAxMDAlO1xuICAgIG9iamVjdC1maXQ6IGNvdmVyO1xufVxuXG5cbmlvbi1jb250ZW50IHtcbiAgICAtLXBhZGRpbmctYm90dG9tOiAyMCUgIWltcG9ydGFudDtcbn0iXX0= */";
      /***/
    },

    /***/
    "./src/app/pages/tabs/cart/cart.page.ts":
    /*!**********************************************!*\
      !*** ./src/app/pages/tabs/cart/cart.page.ts ***!
      \**********************************************/

    /*! exports provided: CartPage */

    /***/
    function srcAppPagesTabsCartCartPageTs(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "CartPage", function () {
        return CartPage;
      });
      /* harmony import */


      var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! tslib */
      "./node_modules/tslib/tslib.es6.js");
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! @angular/core */
      "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
      /* harmony import */


      var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! @angular/router */
      "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");
      /* harmony import */


      var _services_userdata_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
      /*! @services/userdata.service */
      "./src/app/services/userdata.service.ts");

      var CartPage = /*#__PURE__*/function () {
        function CartPage(router, userData, zone) {
          _classCallCheck(this, CartPage);

          this.router = router;
          this.userData = userData;
          this.zone = zone;
          this.lastY = 0;
        }

        _createClass(CartPage, [{
          key: "ngOnInit",
          value: function ngOnInit() {}
        }, {
          key: "routerWatch",
          value: function routerWatch() {
            var _this = this;

            this.routerSubscription = this.router.events.subscribe(function (event) {
              if (event instanceof _angular_router__WEBPACK_IMPORTED_MODULE_2__["NavigationEnd"]) {
                if (event.url == '/tabs/cart') {
                  console.log(111);
                  _this.userData.changeColor = true;
                }
              }
            });
          }
        }, {
          key: "ionPageWillLeave",
          value: function ionPageWillLeave() {
            this.routerSubscription.unsubscribe();
          }
        }, {
          key: "scrollHandler",
          value: function scrollHandler(event) {
            var _this2 = this;

            this.zone.run(function () {
              _this2.lastY = 0;

              if (event.detail.scrollTop > _this2.lastY) {
                var elem = document.querySelector('ion-tab-bar'); // let elem1 = document.querySelector('ion-fab');
                // elem1.style.setProperty('bottom',"-5.5%");

                elem.style.setProperty('margin-bottom', "-100px");
                elem.style.setProperty('transition', "0.5s"); // elem1.style.setProperty('transition',"0.6s");
              } else {
                var _elem = document.querySelector('ion-tab-bar');

                var elem1 = document.querySelector('ion-fab'); // elem1.style.setProperty('bottom',"2.5%");

                _elem.style.setProperty('margin-bottom', "10px");

                _elem.style.setProperty('transition', "0.5s"); // elem1.style.setProperty('transition',"0.6s");

              }

              _this2.lastY = event.detail.scrollTop;
            });
          }
        }]);

        return CartPage;
      }();

      CartPage.ctorParameters = function () {
        return [{
          type: _angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"]
        }, {
          type: _services_userdata_service__WEBPACK_IMPORTED_MODULE_3__["UserdataService"]
        }, {
          type: _angular_core__WEBPACK_IMPORTED_MODULE_1__["NgZone"]
        }];
      };

      CartPage.propDecorators = {
        contentHandle: [{
          type: _angular_core__WEBPACK_IMPORTED_MODULE_1__["ViewChild"],
          args: ["contentRef"]
        }]
      };
      CartPage = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-cart',
        template: Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(
        /*! raw-loader!./cart.page.html */
        "./node_modules/raw-loader/dist/cjs.js!./src/app/pages/tabs/cart/cart.page.html"))["default"],
        styles: [Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(
        /*! ./cart.page.scss */
        "./src/app/pages/tabs/cart/cart.page.scss"))["default"]]
      })], CartPage);
      /***/
    }
  }]);
})();
//# sourceMappingURL=cart-cart-module-es5.js.map