(function () {
  function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

  function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

  function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

  (window["webpackJsonp"] = window["webpackJsonp"] || []).push([["pages-tabs-home-edit-physical-activity-edit-physical-activity-module"], {
    /***/
    "./node_modules/raw-loader/dist/cjs.js!./src/app/pages/tabs/home/edit-physical-activity/edit-physical-activity.page.html":
    /*!*******************************************************************************************************************************!*\
      !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/pages/tabs/home/edit-physical-activity/edit-physical-activity.page.html ***!
      \*******************************************************************************************************************************/

    /*! exports provided: default */

    /***/
    function node_modulesRawLoaderDistCjsJsSrcAppPagesTabsHomeEditPhysicalActivityEditPhysicalActivityPageHtml(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony default export */


      __webpack_exports__["default"] = "<!-- <ion-header>\n  <ion-toolbar>\n    <ion-title>hypertrophy-strengthening-program-creation</ion-title>\n  </ion-toolbar>\n</ion-header> -->\n\n<ion-content>\n  <ion-grid style=\"padding: 0\">\n\n    <ion-row class=\"arrow_section\">\n      <ion-icon name=\"arrow-back-outline\"></ion-icon>\n    </ion-row>\n    <ion-row>\n      <ion-img src=\"assets/images/payment_slide_3.png\"></ion-img>\n    </ion-row>\n\n\n    <ion-row class=\"img_description\">\n\n      <ion-grid>\n        <div class=\"edit-tag\">\n        <ion-row>\n          <ion-label>\n            PROGRAMME D’ENTRAINEMENT\n          </ion-label>\n  \n        </ion-row>\n        <ion-row>\n  \n          <ion-label>\n            Force\n          </ion-label>\n        </ion-row>\n      </div>\n      </ion-grid>\n    </ion-row>\n\n\n    \n\n    <ion-row class=\"btn_section row-padding\">\n      <ion-label class=\"medium_text_header\">Jours d’entraînement </ion-label>\n    </ion-row>\n\n\n\n\n    <ion-row class=\"ion-margin-top row-padding\">\n      <ion-col size=\"8\" >\n        <ion-slides  [options]=\"btnSlideOptions\" class=\"btnSlides\" [pager]=\"false\"  #theSlides>\n          <ion-slide *ngFor=\"let item of btnArray\">\n            <ion-card [class]=\"item.active ? 'active_btn card_btn' : 'card_btn'\" (click)=\"makeActive(item)\">\n              <ion-label class=\"medium_text_bold\"> {{item.text}} </ion-label>\n            </ion-card>\n          </ion-slide>\n        </ion-slides>\n      </ion-col>\n      <ion-col size=\"4\" class=\"add_btn_col\">\n        <ion-card class=\"card_btn\" (click)=\"addBtn(theSlides)\">\n          <ion-label class=\"medium_text_bold\"> + </ion-label>\n        </ion-card>\n      </ion-col>\n    </ion-row>\n\n    <div class=\"ion-margin-top\" class=\"btn_underline\">\n      <div class=\"profile-viewall\">\n        <ion-button\n          >Jour {{filteredData.text}}\n          <!-- <ion-icon name=\"chevron-forward-outline\"></ion-icon> -->\n        </ion-button>\n      </div>\n    </div>\n\n    <ion-row class=\"ion-margin-top row-padding\">\n      <ion-text class=\"small_text\"> Aucun exercice pour le moment </ion-text>\n    </ion-row>\n\n    <ion-row class=\"ion-margin-top row-padding\">\n      <ion-item class=\"ion-no-padding\" lines=\"none\">\n        <img class=\"jour_img\" src=\"assets/icon/2body.png\" />\n\n        <ion-label>\n          <ion-label class=\"medium_text_bold\">\n            Groupes musculaires sollicités :\n          </ion-label>\n\n          <ion-label class=\"small_text\"> Pectoraux </ion-label>\n\n          <ion-label>\n            <ion-row class=\"ion-no-padding exercise-details-section\">\n              <ion-col size=\"8\" class=\"detail_excercise_col\">\n                <ion-label class=\"small_text\">\n                  Détails des exercices\n                </ion-label>\n              </ion-col>\n\n              <ion-col size=\"4\" class=\"toogle_col_section\">\n                <ion-toggle (ionChange)=\"toggleChange($event)\"></ion-toggle>\n              </ion-col>\n            </ion-row>\n          </ion-label>\n        </ion-label>\n      </ion-item>\n    </ion-row>\n\n    <ion-row class=\"ion-margin-top\" *ngIf=\"!showListItem\">\n      <ion-reorder-group disabled=\"false\"  reorder='true' (ionItemReorder)=\"onItemReorder($event)\">\n        <!-- Default reorder icon, end aligned items -->\n        <ion-item>\n          <div class=\"reorder_list_div_section\"></div>\n\n          <ion-label class=\"reorder_list_title\">\n            <ion-label class=\"medium_text_bold\"> Nom de l’exercice </ion-label>\n            <ion-label class=\"medium_text_bold\">\n              Sous nom de l’exercice\n            </ion-label>\n          </ion-label>\n          <ion-reorder slot=\"end\"></ion-reorder>\n        </ion-item>\n\n        <ion-item>\n          <div class=\"reorder_list_div_section\"></div>\n\n          <ion-label class=\"reorder_list_title\">\n            <ion-label class=\"medium_text_bold\"> Nom de l’exercice </ion-label>\n            <ion-label class=\"medium_text_bold\">\n              Sous nom de l’exercice\n            </ion-label>\n          </ion-label>\n          <ion-reorder slot=\"end\"></ion-reorder>\n        </ion-item>\n\n        <ion-list-header>BISET</ion-list-header>\n        <ion-item>\n          <div class=\"reorder_list_div_section\"></div>\n\n          <ion-label class=\"reorder_list_title\">\n            <ion-label class=\"medium_text_bold\"> Nom de l’exercice </ion-label>\n            <ion-label class=\"medium_text_bold\">\n              Sous nom de l’exercice\n            </ion-label>\n          </ion-label>\n          <ion-reorder slot=\"end\"></ion-reorder>\n        </ion-item>\n\n        <ion-item>\n          <div class=\"reorder_list_div_section\"></div>\n\n          <ion-label class=\"reorder_list_title\">\n            <ion-label class=\"medium_text_bold\"> Nom de l’exercice </ion-label>\n            <ion-label class=\"medium_text_bold\">\n              Sous nom de l’exercice\n            </ion-label>\n          </ion-label>\n          <ion-reorder slot=\"end\"></ion-reorder>\n        </ion-item>\n      </ion-reorder-group>\n    </ion-row>\n\n    <ion-row class=\"ion-margin-top\" *ngIf=\"showListItem\">\n      <ion-row>\n        <ion-row class=\"biset_section_big\">\n          <h6>BISET – ENCHAINEMENT DE 2 EXERCICES</h6>\n        </ion-row>\n\n        <ion-row class=\"biset_section_first_row\">\n          <ion-item\n            class=\"ion-no-padding\"\n            lines=\"none\"\n            class=\"biset_section_ion_item\"\n          >\n            <div class=\"reorder_list_div_section\"></div>\n\n            <ion-label class=\"reorder_list_title\">\n              <ion-label class=\"medium_text_bold\">\n                Nom de l’exercice\n              </ion-label>\n\n              <ion-label class=\"small_text\"> Sous nom de l’exercice </ion-label>\n            </ion-label>\n          </ion-item>\n\n          <ion-row class=\"biset_section_second_row\">\n            <ion-col class=\"ion-no-padding\" size=\"6\">\n              Séries effectives : 4\n            </ion-col>\n\n            <ion-col class=\"ion-no-padding\" size=\"6\" style=\"align-self: center\">\n              <ion-button class=\"biset_section_btn_one\">\n                Ajouter une série\n              </ion-button>\n            </ion-col>\n          </ion-row>\n        </ion-row>\n\n        <ion-row class=\"biset_section_btn_row\">\n          <ion-button fill=\"flat\" style=\"--background: transparent\">\n            + Créer un triset\n          </ion-button>\n        </ion-row>\n      </ion-row>\n    </ion-row>\n  </ion-grid>\n</ion-content>\n\n<ion-footer class=\"ion-no-border footer_section\">\n  <ion-row>\n    <ion-button class=\"next footer_section_btn\" expand=\"full\" shape=\"round\" (click)=\"openClickAddExcercise()\">\n      Ajouter des exercices\n    </ion-button>\n  </ion-row>\n</ion-footer>\n";
      /***/
    },

    /***/
    "./src/app/pages/tabs/home/edit-physical-activity/edit-physical-activity-routing.module.ts":
    /*!*************************************************************************************************!*\
      !*** ./src/app/pages/tabs/home/edit-physical-activity/edit-physical-activity-routing.module.ts ***!
      \*************************************************************************************************/

    /*! exports provided: EditPhysicalActivityPageRoutingModule */

    /***/
    function srcAppPagesTabsHomeEditPhysicalActivityEditPhysicalActivityRoutingModuleTs(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "EditPhysicalActivityPageRoutingModule", function () {
        return EditPhysicalActivityPageRoutingModule;
      });
      /* harmony import */


      var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! tslib */
      "./node_modules/tslib/tslib.es6.js");
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! @angular/core */
      "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
      /* harmony import */


      var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! @angular/router */
      "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");
      /* harmony import */


      var _edit_physical_activity_page__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
      /*! ./edit-physical-activity.page */
      "./src/app/pages/tabs/home/edit-physical-activity/edit-physical-activity.page.ts");

      var routes = [{
        path: '',
        component: _edit_physical_activity_page__WEBPACK_IMPORTED_MODULE_3__["EditPhysicalActivityPage"]
      }];

      var EditPhysicalActivityPageRoutingModule = function EditPhysicalActivityPageRoutingModule() {
        _classCallCheck(this, EditPhysicalActivityPageRoutingModule);
      };

      EditPhysicalActivityPageRoutingModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]]
      })], EditPhysicalActivityPageRoutingModule);
      /***/
    },

    /***/
    "./src/app/pages/tabs/home/edit-physical-activity/edit-physical-activity.module.ts":
    /*!*****************************************************************************************!*\
      !*** ./src/app/pages/tabs/home/edit-physical-activity/edit-physical-activity.module.ts ***!
      \*****************************************************************************************/

    /*! exports provided: EditPhysicalActivityPageModule */

    /***/
    function srcAppPagesTabsHomeEditPhysicalActivityEditPhysicalActivityModuleTs(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "EditPhysicalActivityPageModule", function () {
        return EditPhysicalActivityPageModule;
      });
      /* harmony import */


      var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! tslib */
      "./node_modules/tslib/tslib.es6.js");
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! @angular/core */
      "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
      /* harmony import */


      var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! @angular/common */
      "./node_modules/@angular/common/__ivy_ngcc__/fesm2015/common.js");
      /* harmony import */


      var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
      /*! @angular/forms */
      "./node_modules/@angular/forms/__ivy_ngcc__/fesm2015/forms.js");
      /* harmony import */


      var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
      /*! @ionic/angular */
      "./node_modules/@ionic/angular/__ivy_ngcc__/fesm2015/ionic-angular.js");
      /* harmony import */


      var _edit_physical_activity_routing_module__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(
      /*! ./edit-physical-activity-routing.module */
      "./src/app/pages/tabs/home/edit-physical-activity/edit-physical-activity-routing.module.ts");
      /* harmony import */


      var _edit_physical_activity_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(
      /*! ./edit-physical-activity.page */
      "./src/app/pages/tabs/home/edit-physical-activity/edit-physical-activity.page.ts");

      var EditPhysicalActivityPageModule = function EditPhysicalActivityPageModule() {
        _classCallCheck(this, EditPhysicalActivityPageModule);
      };

      EditPhysicalActivityPageModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"], _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"], _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonicModule"], _edit_physical_activity_routing_module__WEBPACK_IMPORTED_MODULE_5__["EditPhysicalActivityPageRoutingModule"]],
        declarations: [_edit_physical_activity_page__WEBPACK_IMPORTED_MODULE_6__["EditPhysicalActivityPage"]]
      })], EditPhysicalActivityPageModule);
      /***/
    },

    /***/
    "./src/app/pages/tabs/home/edit-physical-activity/edit-physical-activity.page.scss":
    /*!*****************************************************************************************!*\
      !*** ./src/app/pages/tabs/home/edit-physical-activity/edit-physical-activity.page.scss ***!
      \*****************************************************************************************/

    /*! exports provided: default */

    /***/
    function srcAppPagesTabsHomeEditPhysicalActivityEditPhysicalActivityPageScss(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony default export */


      __webpack_exports__["default"] = ".row-width-underline {\n  width: 100%;\n}\n\nion-content {\n  --padding-bottom: 20%;\n}\n\n.slide-image img {\n  height: 127px;\n}\n\n.slide_section {\n  margin-top: 3%;\n}\n\n.select_row_section {\n  margin-top: 10%;\n}\n\n.btn_section {\n  margin-top: 10%;\n}\n\n.card_btn {\n  height: 70px;\n  width: 70px;\n  border: 2px solid #565656;\n  margin: 0;\n  display: flex;\n  justify-content: center;\n}\n\n.card_btn ion-label {\n  align-self: center;\n  font-size: 1.2rem;\n  font-weight: 600;\n  color: #000;\n}\n\n.btnSlides ion-slide {\n  width: 80px;\n  margin-right: 10px;\n}\n\n.profile-viewall {\n  width: 100%;\n  position: relative;\n  margin-top: 15px;\n  margin-bottom: 15px;\n}\n\n.profile-viewall ion-button {\n  --border-radius: 25px;\n  display: table;\n  margin: auto;\n  font-size: 12px;\n}\n\n.profile-viewall:before {\n  content: \"\";\n  position: absolute;\n  background: #c1c1c1;\n  height: 1px;\n  width: 100%;\n  top: 19px;\n  left: 0;\n}\n\n.btn_underline {\n  margin-top: 10%;\n}\n\n.add_btn_col {\n  display: flex;\n  justify-content: center;\n}\n\nion-item {\n  width: 100%;\n}\n\nion-reorder-group {\n  width: 100%;\n}\n\n.jour_img {\n  width: 100px;\n}\n\n.exercise-details-section {\n  height: 35px;\n  margin-top: -8px;\n}\n\n.detail_excercise_col {\n  align-self: center;\n  padding: 0;\n}\n\n.toogle_col_section {\n  display: flex;\n  justify-content: flex-end;\n}\n\n.reorder_list_div_section {\n  width: 50px;\n  background: #000;\n  height: 35px;\n}\n\n.reorder_list_title {\n  padding-left: 10px;\n}\n\n.biset_section_big {\n  width: 100%;\n  justify-content: center;\n  background: #e7e6e6;\n  border-top-left-radius: 7px;\n  border-top-right-radius: 7px;\n  margin-bottom: 2px;\n}\n\n.biset_section_first_row {\n  background: #e7e6e6;\n  width: 100%;\n  padding: 5px;\n}\n\n.biset_section_ion_item {\n  --background: transparent ;\n}\n\n.biset_section_second_row {\n  width: 100%;\n}\n\n.biset_section_btn_one {\n  height: 27px;\n  --border-radius: 15px ;\n}\n\n.biset_section_btn_row {\n  width: 100%;\n  justify-content: center;\n  background: #e7e6e6;\n  margin-top: 5px;\n  border-bottom-left-radius: 7px;\n  border-bottom-right-radius: 7px;\n}\n\n.footer_section {\n  width: 100%;\n  position: fixed;\n  bottom: 10px;\n}\n\n.footer_section_btn {\n  width: 100%;\n  padding-left: 10px;\n  padding-right: 10px;\n  margin-top: 32px;\n  --background: var(--ion-btn-custom-color);\n}\n\n.active_btn {\n  background: var(--ion-btn-custom-color);\n}\n\n.row-padding {\n  padding-left: 2%;\n  padding-right: 2%;\n}\n\n.arrow_section {\n  position: absolute;\n  top: 10px;\n  color: #fff;\n  padding-left: 4%;\n  padding-top: 3%;\n  font-size: 20px;\n}\n\n.edit-tag {\n  position: relative;\n  color: #fff !important;\n  top: -80px;\n  padding-left: 10px;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvcGFnZXMvdGFicy9ob21lL2VkaXQtcGh5c2ljYWwtYWN0aXZpdHkvZWRpdC1waHlzaWNhbC1hY3Rpdml0eS5wYWdlLnNjc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBRUE7RUFDSSxXQUFBO0FBREo7O0FBSUE7RUFHSSxxQkFBQTtBQUhKOztBQU1BO0VBRUksYUFBQTtBQUpKOztBQVFBO0VBQ0ksY0FBQTtBQUxKOztBQVVBO0VBQ0ksZUFBQTtBQVBKOztBQVVBO0VBQ0ksZUFBQTtBQVBKOztBQVVBO0VBQ0ksWUFBQTtFQUNBLFdBQUE7RUFDQSx5QkFBQTtFQUNBLFNBQUE7RUFDQSxhQUFBO0VBQ0EsdUJBQUE7QUFQSjs7QUFTSTtFQUNJLGtCQUFBO0VBQ0EsaUJBQUE7RUFDQSxnQkFBQTtFQUNBLFdBQUE7QUFQUjs7QUFhSTtFQUNJLFdBQUE7RUFDQSxrQkFBQTtBQVZSOztBQWVBO0VBQ0ksV0FBQTtFQUNBLGtCQUFBO0VBQ0EsZ0JBQUE7RUFDQSxtQkFBQTtBQVpKOztBQWVBO0VBQ0kscUJBQUE7RUFDQSxjQUFBO0VBQ0EsWUFBQTtFQUNBLGVBQUE7QUFaSjs7QUFlQTtFQUNJLFdBQUE7RUFDQSxrQkFBQTtFQUNBLG1CQUFBO0VBQ0EsV0FBQTtFQUNBLFdBQUE7RUFDQSxTQUFBO0VBQ0EsT0FBQTtBQVpKOztBQXFCQTtFQUNJLGVBQUE7QUFsQko7O0FBc0JBO0VBQ0ksYUFBQTtFQUNBLHVCQUFBO0FBbkJKOztBQXFCQTtFQUNJLFdBQUE7QUFsQko7O0FBcUJBO0VBQ0ksV0FBQTtBQWxCSjs7QUFxQkE7RUFDSSxZQUFBO0FBbEJKOztBQXFCQTtFQUNJLFlBQUE7RUFDQSxnQkFBQTtBQWxCSjs7QUFzQkE7RUFDSSxrQkFBQTtFQUNBLFVBQUE7QUFuQko7O0FBdUJBO0VBQ0ksYUFBQTtFQUNBLHlCQUFBO0FBcEJKOztBQXdCQTtFQUNJLFdBQUE7RUFDQSxnQkFBQTtFQUNBLFlBQUE7QUFyQko7O0FBeUJBO0VBQ0ksa0JBQUE7QUF0Qko7O0FBeUJBO0VBQ0ksV0FBQTtFQUNBLHVCQUFBO0VBQ0EsbUJBQUE7RUFDQSwyQkFBQTtFQUNBLDRCQUFBO0VBQ0Esa0JBQUE7QUF0Qko7O0FBeUJBO0VBQ0ksbUJBQUE7RUFDQSxXQUFBO0VBQ0EsWUFBQTtBQXRCSjs7QUF5QkE7RUFDSSwwQkFBQTtBQXRCSjs7QUF5QkE7RUFDSSxXQUFBO0FBdEJKOztBQXlCQTtFQUNJLFlBQUE7RUFDQSxzQkFBQTtBQXRCSjs7QUEwQkE7RUFDSSxXQUFBO0VBQ0EsdUJBQUE7RUFDQSxtQkFBQTtFQUNBLGVBQUE7RUFDQSw4QkFBQTtFQUNBLCtCQUFBO0FBdkJKOztBQTBCQTtFQUNJLFdBQUE7RUFDQSxlQUFBO0VBQ0EsWUFBQTtBQXZCSjs7QUEyQkE7RUFFSSxXQUFBO0VBQ0Esa0JBQUE7RUFDQSxtQkFBQTtFQUNBLGdCQUFBO0VBQ0EseUNBQUE7QUF6Qko7O0FBOEJBO0VBQ0ksdUNBQUE7QUEzQko7O0FBK0JBO0VBQ0ksZ0JBQUE7RUFDQSxpQkFBQTtBQTVCSjs7QUErQkE7RUFDSSxrQkFBQTtFQUNBLFNBQUE7RUFDQSxXQUFBO0VBQ0EsZ0JBQUE7RUFDQSxlQUFBO0VBQ0EsZUFBQTtBQTVCSjs7QUE4QkE7RUFDSSxrQkFBQTtFQUNBLHNCQUFBO0VBQ0EsVUFBQTtFQUNBLGtCQUFBO0FBM0JKIiwiZmlsZSI6InNyYy9hcHAvcGFnZXMvdGFicy9ob21lL2VkaXQtcGh5c2ljYWwtYWN0aXZpdHkvZWRpdC1waHlzaWNhbC1hY3Rpdml0eS5wYWdlLnNjc3MiLCJzb3VyY2VzQ29udGVudCI6WyJcblxuLnJvdy13aWR0aC11bmRlcmxpbmUge1xuICAgIHdpZHRoOiAxMDAlO1xufVxuXG5pb24tY29udGVudCB7XG4gICAgLy8gLS1wYWRkaW5nLXRvcDogMTAlO1xuICAgIC8vIC0tcGFkZGluZy1zdGFydDogMyU7XG4gICAgLS1wYWRkaW5nLWJvdHRvbTogMjAlO1xufVxuXG4uc2xpZGUtaW1hZ2UgaW1nIHtcblxuICAgIGhlaWdodDogMTI3cHg7XG5cbn1cblxuLnNsaWRlX3NlY3Rpb24ge1xuICAgIG1hcmdpbi10b3A6IDMlO1xufVxuXG5cblxuLnNlbGVjdF9yb3dfc2VjdGlvbiB7XG4gICAgbWFyZ2luLXRvcDogMTAlO1xufVxuXG4uYnRuX3NlY3Rpb24ge1xuICAgIG1hcmdpbi10b3A6IDEwJTtcbn1cblxuLmNhcmRfYnRuIHtcbiAgICBoZWlnaHQ6IDcwcHg7XG4gICAgd2lkdGg6IDcwcHg7XG4gICAgYm9yZGVyOiAycHggc29saWQgIzU2NTY1NjtcbiAgICBtYXJnaW46IDA7XG4gICAgZGlzcGxheTogZmxleDtcbiAgICBqdXN0aWZ5LWNvbnRlbnQ6IGNlbnRlcjtcblxuICAgIGlvbi1sYWJlbCB7XG4gICAgICAgIGFsaWduLXNlbGY6IGNlbnRlcjtcbiAgICAgICAgZm9udC1zaXplOiAxLjJyZW07XG4gICAgICAgIGZvbnQtd2VpZ2h0OiA2MDA7XG4gICAgICAgIGNvbG9yOiAjMDAwO1xuICAgIH1cbn1cblxuXG4uYnRuU2xpZGVzIHtcbiAgICBpb24tc2xpZGUge1xuICAgICAgICB3aWR0aDogODBweDtcbiAgICAgICAgbWFyZ2luLXJpZ2h0OiAxMHB4O1xuICAgIH1cbn1cblxuXG4ucHJvZmlsZS12aWV3YWxsIHtcbiAgICB3aWR0aDogMTAwJTtcbiAgICBwb3NpdGlvbjogcmVsYXRpdmU7XG4gICAgbWFyZ2luLXRvcDogMTVweDtcbiAgICBtYXJnaW4tYm90dG9tOiAxNXB4O1xufVxuXG4ucHJvZmlsZS12aWV3YWxsIGlvbi1idXR0b24ge1xuICAgIC0tYm9yZGVyLXJhZGl1czogMjVweDtcbiAgICBkaXNwbGF5OiB0YWJsZTtcbiAgICBtYXJnaW46IGF1dG87XG4gICAgZm9udC1zaXplOiAxMnB4O1xufVxuXG4ucHJvZmlsZS12aWV3YWxsOmJlZm9yZSB7XG4gICAgY29udGVudDogXCJcIjtcbiAgICBwb3NpdGlvbjogYWJzb2x1dGU7XG4gICAgYmFja2dyb3VuZDogI2MxYzFjMTtcbiAgICBoZWlnaHQ6IDFweDtcbiAgICB3aWR0aDogMTAwJTtcbiAgICB0b3A6IDE5cHg7XG4gICAgbGVmdDogMDtcbn1cblxuXG5pb24tcm93IHtcbiAgICAvLyBwYWRkaW5nLWxlZnQ6IDUlO1xufVxuXG5cbi5idG5fdW5kZXJsaW5lIHtcbiAgICBtYXJnaW4tdG9wOiAxMCU7XG59XG5cblxuLmFkZF9idG5fY29sIHtcbiAgICBkaXNwbGF5OiBmbGV4O1xuICAgIGp1c3RpZnktY29udGVudDogY2VudGVyO1xufVxuaW9uLWl0ZW0ge1xuICAgIHdpZHRoOiAxMDAlO1xufVxuXG5pb24tcmVvcmRlci1ncm91cCB7XG4gICAgd2lkdGg6IDEwMCU7XG59XG5cbi5qb3VyX2ltZyB7XG4gICAgd2lkdGg6IDEwMHB4O1xufVxuXG4uZXhlcmNpc2UtZGV0YWlscy1zZWN0aW9uIHtcbiAgICBoZWlnaHQ6IDM1cHg7XG4gICAgbWFyZ2luLXRvcDogLThweFxufVxuXG5cbi5kZXRhaWxfZXhjZXJjaXNlX2NvbCB7XG4gICAgYWxpZ24tc2VsZjogY2VudGVyO1xuICAgIHBhZGRpbmc6IDBcbn1cblxuXG4udG9vZ2xlX2NvbF9zZWN0aW9uIHtcbiAgICBkaXNwbGF5OiBmbGV4O1xuICAgIGp1c3RpZnktY29udGVudDogZmxleC1lbmRcbn1cblxuXG4ucmVvcmRlcl9saXN0X2Rpdl9zZWN0aW9uIHtcbiAgICB3aWR0aDogNTBweDtcbiAgICBiYWNrZ3JvdW5kOiAjMDAwO1xuICAgIGhlaWdodDogMzVweFxufVxuXG5cbi5yZW9yZGVyX2xpc3RfdGl0bGUge1xuICAgIHBhZGRpbmctbGVmdDogMTBweFxufVxuXG4uYmlzZXRfc2VjdGlvbl9iaWcge1xuICAgIHdpZHRoOiAxMDAlO1xuICAgIGp1c3RpZnktY29udGVudDogY2VudGVyO1xuICAgIGJhY2tncm91bmQ6IHJnYigyMzEsIDIzMCwgMjMwKTtcbiAgICBib3JkZXItdG9wLWxlZnQtcmFkaXVzOiA3cHg7XG4gICAgYm9yZGVyLXRvcC1yaWdodC1yYWRpdXM6IDdweDtcbiAgICBtYXJnaW4tYm90dG9tOiAycHg7XG59XG5cbi5iaXNldF9zZWN0aW9uX2ZpcnN0X3JvdyB7XG4gICAgYmFja2dyb3VuZDogcmdiKDIzMSwgMjMwLCAyMzApO1xuICAgIHdpZHRoOiAxMDAlO1xuICAgIHBhZGRpbmc6IDVweFxufVxuXG4uYmlzZXRfc2VjdGlvbl9pb25faXRlbSB7XG4gICAgLS1iYWNrZ3JvdW5kOiB0cmFuc3BhcmVudFxufVxuXG4uYmlzZXRfc2VjdGlvbl9zZWNvbmRfcm93IHtcbiAgICB3aWR0aDogMTAwJVxufVxuXG4uYmlzZXRfc2VjdGlvbl9idG5fb25lIHtcbiAgICBoZWlnaHQ6IDI3cHg7XG4gICAgLS1ib3JkZXItcmFkaXVzOiAxNXB4XG59XG5cblxuLmJpc2V0X3NlY3Rpb25fYnRuX3JvdyB7XG4gICAgd2lkdGg6IDEwMCU7XG4gICAganVzdGlmeS1jb250ZW50OiBjZW50ZXI7XG4gICAgYmFja2dyb3VuZDogcmdiKDIzMSwgMjMwLCAyMzApO1xuICAgIG1hcmdpbi10b3A6IDVweDtcbiAgICBib3JkZXItYm90dG9tLWxlZnQtcmFkaXVzOiA3cHg7XG4gICAgYm9yZGVyLWJvdHRvbS1yaWdodC1yYWRpdXM6IDdweDtcbn1cblxuLmZvb3Rlcl9zZWN0aW9uIHtcbiAgICB3aWR0aDogMTAwJTtcbiAgICBwb3NpdGlvbjogZml4ZWQ7XG4gICAgYm90dG9tOiAxMHB4XG59XG5cblxuLmZvb3Rlcl9zZWN0aW9uX2J0biB7XG5cbiAgICB3aWR0aDogMTAwJTtcbiAgICBwYWRkaW5nLWxlZnQ6IDEwcHg7XG4gICAgcGFkZGluZy1yaWdodDogMTBweDtcbiAgICBtYXJnaW4tdG9wOiAzMnB4O1xuICAgIC0tYmFja2dyb3VuZDogdmFyKC0taW9uLWJ0bi1jdXN0b20tY29sb3IpO1xufVxuXG5cblxuLmFjdGl2ZV9idG4ge1xuICAgIGJhY2tncm91bmQ6IHZhcigtLWlvbi1idG4tY3VzdG9tLWNvbG9yKTtcbn1cblxuXG4ucm93LXBhZGRpbmcge1xuICAgIHBhZGRpbmctbGVmdDogMiU7XG4gICAgcGFkZGluZy1yaWdodDogMiU7XG59XG5cbi5hcnJvd19zZWN0aW9uIHtcbiAgICBwb3NpdGlvbjogYWJzb2x1dGU7XG4gICAgdG9wOiAxMHB4O1xuICAgIGNvbG9yOiAjZmZmO1xuICAgIHBhZGRpbmctbGVmdDogNCU7XG4gICAgcGFkZGluZy10b3A6IDMlO1xuICAgIGZvbnQtc2l6ZTogMjBweDtcbn1cbi5lZGl0LXRhZyB7XG4gICAgcG9zaXRpb246IHJlbGF0aXZlO1xuICAgIGNvbG9yOiAjZmZmICFpbXBvcnRhbnQ7XG4gICAgdG9wOiAtODBweDtcbiAgICBwYWRkaW5nLWxlZnQ6IDEwcHg7XG59Il19 */";
      /***/
    },

    /***/
    "./src/app/pages/tabs/home/edit-physical-activity/edit-physical-activity.page.ts":
    /*!***************************************************************************************!*\
      !*** ./src/app/pages/tabs/home/edit-physical-activity/edit-physical-activity.page.ts ***!
      \***************************************************************************************/

    /*! exports provided: EditPhysicalActivityPage */

    /***/
    function srcAppPagesTabsHomeEditPhysicalActivityEditPhysicalActivityPageTs(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "EditPhysicalActivityPage", function () {
        return EditPhysicalActivityPage;
      });
      /* harmony import */


      var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! tslib */
      "./node_modules/tslib/tslib.es6.js");
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! @angular/core */
      "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
      /* harmony import */


      var _components_modals_click_add_excercise_modal_click_add_excercise_modal_component__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! @components/modals/click-add-excercise-modal/click-add-excercise-modal.component */
      "./src/app/components/modals/click-add-excercise-modal/click-add-excercise-modal.component.ts");
      /* harmony import */


      var _ionic_angular__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
      /*! @ionic/angular */
      "./node_modules/@ionic/angular/__ivy_ngcc__/fesm2015/ionic-angular.js");

      var EditPhysicalActivityPage = /*#__PURE__*/function () {
        function EditPhysicalActivityPage(modalCntrl) {
          _classCallCheck(this, EditPhysicalActivityPage);

          this.modalCntrl = modalCntrl;
          this.showListItem = false;
          this.slideOptions = {
            // centeredSlides: true,
            slidesPerView: 2.5,
            spaceBetween: 10
          };
          this.btnSlideOptions = {
            // centeredSlides: true,
            slidesPerView: 3,
            spaceBetween: 5
          };
          this.objectif = "Hypertrophie";
          this.format_dentrainement = "SPLIT";
          this.training_days = "3";
          this.progression_method = "Aucune";
          this.start_the_program = "Aujourd’hui";
          this.btnArray = [{
            text: 1,
            active: true
          }, {
            text: 2,
            active: false
          }, {
            text: 3,
            active: false
          }];
          this.filteredData = {};
        }

        _createClass(EditPhysicalActivityPage, [{
          key: "ngOnInit",
          value: function ngOnInit() {
            var _this = this;

            this.btnArray.map(function (doc) {
              if (doc.active) {
                _this.filteredData = doc;
              }
            });
          }
        }, {
          key: "toggleChange",
          value: function toggleChange(event) {
            console.log(event);
            this.showListItem = event.detail.checked;
          }
        }, {
          key: "addBtn",
          value: function addBtn(slides) {
            var _this2 = this;

            if (!(this.btnArray.length >= 7)) {
              var text = 2;
              var arr = this.btnArray[this.btnArray.length - 1];
              var obj = {
                text: arr.text + 1,
                active: false
              };
              this.btnArray.push(obj);
              slides.update();
              slides.slideTo(this.btnArray.length + 1);
              setTimeout(function () {
                slides.slideTo(_this2.btnArray.length + 2);
              });
            }
          }
        }, {
          key: "onItemReorder",
          value: function onItemReorder(_ref) {
            var detail = _ref.detail;
            detail.complete(true);
          }
        }, {
          key: "openClickAddExcercise",
          value: function openClickAddExcercise() {
            return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, /*#__PURE__*/regeneratorRuntime.mark(function _callee() {
              var modal;
              return regeneratorRuntime.wrap(function _callee$(_context) {
                while (1) {
                  switch (_context.prev = _context.next) {
                    case 0:
                      _context.next = 2;
                      return this.modalCntrl.create({
                        component: _components_modals_click_add_excercise_modal_click_add_excercise_modal_component__WEBPACK_IMPORTED_MODULE_2__["ClickAddExcerciseModalComponent"],
                        cssClass: 'small_select_type_modal_after'
                      });

                    case 2:
                      modal = _context.sent;
                      return _context.abrupt("return", modal.present());

                    case 4:
                    case "end":
                      return _context.stop();
                  }
                }
              }, _callee, this);
            }));
          }
        }, {
          key: "makeActive",
          value: function makeActive(item) {
            var _this3 = this;

            console.log(item);
            item.active = true;
            var filteredData = [];
            this.btnArray.forEach(function (doc) {
              if (item.text != doc.text) {
                doc.active = false;
              }

              filteredData.push(doc);
            });
            this.btnArray = filteredData;
            this.btnArray.map(function (doc) {
              if (doc.active) {
                _this3.filteredData = doc;
              }
            });
          }
        }]);

        return EditPhysicalActivityPage;
      }();

      EditPhysicalActivityPage.ctorParameters = function () {
        return [{
          type: _ionic_angular__WEBPACK_IMPORTED_MODULE_3__["ModalController"]
        }];
      };

      EditPhysicalActivityPage = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-edit-physical-activity',
        template: Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(
        /*! raw-loader!./edit-physical-activity.page.html */
        "./node_modules/raw-loader/dist/cjs.js!./src/app/pages/tabs/home/edit-physical-activity/edit-physical-activity.page.html"))["default"],
        styles: [Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(
        /*! ./edit-physical-activity.page.scss */
        "./src/app/pages/tabs/home/edit-physical-activity/edit-physical-activity.page.scss"))["default"]]
      })], EditPhysicalActivityPage);
      /***/
    }
  }]);
})();
//# sourceMappingURL=pages-tabs-home-edit-physical-activity-edit-physical-activity-module-es5.js.map