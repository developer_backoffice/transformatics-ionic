(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["pages-tabs-menu-chat-chat-module"],{

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/pages/tabs/menu/chat/chat.page.html":
/*!*******************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/pages/tabs/menu/chat/chat.page.html ***!
  \*******************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<ion-header class=\"chat-profile\">\n\n    <ion-toolbar>\n        <ion-item>\n            <ion-buttons class=\"ion-no-margin\" slot=\"start\">\n                <ion-back-button></ion-back-button>\n            </ion-buttons>\n            <ion-avatar slot=\"start\">\n                <img src=\"assets/icon/man.png\">\n            </ion-avatar>\n            <ion-label>\n                <h3>JEAN MICHEL ANDRAS</h3>\n                <p>Connecté</p>\n            </ion-label>\n        </ion-item>\n    </ion-toolbar>\n</ion-header>\n\n<ion-content>\n    <div class=\"header-button-box\">\n        <div class=\"header-button-box-button-right\">\n            <div class=\"header-button-box-button \">\n                <img src=\"assets/icon/attach.png\">\n            </div>\n            <div class=\"header-button-box-button\">\n                <img src=\"assets/icon/video.png\">\n            </div>\n            <div class=\"header-button-box-button\">\n                <img src=\"assets/icon/call.png\">\n            </div>\n        </div>\n    </div>\n    <div>\n        <ion-list class=\"chat-screen-box\">\n            <div class=\"ion-text-center startchartdate\">\n                <p>Thursday,January 22,2015</p>\n            </div>\n            <ion-item class=\"chat-input\">\n                <ion-text slot=\"end\" class=\"f10\">4:31 PM</ion-text>\n                <ion-text class=\"chat-text-custemer f12\">\n                    <p>Reference site about Lorem Ipsum, giving information on its origins, as well as a random Lipsum generator.</p>\n                </ion-text>\n            </ion-item>\n            <ion-item class=\"chat-input\">\n                <ion-text slot=\"end\" class=\"chat-text-user f12\">\n                    <p>Reference site about Lorem Ipsum, giving information on its origins, as well as a random Lipsum generator.</p>\n                </ion-text>\n                <ion-text slot=\"start\" class=\"f10\">4:31 PM</ion-text>\n            </ion-item>\n            <div class=\"ion-text-center seendate\">\n                <span>Thursday,January 22,2015</span>\n            </div>\n            <ion-item class=\"chat-input\">\n                <ion-text slot=\"end\" class=\"f10\">4:31 PM</ion-text>\n                <ion-text class=\"chat-text-custemer f12\">\n                    <p>Reference site about Lorem Ipsum, giving information on its origins, as well as a random Lipsum generator.</p>\n                </ion-text>\n                <!-- <ion-avatar slot=\"start\">\n                    <img src=\"assets/icon/man.png\">\n                </ion-avatar> -->\n            </ion-item>\n            <ion-item class=\"chat-input\">\n                <ion-text slot=\"end\" class=\"chat-text-user f12\">\n                    <p>Reference site about Lorem Ipsum, giving information on its origins, as well as a random Lipsum generator.</p>\n                </ion-text>\n                <ion-text slot=\"start\" class=\"f10\">4:31 PM</ion-text>\n            </ion-item>\n            <ion-item class=\"chat-input\">\n                <ion-text slot=\"end\" class=\"f10\">4:31 PM</ion-text>\n                <ion-text class=\"chat-text-custemer f12\">\n                    <p>Reference site about Lorem Ipsum, giving information on its origins, as well as a random Lipsum generator.</p>\n                </ion-text>\n            </ion-item>\n            <div class=\"ion-text-center seendate\">\n                <span>Thursday,January 22,2015</span>\n            </div>\n            <ion-item class=\"chat-input\">\n                <ion-text slot=\"end\" class=\"chat-text-user f12\">\n                    <p>Reference site about Lorem Ipsum, giving information on its origins, as well as a random Lipsum generator.</p>\n                </ion-text>\n                <ion-text slot=\"start\" class=\"f10\">4:31 PM</ion-text>\n            </ion-item>\n        </ion-list>\n    </div>\n</ion-content>\n<ion-footer>\n    <ion-toolbar>\n        <ion-grid>\n            <ion-row>\n                <ion-col>\n                    <ion-item class=\"chat-input\">\n                        <ion-icon slot=\"start\" name=\"image-outline\"></ion-icon>\n                        <ion-input placeholder=\"Ecrivez votre message ici\"></ion-input>\n                        <ion-icon slot=\"end\" name=\"send-outline\"></ion-icon>\n                    </ion-item>\n                </ion-col>\n            </ion-row>\n        </ion-grid>\n    </ion-toolbar>\n</ion-footer>");

/***/ }),

/***/ "./src/app/pages/tabs/menu/chat/chat-routing.module.ts":
/*!*************************************************************!*\
  !*** ./src/app/pages/tabs/menu/chat/chat-routing.module.ts ***!
  \*************************************************************/
/*! exports provided: ChatPageRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ChatPageRoutingModule", function() { return ChatPageRoutingModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");
/* harmony import */ var _chat_page__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./chat.page */ "./src/app/pages/tabs/menu/chat/chat.page.ts");




const routes = [
    {
        path: '',
        component: _chat_page__WEBPACK_IMPORTED_MODULE_3__["ChatPage"]
    }
];
let ChatPageRoutingModule = class ChatPageRoutingModule {
};
ChatPageRoutingModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]],
    })
], ChatPageRoutingModule);



/***/ }),

/***/ "./src/app/pages/tabs/menu/chat/chat.module.ts":
/*!*****************************************************!*\
  !*** ./src/app/pages/tabs/menu/chat/chat.module.ts ***!
  \*****************************************************/
/*! exports provided: ChatPageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ChatPageModule", function() { return ChatPageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/__ivy_ngcc__/fesm2015/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/__ivy_ngcc__/fesm2015/forms.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/__ivy_ngcc__/fesm2015/ionic-angular.js");
/* harmony import */ var _chat_routing_module__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./chat-routing.module */ "./src/app/pages/tabs/menu/chat/chat-routing.module.ts");
/* harmony import */ var _chat_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./chat.page */ "./src/app/pages/tabs/menu/chat/chat.page.ts");







let ChatPageModule = class ChatPageModule {
};
ChatPageModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [
            _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonicModule"],
            _chat_routing_module__WEBPACK_IMPORTED_MODULE_5__["ChatPageRoutingModule"]
        ],
        declarations: [_chat_page__WEBPACK_IMPORTED_MODULE_6__["ChatPage"]]
    })
], ChatPageModule);



/***/ }),

/***/ "./src/app/pages/tabs/menu/chat/chat.page.scss":
/*!*****************************************************!*\
  !*** ./src/app/pages/tabs/menu/chat/chat.page.scss ***!
  \*****************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = (".header-button-box {\n  display: flex;\n  position: fixed;\n  z-index: 999;\n  margin-top: -20px;\n  color: #fff;\n  width: 100%;\n}\n\n.header-button-box-button-right {\n  right: 10%;\n  position: absolute;\n}\n\n.header-button-box-button-right .header-button-box-button {\n  display: inline-block;\n  margin-right: 15px;\n  height: 35px;\n  width: 35px;\n  background: #fff;\n  border-radius: 40px;\n  padding: 5px 5px 0px 6px;\n  border: 2px solid #ffd1b3;\n}\n\n.chat-profile ion-item {\n  --inner-border-width: 0 0 0px 0;\n}\n\n.chat-input {\n  --border-width: 0 0 0px 0;\n}\n\n.chat-screen-box ion-item {\n  --inner-border-width: 0 0 0px 0;\n  margin-bottom: 15px;\n}\n\n.chat-screen-box ion-label {\n  white-space: normal;\n}\n\n.chat-text-user {\n  background: #e8f8ff;\n  padding: 0px 15px;\n  border-radius: 5px;\n}\n\n.chat-text-custemer {\n  background: #adf;\n  padding: 0px 15px;\n  border-radius: 5px;\n}\n\n.seendate {\n  position: relative;\n  margin-bottom: 15px;\n}\n\n.seendate:before {\n  content: \"\";\n  position: absolute;\n  height: 1px;\n  background: #c1c1c1;\n  left: 0;\n  width: 100%;\n  top: 14px;\n  z-index: -1;\n}\n\n.seendate span {\n  background: #ffffff;\n  z-index: 2;\n  padding: 0px 15px;\n  font-size: 12px;\n}\n\n.startchartdate {\n  font-size: 12px;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvcGFnZXMvdGFicy9tZW51L2NoYXQvY2hhdC5wYWdlLnNjc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUE7RUFDSSxhQUFBO0VBQ0EsZUFBQTtFQUNBLFlBQUE7RUFDQSxpQkFBQTtFQUNBLFdBQUE7RUFDQSxXQUFBO0FBQ0o7O0FBRUE7RUFDSSxVQUFBO0VBQ0Esa0JBQUE7QUFDSjs7QUFFQTtFQUNJLHFCQUFBO0VBQ0Esa0JBQUE7RUFDQSxZQUFBO0VBQ0EsV0FBQTtFQUNBLGdCQUFBO0VBQ0EsbUJBQUE7RUFDQSx3QkFBQTtFQUNBLHlCQUFBO0FBQ0o7O0FBRUE7RUFDSSwrQkFBQTtBQUNKOztBQUVBO0VBQ0kseUJBQUE7QUFDSjs7QUFFQTtFQUNJLCtCQUFBO0VBQ0EsbUJBQUE7QUFDSjs7QUFFQTtFQUNJLG1CQUFBO0FBQ0o7O0FBRUE7RUFDSSxtQkFBQTtFQUNBLGlCQUFBO0VBQ0Esa0JBQUE7QUFDSjs7QUFFQTtFQUNJLGdCQUFBO0VBQ0EsaUJBQUE7RUFDQSxrQkFBQTtBQUNKOztBQUVBO0VBQ0ksa0JBQUE7RUFDQSxtQkFBQTtBQUNKOztBQUVBO0VBQ0ksV0FBQTtFQUNBLGtCQUFBO0VBQ0EsV0FBQTtFQUNBLG1CQUFBO0VBQ0EsT0FBQTtFQUNBLFdBQUE7RUFDQSxTQUFBO0VBQ0EsV0FBQTtBQUNKOztBQUVBO0VBQ0ksbUJBQUE7RUFDQSxVQUFBO0VBQ0EsaUJBQUE7RUFDQSxlQUFBO0FBQ0o7O0FBRUE7RUFDSSxlQUFBO0FBQ0oiLCJmaWxlIjoic3JjL2FwcC9wYWdlcy90YWJzL21lbnUvY2hhdC9jaGF0LnBhZ2Uuc2NzcyIsInNvdXJjZXNDb250ZW50IjpbIi5oZWFkZXItYnV0dG9uLWJveCB7XG4gICAgZGlzcGxheTogZmxleDtcbiAgICBwb3NpdGlvbjogZml4ZWQ7XG4gICAgei1pbmRleDogOTk5O1xuICAgIG1hcmdpbi10b3A6IC0yMHB4O1xuICAgIGNvbG9yOiAjZmZmO1xuICAgIHdpZHRoOiAxMDAlO1xufVxuXG4uaGVhZGVyLWJ1dHRvbi1ib3gtYnV0dG9uLXJpZ2h0IHtcbiAgICByaWdodDogMTAlO1xuICAgIHBvc2l0aW9uOiBhYnNvbHV0ZTtcbn1cblxuLmhlYWRlci1idXR0b24tYm94LWJ1dHRvbi1yaWdodCAuaGVhZGVyLWJ1dHRvbi1ib3gtYnV0dG9uIHtcbiAgICBkaXNwbGF5OiBpbmxpbmUtYmxvY2s7XG4gICAgbWFyZ2luLXJpZ2h0OiAxNXB4O1xuICAgIGhlaWdodDogMzVweDtcbiAgICB3aWR0aDogMzVweDtcbiAgICBiYWNrZ3JvdW5kOiAjZmZmO1xuICAgIGJvcmRlci1yYWRpdXM6IDQwcHg7XG4gICAgcGFkZGluZzogNXB4IDVweCAwcHggNnB4O1xuICAgIGJvcmRlcjogMnB4IHNvbGlkICNmZmQxYjM7XG59XG5cbi5jaGF0LXByb2ZpbGUgaW9uLWl0ZW0ge1xuICAgIC0taW5uZXItYm9yZGVyLXdpZHRoOiAwIDAgMHB4IDA7XG59XG5cbi5jaGF0LWlucHV0IHtcbiAgICAtLWJvcmRlci13aWR0aDogMCAwIDBweCAwO1xufVxuXG4uY2hhdC1zY3JlZW4tYm94IGlvbi1pdGVtIHtcbiAgICAtLWlubmVyLWJvcmRlci13aWR0aDogMCAwIDBweCAwO1xuICAgIG1hcmdpbi1ib3R0b206IDE1cHg7XG59XG5cbi5jaGF0LXNjcmVlbi1ib3ggaW9uLWxhYmVsIHtcbiAgICB3aGl0ZS1zcGFjZTogbm9ybWFsO1xufVxuXG4uY2hhdC10ZXh0LXVzZXIge1xuICAgIGJhY2tncm91bmQ6ICNlOGY4ZmY7XG4gICAgcGFkZGluZzogMHB4IDE1cHg7XG4gICAgYm9yZGVyLXJhZGl1czogNXB4O1xufVxuXG4uY2hhdC10ZXh0LWN1c3RlbWVyIHtcbiAgICBiYWNrZ3JvdW5kOiAjYWRmO1xuICAgIHBhZGRpbmc6IDBweCAxNXB4O1xuICAgIGJvcmRlci1yYWRpdXM6IDVweDtcbn1cblxuLnNlZW5kYXRlIHtcbiAgICBwb3NpdGlvbjogcmVsYXRpdmU7XG4gICAgbWFyZ2luLWJvdHRvbTogMTVweDtcbn1cblxuLnNlZW5kYXRlOmJlZm9yZSB7XG4gICAgY29udGVudDogXCJcIjtcbiAgICBwb3NpdGlvbjogYWJzb2x1dGU7XG4gICAgaGVpZ2h0OiAxcHg7XG4gICAgYmFja2dyb3VuZDogI2MxYzFjMTtcbiAgICBsZWZ0OiAwO1xuICAgIHdpZHRoOiAxMDAlO1xuICAgIHRvcDogMTRweDtcbiAgICB6LWluZGV4OiAtMTtcbn1cblxuLnNlZW5kYXRlIHNwYW4ge1xuICAgIGJhY2tncm91bmQ6ICNmZmZmZmY7XG4gICAgei1pbmRleDogMjtcbiAgICBwYWRkaW5nOiAwcHggMTVweDtcbiAgICBmb250LXNpemU6IDEycHg7XG59XG5cbi5zdGFydGNoYXJ0ZGF0ZSB7XG4gICAgZm9udC1zaXplOiAxMnB4O1xufSJdfQ== */");

/***/ }),

/***/ "./src/app/pages/tabs/menu/chat/chat.page.ts":
/*!***************************************************!*\
  !*** ./src/app/pages/tabs/menu/chat/chat.page.ts ***!
  \***************************************************/
/*! exports provided: ChatPage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ChatPage", function() { return ChatPage; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");


let ChatPage = class ChatPage {
    constructor() { }
    ngOnInit() {
    }
};
ChatPage.ctorParameters = () => [];
ChatPage = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-chat',
        template: Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(/*! raw-loader!./chat.page.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/pages/tabs/menu/chat/chat.page.html")).default,
        styles: [Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(/*! ./chat.page.scss */ "./src/app/pages/tabs/menu/chat/chat.page.scss")).default]
    })
], ChatPage);



/***/ })

}]);
//# sourceMappingURL=pages-tabs-menu-chat-chat-module-es2015.js.map