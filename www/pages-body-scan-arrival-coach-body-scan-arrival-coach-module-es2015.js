(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["pages-body-scan-arrival-coach-body-scan-arrival-coach-module"],{

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/pages/body-scan-arrival-coach/body-scan-arrival-coach.page.html":
/*!***********************************************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/pages/body-scan-arrival-coach/body-scan-arrival-coach.page.html ***!
  \***********************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<ion-content>\n\n  <div class=\"main_container\">\n    <div class=\"description_section\">\n      <div class=\"header\">\n        <p>\n          Bienvenue dans Transformatics \n        </p>\n      </div>\n  \n      <div class=\"body\">\n        <p class=\"p1\">\n          Il s’agit de votre première connexion en tant que coach. Afin de développer votre Fiche de Coach, qui sera visible par vos futurs clients, et pour que Transformatics puisse vous authentifier dans sa base de données de coachs sportifs. Ces informations resteront strictement confidentielles et conservées dans l’application. Seules certaines informations seront visibles par vos clients potentiels. \n\n        </p>\n        <!-- <p class=\"p2\">\n          Le <strong>Bodyscan</strong> dure environ 7 minutes. Cliquez sur le bouton ci-dessous pour commencer. \n        </p> -->\n        <p class=\"p3\">\n          Note : il vous faut impérativement renseigner la Fiche de Coach dans son intégralité pour accéder à votre espace personnel et commencer à coacher.  \n\n        </p>\n      </div>\n    </div>\n\n    <div class=\"footer\">\n      <ion-button class=\"commencer\" routerLink=\"/select-plan-coach\">\n        Commencer\n      </ion-button>\n    </div>\n  </div>\n\n</ion-content>\n");

/***/ }),

/***/ "./src/app/pages/body-scan-arrival-coach/body-scan-arrival-coach-routing.module.ts":
/*!*****************************************************************************************!*\
  !*** ./src/app/pages/body-scan-arrival-coach/body-scan-arrival-coach-routing.module.ts ***!
  \*****************************************************************************************/
/*! exports provided: BodyScanArrivalCoachPageRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "BodyScanArrivalCoachPageRoutingModule", function() { return BodyScanArrivalCoachPageRoutingModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");
/* harmony import */ var _body_scan_arrival_coach_page__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./body-scan-arrival-coach.page */ "./src/app/pages/body-scan-arrival-coach/body-scan-arrival-coach.page.ts");




const routes = [
    {
        path: '',
        component: _body_scan_arrival_coach_page__WEBPACK_IMPORTED_MODULE_3__["BodyScanArrivalCoachPage"]
    }
];
let BodyScanArrivalCoachPageRoutingModule = class BodyScanArrivalCoachPageRoutingModule {
};
BodyScanArrivalCoachPageRoutingModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]],
    })
], BodyScanArrivalCoachPageRoutingModule);



/***/ }),

/***/ "./src/app/pages/body-scan-arrival-coach/body-scan-arrival-coach.module.ts":
/*!*********************************************************************************!*\
  !*** ./src/app/pages/body-scan-arrival-coach/body-scan-arrival-coach.module.ts ***!
  \*********************************************************************************/
/*! exports provided: BodyScanArrivalCoachPageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "BodyScanArrivalCoachPageModule", function() { return BodyScanArrivalCoachPageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/__ivy_ngcc__/fesm2015/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/__ivy_ngcc__/fesm2015/forms.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/__ivy_ngcc__/fesm2015/ionic-angular.js");
/* harmony import */ var _body_scan_arrival_coach_routing_module__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./body-scan-arrival-coach-routing.module */ "./src/app/pages/body-scan-arrival-coach/body-scan-arrival-coach-routing.module.ts");
/* harmony import */ var _body_scan_arrival_coach_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./body-scan-arrival-coach.page */ "./src/app/pages/body-scan-arrival-coach/body-scan-arrival-coach.page.ts");







let BodyScanArrivalCoachPageModule = class BodyScanArrivalCoachPageModule {
};
BodyScanArrivalCoachPageModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [
            _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonicModule"],
            _body_scan_arrival_coach_routing_module__WEBPACK_IMPORTED_MODULE_5__["BodyScanArrivalCoachPageRoutingModule"]
        ],
        declarations: [_body_scan_arrival_coach_page__WEBPACK_IMPORTED_MODULE_6__["BodyScanArrivalCoachPage"]]
    })
], BodyScanArrivalCoachPageModule);



/***/ }),

/***/ "./src/app/pages/body-scan-arrival-coach/body-scan-arrival-coach.page.scss":
/*!*********************************************************************************!*\
  !*** ./src/app/pages/body-scan-arrival-coach/body-scan-arrival-coach.page.scss ***!
  \*********************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("ion-content {\n  --background: url('landing_page_background_1.png')0 18%/100% no-repeat;\n  background-size: cover;\n  --overflow: hidden;\n}\n\n.body {\n  color: #fff;\n}\n\n.body .p1,\n.body .p2 {\n  font-family: Oswald, serif;\n  text-align: justify;\n  font-size: 0.8rem;\n  /* letter-spacing: 1px; */\n  margin-top: 5%;\n  line-height: 1rem;\n}\n\n.body .p3 {\n  font-family: Oswald, serif;\n  text-align: justify;\n  font-style: italic;\n  margin-top: 5%;\n  line-height: 0.9rem;\n  font-size: 0.7rem;\n}\n\n.main_container {\n  height: 100%;\n  display: flex;\n  flex-direction: column;\n  justify-content: space-between;\n  padding-bottom: 10%;\n  background-color: rgba(0, 0, 0, 0.7);\n  padding-left: 5%;\n  padding-right: 5%;\n}\n\n.header p {\n  color: var(--ion-color-primary);\n  font-family: Oswald, serif;\n  font-size: 18px;\n  text-align: left;\n}\n\n.footer ion-button {\n  width: 100%;\n  border-radius: 10px;\n  height: 40px;\n}\n\n.description_section {\n  padding-top: 50%;\n}\n\n.commencer {\n  --background: var(--ion-btn-custom-color);\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvcGFnZXMvYm9keS1zY2FuLWFycml2YWwtY29hY2gvYm9keS1zY2FuLWFycml2YWwtY29hY2gucGFnZS5zY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBO0VBQ0ksc0VBQUE7RUFHQSxzQkFBQTtFQUNBLGtCQUFBO0FBQ0o7O0FBR0E7RUFDSSxXQUFBO0FBQUo7O0FBRUk7O0VBRUksMEJBQUE7RUFDQSxtQkFBQTtFQUNBLGlCQUFBO0VBQ0EseUJBQUE7RUFDQSxjQUFBO0VBQ0EsaUJBQUE7QUFBUjs7QUFHSTtFQUNJLDBCQUFBO0VBQ0osbUJBQUE7RUFDQSxrQkFBQTtFQUNBLGNBQUE7RUFDQSxtQkFBQTtFQUNBLGlCQUFBO0FBREo7O0FBTUE7RUFDSSxZQUFBO0VBQ0EsYUFBQTtFQUNBLHNCQUFBO0VBQ0EsOEJBQUE7RUFDQSxtQkFBQTtFQUNBLG9DQUFBO0VBQ0EsZ0JBQUE7RUFDQSxpQkFBQTtBQUhKOztBQU9JO0VBQ0ksK0JBQUE7RUFDQSwwQkFBQTtFQUNBLGVBQUE7RUFDQSxnQkFBQTtBQUpSOztBQVdJO0VBQ0ksV0FBQTtFQUNBLG1CQUFBO0VBQ0EsWUFBQTtBQVJSOztBQVlBO0VBQ0ksZ0JBQUE7QUFUSjs7QUFXQTtFQUNJLHlDQUFBO0FBUkoiLCJmaWxlIjoic3JjL2FwcC9wYWdlcy9ib2R5LXNjYW4tYXJyaXZhbC1jb2FjaC9ib2R5LXNjYW4tYXJyaXZhbC1jb2FjaC5wYWdlLnNjc3MiLCJzb3VyY2VzQ29udGVudCI6WyJpb24tY29udGVudCB7XG4gICAgLS1iYWNrZ3JvdW5kOiB1cmwoLi4vLi4vLi4vYXNzZXRzL2ltYWdlcy9sYW5kaW5nX3BhZ2VfYmFja2dyb3VuZF8xLnBuZykwIDE4JS8xMDAlIG5vLXJlcGVhdDtcbiAgICAtd2Via2l0LWJhY2tncm91bmQtc2l6ZTogY292ZXI7XG4gICAgLW1vei1iYWNrZ3JvdW5kLXNpemU6IGNvdmVyO1xuICAgIGJhY2tncm91bmQtc2l6ZTogY292ZXI7XG4gICAgLS1vdmVyZmxvdzogaGlkZGVuO1xuXG59XG5cbi5ib2R5IHtcbiAgICBjb2xvcjogI2ZmZjtcbiAgXG4gICAgLnAxLFxuICAgIC5wMiB7XG4gICAgICAgIGZvbnQtZmFtaWx5OiBPc3dhbGQsIHNlcmlmO1xuICAgICAgICB0ZXh0LWFsaWduOiBqdXN0aWZ5O1xuICAgICAgICBmb250LXNpemU6IDAuOHJlbTtcbiAgICAgICAgLyogbGV0dGVyLXNwYWNpbmc6IDFweDsgKi9cbiAgICAgICAgbWFyZ2luLXRvcDogNSU7XG4gICAgICAgIGxpbmUtaGVpZ2h0OiAxcmVtO1xuICAgIH1cblxuICAgIC5wMyB7XG4gICAgICAgIGZvbnQtZmFtaWx5OiBPc3dhbGQsIHNlcmlmO1xuICAgIHRleHQtYWxpZ246IGp1c3RpZnk7XG4gICAgZm9udC1zdHlsZTogaXRhbGljO1xuICAgIG1hcmdpbi10b3A6IDUlO1xuICAgIGxpbmUtaGVpZ2h0OiAwLjlyZW07XG4gICAgZm9udC1zaXplOiAwLjdyZW07XG4gICAgfVxuXG59XG5cbi5tYWluX2NvbnRhaW5lciB7XG4gICAgaGVpZ2h0OiAxMDAlO1xuICAgIGRpc3BsYXk6IGZsZXg7XG4gICAgZmxleC1kaXJlY3Rpb246IGNvbHVtbjtcbiAgICBqdXN0aWZ5LWNvbnRlbnQ6IHNwYWNlLWJldHdlZW47XG4gICAgcGFkZGluZy1ib3R0b206IDEwJTtcbiAgICBiYWNrZ3JvdW5kLWNvbG9yOiByZ2JhKDAsMCwwLDAuNyk7XG4gICAgcGFkZGluZy1sZWZ0OiA1JTtcbiAgICBwYWRkaW5nLXJpZ2h0OiA1JTtcbn1cblxuLmhlYWRlciB7XG4gICAgcCB7XG4gICAgICAgIGNvbG9yOiB2YXIoLS1pb24tY29sb3ItcHJpbWFyeSk7XG4gICAgICAgIGZvbnQtZmFtaWx5OiBPc3dhbGQsIHNlcmlmO1xuICAgICAgICBmb250LXNpemU6IDE4cHg7XG4gICAgICAgIHRleHQtYWxpZ246IGxlZnQ7XG4gICAgfVxufVxuXG5cbi5mb290ZXIge1xuXG4gICAgaW9uLWJ1dHRvbiB7XG4gICAgICAgIHdpZHRoOiAxMDAlO1xuICAgICAgICBib3JkZXItcmFkaXVzOiAxMHB4O1xuICAgICAgICBoZWlnaHQ6IDQwcHg7XG4gICAgfVxufVxuXG4uZGVzY3JpcHRpb25fc2VjdGlvbiB7XG4gICAgcGFkZGluZy10b3A6IDUwJTtcbn1cbi5jb21tZW5jZXJ7XG4gICAgLS1iYWNrZ3JvdW5kOiB2YXIoLS1pb24tYnRuLWN1c3RvbS1jb2xvcik7XG59XG4iXX0= */");

/***/ }),

/***/ "./src/app/pages/body-scan-arrival-coach/body-scan-arrival-coach.page.ts":
/*!*******************************************************************************!*\
  !*** ./src/app/pages/body-scan-arrival-coach/body-scan-arrival-coach.page.ts ***!
  \*******************************************************************************/
/*! exports provided: BodyScanArrivalCoachPage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "BodyScanArrivalCoachPage", function() { return BodyScanArrivalCoachPage; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");


let BodyScanArrivalCoachPage = class BodyScanArrivalCoachPage {
    constructor() { }
    ngOnInit() {
    }
};
BodyScanArrivalCoachPage.ctorParameters = () => [];
BodyScanArrivalCoachPage = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-body-scan-arrival-coach',
        template: Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(/*! raw-loader!./body-scan-arrival-coach.page.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/pages/body-scan-arrival-coach/body-scan-arrival-coach.page.html")).default,
        styles: [Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(/*! ./body-scan-arrival-coach.page.scss */ "./src/app/pages/body-scan-arrival-coach/body-scan-arrival-coach.page.scss")).default]
    })
], BodyScanArrivalCoachPage);



/***/ })

}]);
//# sourceMappingURL=pages-body-scan-arrival-coach-body-scan-arrival-coach-module-es2015.js.map