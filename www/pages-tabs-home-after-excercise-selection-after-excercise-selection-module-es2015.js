(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["pages-tabs-home-after-excercise-selection-after-excercise-selection-module"],{

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/pages/tabs/home/after-excercise-selection/after-excercise-selection.page.html":
/*!*************************************************************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/pages/tabs/home/after-excercise-selection/after-excercise-selection.page.html ***!
  \*************************************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<ion-content>\n  <ion-row>\n    <ion-col size=\"12\" class=\"list_item_col2\">\n      <ion-item lines=\"none\">\n        <div class=\"black_box\"></div>\n\n        <ion-label class=\"list_section_title\">\n          <ion-label class=\"very_small_text_1\"> Nom de l’exercice </ion-label>\n          <ion-label class=\"very_small_text_1\">\n            Sous nom de l’exercice\n          </ion-label>\n        </ion-label>\n        <ion-reorder slot=\"end\"></ion-reorder>\n      </ion-item>\n    </ion-col>\n\n    <ion-col size=\"12\" class=\"list_item_col2\">\n      <ion-item lines=\"none\">\n        <ion-label class=\"small_text_bold\"\n          >Nombre de fois par semaine\n        </ion-label>\n        <ion-select value=\"2\" class=\"small_text_bold\">\n          <ion-select-option value=\"2\">2</ion-select-option>\n        </ion-select>\n      </ion-item>\n    </ion-col>\n\n    <ion-col size=\"12\">\n      <ion-label class=\"small_text_bold\"> Quels jours ? </ion-label>\n    </ion-col>\n\n    <div class=\"card_holder\">\n      <ion-card\n        [class]=\"days == '1' ? 'active_btn card_btn card_btn_next': 'card_btn card_btn_next'\"\n        (click)=\"days = '1'\"\n      >\n        <ion-label class=\"small_text_bold\"> 1 </ion-label>\n      </ion-card>\n\n      <ion-card\n        [class]=\"days == '2' ? 'active_btn card_btn card_btn_next': 'card_btn card_btn_next'\"\n        (click)=\"days = '2'\"\n      >\n        <ion-label class=\"small_text_bold\"> 2 </ion-label>\n      </ion-card>\n\n      <ion-card\n        [class]=\"days == '3' ? 'active_btn card_btn card_btn_next': 'card_btn card_btn_next'\"\n        (click)=\"days = '3'\"\n      >\n        <ion-label class=\"small_text_bold\"> 3 </ion-label>\n      </ion-card>\n    </div>\n    \n    <ion-col size=\"12\" class=\"list_item_col2\">\n      <ion-item>\n        <ion-label class=\"small_text_bold\"\n          >Méthode de progression\n\n        </ion-label>\n        <ion-select value=\"2\" class=\"small_text_bold\">\n          <ion-select-option value=\"2\">Aucune</ion-select-option>\n        </ion-select>\n      </ion-item>\n    </ion-col>\n  </ion-row>\n\n  \n\n  <ion-row class=\"ion-margin-top\">\n    <ion-col size=\"12\" class=\"list_item_col2\">\n      <ion-item lines=\"none\">\n        <div class=\"black_box\"></div>\n\n        <ion-label class=\"list_section_title\">\n          <ion-label class=\"very_small_text_1\"> Nom de l’exercice </ion-label>\n          <ion-label class=\"very_small_text_1\">\n            Sous nom de l’exercice\n          </ion-label>\n        </ion-label>\n        <ion-reorder slot=\"end\"></ion-reorder>\n      </ion-item>\n    </ion-col>\n\n    <ion-col size=\"12\" class=\"list_item_col2\">\n      <ion-item lines=\"none\">\n        <ion-label class=\"small_text_bold\"\n          >Nombre de fois par semaine\n        </ion-label>\n        <ion-select value=\"2\" class=\"small_text_bold\">\n          <ion-select-option value=\"2\">2</ion-select-option>\n        </ion-select>\n      </ion-item>\n    </ion-col>\n\n    <ion-col size=\"12\">\n      <ion-label class=\"small_text_bold\"> Quels jours ? </ion-label>\n    </ion-col>\n\n    <div class=\"card_holder\">\n      <ion-card\n        [class]=\"days1 == '1' ? 'active_btn card_btn card_btn_next': 'card_btn card_btn_next'\"\n        (click)=\"days1 = '1'\"\n      >\n        <ion-label class=\"small_text_bold\"> 1 </ion-label>\n      </ion-card>\n\n      <ion-card\n        [class]=\"days1 == '2' ? 'active_btn card_btn card_btn_next': 'card_btn card_btn_next'\"\n        (click)=\"days1 = '2'\"\n      >\n        <ion-label class=\"small_text_bold\"> 2 </ion-label>\n      </ion-card>\n\n      <ion-card\n        [class]=\"days1 == '3' ? 'active_btn card_btn card_btn_next': 'card_btn card_btn_next'\"\n        (click)=\"days1 = '3'\"\n      >\n        <ion-label class=\"small_text_bold\"> 3 </ion-label>\n      </ion-card>\n    </div>\n    \n    <ion-col size=\"12\" class=\"list_item_col2\">\n      <ion-item>\n        <ion-label class=\"small_text_bold\"\n          >Méthode de progression\n\n        </ion-label>\n        <ion-select value=\"2\" class=\"small_text_bold\">\n          <ion-select-option value=\"2\">Aucune</ion-select-option>\n        </ion-select>\n      </ion-item>\n    </ion-col>\n  </ion-row>\n\n</ion-content>\n\n\n\n<ion-footer>\n  <ion-toolbar>\n    <ion-grid>\n      <ion-row>\n        <ion-col size=\"3\">\n          <ion-button\n            class=\"back\"\n            expand=\"full\"\n            shape=\"round\"\n            routerLink=\"/hypertrophy-strengthening-program-creation\"\n            >\n            <img src=\"assets/icon/back-triangle.png\" alt=\"\">\n          </ion-button>\n        </ion-col>\n        <ion-col>\n          <!-- <ion-button\n            class=\"next\"\n            expand=\"full\"\n            shape=\"round\"\n            routerLink=\"/excercise-sheet\"\n            >Ajouter [x] exercices\n          </ion-button> -->\n\n          <ion-button\n          class=\"next\"\n          expand=\"full\"\n          shape=\"round\"\n          routerLink=\"/excercise-sheet\"\n          >Valider les exercices de force\n        </ion-button>\n\n        \n        </ion-col>\n      </ion-row>\n    </ion-grid>\n  </ion-toolbar>\n</ion-footer>\n");

/***/ }),

/***/ "./src/app/pages/tabs/home/after-excercise-selection/after-excercise-selection-routing.module.ts":
/*!*******************************************************************************************************!*\
  !*** ./src/app/pages/tabs/home/after-excercise-selection/after-excercise-selection-routing.module.ts ***!
  \*******************************************************************************************************/
/*! exports provided: AfterExcerciseSelectionPageRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AfterExcerciseSelectionPageRoutingModule", function() { return AfterExcerciseSelectionPageRoutingModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");
/* harmony import */ var _after_excercise_selection_page__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./after-excercise-selection.page */ "./src/app/pages/tabs/home/after-excercise-selection/after-excercise-selection.page.ts");




const routes = [
    {
        path: '',
        component: _after_excercise_selection_page__WEBPACK_IMPORTED_MODULE_3__["AfterExcerciseSelectionPage"]
    }
];
let AfterExcerciseSelectionPageRoutingModule = class AfterExcerciseSelectionPageRoutingModule {
};
AfterExcerciseSelectionPageRoutingModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]],
    })
], AfterExcerciseSelectionPageRoutingModule);



/***/ }),

/***/ "./src/app/pages/tabs/home/after-excercise-selection/after-excercise-selection.module.ts":
/*!***********************************************************************************************!*\
  !*** ./src/app/pages/tabs/home/after-excercise-selection/after-excercise-selection.module.ts ***!
  \***********************************************************************************************/
/*! exports provided: AfterExcerciseSelectionPageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AfterExcerciseSelectionPageModule", function() { return AfterExcerciseSelectionPageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/__ivy_ngcc__/fesm2015/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/__ivy_ngcc__/fesm2015/forms.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/__ivy_ngcc__/fesm2015/ionic-angular.js");
/* harmony import */ var _after_excercise_selection_routing_module__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./after-excercise-selection-routing.module */ "./src/app/pages/tabs/home/after-excercise-selection/after-excercise-selection-routing.module.ts");
/* harmony import */ var _after_excercise_selection_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./after-excercise-selection.page */ "./src/app/pages/tabs/home/after-excercise-selection/after-excercise-selection.page.ts");







let AfterExcerciseSelectionPageModule = class AfterExcerciseSelectionPageModule {
};
AfterExcerciseSelectionPageModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [
            _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonicModule"],
            _after_excercise_selection_routing_module__WEBPACK_IMPORTED_MODULE_5__["AfterExcerciseSelectionPageRoutingModule"]
        ],
        declarations: [_after_excercise_selection_page__WEBPACK_IMPORTED_MODULE_6__["AfterExcerciseSelectionPage"]]
    })
], AfterExcerciseSelectionPageModule);



/***/ }),

/***/ "./src/app/pages/tabs/home/after-excercise-selection/after-excercise-selection.page.scss":
/*!***********************************************************************************************!*\
  !*** ./src/app/pages/tabs/home/after-excercise-selection/after-excercise-selection.page.scss ***!
  \***********************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("ion-content {\n  --padding-bottom: 2%;\n  --padding-start:2%;\n  --padding-top: 2%;\n  --padding-end: 2%;\n}\n\n.black_box {\n  width: 37px;\n  background: #000;\n  height: 30px;\n}\n\n.list_section_title {\n  padding-left: 4%;\n}\n\n.very_small_text_1 {\n  font-size: 0.75rem !important;\n  color: #000;\n  font-weight: 500;\n}\n\n.list_item_col2 ion-item {\n  --padding-start: 0;\n}\n\n.active_btn {\n  background: var(--ion-btn-custom-color);\n}\n\n.card_holder {\n  display: flex;\n}\n\n.card_btn {\n  height: 60px;\n  width: 60px;\n  border: 2px solid #565656;\n  margin: 0;\n  display: flex;\n  justify-content: center;\n}\n\n.card_btn ion-label {\n  align-self: center;\n  font-size: 1.2rem;\n  font-weight: 600;\n  color: #fff;\n}\n\n.add_btn_col {\n  display: flex;\n  justify-content: flex-end;\n}\n\n.card_btn_next {\n  margin-left: 5px;\n}\n\n.card_btn_next ion-label {\n  align-self: center;\n  font-size: 1.2rem;\n  font-weight: 600;\n  color: #000;\n}\n\n.footer_section_btn {\n  width: 100%;\n  padding-left: 10px;\n  padding-right: 10px;\n  margin-top: 32px;\n  --background: var(--ion-btn-custom-color);\n}\n\n.back,\n.next {\n  --ion-button-round-border-radius: 5px;\n  --ion-button-border-radius: 5px;\n  --ion-border-radius: 5px;\n  --border-radius: 5px;\n  height: 40px;\n}\n\n.next {\n  --background: var(--ion-btn-custom-color);\n}\n\n.back {\n  --background: linear-gradient(to right, #161616e0, #c1c1c1);\n}\n\n.back img {\n  width: 70%;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvcGFnZXMvdGFicy9ob21lL2FmdGVyLWV4Y2VyY2lzZS1zZWxlY3Rpb24vYWZ0ZXItZXhjZXJjaXNlLXNlbGVjdGlvbi5wYWdlLnNjc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUE7RUFDSSxvQkFBQTtFQUNBLGtCQUFBO0VBQ0EsaUJBQUE7RUFFQSxpQkFBQTtBQUFKOztBQUtBO0VBQ0ksV0FBQTtFQUNBLGdCQUFBO0VBQ0EsWUFBQTtBQUZKOztBQU1BO0VBQ0ksZ0JBQUE7QUFISjs7QUFNQTtFQUNJLDZCQUFBO0VBQ0EsV0FBQTtFQUNBLGdCQUFBO0FBSEo7O0FBUUk7RUFHSSxrQkFBQTtBQVBSOztBQVdBO0VBQ0ksdUNBQUE7QUFSSjs7QUFXQTtFQUNJLGFBQUE7QUFSSjs7QUFXQTtFQUNJLFlBQUE7RUFDQSxXQUFBO0VBQ0EseUJBQUE7RUFDQSxTQUFBO0VBQ0EsYUFBQTtFQUVBLHVCQUFBO0FBVEo7O0FBV0k7RUFDSSxrQkFBQTtFQUNBLGlCQUFBO0VBQ0EsZ0JBQUE7RUFDQSxXQUFBO0FBVFI7O0FBY0E7RUFDSSxhQUFBO0VBQ0EseUJBQUE7QUFYSjs7QUFlQTtFQUNJLGdCQUFBO0FBWko7O0FBYUk7RUFDSSxrQkFBQTtFQUNBLGlCQUFBO0VBQ0EsZ0JBQUE7RUFDQSxXQUFBO0FBWFI7O0FBa0JBO0VBRUksV0FBQTtFQUNBLGtCQUFBO0VBQ0EsbUJBQUE7RUFDQSxnQkFBQTtFQUNBLHlDQUFBO0FBaEJKOztBQW9CQTs7RUFFSSxxQ0FBQTtFQUNBLCtCQUFBO0VBQ0Esd0JBQUE7RUFDQSxvQkFBQTtFQUNBLFlBQUE7QUFqQko7O0FBcUJBO0VBQ0kseUNBQUE7QUFsQko7O0FBc0JBO0VBQ0ksMkRBQUE7QUFuQko7O0FBb0JJO0VBQ0ksVUFBQTtBQWxCUiIsImZpbGUiOiJzcmMvYXBwL3BhZ2VzL3RhYnMvaG9tZS9hZnRlci1leGNlcmNpc2Utc2VsZWN0aW9uL2FmdGVyLWV4Y2VyY2lzZS1zZWxlY3Rpb24ucGFnZS5zY3NzIiwic291cmNlc0NvbnRlbnQiOlsiaW9uLWNvbnRlbnQge1xuICAgIC0tcGFkZGluZy1ib3R0b206IDIlO1xuICAgIC0tcGFkZGluZy1zdGFydDoyJTtcbiAgICAtLXBhZGRpbmctdG9wOiAyJTtcblxuICAgIC0tcGFkZGluZy1lbmQ6IDIlO1xuXG59XG5cblxuLmJsYWNrX2JveCB7XG4gICAgd2lkdGg6IDM3cHg7XG4gICAgYmFja2dyb3VuZDogIzAwMDtcbiAgICBoZWlnaHQ6IDMwcHg7XG5cbn1cblxuLmxpc3Rfc2VjdGlvbl90aXRsZSB7XG4gICAgcGFkZGluZy1sZWZ0OiA0JTtcbn1cblxuLnZlcnlfc21hbGxfdGV4dF8xIHtcbiAgICBmb250LXNpemU6IDAuNzVyZW0gIWltcG9ydGFudDtcbiAgICBjb2xvcjogIzAwMDtcbiAgICBmb250LXdlaWdodDogNTAwO1xufVxuXG5cbi5saXN0X2l0ZW1fY29sMiB7XG4gICAgaW9uLWl0ZW0ge1xuXG5cbiAgICAgICAgLS1wYWRkaW5nLXN0YXJ0OiAwO1xuICAgIH1cbn1cblxuLmFjdGl2ZV9idG4ge1xuICAgIGJhY2tncm91bmQ6IHZhcigtLWlvbi1idG4tY3VzdG9tLWNvbG9yKTtcbn1cblxuLmNhcmRfaG9sZGVyIHtcbiAgICBkaXNwbGF5OiBmbGV4O1xufVxuXG4uY2FyZF9idG4ge1xuICAgIGhlaWdodDogNjBweDtcbiAgICB3aWR0aDogNjBweDtcbiAgICBib3JkZXI6IDJweCBzb2xpZCAjNTY1NjU2O1xuICAgIG1hcmdpbjogMDtcbiAgICBkaXNwbGF5OiBmbGV4O1xuXG4gICAganVzdGlmeS1jb250ZW50OiBjZW50ZXI7XG5cbiAgICBpb24tbGFiZWwge1xuICAgICAgICBhbGlnbi1zZWxmOiBjZW50ZXI7XG4gICAgICAgIGZvbnQtc2l6ZTogMS4ycmVtO1xuICAgICAgICBmb250LXdlaWdodDogNjAwO1xuICAgICAgICBjb2xvcjogI2ZmZjtcbiAgICB9XG59XG5cblxuLmFkZF9idG5fY29sIHtcbiAgICBkaXNwbGF5OiBmbGV4O1xuICAgIGp1c3RpZnktY29udGVudDogZmxleC1lbmQ7XG59XG5cblxuLmNhcmRfYnRuX25leHQge1xuICAgIG1hcmdpbi1sZWZ0OiA1cHg7XG4gICAgaW9uLWxhYmVsIHtcbiAgICAgICAgYWxpZ24tc2VsZjogY2VudGVyO1xuICAgICAgICBmb250LXNpemU6IDEuMnJlbTtcbiAgICAgICAgZm9udC13ZWlnaHQ6IDYwMDtcbiAgICAgICAgY29sb3I6ICMwMDA7XG4gICAgfVxufVxuXG5cblxuXG4uZm9vdGVyX3NlY3Rpb25fYnRuIHtcblxuICAgIHdpZHRoOiAxMDAlO1xuICAgIHBhZGRpbmctbGVmdDogMTBweDtcbiAgICBwYWRkaW5nLXJpZ2h0OiAxMHB4O1xuICAgIG1hcmdpbi10b3A6IDMycHg7XG4gICAgLS1iYWNrZ3JvdW5kOiB2YXIoLS1pb24tYnRuLWN1c3RvbS1jb2xvcik7XG59XG5cblxuLmJhY2ssXG4ubmV4dCB7XG4gICAgLS1pb24tYnV0dG9uLXJvdW5kLWJvcmRlci1yYWRpdXM6IDVweDtcbiAgICAtLWlvbi1idXR0b24tYm9yZGVyLXJhZGl1czogNXB4O1xuICAgIC0taW9uLWJvcmRlci1yYWRpdXM6IDVweDtcbiAgICAtLWJvcmRlci1yYWRpdXM6IDVweDtcbiAgICBoZWlnaHQ6IDQwcHg7XG59XG5cblxuLm5leHQge1xuICAgIC0tYmFja2dyb3VuZDogdmFyKC0taW9uLWJ0bi1jdXN0b20tY29sb3IpO1xuXG59XG5cbi5iYWNrIHtcbiAgICAtLWJhY2tncm91bmQ6IGxpbmVhci1ncmFkaWVudCh0byByaWdodCwgIzE2MTYxNmUwLCAjYzFjMWMxKTtcbiAgICBpbWcge1xuICAgICAgICB3aWR0aDogNzAlO1xuICAgIH1cbn1cblxuIl19 */");

/***/ }),

/***/ "./src/app/pages/tabs/home/after-excercise-selection/after-excercise-selection.page.ts":
/*!*********************************************************************************************!*\
  !*** ./src/app/pages/tabs/home/after-excercise-selection/after-excercise-selection.page.ts ***!
  \*********************************************************************************************/
/*! exports provided: AfterExcerciseSelectionPage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AfterExcerciseSelectionPage", function() { return AfterExcerciseSelectionPage; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");


let AfterExcerciseSelectionPage = class AfterExcerciseSelectionPage {
    constructor() {
        this.days = '1';
        this.days1 = '1';
    }
    ngOnInit() {
    }
};
AfterExcerciseSelectionPage.ctorParameters = () => [];
AfterExcerciseSelectionPage = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-after-excercise-selection',
        template: Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(/*! raw-loader!./after-excercise-selection.page.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/pages/tabs/home/after-excercise-selection/after-excercise-selection.page.html")).default,
        styles: [Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(/*! ./after-excercise-selection.page.scss */ "./src/app/pages/tabs/home/after-excercise-selection/after-excercise-selection.page.scss")).default]
    })
], AfterExcerciseSelectionPage);



/***/ })

}]);
//# sourceMappingURL=pages-tabs-home-after-excercise-selection-after-excercise-selection-module-es2015.js.map