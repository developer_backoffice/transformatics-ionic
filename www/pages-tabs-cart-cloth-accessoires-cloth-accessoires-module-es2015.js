(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["pages-tabs-cart-cloth-accessoires-cloth-accessoires-module"],{

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/pages/tabs/cart/cloth-accessoires/cloth-accessoires.page.html":
/*!*********************************************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/pages/tabs/cart/cloth-accessoires/cloth-accessoires.page.html ***!
  \*********************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<ion-content>\n    <div class=\"banner\">\n        <img src=\"assets/images/cloth-assories.png\">\n    </div>\n    <ion-grid>\n        <ion-row>\n            <ion-col size=12>\n                <div class=\"ebook-title\">\n                    <h4>VETEMENTS & ACCESSOIRES</h4>\n                </div>\n            </ion-col>\n            <ion-col size=\"4\">\n                <div class=\"brand-logo\">\n                    <img src=\"assets/icon/brand/logo1.jpg\">\n                </div>\n            </ion-col>\n            <ion-col size=\"4\">\n                <div class=\"brand-logo\">\n                    <img src=\"assets/icon/brand/logo2.jpg\">\n                </div>\n            </ion-col>\n            <ion-col size=\"4\">\n                <div class=\"brand-logo\">\n                    <img src=\"assets/icon/brand/logo3.jpg\">\n                </div>\n            </ion-col>\n            <ion-col size=\"4\">\n                <div class=\"brand-logo\">\n                    <img src=\"assets/icon/brand/logo4.png\">\n                </div>\n            </ion-col>\n            <ion-col size=\"4\">\n                <div class=\"brand-logo\">\n                    <img src=\"assets/icon/brand/logo5.png\">\n                </div>\n            </ion-col>\n        </ion-row>\n    </ion-grid>\n</ion-content>");

/***/ }),

/***/ "./src/app/pages/tabs/cart/cloth-accessoires/cloth-accessoires-routing.module.ts":
/*!***************************************************************************************!*\
  !*** ./src/app/pages/tabs/cart/cloth-accessoires/cloth-accessoires-routing.module.ts ***!
  \***************************************************************************************/
/*! exports provided: ClothAccessoiresPageRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ClothAccessoiresPageRoutingModule", function() { return ClothAccessoiresPageRoutingModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");
/* harmony import */ var _cloth_accessoires_page__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./cloth-accessoires.page */ "./src/app/pages/tabs/cart/cloth-accessoires/cloth-accessoires.page.ts");




const routes = [
    {
        path: '',
        component: _cloth_accessoires_page__WEBPACK_IMPORTED_MODULE_3__["ClothAccessoiresPage"]
    }
];
let ClothAccessoiresPageRoutingModule = class ClothAccessoiresPageRoutingModule {
};
ClothAccessoiresPageRoutingModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]],
    })
], ClothAccessoiresPageRoutingModule);



/***/ }),

/***/ "./src/app/pages/tabs/cart/cloth-accessoires/cloth-accessoires.module.ts":
/*!*******************************************************************************!*\
  !*** ./src/app/pages/tabs/cart/cloth-accessoires/cloth-accessoires.module.ts ***!
  \*******************************************************************************/
/*! exports provided: ClothAccessoiresPageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ClothAccessoiresPageModule", function() { return ClothAccessoiresPageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/__ivy_ngcc__/fesm2015/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/__ivy_ngcc__/fesm2015/forms.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/__ivy_ngcc__/fesm2015/ionic-angular.js");
/* harmony import */ var _cloth_accessoires_routing_module__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./cloth-accessoires-routing.module */ "./src/app/pages/tabs/cart/cloth-accessoires/cloth-accessoires-routing.module.ts");
/* harmony import */ var _cloth_accessoires_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./cloth-accessoires.page */ "./src/app/pages/tabs/cart/cloth-accessoires/cloth-accessoires.page.ts");







let ClothAccessoiresPageModule = class ClothAccessoiresPageModule {
};
ClothAccessoiresPageModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [
            _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonicModule"],
            _cloth_accessoires_routing_module__WEBPACK_IMPORTED_MODULE_5__["ClothAccessoiresPageRoutingModule"]
        ],
        declarations: [_cloth_accessoires_page__WEBPACK_IMPORTED_MODULE_6__["ClothAccessoiresPage"]]
    })
], ClothAccessoiresPageModule);



/***/ }),

/***/ "./src/app/pages/tabs/cart/cloth-accessoires/cloth-accessoires.page.scss":
/*!*******************************************************************************!*\
  !*** ./src/app/pages/tabs/cart/cloth-accessoires/cloth-accessoires.page.scss ***!
  \*******************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("ion-content {\n  --background: #000;\n}\n\n.ebook-title h4 {\n  color: #fff;\n  text-align: center;\n  font-size: 15px;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvcGFnZXMvdGFicy9jYXJ0L2Nsb3RoLWFjY2Vzc29pcmVzL2Nsb3RoLWFjY2Vzc29pcmVzLnBhZ2Uuc2NzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQTtFQUNJLGtCQUFBO0FBQ0o7O0FBRUE7RUFDSSxXQUFBO0VBQ0Esa0JBQUE7RUFDQSxlQUFBO0FBQ0oiLCJmaWxlIjoic3JjL2FwcC9wYWdlcy90YWJzL2NhcnQvY2xvdGgtYWNjZXNzb2lyZXMvY2xvdGgtYWNjZXNzb2lyZXMucGFnZS5zY3NzIiwic291cmNlc0NvbnRlbnQiOlsiaW9uLWNvbnRlbnQge1xuICAgIC0tYmFja2dyb3VuZDogIzAwMDtcbn1cblxuLmVib29rLXRpdGxlIGg0IHtcbiAgICBjb2xvcjogI2ZmZjtcbiAgICB0ZXh0LWFsaWduOiBjZW50ZXI7XG4gICAgZm9udC1zaXplOiAxNXB4O1xufSJdfQ== */");

/***/ }),

/***/ "./src/app/pages/tabs/cart/cloth-accessoires/cloth-accessoires.page.ts":
/*!*****************************************************************************!*\
  !*** ./src/app/pages/tabs/cart/cloth-accessoires/cloth-accessoires.page.ts ***!
  \*****************************************************************************/
/*! exports provided: ClothAccessoiresPage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ClothAccessoiresPage", function() { return ClothAccessoiresPage; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");


let ClothAccessoiresPage = class ClothAccessoiresPage {
    constructor() { }
    ngOnInit() {
    }
};
ClothAccessoiresPage.ctorParameters = () => [];
ClothAccessoiresPage = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-cloth-accessoires',
        template: Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(/*! raw-loader!./cloth-accessoires.page.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/pages/tabs/cart/cloth-accessoires/cloth-accessoires.page.html")).default,
        styles: [Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(/*! ./cloth-accessoires.page.scss */ "./src/app/pages/tabs/cart/cloth-accessoires/cloth-accessoires.page.scss")).default]
    })
], ClothAccessoiresPage);



/***/ })

}]);
//# sourceMappingURL=pages-tabs-cart-cloth-accessoires-cloth-accessoires-module-es2015.js.map