(function () {
  function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

  function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

  function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

  (window["webpackJsonp"] = window["webpackJsonp"] || []).push([["pages-tabs-tabs-module"], {
    /***/
    "./node_modules/raw-loader/dist/cjs.js!./src/app/pages/tabs/tabs.page.html":
    /*!*********************************************************************************!*\
      !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/pages/tabs/tabs.page.html ***!
      \*********************************************************************************/

    /*! exports provided: default */

    /***/
    function node_modulesRawLoaderDistCjsJsSrcAppPagesTabsTabsPageHtml(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony default export */


      __webpack_exports__["default"] = "<ion-tabs [ngClass]=\"{menu: this.userData.changeColor}\">\n  <ion-tab-bar slot=\"bottom\">\n    <ion-tab-button tab=\"menu\" (click)=\"myMethod1()\">\n      <ion-icon name=\"grid-outline\"></ion-icon>\n      <!-- <ion-label>Menu</ion-label> -->\n    </ion-tab-button>\n\n    <ion-tab-button tab=\"myaccount\" (click)=\"myMethod2()\">\n      <ion-icon name=\"person-circle-outline\"></ion-icon>\n      <!-- <ion-label>My Account</ion-label> -->\n    </ion-tab-button>\n\n    <ion-tab-button tab=\"home\" (click)=\"myMethod2()\">\n      <ion-icon name=\"home-outline\"></ion-icon>\n      <!-- <ion-label>Home</ion-label> -->\n    </ion-tab-button>\n\n    <ion-tab-button tab=\"cart\" (click)=\"myMethod2()\">\n      <ion-icon name=\"cart-outline\"></ion-icon>\n      <!-- <ion-label>Cart</ion-label> -->\n    </ion-tab-button>\n    <ion-tab-button  >\n\n<ion-icon *ngIf=\"!showBackdrop\" name=\"add-circle-outline\" class=\"setting_icon\"></ion-icon>\n\t\t\n\n      <!-- <ion-fab \n  horizontal=\"end\"\n  vertical=\"bottom\"\n  slot=\"fixed\"\n  (click)=\"showBackdrop = !showBackdrop\"\n>\n  <ion-fab-button class=\"\">\n    <ion-icon name=\"add-circle-outline\" class=\"setting_icon\"></ion-icon>\n\n  </ion-fab-button>\n  <ion-fab-list side=\"top\"> -->\n\n    <!-- <ion-fab-button\n      color=\"light\"\n      routerDirection=\"forward\"\n      data-desc=\"Petit-déjeuner\"\n      class=\"common_fab_btn\"\n      routerLink=\"/aliments\"\n    >\n      <ion-img src=\"assets/icon/p-designer.png\"></ion-img>\n    </ion-fab-button> -->\n    <!-- <ion-fab-button color=\"light\" routerDirection=\"forward\" data-desc=\"Déjeuner\" class=\"common_fab_btn\" routerLink=\"/aliments\">\n      <ion-img src=\"assets/icon/designer.png\"></ion-img>\n    </ion-fab-button> -->\n    <!-- <ion-fab-button color=\"light\" routerDirection=\"forward\" data-desc=\"Repas\" class=\"common_fab_btn\" routerLink=\"/aliments\">\n      <ion-img src=\"assets/icon/dinner.png\"></ion-img>\n    </ion-fab-button> -->\n    <!-- <ion-fab-button\n      color=\"light\"\n      routerDirection=\"forward\"\n      data-desc=\"Snack\"\n      class=\"common_fab_btn\"\n      routerLink=\"/aliments\"\n    >\n      <ion-img src=\"assets/icon/snacks-one.png\"></ion-img>\n    </ion-fab-button> -->\n\n    <!-- <ion-fab-button color=\"light\" routerDirection=\"forward\" data-desc=\"ACTIVITE PHYSIQUE\" class=\"common_fab_btn\">\n        <ion-img src=\"assets/icon/activity_physique.png\"></ion-img>\n      </ion-fab-button>\n    \n      \n  </ion-fab-list>\n</ion-fab> -->\n\n      <!-- <ion-icon name=\"add-circle-outline\" class=\"setting_icon\"></ion-icon> -->\n     \n    </ion-tab-button>\n  </ion-tab-bar>\n</ion-tabs>\n\n<ion-fab  horizontal=\"end\"\nvertical=\"bottom\"\nslot=\"fixed\"\n(click)=\"showBackdrop = !showBackdrop\">\n\t<ion-fab-button>\n\t</ion-fab-button>\n\t<ion-fab-list side=\"top\"> \n\t\t<ion-fab-button color=\"light\" routerDirection=\"forward\" data-desc=\"Repas\" class=\"common_fab_btn\" (click)=\"openManualModal()\">\n\t\t  <ion-img src=\"assets/icon/dinner.png\"></ion-img>\n\t\t</ion-fab-button>\n\t\n\t\t<ion-fab-button color=\"light\" routerDirection=\"forward\" data-desc=\"ACTIVITE PHYSIQUE\" class=\"common_fab_btn\">\n\t\t\t<ion-img src=\"assets/icon/activity_physique.png\"></ion-img>\n\t\t  </ion-fab-button>\n\t  </ion-fab-list>\n\n  </ion-fab>\n<div *ngIf=\"showBackdrop\" class=\"overlay\"></div>\n";
      /***/
    },

    /***/
    "./src/app/pages/tabs/tabs-routing.module.ts":
    /*!***************************************************!*\
      !*** ./src/app/pages/tabs/tabs-routing.module.ts ***!
      \***************************************************/

    /*! exports provided: TabsPageRoutingModule */

    /***/
    function srcAppPagesTabsTabsRoutingModuleTs(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "TabsPageRoutingModule", function () {
        return TabsPageRoutingModule;
      });
      /* harmony import */


      var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! tslib */
      "./node_modules/tslib/tslib.es6.js");
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! @angular/core */
      "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
      /* harmony import */


      var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! @angular/router */
      "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");
      /* harmony import */


      var _tabs_page__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
      /*! ./tabs.page */
      "./src/app/pages/tabs/tabs.page.ts");

      var routes = [// {
      //   path: '',
      //   component: TabsPage
      // },
      // {
      //   path: 'home',
      //   loadChildren: () => import('./home/home.module').then( m => m.HomePageModule)
      // },
      // {
      //   path: 'myaccount',
      //   loadChildren: () => import('./myaccount/myaccount.module').then( m => m.MyaccountPageModule)
      // },
      // {
      //   path: 'cart',
      //   loadChildren: () => import('./cart/cart.module').then( m => m.CartPageModule)
      // },
      // {
      //   path: 'menu',
      //   loadChildren: () => import('./menu/menu.module').then( m => m.MenuPageModule)
      // },
      // {
      //   path: 'settings',
      //   loadChildren: () => import('./settings/settings.module').then( m => m.SettingsPageModule)
      // }
      {
        path: '',
        component: _tabs_page__WEBPACK_IMPORTED_MODULE_3__["TabsPage"],
        children: [{
          path: 'home',
          children: [{
            path: '',
            loadChildren: function loadChildren() {
              return __webpack_require__.e(
              /*! import() | home-home-module */
              "home-home-module").then(__webpack_require__.bind(null,
              /*! ./home/home.module */
              "./src/app/pages/tabs/home/home.module.ts")).then(function (m) {
                return m.HomePageModule;
              });
            }
          }]
        }, {
          path: 'myaccount',
          children: [{
            path: '',
            loadChildren: function loadChildren() {
              return __webpack_require__.e(
              /*! import() | myaccount-myaccount-module */
              "myaccount-myaccount-module").then(__webpack_require__.bind(null,
              /*! ./myaccount/myaccount.module */
              "./src/app/pages/tabs/myaccount/myaccount.module.ts")).then(function (m) {
                return m.MyaccountPageModule;
              });
            }
          }]
        }, {
          path: 'cart',
          children: [{
            path: '',
            loadChildren: function loadChildren() {
              return __webpack_require__.e(
              /*! import() | cart-cart-module */
              "cart-cart-module").then(__webpack_require__.bind(null,
              /*! ./cart/cart.module */
              "./src/app/pages/tabs/cart/cart.module.ts")).then(function (m) {
                return m.CartPageModule;
              });
            }
          }]
        }, {
          path: 'menu',
          children: [{
            path: '',
            loadChildren: function loadChildren() {
              return __webpack_require__.e(
              /*! import() | menu-menu-module */
              "menu-menu-module").then(__webpack_require__.bind(null,
              /*! ./menu/menu.module */
              "./src/app/pages/tabs/menu/menu.module.ts")).then(function (m) {
                return m.MenuPageModule;
              });
            }
          }]
        }, {
          path: 'settings',
          children: [{
            path: '',
            loadChildren: function loadChildren() {
              return __webpack_require__.e(
              /*! import() | settings-settings-module */
              "settings-settings-module").then(__webpack_require__.bind(null,
              /*! ./settings/settings.module */
              "./src/app/pages/tabs/settings/settings.module.ts")).then(function (m) {
                return m.SettingsPageModule;
              });
            }
          }]
        }, {
          path: '',
          redirectTo: 'home',
          pathMatch: 'full'
        }]
      }];

      var TabsPageRoutingModule = function TabsPageRoutingModule() {
        _classCallCheck(this, TabsPageRoutingModule);
      };

      TabsPageRoutingModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]]
      })], TabsPageRoutingModule);
      /***/
    },

    /***/
    "./src/app/pages/tabs/tabs.module.ts":
    /*!*******************************************!*\
      !*** ./src/app/pages/tabs/tabs.module.ts ***!
      \*******************************************/

    /*! exports provided: TabsPageModule */

    /***/
    function srcAppPagesTabsTabsModuleTs(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "TabsPageModule", function () {
        return TabsPageModule;
      });
      /* harmony import */


      var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! tslib */
      "./node_modules/tslib/tslib.es6.js");
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! @angular/core */
      "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
      /* harmony import */


      var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! @angular/common */
      "./node_modules/@angular/common/__ivy_ngcc__/fesm2015/common.js");
      /* harmony import */


      var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
      /*! @angular/forms */
      "./node_modules/@angular/forms/__ivy_ngcc__/fesm2015/forms.js");
      /* harmony import */


      var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
      /*! @ionic/angular */
      "./node_modules/@ionic/angular/__ivy_ngcc__/fesm2015/ionic-angular.js");
      /* harmony import */


      var _tabs_routing_module__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(
      /*! ./tabs-routing.module */
      "./src/app/pages/tabs/tabs-routing.module.ts");
      /* harmony import */


      var _tabs_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(
      /*! ./tabs.page */
      "./src/app/pages/tabs/tabs.page.ts");
      /* harmony import */


      var _components_shared_module__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(
      /*! @components/shared.module */
      "./src/app/components/shared.module.ts");

      var TabsPageModule = function TabsPageModule() {
        _classCallCheck(this, TabsPageModule);
      };

      TabsPageModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"], _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"], _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonicModule"], _tabs_routing_module__WEBPACK_IMPORTED_MODULE_5__["TabsPageRoutingModule"], _components_shared_module__WEBPACK_IMPORTED_MODULE_7__["SharedModule"]],
        declarations: [_tabs_page__WEBPACK_IMPORTED_MODULE_6__["TabsPage"]]
      })], TabsPageModule);
      /***/
    },

    /***/
    "./src/app/pages/tabs/tabs.page.scss":
    /*!*******************************************!*\
      !*** ./src/app/pages/tabs/tabs.page.scss ***!
      \*******************************************/

    /*! exports provided: default */

    /***/
    function srcAppPagesTabsTabsPageScss(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony default export */


      __webpack_exports__["default"] = "ion-tab-bar {\n  position: relative;\n  margin-bottom: 10px;\n  margin-top: 10px;\n  margin-left: 10px;\n  margin-right: 10px;\n  border-radius: 15px;\n  --background: #e6e6e6;\n  padding: 10px;\n  box-shadow: 0px 1px 3px 0px rgba(0, 0, 0, 0.1), 0px 1px 3px 0px rgba(0, 0, 0, 0.08);\n  border: none;\n}\n\nion-tab-button {\n  border-radius: 15px;\n  background: #fff;\n  padding: 5px;\n  box-shadow: rgba(0, 0, 0, 0.12) 0px 4px 16px;\n  margin-right: 5px;\n  margin-left: 5px;\n}\n\nion-tabs.menu {\n  background: linear-gradient(56deg, #500816, #630f1f) !important;\n}\n\nion-tabs.page {\n  background: transparent !important;\n}\n\n.overlay {\n  position: fixed;\n  display: block;\n  width: 100%;\n  height: 100%;\n  top: 0;\n  left: 0;\n  right: 0;\n  bottom: 0;\n  background-color: rgba(0, 0, 0, 0.5);\n  z-index: 99;\n  cursor: pointer;\n}\n\nion-fab-button[data-desc]::after {\n  position: absolute;\n  content: attr(data-desc);\n  z-index: 1;\n  right: 50px;\n  bottom: 4px;\n  background-color: var(--ion-color-dark);\n  padding: 5px 20px;\n  font-size: 10px;\n  border-radius: 5px;\n  background: #fff;\n  color: #000;\n  font-weight: bold;\n  box-shadow: 0 3px 5px -1px rgba(0, 0, 0, 0.2), 0 6px 10px 0 rgba(0, 0, 0, 0.14), 0 1px 18px 0 rgba(0, 0, 0, 0.12);\n}\n\n.setting_icon {\n  color: #615858;\n}\n\n.common_fab_btn ion-img {\n  width: 20px;\n}\n\nion-fab-list {\n  margin-bottom: 90px;\n  -webkit-animation: zoom-in 0.4s;\n          animation: zoom-in 0.4s;\n}\n\nion-fab {\n  position: absolute;\n  bottom: 2.5%;\n  right: 7%;\n}\n\nion-fab ion-fab-button {\n  --background: transparent;\n  --background-activated: transparent !important;\n  --box-shadow: none;\n}\n\n@-webkit-keyframes zoom-in {\n  from {\n    opacity: 0.5;\n    transform: scale3d(0.4, 0.4, 0.4);\n  }\n  to {\n    opacity: 1;\n  }\n}\n\n@keyframes zoom-in {\n  from {\n    opacity: 0.5;\n    transform: scale3d(0.4, 0.4, 0.4);\n  }\n  to {\n    opacity: 1;\n  }\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvcGFnZXMvdGFicy90YWJzLnBhZ2Uuc2NzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQTtFQUNJLGtCQUFBO0VBQ0EsbUJBQUE7RUFDQSxnQkFBQTtFQUNBLGlCQUFBO0VBQ0Esa0JBQUE7RUFDQSxtQkFBQTtFQUNBLHFCQUFBO0VBQ0EsYUFBQTtFQUNBLG1GQUFBO0VBQ0EsWUFBQTtBQUNKOztBQUNBO0VBQ0ksbUJBQUE7RUFDQSxnQkFBQTtFQUNBLFlBQUE7RUFDQSw0Q0FBQTtFQUNBLGlCQUFBO0VBQ0EsZ0JBQUE7QUFFSjs7QUFDSTtFQUNJLCtEQUFBO0FBRVI7O0FBQUk7RUFDSSxrQ0FBQTtBQUVSOztBQUdBO0VBQ0ksZUFBQTtFQUNBLGNBQUE7RUFDQSxXQUFBO0VBQ0EsWUFBQTtFQUNBLE1BQUE7RUFDQSxPQUFBO0VBQ0EsUUFBQTtFQUNBLFNBQUE7RUFDQSxvQ0FBQTtFQUNBLFdBQUE7RUFDQSxlQUFBO0FBQUo7O0FBSUU7RUFDRSxrQkFBQTtFQUNBLHdCQUFBO0VBQ0EsVUFBQTtFQUNBLFdBQUE7RUFDQSxXQUFBO0VBQ0EsdUNBQUE7RUFDQSxpQkFBQTtFQUNBLGVBQUE7RUFDQSxrQkFBQTtFQUNBLGdCQUFBO0VBQ0EsV0FBQTtFQUNBLGlCQUFBO0VBQ0EsaUhBQUE7QUFESjs7QUFLRTtFQUNFLGNBQUE7QUFGSjs7QUFNSTtFQUNJLFdBQUE7QUFIUjs7QUFPRTtFQUNFLG1CQUFBO0VBQ0EsK0JBQUE7VUFBQSx1QkFBQTtBQUpKOztBQVNBO0VBQ0Usa0JBQUE7RUFDQSxZQUFBO0VBQ0EsU0FBQTtBQU5GOztBQVFJO0VBQ0kseUJBQUE7RUFDQSw4Q0FBQTtFQUNBLGtCQUFBO0FBTlI7O0FBU0E7RUFFRTtJQUVJLFlBQUE7SUFDQSxpQ0FBQTtFQVJKO0VBV0E7SUFFRyxVQUFBO0VBVkg7QUFDRjs7QUFEQTtFQUVFO0lBRUksWUFBQTtJQUNBLGlDQUFBO0VBUko7RUFXQTtJQUVHLFVBQUE7RUFWSDtBQUNGIiwiZmlsZSI6InNyYy9hcHAvcGFnZXMvdGFicy90YWJzLnBhZ2Uuc2NzcyIsInNvdXJjZXNDb250ZW50IjpbImlvbi10YWItYmFyIHtcbiAgICBwb3NpdGlvbjogcmVsYXRpdmU7XG4gICAgbWFyZ2luLWJvdHRvbTogMTBweDtcbiAgICBtYXJnaW4tdG9wOiAxMHB4O1xuICAgIG1hcmdpbi1sZWZ0OiAxMHB4O1xuICAgIG1hcmdpbi1yaWdodDogMTBweDtcbiAgICBib3JkZXItcmFkaXVzOiAxNXB4O1xuICAgIC0tYmFja2dyb3VuZDogI2U2ZTZlNjtcbiAgICBwYWRkaW5nOiAxMHB4O1xuICAgIGJveC1zaGFkb3c6IDBweCAxcHggM3B4IDBweCByZ2IoMCAwIDAgLyAxMCUpLCAwcHggMXB4IDNweCAwcHggcmdiKDAgMCAwIC8gOCUpO1xuICAgIGJvcmRlcjogbm9uZTtcbn1cbmlvbi10YWItYnV0dG9uIHtcbiAgICBib3JkZXItcmFkaXVzOiAxNXB4O1xuICAgIGJhY2tncm91bmQ6ICNmZmY7XG4gICAgcGFkZGluZzogNXB4O1xuICAgIGJveC1zaGFkb3c6IHJnYigwIDAgMCAvIDEyJSkgMHB4IDRweCAxNnB4O1xuICAgIG1hcmdpbi1yaWdodDogNXB4O1xuICAgIG1hcmdpbi1sZWZ0OiA1cHg7XG59XG5pb24tdGFicyB7XG4gICAgJi5tZW51IHtcbiAgICAgICAgYmFja2dyb3VuZDogbGluZWFyLWdyYWRpZW50KDU2ZGVnLCAjNTAwODE2LCAjNjMwZjFmKSAhaW1wb3J0YW50O1xuICAgIH1cbiAgICAmLnBhZ2Uge1xuICAgICAgICBiYWNrZ3JvdW5kOiB0cmFuc3BhcmVudCAhaW1wb3J0YW50O1xuICAgIH1cbn1cblxuXG4ub3ZlcmxheSB7XG4gICAgcG9zaXRpb246IGZpeGVkO1xuICAgIGRpc3BsYXk6IGJsb2NrO1xuICAgIHdpZHRoOiAxMDAlO1xuICAgIGhlaWdodDogMTAwJTtcbiAgICB0b3A6IDA7XG4gICAgbGVmdDogMDtcbiAgICByaWdodDogMDtcbiAgICBib3R0b206IDA7XG4gICAgYmFja2dyb3VuZC1jb2xvcjogcmdiYSgwLCAwLCAwLCAwLjUpO1xuICAgIHotaW5kZXg6IDk5O1xuICAgIGN1cnNvcjogcG9pbnRlcjtcbiAgfVxuICBcbiAgXG4gIGlvbi1mYWItYnV0dG9uW2RhdGEtZGVzY106OmFmdGVyIHtcbiAgICBwb3NpdGlvbjogYWJzb2x1dGU7XG4gICAgY29udGVudDogYXR0cihkYXRhLWRlc2MpO1xuICAgIHotaW5kZXg6IDE7XG4gICAgcmlnaHQ6IDUwcHg7XG4gICAgYm90dG9tOiA0cHg7XG4gICAgYmFja2dyb3VuZC1jb2xvcjogdmFyKC0taW9uLWNvbG9yLWRhcmspO1xuICAgIHBhZGRpbmc6IDVweCAyMHB4O1xuICAgIGZvbnQtc2l6ZTogMTBweDtcbiAgICBib3JkZXItcmFkaXVzOiA1cHg7XG4gICAgYmFja2dyb3VuZDogI2ZmZjtcbiAgICBjb2xvcjogIzAwMDtcbiAgICBmb250LXdlaWdodDogYm9sZDtcbiAgICBib3gtc2hhZG93OiAwIDNweCA1cHggLTFweCByZ2IoMCAwIDAgLyAyMCUpLCAwIDZweCAxMHB4IDAgcmdiKDAgMCAwIC8gMTQlKSwgMCAxcHggMThweCAwIHJnYigwIDAgMCAvIDEyJSk7XG4gIH1cbiAgXG5cbiAgLnNldHRpbmdfaWNvbiB7XG4gICAgY29sb3I6ICM2MTU4NTg7XG4gIH1cblxuICAuY29tbW9uX2ZhYl9idG4ge1xuICAgIGlvbi1pbWcge1xuICAgICAgICB3aWR0aDogMjBweDtcbiAgICB9XG4gIH1cblxuICBpb24tZmFiLWxpc3Qge1xuICAgIG1hcmdpbi1ib3R0b206IDkwcHg7XG4gICAgYW5pbWF0aW9uOiB6b29tLWluIC40MHM7XG4gIH1cblxuXG5cbmlvbi1mYWIge1xuICBwb3NpdGlvbjogYWJzb2x1dGU7XG4gIGJvdHRvbTogMi41JTtcbiAgcmlnaHQ6IDclO1xuICBcbiAgICBpb24tZmFiLWJ1dHRvbiB7XG4gICAgICAgIC0tYmFja2dyb3VuZDogdHJhbnNwYXJlbnQ7XG4gICAgICAgIC0tYmFja2dyb3VuZC1hY3RpdmF0ZWQ6IHRyYW5zcGFyZW50ICFpbXBvcnRhbnQ7XG4gICAgICAgIC0tYm94LXNoYWRvdzogbm9uZTtcbiAgICB9XG59XG5Aa2V5ZnJhbWVzIHpvb20taW4ge1xuXG4gIGZyb20ge1xuXG4gICAgICBvcGFjaXR5OiAuNTtcbiAgICAgIHRyYW5zZm9ybTogc2NhbGUzZCguNCwgLjQsIC40KTtcbiAgfSBcbiAgXG4gIHRvIHtcblxuICAgICBvcGFjaXR5OiAxO1xuICB9XG5cbn1cblxuXG4vLyBpb24tZmFiIHtcbi8vICAgICBAbWVkaWEgKG1heC13aWR0aDogMTUwcHgpIHtcbi8vICAgICAgIHBvc2l0aW9uOiBhYnNvbHV0ZTtcbi8vICAgICBib3R0b206IDMlO1xuLy8gICAgIHJpZ2h0OiA0LjglO1xuLy8gICAgIH1cbiBcbi8vICB9XG4gIl19 */";
      /***/
    },

    /***/
    "./src/app/pages/tabs/tabs.page.ts":
    /*!*****************************************!*\
      !*** ./src/app/pages/tabs/tabs.page.ts ***!
      \*****************************************/

    /*! exports provided: TabsPage */

    /***/
    function srcAppPagesTabsTabsPageTs(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "TabsPage", function () {
        return TabsPage;
      });
      /* harmony import */


      var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! tslib */
      "./node_modules/tslib/tslib.es6.js");
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! @angular/core */
      "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
      /* harmony import */


      var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! @angular/router */
      "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");
      /* harmony import */


      var _components_modals_before_aliments_before_aliments_component__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
      /*! @components/modals/before-aliments/before-aliments.component */
      "./src/app/components/modals/before-aliments/before-aliments.component.ts");
      /* harmony import */


      var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
      /*! @ionic/angular */
      "./node_modules/@ionic/angular/__ivy_ngcc__/fesm2015/ionic-angular.js");
      /* harmony import */


      var _services_userdata_service__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(
      /*! @services/userdata.service */
      "./src/app/services/userdata.service.ts");

      var TabsPage = /*#__PURE__*/function () {
        function TabsPage(router, userData, modal) {
          _classCallCheck(this, TabsPage);

          this.router = router;
          this.userData = userData;
          this.modal = modal;
          this.active = false;
          this.showBackdrop = false;
        }

        _createClass(TabsPage, [{
          key: "ngAfterViewChecked",
          value: function ngAfterViewChecked() {}
        }, {
          key: "ngAfterViewInit",
          value: function ngAfterViewInit() {}
        }, {
          key: "myMethod1",
          value: function myMethod1() {
            this.userData.changeColor = true; // this.active = true;
          }
        }, {
          key: "myMethod2",
          value: function myMethod2() {
            this.userData.changeColor = false; // this.active = false;
          }
        }, {
          key: "openManualModal",
          value: function openManualModal() {
            return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, /*#__PURE__*/regeneratorRuntime.mark(function _callee() {
              var modal;
              return regeneratorRuntime.wrap(function _callee$(_context) {
                while (1) {
                  switch (_context.prev = _context.next) {
                    case 0:
                      _context.next = 2;
                      return this.modal.create({
                        component: _components_modals_before_aliments_before_aliments_component__WEBPACK_IMPORTED_MODULE_3__["BeforeAlimentsComponent"],
                        cssClass: 'check-up-modal',
                        showBackdrop: false
                      });

                    case 2:
                      modal = _context.sent;
                      _context.next = 5;
                      return modal.present();

                    case 5:
                      return _context.abrupt("return", _context.sent);

                    case 6:
                    case "end":
                      return _context.stop();
                  }
                }
              }, _callee, this);
            }));
          }
        }]);

        return TabsPage;
      }();

      TabsPage.ctorParameters = function () {
        return [{
          type: _angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"]
        }, {
          type: _services_userdata_service__WEBPACK_IMPORTED_MODULE_5__["UserdataService"]
        }, {
          type: _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["ModalController"]
        }];
      };

      TabsPage = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-tabs',
        template: Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(
        /*! raw-loader!./tabs.page.html */
        "./node_modules/raw-loader/dist/cjs.js!./src/app/pages/tabs/tabs.page.html"))["default"],
        styles: [Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(
        /*! ./tabs.page.scss */
        "./src/app/pages/tabs/tabs.page.scss"))["default"]]
      })], TabsPage);
      /***/
    }
  }]);
})();
//# sourceMappingURL=pages-tabs-tabs-module-es5.js.map