(function () {
  function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

  function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

  function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

  (window["webpackJsonp"] = window["webpackJsonp"] || []).push([["pages-tabs-home-conseils-nutritionnels-conseils-nutritionnels-module"], {
    /***/
    "./node_modules/raw-loader/dist/cjs.js!./src/app/pages/tabs/home/conseils-nutritionnels/conseils-nutritionnels.page.html":
    /*!*******************************************************************************************************************************!*\
      !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/pages/tabs/home/conseils-nutritionnels/conseils-nutritionnels.page.html ***!
      \*******************************************************************************************************************************/

    /*! exports provided: default */

    /***/
    function node_modulesRawLoaderDistCjsJsSrcAppPagesTabsHomeConseilsNutritionnelsConseilsNutritionnelsPageHtml(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony default export */


      __webpack_exports__["default"] = "<ion-header>\n    <ion-toolbar>\n        <ion-buttons slot=\"start\">\n            <ion-back-button></ion-back-button>\n        </ion-buttons>\n        <ion-title>CONSEILS NUTRITIONNELS</ion-title>\n    </ion-toolbar>\n</ion-header>\n\n<ion-content>\n    <div class=\"advice-box\">\n        <div class=\"advice-box-group\" routerLink=\"/conseils-nutritionnels-details\">\n            <div class=\"advice-box-group-img\"><img src=\"assets/images/Picture2.png\"></div>\n            <div class=\"advice-box-group-text\">\n                <h4>Le gingembre\n                </h4>\n                <p>et son influence sur la santé globale des individus\n                </p>\n            </div>\n        </div>\n        <div class=\"advice-box-group\">\n            <div class=\"advice-box-group-img\"><img src=\"assets/images/Picture3.png\"></div>\n            <div class=\"advice-box-group-text\">\n                <h4>Le gingembre\n                </h4>\n                <p>et son influence sur la santé globale des individus\n                </p>\n            </div>\n        </div>\n        <div class=\"advice-box-group\">\n            <div class=\"advice-box-group-img\"><img src=\"assets/images/Picture4.png\"></div>\n            <div class=\"advice-box-group-text\">\n                <h4>Le gingembre\n                </h4>\n                <p>et son influence sur la santé globale des individus\n                </p>\n            </div>\n        </div>\n        <div class=\"advice-box-group\">\n            <div class=\"advice-box-group-img\"><img src=\"assets/images/Picture2.png\"></div>\n            <div class=\"advice-box-group-text\">\n                <h4>Le gingembre\n                </h4>\n                <p>et son influence sur la santé globale des individus\n                </p>\n            </div>\n        </div>\n    </div>\n</ion-content>";
      /***/
    },

    /***/
    "./src/app/pages/tabs/home/conseils-nutritionnels/conseils-nutritionnels-routing.module.ts":
    /*!*************************************************************************************************!*\
      !*** ./src/app/pages/tabs/home/conseils-nutritionnels/conseils-nutritionnels-routing.module.ts ***!
      \*************************************************************************************************/

    /*! exports provided: ConseilsNutritionnelsPageRoutingModule */

    /***/
    function srcAppPagesTabsHomeConseilsNutritionnelsConseilsNutritionnelsRoutingModuleTs(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "ConseilsNutritionnelsPageRoutingModule", function () {
        return ConseilsNutritionnelsPageRoutingModule;
      });
      /* harmony import */


      var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! tslib */
      "./node_modules/tslib/tslib.es6.js");
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! @angular/core */
      "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
      /* harmony import */


      var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! @angular/router */
      "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");
      /* harmony import */


      var _conseils_nutritionnels_page__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
      /*! ./conseils-nutritionnels.page */
      "./src/app/pages/tabs/home/conseils-nutritionnels/conseils-nutritionnels.page.ts");

      var routes = [{
        path: '',
        component: _conseils_nutritionnels_page__WEBPACK_IMPORTED_MODULE_3__["ConseilsNutritionnelsPage"]
      }];

      var ConseilsNutritionnelsPageRoutingModule = function ConseilsNutritionnelsPageRoutingModule() {
        _classCallCheck(this, ConseilsNutritionnelsPageRoutingModule);
      };

      ConseilsNutritionnelsPageRoutingModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]]
      })], ConseilsNutritionnelsPageRoutingModule);
      /***/
    },

    /***/
    "./src/app/pages/tabs/home/conseils-nutritionnels/conseils-nutritionnels.module.ts":
    /*!*****************************************************************************************!*\
      !*** ./src/app/pages/tabs/home/conseils-nutritionnels/conseils-nutritionnels.module.ts ***!
      \*****************************************************************************************/

    /*! exports provided: ConseilsNutritionnelsPageModule */

    /***/
    function srcAppPagesTabsHomeConseilsNutritionnelsConseilsNutritionnelsModuleTs(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "ConseilsNutritionnelsPageModule", function () {
        return ConseilsNutritionnelsPageModule;
      });
      /* harmony import */


      var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! tslib */
      "./node_modules/tslib/tslib.es6.js");
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! @angular/core */
      "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
      /* harmony import */


      var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! @angular/common */
      "./node_modules/@angular/common/__ivy_ngcc__/fesm2015/common.js");
      /* harmony import */


      var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
      /*! @angular/forms */
      "./node_modules/@angular/forms/__ivy_ngcc__/fesm2015/forms.js");
      /* harmony import */


      var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
      /*! @ionic/angular */
      "./node_modules/@ionic/angular/__ivy_ngcc__/fesm2015/ionic-angular.js");
      /* harmony import */


      var _conseils_nutritionnels_routing_module__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(
      /*! ./conseils-nutritionnels-routing.module */
      "./src/app/pages/tabs/home/conseils-nutritionnels/conseils-nutritionnels-routing.module.ts");
      /* harmony import */


      var _conseils_nutritionnels_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(
      /*! ./conseils-nutritionnels.page */
      "./src/app/pages/tabs/home/conseils-nutritionnels/conseils-nutritionnels.page.ts");

      var ConseilsNutritionnelsPageModule = function ConseilsNutritionnelsPageModule() {
        _classCallCheck(this, ConseilsNutritionnelsPageModule);
      };

      ConseilsNutritionnelsPageModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"], _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"], _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonicModule"], _conseils_nutritionnels_routing_module__WEBPACK_IMPORTED_MODULE_5__["ConseilsNutritionnelsPageRoutingModule"]],
        declarations: [_conseils_nutritionnels_page__WEBPACK_IMPORTED_MODULE_6__["ConseilsNutritionnelsPage"]]
      })], ConseilsNutritionnelsPageModule);
      /***/
    },

    /***/
    "./src/app/pages/tabs/home/conseils-nutritionnels/conseils-nutritionnels.page.scss":
    /*!*****************************************************************************************!*\
      !*** ./src/app/pages/tabs/home/conseils-nutritionnels/conseils-nutritionnels.page.scss ***!
      \*****************************************************************************************/

    /*! exports provided: default */

    /***/
    function srcAppPagesTabsHomeConseilsNutritionnelsConseilsNutritionnelsPageScss(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony default export */


      __webpack_exports__["default"] = ".advice-box-group {\n  position: relative;\n  color: #fff;\n  height: 200px;\n  margin-bottom: 5px;\n}\n\n.advice-box-group-text {\n  position: absolute;\n  bottom: 10%;\n  left: 5%;\n  width: 100%;\n}\n\n.advice-box-group-text p {\n  margin: 0;\n}\n\n.advice-box-group-img img {\n  height: 200px;\n  -o-object-fit: cover;\n     object-fit: cover;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvcGFnZXMvdGFicy9ob21lL2NvbnNlaWxzLW51dHJpdGlvbm5lbHMvY29uc2VpbHMtbnV0cml0aW9ubmVscy5wYWdlLnNjc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUE7RUFDSSxrQkFBQTtFQUNBLFdBQUE7RUFDQSxhQUFBO0VBQ0Esa0JBQUE7QUFDSjs7QUFFQTtFQUNJLGtCQUFBO0VBQ0EsV0FBQTtFQUNBLFFBQUE7RUFDQSxXQUFBO0FBQ0o7O0FBRUE7RUFDSSxTQUFBO0FBQ0o7O0FBRUE7RUFDSSxhQUFBO0VBQ0Esb0JBQUE7S0FBQSxpQkFBQTtBQUNKIiwiZmlsZSI6InNyYy9hcHAvcGFnZXMvdGFicy9ob21lL2NvbnNlaWxzLW51dHJpdGlvbm5lbHMvY29uc2VpbHMtbnV0cml0aW9ubmVscy5wYWdlLnNjc3MiLCJzb3VyY2VzQ29udGVudCI6WyIuYWR2aWNlLWJveC1ncm91cCB7XG4gICAgcG9zaXRpb246IHJlbGF0aXZlO1xuICAgIGNvbG9yOiAjZmZmO1xuICAgIGhlaWdodDogMjAwcHg7XG4gICAgbWFyZ2luLWJvdHRvbTogNXB4O1xufVxuXG4uYWR2aWNlLWJveC1ncm91cC10ZXh0IHtcbiAgICBwb3NpdGlvbjogYWJzb2x1dGU7XG4gICAgYm90dG9tOiAxMCU7XG4gICAgbGVmdDogNSU7XG4gICAgd2lkdGg6IDEwMCU7XG59XG5cbi5hZHZpY2UtYm94LWdyb3VwLXRleHQgcCB7XG4gICAgbWFyZ2luOiAwO1xufVxuXG4uYWR2aWNlLWJveC1ncm91cC1pbWcgaW1nIHtcbiAgICBoZWlnaHQ6IDIwMHB4O1xuICAgIG9iamVjdC1maXQ6IGNvdmVyO1xufSJdfQ== */";
      /***/
    },

    /***/
    "./src/app/pages/tabs/home/conseils-nutritionnels/conseils-nutritionnels.page.ts":
    /*!***************************************************************************************!*\
      !*** ./src/app/pages/tabs/home/conseils-nutritionnels/conseils-nutritionnels.page.ts ***!
      \***************************************************************************************/

    /*! exports provided: ConseilsNutritionnelsPage */

    /***/
    function srcAppPagesTabsHomeConseilsNutritionnelsConseilsNutritionnelsPageTs(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "ConseilsNutritionnelsPage", function () {
        return ConseilsNutritionnelsPage;
      });
      /* harmony import */


      var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! tslib */
      "./node_modules/tslib/tslib.es6.js");
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! @angular/core */
      "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");

      var ConseilsNutritionnelsPage = /*#__PURE__*/function () {
        function ConseilsNutritionnelsPage() {
          _classCallCheck(this, ConseilsNutritionnelsPage);
        }

        _createClass(ConseilsNutritionnelsPage, [{
          key: "ngOnInit",
          value: function ngOnInit() {}
        }]);

        return ConseilsNutritionnelsPage;
      }();

      ConseilsNutritionnelsPage.ctorParameters = function () {
        return [];
      };

      ConseilsNutritionnelsPage = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-conseils-nutritionnels',
        template: Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(
        /*! raw-loader!./conseils-nutritionnels.page.html */
        "./node_modules/raw-loader/dist/cjs.js!./src/app/pages/tabs/home/conseils-nutritionnels/conseils-nutritionnels.page.html"))["default"],
        styles: [Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(
        /*! ./conseils-nutritionnels.page.scss */
        "./src/app/pages/tabs/home/conseils-nutritionnels/conseils-nutritionnels.page.scss"))["default"]]
      })], ConseilsNutritionnelsPage);
      /***/
    }
  }]);
})();
//# sourceMappingURL=pages-tabs-home-conseils-nutritionnels-conseils-nutritionnels-module-es5.js.map