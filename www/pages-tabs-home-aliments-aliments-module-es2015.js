(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["pages-tabs-home-aliments-aliments-module"],{

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/pages/tabs/home/aliments/aliments.page.html":
/*!***************************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/pages/tabs/home/aliments/aliments.page.html ***!
  \***************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<ion-header>\n    <ion-toolbar class=\"ion-text-center\">\n        <ion-title>AJOUT D’ALIMENTS </ion-title>\n    </ion-toolbar>\n</ion-header>\n\n<ion-content>\n\n    <ion-segment value=\"aliments\" color=\"primary\" mode=\"android\" [(ngModel)]=\"segmentModel\" (ionChange)=\"segmentChanged($event)\">\n\n        <ion-segment-button value=\"aliments\">\n            <ion-label>ALIMENTS</ion-label>\n        </ion-segment-button>\n\n        <ion-segment-button value=\"recettes\">\n            <ion-label>RECETTES</ion-label>\n        </ion-segment-button>\n\n        <ion-segment-button value=\"favoris\">\n            <ion-label>FAVORIS </ion-label>\n        </ion-segment-button>\n\n    </ion-segment>\n    <div class=\"aliments\" *ngIf=\"segmentModel === 'aliments'\">\n        <ion-row>\n            <ion-col size=\"12\">\n                <ion-item class=\"search\">\n                    <ion-input type=\"password\" placeholder=\"Rechercher dans la base de données\"></ion-input>\n                    <ion-icon clear name=\"barcode-outline\"></ion-icon>\n                </ion-item>\n                <ion-item (click)=\"openModal()\">\n                    <ion-label>Filtrer</ion-label>\n                  \n                   \n                    <ion-icon name=\"chevron-down-outline\" style=\"color: #000;\"></ion-icon>\n                    <!-- <ion-select value=\"notifications\" interface=\"action-sheet\">\n                        <ion-select-option value=\"enable\">Tout</ion-select-option>\n                    </ion-select> -->\n                </ion-item>\n                <ion-item routerLink=\"/aliments-details\">\n                    <ion-avatar slot=\"start\">\n                        <img src=\"data:image/gif;base64,R0lGODlhAQABAIAAAAAAAAAAACH5BAAAAAAALAAAAAABAAEAAAICTAEAOw==\">\n                    </ion-avatar>\n                    <ion-label>Beurre de cacahuète, 35g </ion-label>\n                    <ion-note slot=\"end\" color=\"secondary\">44</ion-note>\n                    <ion-icon slot=\"end\" name=\"add-circle-outline\" ></ion-icon>\n                </ion-item>\n                <ion-item routerLink=\"/aliments-details\">\n                    <ion-avatar slot=\"start\">\n                        <img src=\"data:image/gif;base64,R0lGODlhAQABAIAAAAAAAAAAACH5BAAAAAAALAAAAAABAAEAAAICTAEAOw==\">\n                    </ion-avatar>\n                    <ion-label>Ketchup, 5g</ion-label>\n                    <ion-note slot=\"end\" color=\"secondary\">17</ion-note>\n                    <ion-icon slot=\"end\" name=\"add-circle-outline\" ></ion-icon>\n                </ion-item>\n                <ion-item routerLink=\"/aliments-details\">\n                    <ion-avatar slot=\"start\">\n                        <img src=\"data:image/gif;base64,R0lGODlhAQABAIAAAAAAAAAAACH5BAAAAAAALAAAAAABAAEAAAICTAEAOw==\">\n                    </ion-avatar>\n                    <ion-label>Blancs de dinde, 250g </ion-label>\n                    <ion-note slot=\"end\" color=\"secondary\">320</ion-note>\n                    <ion-icon slot=\"end\" name=\"add-circle-outline\" ></ion-icon>\n                </ion-item>\n                <!-- <ion-item>\n                    <ion-avatar slot=\"start\">\n                        <img src=\"data:image/gif;base64,R0lGODlhAQABAIAAAAAAAAAAACH5BAAAAAAALAAAAAABAAEAAAICTAEAOw==\">\n                    </ion-avatar>\n                    <ion-label>Beurre de cacahuète, 35g </ion-label>\n                    <ion-note slot=\"end\" color=\"secondary\">44</ion-note>\n                    <ion-icon slot=\"end\" name=\"add-circle-outline\"></ion-icon>\n                </ion-item>\n                <ion-item>\n                    <ion-avatar slot=\"start\">\n                        <img src=\"data:image/gif;base64,R0lGODlhAQABAIAAAAAAAAAAACH5BAAAAAAALAAAAAABAAEAAAICTAEAOw==\">\n                    </ion-avatar>\n                    <ion-label>Ketchup, 5g</ion-label>\n                    <ion-note slot=\"end\" color=\"secondary\">17</ion-note>\n                    <ion-icon slot=\"end\" name=\"add-circle-outline\"></ion-icon>\n                </ion-item>\n                <ion-item>\n                    <ion-avatar slot=\"start\">\n                        <img src=\"data:image/gif;base64,R0lGODlhAQABAIAAAAAAAAAAACH5BAAAAAAALAAAAAABAAEAAAICTAEAOw==\">\n                    </ion-avatar>\n                    <ion-label>Blancs de dinde, 250g </ion-label>\n                    <ion-note slot=\"end\" color=\"secondary\">320</ion-note>\n                    <ion-icon slot=\"end\" name=\"add-circle-outline\"></ion-icon>\n                </ion-item> -->\n\n            </ion-col>\n        </ion-row>\n\n\n\n        <ion-row *ngIf=\"showMore\">\n            <ion-col size=\"12\">\n      \n                <ion-item routerLink=\"/aliments-details\">\n                    <ion-avatar slot=\"start\">\n                        <img src=\"data:image/gif;base64,R0lGODlhAQABAIAAAAAAAAAAACH5BAAAAAAALAAAAAABAAEAAAICTAEAOw==\">\n                    </ion-avatar>\n                    <ion-label>Beurre de cacahuète, 35g </ion-label>\n                    <ion-note slot=\"end\" color=\"secondary\">44</ion-note>\n                    <ion-icon slot=\"end\" name=\"add-circle-outline\" ></ion-icon>\n                </ion-item>\n                <ion-item routerLink=\"/aliments-details\">\n                    <ion-avatar slot=\"start\">\n                        <img src=\"data:image/gif;base64,R0lGODlhAQABAIAAAAAAAAAAACH5BAAAAAAALAAAAAABAAEAAAICTAEAOw==\">\n                    </ion-avatar>\n                    <ion-label>Ketchup, 5g</ion-label>\n                    <ion-note slot=\"end\" color=\"secondary\">17</ion-note>\n                    <ion-icon slot=\"end\" name=\"add-circle-outline\" ></ion-icon>\n                </ion-item>\n                <ion-item routerLink=\"/aliments-details\">\n                    <ion-avatar slot=\"start\">\n                        <img src=\"data:image/gif;base64,R0lGODlhAQABAIAAAAAAAAAAACH5BAAAAAAALAAAAAABAAEAAAICTAEAOw==\">\n                    </ion-avatar>\n                    <ion-label>Blancs de dinde, 250g </ion-label>\n                    <ion-note slot=\"end\" color=\"secondary\">320</ion-note>\n                    <ion-icon slot=\"end\" name=\"add-circle-outline\" ></ion-icon>\n                </ion-item>\n     \n\n            </ion-col>\n        </ion-row>\n        <ion-row>\n            <div class=\"viewall\">\n                <ion-button (click)=\"showMore=!showMore\">{{showMore ? 'VOIR TOUS LES ALIMENTS' : 'VOIR MOINS DE NOURRITURES'}}\n                    <ion-icon name=\"chevron-forward-outline\"></ion-icon>\n                </ion-button>\n            </div>\n        </ion-row>\n        <ion-grid>\n            <ion-row>\n                <ion-col size=\"12\">\n                    <ion-card class=\"card-box ion-no-margin\">\n                        <ion-item>\n                            <img class=\"logo\" style=\"width: 40px;\" slot=\"start\" src=\"assets/icon/traficationlogo.png\">\n                            <ion-label>NOTE SUR LES ALIMENTS</ion-label>\n                        </ion-item>\n\n                        <ion-card-content>\n                            Les aliments que vous utilisez le plus fréquemment apparaissent systématiquement en tête de liste lorsque vous recherchez un aliment en particulier.\n                        </ion-card-content>\n                        <div class=\"ribbon-wrapper-1\">\n                            <div class=\"ribbon-1\">Ribbon</div>\n                        </div>\n                    </ion-card>\n                </ion-col>\n            </ion-row>\n        </ion-grid>\n    </div>\n\n    <div class=\"aliments\" *ngIf=\"segmentModel === 'recettes'\">\n        <ion-row>\n            <ion-col size=\"12\">\n                <ion-item class=\"search\">\n                    <ion-input type=\"password\" placeholder=\"Rechercher dans la base de données\"></ion-input>\n                    <ion-icon clear name=\"barcode-outline\"></ion-icon>\n                </ion-item>\n                <ion-item  (click)=\"openModal()\">\n                    <ion-label>Filtrer</ion-label>\n                   \n                    <ion-icon name=\"chevron-down-outline\" style=\"color: #000;\"></ion-icon>\n                </ion-item>\n                <ion-item >\n                    <ion-avatar slot=\"start\">\n                        <img src=\"data:image/gif;base64,R0lGODlhAQABAIAAAAAAAAAAACH5BAAAAAAALAAAAAABAAEAAAICTAEAOw==\">\n                    </ion-avatar>\n                    <ion-label>Bœuf Bourguignon</ion-label>\n                    <ion-note slot=\"end\" color=\"secondary\">255</ion-note>\n                    <ion-icon slot=\"end\" name=\"add-circle-outline\"></ion-icon>\n                </ion-item>\n                <ion-item>\n                    <ion-avatar slot=\"start\">\n                        <img src=\"data:image/gif;base64,R0lGODlhAQABAIAAAAAAAAAAACH5BAAAAAAALAAAAAABAAEAAAICTAEAOw==\">\n                    </ion-avatar>\n                    <ion-label>Poulet au curry</ion-label>\n                    <ion-note slot=\"end\" color=\"secondary\">179</ion-note>\n                    <ion-icon slot=\"end\" name=\"add-circle-outline\"></ion-icon>\n                </ion-item>\n                <ion-item>\n                    <ion-avatar slot=\"start\">\n                        <img src=\"data:image/gif;base64,R0lGODlhAQABAIAAAAAAAAAAACH5BAAAAAAALAAAAAABAAEAAAICTAEAOw==\">\n                    </ion-avatar>\n                    <ion-label>Tajine</ion-label>\n                    <ion-note slot=\"end\" color=\"secondary\">395</ion-note>\n                    <ion-icon slot=\"end\" name=\"add-circle-outline\"></ion-icon>\n                </ion-item>\n\n            </ion-col>\n        </ion-row>\n        <ion-row>\n            <div class=\"viewall\">\n                <ion-button>VOIR TOTES LES RECETTES\n                    <ion-icon name=\"chevron-forward-outline\"></ion-icon>\n                </ion-button>\n            </div>\n        </ion-row>\n        <ion-grid>\n            <ion-row>\n                <ion-col size=\"12\">\n                    <ion-card class=\"card-box ion-no-margin\">\n                        <ion-item>\n                            <img class=\"logo\" style=\"width: 40px;\" slot=\"start\" src=\"assets/icon/traficationlogo.png\">\n                            <ion-label>NOTE SUR LES ALIMENTS</ion-label>\n                        </ion-item>\n\n                        <ion-card-content>\n                            Vous avez toujours la possibilité d’élaborer vous-même une recette, pour mieux la partager ensuite avec toute la communauté Tranformatics. Pour créer une recette, il vous suffit de cliquer sur le bouton ci-dessous.\n                        </ion-card-content>\n                        <div class=\"ribbon-wrapper-1\">\n                            <div class=\"ribbon-1\">Ribbon</div>\n                        </div>\n                    </ion-card>\n                </ion-col>\n            </ion-row>\n\n\n    <div class=\"ion-padding ion-no-border\" class=\"recettes_btn\">\n        <ion-button routerLink=\"/recipe-details\">\n            Créer une recette\n\n        </ion-button>\n    </div>\n  \n        </ion-grid>\n    </div>\n\n    <ion-card *ngIf=\"segmentModel === 'favoris'\">\n        <ion-card-header>\n            <ion-card-subtitle>Segment Content</ion-card-subtitle>\n            <ion-card-title>Profile</ion-card-title>\n        </ion-card-header>\n\n        <ion-card-content>\n            Keep close to Nature's heart... and break clear away, once in awhile, and climb a mountain or spend a week in the woods. Wash your spirit clean.\n        </ion-card-content>\n    </ion-card>\n\n\n</ion-content>");

/***/ }),

/***/ "./src/app/pages/tabs/home/aliments/aliments-routing.module.ts":
/*!*********************************************************************!*\
  !*** ./src/app/pages/tabs/home/aliments/aliments-routing.module.ts ***!
  \*********************************************************************/
/*! exports provided: AlimentsPageRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AlimentsPageRoutingModule", function() { return AlimentsPageRoutingModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");
/* harmony import */ var _aliments_page__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./aliments.page */ "./src/app/pages/tabs/home/aliments/aliments.page.ts");




const routes = [
    {
        path: '',
        component: _aliments_page__WEBPACK_IMPORTED_MODULE_3__["AlimentsPage"]
    }
];
let AlimentsPageRoutingModule = class AlimentsPageRoutingModule {
};
AlimentsPageRoutingModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]],
    })
], AlimentsPageRoutingModule);



/***/ }),

/***/ "./src/app/pages/tabs/home/aliments/aliments.module.ts":
/*!*************************************************************!*\
  !*** ./src/app/pages/tabs/home/aliments/aliments.module.ts ***!
  \*************************************************************/
/*! exports provided: AlimentsPageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AlimentsPageModule", function() { return AlimentsPageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/__ivy_ngcc__/fesm2015/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/__ivy_ngcc__/fesm2015/forms.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/__ivy_ngcc__/fesm2015/ionic-angular.js");
/* harmony import */ var _aliments_routing_module__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./aliments-routing.module */ "./src/app/pages/tabs/home/aliments/aliments-routing.module.ts");
/* harmony import */ var _aliments_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./aliments.page */ "./src/app/pages/tabs/home/aliments/aliments.page.ts");







let AlimentsPageModule = class AlimentsPageModule {
};
AlimentsPageModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [
            _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonicModule"],
            _aliments_routing_module__WEBPACK_IMPORTED_MODULE_5__["AlimentsPageRoutingModule"]
        ],
        declarations: [_aliments_page__WEBPACK_IMPORTED_MODULE_6__["AlimentsPage"]]
    })
], AlimentsPageModule);



/***/ }),

/***/ "./src/app/pages/tabs/home/aliments/aliments.page.scss":
/*!*************************************************************!*\
  !*** ./src/app/pages/tabs/home/aliments/aliments.page.scss ***!
  \*************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("ion-item {\n  --inner-border-width: 0 0 0px 0;\n  --border-width: 0 0 0px 0;\n}\n\n.search {\n  --background: #f6f6f6;\n  border-radius: 10px;\n  margin-top: 15px;\n}\n\n.viewall {\n  width: 100%;\n  position: relative;\n  margin-top: 15px;\n  margin-bottom: 15px;\n}\n\n.viewall ion-button {\n  --border-radius: 25px;\n  display: table;\n  margin: auto;\n  font-size: 12px;\n}\n\n.viewall:before {\n  content: \"\";\n  position: absolute;\n  background: #c1c1c1;\n  height: 1px;\n  width: 100%;\n  top: 19px;\n  left: 0;\n}\n\n.card-box {\n  box-shadow: rgba(0, 0, 0, 0.12) 0px 4px 16px;\n  padding: 5px;\n}\n\n.card-box ion-item {\n  --inner-border-width: 0 0 0px 0;\n}\n\n.aliments ion-icon {\n  color: #f4b183;\n}\n\n.logo {\n  width: 40px;\n  border: 1px solid #000;\n  border-radius: 25px;\n  height: 40px;\n  padding: 0px;\n}\n\n.ribbon-wrapper-1 {\n  width: 51px;\n  height: 65px;\n  overflow: hidden;\n  position: absolute;\n  top: 0px;\n  right: 0px;\n}\n\n.ribbon-1 {\n  font: bold 15px Sans-Serif;\n  line-height: 18px;\n  color: #333;\n  text-align: center;\n  text-transform: uppercase;\n  -webkit-transform: rotate(47deg);\n  -moz-transform: rotate(45deg);\n  -ms-transform: rotate(45deg);\n  -o-transform: rotate(45deg);\n  position: relative;\n  padding: 7px 0;\n  left: 0px;\n  top: 16px;\n  height: 44px;\n  width: 150px;\n  background: var(--ion-btn-custom-color);\n  color: #fff;\n  box-shadow: 0 4px 6px rgba(0, 0, 0, 0.1);\n  letter-spacing: 0.5px;\n  z-index: 1;\n}\n\n.recettes_btn {\n  padding: 6px;\n  margin-top: 5%;\n}\n\n.recettes_btn ion-button {\n  width: 100%;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvcGFnZXMvdGFicy9ob21lL2FsaW1lbnRzL2FsaW1lbnRzLnBhZ2Uuc2NzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQTtFQUNJLCtCQUFBO0VBQ0EseUJBQUE7QUFDSjs7QUFFQTtFQUNJLHFCQUFBO0VBQ0EsbUJBQUE7RUFDQSxnQkFBQTtBQUNKOztBQUVBO0VBQ0ksV0FBQTtFQUNBLGtCQUFBO0VBQ0EsZ0JBQUE7RUFDQSxtQkFBQTtBQUNKOztBQUVBO0VBQ0kscUJBQUE7RUFDQSxjQUFBO0VBQ0EsWUFBQTtFQUNBLGVBQUE7QUFDSjs7QUFFQTtFQUNJLFdBQUE7RUFDQSxrQkFBQTtFQUNBLG1CQUFBO0VBQ0EsV0FBQTtFQUNBLFdBQUE7RUFDQSxTQUFBO0VBQ0EsT0FBQTtBQUNKOztBQWFBO0VBQ0ksNENBQUE7RUFDQSxZQUFBO0FBVko7O0FBYUE7RUFDSSwrQkFBQTtBQVZKOztBQWFBO0VBQ0ksY0FBQTtBQVZKOztBQWFBO0VBQ0ksV0FBQTtFQUNBLHNCQUFBO0VBQ0EsbUJBQUE7RUFDQSxZQUFBO0VBQ0EsWUFBQTtBQVZKOztBQWFBO0VBQ0ksV0FBQTtFQUNBLFlBQUE7RUFDQSxnQkFBQTtFQUNBLGtCQUFBO0VBQ0EsUUFBQTtFQUNBLFVBQUE7QUFWSjs7QUFhQTtFQUNJLDBCQUFBO0VBQ0EsaUJBQUE7RUFDQSxXQUFBO0VBQ0Esa0JBQUE7RUFDQSx5QkFBQTtFQUNBLGdDQUFBO0VBQ0EsNkJBQUE7RUFDQSw0QkFBQTtFQUNBLDJCQUFBO0VBQ0Esa0JBQUE7RUFDQSxjQUFBO0VBQ0EsU0FBQTtFQUNBLFNBQUE7RUFDQSxZQUFBO0VBQ0EsWUFBQTtFQUNBLHVDQUFBO0VBQ0EsV0FBQTtFQUNBLHdDQUFBO0VBQ0EscUJBQUE7RUFDQSxVQUFBO0FBVko7O0FBYUE7RUFDSSxZQUFBO0VBQ0EsY0FBQTtBQVZKOztBQVlJO0VBQ0ksV0FBQTtBQVZSIiwiZmlsZSI6InNyYy9hcHAvcGFnZXMvdGFicy9ob21lL2FsaW1lbnRzL2FsaW1lbnRzLnBhZ2Uuc2NzcyIsInNvdXJjZXNDb250ZW50IjpbImlvbi1pdGVtIHtcbiAgICAtLWlubmVyLWJvcmRlci13aWR0aDogMCAwIDBweCAwO1xuICAgIC0tYm9yZGVyLXdpZHRoOiAwIDAgMHB4IDA7XG59XG5cbi5zZWFyY2gge1xuICAgIC0tYmFja2dyb3VuZDogI2Y2ZjZmNjtcbiAgICBib3JkZXItcmFkaXVzOiAxMHB4O1xuICAgIG1hcmdpbi10b3A6IDE1cHg7XG59XG5cbi52aWV3YWxsIHtcbiAgICB3aWR0aDogMTAwJTtcbiAgICBwb3NpdGlvbjogcmVsYXRpdmU7XG4gICAgbWFyZ2luLXRvcDogMTVweDtcbiAgICBtYXJnaW4tYm90dG9tOiAxNXB4O1xufVxuXG4udmlld2FsbCBpb24tYnV0dG9uIHtcbiAgICAtLWJvcmRlci1yYWRpdXM6IDI1cHg7XG4gICAgZGlzcGxheTogdGFibGU7XG4gICAgbWFyZ2luOiBhdXRvO1xuICAgIGZvbnQtc2l6ZTogMTJweDtcbn1cblxuLnZpZXdhbGw6YmVmb3JlIHtcbiAgICBjb250ZW50OiBcIlwiO1xuICAgIHBvc2l0aW9uOiBhYnNvbHV0ZTtcbiAgICBiYWNrZ3JvdW5kOiAjYzFjMWMxO1xuICAgIGhlaWdodDogMXB4O1xuICAgIHdpZHRoOiAxMDAlO1xuICAgIHRvcDogMTlweDtcbiAgICBsZWZ0OiAwO1xufVxuXG4vLyAuY2FyZC1ib3g6YmVmb3JlIHtcbi8vICAgICBjb250ZW50OiBcIlwiO1xuLy8gICAgIGJvcmRlci1yaWdodDogMjBweCBzb2xpZCAjRjRCMTgzO1xuLy8gICAgIGJvcmRlci10b3A6IDIwcHggc29saWQgI0Y0QjE4Mztcbi8vICAgICBib3JkZXItbGVmdDogMjBweCBzb2xpZCAjZmYwMDAwMDA7XG4vLyAgICAgYm9yZGVyLWJvdHRvbTogMjBweCBzb2xpZCAjZmYwMDAwMDA7XG4vLyAgICAgcG9zaXRpb246IGFic29sdXRlO1xuLy8gICAgIHRvcDogMDtcbi8vICAgICByaWdodDogMDtcbi8vICAgICB6LWluZGV4OiA5OTk7XG4vLyB9XG4uY2FyZC1ib3gge1xuICAgIGJveC1zaGFkb3c6IHJnYigwIDAgMCAvIDEyJSkgMHB4IDRweCAxNnB4O1xuICAgIHBhZGRpbmc6IDVweDtcbn1cblxuLmNhcmQtYm94IGlvbi1pdGVtIHtcbiAgICAtLWlubmVyLWJvcmRlci13aWR0aDogMCAwIDBweCAwO1xufVxuXG4uYWxpbWVudHMgaW9uLWljb24ge1xuICAgIGNvbG9yOiAjZjRiMTgzO1xufVxuXG4ubG9nbyB7XG4gICAgd2lkdGg6IDQwcHg7XG4gICAgYm9yZGVyOiAxcHggc29saWQgIzAwMDtcbiAgICBib3JkZXItcmFkaXVzOiAyNXB4O1xuICAgIGhlaWdodDogNDBweDtcbiAgICBwYWRkaW5nOiAwcHg7XG59XG5cbi5yaWJib24td3JhcHBlci0xIHtcbiAgICB3aWR0aDogNTFweDtcbiAgICBoZWlnaHQ6IDY1cHg7XG4gICAgb3ZlcmZsb3c6IGhpZGRlbjtcbiAgICBwb3NpdGlvbjogYWJzb2x1dGU7XG4gICAgdG9wOiAwcHg7XG4gICAgcmlnaHQ6IDBweDtcbn1cblxuLnJpYmJvbi0xIHtcbiAgICBmb250OiBib2xkIDE1cHggU2Fucy1TZXJpZjtcbiAgICBsaW5lLWhlaWdodDogMThweDtcbiAgICBjb2xvcjogIzMzMztcbiAgICB0ZXh0LWFsaWduOiBjZW50ZXI7XG4gICAgdGV4dC10cmFuc2Zvcm06IHVwcGVyY2FzZTtcbiAgICAtd2Via2l0LXRyYW5zZm9ybTogcm90YXRlKCA0N2RlZyk7XG4gICAgLW1vei10cmFuc2Zvcm06IHJvdGF0ZSg0NWRlZyk7XG4gICAgLW1zLXRyYW5zZm9ybTogcm90YXRlKDQ1ZGVnKTtcbiAgICAtby10cmFuc2Zvcm06IHJvdGF0ZSg0NWRlZyk7XG4gICAgcG9zaXRpb246IHJlbGF0aXZlO1xuICAgIHBhZGRpbmc6IDdweCAwO1xuICAgIGxlZnQ6IDBweDtcbiAgICB0b3A6IDE2cHg7XG4gICAgaGVpZ2h0OiA0NHB4O1xuICAgIHdpZHRoOiAxNTBweDtcbiAgICBiYWNrZ3JvdW5kOiB2YXIoLS1pb24tYnRuLWN1c3RvbS1jb2xvcik7XG4gICAgY29sb3I6ICNmZmY7XG4gICAgYm94LXNoYWRvdzogMCA0cHggNnB4IHJnYigwIDAgMCAvIDEwJSk7XG4gICAgbGV0dGVyLXNwYWNpbmc6IDAuNXB4O1xuICAgIHotaW5kZXg6IDE7XG59XG5cbi5yZWNldHRlc19idG4ge1xuICAgIHBhZGRpbmc6IDZweDtcbiAgICBtYXJnaW4tdG9wOiA1JTtcblxuICAgIGlvbi1idXR0b24ge1xuICAgICAgICB3aWR0aDogMTAwJTtcbiAgICB9XG59Il19 */");

/***/ }),

/***/ "./src/app/pages/tabs/home/aliments/aliments.page.ts":
/*!***********************************************************!*\
  !*** ./src/app/pages/tabs/home/aliments/aliments.page.ts ***!
  \***********************************************************/
/*! exports provided: AlimentsPage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AlimentsPage", function() { return AlimentsPage; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/__ivy_ngcc__/fesm2015/ionic-angular.js");
/* harmony import */ var _components_modals_recipes_filters_recipes_filters_component__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @components/modals/recipes-filters/recipes-filters.component */ "./src/app/components/modals/recipes-filters/recipes-filters.component.ts");




let AlimentsPage = class AlimentsPage {
    constructor(modalCtrl) {
        this.modalCtrl = modalCtrl;
        this.segmentModel = "aliments";
        this.showMore = false;
    }
    ngOnInit() {
    }
    segmentChanged(event) {
        console.log(this.segmentModel);
        console.log(event);
    }
    openModal() {
        return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, function* () {
            const modal = yield this.modalCtrl.create({
                component: _components_modals_recipes_filters_recipes_filters_component__WEBPACK_IMPORTED_MODULE_3__["RecipesFiltersComponent"],
                cssClass: 'aliments-modal',
                showBackdrop: false,
            });
            return yield modal.present();
        });
    }
};
AlimentsPage.ctorParameters = () => [
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["ModalController"] }
];
AlimentsPage = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-aliments',
        template: Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(/*! raw-loader!./aliments.page.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/pages/tabs/home/aliments/aliments.page.html")).default,
        styles: [Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(/*! ./aliments.page.scss */ "./src/app/pages/tabs/home/aliments/aliments.page.scss")).default]
    })
], AlimentsPage);



/***/ })

}]);
//# sourceMappingURL=pages-tabs-home-aliments-aliments-module-es2015.js.map