(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["pages-landing-page-landing-page-module"],{

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/pages/landing-page/landing-page.page.html":
/*!*************************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/pages/landing-page/landing-page.page.html ***!
  \*************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<ion-content>\n  <!-- <select #select (change)=\"SelectLang(select.value)\">\n        <option value=\"fr\">fr</option>\n    <option value=\"en\">En</option>\n</select> -->\n  <div>\n    <!-- <mat-form-field class=\"example-user-input language\" slot=\"secondary\">\n            <mat-select value=\"fr\" (selectionChange)=\"SelectLang($event.value)\">\n               \n                <mat-option value=\"fr\">French</mat-option>\n                <mat-option value=\"en\">English</mat-option>\n            </mat-select>\n        </mat-form-field> -->\n  </div>\n  <div class=\"lending-content-box-\">\n    <div class=\"lending logo \">\n      <img class=\"logo-img \" src=\"assets/icon/Picture1.png \">\n\n    </div>\n    <!-- <div class=\"logo-title \">TRANSFORMATICS\n        </div> -->\n    <div class=\"lending-box\">\n      <div class=\"lending-box-title\">\n        <!-- <h2>{{ 'LENDING_PAGE.title-one' | translate }} <br>{{ 'LENDING_PAGE.title-two' | translate }}\n                </h2> -->\n\n        <h2>\n          <span class=\"gray-text\"> DEVENEZ </span>\n          <br />\n\n          <span class=\"orange-text\">LA PERSONNE</span>\n\n          <br />\n\n          <span class=\"gray-text\"> QUE VOUS</span>\n\n          <br />\n          <span class=\"gray-text\"> AVEZ </span>\n          <br />\n\n          <span class=\"gray-text\"> TOUJOURS </span>\n          <br />\n\n          <span class=\"gray-text\"> VOULU </span>\n          <br />\n\n          <div class=\"orange-text\">ETRE.</div>\n        </h2>\n      </div>\n    </div>\n\n    <div class=\"text-content\">\n      <div class=\"lending-box-content\">\n        <ion-button class=\"logIn\" expand=\"full \" shape=\"round \" (click)=\"goToIdentify()\">\n          S’IDENTIFIER\n        </ion-button>\n      </div>\n      <div class=\"lending-box-content\">\n        <ion-button class=\"sign-Up\" expand=\"full\" shape=\"round\" (click)=\"goToIdentify()\">\n          S’INSCRIRE\n        </ion-button>\n      </div>\n    </div>\n  </div>\n</ion-content>");

/***/ }),

/***/ "./src/app/pages/landing-page/landing-page-routing.module.ts":
/*!*******************************************************************!*\
  !*** ./src/app/pages/landing-page/landing-page-routing.module.ts ***!
  \*******************************************************************/
/*! exports provided: LandingPagePageRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "LandingPagePageRoutingModule", function() { return LandingPagePageRoutingModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");
/* harmony import */ var _landing_page_page__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./landing-page.page */ "./src/app/pages/landing-page/landing-page.page.ts");




const routes = [
    {
        path: '',
        component: _landing_page_page__WEBPACK_IMPORTED_MODULE_3__["LandingPagePage"]
    }
];
let LandingPagePageRoutingModule = class LandingPagePageRoutingModule {
};
LandingPagePageRoutingModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]],
    })
], LandingPagePageRoutingModule);



/***/ }),

/***/ "./src/app/pages/landing-page/landing-page.module.ts":
/*!***********************************************************!*\
  !*** ./src/app/pages/landing-page/landing-page.module.ts ***!
  \***********************************************************/
/*! exports provided: LandingPagePageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "LandingPagePageModule", function() { return LandingPagePageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/__ivy_ngcc__/fesm2015/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/__ivy_ngcc__/fesm2015/forms.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/__ivy_ngcc__/fesm2015/ionic-angular.js");
/* harmony import */ var _landing_page_routing_module__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./landing-page-routing.module */ "./src/app/pages/landing-page/landing-page-routing.module.ts");
/* harmony import */ var _landing_page_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./landing-page.page */ "./src/app/pages/landing-page/landing-page.page.ts");







let LandingPagePageModule = class LandingPagePageModule {
};
LandingPagePageModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [
            _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonicModule"],
            _landing_page_routing_module__WEBPACK_IMPORTED_MODULE_5__["LandingPagePageRoutingModule"]
        ],
        declarations: [_landing_page_page__WEBPACK_IMPORTED_MODULE_6__["LandingPagePage"]]
    })
], LandingPagePageModule);



/***/ }),

/***/ "./src/app/pages/landing-page/landing-page.page.scss":
/*!***********************************************************!*\
  !*** ./src/app/pages/landing-page/landing-page.page.scss ***!
  \***********************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("ion-content {\n  --background: url('landing_page_background_1.png') 0 0/100% no-repeat;\n  background-size: cover;\n  --overflow: hidden;\n}\n\n.lending.logo {\n  position: relative;\n  width: 100%;\n  padding-top: 10%;\n}\n\n.lending-box {\n  position: relative;\n  width: 100%;\n  padding-top: 5%;\n}\n\n.lending-content-box- {\n  position: relative;\n  height: 100%;\n  width: 100%;\n  margin: 0 auto;\n  padding-left: 5%;\n  padding-right: 5%;\n  background: rgba(0, 0, 0, 0.6);\n}\n\n.lending-content-box- h2 {\n  font-family: Oswald-Bold, sans-serif;\n  line-height: 1.3;\n}\n\n.lending-content-box- h2 .gray-text {\n  color: #a4a4a4;\n}\n\n.lending-content-box- h2 .orange-text {\n  color: var(--ion-color-primary);\n}\n\n.logIn {\n  --background: var(--ion-btn-custom-color);\n  color: #000;\n  --border-radius: 5px;\n  margin-bottom: 15px;\n  position: relative;\n  height: 45px;\n  font-family: Oswald, sans-serif;\n}\n\n.sign-Up {\n  --background: transparent;\n  border: 2px solid #fff;\n  border-radius: 5px;\n  position: relative;\n  height: 45px;\n  font-family: Oswald, sans-serif;\n}\n\n.logo-title {\n  color: #fff;\n  text-align: left;\n  font-size: 25px;\n  font-weight: bold;\n  top: 18%;\n  left: 0;\n  position: fixed;\n  z-index: 5;\n  transform: rotate(90deg);\n}\n\n.language {\n  position: absolute;\n  right: 10px;\n  top: 0px;\n  width: 85px;\n  z-index: 5;\n}\n\n.text-content {\n  position: absolute;\n  bottom: 2%;\n  width: 90%;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvcGFnZXMvbGFuZGluZy1wYWdlL2xhbmRpbmctcGFnZS5wYWdlLnNjc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUE7RUFDSSxxRUFBQTtFQUdBLHNCQUFBO0VBQ0Esa0JBQUE7QUFDSjs7QUFFQTtFQUNJLGtCQUFBO0VBRUEsV0FBQTtFQUVBLGdCQUFBO0FBREo7O0FBSUE7RUFDSSxrQkFBQTtFQUNBLFdBQUE7RUFJQSxlQUFBO0FBSko7O0FBT0E7RUFDSSxrQkFBQTtFQUNBLFlBQUE7RUFDQSxXQUFBO0VBQ0EsY0FBQTtFQUNBLGdCQUFBO0VBQ0EsaUJBQUE7RUFDQSw4QkFBQTtBQUpKOztBQUtJO0VBQ0ksb0NBQUE7RUFDQSxnQkFBQTtBQUhSOztBQUlRO0VBQ0ksY0FBQTtBQUZaOztBQUlRO0VBQ0ksK0JBQUE7QUFGWjs7QUFPQTtFQUNJLHlDQUFBO0VBQ0EsV0FBQTtFQUNBLG9CQUFBO0VBQ0EsbUJBQUE7RUFDQSxrQkFBQTtFQUNBLFlBQUE7RUFDQSwrQkFBQTtBQUpKOztBQU9BO0VBQ0kseUJBQUE7RUFDQSxzQkFBQTtFQUNBLGtCQUFBO0VBQ0Esa0JBQUE7RUFDQSxZQUFBO0VBQ0EsK0JBQUE7QUFKSjs7QUFhQTtFQUNJLFdBQUE7RUFDQSxnQkFBQTtFQUNBLGVBQUE7RUFDQSxpQkFBQTtFQUNBLFFBQUE7RUFDQSxPQUFBO0VBQ0EsZUFBQTtFQUNBLFVBQUE7RUFDQSx3QkFBQTtBQVZKOztBQWFBO0VBQ0ksa0JBQUE7RUFDQSxXQUFBO0VBQ0EsUUFBQTtFQUNBLFdBQUE7RUFDQSxVQUFBO0FBVko7O0FBa0RBO0VBQ0ksa0JBQUE7RUFDQSxVQUFBO0VBQ0EsVUFBQTtBQS9DSiIsImZpbGUiOiJzcmMvYXBwL3BhZ2VzL2xhbmRpbmctcGFnZS9sYW5kaW5nLXBhZ2UucGFnZS5zY3NzIiwic291cmNlc0NvbnRlbnQiOlsiaW9uLWNvbnRlbnQge1xuICAgIC0tYmFja2dyb3VuZDogdXJsKC4uLy4uLy4uL2Fzc2V0cy9pbWFnZXMvbGFuZGluZ19wYWdlX2JhY2tncm91bmRfMS5wbmcpIDAgMC8xMDAlIG5vLXJlcGVhdDtcbiAgICAtd2Via2l0LWJhY2tncm91bmQtc2l6ZTogY292ZXI7XG4gICAgLW1vei1iYWNrZ3JvdW5kLXNpemU6IGNvdmVyO1xuICAgIGJhY2tncm91bmQtc2l6ZTogY292ZXI7XG4gICAgLS1vdmVyZmxvdzogaGlkZGVuO1xufVxuXG4ubGVuZGluZy5sb2dvIHtcbiAgICBwb3NpdGlvbjogcmVsYXRpdmU7XG4gICAgLy8gdGV4dC1hbGlnbjogY2VudGVyO1xuICAgIHdpZHRoOiAxMDAlO1xuICAgIC8vIG1hcmdpbjogMCBhdXRvO1xuICAgIHBhZGRpbmctdG9wOiAxMCU7XG59XG5cbi5sZW5kaW5nLWJveCB7XG4gICAgcG9zaXRpb246IHJlbGF0aXZlO1xuICAgIHdpZHRoOiAxMDAlO1xuICAgIC8vIHBvc2l0aW9uOiBhYnNvbHV0ZTtcbiAgICAvLyBib3R0b206IDEwJTtcbiAgICAvLyB3aWR0aDogMTAwJTtcbiAgICBwYWRkaW5nLXRvcDogNSU7XG59XG5cbi5sZW5kaW5nLWNvbnRlbnQtYm94LSB7XG4gICAgcG9zaXRpb246IHJlbGF0aXZlO1xuICAgIGhlaWdodDogMTAwJTtcbiAgICB3aWR0aDogMTAwJTtcbiAgICBtYXJnaW46IDAgYXV0bztcbiAgICBwYWRkaW5nLWxlZnQ6IDUlO1xuICAgIHBhZGRpbmctcmlnaHQ6IDUlO1xuICAgIGJhY2tncm91bmQ6IHJnYmEoMCwgMCwgMCwgLjYpO1xuICAgIGgyIHtcbiAgICAgICAgZm9udC1mYW1pbHk6IE9zd2FsZC1Cb2xkLCBzYW5zLXNlcmlmO1xuICAgICAgICBsaW5lLWhlaWdodDogMS4zO1xuICAgICAgICAuZ3JheS10ZXh0IHtcbiAgICAgICAgICAgIGNvbG9yOiAjYTRhNGE0O1xuICAgICAgICB9XG4gICAgICAgIC5vcmFuZ2UtdGV4dCB7XG4gICAgICAgICAgICBjb2xvcjogdmFyKC0taW9uLWNvbG9yLXByaW1hcnkpO1xuICAgICAgICB9XG4gICAgfVxufVxuXG4ubG9nSW4ge1xuICAgIC0tYmFja2dyb3VuZDogdmFyKC0taW9uLWJ0bi1jdXN0b20tY29sb3IpO1xuICAgIGNvbG9yOiAjMDAwO1xuICAgIC0tYm9yZGVyLXJhZGl1czogNXB4O1xuICAgIG1hcmdpbi1ib3R0b206IDE1cHg7XG4gICAgcG9zaXRpb246IHJlbGF0aXZlO1xuICAgIGhlaWdodDogNDVweDtcbiAgICBmb250LWZhbWlseTogT3N3YWxkLCBzYW5zLXNlcmlmO1xufVxuXG4uc2lnbi1VcCB7XG4gICAgLS1iYWNrZ3JvdW5kOiB0cmFuc3BhcmVudDtcbiAgICBib3JkZXI6IDJweCBzb2xpZCAjZmZmO1xuICAgIGJvcmRlci1yYWRpdXM6IDVweDtcbiAgICBwb3NpdGlvbjogcmVsYXRpdmU7XG4gICAgaGVpZ2h0OiA0NXB4O1xuICAgIGZvbnQtZmFtaWx5OiBPc3dhbGQsIHNhbnMtc2VyaWY7XG59XG5cbi8vIC5sZW5kaW5nLWJveC10aXRsZSB7XG4vLyAgICAgYmFja2dyb3VuZDogbGluZWFyLWdyYWRpZW50KDI5MGRlZywgI2ZmZiwgI2ZmZik7XG4vLyAgICAgLXdlYmtpdC1iYWNrZ3JvdW5kLWNsaXA6IHRleHQ7XG4vLyAgICAgLXdlYmtpdC10ZXh0LWZpbGwtY29sb3I6IHRyYW5zcGFyZW50IWltcG9ydGFudDtcbi8vICAgICBwb3NpdGlvbjogcmVsYXRpdmU7XG4vLyB9XG4ubG9nby10aXRsZSB7XG4gICAgY29sb3I6ICNmZmY7XG4gICAgdGV4dC1hbGlnbjogbGVmdDtcbiAgICBmb250LXNpemU6IDI1cHg7XG4gICAgZm9udC13ZWlnaHQ6IGJvbGQ7XG4gICAgdG9wOiAxOCU7XG4gICAgbGVmdDogMDtcbiAgICBwb3NpdGlvbjogZml4ZWQ7XG4gICAgei1pbmRleDogNTtcbiAgICB0cmFuc2Zvcm06IHJvdGF0ZSg5MGRlZyk7XG59XG5cbi5sYW5ndWFnZSB7XG4gICAgcG9zaXRpb246IGFic29sdXRlO1xuICAgIHJpZ2h0OiAxMHB4O1xuICAgIHRvcDogMHB4O1xuICAgIHdpZHRoOiA4NXB4O1xuICAgIHotaW5kZXg6IDU7XG59XG5cbi8vIDo6bmctZGVlcCAubWF0LXNlbGVjdC1hcnJvdyB7XG4vLyAgICAgY29sb3I6ICNmZmZmZmY7XG4vLyB9XG4vLyA6Om5nLWRlZXAgLm1hdC1mb3JtLWZpZWxkLWFwcGVhcmFuY2UtbGVnYWN5IC5tYXQtZm9ybS1maWVsZC1sYWJlbCB7XG4vLyAgICAgY29sb3I6ICNmZmZmZmYgIWltcG9ydGFudDtcbi8vIH1cbi8vIDo6bmctZGVlcCAubWF0LXNlbGVjdC12YWx1ZSB7XG4vLyAgICAgY29sb3I6ICNmZmZmZmY7XG4vLyB9XG4vLyA6Om5nLWRlZXAgLm1hdC1mb3JtLWZpZWxkLWFwcGVhcmFuY2UtbGVnYWN5IC5tYXQtZm9ybS1maWVsZC11bmRlcmxpbmUge1xuLy8gICAgIGJhY2tncm91bmQtY29sb3I6IHRyYW5zcGFyZW50O1xuLy8gfVxuLy8gOjpuZy1kZWVwIC5tYXQtZm9ybS1maWVsZC5tYXQtZm9jdXNlZCAubWF0LWZvcm0tZmllbGQtcmlwcGxlIHtcbi8vICAgICBkaXNwbGF5OiBub25lO1xuLy8gfVxuLy8gOjpuZy1kZWVwIC5tYXQtZm9ybS1maWVsZC5tYXQtZm9jdXNlZC5tYXQtcHJpbWFyeSAubWF0LXNlbGVjdC1hcnJvdyB7XG4vLyAgICAgY29sb3I6ICMwMDA7XG4vLyB9XG4vLyA6Om5nLWRlZXAgLm1hdC1mb3JtLWZpZWxkLm1hdC1mb2N1c2VkLm1hdC1wcmltYXJ5IC5tYXQtc2VsZWN0LWFycm93IHtcbi8vICAgICBjb2xvcjogI2ZmZjtcbi8vIH1cbi8vIDo6bmctZGVlcCAubWF0LXByaW1hcnkgLm1hdC1vcHRpb24ubWF0LXNlbGVjdGVkOm5vdCgubWF0LW9wdGlvbi1kaXNhYmxlZCkge1xuLy8gICAgIGNvbG9yOiAjMDAwMDAwO1xuLy8gfVxuLy8gOjpuZy1kZWVwIG1hdC1mb3JtLWZpZWxkLm1hdC1mb2N1c2VkLm1hdC1wcmltYXJ5IC5tYXQtc2VsZWN0LWFycm93IHtcbi8vICAgICBjb2xvcjogI2ZmZjtcbi8vIH1cbi8vIC5sZW5kaW5nLWJveDpiZWZvcmUge1xuLy8gICAgIGNvbnRlbnQ6IFwiXCI7XG4vLyAgICAgcG9zaXRpb246IGZpeGVkO1xuLy8gICAgIGhlaWdodDogMTAwJTtcbi8vICAgICB3aWR0aDogMTAwJTtcbi8vICAgICBiYWNrZ3JvdW5kOiBsaW5lYXItZ3JhZGllbnQodHJhbnNwYXJlbnQsICM0YzE4MTEpO1xuLy8gICAgIGxlZnQ6IDBweDtcbi8vICAgICB0b3A6IDA7XG4vLyAgICAgei1pbmRleDogMDtcbi8vIH1cbi50ZXh0LWNvbnRlbnQge1xuICAgIHBvc2l0aW9uOiBhYnNvbHV0ZTtcbiAgICBib3R0b206IDIlO1xuICAgIHdpZHRoOiA5MCU7XG59Il19 */");

/***/ }),

/***/ "./src/app/pages/landing-page/landing-page.page.ts":
/*!*********************************************************!*\
  !*** ./src/app/pages/landing-page/landing-page.page.ts ***!
  \*********************************************************/
/*! exports provided: LandingPagePage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "LandingPagePage", function() { return LandingPagePage; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");



let LandingPagePage = class LandingPagePage {
    constructor(router) {
        this.router = router;
    }
    ngOnInit() {
    }
    goToIdentify() {
        this.router.navigate(['select-roles']);
    }
};
LandingPagePage.ctorParameters = () => [
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"] }
];
LandingPagePage = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-landing-page',
        template: Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(/*! raw-loader!./landing-page.page.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/pages/landing-page/landing-page.page.html")).default,
        styles: [Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(/*! ./landing-page.page.scss */ "./src/app/pages/landing-page/landing-page.page.scss")).default]
    })
], LandingPagePage);



/***/ })

}]);
//# sourceMappingURL=pages-landing-page-landing-page-module-es2015.js.map