(function () {
  function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

  function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

  function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

  (window["webpackJsonp"] = window["webpackJsonp"] || []).push([["pages-tabs-cart-ebook-ebook-module"], {
    /***/
    "./node_modules/raw-loader/dist/cjs.js!./src/app/pages/tabs/cart/ebook/ebook.page.html":
    /*!*********************************************************************************************!*\
      !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/pages/tabs/cart/ebook/ebook.page.html ***!
      \*********************************************************************************************/

    /*! exports provided: default */

    /***/
    function node_modulesRawLoaderDistCjsJsSrcAppPagesTabsCartEbookEbookPageHtml(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony default export */


      __webpack_exports__["default"] = "<ion-content>\n    <div class=\"banner\">\n        <img src=\"assets/images/ebook.png\">\n    </div>\n    <ion-grid>\n        <ion-row>\n            <ion-col size=\"12\">\n                <div class=\"ebook-title\">\n                    <h4>E-BOOKS TRANSFORMATICS</h4>\n                </div>\n            </ion-col>\n            <ion-col size=\"6\">\n                <div class=\"ebook-filter\">\n                    <ion-item class=\"ion-no-padding\">\n                        <ion-label>Filtres</ion-label>\n                        <ion-select value=\"notifications\" interface=\"action-sheet\">\n                            <ion-select-option value=\"enable\">Enable</ion-select-option>\n                            <ion-select-option value=\"mute\">Mute</ion-select-option>\n                            <ion-select-option value=\"mute_week\">Mute for a week</ion-select-option>\n                            <ion-select-option value=\"mute_year\">Mute for a year</ion-select-option>\n                        </ion-select>\n                    </ion-item>\n                </div>\n            </ion-col>\n            <ion-col size=\"6\">\n                <div class=\"ebook-filter\">\n                    <ion-item lass=\"ion-no-padding\">\n                        <ion-label>Classez par</ion-label>\n                        <ion-select value=\"notifications\" interface=\"action-sheet\">\n                            <ion-select-option value=\"enable\">Enable</ion-select-option>\n                            <ion-select-option value=\"mute\">Mute</ion-select-option>\n                            <ion-select-option value=\"mute_week\">Mute for a week</ion-select-option>\n                            <ion-select-option value=\"mute_year\">Mute for a year</ion-select-option>\n                        </ion-select>\n                    </ion-item>\n                </div>\n            </ion-col>\n            <ion-col size=\"12\">\n                <div class=\"ebook-filter-result\">\n                    <h4>32 produits</h4>\n                </div>\n            </ion-col>\n            <div>\n                <app-book></app-book>\n            </div>\n        </ion-row>\n    </ion-grid>\n</ion-content>";
      /***/
    },

    /***/
    "./src/app/pages/tabs/cart/ebook/ebook-routing.module.ts":
    /*!***************************************************************!*\
      !*** ./src/app/pages/tabs/cart/ebook/ebook-routing.module.ts ***!
      \***************************************************************/

    /*! exports provided: EbookPageRoutingModule */

    /***/
    function srcAppPagesTabsCartEbookEbookRoutingModuleTs(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "EbookPageRoutingModule", function () {
        return EbookPageRoutingModule;
      });
      /* harmony import */


      var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! tslib */
      "./node_modules/tslib/tslib.es6.js");
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! @angular/core */
      "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
      /* harmony import */


      var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! @angular/router */
      "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");
      /* harmony import */


      var _ebook_page__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
      /*! ./ebook.page */
      "./src/app/pages/tabs/cart/ebook/ebook.page.ts");

      var routes = [{
        path: '',
        component: _ebook_page__WEBPACK_IMPORTED_MODULE_3__["EbookPage"]
      }];

      var EbookPageRoutingModule = function EbookPageRoutingModule() {
        _classCallCheck(this, EbookPageRoutingModule);
      };

      EbookPageRoutingModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]]
      })], EbookPageRoutingModule);
      /***/
    },

    /***/
    "./src/app/pages/tabs/cart/ebook/ebook.module.ts":
    /*!*******************************************************!*\
      !*** ./src/app/pages/tabs/cart/ebook/ebook.module.ts ***!
      \*******************************************************/

    /*! exports provided: EbookPageModule */

    /***/
    function srcAppPagesTabsCartEbookEbookModuleTs(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "EbookPageModule", function () {
        return EbookPageModule;
      });
      /* harmony import */


      var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! tslib */
      "./node_modules/tslib/tslib.es6.js");
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! @angular/core */
      "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
      /* harmony import */


      var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! @angular/common */
      "./node_modules/@angular/common/__ivy_ngcc__/fesm2015/common.js");
      /* harmony import */


      var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
      /*! @angular/forms */
      "./node_modules/@angular/forms/__ivy_ngcc__/fesm2015/forms.js");
      /* harmony import */


      var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
      /*! @ionic/angular */
      "./node_modules/@ionic/angular/__ivy_ngcc__/fesm2015/ionic-angular.js");
      /* harmony import */


      var _ebook_routing_module__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(
      /*! ./ebook-routing.module */
      "./src/app/pages/tabs/cart/ebook/ebook-routing.module.ts");
      /* harmony import */


      var _ebook_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(
      /*! ./ebook.page */
      "./src/app/pages/tabs/cart/ebook/ebook.page.ts");
      /* harmony import */


      var _components_shared_module__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(
      /*! @components/shared.module */
      "./src/app/components/shared.module.ts");

      var EbookPageModule = function EbookPageModule() {
        _classCallCheck(this, EbookPageModule);
      };

      EbookPageModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"], _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"], _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonicModule"], _ebook_routing_module__WEBPACK_IMPORTED_MODULE_5__["EbookPageRoutingModule"], _components_shared_module__WEBPACK_IMPORTED_MODULE_7__["SharedModule"]],
        declarations: [_ebook_page__WEBPACK_IMPORTED_MODULE_6__["EbookPage"]]
      })], EbookPageModule);
      /***/
    },

    /***/
    "./src/app/pages/tabs/cart/ebook/ebook.page.scss":
    /*!*******************************************************!*\
      !*** ./src/app/pages/tabs/cart/ebook/ebook.page.scss ***!
      \*******************************************************/

    /*! exports provided: default */

    /***/
    function srcAppPagesTabsCartEbookEbookPageScss(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony default export */


      __webpack_exports__["default"] = ".ebook-filter {\n  border: 1px solid #fff;\n  padding: 0px 10px;\n}\n\n.ebook-filter ion-item {\n  --inner-border-width: 0px 0px 0px 0px;\n  --background: transparent;\n  color: #fff;\n}\n\nion-content {\n  --background: #000;\n}\n\n.ebook-title h4 {\n  color: #fff;\n  text-align: center;\n  font-size: 15px;\n}\n\n.ebook-filter-result h4 {\n  color: #fff;\n  text-align: center;\n  font-size: 12px;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvcGFnZXMvdGFicy9jYXJ0L2Vib29rL2Vib29rLnBhZ2Uuc2NzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQTtFQUNJLHNCQUFBO0VBQ0EsaUJBQUE7QUFDSjs7QUFFQTtFQUNJLHFDQUFBO0VBQ0EseUJBQUE7RUFDQSxXQUFBO0FBQ0o7O0FBRUE7RUFDSSxrQkFBQTtBQUNKOztBQUVBO0VBQ0ksV0FBQTtFQUNBLGtCQUFBO0VBQ0EsZUFBQTtBQUNKOztBQUVBO0VBQ0ksV0FBQTtFQUNBLGtCQUFBO0VBQ0EsZUFBQTtBQUNKIiwiZmlsZSI6InNyYy9hcHAvcGFnZXMvdGFicy9jYXJ0L2Vib29rL2Vib29rLnBhZ2Uuc2NzcyIsInNvdXJjZXNDb250ZW50IjpbIi5lYm9vay1maWx0ZXIge1xuICAgIGJvcmRlcjogMXB4IHNvbGlkICNmZmY7XG4gICAgcGFkZGluZzogMHB4IDEwcHg7XG59XG5cbi5lYm9vay1maWx0ZXIgaW9uLWl0ZW0ge1xuICAgIC0taW5uZXItYm9yZGVyLXdpZHRoOiAwcHggMHB4IDBweCAwcHg7XG4gICAgLS1iYWNrZ3JvdW5kOiB0cmFuc3BhcmVudDtcbiAgICBjb2xvcjogI2ZmZjtcbn1cblxuaW9uLWNvbnRlbnQge1xuICAgIC0tYmFja2dyb3VuZDogIzAwMDtcbn1cblxuLmVib29rLXRpdGxlIGg0IHtcbiAgICBjb2xvcjogI2ZmZjtcbiAgICB0ZXh0LWFsaWduOiBjZW50ZXI7XG4gICAgZm9udC1zaXplOiAxNXB4O1xufVxuXG4uZWJvb2stZmlsdGVyLXJlc3VsdCBoNCB7XG4gICAgY29sb3I6ICNmZmY7XG4gICAgdGV4dC1hbGlnbjogY2VudGVyO1xuICAgIGZvbnQtc2l6ZTogMTJweDtcbn0iXX0= */";
      /***/
    },

    /***/
    "./src/app/pages/tabs/cart/ebook/ebook.page.ts":
    /*!*****************************************************!*\
      !*** ./src/app/pages/tabs/cart/ebook/ebook.page.ts ***!
      \*****************************************************/

    /*! exports provided: EbookPage */

    /***/
    function srcAppPagesTabsCartEbookEbookPageTs(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "EbookPage", function () {
        return EbookPage;
      });
      /* harmony import */


      var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! tslib */
      "./node_modules/tslib/tslib.es6.js");
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! @angular/core */
      "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");

      var EbookPage = /*#__PURE__*/function () {
        function EbookPage() {
          _classCallCheck(this, EbookPage);
        }

        _createClass(EbookPage, [{
          key: "ngOnInit",
          value: function ngOnInit() {}
        }]);

        return EbookPage;
      }();

      EbookPage.ctorParameters = function () {
        return [];
      };

      EbookPage = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-ebook',
        template: Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(
        /*! raw-loader!./ebook.page.html */
        "./node_modules/raw-loader/dist/cjs.js!./src/app/pages/tabs/cart/ebook/ebook.page.html"))["default"],
        styles: [Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(
        /*! ./ebook.page.scss */
        "./src/app/pages/tabs/cart/ebook/ebook.page.scss"))["default"]]
      })], EbookPage);
      /***/
    }
  }]);
})();
//# sourceMappingURL=pages-tabs-cart-ebook-ebook-module-es5.js.map